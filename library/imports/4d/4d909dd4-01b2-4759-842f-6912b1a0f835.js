"use strict";
cc._RF.push(module, '4d9093UAbJHWYQvaRKxoPg1', 'UIGaide');
// Script/Guide/UIGaide.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventCenter_1 = require("../Net/EventCenter");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIGaide = /** @class */ (function (_super) {
    __extends(UIGaide, _super);
    function UIGaide() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.maskNode = null;
        return _this;
    }
    UIGaide.prototype.start = function () {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBlockEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchBlockEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchBlockEvent, this);
        EventCenter_1.EventCenter.on("shouguide", this.Show, this);
        this.node.active = false;
    };
    UIGaide.prototype.Show = function (node) {
        //得到当前节点大小
        var contentSize = node.getContentSize();
        this.maskNode.width = contentSize.width + 20;
        this.maskNode.height = contentSize.height + 20;
        //转换世界坐标，再转换到引导节点的本地坐标
        var pos = node.parent.convertToWorldSpaceAR(node.position);
        var localpos = this.maskNode.parent.convertToNodeSpaceAR(pos);
        this.maskNode.setPosition(cc.v2(localpos.x, localpos.y));
        this.node.active = true;
    };
    UIGaide.prototype.onTouchBlockEvent = function (event) {
        //setSwallowTouches：true 不向下触摸
        var pt = this.maskNode.convertToNodeSpaceAR(event.getLocation());
        var rect = cc.rect(0, 0, this.maskNode.width, this.maskNode.height);
        //如果没有命中目标节点，则吞噬向下触摸时间，命中则隐藏引导节点
        if (!rect.contains(pt)) {
            console.log("没有命中");
            this.node._touchListener.setSwallowTouches(true);
            event.stopPropagationImmediate();
            return;
        }
        else {
            console.log("命中");
            this.node._touchListener.setSwallowTouches(false);
        }
        this.node.active = false;
    };
    __decorate([
        property(cc.Node)
    ], UIGaide.prototype, "maskNode", void 0);
    UIGaide = __decorate([
        ccclass
    ], UIGaide);
    return UIGaide;
}(cc.Component));
exports.default = UIGaide;

cc._RF.pop();