"use strict";
cc._RF.push(module, 'c8311M1hzRAw68nMoNfGR8/', 'ItemFriendSee');
// Script/UIScript/Friend/ItemFriendSee.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("../../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemFriendSee = /** @class */ (function (_super) {
    __extends(ItemFriendSee, _super);
    function ItemFriendSee() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.user_name = null;
        _this.user_id = null;
        _this.user_head = null;
        _this.see_btn = null;
        return _this;
    }
    ItemFriendSee.prototype.start = function () {
        this.see_btn.addClick(function () {
            console.log("baifang");
        }, this);
    };
    ItemFriendSee.prototype.setData = function () {
    };
    __decorate([
        property(cc.Label)
    ], ItemFriendSee.prototype, "user_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendSee.prototype, "user_id", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemFriendSee.prototype, "user_head", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], ItemFriendSee.prototype, "see_btn", void 0);
    ItemFriendSee = __decorate([
        ccclass
    ], ItemFriendSee);
    return ItemFriendSee;
}(cc.Component));
exports.default = ItemFriendSee;

cc._RF.pop();