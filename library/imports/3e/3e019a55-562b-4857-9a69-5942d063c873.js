"use strict";
cc._RF.push(module, '3e019pVVitIV5ppWULQY8hz', 'ItemRank');
// Script/UIScript/Rank/ItemRank.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemRank = /** @class */ (function (_super) {
    __extends(ItemRank, _super);
    function ItemRank() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.level_bg = null;
        _this.user_level = null;
        _this.user_name = null;
        _this.user_num = null;
        _this.user_head = null;
        _this.icon = null;
        _this.level_bgs = Array();
        _this.icons = Array();
        return _this;
    }
    ItemRank.prototype.setData = function (type, data, level) {
        if (type === 1) {
            this.icon.spriteFrame = this.icons[0];
        }
        else {
            this.icon.spriteFrame = this.icons[1];
        }
        this.level_bg.active = false;
        this.user_num.node.color = new cc.Color(0, 0, 0);
        if (level < 4) {
            this.level_bg.getComponent(cc.Sprite).spriteFrame = this.level_bgs[level - 1];
            this.level_bg.active = true;
            this.user_num.node.color = new cc.Color(229, 27, 27);
        }
        CocosHelper_1.default.loadHead(data.picUrl, this.user_head);
        this.user_level.string = level.toString();
        this.user_name.string = data.name;
        this.user_num.string = data.money.toString();
    };
    __decorate([
        property(cc.Node)
    ], ItemRank.prototype, "level_bg", void 0);
    __decorate([
        property(cc.Label)
    ], ItemRank.prototype, "user_level", void 0);
    __decorate([
        property(cc.Label)
    ], ItemRank.prototype, "user_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemRank.prototype, "user_num", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemRank.prototype, "user_head", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemRank.prototype, "icon", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], ItemRank.prototype, "level_bgs", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], ItemRank.prototype, "icons", void 0);
    ItemRank = __decorate([
        ccclass
    ], ItemRank);
    return ItemRank;
}(cc.Component));
exports.default = ItemRank;

cc._RF.pop();