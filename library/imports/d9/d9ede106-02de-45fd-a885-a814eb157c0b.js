"use strict";
cc._RF.push(module, 'd9edeEGAt5F/aiFqBTrFXwL', 'ItemFriend');
// Script/UIScript/My/ItemFriend.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemFriend = /** @class */ (function (_super) {
    __extends(ItemFriend, _super);
    function ItemFriend() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.user_name = null;
        _this.user_time = null;
        _this.user_head = null;
        _this.user_level = null;
        return _this;
    }
    ItemFriend.prototype.setData = function (data) {
        this.user_name.string = data.user_name;
        this.user_level.string = data.user_level;
        this.user_time.string = data.add_time;
        CocosHelper_1.default.loadHead(data.user_head, this.user_head);
    };
    __decorate([
        property(cc.Label)
    ], ItemFriend.prototype, "user_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriend.prototype, "user_time", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemFriend.prototype, "user_head", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriend.prototype, "user_level", void 0);
    ItemFriend = __decorate([
        ccclass
    ], ItemFriend);
    return ItemFriend;
}(cc.Component));
exports.default = ItemFriend;

cc._RF.pop();