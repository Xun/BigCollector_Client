"use strict";
cc._RF.push(module, '49da5xdVQtLO6+ecL6HL4ks', 'UIUnlockNewLevel');
// Script/UIScript/Home/UIUnlockNewLevel.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Struct_1 = require("../../Common/Struct");
var SysDefine_1 = require("../../Common/SysDefine");
var UIForm_1 = require("../../Common/UIForm");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIUnlockNewLevel = /** @class */ (function (_super) {
    __extends(UIUnlockNewLevel, _super);
    function UIUnlockNewLevel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // onLoad () {}
        _this.modalType = new Struct_1.ModalType(SysDefine_1.ModalOpacity.OpacityHigh);
        return _this;
    }
    UIUnlockNewLevel.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
    };
    UIUnlockNewLevel.prototype.onShow = function (params) {
        this.view.NewLevelLab.string = "等级" + params.lv;
        this.view.YinPiaoNumLab.string = "银票" + params.money;
    };
    UIUnlockNewLevel = __decorate([
        ccclass
    ], UIUnlockNewLevel);
    return UIUnlockNewLevel;
}(UIForm_1.UIWindow));
exports.default = UIUnlockNewLevel;

cc._RF.pop();