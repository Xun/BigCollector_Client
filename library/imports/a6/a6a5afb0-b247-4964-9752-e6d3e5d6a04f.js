"use strict";
cc._RF.push(module, 'a6a5a+wskdJZJdS5tPl1qBP', 'UIModalScript');
// Script/Common/UIModalScript.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ContentAdapter_1 = require("../Adapter/ContentAdapter");
var UIManager_1 = require("../Manager/UIManager");
var WindowMgr_1 = require("../Manager/WindowMgr");
var CocosHelper_1 = require("../Utils/CocosHelper");
var SysDefine_1 = require("./SysDefine");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIModalScript = /** @class */ (function (_super) {
    __extends(UIModalScript, _super);
    function UIModalScript() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /** 代码创建一个单色texture */
        _this._texture = null;
        return _this;
    }
    UIModalScript.prototype.getSingleTexture = function () {
        if (this._texture)
            return this._texture;
        var data = new Uint8Array(2 * 2 * 4);
        for (var i = 0; i < 2; i++) {
            for (var j = 0; j < 2; j++) {
                data[i * 2 * 4 + j * 4 + 0] = 255;
                data[i * 2 * 4 + j * 4 + 1] = 255;
                data[i * 2 * 4 + j * 4 + 2] = 255;
                data[i * 2 * 4 + j * 4 + 3] = 255;
            }
        }
        var texture = new cc.Texture2D();
        texture.name = 'single color';
        texture.initWithData(data, cc.Texture2D.PixelFormat.RGBA8888, 2, 2);
        texture.handleLoadedTexture();
        this._texture = texture;
        // texture.packable = true;
        texture.addRef();
        return this._texture;
    };
    /**
     * 初始化
     */
    UIModalScript.prototype.init = function () {
        var maskTexture = this.getSingleTexture();
        var size = cc.view.getVisibleSize();
        this.node.height = size.height;
        this.node.width = size.width;
        this.node.addComponent(cc.Button);
        this.node.addComponent(ContentAdapter_1.default);
        this.node.on('click', this.clickMaskWindow, this);
        var sprite = this.node.addComponent(cc.Sprite);
        sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
        sprite.type = cc.Sprite.Type.SIMPLE;
        sprite.spriteFrame = new cc.SpriteFrame(maskTexture);
        this.node.color = new cc.Color(0, 0, 0);
        this.node.opacity = 0;
        this.node.active = false;
    };
    // 
    UIModalScript.prototype.showModal = function (lucenyType, time, isEasing) {
        if (time === void 0) { time = 0.6; }
        if (isEasing === void 0) { isEasing = true; }
        return __awaiter(this, void 0, void 0, function () {
            var o;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        o = 0;
                        switch (lucenyType) {
                            case SysDefine_1.ModalOpacity.None:
                                this.node.active = false;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityZero:
                                o = 0;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityLow:
                                o = 63;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityHalf:
                                o = 126;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityHigh:
                                o = 189;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityFull:
                                o = 255;
                                break;
                        }
                        if (!this.node.active)
                            return [2 /*return*/];
                        if (!isEasing) return [3 /*break*/, 2];
                        return [4 /*yield*/, CocosHelper_1.default.runTweenSync(this.node, cc.tween().to(time, { opacity: o }))];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        this.node.opacity = o;
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UIModalScript.prototype.clickMaskWindow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        com = UIManager_1.default.getInstance().getForm(this.fid);
                        if (!(com && com.modalType.clickMaskClose)) return [3 /*break*/, 2];
                        return [4 /*yield*/, WindowMgr_1.default.close(this.fid)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    UIModalScript = __decorate([
        ccclass
    ], UIModalScript);
    return UIModalScript;
}(cc.Component));
exports.default = UIModalScript;

cc._RF.pop();