"use strict";
cc._RF.push(module, '5e849aoGq9Gf5KX+/JssUhF', 'DataCollection');
// Script/DataModal/DataCollection.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataCollection = void 0;
var DataCollection = /** @class */ (function () {
    function DataCollection() {
        //文房四宝
        this._curWFSBProgress = 0;
        this._WFSBGetMoney = 0;
        //四君子
        this._curSJZProgress = 0;
        this._SJZGetMoney = 0;
        //四大名著
        this._curSDMZProgress = 0;
        this._SDMZGetMoney = 0;
        //四大神兽
        this._curSDSSProgress = 0;
        this._SDSSGetMoney = 0;
        //唐宋八大家
        this._curBDJProgress = 0;
        this._BDJGetMoney = 0;
        //金蟾
        this._curJCProgress = 0;
        this._JCDescribe = "";
        //貔貅
        this._curPXProgress = 0;
        this._PXDescribe = "";
        //金龙
        this._curJLProgress = 0;
        this._JLDescribe = "";
    }
    Object.defineProperty(DataCollection.prototype, "curWFSBProgress", {
        get: function () {
            if (this._curWFSBProgress === undefined) {
                this._curWFSBProgress = 0;
            }
            return this._curWFSBProgress;
        },
        set: function (value) {
            this._curWFSBProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "WFSBGetMoney", {
        get: function () {
            if (this._WFSBGetMoney === undefined) {
                this._WFSBGetMoney = 0;
            }
            return this._WFSBGetMoney;
        },
        set: function (value) {
            this._WFSBGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curSJZProgress", {
        get: function () {
            if (this._curSJZProgress === undefined) {
                this._curSJZProgress = 0;
            }
            return this._curSJZProgress;
        },
        set: function (value) {
            this._curSJZProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "SJZGetMoney", {
        get: function () {
            if (this._SJZGetMoney === undefined) {
                this._SJZGetMoney = 0;
            }
            return this._SJZGetMoney;
        },
        set: function (value) {
            this._SJZGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curSDMZProgress", {
        get: function () {
            if (this._curSDMZProgress === undefined) {
                this._curSDMZProgress = 0;
            }
            return this._curSDMZProgress;
        },
        set: function (value) {
            this._curSDMZProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "SDMZGetMoney", {
        get: function () {
            if (this._SDMZGetMoney === undefined) {
                this._SDMZGetMoney = 0;
            }
            return this._SDMZGetMoney;
        },
        set: function (value) {
            this._SDMZGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curSDSSProgress", {
        get: function () {
            if (this._curSDSSProgress === undefined) {
                this._curSDSSProgress = 0;
            }
            return this._curSDSSProgress;
        },
        set: function (value) {
            this._curSDSSProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "SDSSGetMoney", {
        get: function () {
            if (this._SDSSGetMoney === undefined) {
                this._SDSSGetMoney = 0;
            }
            return this._SDSSGetMoney;
        },
        set: function (value) {
            this._SDSSGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curBDJProgress", {
        get: function () {
            if (this._curBDJProgress === undefined) {
                this._curBDJProgress = 0;
            }
            return this._curBDJProgress;
        },
        set: function (value) {
            this._curBDJProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "BDJGetMoney", {
        get: function () {
            if (this._BDJGetMoney === undefined) {
                this._BDJGetMoney = 0;
            }
            return this._BDJGetMoney;
        },
        set: function (value) {
            this._BDJGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curJCProgress", {
        get: function () {
            if (this._curJCProgress === undefined) {
                this._curJCProgress = 0;
            }
            return this._curJCProgress;
        },
        set: function (value) {
            this._curJCProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "JCDescribe", {
        get: function () {
            if (this._JCDescribe === undefined) {
                this._JCDescribe = "";
            }
            return this._JCDescribe;
        },
        set: function (value) {
            this._JCDescribe = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curPXProgress", {
        get: function () {
            if (this._curPXProgress === undefined) {
                this._curPXProgress = 0;
            }
            return this._curPXProgress;
        },
        set: function (value) {
            this._curPXProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "PXDescribe", {
        get: function () {
            if (this._PXDescribe === undefined) {
                this._PXDescribe = "";
            }
            return this._PXDescribe;
        },
        set: function (value) {
            this._PXDescribe = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curJLProgress", {
        get: function () {
            if (this._curJLProgress === undefined) {
                this._curJLProgress = 0;
            }
            return this._curJLProgress;
        },
        set: function (value) {
            this._curJLProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "JLDescribe", {
        get: function () {
            if (this._JLDescribe === undefined) {
                this._JLDescribe = "";
            }
            return this._JLDescribe;
        },
        set: function (value) {
            this._JLDescribe = value;
        },
        enumerable: false,
        configurable: true
    });
    return DataCollection;
}());
exports.DataCollection = DataCollection;

cc._RF.pop();