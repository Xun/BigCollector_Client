"use strict";
cc._RF.push(module, '2bda8vJmM1CbaeYq3QgXRRS', 'ItemFriendSeeDetail');
// Script/UIScript/Friend/ItemFriendSeeDetail.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("../../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemFriendSeeDetail = /** @class */ (function (_super) {
    __extends(ItemFriendSeeDetail, _super);
    function ItemFriendSeeDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.uname = null;
        _this.uid = null;
        _this.utime = null;
        _this.uhead = null;
        _this.seeBtn = null;
        return _this;
    }
    ItemFriendSeeDetail.prototype.start = function () {
        this.seeBtn.addClick(function () {
            console.log("huifang");
        }, this);
    };
    ItemFriendSeeDetail.prototype.setData = function (data) {
    };
    __decorate([
        property(cc.Label)
    ], ItemFriendSeeDetail.prototype, "uname", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendSeeDetail.prototype, "uid", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendSeeDetail.prototype, "utime", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemFriendSeeDetail.prototype, "uhead", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], ItemFriendSeeDetail.prototype, "seeBtn", void 0);
    ItemFriendSeeDetail = __decorate([
        ccclass
    ], ItemFriendSeeDetail);
    return ItemFriendSeeDetail;
}(cc.Component));
exports.default = ItemFriendSeeDetail;

cc._RF.pop();