"use strict";
cc._RF.push(module, '833b9VVoiRBu4HdjfWzaMYu', 'ItemProduce');
// Script/UIScript/Items/ItemProduce.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemProduce = /** @class */ (function (_super) {
    __extends(ItemProduce, _super);
    function ItemProduce() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.nodeIcon = null;
        _this.nodeBg = null;
        _this.nodeLevel = null;
        _this.txtLevel = null;
        _this.txtCoin = null;
        _this.isDrag = false;
        _this.isUsed = false;
        _this._itemId = 0;
        _this._index = -1;
        _this._itemType = -1;
        _this._nextLevel = -1;
        _this._itemInfo = null;
        return _this;
    }
    // onLoad () {}
    ItemProduce.prototype.start = function () {
    };
    /**
     * 设置工作台物品信息
     * @param {Number} index
     * @param {Number} itemId
     */
    ItemProduce.prototype.setWorkbenchItemInfo = function (index, itemId) {
        var _this = this;
        //设置信息
        // console.log("index:" + index);
        // console.log("itemId:" + itemId);
        this._index = index;
        this._itemId = itemId;
        this._itemType = 1;
        this.isUsed = false;
        this.txtCoin.node.active = true;
        if (this._itemId) {
            this.nodeIcon.active = false;
            this._itemInfo = CocosHelper_1.default.getItemProduceInfoById(this._itemId);
            CocosHelper_1.default.setItemProduceIcon(this._itemId, this.nodeIcon.getComponent(cc.Sprite), function () {
                _this.nodeIcon.active = true;
                _this.txtLevel.string = _this._itemInfo.id.toString();
                _this.nodeLevel.active = true;
                _this.playProduceAni(true);
            });
        }
        else {
            this.nodeIcon.active = false;
            this.nodeLevel.active = false;
            this.txtCoin.node.active = false;
        }
        if (this.isUsed) {
            this.nodeIcon.opacity = 150;
        }
        else if (!this.isDrag) {
            this.nodeIcon.opacity = 255;
        }
    };
    ItemProduce.prototype.dragStart = function () {
        if (this.isUsed || !this._itemId) { //正在使用中，不可拖拽
            return false;
        }
        this.isDrag = true;
        this.nodeIcon.opacity = 150;
        return true;
    };
    ItemProduce.prototype.dragOver = function () {
        if (this.isDrag) {
            this.isDrag = false;
            this.nodeIcon.opacity = 255;
        }
    };
    ItemProduce.prototype.getInfo = function () {
        return this._itemId;
    };
    ItemProduce.prototype.showVirtual = function () {
        this.nodeBg.active = false;
    };
    ItemProduce.prototype.playProduceAni = function (isShow) {
        var _this = this;
        if (isShow) {
            this.schedule(function () {
                CocosHelper_1.default.runTweenSync(_this.txtCoin.node, cc.tween().to(1, { y: 110 }).to(1, { y: 20 }));
                CocosHelper_1.default.runTweenSync(_this.nodeIcon, cc.tween().to(0.5, { scale: 1.1 }).to(0.2, { scale: 1 }));
            }, 5);
            //播放动画
        }
        else {
            this.unscheduleAllCallbacks();
            //隐藏动画
        }
    };
    __decorate([
        property(cc.Node)
    ], ItemProduce.prototype, "nodeIcon", void 0);
    __decorate([
        property(cc.Node)
    ], ItemProduce.prototype, "nodeBg", void 0);
    __decorate([
        property(cc.Node)
    ], ItemProduce.prototype, "nodeLevel", void 0);
    __decorate([
        property(cc.Label)
    ], ItemProduce.prototype, "txtLevel", void 0);
    __decorate([
        property(cc.Label)
    ], ItemProduce.prototype, "txtCoin", void 0);
    ItemProduce = __decorate([
        ccclass
    ], ItemProduce);
    return ItemProduce;
}(cc.Component));
exports.default = ItemProduce;

cc._RF.pop();