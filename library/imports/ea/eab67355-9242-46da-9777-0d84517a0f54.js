"use strict";
cc._RF.push(module, 'eab67NVkkJG2pd3DYRReg9U', 'UIHome_Auto');
// Script/AutoScripts/UIHome_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIHome_Auto = /** @class */ (function (_super) {
    __extends(UIHome_Auto, _super);
    function UIHome_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.accEffect = null;
        _this.Accelerate = null;
        _this.RecoveryBtn = null;
        _this.topNode = null;
        _this.MakeMoneyTxt = null;
        _this.CoinTxt = null;
        _this.DiamondTxt = null;
        _this.MoneyTxt = null;
        _this.HeadSp = null;
        _this.RankBtn = null;
        _this.CollectBtn = null;
        _this.BagBtn = null;
        _this.ShopBtn = null;
        _this.DrawBtn = null;
        _this.WorkContent = null;
        _this.FriendBtn = null;
        _this.SignInBtn = null;
        _this.BuyBtn = null;
        _this.BtnCakeSp = null;
        _this.BuyCoinNumTxt = null;
        _this.MyBtn = null;
        _this.TaskBtn = null;
        _this.CombineAutoBtn = null;
        _this.CombineAutoTxt = null;
        return _this;
    }
    __decorate([
        property(cc.Node)
    ], UIHome_Auto.prototype, "accEffect", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "Accelerate", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "RecoveryBtn", void 0);
    __decorate([
        property(cc.Node)
    ], UIHome_Auto.prototype, "topNode", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "MakeMoneyTxt", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "CoinTxt", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "DiamondTxt", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "MoneyTxt", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIHome_Auto.prototype, "HeadSp", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "RankBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "CollectBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "BagBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "ShopBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "DrawBtn", void 0);
    __decorate([
        property(cc.Node)
    ], UIHome_Auto.prototype, "WorkContent", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "FriendBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "SignInBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "BuyBtn", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIHome_Auto.prototype, "BtnCakeSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "BuyCoinNumTxt", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "MyBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "TaskBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "CombineAutoBtn", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "CombineAutoTxt", void 0);
    UIHome_Auto = __decorate([
        ccclass
    ], UIHome_Auto);
    return UIHome_Auto;
}(cc.Component));
exports.default = UIHome_Auto;

cc._RF.pop();