"use strict";
cc._RF.push(module, 'fd205FAJMBOjLBN37u8ieyG', 'UIDeal_Auto');
// Script/AutoScripts/UIDeal_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDeal_Auto = /** @class */ (function (_super) {
    __extends(UIDeal_Auto, _super);
    function UIDeal_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.CloseBtn = null;
        _this.BtnMyBuy = null;
        _this.BtnMySell = null;
        _this.BtnSelectType = null;
        _this.DealIcon = null;
        _this.BtnDealDetail = null;
        _this.DealToggleNode = null;
        _this.ContentViewNode = null;
        _this.NodeDealItems = null;
        _this.BtnCloseNodeItems = null;
        _this.BtnBi = null;
        _this.BtnMo = null;
        _this.BtnZhi = null;
        _this.BtnYan = null;
        _this.BtnSGYY = null;
        _this.BtnHLM = null;
        _this.BtnXYJ = null;
        _this.BtnSHZ = null;
        _this.BtnL = null;
        _this.BtnM = null;
        _this.BtnZ = null;
        _this.BtnJ = null;
        _this.BtnZQ = null;
        _this.BtnXW = null;
        _this.BtnBH = null;
        _this.BtnQL = null;
        _this.BtnZG = null;
        _this.BtnLZY = null;
        _this.BtnOYX = null;
        _this.BtnWAS = null;
        _this.BtnSX = null;
        _this.BtnSS = null;
        _this.BtnSZ = null;
        _this.BtnHY = null;
        _this.BtnJC = null;
        _this.BtnPX = null;
        _this.BtnJL = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "CloseBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnMyBuy", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnMySell", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSelectType", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIDeal_Auto.prototype, "DealIcon", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnDealDetail", void 0);
    __decorate([
        property(cc.Node)
    ], UIDeal_Auto.prototype, "DealToggleNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIDeal_Auto.prototype, "ContentViewNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIDeal_Auto.prototype, "NodeDealItems", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnCloseNodeItems", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnBi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnMo", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnZhi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnYan", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSGYY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnHLM", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnXYJ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSHZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnL", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnM", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnJ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnZQ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnXW", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnBH", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnQL", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnZG", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnLZY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnOYX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnWAS", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSS", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnHY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnJC", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnPX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnJL", void 0);
    UIDeal_Auto = __decorate([
        ccclass
    ], UIDeal_Auto);
    return UIDeal_Auto;
}(cc.Component));
exports.default = UIDeal_Auto;

cc._RF.pop();