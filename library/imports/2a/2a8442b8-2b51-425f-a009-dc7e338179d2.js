"use strict";
cc._RF.push(module, '2a844K4K1FCX6AJ3H4zgXnS', 'UIScoketReconnent');
// Script/UIScript/UIScoketReconnent.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Struct_1 = require("../Common/Struct");
var SysDefine_1 = require("../Common/SysDefine");
var UIForm_1 = require("../Common/UIForm");
var CocosHelper_1 = require("../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIScoketReconnent = /** @class */ (function (_super) {
    __extends(UIScoketReconnent, _super);
    function UIScoketReconnent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.modalType = new Struct_1.ModalType(SysDefine_1.ModalOpacity.OpacityHigh);
        return _this;
    }
    UIScoketReconnent.prototype.onLoad = function () {
        CocosHelper_1.default.runRepeatTweenSync(this.view.pan, -1, cc.tween().to(6, { angle: -360 }).to(0, { angle: 0 }));
    };
    UIScoketReconnent = __decorate([
        ccclass
    ], UIScoketReconnent);
    return UIScoketReconnent;
}(UIForm_1.UIWindow));
exports.default = UIScoketReconnent;

cc._RF.pop();