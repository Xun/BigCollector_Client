"use strict";
cc._RF.push(module, 'cf520EQdM1Bv7ZY01WPSsFp', 'serviceProto');
// Script/Net/shared/protocols/serviceProto.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.serviceProto = void 0;
exports.serviceProto = {
    "version": 3,
    "services": [
        {
            "id": 0,
            "name": "gate/GetServer",
            "type": "api"
        },
        {
            "id": 1,
            "name": "Chat",
            "type": "msg"
        },
        {
            "id": 2,
            "name": "GameJson",
            "type": "msg"
        },
        {
            "id": 3,
            "name": "LvUp",
            "type": "msg"
        },
        {
            "id": 4,
            "name": "MakeCoin",
            "type": "msg"
        },
        {
            "id": 5,
            "name": "Social",
            "type": "msg"
        },
        {
            "id": 6,
            "name": "UpdateBackpack",
            "type": "msg"
        },
        {
            "id": 7,
            "name": "UpdateUserInfo",
            "type": "msg"
        },
        {
            "id": 8,
            "name": "WorkBench",
            "type": "msg"
        },
        {
            "id": 9,
            "name": "BagInfo",
            "type": "api"
        },
        {
            "id": 10,
            "name": "Convert",
            "type": "api"
        },
        {
            "id": 11,
            "name": "ConvertCoin",
            "type": "api"
        },
        {
            "id": 12,
            "name": "Deal",
            "type": "api"
        },
        {
            "id": 13,
            "name": "DealCancel",
            "type": "api"
        },
        {
            "id": 14,
            "name": "DealDetail",
            "type": "api"
        },
        {
            "id": 15,
            "name": "DealIssue",
            "type": "api"
        },
        {
            "id": 16,
            "name": "DealOrder",
            "type": "api"
        },
        {
            "id": 17,
            "name": "DealType",
            "type": "api"
        },
        {
            "id": 39,
            "name": "Earnings",
            "type": "api"
        },
        {
            "id": 40,
            "name": "FriendEarningsDetail",
            "type": "api"
        },
        {
            "id": 18,
            "name": "GiveFriend",
            "type": "api"
        },
        {
            "id": 41,
            "name": "InviteCode",
            "type": "api"
        },
        {
            "id": 19,
            "name": "Linkedln",
            "type": "api"
        },
        {
            "id": 20,
            "name": "Login",
            "type": "api"
        },
        {
            "id": 21,
            "name": "Rank",
            "type": "api"
        },
        {
            "id": 22,
            "name": "Send",
            "type": "api"
        },
        {
            "id": 23,
            "name": "SerchFriend",
            "type": "api"
        },
        {
            "id": 24,
            "name": "Sign",
            "type": "api"
        },
        {
            "id": 25,
            "name": "SocialSet",
            "type": "api"
        },
        {
            "id": 26,
            "name": "WheelSurf",
            "type": "api"
        },
        {
            "id": 27,
            "name": "WorkBenchCtrl",
            "type": "api"
        },
        {
            "id": 28,
            "name": "remote/Connect",
            "type": "msg"
        },
        {
            "id": 29,
            "name": "remote/Modify",
            "type": "msg"
        },
        {
            "id": 30,
            "name": "remote/ModifyItem",
            "type": "msg"
        },
        {
            "id": 31,
            "name": "remote/ModifyMoney",
            "type": "msg"
        },
        {
            "id": 32,
            "name": "remote/Remote",
            "type": "msg"
        },
        {
            "id": 33,
            "name": "remote/GetGameServer",
            "type": "api"
        },
        {
            "id": 34,
            "name": "remote/Modify",
            "type": "api"
        },
        {
            "id": 35,
            "name": "remote/ModifyItem",
            "type": "api"
        },
        {
            "id": 36,
            "name": "remote/ModifyMoney",
            "type": "api"
        },
        {
            "id": 37,
            "name": "remote/UserLogin",
            "type": "api"
        },
        {
            "id": 38,
            "name": "remote/UserLogout",
            "type": "api"
        }
    ],
    "types": {
        "gate/PtlGetServer/ReqGetServer": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "code",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "gate/PtlGetServer/ResGetServer": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "host",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "port",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgChat/MsgChat": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "content",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "time",
                    "type": {
                        "type": "Date"
                    }
                }
            ]
        },
        "MsgGameJson/MsgGameJson": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "gameJsons",
                    "type": {
                        "type": "Object"
                    }
                }
            ]
        },
        "MsgLvUp/MsgLvUp": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "lv",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "money",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgMakeCoin/MsgMakeCoin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "coin",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgSocial/MsgSocial": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "weChat",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "QQ",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "MsgUpdateBackpack/MsgUpdateBackpack": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 1,
                    "name": "chipCount",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 2,
                    "name": "backpack",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "MsgUpdateBackpack/ItemInfo"
                        }
                    }
                },
                {
                    "id": 3,
                    "name": "bagVol",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgUpdateBackpack/ItemInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "itemID",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "itemCount",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgUpdateUserInfo/MsgUpdateUserInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "coin",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "diamond",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "money",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgWorkBench/MsgWorkBench": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userWorkBench",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Number"
                        }
                    }
                },
                {
                    "id": 1,
                    "name": "makeCoin",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "buyNeedCoin",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlBagInfo/ReqBagInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlBagInfo/ResBagInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "bagVol",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "bagList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "MsgUpdateBackpack/ItemInfo"
                        }
                    }
                }
            ]
        },
        "PtlConvert/ReqConvert": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlConvert/ResConvert": {
            "type": "Interface"
        },
        "PtlConvertCoin/ReqConvertCoin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "num",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlConvertCoin/ResConvertCoin": {
            "type": "Interface"
        },
        "PtlDeal/ReqDeal": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDeal/ResDeal": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlDeal/TransactionInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "guid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "needCount",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "currentCount",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "price",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 5,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 6,
                    "name": "status",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealCancel/ReqDealCancel": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "dealID",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlDealCancel/ResDealCancel": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlDealDetail/ReqDealDetail": {
            "type": "Interface"
        },
        "PtlDealDetail/ResDealDetail": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDealDetail/DealDetailInfo"
                        }
                    }
                }
            ]
        },
        "PtlDealDetail/DealDetailInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "time",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "price",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealIssue/ReqDealIssue": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealType",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "price",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "chipNum",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealIssue/ResDealIssue": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlDealOrder/ReqDealOrder": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealOrder/ResDealOrder": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlDealType/ReqDealType": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "dealID",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealType/ResDealType": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlEarnings/ReqEarnings": {
            "type": "Interface"
        },
        "PtlEarnings/ResEarnings": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "myParentInfo",
                    "type": {
                        "type": "Interface",
                        "properties": [
                            {
                                "id": 0,
                                "name": "name",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 1,
                                "name": "url",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 2,
                                "name": "weChat",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 3,
                                "name": "qq",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 4,
                                "name": "id",
                                "type": {
                                    "type": "String"
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 1,
                    "name": "todayEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "totalEarnings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "friendNum",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlFriendEarningsDetail/ReqFriendEarningsDetail": {
            "type": "Interface"
        },
        "PtlFriendEarningsDetail/ResFriendEarningsDetail": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "todayEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "totalEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "eraningsDetailList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlFriendEarningsDetail/earningsDetailItem"
                        }
                    }
                }
            ]
        },
        "PtlFriendEarningsDetail/earningsDetailItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "time",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "oneFriendEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "twoFriendEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "todayTotalEranings",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlGiveFriend/ReqGiveFriend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "num",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "id",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlGiveFriend/ResGiveFriend": {
            "type": "Interface"
        },
        "PtlInviteCode/ReqInviteCode": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "code",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlInviteCode/ResInviteCode": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "parentInfo",
                    "type": {
                        "type": "Interface",
                        "properties": [
                            {
                                "id": 0,
                                "name": "name",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 1,
                                "name": "url",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 2,
                                "name": "weChat",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 3,
                                "name": "qq",
                                "type": {
                                    "type": "String"
                                }
                            }
                        ]
                    }
                }
            ]
        },
        "PtlLinkedln/ReqLinkedln": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "friendType",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlLinkedln/ResLinkedln": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "friendArray",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlLinkedln/LinkedlnItem"
                        }
                    }
                }
            ]
        },
        "PtlLinkedln/LinkedlnItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "name",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "url",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "time",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "lv",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "active",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlLogin/ReqLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "code",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlLogin/ResLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userInfo",
                    "type": {
                        "type": "Interface",
                        "properties": [
                            {
                                "id": 0,
                                "name": "userName",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 1,
                                "name": "userAvatarUrl",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 2,
                                "name": "userAccount",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 3,
                                "name": "userCoin",
                                "type": {
                                    "type": "Number"
                                }
                            },
                            {
                                "id": 4,
                                "name": "userDiamond",
                                "type": {
                                    "type": "Number"
                                }
                            },
                            {
                                "id": 7,
                                "name": "userMoney",
                                "type": {
                                    "type": "Number"
                                }
                            }
                        ]
                    }
                }
            ]
        },
        "PtlRank/ReqRank": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlRank/ResRank": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "rank",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlRank/RankItem"
                        }
                    }
                }
            ]
        },
        "PtlRank/RankItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "name",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "money",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "picUrl",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlSend/ReqSend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "content",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlSend/ResSend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "time",
                    "type": {
                        "type": "Date"
                    }
                }
            ]
        },
        "PtlSerchFriend/ReqSerchFriend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlSerchFriend/ResSerchFriend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "serchInfo",
                    "type": {
                        "type": "Interface",
                        "properties": [
                            {
                                "id": 0,
                                "name": "name",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 1,
                                "name": "url",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 2,
                                "name": "id",
                                "type": {
                                    "type": "String"
                                }
                            }
                        ]
                    }
                }
            ]
        },
        "PtlSign/ReqSign": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "receiveIndex",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                }
            ]
        },
        "PtlSign/ResSign": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "signDay",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "maxSignDay",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "receive",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Number"
                        }
                    }
                }
            ]
        },
        "PtlSocialSet/ReqSocialSet": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "weChat",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "QQ",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlSocialSet/ResSocialSet": {
            "type": "Interface"
        },
        "PtlWheelSurf/ReqWheelSurf": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "operationId",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlWheelSurf/ResWheelSurf": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "drawId",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "drawCount",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "advertCount",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlWorkBenchCtrl/ReqWorkBenchCtrl": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "ctrlType",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "curPos",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 3,
                    "name": "target",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                }
            ]
        },
        "PtlWorkBenchCtrl/ResWorkBenchCtrl": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userWorkBench",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Number"
                        }
                    }
                }
            ]
        },
        "remote/MsgConnect/MsgConnect": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "serverID",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                }
            ]
        },
        "remote/MsgModify/MsgModify": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "cid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/MsgModifyItem/MsgModifyItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "cid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/MsgModifyMoney/MsgModifyMoney": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "type",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/MsgRemote/MsgRemote": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "arg",
                    "type": {
                        "type": "Any"
                    }
                }
            ]
        },
        "remote/PtlGetGameServer/ReqGetGameServer": {
            "type": "Interface"
        },
        "remote/PtlGetGameServer/ResGetGameServer": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "serverID",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/PtlModify/ReqModify": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "cid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/PtlModify/ResModify": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "result",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "remote/PtlModifyItem/ReqModifyItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "cid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/PtlModifyItem/ResModifyItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "result",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "remote/PtlModifyMoney/ReqModifyMoney": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "type",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/PtlModifyMoney/ResModifyMoney": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "result",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "remote/PtlUserLogin/ReqUserLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "sid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "remote/PtlUserLogin/ResUserLogin": {
            "type": "Interface"
        },
        "remote/PtlUserLogout/ReqUserLogout": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "sid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "remote/PtlUserLogout/ResUserLogout": {
            "type": "Interface"
        }
    }
};

cc._RF.pop();