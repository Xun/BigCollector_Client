"use strict";
cc._RF.push(module, '0ba37eeEWlGJrYOCXeBstW3', 'UIMyWallet_Auto');
// Script/AutoScripts/UIMyWallet_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMyWallet_Auto = /** @class */ (function (_super) {
    __extends(UIMyWallet_Auto, _super);
    function UIMyWallet_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.MyMoneyLab = null;
        _this.BtnTips = null;
        _this.BtnGetMoney = null;
        _this.BtnGetMoneyLog = null;
        _this.MoneyToggleContainer = null;
        _this.BtnWeiXin = null;
        _this.WeiXinShiMingStateLab = null;
        _this.BtnZhiFuBao = null;
        _this.ZhiFuBaoShiMingStateLab = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyWallet_Auto.prototype, "MyMoneyLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnTips", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnGetMoney", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnGetMoneyLog", void 0);
    __decorate([
        property(cc.Node)
    ], UIMyWallet_Auto.prototype, "MoneyToggleContainer", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnWeiXin", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyWallet_Auto.prototype, "WeiXinShiMingStateLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnZhiFuBao", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyWallet_Auto.prototype, "ZhiFuBaoShiMingStateLab", void 0);
    UIMyWallet_Auto = __decorate([
        ccclass
    ], UIMyWallet_Auto);
    return UIMyWallet_Auto;
}(cc.Component));
exports.default = UIMyWallet_Auto;

cc._RF.pop();