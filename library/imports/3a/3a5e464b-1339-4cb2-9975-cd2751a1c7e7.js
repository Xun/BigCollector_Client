"use strict";
cc._RF.push(module, '3a5e4ZLEzlMspl1zSdRocfn', 'NativeMgr');
// Script/Manager/NativeMgr.ts

"use strict";
/*

函数定义：
    andrid -> org.cocos2dx.javascript.Native.SayHello(String helloString, String cbName);
    ios    -> Native.SayHello : (NSString*) helloString
                         arg1 : (NSString*) cbName;
js调用写法
native.call("SayHello", "hello world", (ok) => { })
native.callClass("Native", "SayHello", "hello world", (ok) => { })

注意：
    Android 这边 接受数字的函数都要用float。
        double 在 cocos c++ 代码中未实现
*/
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NativeMgr = exports.AndrodSign = void 0;
var AndrodSign;
(function (AndrodSign) {
    AndrodSign["Void"] = "V";
    AndrodSign["String"] = "Ljava/lang/String;";
    AndrodSign["Boolean"] = "Z";
    AndrodSign["Float"] = "F";
    AndrodSign["Double"] = "D";
    AndrodSign["Int"] = "I";
})(AndrodSign = exports.AndrodSign || (exports.AndrodSign = {}));
var NativeMgr = /** @class */ (function () {
    function NativeMgr() {
    }
    NativeMgr.callback = function (cbID) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var func = this.cbs[cbID];
        // console.log("func:" + func)
        if (func) {
            delete this.cbs[cbID];
            func.apply(void 0, args);
            // console.log("只行了：" + func);
        }
        else {
            cc.log("no func ", cbID);
        }
    };
    NativeMgr._newCB = function (f) {
        this.cbIdx++;
        var cbID = "" + this.cbIdx;
        this.cbs[cbID] = f;
        return cbID;
    };
    NativeMgr.getStr = function (clazz, method) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        return this.callWithPackage.apply(this, __spreadArrays([this.defaultPackage, clazz, method, AndrodSign.String], args));
    };
    NativeMgr.callNativeClass = function (clazz, method) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        console.log("调用1：" + clazz + "   " + method);
        this.callWithPackage.apply(this, __spreadArrays([this.defaultPackage, clazz, method, AndrodSign.Void], args));
    };
    NativeMgr.callWithPackage = function (pkg, clazz, method, returnTypeAndroid) {
        var _a, _b;
        var args = [];
        for (var _i = 4; _i < arguments.length; _i++) {
            args[_i - 4] = arguments[_i];
        }
        var real_args = [];
        console.log("调用2：" + pkg + clazz + "   " + method);
        cc.log("clazz:", clazz);
        cc.log("method:", method);
        if (cc.sys.os == cc.sys.OS_ANDROID) {
            var sig = "";
            for (var i = 0; i < args.length; i++) {
                var v = args[i];
                switch (typeof v) {
                    case 'boolean':
                        sig += AndrodSign.Boolean;
                        real_args.push(v + "");
                        break;
                    case 'string':
                        sig += AndrodSign.String;
                        real_args.push(v);
                        break;
                    case 'number':
                        sig += AndrodSign.Float;
                        real_args.push(v);
                        break;
                    case 'function':
                        sig += AndrodSign.String;
                        real_args.push(this._newCB(v));
                        break;
                }
            }
            return (_a = jsb.reflection).callStaticMethod.apply(_a, __spreadArrays([pkg + clazz, method, "(" + sig + ")" + returnTypeAndroid], real_args));
        }
        if (cc.sys.os == cc.sys.OS_IOS) {
            for (var i = 0; i < args.length; i++) {
                var v = args[i];
                if (typeof v == "function") {
                    real_args.push(this._newCB(v));
                }
                else {
                    real_args.push(v);
                }
                if (i == 0) {
                    method += ":";
                }
                else {
                    method += "arg" + i + ":";
                }
            }
            console.log("clazz:" + clazz);
            console.log("method:" + method);
            //@ts-ignore
            return (_b = jsb.reflection).callStaticMethod.apply(_b, __spreadArrays([clazz, method], real_args));
        }
    };
    NativeMgr.cbIdx = 0;
    NativeMgr.cbs = {};
    NativeMgr.defaultPackage = "org/cocos2dx/javascript/";
    return NativeMgr;
}());
exports.NativeMgr = NativeMgr;
//@ts-ignore
window.nativeMgr = NativeMgr;

cc._RF.pop();