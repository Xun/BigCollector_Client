"use strict";
cc._RF.push(module, 'ec32a86RdVPv6wX2/nK6MG6', 'UIMyExtar');
// Script/UIScript/My/UIMyExtar.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMyExtar = /** @class */ (function (_super) {
    __extends(UIMyExtar, _super);
    function UIMyExtar() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // onLoad () {}
    UIMyExtar.prototype.start = function () {
        var _this = this;
        this.view.BtnClose.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.TodayExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.today_extar_get_money + "元";
        this.view.TodayExtarAddUserNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.today_extar_add_user + "人";
        this.view.MonthExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.month_extar_get_money + "元";
        this.view.MonthExtarAddUserNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.month_extar_add_user + "人";
        this.view.TotalExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_extar_get_money + "元";
        this.view.TotalExtarAddUserNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_extar_add_user + "人";
        this.view.JieDuanNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.cur_stage;
        this.view.JDCurExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.cur_stage_money + "元/";
        this.view.JDTotalExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "元";
        this.view.MultLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.add_mult + "倍增速中";
        this.view.CompleteMoneyLab.string = "已完成" + GameMgr_1.default.dataModalMgr.DataMyExtarInfo.cur_stage_money / GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "%，完成" + GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "元自动转入钱包";
        this.view.JDPS.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.cur_stage_money / GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_stage_moner;
    };
    UIMyExtar = __decorate([
        ccclass
    ], UIMyExtar);
    return UIMyExtar;
}(UIForm_1.UIWindow));
exports.default = UIMyExtar;

cc._RF.pop();