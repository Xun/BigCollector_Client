"use strict";
cc._RF.push(module, 'f9635cnR6JOc6PhThV/LkS/', 'DataRank');
// Script/DataModal/DataRank.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataRankItem = exports.DataRank = void 0;
var DataRank = /** @class */ (function () {
    function DataRank() {
        this.rank_list = Array();
    }
    return DataRank;
}());
exports.DataRank = DataRank;
var DataRankItem = /** @class */ (function () {
    function DataRankItem() {
        this.uid = "";
        this.name = "";
        this.money = 0;
        this.picUrl = "";
    }
    return DataRankItem;
}());
exports.DataRankItem = DataRankItem;

cc._RF.pop();