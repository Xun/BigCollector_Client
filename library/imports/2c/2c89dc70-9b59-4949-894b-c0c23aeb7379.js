"use strict";
cc._RF.push(module, '2c89dxwm1lJSYlLwMI663N5', 'ItemBag');
// Script/UIScript/Bag/ItemBag.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemBag = /** @class */ (function (_super) {
    __extends(ItemBag, _super);
    function ItemBag() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item_name = null;
        _this.item_sp = null;
        return _this;
    }
    ItemBag.prototype.setData = function (item) {
        if (Number(item.itemID) > 1100) {
            var _chipName = CocosHelper_1.default.getShopNameByID(item.itemID.toString());
            this.item_name.string = _chipName + "+" + item.itemCount;
            CocosHelper_1.default.setShopInfo(_chipName, this.item_sp);
        }
        else {
            var _chipName = CocosHelper_1.default.getChipNameByID(item.itemID.toString());
            this.item_name.string = _chipName + "+" + item.itemCount;
            var _iconName = _chipName.replace("碎片", "");
            CocosHelper_1.default.setDealIcon(_iconName, this.item_sp);
        }
    };
    __decorate([
        property(cc.Label)
    ], ItemBag.prototype, "item_name", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemBag.prototype, "item_sp", void 0);
    ItemBag = __decorate([
        ccclass
    ], ItemBag);
    return ItemBag;
}(cc.Component));
exports.default = ItemBag;

cc._RF.pop();