"use strict";
cc._RF.push(module, 'bffc2P1v01J06LhjMuNsq97', 'DataUser');
// Script/DataModal/DataUser.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserInfo = exports.OperationType = void 0;
var CocosHelper_1 = require("../Utils/CocosHelper");
var OperationType;
(function (OperationType) {
    OperationType[OperationType["buy"] = 1] = "buy";
    OperationType[OperationType["sell"] = 2] = "sell";
    OperationType[OperationType["concat"] = 3] = "concat";
    OperationType[OperationType["autoBuy"] = 4] = "autoBuy";
    OperationType[OperationType["autoSell"] = 5] = "autoSell";
})(OperationType = exports.OperationType || (exports.OperationType = {}));
var UserInfo = /** @class */ (function () {
    function UserInfo() {
        //基础信息
        this._userName = "";
        this._userAvatarUrl = "";
        this._userAccount = "";
        //金币
        this._userCoin = 0;
        //钻石
        this._userDiamond = 0;
        //金额
        this._userMoney = 0;
        //当前购买所需金币
        this._userCurNeedBuyCoinNum = 0;
        //当前每秒生产金币
        this._userMakeCoin = 0;
        //工作台数据
        this._userWorkbench = new Array();
        //当前解锁等级
        this._userUnlockLevel = 1;
        //自动合成时间
        this._combineAutoTime = 15;
        this._UserInfos = null;
    }
    Object.defineProperty(UserInfo.prototype, "userName", {
        get: function () {
            if (this._userName === undefined) {
                this._userName = "";
            }
            return this._userName;
        },
        set: function (value) {
            this._userName = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userAvatarUrl", {
        get: function () {
            if (this._userAvatarUrl === undefined) {
                this._userAvatarUrl = "";
            }
            return this._userAvatarUrl;
        },
        set: function (value) {
            this._userAvatarUrl = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userAccount", {
        get: function () {
            if (this._userAccount === undefined) {
                this._userAccount = "";
            }
            return this._userAccount;
        },
        set: function (value) {
            this._userAccount = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userCoin", {
        get: function () {
            if (this._userCoin === undefined) {
                this._userCoin = 0;
            }
            return this._userCoin;
        },
        set: function (value) {
            this._userCoin = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userDiamond", {
        get: function () {
            if (this._userDiamond === undefined) {
                this._userDiamond = 0;
            }
            return this._userDiamond;
        },
        set: function (value) {
            this._userDiamond = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userMoney", {
        get: function () {
            if (this._userMoney === undefined) {
                this._userMoney = 0;
            }
            return this._userMoney;
        },
        set: function (value) {
            this._userMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userCurNeedBuyCoinNum", {
        get: function () {
            if (this._userCurNeedBuyCoinNum === undefined) {
                this._userCurNeedBuyCoinNum = 0;
            }
            return this._userCurNeedBuyCoinNum;
        },
        set: function (value) {
            this._userCurNeedBuyCoinNum = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userMakeCoin", {
        get: function () {
            if (this._userMakeCoin === undefined) {
                this._userMakeCoin = 0;
            }
            return this._userMakeCoin;
        },
        set: function (value) {
            this._userMakeCoin = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userWorkbench", {
        get: function () {
            if (this._userWorkbench === undefined) {
                this._userWorkbench = [];
            }
            return this._userWorkbench;
        },
        set: function (value) {
            this._userWorkbench = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userUnlockLevel", {
        get: function () {
            if (this._userUnlockLevel === undefined) {
                this._userUnlockLevel = 1;
            }
            return this._userUnlockLevel;
        },
        set: function (value) {
            this._userUnlockLevel = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "combineAutoTime", {
        get: function () {
            if (this._combineAutoTime === undefined) {
                this._combineAutoTime = 15;
            }
            return this._combineAutoTime;
        },
        set: function (value) {
            this._combineAutoTime = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userInfos", {
        get: function () {
            return this._UserInfos;
        },
        set: function (v) {
            this._UserInfos = v;
        },
        enumerable: false,
        configurable: true
    });
    //是否还有空余位置
    UserInfo.prototype.hasPosAtWorkbench = function () {
        for (var idx = 0; idx < this._userWorkbench.length; idx++) {
            if (this._userWorkbench[idx] === 0) {
                return true;
            }
        }
        return false;
    };
    ;
    /**
     * 增加物品到工作台
     * @param {string} itemId
     * @return {number} 添加到哪个位置
     */
    UserInfo.prototype.checkNullPos = function () {
        //检查有没有空位置
        if (!this.hasPosAtWorkbench()) {
            return -1;
        }
        //找到空位置
        for (var idx = 0; idx < this._userWorkbench.length; idx++) {
            if (this._userWorkbench[idx] === 0) {
                // this._userWorkbench[idx] = itemId;
                return idx;
            }
        }
    };
    /**
     * 物品结合
     */
    UserInfo.prototype.combineCake = function (originIndex, targetIndex, callback) {
        if (!this._userWorkbench) {
            callback(false);
            return false;
        }
        if (this._userWorkbench.length <= originIndex) {
            callback(false);
            return false;
        }
        if (this._userWorkbench.length <= targetIndex) {
            callback(false);
            return false;
        }
        var targetItemId = this._userWorkbench[targetIndex];
        if (this._userWorkbench[originIndex] !== targetItemId) {
            callback(false);
            return false;
        }
        var itemInfo = CocosHelper_1.default.getItemProduceInfoById(targetItemId);
        if (!itemInfo) {
            //未找到信息，不能判断下一步进化到那一步骤
            callback(false);
            return false;
        }
        if (!itemInfo.nextLevel) {
            callback(false, 'maxLevel');
            return false;
        }
        this._userWorkbench[targetIndex] = itemInfo.nextLevel;
        this._userWorkbench[originIndex] = 0; //将原有的置空
        var nextLevel = itemInfo.nextLevel;
        var isUnlock = this.unlockProduceLevel(nextLevel);
        callback(true, isUnlock);
        return true;
    };
    /**
     * 解锁新等级
     * @param nextLevel --新等级
     */
    UserInfo.prototype.unlockProduceLevel = function (nextLevel) {
        if (nextLevel > this._userUnlockLevel) {
            var isOnlyMaxLevel = true;
            for (var idx = 0; idx < this._userWorkbench.length; idx++) {
                if (nextLevel < Number(this._userWorkbench[idx])) {
                    isOnlyMaxLevel = false;
                    break;
                }
            }
            this._userUnlockLevel = nextLevel;
            return isOnlyMaxLevel;
        }
        return false;
    };
    return UserInfo;
}());
exports.UserInfo = UserInfo;

cc._RF.pop();