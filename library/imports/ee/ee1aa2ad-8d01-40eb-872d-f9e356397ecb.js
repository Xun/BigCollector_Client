"use strict";
cc._RF.push(module, 'ee1aaKtjQFA64ct+eNWOX7L', 'UIShare_Auto');
// Script/AutoScripts/UIShare_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIShare_Auto = /** @class */ (function (_super) {
    __extends(UIShare_Auto, _super);
    function UIShare_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.CloseBtn = null;
        _this.WechatBtn = null;
        _this.PengYouQuanBtn = null;
        _this.PhotoBtn = null;
        _this.contentNode = null;
        _this.HeadSp1 = null;
        _this.NameLab1 = null;
        _this.HeadSp2 = null;
        _this.NameLab2 = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShare_Auto.prototype, "CloseBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShare_Auto.prototype, "WechatBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShare_Auto.prototype, "PengYouQuanBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShare_Auto.prototype, "PhotoBtn", void 0);
    __decorate([
        property(cc.Node)
    ], UIShare_Auto.prototype, "contentNode", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIShare_Auto.prototype, "HeadSp1", void 0);
    __decorate([
        property(cc.Label)
    ], UIShare_Auto.prototype, "NameLab1", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIShare_Auto.prototype, "HeadSp2", void 0);
    __decorate([
        property(cc.Label)
    ], UIShare_Auto.prototype, "NameLab2", void 0);
    UIShare_Auto = __decorate([
        ccclass
    ], UIShare_Auto);
    return UIShare_Auto;
}(cc.Component));
exports.default = UIShare_Auto;

cc._RF.pop();