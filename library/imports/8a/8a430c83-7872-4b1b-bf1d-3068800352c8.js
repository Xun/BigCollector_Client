"use strict";
cc._RF.push(module, '8a430yDeHJLG78dMGiAA1LI', 'LocalStorageMgr');
// Script/Manager/LocalStorageMgr.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LocalStorageMgr = /** @class */ (function () {
    function LocalStorageMgr() {
    }
    LocalStorageMgr.setItem = function (key, value) {
        cc.sys.localStorage.setItem("model_" + key, JSON.stringify(value));
    };
    LocalStorageMgr.getItem = function (key) {
        var data = JSON.parse(cc.sys.localStorage.getItem("model_" + key));
        if (!data || data === "") {
            return {};
        }
        return data;
    };
    LocalStorageMgr.removeItem = function (key) {
        cc.sys.localStorage.removeItem("model_" + key);
    };
    LocalStorageMgr.clearAll = function () {
        cc.sys.localStorage.clear();
    };
    return LocalStorageMgr;
}());
exports.default = LocalStorageMgr;

cc._RF.pop();