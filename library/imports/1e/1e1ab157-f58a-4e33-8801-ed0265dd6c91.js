"use strict";
cc._RF.push(module, '1e1abFX9YpOM4gB7QJl3WyR', 'UIFriend_Auto');
// Script/AutoScripts/UIFriend_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriend_Auto = /** @class */ (function (_super) {
    __extends(UIFriend_Auto, _super);
    function UIFriend_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.BtnInvoedFriend = null;
        _this.BtnMyFriend = null;
        _this.MyFriendNumLab = null;
        _this.ProbablyMoneyLab = null;
        _this.ProbablyDetail = null;
        _this.TodayGetMoneyLab = null;
        _this.TotalGetMoneyLab = null;
        _this.UpChannel = null;
        _this.ExtarTip = null;
        _this.TodayExtarGetMoneyLab = null;
        _this.TodayExtarGetMoneyTipsLab = null;
        _this.Call = null;
        _this.Name = null;
        _this.HeadSp = null;
        _this.TodayAddYushi = null;
        _this.TotalYushi = null;
        _this.BtnGive = null;
        _this.BtnVisit = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnInvoedFriend", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnMyFriend", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "MyFriendNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "ProbablyMoneyLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "ProbablyDetail", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TodayGetMoneyLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TotalGetMoneyLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "UpChannel", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "ExtarTip", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TodayExtarGetMoneyLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TodayExtarGetMoneyTipsLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "Call", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "Name", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIFriend_Auto.prototype, "HeadSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TodayAddYushi", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TotalYushi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnGive", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnVisit", void 0);
    UIFriend_Auto = __decorate([
        ccclass
    ], UIFriend_Auto);
    return UIFriend_Auto;
}(cc.Component));
exports.default = UIFriend_Auto;

cc._RF.pop();