"use strict";
cc._RF.push(module, '86cccZOwR5BRaVFT2dQpxD9', 'DataDeal');
// Script/DataModal/DataDeal.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataDealDetailItem = exports.DataDealItem = exports.DataDeal = void 0;
var DataDeal = /** @class */ (function () {
    function DataDeal() {
        this.dealBuyList = Array();
        this.dealSellList = Array();
        this.dealMyBuyList = Array();
        this.dealMySellList = Array();
        this.dealDetailList = Array();
        // public dealList: [];
    }
    return DataDeal;
}());
exports.DataDeal = DataDeal;
var DataDealItem = /** @class */ (function () {
    function DataDealItem() {
    }
    return DataDealItem;
}());
exports.DataDealItem = DataDealItem;
var DataDealDetailItem = /** @class */ (function () {
    function DataDealDetailItem() {
        this.chipID = 0;
        this.time = 0;
        this.type = 0;
        this.count = 0;
        this.price = 0;
    }
    return DataDealDetailItem;
}());
exports.DataDealDetailItem = DataDealDetailItem;

cc._RF.pop();