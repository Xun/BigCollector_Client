"use strict";
cc._RF.push(module, 'a2975EwmBRNKaMDx+6NEG7u', 'UILottery');
// Script/UIScript/Lottery/UILottery.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Struct_1 = require("../../Common/Struct");
var SysDefine_1 = require("../../Common/SysDefine");
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UILottery = /** @class */ (function (_super) {
    __extends(UILottery, _super);
    function UILottery() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.modalType = new Struct_1.ModalType(SysDefine_1.ModalOpacity.OpacityHigh);
        _this.lotteryItem = null;
        return _this;
    }
    UILottery.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () { _this.closeSelf(); }, this);
        this.view.GoBtn.addClick(this.onGoClick, this);
        for (var i = 0; i < this.view.LotteryReward.childrenCount; i++) {
            var parentNode = this.view.LotteryReward.children[i];
            var rewardItem = cc.instantiate(this.lotteryItem);
            rewardItem.getComponent("ItemLottery").setInfo({ num: 100 + i, icon: (i + 1).toString() });
            rewardItem.parent = parentNode;
        }
    };
    UILottery.prototype.onGoClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var datas, index;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.view.GoBtn.interactable = false;
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("WheelSurf", {
                                userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount,
                                operationId: 1
                            })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                        }
                        else {
                            this.view.NumTxt.string = "\u6BCF\u592924\uFF1A00\u70B9\u6062\u590D\u62BD\u5956\u6B21\u6570\uFF0C\u5F53\u524D\u5269\u4F59" + datas.res.drawCount + "\u6B21";
                            index = (360 / 8 * datas.res.drawId) + 360 * 5;
                            CocosHelper_1.default.runTweenSync(this.view.LotteryReward, cc.tween()
                                .to(5, { angle: -index }, cc.easeInOut(3.0))).then(function () {
                                _this.view.GoBtn.interactable = true;
                                FormMgr_1.default.open(UIConfig_1.default.UILotteryGetTip, 1);
                                _this.view.LotteryReward.angle = 0;
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        property(cc.Prefab)
    ], UILottery.prototype, "lotteryItem", void 0);
    UILottery = __decorate([
        ccclass
    ], UILottery);
    return UILottery;
}(UIForm_1.UIWindow));
exports.default = UILottery;

cc._RF.pop();