"use strict";
cc._RF.push(module, 'ac0411e/YpCgrETAlXWXJNy', 'UIMy_Auto');
// Script/AutoScripts/UIMy_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMy_Auto = /** @class */ (function (_super) {
    __extends(UIMy_Auto, _super);
    function UIMy_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.HeadSp = null;
        _this.NameLab = null;
        _this.IDLab = null;
        _this.BtnClose = null;
        _this.MoneyNode = null;
        _this.MyMoneyLab = null;
        _this.BtnGetMoney = null;
        _this.BtnFriend = null;
        _this.BtnVip = null;
        _this.BtnShare = null;
        _this.BtnPlay = null;
        _this.BtnSetting = null;
        return _this;
    }
    __decorate([
        property(cc.Sprite)
    ], UIMy_Auto.prototype, "HeadSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIMy_Auto.prototype, "NameLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMy_Auto.prototype, "IDLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Node)
    ], UIMy_Auto.prototype, "MoneyNode", void 0);
    __decorate([
        property(cc.Label)
    ], UIMy_Auto.prototype, "MyMoneyLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnGetMoney", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnFriend", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnVip", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnShare", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnPlay", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnSetting", void 0);
    UIMy_Auto = __decorate([
        ccclass
    ], UIMy_Auto);
    return UIMy_Auto;
}(cc.Component));
exports.default = UIMy_Auto;

cc._RF.pop();