"use strict";
cc._RF.push(module, '458d9swLH9Pm7SGFvP8V6H5', 'UILogin');
// Script/UIScript/UILogin.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../Common/UIForm");
var FormMgr_1 = require("../Manager/FormMgr");
var GameMgr_1 = require("../Manager/GameMgr");
var RpcConent_1 = require("../Net/RpcConent");
var UIConfig_1 = require("../UIConfig");
var UIToast_1 = require("./UIToast");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UILogin = /** @class */ (function (_super) {
    __extends(UILogin, _super);
    function UILogin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UILogin_1 = UILogin;
    UILogin.prototype.onLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RpcConent_1.apiClient.flows.postDisconnectFlow.push(function (v) {
                            if (!v.isManual) {
                                setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                                    var res;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4 /*yield*/, RpcConent_1.apiClient.connect()];
                                            case 1:
                                                res = _a.sent();
                                                if (!res.isSucc) {
                                                    console.log(result.errMsg);
                                                    FormMgr_1.default.open(UIConfig_1.default.UIScoketReconnent);
                                                }
                                                return [2 /*return*/];
                                        }
                                    });
                                }); });
                            }
                            return v;
                        });
                        RpcConent_1.apiClient.listenMsg("WorkBench", function (msg) {
                            GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench = msg.userWorkBench;
                            GameMgr_1.default.dataModalMgr.UserInfo.userCurNeedBuyCoinNum = msg.buyNeedCoin;
                        });
                        return [4 /*yield*/, RpcConent_1.apiClient.connect()];
                    case 1:
                        result = _a.sent();
                        // 连接不一定成功（例如网络错误），所以要记得错误处理
                        if (!result.isSucc) {
                            console.log(result.errMsg);
                            FormMgr_1.default.open(UIConfig_1.default.UIScoketReconnent);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    UILogin.wechatCall = function (code) {
        return __awaiter(this, void 0, void 0, function () {
            var datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("code:" + code);
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("Login", {
                                code: code
                            })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            FormMgr_1.default.open(UIConfig_1.default.UIScoketReconnent);
                            return [2 /*return*/];
                        }
                        // console.log()
                        GameMgr_1.default.dataModalMgr.UserInfo.userAccount = datas.res.userInfo.userAccount;
                        GameMgr_1.default.dataModalMgr.UserInfo.userName = datas.res.userInfo.userName;
                        // GameMgr.dataModalMgr.UserInfo.userAvatarUrl = datas.res.userInfo.userAvatarUrl;
                        GameMgr_1.default.dataModalMgr.UserInfo.userAvatarUrl = "https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=false&word=%E5%A4%B4%E5%83%8F&hs=0&pn=4&spn=0&di=7146857200093233153&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&ie=utf-8&oe=utf-8&cl=2&lm=-1&cs=3172226535%2C2587509282&os=1391451534%2C125733970&simid=59149375%2C589686028&adpicid=0&lpn=0&ln=30&fr=ala&fm=&sme=&cg=head&bdtype=0&oriquery=%E5%A4%B4%E5%83%8F&objurl=https%3A%2F%2Fgimg2.baidu.com%2Fimage_search%2Fsrc%3Dhttp%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202107%2F09%2F20210709142454_dc8dc.thumb.1000_0.jpeg%26refer%3Dhttp%3A%2F%2Fc-ssl.duitang.com%26app%3D2002%26size%3Df9999%2C10000%26q%3Da80%26n%3D0%26g%3D0n%26fmt%3Dauto%3Fsec%3D1670321388%26t%3D27faa2c2687ab622529fa60f58795d7f&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B17tpwg2_z%26e3Bv54AzdH3Fks52AzdH3F%3Ft1%3D8nc000m8an&gsm=&islist=&querylist=&dyTabStr=MCwzLDEsNSwyLDcsOCw2LDQsOQ%3D%3D";
                        GameMgr_1.default.dataModalMgr.UserInfo.userCoin = datas.res.userInfo.userCoin;
                        GameMgr_1.default.dataModalMgr.UserInfo.userDiamond = datas.res.userInfo.userDiamond;
                        GameMgr_1.default.dataModalMgr.UserInfo.userMoney = datas.res.userInfo.userMoney;
                        FormMgr_1.default.open(UIConfig_1.default.UIHome);
                        return [2 /*return*/];
                }
            });
        });
    };
    UILogin.prototype.start = function () {
        var _this = this;
        this.view.BtnWechat.addClick(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.login();
                return [2 /*return*/];
            });
        }); }, this);
    };
    UILogin.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isAgarn;
            return __generator(this, function (_a) {
                isAgarn = this.view.UserAgarn.getComponent(cc.Toggle).isChecked;
                if (!isAgarn) {
                    UIToast_1.default.popUp("请阅读并同意《用户协议》");
                    return [2 /*return*/];
                }
                // NativeMgr.callNativeClass("AppActivity", "loginWX", UILogin.testRpc);
                // console.log("调用完成")
                UILogin_1.wechatCall(this.view.testAccount.string);
                return [2 /*return*/];
            });
        });
    };
    var UILogin_1;
    UILogin = UILogin_1 = __decorate([
        ccclass
    ], UILogin);
    return UILogin;
}(UIForm_1.UIScreen));
exports.default = UILogin;

cc._RF.pop();