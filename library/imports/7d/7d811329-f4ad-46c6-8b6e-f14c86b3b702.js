"use strict";
cc._RF.push(module, '7d811Mp9K1Gxotu8UyGs7cC', 'CardArrayFlip_FrontCardBase');
// Script/UIScript/RotateAndFlip/CardArrayFlip_FrontCardBase.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CardArrayFlip_FrontCardBase = /** @class */ (function (_super) {
    __extends(CardArrayFlip_FrontCardBase, _super);
    function CardArrayFlip_FrontCardBase() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.main = null;
        _this.back = null;
        _this.front = null;
        return _this;
    }
    CardArrayFlip_FrontCardBase.prototype.onLoad = function () {
        this.init();
    };
    /**
     * 初始化
     */
    CardArrayFlip_FrontCardBase.prototype.init = function () {
        // 隐藏
        this.hide();
        // 重置
        this.front.active = false;
        this.back.active = true;
    };
    /**
     * 展示
     */
    CardArrayFlip_FrontCardBase.prototype.show = function () {
        this.main.active = true;
    };
    /**
     * 隐藏
     */
    CardArrayFlip_FrontCardBase.prototype.hide = function () {
        this.main.active = false;
    };
    /**
     * 翻到正面
     */
    CardArrayFlip_FrontCardBase.prototype.flipToFront = function () {
        return null;
    };
    /**
     * 翻到背面
     */
    CardArrayFlip_FrontCardBase.prototype.flipToBack = function () {
        return null;
    };
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_FrontCardBase.prototype, "main", void 0);
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_FrontCardBase.prototype, "back", void 0);
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_FrontCardBase.prototype, "front", void 0);
    CardArrayFlip_FrontCardBase = __decorate([
        ccclass
    ], CardArrayFlip_FrontCardBase);
    return CardArrayFlip_FrontCardBase;
}(cc.Component));
exports.default = CardArrayFlip_FrontCardBase;

cc._RF.pop();