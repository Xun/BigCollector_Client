"use strict";
cc._RF.push(module, '64643IPUkZDGb5aHw/lReOt', 'UIConfig');
// Script/UIConfig.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UIConfig = /** @class */ (function () {
    function UIConfig() {
    }
    UIConfig.UIHome = {
        prefabUrl: "Forms/Screen/UIHome",
        type: "UIScreen"
    };
    UIConfig.UILogin = {
        prefabUrl: "Forms/Screen/UILogin",
        type: "UIScreen"
    };
    UIConfig.UIStart = {
        prefabUrl: "Forms/Screen/UIStart",
        type: "UIScreen"
    };
    UIConfig.UILoading = {
        prefabUrl: "Forms/Tips/UILoading",
        type: "UITips"
    };
    UIConfig.UIBag = {
        prefabUrl: "Forms/Windows/UIBag",
        type: "UIWindow"
    };
    UIConfig.UIBagAdd = {
        prefabUrl: "Forms/Windows/UIBagAdd",
        type: "UIWindow"
    };
    UIConfig.UIBuyTip = {
        prefabUrl: "Forms/Windows/UIBuyTip",
        type: "UIWindow"
    };
    UIConfig.UIColleCtionConversion = {
        prefabUrl: "Forms/Windows/UIColleCtionConversion",
        type: "UIWindow"
    };
    UIConfig.UICollection = {
        prefabUrl: "Forms/Windows/UICollection",
        type: "UIWindow"
    };
    UIConfig.UIDeal = {
        prefabUrl: "Forms/Windows/UIDeal",
        type: "UIWindow"
    };
    UIConfig.UIDebris = {
        prefabUrl: "Forms/Windows/UIDebris",
        type: "UIWindow"
    };
    UIConfig.UIDialog = {
        prefabUrl: "Forms/Windows/UIDialog",
        type: "UIWindow"
    };
    UIConfig.UIExtarCommitSure = {
        prefabUrl: "Forms/Windows/UIExtarCommitSure",
        type: "UIWindow"
    };
    UIConfig.UIFriend = {
        prefabUrl: "Forms/Windows/UIFriend",
        type: "UIWindow"
    };
    UIConfig.UIFriendGive = {
        prefabUrl: "Forms/Windows/UIFriendGive",
        type: "UIWindow"
    };
    UIConfig.UIFriendGiveDetail = {
        prefabUrl: "Forms/Windows/UIFriendGiveDetail",
        type: "UIWindow"
    };
    UIConfig.UIFriendMonerDetail = {
        prefabUrl: "Forms/Windows/UIFriendMonerDetail",
        type: "UIWindow"
    };
    UIConfig.UIFriendSee = {
        prefabUrl: "Forms/Windows/UIFriendSee",
        type: "UIWindow"
    };
    UIConfig.UIFriendSeeDetail = {
        prefabUrl: "Forms/Windows/UIFriendSeeDetail",
        type: "UIWindow"
    };
    UIConfig.UIFriendSerch = {
        prefabUrl: "Forms/Windows/UIFriendSerch",
        type: "UIWindow"
    };
    UIConfig.UIGetMoneyLog = {
        prefabUrl: "Forms/Windows/UIGetMoneyLog",
        type: "UIWindow"
    };
    UIConfig.UIGetMoneyRule = {
        prefabUrl: "Forms/Windows/UIGetMoneyRule",
        type: "UIWindow"
    };
    UIConfig.UIInviteGetInfo = {
        prefabUrl: "Forms/Windows/UIInviteGetInfo",
        type: "UIWindow"
    };
    UIConfig.UILottery = {
        prefabUrl: "Forms/Windows/UILottery",
        type: "UIWindow"
    };
    UIConfig.UILotteryGetTip = {
        prefabUrl: "Forms/Windows/UILotteryGetTip",
        type: "UIWindow"
    };
    UIConfig.UIMy = {
        prefabUrl: "Forms/Windows/UIMy",
        type: "UIWindow"
    };
    UIConfig.UIMyExtar = {
        prefabUrl: "Forms/Windows/UIMyExtar",
        type: "UIWindow"
    };
    UIConfig.UIMyFriend = {
        prefabUrl: "Forms/Windows/UIMyFriend",
        type: "UIWindow"
    };
    UIConfig.UIMyWallet = {
        prefabUrl: "Forms/Windows/UIMyWallet",
        type: "UIWindow"
    };
    UIConfig.UIOline = {
        prefabUrl: "Forms/Windows/UIOline",
        type: "UIWindow"
    };
    UIConfig.UIRank = {
        prefabUrl: "Forms/Windows/UIRank",
        type: "UIWindow"
    };
    UIConfig.UIRecoverTip = {
        prefabUrl: "Forms/Windows/UIRecoverTip",
        type: "UIWindow"
    };
    UIConfig.UIRotateAndFlip = {
        prefabUrl: "Forms/Windows/UIRotateAndFlip",
        type: "UIWindow"
    };
    UIConfig.UIScoketReconnent = {
        prefabUrl: "Forms/Windows/UIScoketReconnent",
        type: "UIWindow"
    };
    UIConfig.UISetSocial = {
        prefabUrl: "Forms/Windows/UISetSocial",
        type: "UIWindow"
    };
    UIConfig.UISetting = {
        prefabUrl: "Forms/Windows/UISetting",
        type: "UIWindow"
    };
    UIConfig.UIShare = {
        prefabUrl: "Forms/Windows/UIShare",
        type: "UIWindow"
    };
    UIConfig.UIShiMing = {
        prefabUrl: "Forms/Windows/UIShiMing",
        type: "UIWindow"
    };
    UIConfig.UIShop = {
        prefabUrl: "Forms/Windows/UIShop",
        type: "UIWindow"
    };
    UIConfig.UISignIn = {
        prefabUrl: "Forms/Windows/UISignIn",
        type: "UIWindow"
    };
    UIConfig.UITask = {
        prefabUrl: "Forms/Windows/UITask",
        type: "UIWindow"
    };
    UIConfig.UIUnlockNewLevel = {
        prefabUrl: "Forms/Windows/UIUnlockNewLevel",
        type: "UIWindow"
    };
    UIConfig.UIDealDetail = {
        prefabUrl: "Forms/Windows/UIDealDetail",
        type: "UIWindow"
    };
    UIConfig.UIDealMyGet = {
        prefabUrl: "Forms/Windows/UIDealMyGet",
        type: "UIWindow"
    };
    UIConfig.UIDealAdd = {
        prefabUrl: "Forms/Windows/UIDealAdd",
        type: "UIWindow"
    };
    UIConfig.UIDealMySell = {
        prefabUrl: "Forms/Windows/UIDealMySell",
        type: "UIWindow"
    };
    UIConfig.UIDealSell = {
        prefabUrl: "Forms/Windows/UIDealSell",
        type: "UIWindow"
    };
    UIConfig.UIDealTip = {
        prefabUrl: "Forms/Windows/UIDealTip",
        type: "UIWindow"
    };
    return UIConfig;
}());
exports.default = UIConfig;

cc._RF.pop();