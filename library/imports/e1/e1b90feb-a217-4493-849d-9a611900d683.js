"use strict";
cc._RF.push(module, 'e1b90/rohdEk4SdmmEZANaD', 'Main');
// Script/Main.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("./Manager/GameMgr");
var RpcConent_1 = require("./Net/RpcConent");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Main = /** @class */ (function (_super) {
    __extends(Main, _super);
    function Main() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Main.prototype.onLoad = function () {
    };
    Main.prototype.start = function () {
        var _this = this;
        RpcConent_1.apiClient.listenMsg("UpdateBackpack", function (data) {
            _this.upDataChips(data);
        });
        RpcConent_1.apiClient.listenMsg("GameJson", function (data) {
            _this.setGameJsons(data);
        });
    };
    Main.prototype.upDataChips = function (data) {
        GameMgr_1.default.dataModalMgr.BagInfo.list_item = data.backpack;
        GameMgr_1.default.dataModalMgr.BagInfo.total_bag_capacity = data.bagVol;
    };
    Main.prototype.setGameJsons = function (data) {
        for (var key in data.gameJsons.shop) {
            var element = data.gameJsons.shop[key];
            GameMgr_1.default.dataModalMgr.ShopInfoJson.shopInfo.push(element);
        }
        GameMgr_1.default.dataModalMgr.ChipsInfoJson.chipInfo = data.gameJsons.chips;
    };
    Main.prototype.adaptiveNoteLayout = function () {
        var winSize = cc.winSize; //获取当前游戏窗口大小
        cc.log("--当前游戏窗口大小  w:" + winSize.width + "   h:" + winSize.height);
        var viewSize = cc.view.getFrameSize();
        cc.log("--视图边框尺寸：w:" + viewSize.width + "  h:" + viewSize.height);
        var canvasSize = cc.view.getCanvasSize(); //视图中canvas尺寸
        cc.log("--视图中canvas尺寸  w:" + canvasSize.width + "  H:" + canvasSize.height);
        var visibleSize = cc.view.getVisibleSize();
        cc.log("--视图中窗口可见区域的尺寸 w:" + visibleSize.width + "   h:" + visibleSize.height);
        var designSize = cc.view.getDesignResolutionSize();
        cc.log("--设计分辨率：" + designSize.width + "    h: " + designSize.height);
        cc.log("--当前节点的尺寸 w:" + this.node.width + "   h:" + this.node.height);
    };
    Main = __decorate([
        ccclass
    ], Main);
    return Main;
}(cc.Component));
exports.default = Main;

cc._RF.pop();