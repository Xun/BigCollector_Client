"use strict";
cc._RF.push(module, '301c82uYRJB0IHWbyIdk4kE', 'CardArrayFlip_FrontCard2D');
// Script/UIScript/RotateAndFlip/CardArrayFlip_FrontCard2D.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CardArrayFlip_FrontCardBase_1 = require("./CardArrayFlip_FrontCardBase");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CardArrayFlip_FrontCard2D = /** @class */ (function (_super) {
    __extends(CardArrayFlip_FrontCard2D, _super);
    function CardArrayFlip_FrontCard2D() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CardArrayFlip_FrontCard2D.prototype.flipToFront = function () {
        var _this = this;
        this.node.is3DNode = true;
        return new Promise(function (res) {
            var tween = cc.tween, duration = 1, half = duration / 2;
            tween(_this.node)
                .to(duration, { scale: 1.1 })
                .start();
            tween(_this.main)
                .parallel(tween().to(half, { scaleX: 0 }, { easing: 'quadIn' }), tween().to(half, { eulerAngles: cc.v3(_this.main.x, -15, _this.main.z) }, { easing: 'quadOut' }))
                .call(function () {
                _this.front.active = true;
                _this.back.active = false;
            })
                .parallel(tween().to(half, { scaleX: -1 }, { easing: 'quadOut' }), tween().to(half, { eulerAngles: cc.v3(_this.main.x, 0, _this.main.z) }, { easing: 'quadIn' }))
                .call(res)
                .start();
        });
    };
    CardArrayFlip_FrontCard2D.prototype.flipToBack = function () {
        var _this = this;
        return new Promise(function (res) {
            var tween = cc.tween, duration = 1, half = duration / 2;
            tween(_this.node)
                .to(duration, { scale: 0.8 })
                .start();
            tween(_this.main)
                .parallel(tween().to(half, { scaleX: 0 }, { easing: 'quadIn' }), tween().to(half, { eulerAngles: cc.v3(_this.main.x, 15, _this.main.z) }, { easing: 'quadOut' }))
                .call(function () {
                _this.front.active = false;
                _this.back.active = true;
            })
                .parallel(tween().to(half, { scaleX: 1 }, { easing: 'quadOut' }), tween().to(half, { eulerAngles: cc.v3(_this.main.x, 0, _this.main.z) }, { easing: 'quadIn' }))
                .call(res)
                .start();
        });
    };
    CardArrayFlip_FrontCard2D = __decorate([
        ccclass
    ], CardArrayFlip_FrontCard2D);
    return CardArrayFlip_FrontCard2D;
}(CardArrayFlip_FrontCardBase_1.default));
exports.default = CardArrayFlip_FrontCard2D;

cc._RF.pop();