import * as $protobuf from "protobufjs";
/** Properties of an InvokeBegin. */
export interface IInvokeBegin {

    /** InvokeBegin invokeId */
    invokeId?: (number|null);

    /** InvokeBegin method */
    method?: (string|null);
}

/** Represents an InvokeBegin. */
export class InvokeBegin implements IInvokeBegin {

    /**
     * Constructs a new InvokeBegin.
     * @param [properties] Properties to set
     */
    constructor(properties?: IInvokeBegin);

    /** InvokeBegin invokeId. */
    public invokeId: number;

    /** InvokeBegin method. */
    public method: string;

    /**
     * Creates a new InvokeBegin instance using the specified properties.
     * @param [properties] Properties to set
     * @returns InvokeBegin instance
     */
    public static create(properties?: IInvokeBegin): InvokeBegin;

    /**
     * Encodes the specified InvokeBegin message. Does not implicitly {@link InvokeBegin.verify|verify} messages.
     * @param message InvokeBegin message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IInvokeBegin, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified InvokeBegin message, length delimited. Does not implicitly {@link InvokeBegin.verify|verify} messages.
     * @param message InvokeBegin message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IInvokeBegin, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an InvokeBegin message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns InvokeBegin
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): InvokeBegin;

    /**
     * Decodes an InvokeBegin message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns InvokeBegin
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): InvokeBegin;

    /**
     * Verifies an InvokeBegin message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an InvokeBegin message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns InvokeBegin
     */
    public static fromObject(object: { [k: string]: any }): InvokeBegin;

    /**
     * Creates a plain object from an InvokeBegin message. Also converts values to other types if specified.
     * @param message InvokeBegin
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: InvokeBegin, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this InvokeBegin to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of an InvokeEnd. */
export interface IInvokeEnd {

    /** InvokeEnd invokeId */
    invokeId?: (number|null);

    /** InvokeEnd status */
    status?: (number|null);

    /** InvokeEnd error */
    error?: (string|null);
}

/** Represents an InvokeEnd. */
export class InvokeEnd implements IInvokeEnd {

    /**
     * Constructs a new InvokeEnd.
     * @param [properties] Properties to set
     */
    constructor(properties?: IInvokeEnd);

    /** InvokeEnd invokeId. */
    public invokeId: number;

    /** InvokeEnd status. */
    public status: number;

    /** InvokeEnd error. */
    public error: string;

    /**
     * Creates a new InvokeEnd instance using the specified properties.
     * @param [properties] Properties to set
     * @returns InvokeEnd instance
     */
    public static create(properties?: IInvokeEnd): InvokeEnd;

    /**
     * Encodes the specified InvokeEnd message. Does not implicitly {@link InvokeEnd.verify|verify} messages.
     * @param message InvokeEnd message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IInvokeEnd, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified InvokeEnd message, length delimited. Does not implicitly {@link InvokeEnd.verify|verify} messages.
     * @param message InvokeEnd message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IInvokeEnd, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an InvokeEnd message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns InvokeEnd
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): InvokeEnd;

    /**
     * Decodes an InvokeEnd message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns InvokeEnd
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): InvokeEnd;

    /**
     * Verifies an InvokeEnd message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an InvokeEnd message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns InvokeEnd
     */
    public static fromObject(object: { [k: string]: any }): InvokeEnd;

    /**
     * Creates a plain object from an InvokeEnd message. Also converts values to other types if specified.
     * @param message InvokeEnd
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: InvokeEnd, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this InvokeEnd to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a CloseConnection. */
export interface ICloseConnection {

    /** CloseConnection code */
    code?: (number|null);

    /** CloseConnection reason */
    reason?: (string|null);

    /** CloseConnection allowReconnect */
    allowReconnect?: (boolean|null);
}

/** Represents a CloseConnection. */
export class CloseConnection implements ICloseConnection {

    /**
     * Constructs a new CloseConnection.
     * @param [properties] Properties to set
     */
    constructor(properties?: ICloseConnection);

    /** CloseConnection code. */
    public code: number;

    /** CloseConnection reason. */
    public reason: string;

    /** CloseConnection allowReconnect. */
    public allowReconnect: boolean;

    /**
     * Creates a new CloseConnection instance using the specified properties.
     * @param [properties] Properties to set
     * @returns CloseConnection instance
     */
    public static create(properties?: ICloseConnection): CloseConnection;

    /**
     * Encodes the specified CloseConnection message. Does not implicitly {@link CloseConnection.verify|verify} messages.
     * @param message CloseConnection message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: ICloseConnection, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified CloseConnection message, length delimited. Does not implicitly {@link CloseConnection.verify|verify} messages.
     * @param message CloseConnection message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: ICloseConnection, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a CloseConnection message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns CloseConnection
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): CloseConnection;

    /**
     * Decodes a CloseConnection message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns CloseConnection
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): CloseConnection;

    /**
     * Verifies a CloseConnection message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a CloseConnection message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns CloseConnection
     */
    public static fromObject(object: { [k: string]: any }): CloseConnection;

    /**
     * Creates a plain object from a CloseConnection message. Also converts values to other types if specified.
     * @param message CloseConnection
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: CloseConnection, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this CloseConnection to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of an Envelope. */
export interface IEnvelope {

    /** Envelope header */
    header?: (Envelope.IHeader|null);

    /** Envelope payload */
    payload?: (Uint8Array|null);
}

/** Represents an Envelope. */
export class Envelope implements IEnvelope {

    /**
     * Constructs a new Envelope.
     * @param [properties] Properties to set
     */
    constructor(properties?: IEnvelope);

    /** Envelope header. */
    public header?: (Envelope.IHeader|null);

    /** Envelope payload. */
    public payload: Uint8Array;

    /**
     * Creates a new Envelope instance using the specified properties.
     * @param [properties] Properties to set
     * @returns Envelope instance
     */
    public static create(properties?: IEnvelope): Envelope;

    /**
     * Encodes the specified Envelope message. Does not implicitly {@link Envelope.verify|verify} messages.
     * @param message Envelope message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IEnvelope, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified Envelope message, length delimited. Does not implicitly {@link Envelope.verify|verify} messages.
     * @param message Envelope message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IEnvelope, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes an Envelope message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns Envelope
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Envelope;

    /**
     * Decodes an Envelope message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns Envelope
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Envelope;

    /**
     * Verifies an Envelope message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates an Envelope message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns Envelope
     */
    public static fromObject(object: { [k: string]: any }): Envelope;

    /**
     * Creates a plain object from an Envelope message. Also converts values to other types if specified.
     * @param message Envelope
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: Envelope, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this Envelope to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

export namespace Envelope {

    /** Properties of a Header. */
    interface IHeader {

        /** Header invokeBegin */
        invokeBegin?: (IInvokeBegin|null);

        /** Header invokeEnd */
        invokeEnd?: (IInvokeEnd|null);

        /** Header closeConnection */
        closeConnection?: (ICloseConnection|null);

        /** Header event */
        event?: (string|null);
    }

    /** Represents a Header. */
    class Header implements IHeader {

        /**
         * Constructs a new Header.
         * @param [properties] Properties to set
         */
        constructor(properties?: Envelope.IHeader);

        /** Header invokeBegin. */
        public invokeBegin?: (IInvokeBegin|null);

        /** Header invokeEnd. */
        public invokeEnd?: (IInvokeEnd|null);

        /** Header closeConnection. */
        public closeConnection?: (ICloseConnection|null);

        /** Header event. */
        public event?: (string|null);

        /** Header kind. */
        public kind?: ("invokeBegin"|"invokeEnd"|"closeConnection"|"event");

        /**
         * Creates a new Header instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Header instance
         */
        public static create(properties?: Envelope.IHeader): Envelope.Header;

        /**
         * Encodes the specified Header message. Does not implicitly {@link Envelope.Header.verify|verify} messages.
         * @param message Header message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: Envelope.IHeader, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Header message, length delimited. Does not implicitly {@link Envelope.Header.verify|verify} messages.
         * @param message Header message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: Envelope.IHeader, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Header message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Header
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): Envelope.Header;

        /**
         * Decodes a Header message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Header
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): Envelope.Header;

        /**
         * Verifies a Header message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Header message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Header
         */
        public static fromObject(object: { [k: string]: any }): Envelope.Header;

        /**
         * Creates a plain object from a Header message. Also converts values to other types if specified.
         * @param message Header
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: Envelope.Header, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Header to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }
}

/** Represents a GameServer */
export class GameServer extends $protobuf.rpc.Service {

    /**
     * Constructs a new GameServer service.
     * @param rpcImpl RPC implementation
     * @param [requestDelimited=false] Whether requests are length-delimited
     * @param [responseDelimited=false] Whether responses are length-delimited
     */
    constructor(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean);

    /**
     * Creates new GameServer service using the specified rpc implementation.
     * @param rpcImpl RPC implementation
     * @param [requestDelimited=false] Whether requests are length-delimited
     * @param [responseDelimited=false] Whether responses are length-delimited
     * @returns RPC service. Useful where requests and/or responses are streamed.
     */
    public static create(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean): GameServer;

    /**
     * Calls Login.
     * @param request LoginRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and LoginReply
     */
    public login(request: ILoginRequest, callback: GameServer.LoginCallback): void;

    /**
     * Calls Login.
     * @param request LoginRequest message or plain object
     * @returns Promise
     */
    public login(request: ILoginRequest): Promise<LoginReply>;

    /**
     * Calls BuyProduce.
     * @param request BuyProduceRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and BuyProduceReply
     */
    public buyProduce(request: IBuyProduceRequest, callback: GameServer.BuyProduceCallback): void;

    /**
     * Calls BuyProduce.
     * @param request BuyProduceRequest message or plain object
     * @returns Promise
     */
    public buyProduce(request: IBuyProduceRequest): Promise<BuyProduceReply>;

    /**
     * Calls CombineProduce.
     * @param request CombineProduceRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and CombineProduceReply
     */
    public combineProduce(request: ICombineProduceRequest, callback: GameServer.CombineProduceCallback): void;

    /**
     * Calls CombineProduce.
     * @param request CombineProduceRequest message or plain object
     * @returns Promise
     */
    public combineProduce(request: ICombineProduceRequest): Promise<CombineProduceReply>;

    /**
     * Calls RefreshWorkBench.
     * @param request RefreshWorkBenchRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and RefreshWorkBenchReply
     */
    public refreshWorkBench(request: IRefreshWorkBenchRequest, callback: GameServer.RefreshWorkBenchCallback): void;

    /**
     * Calls RefreshWorkBench.
     * @param request RefreshWorkBenchRequest message or plain object
     * @returns Promise
     */
    public refreshWorkBench(request: IRefreshWorkBenchRequest): Promise<RefreshWorkBenchReply>;
}

export namespace GameServer {

    /**
     * Callback as used by {@link GameServer#login}.
     * @param error Error, if any
     * @param [response] LoginReply
     */
    type LoginCallback = (error: (Error|null), response?: LoginReply) => void;

    /**
     * Callback as used by {@link GameServer#buyProduce}.
     * @param error Error, if any
     * @param [response] BuyProduceReply
     */
    type BuyProduceCallback = (error: (Error|null), response?: BuyProduceReply) => void;

    /**
     * Callback as used by {@link GameServer#combineProduce}.
     * @param error Error, if any
     * @param [response] CombineProduceReply
     */
    type CombineProduceCallback = (error: (Error|null), response?: CombineProduceReply) => void;

    /**
     * Callback as used by {@link GameServer#refreshWorkBench}.
     * @param error Error, if any
     * @param [response] RefreshWorkBenchReply
     */
    type RefreshWorkBenchCallback = (error: (Error|null), response?: RefreshWorkBenchReply) => void;
}

/** Properties of a LoginRequest. */
export interface ILoginRequest {

    /** LoginRequest accessToken */
    accessToken?: (string|null);
}

/** Represents a LoginRequest. */
export class LoginRequest implements ILoginRequest {

    /**
     * Constructs a new LoginRequest.
     * @param [properties] Properties to set
     */
    constructor(properties?: ILoginRequest);

    /** LoginRequest accessToken. */
    public accessToken: string;

    /**
     * Creates a new LoginRequest instance using the specified properties.
     * @param [properties] Properties to set
     * @returns LoginRequest instance
     */
    public static create(properties?: ILoginRequest): LoginRequest;

    /**
     * Encodes the specified LoginRequest message. Does not implicitly {@link LoginRequest.verify|verify} messages.
     * @param message LoginRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: ILoginRequest, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified LoginRequest message, length delimited. Does not implicitly {@link LoginRequest.verify|verify} messages.
     * @param message LoginRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: ILoginRequest, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a LoginRequest message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns LoginRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): LoginRequest;

    /**
     * Decodes a LoginRequest message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns LoginRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): LoginRequest;

    /**
     * Verifies a LoginRequest message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a LoginRequest message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns LoginRequest
     */
    public static fromObject(object: { [k: string]: any }): LoginRequest;

    /**
     * Creates a plain object from a LoginRequest message. Also converts values to other types if specified.
     * @param message LoginRequest
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: LoginRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this LoginRequest to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a LoginReply. */
export interface ILoginReply {

    /** LoginReply userInfo */
    userInfo?: (IUserInfo|null);

    /** LoginReply serverTime */
    serverTime?: (number|null);

    /** LoginReply coin */
    coin?: (number|Long|null);

    /** LoginReply diamond */
    diamond?: (number|Long|null);

    /** LoginReply workbenchItems */
    workbenchItems?: (IWorkBenchItemInfo[]|null);
}

/** Represents a LoginReply. */
export class LoginReply implements ILoginReply {

    /**
     * Constructs a new LoginReply.
     * @param [properties] Properties to set
     */
    constructor(properties?: ILoginReply);

    /** LoginReply userInfo. */
    public userInfo?: (IUserInfo|null);

    /** LoginReply serverTime. */
    public serverTime: number;

    /** LoginReply coin. */
    public coin: (number|Long);

    /** LoginReply diamond. */
    public diamond: (number|Long);

    /** LoginReply workbenchItems. */
    public workbenchItems: IWorkBenchItemInfo[];

    /**
     * Creates a new LoginReply instance using the specified properties.
     * @param [properties] Properties to set
     * @returns LoginReply instance
     */
    public static create(properties?: ILoginReply): LoginReply;

    /**
     * Encodes the specified LoginReply message. Does not implicitly {@link LoginReply.verify|verify} messages.
     * @param message LoginReply message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: ILoginReply, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified LoginReply message, length delimited. Does not implicitly {@link LoginReply.verify|verify} messages.
     * @param message LoginReply message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: ILoginReply, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a LoginReply message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns LoginReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): LoginReply;

    /**
     * Decodes a LoginReply message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns LoginReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): LoginReply;

    /**
     * Verifies a LoginReply message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a LoginReply message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns LoginReply
     */
    public static fromObject(object: { [k: string]: any }): LoginReply;

    /**
     * Creates a plain object from a LoginReply message. Also converts values to other types if specified.
     * @param message LoginReply
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: LoginReply, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this LoginReply to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a BuyProduceRequest. */
export interface IBuyProduceRequest {

    /** BuyProduceRequest produceLevel */
    produceLevel?: (number|null);
}

/** Represents a BuyProduceRequest. */
export class BuyProduceRequest implements IBuyProduceRequest {

    /**
     * Constructs a new BuyProduceRequest.
     * @param [properties] Properties to set
     */
    constructor(properties?: IBuyProduceRequest);

    /** BuyProduceRequest produceLevel. */
    public produceLevel: number;

    /**
     * Creates a new BuyProduceRequest instance using the specified properties.
     * @param [properties] Properties to set
     * @returns BuyProduceRequest instance
     */
    public static create(properties?: IBuyProduceRequest): BuyProduceRequest;

    /**
     * Encodes the specified BuyProduceRequest message. Does not implicitly {@link BuyProduceRequest.verify|verify} messages.
     * @param message BuyProduceRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IBuyProduceRequest, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified BuyProduceRequest message, length delimited. Does not implicitly {@link BuyProduceRequest.verify|verify} messages.
     * @param message BuyProduceRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IBuyProduceRequest, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a BuyProduceRequest message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns BuyProduceRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): BuyProduceRequest;

    /**
     * Decodes a BuyProduceRequest message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns BuyProduceRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): BuyProduceRequest;

    /**
     * Verifies a BuyProduceRequest message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a BuyProduceRequest message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns BuyProduceRequest
     */
    public static fromObject(object: { [k: string]: any }): BuyProduceRequest;

    /**
     * Creates a plain object from a BuyProduceRequest message. Also converts values to other types if specified.
     * @param message BuyProduceRequest
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: BuyProduceRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this BuyProduceRequest to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a BuyProduceReply. */
export interface IBuyProduceReply {

    /** BuyProduceReply errcode */
    errcode?: (number|null);

    /** BuyProduceReply produceLevel */
    produceLevel?: (number|null);

    /** BuyProduceReply position */
    position?: (number|null);
}

/** Represents a BuyProduceReply. */
export class BuyProduceReply implements IBuyProduceReply {

    /**
     * Constructs a new BuyProduceReply.
     * @param [properties] Properties to set
     */
    constructor(properties?: IBuyProduceReply);

    /** BuyProduceReply errcode. */
    public errcode: number;

    /** BuyProduceReply produceLevel. */
    public produceLevel: number;

    /** BuyProduceReply position. */
    public position: number;

    /**
     * Creates a new BuyProduceReply instance using the specified properties.
     * @param [properties] Properties to set
     * @returns BuyProduceReply instance
     */
    public static create(properties?: IBuyProduceReply): BuyProduceReply;

    /**
     * Encodes the specified BuyProduceReply message. Does not implicitly {@link BuyProduceReply.verify|verify} messages.
     * @param message BuyProduceReply message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IBuyProduceReply, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified BuyProduceReply message, length delimited. Does not implicitly {@link BuyProduceReply.verify|verify} messages.
     * @param message BuyProduceReply message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IBuyProduceReply, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a BuyProduceReply message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns BuyProduceReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): BuyProduceReply;

    /**
     * Decodes a BuyProduceReply message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns BuyProduceReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): BuyProduceReply;

    /**
     * Verifies a BuyProduceReply message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a BuyProduceReply message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns BuyProduceReply
     */
    public static fromObject(object: { [k: string]: any }): BuyProduceReply;

    /**
     * Creates a plain object from a BuyProduceReply message. Also converts values to other types if specified.
     * @param message BuyProduceReply
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: BuyProduceReply, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this BuyProduceReply to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a CombineProduceRequest. */
export interface ICombineProduceRequest {

    /** CombineProduceRequest positionBase */
    positionBase?: (number|null);

    /** CombineProduceRequest positionTarget */
    positionTarget?: (number|null);
}

/** Represents a CombineProduceRequest. */
export class CombineProduceRequest implements ICombineProduceRequest {

    /**
     * Constructs a new CombineProduceRequest.
     * @param [properties] Properties to set
     */
    constructor(properties?: ICombineProduceRequest);

    /** CombineProduceRequest positionBase. */
    public positionBase: number;

    /** CombineProduceRequest positionTarget. */
    public positionTarget: number;

    /**
     * Creates a new CombineProduceRequest instance using the specified properties.
     * @param [properties] Properties to set
     * @returns CombineProduceRequest instance
     */
    public static create(properties?: ICombineProduceRequest): CombineProduceRequest;

    /**
     * Encodes the specified CombineProduceRequest message. Does not implicitly {@link CombineProduceRequest.verify|verify} messages.
     * @param message CombineProduceRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: ICombineProduceRequest, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified CombineProduceRequest message, length delimited. Does not implicitly {@link CombineProduceRequest.verify|verify} messages.
     * @param message CombineProduceRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: ICombineProduceRequest, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a CombineProduceRequest message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns CombineProduceRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): CombineProduceRequest;

    /**
     * Decodes a CombineProduceRequest message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns CombineProduceRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): CombineProduceRequest;

    /**
     * Verifies a CombineProduceRequest message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a CombineProduceRequest message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns CombineProduceRequest
     */
    public static fromObject(object: { [k: string]: any }): CombineProduceRequest;

    /**
     * Creates a plain object from a CombineProduceRequest message. Also converts values to other types if specified.
     * @param message CombineProduceRequest
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: CombineProduceRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this CombineProduceRequest to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a CombineProduceReply. */
export interface ICombineProduceReply {

    /** CombineProduceReply errcode */
    errcode?: (number|null);

    /** CombineProduceReply produceLevel */
    produceLevel?: (number|null);

    /** CombineProduceReply positionNew */
    positionNew?: (number|null);
}

/** Represents a CombineProduceReply. */
export class CombineProduceReply implements ICombineProduceReply {

    /**
     * Constructs a new CombineProduceReply.
     * @param [properties] Properties to set
     */
    constructor(properties?: ICombineProduceReply);

    /** CombineProduceReply errcode. */
    public errcode: number;

    /** CombineProduceReply produceLevel. */
    public produceLevel: number;

    /** CombineProduceReply positionNew. */
    public positionNew: number;

    /**
     * Creates a new CombineProduceReply instance using the specified properties.
     * @param [properties] Properties to set
     * @returns CombineProduceReply instance
     */
    public static create(properties?: ICombineProduceReply): CombineProduceReply;

    /**
     * Encodes the specified CombineProduceReply message. Does not implicitly {@link CombineProduceReply.verify|verify} messages.
     * @param message CombineProduceReply message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: ICombineProduceReply, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified CombineProduceReply message, length delimited. Does not implicitly {@link CombineProduceReply.verify|verify} messages.
     * @param message CombineProduceReply message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: ICombineProduceReply, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a CombineProduceReply message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns CombineProduceReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): CombineProduceReply;

    /**
     * Decodes a CombineProduceReply message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns CombineProduceReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): CombineProduceReply;

    /**
     * Verifies a CombineProduceReply message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a CombineProduceReply message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns CombineProduceReply
     */
    public static fromObject(object: { [k: string]: any }): CombineProduceReply;

    /**
     * Creates a plain object from a CombineProduceReply message. Also converts values to other types if specified.
     * @param message CombineProduceReply
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: CombineProduceReply, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this CombineProduceReply to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a RefreshWorkBenchRequest. */
export interface IRefreshWorkBenchRequest {
}

/** Represents a RefreshWorkBenchRequest. */
export class RefreshWorkBenchRequest implements IRefreshWorkBenchRequest {

    /**
     * Constructs a new RefreshWorkBenchRequest.
     * @param [properties] Properties to set
     */
    constructor(properties?: IRefreshWorkBenchRequest);

    /**
     * Creates a new RefreshWorkBenchRequest instance using the specified properties.
     * @param [properties] Properties to set
     * @returns RefreshWorkBenchRequest instance
     */
    public static create(properties?: IRefreshWorkBenchRequest): RefreshWorkBenchRequest;

    /**
     * Encodes the specified RefreshWorkBenchRequest message. Does not implicitly {@link RefreshWorkBenchRequest.verify|verify} messages.
     * @param message RefreshWorkBenchRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IRefreshWorkBenchRequest, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified RefreshWorkBenchRequest message, length delimited. Does not implicitly {@link RefreshWorkBenchRequest.verify|verify} messages.
     * @param message RefreshWorkBenchRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IRefreshWorkBenchRequest, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a RefreshWorkBenchRequest message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns RefreshWorkBenchRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): RefreshWorkBenchRequest;

    /**
     * Decodes a RefreshWorkBenchRequest message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns RefreshWorkBenchRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): RefreshWorkBenchRequest;

    /**
     * Verifies a RefreshWorkBenchRequest message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a RefreshWorkBenchRequest message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns RefreshWorkBenchRequest
     */
    public static fromObject(object: { [k: string]: any }): RefreshWorkBenchRequest;

    /**
     * Creates a plain object from a RefreshWorkBenchRequest message. Also converts values to other types if specified.
     * @param message RefreshWorkBenchRequest
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: RefreshWorkBenchRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this RefreshWorkBenchRequest to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a RefreshWorkBenchReply. */
export interface IRefreshWorkBenchReply {

    /** RefreshWorkBenchReply workbenchInfo */
    workbenchInfo?: (number[]|null);
}

/** Represents a RefreshWorkBenchReply. */
export class RefreshWorkBenchReply implements IRefreshWorkBenchReply {

    /**
     * Constructs a new RefreshWorkBenchReply.
     * @param [properties] Properties to set
     */
    constructor(properties?: IRefreshWorkBenchReply);

    /** RefreshWorkBenchReply workbenchInfo. */
    public workbenchInfo: number[];

    /**
     * Creates a new RefreshWorkBenchReply instance using the specified properties.
     * @param [properties] Properties to set
     * @returns RefreshWorkBenchReply instance
     */
    public static create(properties?: IRefreshWorkBenchReply): RefreshWorkBenchReply;

    /**
     * Encodes the specified RefreshWorkBenchReply message. Does not implicitly {@link RefreshWorkBenchReply.verify|verify} messages.
     * @param message RefreshWorkBenchReply message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IRefreshWorkBenchReply, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified RefreshWorkBenchReply message, length delimited. Does not implicitly {@link RefreshWorkBenchReply.verify|verify} messages.
     * @param message RefreshWorkBenchReply message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IRefreshWorkBenchReply, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a RefreshWorkBenchReply message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns RefreshWorkBenchReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): RefreshWorkBenchReply;

    /**
     * Decodes a RefreshWorkBenchReply message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns RefreshWorkBenchReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): RefreshWorkBenchReply;

    /**
     * Verifies a RefreshWorkBenchReply message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a RefreshWorkBenchReply message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns RefreshWorkBenchReply
     */
    public static fromObject(object: { [k: string]: any }): RefreshWorkBenchReply;

    /**
     * Creates a plain object from a RefreshWorkBenchReply message. Also converts values to other types if specified.
     * @param message RefreshWorkBenchReply
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: RefreshWorkBenchReply, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this RefreshWorkBenchReply to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a DecimalValue. */
export interface IDecimalValue {

    /** DecimalValue uints */
    uints?: (number|Long|null);

    /** DecimalValue nanos */
    nanos?: (number|null);
}

/** Represents a DecimalValue. */
export class DecimalValue implements IDecimalValue {

    /**
     * Constructs a new DecimalValue.
     * @param [properties] Properties to set
     */
    constructor(properties?: IDecimalValue);

    /** DecimalValue uints. */
    public uints: (number|Long);

    /** DecimalValue nanos. */
    public nanos: number;

    /**
     * Creates a new DecimalValue instance using the specified properties.
     * @param [properties] Properties to set
     * @returns DecimalValue instance
     */
    public static create(properties?: IDecimalValue): DecimalValue;

    /**
     * Encodes the specified DecimalValue message. Does not implicitly {@link DecimalValue.verify|verify} messages.
     * @param message DecimalValue message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IDecimalValue, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified DecimalValue message, length delimited. Does not implicitly {@link DecimalValue.verify|verify} messages.
     * @param message DecimalValue message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IDecimalValue, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a DecimalValue message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns DecimalValue
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): DecimalValue;

    /**
     * Decodes a DecimalValue message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns DecimalValue
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): DecimalValue;

    /**
     * Verifies a DecimalValue message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a DecimalValue message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns DecimalValue
     */
    public static fromObject(object: { [k: string]: any }): DecimalValue;

    /**
     * Creates a plain object from a DecimalValue message. Also converts values to other types if specified.
     * @param message DecimalValue
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: DecimalValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this DecimalValue to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a UserInfo. */
export interface IUserInfo {

    /** UserInfo account */
    account?: (string|null);

    /** UserInfo nickname */
    nickname?: (string|null);

    /** UserInfo avatarUrl */
    avatarUrl?: (string|null);

    /** UserInfo money */
    money?: (IDecimalValue|null);
}

/** Represents a UserInfo. */
export class UserInfo implements IUserInfo {

    /**
     * Constructs a new UserInfo.
     * @param [properties] Properties to set
     */
    constructor(properties?: IUserInfo);

    /** UserInfo account. */
    public account: string;

    /** UserInfo nickname. */
    public nickname: string;

    /** UserInfo avatarUrl. */
    public avatarUrl: string;

    /** UserInfo money. */
    public money?: (IDecimalValue|null);

    /**
     * Creates a new UserInfo instance using the specified properties.
     * @param [properties] Properties to set
     * @returns UserInfo instance
     */
    public static create(properties?: IUserInfo): UserInfo;

    /**
     * Encodes the specified UserInfo message. Does not implicitly {@link UserInfo.verify|verify} messages.
     * @param message UserInfo message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IUserInfo, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified UserInfo message, length delimited. Does not implicitly {@link UserInfo.verify|verify} messages.
     * @param message UserInfo message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IUserInfo, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a UserInfo message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns UserInfo
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): UserInfo;

    /**
     * Decodes a UserInfo message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns UserInfo
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): UserInfo;

    /**
     * Verifies a UserInfo message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a UserInfo message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns UserInfo
     */
    public static fromObject(object: { [k: string]: any }): UserInfo;

    /**
     * Creates a plain object from a UserInfo message. Also converts values to other types if specified.
     * @param message UserInfo
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: UserInfo, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this UserInfo to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a UserMoney. */
export interface IUserMoney {

    /** UserMoney coin */
    coin?: (number|null);

    /** UserMoney diamond */
    diamond?: (number|null);

    /** UserMoney money */
    money?: (number|null);
}

/** Represents a UserMoney. */
export class UserMoney implements IUserMoney {

    /**
     * Constructs a new UserMoney.
     * @param [properties] Properties to set
     */
    constructor(properties?: IUserMoney);

    /** UserMoney coin. */
    public coin: number;

    /** UserMoney diamond. */
    public diamond: number;

    /** UserMoney money. */
    public money: number;

    /**
     * Creates a new UserMoney instance using the specified properties.
     * @param [properties] Properties to set
     * @returns UserMoney instance
     */
    public static create(properties?: IUserMoney): UserMoney;

    /**
     * Encodes the specified UserMoney message. Does not implicitly {@link UserMoney.verify|verify} messages.
     * @param message UserMoney message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IUserMoney, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified UserMoney message, length delimited. Does not implicitly {@link UserMoney.verify|verify} messages.
     * @param message UserMoney message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IUserMoney, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a UserMoney message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns UserMoney
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): UserMoney;

    /**
     * Decodes a UserMoney message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns UserMoney
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): UserMoney;

    /**
     * Verifies a UserMoney message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a UserMoney message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns UserMoney
     */
    public static fromObject(object: { [k: string]: any }): UserMoney;

    /**
     * Creates a plain object from a UserMoney message. Also converts values to other types if specified.
     * @param message UserMoney
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: UserMoney, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this UserMoney to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a WorkBenchItemInfo. */
export interface IWorkBenchItemInfo {

    /** WorkBenchItemInfo level */
    level?: (number|null);

    /** WorkBenchItemInfo coinCost */
    coinCost?: (number|Long|null);

    /** WorkBenchItemInfo coinIncome */
    coinIncome?: (number|Long|null);
}

/** Represents a WorkBenchItemInfo. */
export class WorkBenchItemInfo implements IWorkBenchItemInfo {

    /**
     * Constructs a new WorkBenchItemInfo.
     * @param [properties] Properties to set
     */
    constructor(properties?: IWorkBenchItemInfo);

    /** WorkBenchItemInfo level. */
    public level: number;

    /** WorkBenchItemInfo coinCost. */
    public coinCost: (number|Long);

    /** WorkBenchItemInfo coinIncome. */
    public coinIncome: (number|Long);

    /**
     * Creates a new WorkBenchItemInfo instance using the specified properties.
     * @param [properties] Properties to set
     * @returns WorkBenchItemInfo instance
     */
    public static create(properties?: IWorkBenchItemInfo): WorkBenchItemInfo;

    /**
     * Encodes the specified WorkBenchItemInfo message. Does not implicitly {@link WorkBenchItemInfo.verify|verify} messages.
     * @param message WorkBenchItemInfo message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IWorkBenchItemInfo, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified WorkBenchItemInfo message, length delimited. Does not implicitly {@link WorkBenchItemInfo.verify|verify} messages.
     * @param message WorkBenchItemInfo message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IWorkBenchItemInfo, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a WorkBenchItemInfo message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns WorkBenchItemInfo
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): WorkBenchItemInfo;

    /**
     * Decodes a WorkBenchItemInfo message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns WorkBenchItemInfo
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): WorkBenchItemInfo;

    /**
     * Verifies a WorkBenchItemInfo message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a WorkBenchItemInfo message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns WorkBenchItemInfo
     */
    public static fromObject(object: { [k: string]: any }): WorkBenchItemInfo;

    /**
     * Creates a plain object from a WorkBenchItemInfo message. Also converts values to other types if specified.
     * @param message WorkBenchItemInfo
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: WorkBenchItemInfo, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this WorkBenchItemInfo to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}
