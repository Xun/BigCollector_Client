
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIBuyTip_Auto extends cc.Component {
	@property(cc.Label)
	BuyTipLab: cc.Label = null;
	@property(cc.Sprite)
	BuyMoneyTypeSp: cc.Sprite = null;
	@property(cc.Label)
	BuyName: cc.Label = null;
	@property(ButtonPlus)
	BtnSure: ButtonPlus = null;
	@property(cc.Sprite)
	BuyIconTypeSp: cc.Sprite = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
 
}