
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIShare_Auto extends cc.Component {
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
	@property(ButtonPlus)
	WechatBtn: ButtonPlus = null;
	@property(ButtonPlus)
	PengYouQuanBtn: ButtonPlus = null;
	@property(ButtonPlus)
	PhotoBtn: ButtonPlus = null;
	@property(cc.Node)
	contentNode: cc.Node = null;
	@property(cc.Sprite)
	HeadSp1: cc.Sprite = null;
	@property(cc.Label)
	NameLab1: cc.Label = null;
	@property(cc.Sprite)
	HeadSp2: cc.Sprite = null;
	@property(cc.Label)
	NameLab2: cc.Label = null;
 
}