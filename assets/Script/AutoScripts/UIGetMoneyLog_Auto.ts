
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIGetMoneyLog_Auto extends cc.Component {
	@property(cc.Node)
	ContentViewNode: cc.Node = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
 
}