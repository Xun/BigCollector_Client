
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIFriendGiveDetail_Auto extends cc.Component {
	@property(cc.Node)
	FriendSeeContentNode: cc.Node = null;
	@property(cc.Node)
	CoinContentViewNode: cc.Node = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
 
}