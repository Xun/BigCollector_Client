
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIInviteGetInfo_Auto extends cc.Component {
	@property(ButtonPlus)
	SetInfoBtn: ButtonPlus = null;
	@property(ButtonPlus)
	CopyWechatBtn: ButtonPlus = null;
	@property(ButtonPlus)
	CopyQQBtn: ButtonPlus = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
	@property(cc.Sprite)
	HeadSP: cc.Sprite = null;
	@property(cc.Label)
	NameLab: cc.Label = null;
 
}