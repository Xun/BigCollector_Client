
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIRecoverTip_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnSure: ButtonPlus = null;
	@property(cc.Sprite)
	GetIconTypeSp: cc.Sprite = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(cc.Sprite)
	BuyMoneyTypeSp: cc.Sprite = null;
	@property(cc.Label)
	GetNumLab: cc.Label = null;
 
}