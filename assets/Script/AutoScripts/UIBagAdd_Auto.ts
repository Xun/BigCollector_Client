
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIBagAdd_Auto extends cc.Component {
	@property(ButtonPlus)
	GetBtn: ButtonPlus = null;
	@property(cc.Label)
	NeedMoneyLab: cc.Label = null;
	@property(ButtonPlus)
	WatchBtn: ButtonPlus = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
 
}