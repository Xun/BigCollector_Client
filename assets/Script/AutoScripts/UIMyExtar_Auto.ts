
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIMyExtar_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(cc.Label)
	TodayExtarMoneyNumLab: cc.Label = null;
	@property(cc.Label)
	TodayExtarAddUserNumLab: cc.Label = null;
	@property(cc.Label)
	MonthExtarMoneyNumLab: cc.Label = null;
	@property(cc.Label)
	MonthExtarAddUserNumLab: cc.Label = null;
	@property(cc.Label)
	TotalExtarMoneyNumLab: cc.Label = null;
	@property(cc.Label)
	TotalExtarAddUserNumLab: cc.Label = null;
	@property(cc.Label)
	JieDuanNumLab: cc.Label = null;
	@property(cc.Label)
	JDCurExtarMoneyNumLab: cc.Label = null;
	@property(cc.Label)
	JDTotalExtarMoneyNumLab: cc.Label = null;
	@property(cc.Label)
	CompleteMoneyLab: cc.Label = null;
	@property(cc.ProgressBar)
	JDPS: cc.ProgressBar = null;
	@property(cc.Label)
	MultLab: cc.Label = null;
 
}