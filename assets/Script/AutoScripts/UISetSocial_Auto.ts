
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UISetSocial_Auto extends cc.Component {
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
	@property(ButtonPlus)
	SureBtn: ButtonPlus = null;
	@property(cc.EditBox)
	Wechat: cc.EditBox = null;
	@property(cc.EditBox)
	QQ: cc.EditBox = null;
 
}