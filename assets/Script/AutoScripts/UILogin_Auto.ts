
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UILogin_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnWechat: ButtonPlus = null;
	@property(cc.Toggle)
	UserAgarn: cc.Toggle = null;
	@property(ButtonPlus)
	BtnXieYi: ButtonPlus = null;
	@property(ButtonPlus)
	BtnZhengCe: ButtonPlus = null;
	@property(cc.EditBox)
	testAccount: cc.EditBox = null;
 
}