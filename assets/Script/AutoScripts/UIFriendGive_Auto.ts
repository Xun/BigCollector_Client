
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIFriendGive_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSerch: ButtonPlus = null;
	@property(ButtonPlus)
	BtnGive: ButtonPlus = null;
	@property(cc.EditBox)
	IDBox: cc.EditBox = null;
	@property(cc.EditBox)
	GiveBox: cc.EditBox = null;
	@property(cc.Sprite)
	HeadSP: cc.Sprite = null;
	@property(cc.Label)
	NameLab: cc.Label = null;
 
}