
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIGetMoneyRule_Auto extends cc.Component {
	@property(cc.Label)
	Tip: cc.Label = null;
	@property(ButtonPlus)
	Sure: ButtonPlus = null;
 
}