
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIDialog_Auto extends cc.Component {
	@property(cc.Label)
	Msg: cc.Label = null;
	@property(ButtonPlus)
	Sure: ButtonPlus = null;
 
}