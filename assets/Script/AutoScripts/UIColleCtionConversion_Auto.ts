
import ButtonPlus from "./../Common/Components/ButtonPlus"

const { ccclass, property } = cc._decorator;
@ccclass
export default class UIColleCtionConversion_Auto extends cc.Component {
	@property(cc.Label)
	Msg: cc.Label = null;
	@property(cc.Label)
	MoneyNameLab: cc.Label = null;
	@property(ButtonPlus)
	BtnSure: ButtonPlus = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(cc.Sprite)
	MoneyIconSp: cc.Sprite = null;

}