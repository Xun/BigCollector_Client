
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UISignIn_Auto extends cc.Component {
	@property(cc.ProgressBar)
	JDPS: cc.ProgressBar = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(cc.Label)
	SignInDayNumLab: cc.Label = null;
 
}