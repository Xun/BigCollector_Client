
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIFriendSee_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(ButtonPlus)
	AddFriendBtn: ButtonPlus = null;
	@property(cc.Node)
	ContentNode: cc.Node = null;
	@property(cc.Node)
	ContentViewNode: cc.Node = null;
	@property(ButtonPlus)
	SeeDetailBtn: ButtonPlus = null;
	@property(ButtonPlus)
	InviteBtn: ButtonPlus = null;
 
}