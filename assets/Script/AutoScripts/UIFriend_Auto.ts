
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIFriend_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(ButtonPlus)
	BtnInvoedFriend: ButtonPlus = null;
	@property(ButtonPlus)
	BtnMyFriend: ButtonPlus = null;
	@property(cc.Label)
	MyFriendNumLab: cc.Label = null;
	@property(cc.Label)
	ProbablyMoneyLab: cc.Label = null;
	@property(ButtonPlus)
	ProbablyDetail: ButtonPlus = null;
	@property(cc.Label)
	TodayGetMoneyLab: cc.Label = null;
	@property(cc.Label)
	TotalGetMoneyLab: cc.Label = null;
	@property(ButtonPlus)
	UpChannel: ButtonPlus = null;
	@property(ButtonPlus)
	ExtarTip: ButtonPlus = null;
	@property(cc.Label)
	TodayExtarGetMoneyLab: cc.Label = null;
	@property(cc.Label)
	TodayExtarGetMoneyTipsLab: cc.Label = null;
	@property(ButtonPlus)
	Call: ButtonPlus = null;
	@property(cc.Label)
	Name: cc.Label = null;
	@property(cc.Sprite)
	HeadSp: cc.Sprite = null;
	@property(cc.Label)
	TodayAddYushi: cc.Label = null;
	@property(cc.Label)
	TotalYushi: cc.Label = null;
	@property(ButtonPlus)
	BtnGive: ButtonPlus = null;
	@property(ButtonPlus)
	BtnVisit: ButtonPlus = null;
 
}