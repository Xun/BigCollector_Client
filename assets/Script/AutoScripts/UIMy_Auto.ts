
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIMy_Auto extends cc.Component {
	@property(cc.Sprite)
	HeadSp: cc.Sprite = null;
	@property(cc.Label)
	NameLab: cc.Label = null;
	@property(cc.Label)
	IDLab : cc.Label = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(cc.Node)
	MoneyNode: cc.Node = null;
	@property(cc.Label)
	MyMoneyLab: cc.Label = null;
	@property(ButtonPlus)
	BtnGetMoney: ButtonPlus = null;
	@property(ButtonPlus)
	BtnFriend: ButtonPlus = null;
	@property(ButtonPlus)
	BtnVip: ButtonPlus = null;
	@property(ButtonPlus)
	BtnShare: ButtonPlus = null;
	@property(ButtonPlus)
	BtnPlay: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSetting: ButtonPlus = null;
 
}