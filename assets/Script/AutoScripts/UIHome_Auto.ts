
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIHome_Auto extends cc.Component {
	@property(cc.Node)
	accEffect: cc.Node = null;
	@property(ButtonPlus)
	Accelerate: ButtonPlus = null;
	@property(ButtonPlus)
	RecoveryBtn: ButtonPlus = null;
	@property(cc.Node)
	topNode: cc.Node = null;
	@property(cc.Label)
	MakeMoneyTxt: cc.Label = null;
	@property(cc.Label)
	CoinTxt: cc.Label = null;
	@property(cc.Label)
	DiamondTxt: cc.Label = null;
	@property(cc.Label)
	MoneyTxt: cc.Label = null;
	@property(cc.Sprite)
	HeadSp: cc.Sprite = null;
	@property(ButtonPlus)
	RankBtn: ButtonPlus = null;
	@property(ButtonPlus)
	CollectBtn: ButtonPlus = null;
	@property(ButtonPlus)
	BagBtn: ButtonPlus = null;
	@property(ButtonPlus)
	ShopBtn: ButtonPlus = null;
	@property(ButtonPlus)
	DrawBtn: ButtonPlus = null;
	@property(cc.Node)
	WorkContent: cc.Node = null;
	@property(ButtonPlus)
	FriendBtn: ButtonPlus = null;
	@property(ButtonPlus)
	SignInBtn: ButtonPlus = null;
	@property(ButtonPlus)
	BuyBtn: ButtonPlus = null;
	@property(cc.Sprite)
	BtnCakeSp: cc.Sprite = null;
	@property(cc.Label)
	BuyCoinNumTxt: cc.Label = null;
	@property(ButtonPlus)
	MyBtn: ButtonPlus = null;
	@property(ButtonPlus)
	TaskBtn: ButtonPlus = null;
	@property(ButtonPlus)
	CombineAutoBtn: ButtonPlus = null;
	@property(cc.Label)
	CombineAutoTxt: cc.Label = null;
 
}