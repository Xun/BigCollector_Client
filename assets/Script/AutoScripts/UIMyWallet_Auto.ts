
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIMyWallet_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(cc.Label)
	MyMoneyLab: cc.Label = null;
	@property(ButtonPlus)
	BtnTips: ButtonPlus = null;
	@property(ButtonPlus)
	BtnGetMoney: ButtonPlus = null;
	@property(ButtonPlus)
	BtnGetMoneyLog: ButtonPlus = null;
	@property(cc.Node)
	MoneyToggleContainer: cc.Node = null;
	@property(ButtonPlus)
	BtnWeiXin: ButtonPlus = null;
	@property(cc.Label)
	WeiXinShiMingStateLab: cc.Label = null;
	@property(ButtonPlus)
	BtnZhiFuBao: ButtonPlus = null;
	@property(cc.Label)
	ZhiFuBaoShiMingStateLab: cc.Label = null;
 
}