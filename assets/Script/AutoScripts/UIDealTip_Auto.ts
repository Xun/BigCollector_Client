
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIDealTip_Auto extends cc.Component {
	@property(cc.Sprite)
	SpChipIcon: cc.Sprite = null;
	@property(cc.Label)
	LabName: cc.Label = null;
	@property(cc.Label)
	LabCurNum: cc.Label = null;
	@property(cc.Label)
	LabTipType: cc.Label = null;
	@property(ButtonPlus)
	BtnSure: ButtonPlus = null;
	@property(cc.Label)
	LabBuyOrSell: cc.Label = null;
	@property(cc.EditBox)
	EditeNum: cc.EditBox = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
 
}