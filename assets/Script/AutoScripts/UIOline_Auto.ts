
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIOline_Auto extends cc.Component {
	@property(cc.Label)
	OnlineTimeLab: cc.Label = null;
	@property(cc.Label)
	GetNumLab: cc.Label = null;
	@property(ButtonPlus)
	GetBtn: ButtonPlus = null;
	@property(ButtonPlus)
	GetDoubelBtn: ButtonPlus = null;
	@property(ButtonPlus)
	TipBtn: ButtonPlus = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
 
}