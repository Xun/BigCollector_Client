
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIRank_Auto extends cc.Component {
	@property(cc.Node)
	RankToggleNode: cc.Node = null;
	@property(cc.Node)
	CoinContentNode: cc.Node = null;
	@property(cc.Node)
	CoinContentViewNode: cc.Node = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
 
}