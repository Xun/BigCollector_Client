
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UICollection_Auto extends cc.Component {
	@property(cc.Node)
	ContentNode: cc.Node = null;
	@property(cc.ProgressBar)
	WenFangSiBaoProgress: cc.ProgressBar = null;
	@property(cc.Label)
	WenFangSiBaoProgressLab: cc.Label = null;
	@property(ButtonPlus)
	BtnWenFangSiBao: ButtonPlus = null;
	@property(cc.Label)
	WenFangSiBaoGetNumTipLab: cc.Label = null;
	@property(cc.ProgressBar)
	SiJunZiProgress: cc.ProgressBar = null;
	@property(cc.Label)
	SiJunZiProgressLab: cc.Label = null;
	@property(ButtonPlus)
	BtnSiJunZi: ButtonPlus = null;
	@property(cc.Label)
	SiJunZiGetNumTipLab: cc.Label = null;
	@property(cc.ProgressBar)
	MingZhuProgress: cc.ProgressBar = null;
	@property(cc.Label)
	MingZhuProgressLab: cc.Label = null;
	@property(ButtonPlus)
	BtnMingZhu: ButtonPlus = null;
	@property(cc.Label)
	MingZhuGetNumTipLab: cc.Label = null;
	@property(cc.ProgressBar)
	ShenShouProgress: cc.ProgressBar = null;
	@property(cc.Label)
	ShenShouProgressLab: cc.Label = null;
	@property(ButtonPlus)
	BtnShenShou: ButtonPlus = null;
	@property(cc.Label)
	ShenShouGetNumTipLab: cc.Label = null;
	@property(cc.ProgressBar)
	BaDaJiaProgress: cc.ProgressBar = null;
	@property(cc.Label)
	BaDaJiaProgressLab: cc.Label = null;
	@property(ButtonPlus)
	BtnBaDaJia: ButtonPlus = null;
	@property(cc.Label)
	BaDaJiaGetNumTipLab: cc.Label = null;
	@property(cc.ProgressBar)
	JinChanProgress: cc.ProgressBar = null;
	@property(cc.Label)
	JinChanProgressLab: cc.Label = null;
	@property(cc.Label)
	JinChanGetNumTipLab: cc.Label = null;
	@property(cc.ProgressBar)
	PiXiuProgress: cc.ProgressBar = null;
	@property(cc.Label)
	PiXiuProgressLab: cc.Label = null;
	@property(ButtonPlus)
	BtnPiXiu: ButtonPlus = null;
	@property(cc.Label)
	PiXiuGetNumTipLab: cc.Label = null;
	@property(cc.ProgressBar)
	JinLongProgress: cc.ProgressBar = null;
	@property(cc.Label)
	JinLongProgressLab: cc.Label = null;
	@property(ButtonPlus)
	BtnJinLong: ButtonPlus = null;
	@property(cc.Label)
	JinLongGetNumTipLab: cc.Label = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
 
}