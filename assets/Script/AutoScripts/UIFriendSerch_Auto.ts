
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIFriendSerch_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnSure: ButtonPlus = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSerch: ButtonPlus = null;
	@property(cc.EditBox)
	IDBox: cc.EditBox = null;
	@property(cc.Label)
	NameLab: cc.Label = null;
 
}