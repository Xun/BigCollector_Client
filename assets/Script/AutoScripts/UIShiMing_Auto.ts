
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIShiMing_Auto extends cc.Component {
	@property(cc.EditBox)
	Name: cc.EditBox = null;
	@property(cc.EditBox)
	IDCard: cc.EditBox = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSubMit: ButtonPlus = null;
 
}