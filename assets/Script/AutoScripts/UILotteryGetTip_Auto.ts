
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UILotteryGetTip_Auto extends cc.Component {
	@property(cc.Label)
	GetNumLab: cc.Label = null;
	@property(ButtonPlus)
	BtnSure: ButtonPlus = null;
	@property(cc.Sprite)
	GetIconSp: cc.Sprite = null;
 
}