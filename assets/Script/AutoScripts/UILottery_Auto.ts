
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UILottery_Auto extends cc.Component {
	@property(cc.Node)
	LotteryReward: cc.Node = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
	@property(ButtonPlus)
	GoBtn: ButtonPlus = null;
	@property(cc.Label)
	NumTxt: cc.Label = null;
	@property(cc.Label)
	CDTxt: cc.Label = null;
 
}