
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIDebrisGetTip_Auto extends cc.Component {
	@property(cc.Label)
	DebrisLab: cc.Label = null;
	@property(cc.Sprite)
	DebrisIconSp: cc.Sprite = null;
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(ButtonPlus)
	BtnGoToBag: ButtonPlus = null;
 
}