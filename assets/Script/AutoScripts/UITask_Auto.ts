
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UITask_Auto extends cc.Component {
	@property(cc.Node)
	ContentViewNode: cc.Node = null;
	@property(cc.Label)
	TipLab: cc.Label = null;
	@property(cc.ProgressBar)
	TaskProgress: cc.ProgressBar = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
 
}