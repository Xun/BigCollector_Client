
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIBag_Auto extends cc.Component {
	@property(cc.Node)
	ContentViewNode: cc.Node = null;
	@property(cc.Label)
	BagNumLab: cc.Label = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
	@property(ButtonPlus)
	AddBtn: ButtonPlus = null;
 
}