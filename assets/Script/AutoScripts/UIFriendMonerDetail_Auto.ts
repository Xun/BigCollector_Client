
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIFriendMonerDetail_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(cc.Label)
	TodayGetMoneyLab: cc.Label = null;
	@property(cc.Label)
	TotalGetMoneyLab: cc.Label = null;
	@property(cc.Node)
	ContentNode: cc.Node = null;
	@property(cc.Node)
	ContentViewNode: cc.Node = null;
 
}