
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UISetting_Auto extends cc.Component {
	@property(ButtonPlus)
	BtnClose: ButtonPlus = null;
	@property(ButtonPlus)
	BtnOutLogin: ButtonPlus = null;
	@property(ButtonPlus)
	BtnMusic: ButtonPlus = null;
	@property(cc.Node)
	offNode: cc.Node = null;
	@property(cc.Node)
	onNode: cc.Node = null;
	@property(ButtonPlus)
	BtnShiMing: ButtonPlus = null;
	@property(ButtonPlus)
	BtnYinSi: ButtonPlus = null;
	@property(ButtonPlus)
	BtnXieYi: ButtonPlus = null;
 
}