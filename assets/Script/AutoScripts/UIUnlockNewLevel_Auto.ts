
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIUnlockNewLevel_Auto extends cc.Component {
	@property(cc.Label)
	NewLevelLab: cc.Label = null;
	@property(cc.Label)
	NewNameLab: cc.Label = null;
	@property(cc.Label)
	CurLevelProductionNumLab: cc.Label = null;
	@property(cc.Sprite)
	CurLevelProductionIconSp: cc.Sprite = null;
	@property(cc.Node)
	YinPiaoNode: cc.Node = null;
	@property(cc.Label)
	YinPiaoNumLab: cc.Label = null;
	@property(cc.Node)
	SuiPianNode: cc.Node = null;
	@property(cc.Label)
	SuiPianNameLab: cc.Label = null;
	@property(cc.Sprite)
	SuiPianIconSp: cc.Sprite = null;
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
 
}