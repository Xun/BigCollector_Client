import FormMgr from "../Manager/FormMgr";
import CocosHelper from "../Utils/CocosHelper";
import { ModalType } from "./Struct";
import { FormType } from "./SysDefine";
import UIBase from "./UIBase";


export class UIScreen extends UIBase {
    formType = FormType.Screen;
    willDestory = true;

    public async closeSelf(): Promise<boolean> {
        return await FormMgr.close({ prefabUrl: this.fid, type: this.formType });
    }
}

export class UIWindow extends UIBase {
    formType = FormType.Window;
    modalType = new ModalType();                // 阴影类型
    willDestory = true;

    /** 显示效果 */
    public async showEffect() {
        this.node.scale = 0;
        await CocosHelper.runTweenSync(this.node, cc.tween().to(0.3, { scale: 1 }, cc.easeBackOut()));
    }

    public async closeSelf(): Promise<boolean> {
        return await FormMgr.close({ prefabUrl: this.fid, type: this.formType });
    }

}

export class UIFixed extends UIBase {
    formType = FormType.Fixed;
    willDestory = true;

    public async closeSelf(): Promise<boolean> {
        return await FormMgr.close({ prefabUrl: this.fid, type: this.formType });
    }

}

export class UITips extends UIBase {
    formType = FormType.Tips;
    willDestory = true;
    public async closeSelf(): Promise<boolean> {
        return await FormMgr.close({ prefabUrl: this.fid, type: this.formType });
    }
}

// @ts-ignore
cc.UIScreen = UIScreen;
// @ts-ignore
cc.UIWindow = UIWindow;
// @ts-ignore
cc.UIFixed = UIFixed;
// @ts-ignore
cc.UITips = UITips;
