import CocosHelper from "../Utils/CocosHelper";
import GameMgr from "./GameMgr";

export default class ConfigMgr {
    /** 加载配置文件 */
    async loadConfigs() {
        GameMgr.dataModalMgr.ItemProfucesJson.itemsInfo = await CocosHelper.loadJsonSync<cc.JsonAsset>("JsonFile/items", cc.JsonAsset);
        // GameMgr.dataModalMgr.ChipsInfoJson.chipInfo = await CocosHelper.loadJsonSync<cc.JsonAsset>("JsonFile/chips", cc.JsonAsset);
        // GameMgr.dataModalMgr.ShopInfoJson.shopInfo = await CocosHelper.loadJsonSync<cc.JsonAsset>("JsonFile/shop", cc.JsonAsset);
    }
}