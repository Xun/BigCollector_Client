export interface UserInfos {
    account: string;
    nickname: string;
    avatarUrl: string;
}

export interface WorkBench {
    list_workbench: Array<number>;
}

export interface IiemLottery {
    num: string;
    icon: string;
}

export interface IRankItem {
    level: number;
    name: string;
    head?: string;
    num: number;
}

export interface IShopItem {
    name: string;
    item_icon: string;
    buy_icon: string;
    buy_num: number;
}