
export default class LocalStorageMgr {

    public static setItem(key: string, value: any): void {
        cc.sys.localStorage.setItem(`model_${key}`, JSON.stringify(value));
    }

    public static getItem(key: string): any {
        let data = JSON.parse(cc.sys.localStorage.getItem(`model_${key}`));
        if (!data || data === "") {
            return {}
        }
        return data;
    }

    public static removeItem(key: string): void {
        cc.sys.localStorage.removeItem(`model_${key}`);
    }

    public static clearAll(): void {
        cc.sys.localStorage.clear();
    }

}
