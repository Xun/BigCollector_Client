import { DataBagInfo } from "../DataModal/DataBag";
import { DataCollection } from "../DataModal/DataCollection";
import { DataDeal } from "../DataModal/DataDeal";
import { DataFriend, DataFriendGiveDetail, DataFriendMoneyDetail, DataSeeDetail } from "../DataModal/DataFriend";
import { DataGetMoneyLog } from "../DataModal/DataGetMoneyLog";
import { DataMyExtar } from "../DataModal/DataMyExtar";
import { DataCurBuyProduce, ProducesJson } from "../DataModal/DataProduce";
import { DataRank } from "../DataModal/DataRank";
import { ChipJson, DataShop, ShopJson } from "../DataModal/DataShop";
import { UserInfo } from "../DataModal/DataUser";

export class DataModalMgr {
    //用户信息
    public UserInfo = new UserInfo();
    public CurBuyProduceInfo = new DataCurBuyProduce();
    public ItemProfucesJson = new ProducesJson();
    public CollectionInfo = new DataCollection();
    public BagInfo = new DataBagInfo();
    public ShopInfo = new DataShop();
    public FriendInfo = new DataFriend();
    public FriendMonerDetailInfo = new DataFriendMoneyDetail();
    public DataGetMoneyLogInfo = new DataGetMoneyLog();
    public DataMyExtarInfo = new DataMyExtar();
    public DataFriendGiveDetailInfo = new DataFriendGiveDetail();
    public DataSeeDetailInfo = new DataSeeDetail();
    public DataDealInfo = new DataDeal();
    public ChipsInfoJson = new ChipJson();
    public ShopInfoJson = new ShopJson();
    public DataRankInfo = new DataRank();
}

