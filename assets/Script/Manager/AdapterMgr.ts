/**
 * @Author: 邓朗 
 * @Date: 2019-06-12 17:18:04  
 * @Describe: 适配组件, 主要适配背景大小,窗体的位置
 */

let flagOffset = 0;
const _None = 1 << flagOffset++;
const _Left = 1 << flagOffset++;            // 左对齐
const _Right = 1 << flagOffset++;           // 右对齐
const _Top = 1 << flagOffset++;             // 上对齐
const _Bottom = 1 << flagOffset++;          // 下对齐
const _StretchWidth = _Left | _Right;          // 拉伸宽
const _StretchHeight = _Top | _Bottom;         // 拉伸高

const _FullWidth = 1 << flagOffset++;       // 等比充满宽
const _FullHeight = 1 << flagOffset++;      // 等比充满高
const _Final = 1 << flagOffset++;

/**  */
export enum AdapterType {
    Top = _Top,
    Bottom = _Bottom,
    Left = _Left,
    Right = _Right,

    StretchWidth = _StretchWidth,
    StretchHeight = _StretchHeight,

    FullWidth = _FullWidth,
    FullHeight = _FullHeight,
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class AdapterMgr {

    private static _instance: AdapterMgr = null;                     // 单例
    public static get inst() {
        if (this._instance == null) {
            this._instance = new AdapterMgr();
            this._instance.visibleSize = cc.view.getVisibleSize();
            console.log(`visiable size: ${this._instance.visibleSize}`);
        }
        return this._instance;
    }

    /** 屏幕尺寸 */
    public visibleSize: cc.Size;

    public adapteByType(flag: number, node: cc.Node, distance = 0) {
        let widget = node.getComponent(cc.Widget);
        this._doAdapte(flag, node, distance);
        widget.updateAlignment();
    }

    private _doAdapte(flag: number, node: cc.Node, distance: number = 0) {
        let widget = node.getComponent(cc.Widget);
        if (!widget) {
            widget = node.addComponent(cc.Widget);
        }
        switch (flag) {
            case _None:
                break;
            case _Left:
                widget.left = distance ? distance : 0;
                break;
            case _Right:
                widget.right = distance ? distance : 0;
                break;
            case _Top:
                widget.top = distance ? distance : 0;
                break;
            case _Bottom:
                widget.bottom = distance ? distance : 0;
                break;
            case _FullWidth:
                node.height /= node.width / this.visibleSize.width;
                node.width = this.visibleSize.width;
                break;
            case _FullHeight:
                node.width /= node.height / this.visibleSize.height;
                node.height = this.visibleSize.height;
                break;
        }
    }


    /** 移除 */
    removeAdaptater(node: cc.Node) {
        if (node.getComponent(cc.Widget)) {
            node.removeComponent(cc.Widget);
        }
    }

    public checkIsIphoneX() {
        if (cc.view.getFrameSize().width === 375 && cc.view.getFrameSize().height === 812
            || cc.view.getFrameSize().width === 414 && cc.view.getFrameSize().height === 896
            || cc.view.getFrameSize().width === 390 && cc.view.getFrameSize().height === 844
            || cc.view.getFrameSize().width === 428 && cc.view.getFrameSize().height === 926) {
            return true;
        }
        return false;
    }
}
