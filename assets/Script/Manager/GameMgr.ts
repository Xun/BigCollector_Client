
import ConfigMgr from "./ConfigMgr";
import { DataModalMgr } from "./DataModalMgr";

/**
 * 掌管逻辑层
 */
class GameMgr {

    public inited = false;
    public configMgr: ConfigMgr = null;
    public dataModalMgr: DataModalMgr = null;
    public async init(uiRoot: cc.Node) {
        this.configMgr = new ConfigMgr();
        this.dataModalMgr = new DataModalMgr();
        // 初始化平台sdk
        // todo...

        // 加载配置
        await this.configMgr.loadConfigs();
        // this.clientChannel = new ClientChannel(WebSocket);
        // await this.clientChannel.connect("ws://112.74.43.142:5000/ws/test_ticket");
        // 
        this.inited = true;
    }

    onGameShow() {

    }
    onGameHide() {

    }

    /**
     * 逻辑层的时间更新控制
     * @param dt 
     */
    public update(dt: number) {
        if (!this.inited) return;
        // 例如Task.update(dt); 更新任务进度
    }
}

export default new GameMgr();