const { ccclass, property } = cc._decorator;

@ccclass
export default class HttpHelper extends cc.Component {
    /**
     * get请求
     * @param {string} url 
     * @param {function} callback 
     */
    httpGet(url, callback) {
        let xhr = cc.loader.getXMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status == 200) {
                let respone = xhr.responseText;
                cc.log("httpGet response:" + respone)
                let rsp = JSON.parse(respone);
                callback(rsp);
            } else if (xhr.readyState === 4 && xhr.status == 401) {
                cc.log("httpGet response:", xhr.readyState);
            } else {
                cc.log("httpGet response:" + xhr.readyState);
            }
        };
        cc.log("get url ", url);
        xhr.open('GET', url, true);

        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET, POST');
        xhr.setRequestHeader('Access-Control-Allow-Headers', 'x-requested-with,content-type,authorization');
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.timeout = 8000;// 8 seconds for timeout
        xhr.send();
    }

    /**
     * post请求
     * @param {string} url 
     * @param {object} params 
     * @param {function} callback 
     */
    httpPost(url, params, callback) {
        let xhr = cc.loader.getXMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status == 200) {
                let respone = xhr.responseText;
                cc.log("response:", respone);
                let rsp = JSON.parse(respone);
                callback(rsp);
            } else {
                cc.log("httpPost response error ", xhr.readyState);
            }
        };
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET, POST');
        xhr.setRequestHeader('Access-Control-Allow-Headers', 'x-requested-with,content-type');
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.timeout = 8000;// 8 seconds for timeout
        cc.log("send:", JSON.stringify(params))
        xhr.send(JSON.stringify(params));
    }
    //post 表格数据
    public static POST(url: string, param: object = {}, callback: Function) {
        let xhr = cc.loader.getXMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                let response = xhr.responseText;
                if (xhr.status >= 200 && xhr.status < 300) {
                    callback(true, response);
                } else {
                    callback(false, response);
                }
            }
        };
        cc.log("send param :" + param);
        xhr.send(JSON.stringify(param));
    }
}
