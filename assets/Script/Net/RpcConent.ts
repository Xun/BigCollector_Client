import { WsClient } from 'tsrpc-browser';
import { serviceProto } from './shared/protocols/serviceProto';

export const apiClient = new WsClient(serviceProto, {
    server: 'ws://192.168.0.104:3004',
    json: false,
    // jsonPrune: false,
    logger: console,
    heartbeat: {
        interval: 3000,
        timeout: 5000
    }
});