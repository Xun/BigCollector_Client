
import { EventCenter } from "../Net/EventCenter";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIGaide extends cc.Component {

    @property(cc.Node)
    maskNode: cc.Node = null;

    start() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBlockEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchBlockEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchBlockEvent, this);
        EventCenter.on("shouguide", this.Show, this);
        this.node.active = false;
    }

    public Show(node: cc.Node) {
        //得到当前节点大小
        let contentSize = node.getContentSize();
        this.maskNode.width = contentSize.width + 20;
        this.maskNode.height = contentSize.height + 20;

        //转换世界坐标，再转换到引导节点的本地坐标
        let pos = node.parent.convertToWorldSpaceAR(node.position);
        let localpos = this.maskNode.parent.convertToNodeSpaceAR(pos);
        this.maskNode.setPosition(cc.v2(localpos.x, localpos.y));

        this.node.active = true;
    }

    onTouchBlockEvent(event: cc.Event.EventTouch) {
        //setSwallowTouches：true 不向下触摸
        let pt = this.maskNode.convertToNodeSpaceAR(event.getLocation());
        let rect = cc.rect(0, 0, this.maskNode.width, this.maskNode.height);
        //如果没有命中目标节点，则吞噬向下触摸时间，命中则隐藏引导节点
        if (!rect.contains(pt)) {
            console.log("没有命中")
            this.node._touchListener.setSwallowTouches(true);
            event.stopPropagationImmediate();
            return;
        } else {
            console.log("命中")
            this.node._touchListener.setSwallowTouches(false);
        }

        this.node.active = false;
    }

}
