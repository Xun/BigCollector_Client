import UIScoketReconnent_Auto from "../AutoScripts/UIScoketReconnent_Auto";
import { ModalType } from "../Common/Struct";
import { ModalOpacity } from "../Common/SysDefine";
import { UITips, UIWindow } from "../Common/UIForm";
import FormMgr from "../Manager/FormMgr";
import TipsMgr from "../Manager/TipsMgr";
import UIConfig from "../UIConfig";
import CocosHelper from "../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIScoketReconnent extends UIWindow {

    view: UIScoketReconnent_Auto;

    modalType = new ModalType(ModalOpacity.OpacityHigh);

    onLoad() {
        CocosHelper.runRepeatTweenSync(this.view.pan, -1, cc.tween().to(6, { angle: -360 }).to(0, { angle: 0 }));
    }

}
