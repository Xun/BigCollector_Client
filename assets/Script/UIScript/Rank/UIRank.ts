import UIRank_Auto from "../../AutoScripts/UIRank_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { ModalType } from "../../Common/Struct";
import { ModalOpacity } from "../../Common/SysDefine";
import { UIWindow } from "../../Common/UIForm";
import GameMgr from "../../Manager/GameMgr";
import { IRankItem } from "../../Manager/InterfaceMgr";
import { apiClient } from "../../Net/RpcConent";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import ItemRank from "./ItemRank";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIRank extends UIWindow {

    modalType = new ModalType(ModalOpacity.OpacityHigh);
    view: UIRank_Auto;

    @property(ListUtil)
    listRank: ListUtil = null;
    type: number = 0;

    willDestory = false;

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.RankToggleNode.children.forEach((item) => {
            item.on(cc.Node.EventType.TOUCH_END, (e: cc.Event.EventTouch) => {
                this.changeType();
            }, this)
        });

        this.changeType();
    }

    async changeType() {
        let index = CocosHelper.getContenerindex(this.view.RankToggleNode);
        if (index == 0) {
            let data = await apiClient.callApi("Rank", { type: 1 });
            if (!data.isSucc) {
                UIToast.popUp(data.err.message);
                this.listRank.numItems = 0;
                return;
            }
            GameMgr.dataModalMgr.DataRankInfo.rank_list = data.res.rank;
            this.listRank.numItems = data.res.rank.length;
        } else {
            let data = await apiClient.callApi("Rank", { type: 2 });
            if (!data.isSucc) {
                UIToast.popUp(data.err.message);
                this.listRank.numItems = 0;
                return;
            }
            GameMgr.dataModalMgr.DataRankInfo.rank_list = data.res.rank;
            this.listRank.numItems = data.res.rank.length;
        }
    }

    //垂直列表渲染器
    onListRankRender(item: cc.Node, idx: number) {
        let index = CocosHelper.getContenerindex(this.view.RankToggleNode);
        item.getComponent(ItemRank).setData(index, GameMgr.dataModalMgr.DataRankInfo.rank_list[idx], idx + 1);
    }
}
