
import { IRankItem } from "../../Manager/InterfaceMgr";
import { RankItem } from "../../Net/shared/protocols/PtlRank";
import CocosHelper from "../../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemRank extends cc.Component {

    @property(cc.Node)
    level_bg: cc.Node = null;

    @property(cc.Label)
    user_level: cc.Label = null;

    @property(cc.Label)
    user_name: cc.Label = null;

    @property(cc.Label)
    user_num: cc.Label = null;

    @property(cc.Sprite)
    user_head: cc.Sprite = null;

    @property(cc.Sprite)
    icon: cc.Sprite = null;

    @property([cc.SpriteFrame])
    level_bgs: cc.SpriteFrame[] = Array<cc.SpriteFrame>();

    @property([cc.SpriteFrame])
    icons: cc.SpriteFrame[] = Array<cc.SpriteFrame>();

    public setData(type: number, data: RankItem, level: number) {
        if (type === 1) {
            this.icon.spriteFrame = this.icons[0];
        } else {
            this.icon.spriteFrame = this.icons[1];
        }
        this.level_bg.active = false;
        this.user_num.node.color = new cc.Color(0, 0, 0);
        if (level < 4) {
            this.level_bg.getComponent(cc.Sprite).spriteFrame = this.level_bgs[level - 1];
            this.level_bg.active = true;
            this.user_num.node.color = new cc.Color(229, 27, 27);
        }
        CocosHelper.loadHead(data.picUrl, this.user_head);
        this.user_level.string = level.toString();
        this.user_name.string = data.name;
        this.user_num.string = data.money.toString();
    }

}
