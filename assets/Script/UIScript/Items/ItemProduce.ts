import CocosHelper from "../../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemProduce extends cc.Component {

    @property(cc.Node)
    nodeIcon: cc.Node = null;

    @property(cc.Node)
    nodeBg: cc.Node = null;

    @property(cc.Node)
    nodeLevel: cc.Node = null;

    @property(cc.Label)
    txtLevel: cc.Label = null;

    @property(cc.Label)
    txtCoin: cc.Label = null;

    public isDrag: boolean = false;
    public isUsed: boolean = false;
    public _itemId: number = 0;
    public _index: number = -1;
    public _itemType: number = -1;
    public _nextLevel: number = -1;
    public _itemInfo: any = null;
    // onLoad () {}

    start() {

    }
    /**
     * 设置工作台物品信息
     * @param {Number} index 
     * @param {Number} itemId 
     */
    setWorkbenchItemInfo(index: number, itemId: number) {
        //设置信息
        // console.log("index:" + index);
        // console.log("itemId:" + itemId);
        this._index = index;
        this._itemId = itemId;
        this._itemType = 1;
        this.isUsed = false;
        this.txtCoin.node.active = true;
        if (this._itemId) {
            this.nodeIcon.active = false;
            this._itemInfo = CocosHelper.getItemProduceInfoById(this._itemId);
            CocosHelper.setItemProduceIcon(this._itemId, this.nodeIcon.getComponent(cc.Sprite), () => {
                this.nodeIcon.active = true;
                this.txtLevel.string = this._itemInfo.id.toString();
                this.nodeLevel.active = true;
                this.playProduceAni(true);
            });
        } else {
            this.nodeIcon.active = false;
            this.nodeLevel.active = false;
            this.txtCoin.node.active = false;
        }

        if (this.isUsed) {
            this.nodeIcon.opacity = 150;
        } else if (!this.isDrag) {
            this.nodeIcon.opacity = 255;
        }
    }

    dragStart() {
        if (this.isUsed || !this._itemId) { //正在使用中，不可拖拽
            return false;
        }

        this.isDrag = true;
        this.nodeIcon.opacity = 150;
        return true;
    }

    dragOver() {
        if (this.isDrag) {
            this.isDrag = false;
            this.nodeIcon.opacity = 255;
        }
    }

    getInfo() {
        return this._itemId;
    }

    showVirtual() {
        this.nodeBg.active = false;
    }

    playProduceAni(isShow) {
        if (isShow) {
            this.schedule(() => {
                CocosHelper.runTweenSync(this.txtCoin.node, cc.tween().to(1, { y: 110 }).to(1, { y: 20 }))
                CocosHelper.runTweenSync(this.nodeIcon, cc.tween().to(0.5, { scale: 1.1 }).to(0.2, { scale: 1 }))
            }, 5)
            //播放动画
        } else {
            this.unscheduleAllCallbacks();
            //隐藏动画
        }
    }
}
