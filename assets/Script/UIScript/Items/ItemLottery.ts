import { IiemLottery } from "../../Manager/InterfaceMgr";
import CocosHelper from "../../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemLottery extends cc.Component {

    // @property(cc.Label)
    // num: cc.Label = null;

    @property(cc.Sprite)
    icon: cc.Sprite = null;

    setInfo(data: IiemLottery) {
        // this.num.string = data.num;
        CocosHelper.setLotteryIcon(data.icon, this.icon);
    }
}
