
import UITask_Auto from "../../AutoScripts/UITask_Auto";
import { UIWindow } from "../../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UITask extends UIWindow {

    view: UITask_Auto;

    // onLoad () {}

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
    }

}
