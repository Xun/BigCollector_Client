// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIDealDetail_Auto from "../../AutoScripts/UIDealDetail_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import GameMgr from "../../Manager/GameMgr";
import { apiClient } from "../../Net/RpcConent";
import UIToast from "../UIToast";
import ItemDealDetail from "./ItemDealDetail";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIDealDetail extends UIWindow {

    view: UIDealDetail_Auto;

    @property(ListUtil)
    listDealDetail: ListUtil = null;

    async start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
        let data = await apiClient.callApi("DealDetail", {});
        if (!data.isSucc) {
            UIToast.popUp(data.err.message);
            GameMgr.dataModalMgr.DataDealInfo.dealDetailList = [];
            this.listDealDetail.numItems = 0;
        }
        GameMgr.dataModalMgr.DataDealInfo.dealDetailList = data.res.dealList;
        this.listDealDetail.numItems = data.res.dealList.length;
    }

    //垂直列表渲染器
    onListDealDetailRender(item: cc.Node, idx: number) {
        item.getComponent(ItemDealDetail).setData(GameMgr.dataModalMgr.DataDealInfo.dealDetailList[idx]);
    }
}
