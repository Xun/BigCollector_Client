
import UIBuyTip_Auto from "../../AutoScripts/UIBuyTip_Auto";
import { UIWindow } from "../../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIBuyTip extends UIWindow {

    view: UIBuyTip_Auto;

    // onLoad () {}

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);
    }

    // update (dt) {}
}
