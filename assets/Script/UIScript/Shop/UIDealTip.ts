
import UIDealTip_Auto from "../../AutoScripts/UIDealTip_Auto";
import { UIWindow } from "../../Common/UIForm";
import GameMgr from "../../Manager/GameMgr";
import UIManager from "../../Manager/UIManager";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import UIDeal from "./UIDeal";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIDealTip extends UIWindow {

    view: UIDealTip_Auto;

    private _dealType: number = 0;
    private _chipNum: number = 0;
    private _curNum: number = 0;
    private _chipName: string = "";
    private _dealId: string = "";
    private _price: number = 0;
    // onLoad () {}

    public onShow(params: any): void {
        if (params.type == 2) {
            this.view.LabBuyOrSell.string = "购买";
            this.view.LabCurNum.string = "购买1个" + params.chipName + "碎片需要" + params.price + "玉石";
            this.view.LabTipType.string = "";
        } else {
            this.view.LabBuyOrSell.string = "出售";
            this.view.LabCurNum.string = "当前可出售：999个";
            this.view.LabTipType.string = "出售所得玉石将扣5%手续费";
        }
        this._price = params.price;
        this._dealId = params.orderId;
        this._dealType = params.type;
        this._chipNum = params.chipNum;
        this._chipName = params.chipName;
        this.view.LabName.string = params.chipName + "碎片";
        CocosHelper.setDealIcon(params.chipName, this.view.SpChipIcon);
    }

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);
        this.view.EditeNum.node.on('text-changed', this.onInputNum, this);
        this.view.BtnSure.addClick(this.onSureClick, this);
    }

    onInputNum(event) {
        this._curNum = Number(event.string);
        let curPrice = this._curNum * this._price;
        this.view.LabCurNum.string = "购买" + this._curNum + "个" + this._chipName + "需要" + curPrice + "玉石";
    }

    async onSureClick() {
        if (this._chipNum <= 0) {
            UIToast.popUp("请输入正确的数量");
            return;
        }
        if (this._chipNum < this._curNum) {
            UIToast.popUp("超过当前订单数量");
            return;
        }
        if (this._dealType == 2) {
            let data = await apiClient.callApi("DealType", { type: this._dealType, dealID: this._dealId, count: this._curNum });
            if (!data.isSucc) {
                UIToast.popUp(data.err.message);
                return;
            }
            GameMgr.dataModalMgr.DataDealInfo.dealBuyList = data.res.dealList;
            UIManager.getInstance().getForm(UIConfig.UIDeal.prefabUrl).getComponent(UIDeal).reflushBuyList();
        } else {
            let data = await apiClient.callApi("DealType", { type: this._dealType, dealID: this._dealId, count: this._curNum });
            if (!data.isSucc) {
                UIToast.popUp(data.err.message);
                return;
            }
            GameMgr.dataModalMgr.DataDealInfo.dealSellList = data.res.dealList;
            UIManager.getInstance().getForm(UIConfig.UIDeal.prefabUrl).getComponent(UIDeal).reflushSellList();
        }
        this.closeSelf();
    }

    // update (dt) {}
}
