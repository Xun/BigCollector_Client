import { DataDealItem } from "../../DataModal/DataDeal";
import FormMgr from "../../Manager/FormMgr";
import GameMgr from "../../Manager/GameMgr";
import UIManager from "../../Manager/UIManager";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import UIDeal from "./UIDeal";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemDeal extends cc.Component {

    @property(cc.Label)
    item_name: cc.Label = null;

    @property(cc.Label)
    item_num: cc.Label = null;

    @property(cc.Label)
    item_price: cc.Label = null;

    @property(cc.Sprite)
    item_icon: cc.Sprite = null;

    @property(cc.Node)
    item_buy: cc.Node = null;

    @property(cc.Node)
    item_sell: cc.Node = null;

    private type_str: string = "";
    private orderid: string = "";
    private chip_num: number = 0;
    private chip_name: string = "";
    setData(data: DataDealItem, type: number) {
        if (type === 1) {
            this.type_str = "求购数量:"
            this.item_buy.active = false;
            this.item_sell.active = true;
        }
        if (type === 2) {
            this.type_str = "出售数量:"
            this.item_buy.active = true;
            this.item_sell.active = false;
        }
        this.item_name.string = CocosHelper.getChipNameByID(data.chipID.toString());
        this.chip_name = this.item_name.string.replace("碎片", "");
        CocosHelper.setDealIcon(this.chip_name, this.item_icon);
        this.item_num.string = this.type_str + data.currentCount;
        this.chip_num = data.currentCount;
        this.item_price.string = data.price.toString();
        this.orderid = data.guid;
    }

    async onBuy() {
        FormMgr.open(UIConfig.UIDealTip, { price: Number(this.item_price.string), orderId: this.orderid, chipName: this.chip_name, chipNum: this.chip_num, type: 2 })
    }

    async onSell() {
        FormMgr.open(UIConfig.UIDealTip, { price: Number(this.item_price.string), orderId: this.orderid, chipName: this.chip_name, chipNum: this.chip_num, type: 1 })
    }
}
