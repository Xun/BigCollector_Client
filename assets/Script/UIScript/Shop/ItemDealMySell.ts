// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { DataDealItem } from "../../DataModal/DataDeal";
import GameMgr from "../../Manager/GameMgr";
import UIManager from "../../Manager/UIManager";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import UIDealMySell from "./UIDealMySell";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemDealMySell extends cc.Component {

    @property(cc.Label)
    item_name: cc.Label = null;

    @property(cc.Label)
    item_num: cc.Label = null;

    @property(cc.Label)
    item_type: cc.Label = null;

    @property(cc.Label)
    item_price: cc.Label = null;

    @property(cc.Sprite)
    item_icon: cc.Sprite = null;

    @property(cc.Node)
    item_cancel: cc.Node = null;

    private curType: string = "进行中";
    private deal_id: string = "";
    setData(data: DataDealItem) {
        this.item_name.string = CocosHelper.getChipNameByID(data.chipID.toString());
        let chip_name: string = this.item_name.string.replace("碎片", "");
        CocosHelper.setDealIcon(chip_name, this.item_icon);
        this.item_num.string = "已卖出" + (data.needCount - data.currentCount) + "/" + data.needCount;
        this.item_price.string = data.price.toString();
        if (data.status === -1) {
            this.curType = "订单下架";
        }
        if (data.status === 1) {
            this.curType = "已完成";
        }
        this.item_type.string = this.curType;
        this.deal_id = data.guid;
        this.item_cancel.active = data.status === 0;
    }

    async onCancel() {
        let datas: any = await apiClient.callApi("DealCancel", { type: 1, dealID: this.deal_id });
        if (!datas.isSucc) {
            UIToast.popUp(datas.err.message);
            return;
        }

        for (let i = 0; i < GameMgr.dataModalMgr.DataDealInfo.dealMySellList.length; i++) {
            if (GameMgr.dataModalMgr.DataDealInfo.dealMySellList[i].guid == datas.res.dealList[0].guid) {
                GameMgr.dataModalMgr.DataDealInfo.dealMySellList[i] = datas.res.dealList[0];
                UIManager.getInstance().getForm(UIConfig.UIDealMySell.prefabUrl).getComponent(UIDealMySell).reflashList();
                break;
            }
        }
    }
}
