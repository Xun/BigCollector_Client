// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIShop_Auto from "../../AutoScripts/UIShop_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import { DataShopItem } from "../../DataModal/DataShop";
import GameMgr from "../../Manager/GameMgr";
import CocosHelper from "../../Utils/CocosHelper";
import ItemShop from "./ItemShop";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIShop extends UIWindow {

    view: UIShop_Auto;

    @property(ListUtil)
    listShop: ListUtil = null;

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
        GameMgr.dataModalMgr.ShopInfo.list_item = GameMgr.dataModalMgr.ShopInfoJson.shopInfo;
        this.listShop.numItems = GameMgr.dataModalMgr.ShopInfo.list_item.length;
    }

    //垂直列表渲染器
    onListShopRender(item: cc.Node, idx: number) {
        item.getComponent(ItemShop).setData(GameMgr.dataModalMgr.ShopInfo.list_item[idx]);
    }
}
