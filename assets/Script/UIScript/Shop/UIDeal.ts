import UIDeal_Auto from "../../AutoScripts/UIDeal_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import FormMgr from "../../Manager/FormMgr";
import GameMgr from "../../Manager/GameMgr";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import ItemDeal from "./ItemDeal";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIDeal extends UIWindow {

    view: UIDeal_Auto;

    @property(ListUtil)
    listDeal: ListUtil = null;
    private chipid: string = "";
    private itemname: string = "笔";
    private deal_type: number = 0;
    onLoad() {
        this.view.NodeDealItems.active = false;
    }

    start() {

        this.view.DealToggleNode.children.forEach((item) => {
            item.on(cc.Node.EventType.TOUCH_END, (e: cc.Event.EventTouch) => {
                this.onSelectType(this.itemname);
            }, this)
        })

        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.BtnCloseNodeItems.addClick(() => {
            this.view.NodeDealItems.active = false;
        }, this);

        this.view.BtnSelectType.addClick(() => {
            this.view.NodeDealItems.active = !this.view.NodeDealItems.active;
        }, this);

        this.view.BtnDealDetail.addClick(() => {
            this.view.NodeDealItems.active = false;
            FormMgr.open(UIConfig.UIDealDetail);
        }, this);

        this.view.BtnMyBuy.addClick(() => {
            FormMgr.open(UIConfig.UIDealMyGet);
        }, this);

        this.view.BtnMySell.addClick(() => {
            FormMgr.open(UIConfig.UIDealMySell);
        }, this);

        this.view.BtnBi.addClick(() => {
            this.onSelectType("笔");
        }, this);

        this.view.BtnZhi.addClick(() => {
            this.onSelectType("纸");
        }, this);

        this.view.BtnMo.addClick(() => {
            this.onSelectType("墨");
        }, this);

        this.view.BtnYan.addClick(() => {
            this.onSelectType("砚");
        }, this);

        this.view.BtnM.addClick(() => {
            this.onSelectType("梅");
        }, this);

        this.view.BtnL.addClick(() => {
            this.onSelectType("兰");
        }, this);

        this.view.BtnZ.addClick(() => {
            this.onSelectType("竹");
        }, this);

        this.view.BtnJ.addClick(() => {
            this.onSelectType("菊");
        }, this);

        this.view.BtnXYJ.addClick(() => {
            this.onSelectType("西游记");
        }, this);

        this.view.BtnHLM.addClick(() => {
            this.onSelectType("红楼梦");
        }, this);

        this.view.BtnSHZ.addClick(() => {
            this.onSelectType("水浒传");
        }, this);

        this.view.BtnSGYY.addClick(() => {
            this.onSelectType("三国演义");
        }, this);

        this.view.BtnQL.addClick(() => {
            this.onSelectType("青龙");
        }, this);

        this.view.BtnBH.addClick(() => {
            this.onSelectType("白虎");
        }, this);

        this.view.BtnXW.addClick(() => {
            this.onSelectType("玄武");
        }, this);

        this.view.BtnZQ.addClick(() => {
            this.onSelectType("朱雀");
        }, this);

        this.view.BtnZG.addClick(() => {
            this.onSelectType("曾巩");
        }, this);

        this.view.BtnLZY.addClick(() => {
            this.onSelectType("柳宗元");
        }, this);

        this.view.BtnWAS.addClick(() => {
            this.onSelectType("王安石");
        }, this);

        this.view.BtnHY.addClick(() => {
            this.onSelectType("韩愈");
        }, this);

        this.view.BtnOYX.addClick(() => {
            this.onSelectType("欧阳修");
        }, this);

        this.view.BtnSX.addClick(() => {
            this.onSelectType("苏洵");
        }, this);

        this.view.BtnSZ.addClick(() => {
            this.onSelectType("苏辙");
        }, this);

        this.view.BtnSS.addClick(() => {
            this.onSelectType("苏轼");
        }, this);

        this.view.BtnJC.addClick(() => {
            this.onSelectType("金蟾");
        }, this);

        this.view.BtnPX.addClick(() => {
            this.onSelectType("貔貅");
        }, this);

        this.view.BtnJL.addClick(() => {
            this.onSelectType("金龙");
        }, this);
        this.onSelectType("笔");
        // this.changeType();
    }

    async onSelectType(itme_name: string) {
        this.itemname = itme_name;
        CocosHelper.setDealIcon(itme_name, this.view.DealIcon);
        this.view.NodeDealItems.active = false;
        this.chipid = CocosHelper.getChipInfoByName(itme_name + "碎片");
        console.log("------------" + this.chipid);

        let index = CocosHelper.getContenerindex(this.view.DealToggleNode);

        if (index == 0) {
            this.deal_type = 2;
        }
        if (index == 1) {
            this.deal_type = 1;
        }
        let datas = await apiClient.callApi("Deal", { type: this.deal_type, chipID: Number(this.chipid) });
        if (!datas.isSucc) {
            UIToast.popUp(datas.err.message);
            return;
        }
        if (index == 0) {
            console.log("aaaaaa")
            GameMgr.dataModalMgr.DataDealInfo.dealSellList = datas.res.dealList;
            this.listDeal.numItems = GameMgr.dataModalMgr.DataDealInfo.dealSellList.length;

        }
        if (index == 1) {
            console.log("bbbbbb")
            GameMgr.dataModalMgr.DataDealInfo.dealBuyList = datas.res.dealList;
            this.listDeal.numItems = GameMgr.dataModalMgr.DataDealInfo.dealBuyList.length;
        }

    }

    reflushBuyList() {
        this.listDeal.numItems = GameMgr.dataModalMgr.DataDealInfo.dealBuyList.length;
    }

    reflushSellList() {
        this.listDeal.numItems = GameMgr.dataModalMgr.DataDealInfo.dealSellList.length;
    }

    //垂直列表渲染器
    onListDealRender(item: cc.Node, idx: number) {
        let index = CocosHelper.getContenerindex(this.view.DealToggleNode);
        console.log("-------------index:" + index);
        if (index == 0) {
            if (GameMgr.dataModalMgr.DataDealInfo.dealSellList.length) {
                item.getComponent(ItemDeal).setData(GameMgr.dataModalMgr.DataDealInfo.dealSellList[idx], 1);
            }


        } else {
            if (GameMgr.dataModalMgr.DataDealInfo.dealBuyList.length > 0) {
                item.getComponent(ItemDeal).setData(GameMgr.dataModalMgr.DataDealInfo.dealBuyList[idx], 2);
            }

        }
    }

}
