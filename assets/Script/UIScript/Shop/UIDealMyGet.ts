// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIDealMyGet_Auto from "../../AutoScripts/UIDealMyGet_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import { DataSeeDetail } from "../../DataModal/DataFriend";
import FormMgr from "../../Manager/FormMgr";
import GameMgr from "../../Manager/GameMgr";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import ItemDealMyGet from "./ItemDealMyGet";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIDealMyGet extends UIWindow {

    view: UIDealMyGet_Auto;

    @property(ListUtil)
    listDeal: ListUtil = null;

    // onLoad () {}

    async start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.BuyAdd.addClick(() => {
            FormMgr.open(UIConfig.UIDealAdd);
        }, this);

        let datas = await apiClient.callApi("DealOrder", { userAccount: GameMgr.dataModalMgr.UserInfo.userAccount, type: 2 });
        if (!datas.isSucc) {
            UIToast.popUp(datas.err.message);
        }
        GameMgr.dataModalMgr.DataDealInfo.dealMyBuyList = datas.res.dealList;
        console.log(datas.res.dealList.length)
        this.listDeal.numItems = GameMgr.dataModalMgr.DataDealInfo.dealMyBuyList.length;
    }
    public reflashList() {
        this.listDeal.numItems = GameMgr.dataModalMgr.DataDealInfo.dealMyBuyList.length;
        console.log("刷新之后-------------" + this.listDeal.numItems)
    }
    //垂直列表渲染器
    onListDealMyGetRender(item: cc.Node, idx: number) {
        item.getComponent(ItemDealMyGet).setData(GameMgr.dataModalMgr.DataDealInfo.dealMyBuyList[idx]);
    }

}
