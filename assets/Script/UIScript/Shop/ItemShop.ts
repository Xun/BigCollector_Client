
import ButtonPlus from "../../Common/Components/ButtonPlus";
import { DataShopItem } from "../../DataModal/DataShop";
import FormMgr from "../../Manager/FormMgr";
import { IShopItem } from "../../Manager/InterfaceMgr";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemShop extends cc.Component {

    @property(cc.Label)
    item_name: cc.Label = null;

    @property(cc.Sprite)
    item_icon: cc.Sprite = null;

    @property(cc.Sprite)
    buy_type_sp: cc.Sprite = null;

    @property(cc.Label)
    buy_type_lab: cc.Label = null;

    @property(ButtonPlus)
    buy_btn: ButtonPlus = null;

    private buyId: number = 0;
    start() {
        this.buy_btn.addClick(() => {
            // FormMgr.open(UIConfig.UIBuyTip);
            this.onBuy();
        }, this);
    }

    public setData(data: DataShopItem) {
        this.buyId = Number(data.id);
        if (data.name == "铜钱少" || data.name == "铜钱多") {
            CocosHelper.setDealIcon("玉石", this.buy_type_sp);
            if (data.name == "铜钱少") {
                this.item_name.string = "50万";
            } else {
                this.item_name.string = "500万";
            }
        } else {
            CocosHelper.setDealIcon(data.name, this.buy_type_sp);
            this.item_name.string = data.name;
        }
        CocosHelper.setShopInfo(data.name, this.item_icon);

        this.buy_type_lab.string = data.need_num;
    }

    async onBuy() {
        let data = await apiClient.callApi("Convert", { id: this.buyId });
        if (!data.isSucc) {
            UIToast.popUp(data.err.message);
            return;
        }
        UIToast.popUp("购买成功")
    }

}
