// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIDealSell_Auto from "../../AutoScripts/UIDealSell_Auto";
import { UIWindow } from "../../Common/UIForm";
import GameMgr from "../../Manager/GameMgr";
import UIManager from "../../Manager/UIManager";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import UIDealMySell from "./UIDealMySell";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIDealSell extends UIWindow {

    view: UIDealSell_Auto;

    private deal_num: number = 0;
    private deal_price: number = 0;

    // onLoad () {}

    start() {

        this.view.LabRemain.string = "可出售数量为：";

        this.view.LabBtn.node.active = true;
        this.view.IconType.node.active = false;

        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.BtnSelectType.addClick(() => {
            this.view.NodeDealItems.active = !this.view.NodeDealItems.active;
        }, this);

        this.view.BtnCloseNodeItems.addClick(() => {
            this.view.NodeDealItems.active = false;
        }, this);

        this.view.BuySell.addClick(() => {
            this.onOrderSell();
        }, this);

        this.view.EditeNum.node.on('text-changed', this.onInputNum, this);
        this.view.EditePrice.node.on('text-changed', this.onInputPrice, this);

        this.view.BtnBi.addClick(() => {
            this.onSelectType("笔");
        }, this);

        this.view.BtnZhi.addClick(() => {
            this.onSelectType("纸");
        }, this);

        this.view.BtnMo.addClick(() => {
            this.onSelectType("墨");
        }, this);

        this.view.BtnYan.addClick(() => {
            this.onSelectType("砚");
        }, this);

        this.view.BtnM.addClick(() => {
            this.onSelectType("梅");
        }, this);

        this.view.BtnL.addClick(() => {
            this.onSelectType("兰");
        }, this);

        this.view.BtnZ.addClick(() => {
            this.onSelectType("竹");
        }, this);

        this.view.BtnJ.addClick(() => {
            this.onSelectType("菊");
        }, this);

        this.view.BtnXYJ.addClick(() => {
            this.onSelectType("西游记");
        }, this);

        this.view.BtnHLM.addClick(() => {
            this.onSelectType("红楼梦");
        }, this);

        this.view.BtnSHZ.addClick(() => {
            this.onSelectType("水浒传");
        }, this);

        this.view.BtnSGYY.addClick(() => {
            this.onSelectType("三国演义");
        }, this);

        this.view.BtnQL.addClick(() => {
            this.onSelectType("青龙");
        }, this);

        this.view.BtnBH.addClick(() => {
            this.onSelectType("白虎");
        }, this);

        this.view.BtnXW.addClick(() => {
            this.onSelectType("玄武");
        }, this);

        this.view.BtnZQ.addClick(() => {
            this.onSelectType("朱雀");
        }, this);

        this.view.BtnZG.addClick(() => {
            this.onSelectType("曾巩");
        }, this);

        this.view.BtnLZY.addClick(() => {
            this.onSelectType("柳宗元");
        }, this);

        this.view.BtnWAS.addClick(() => {
            this.onSelectType("王安石");
        }, this);

        this.view.BtnHY.addClick(() => {
            this.onSelectType("韩愈");
        }, this);

        this.view.BtnOYX.addClick(() => {
            this.onSelectType("欧阳修");
        }, this);

        this.view.BtnSX.addClick(() => {
            this.onSelectType("苏洵");
        }, this);

        this.view.BtnSZ.addClick(() => {
            this.onSelectType("苏辙");
        }, this);

        this.view.BtnSS.addClick(() => {
            this.onSelectType("苏轼");
        }, this);

        this.view.BtnJC.addClick(() => {
            this.onSelectType("金蟾");
        }, this);

        this.view.BtnPX.addClick(() => {
            this.onSelectType("貔貅");
        }, this);

        this.view.BtnJL.addClick(() => {
            this.onSelectType("金龙");
        }, this);

    }

    onInputNum(event) {
        this.deal_num = Number(event.string);

    }

    onInputPrice(event) {
        this.deal_price = Number(event.string);
    }

    onSelectType(itme_name: string) {
        this.view.LabBtn.node.active = false;
        this.view.IconType.node.active = true;
        this.view.IconName.string = itme_name + "碎片";
        CocosHelper.setDealIcon(itme_name, this.view.IconType);
        this.view.NodeDealItems.active = false;
    }

    async onOrderSell() {
        if (this.deal_price === 0 || this.deal_price === null) {
            UIToast.popUp("请输入您的出售单价");
            return;
        }
        if (this.deal_num === 0 || this.deal_num === null) {
            UIToast.popUp("请输入您要出售的数量");
            return;
        }
        let chipid = CocosHelper.getChipInfoByName(this.view.IconName.string)
        let datas: any = await apiClient.callApi("DealIssue", { dealType: 1, chipID: Number(chipid), price: this.deal_price, chipNum: this.deal_num });
        if (!datas.isSucc) {
            UIToast.popUp(datas.err.message);
            return;
        }
        GameMgr.dataModalMgr.DataDealInfo.dealMySellList.push(datas.res.dealList[0]);
        UIManager.getInstance().getForm(UIConfig.UIDealMySell.prefabUrl).getComponent(UIDealMySell).reflashList();
        UIToast.popUp("发布订单成功");
        this.closeSelf();
    }

}
