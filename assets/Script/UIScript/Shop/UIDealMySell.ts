// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIDealMySell_Auto from "../../AutoScripts/UIDealMySell_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import FormMgr from "../../Manager/FormMgr";
import GameMgr from "../../Manager/GameMgr";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import UIToast from "../UIToast";
import ItemDealMyGet from "./ItemDealMyGet";
import ItemDealMySell from "./ItemDealMySell";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIDealMySell extends UIWindow {

    view: UIDealMySell_Auto;

    @property(ListUtil)
    listDeal: ListUtil = null;


    // onLoad () {}

    async start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.BuyAdd.addClick(() => {
            FormMgr.open(UIConfig.UIDealSell);
        }, this);

        let datas = await apiClient.callApi("DealOrder", { userAccount: GameMgr.dataModalMgr.UserInfo.userAccount, type: 1 });
        if (!datas.isSucc) {
            UIToast.popUp(datas.err.message);
        }
        GameMgr.dataModalMgr.DataDealInfo.dealMySellList = datas.res.dealList;
        this.listDeal.numItems = GameMgr.dataModalMgr.DataDealInfo.dealMySellList.length;
    }

    public reflashList() {
        this.listDeal.numItems = GameMgr.dataModalMgr.DataDealInfo.dealMySellList.length;
    }
    //垂直列表渲染器
    onListDealMySellRender(item: cc.Node, idx: number) {
        item.getComponent(ItemDealMySell).setData(GameMgr.dataModalMgr.DataDealInfo.dealMySellList[idx]);
    }

}
