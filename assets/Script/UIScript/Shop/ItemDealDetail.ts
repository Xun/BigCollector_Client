// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { DataDealDetailItem } from "../../DataModal/DataDeal";
import CocosHelper from "../../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemDealDetail extends cc.Component {

    @property(cc.Label)
    item_num: cc.Label = null;

    @property(cc.Label)
    item_name: cc.Label = null;

    @property(cc.Label)
    item_time: cc.Label = null;

    @property(cc.Label)
    item_type: cc.Label = null;

    @property(cc.Label)
    item_price: cc.Label = null;

    setData(data: DataDealDetailItem) {
        this.item_name.string = CocosHelper.getChipNameByID(data.chipID.toString());
        this.item_num.string = data.count.toString();
        this.item_price.string = data.price.toString();
        this.item_type.string = data.type == 1 ? "卖出" : "买入";
        this.item_time.string = CocosHelper.changeTime(data.time);
    }

}
