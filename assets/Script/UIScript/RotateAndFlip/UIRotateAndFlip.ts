
import UIRotateAndFlip_Auto from "../../AutoScripts/UIRotateAndFlip_Auto";
import { UIWindow } from "../../Common/UIForm";
import CocosHelper from "../../Utils/CocosHelper";
import CardArrayFlip_FrontCardBase from "./CardArrayFlip_FrontCardBase";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIRotateAndFlip extends UIWindow {

    view: UIRotateAndFlip_Auto;

    protected card: CardArrayFlip_FrontCardBase = null;

    protected get frontArrayCard() {
        return this.view.ContainerNode.children[this.view.ContainerNode.childrenCount - 1];
    }

    start() {
        this.card = this.view.CardNode.getComponent(CardArrayFlip_FrontCardBase);
        // GO
        this.play();
    }

    /**
     * GO
     */
    public async play() {
        const frontCard = this.card;
        // 旋转两圈
        await this.rotate(2);
        // 等一会
        await CocosHelper.sleepSync(0.2);
        // 替换卡片
        frontCard.show();
        this.frontArrayCard.active = false;
        // 翻卡
        await frontCard.flipToFront();
        // 等一会
        await CocosHelper.sleepSync(0.2);
        // 翻卡
        await frontCard.flipToBack();
        // 替换卡片
        this.frontArrayCard.active = true;
        frontCard.hide();
        // 等一会
        await CocosHelper.sleepSync(0.2);
        // 继续
        this.play();
    }

    /**
     * 旋转
     * @param round 圈数
     */
    public rotate(round: number) {
        return new Promise<void>(ress => {
            const node = this.view.ContainerNode,
                time = 1 * round,
                { x, z } = this.node.eulerAngles,
                eulerAngles = cc.v3(x, 360 * round, z);
            cc.tween(node)
                .by(time, { eulerAngles }, { easing: 'quadOut' })
                .call(ress)
                .start();
        });
    }

}
