
const { ccclass, property } = cc._decorator;

@ccclass
export default class GenerateQR extends cc.Component {


	onLoad() {
		this.init('http://baidu.com');
	}

	init(url: string) {

		let ctx: cc.Graphics = this.node.addComponent(cc.Graphics);
		if (typeof (url) !== 'string') {
			console.log('url is not string', url);
			return;
		}

		this.createQR(ctx, url);
	}

	createQR(ctx: cc.Graphics, url: string) {
		let qrcode: QRCode = new QRCode(-1, QRErrorCorrectLevel.H);
		qrcode.addData(url);
		qrcode.make();

		ctx.fillColor = cc.Color.BLACK;

		//块宽高
		let tileW = this.node.width / qrcode.getModuleCount();
		let tileH = this.node.height / qrcode.getModuleCount();

		// 用Graphics画二维码
		for (let row = 0; row < qrcode.getModuleCount(); row++) {
			for (let col = 0; col < qrcode.getModuleCount(); col++) {
				if (qrcode.isDark(row, col)) {
					// ctx.fillColor = cc.Color.BLACK;
					let w = (Math.ceil((col + 1) * tileW) - Math.floor(col * tileW));
					let h = (Math.ceil((row + 1) * tileW) - Math.floor(row * tileW));
					ctx.rect(Math.round(col * tileW) - this.node.width / 2, Math.round(row * tileH) - this.node.height / 2, w, h);
					ctx.fill();
				}
			}
		}
	}
}
