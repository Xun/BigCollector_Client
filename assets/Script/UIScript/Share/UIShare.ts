// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIShare_Auto from "../../AutoScripts/UIShare_Auto";
import { UIWindow } from "../../Common/UIForm";
import CocosHelper from "../../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIShare extends UIWindow {

    @property(cc.Camera)
    camereCapture: cc.Camera = null;

    texture2: cc.RenderTexture = null;
    _width: number = 0;
    _height: number = 0;

    view: UIShare_Auto;

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
        this.view.PhotoBtn.addClick(this.onCapture, this);
    }
    onCapture() {
        let node = this.screenShot(this.view.contentNode);
        node.x = 0;
        node.y = 0;
        node.parent = cc.director.getScene();
        this.captureAction(node, node.width, node.height);
    }

    /**
     * 截图
     * @param targetNode  截图目标节点，如果为null则表示截全屏
     * @returns 返回截屏图片的node
     */
    screenShot(targetNode: cc.Node = null) {
        //创建新的texture
        let texture = new cc.RenderTexture();
        texture.initWithSize(cc.winSize.width, cc.winSize.height, (cc.game as any)._renderContext.STENCIL_INDEX8);
        //创建新的spriteFrame
        let spriteFrame = new cc.SpriteFrame();
        let nodeX = cc.winSize.width / 2 + targetNode.x - targetNode.width / 2;
        let nodeY = cc.winSize.height / 2 + targetNode.y - targetNode.height / 2;
        let nodeWidth = targetNode.width;
        let nodeHeight = targetNode.height;
        if (targetNode == null) {
            spriteFrame.setTexture(texture);
        } else {

            //只显示node部分的图片
            spriteFrame.setTexture(texture, new cc.Rect(nodeX, nodeY, nodeWidth, nodeHeight));
        }
        //创建新的node
        let node = new cc.Node();
        let sprite = node.addComponent(cc.Sprite);
        sprite.spriteFrame = spriteFrame;
        //截图是反的，这里将截图scaleY取反，这样就是正的了
        // sprite.node.scaleY = - Math.abs(sprite.node.scaleY);
        sprite.spriteFrame.setFlipY(true);

        // let buffer = new ArrayBuffer(targetNode.width * targetNode.height * 4);
        // let data = new Uint8Array(buffer);
        // texture.readPixels(data, nodeX, nodeY, nodeWidth, nodeHeight);
        //手动渲染camera
        // camera.cullingMask = 0xffffffff;
        this.camereCapture.targetTexture = texture;
        this.camereCapture.render();
        this.camereCapture.targetTexture = null;

        return node;
    }

    captureAction(capture, width, height) {
        let scaleAction = cc.scaleTo(1, 0.5);
        let targetPos = cc.v2(this.node.width / 2 + width / 2 - 50, height + height / 2);
        let moveAction = cc.moveTo(1, targetPos);
        let spawn = cc.spawn(scaleAction, moveAction);
        capture.runAction(spawn);
        let blinkAction = cc.blink(0.1, 1);
        // scene action
        this.node.runAction(blinkAction);
    }

    saveFile(picData) {
        if (CC_JSB) {
            // let filePath = jsb.fileUtils.getWritablePath() + 'render_to_sprite_image.png';

            // let success = jsb.saveImageData(picData, this._width, this._height, filePath)
            // if (success) {
            //     cc.log("save image data success, file: " + filePath);
            // }
            // else {
            //     cc.error("save image data failed!");
            // }
        }
    }
}
