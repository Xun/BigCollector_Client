
import UIFriendGive_Auto from "../../AutoScripts/UIFriendGive_Auto";
import { UIWindow } from "../../Common/UIForm";
import { apiClient } from "../../Net/RpcConent";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIFriendGive extends UIWindow {

    view: UIFriendGive_Auto;
    private _userId: string = "";
    private _num: string = "";

    // onLoad () {}

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);
        this.view.BtnSerch.addClick(() => {
            this.onSerch();
        }, this);
        this.view.BtnGive.addClick(() => {
            this.onGive();
        }, this);
        this.view.IDBox.node.on('text-changed', this.onInputID, this);
        this.view.GiveBox.node.on('text-changed', this.onInputNum, this)
    }

    onInputID(event) {
        this._userId = event.string;
    }

    onInputNum(event) {
        this._num = event.string;
    }
    async onSerch() {
        let data = await apiClient.callApi("SerchFriend", { id: this._userId });
        if (!data.isSucc) {
            UIToast.popUp(data.err.message);
            return;
        }
        this.view.NameLab.string = data.res.serchInfo.name;
        CocosHelper.loadHead(data.res.serchInfo.url, this.view.HeadSP);
    }

    async onGive() {
        let data = await apiClient.callApi("GiveFriend", { id: this._userId, num: this._num });
        if (!data.isSucc) {
            UIToast.popUp(data.err.message);
            return;
        }
        UIToast.popUp("赠送成功");
    }
}
