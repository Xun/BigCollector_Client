// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIFriendGiveDetail_Auto from "../../AutoScripts/UIFriendGiveDetail_Auto";
import { UIWindow } from "../../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIFriendGiveDetail extends UIWindow {

    view: UIFriendGiveDetail_Auto;

    // onLoad () {}

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
    }

    // update (dt) {}
}
