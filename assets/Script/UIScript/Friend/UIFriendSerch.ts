// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIFriendSerch_Auto from "../../AutoScripts/UIFriendSerch_Auto";
import { UIWindow } from "../../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIFriendSerch extends UIWindow {

    view: UIFriendSerch_Auto;

    // onLoad () {}

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);
    }

    // update (dt) {}
}
