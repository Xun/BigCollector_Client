
import UISetSocial_Auto from "../../AutoScripts/UISetSocial_Auto";
import { UIWindow } from "../../Common/UIForm";
import { apiClient } from "../../Net/RpcConent";
import UIToast from "../UIToast";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UISetSocial extends UIWindow {

    view: UISetSocial_Auto;
    private _wechat: string = "";
    private _qq: string = "";
    // onLoad () {}

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.SureBtn.addClick(() => {
            this.sure();
        }, this);
        this.view.Wechat.node.on('text-changed', this.onWechat, this);
        this.view.QQ.node.on('text-changed', this.onQQ, this);
    }

    onWechat(event) {
        this._wechat = event.string;
    }

    onQQ(event) {
        this._qq = event.string;
    }

    async sure() {
        let data = await apiClient.callApi("SocialSet", { weChat: this._wechat, QQ: this._qq });
        if (!data.isSucc) {
            UIToast.popUp(data.err.message);
            return;
        }
        this.closeSelf();
        UIToast.popUp("设置成功");
    }
}
