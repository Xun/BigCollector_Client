// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ButtonPlus from "../../Common/Components/ButtonPlus";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemFriendSee extends cc.Component {

    @property(cc.Label)
    user_name: cc.Label = null;

    @property(cc.Label)
    user_id: cc.Label = null;

    @property(cc.Sprite)
    user_head: cc.Sprite = null;

    @property(ButtonPlus)
    see_btn: ButtonPlus = null;

    start() {
        this.see_btn.addClick(() => {
            console.log("baifang");
        }, this);
    }

    setData() {

    }
}
