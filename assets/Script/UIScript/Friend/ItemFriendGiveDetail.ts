
import ButtonPlus from "../../Common/Components/ButtonPlus";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemFriendGiveDetail extends cc.Component {

    @property(cc.Label)
    uname: cc.Label = null;

    @property(cc.Label)
    uid: cc.Label = null;

    @property(cc.Label)
    utime: cc.Label = null;

    @property(cc.Sprite)
    uhead: cc.Sprite = null;

    @property(cc.Label)
    givenum: cc.Label = null;


    start() {

    }

    setData() {

    }
}
