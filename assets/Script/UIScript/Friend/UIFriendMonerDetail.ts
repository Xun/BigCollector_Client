

import UIFriendMonerDetail_Auto from "../../AutoScripts/UIFriendMonerDetail_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import GameMgr from "../../Manager/GameMgr";
import { apiClient } from "../../Net/RpcConent";
import ItemFriendExtraDetail from "./ItemFriendExtraDetail";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIFriendMonerDetail extends UIWindow {

    view: UIFriendMonerDetail_Auto;

    @property(ListUtil)
    listFriendMonerDetail: ListUtil = null;


    async start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);
        let data = await apiClient.callApi("FriendEarningsDetail", {});
        if (!data.isSucc) {
            this.listFriendMonerDetail.numItems = 0;
            this.view.TodayGetMoneyLab.string = "0";
            this.view.TotalGetMoneyLab.string = "0";
            return;
        }
        this.view.TodayGetMoneyLab.string = data.res.todayEranings.toString();
        this.view.TotalGetMoneyLab.string = data.res.totalEranings.toString();
        GameMgr.dataModalMgr.FriendMonerDetailInfo.list_item_friend_money_detail = data.res.eraningsDetailList;
        this.listFriendMonerDetail.numItems = data.res.eraningsDetailList.length;
    }

    //垂直列表渲染器
    onListShopRender(item: cc.Node, idx: number) {
        item.getComponent(ItemFriendExtraDetail).setData(GameMgr.dataModalMgr.FriendMonerDetailInfo.list_item_friend_money_detail[idx]);
    }
}
