
import UIFriend_Auto from "../../AutoScripts/UIFriend_Auto";
import { UIWindow } from "../../Common/UIForm";
import FormMgr from "../../Manager/FormMgr";
import GameMgr from "../../Manager/GameMgr";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import UIFriendSee from "./UIFriendSee";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIFriend extends UIWindow {

    view: UIFriend_Auto;

    // onLoad () {}

    async start() {
        this.view.BtnInvoedFriend.addClick(() => {
            FormMgr.open(UIConfig.UIShare);
        }, this);

        this.view.BtnMyFriend.addClick(() => {
            FormMgr.open(UIConfig.UIMyFriend);
        }, this);

        this.view.ProbablyDetail.addClick(() => {
            FormMgr.open(UIConfig.UIFriendMonerDetail);
        }, this);

        this.view.UpChannel.addClick(() => {
            FormMgr.open(UIConfig.UIExtarCommitSure);
        }, this);

        this.view.Call.addClick(() => {
            FormMgr.open(UIConfig.UIInviteGetInfo);
        }, this);

        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.BtnGive.addClick(() => {
            FormMgr.open(UIConfig.UIFriendGive);
        }, this);

        this.view.BtnVisit.addClick(() => {
            FormMgr.open(UIConfig.UIFriendSee);
        }, this);

        let data = await apiClient.callApi("Earnings", {});
        if (!data.isSucc) {
            UIToast.popUp(data.err.message);
        }

        this.view.TodayGetMoneyLab.string = data.res.todayEranings.toString();
        this.view.TotalGetMoneyLab.string = data.res.totalEarnings.toString();
        this.view.Name.string = data.res.myParentInfo.name;
        CocosHelper.loadHead(data.res.myParentInfo.url, this.view.HeadSp);

        this.view.MyFriendNumLab.string = "我的好友：" + GameMgr.dataModalMgr.FriendInfo.my_friend_num + "人";
        this.view.ProbablyMoneyLab.string = "预计每个好友每天为你产出" + GameMgr.dataModalMgr.FriendInfo.probabyly_money + "元";
        // this.view.TodayGetMoneyLab.string = GameMgr.dataModalMgr.FriendInfo.today_get_money + "元";
        // this.view.TotalGetMoneyLab.string = GameMgr.dataModalMgr.FriendInfo.total_get_money + "元";
        this.view.TodayExtarGetMoneyLab.string = GameMgr.dataModalMgr.FriendInfo.today_extar_get_money + "元";
        this.view.TodayExtarGetMoneyTipsLab.string = "累计获得8000元后成为渠道，当前已获得" + GameMgr.dataModalMgr.FriendInfo.cur_get_money + "元";
        // this.view.Name.string = GameMgr.dataModalMgr.FriendInfo.my_inviter_name;
        // CocosHelper.loadHead(GameMgr.dataModalMgr.FriendInfo.my_invite_head, this.view.HeadSp);
    }

}
