
import UIFriendSee_Auto from "../../AutoScripts/UIFriendSee_Auto";
import { UIWindow } from "../../Common/UIForm";
import FormMgr from "../../Manager/FormMgr";
import UIConfig from "../../UIConfig";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIFriendSee extends UIWindow {

    view: UIFriendSee_Auto;

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.InviteBtn.addClick(() => {
            FormMgr.open(UIConfig.UIShare);
        }, this);

        this.view.AddFriendBtn.addClick(() => {
            FormMgr.open(UIConfig.UIFriendSerch);
        }, this);

        this.view.SeeDetailBtn.addClick(() => {
            FormMgr.open(UIConfig.UIFriendSeeDetail);
        }, this);
    }

}
