import { DataItemFriendMoneyDetail } from "../../DataModal/DataFriend";
import CocosHelper from "../../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemFriendExtraDetail extends cc.Component {

    @property(cc.Label)
    data_lab: cc.Label = null;

    @property(cc.Label)
    friend_one_lab: cc.Label = null;

    @property(cc.Label)
    friend_two_lab: cc.Label = null;

    @property(cc.Label)
    total_lab: cc.Label = null;

    setData(data: DataItemFriendMoneyDetail) {
        this.data_lab.string = CocosHelper.changeTime(data.time);
        this.friend_one_lab.string = data.oneFriendEranings + "元";
        this.friend_two_lab.string = data.twoFriendEranings + "元";
        this.total_lab.string = data.todayTotalEranings + "元";
    }

}
