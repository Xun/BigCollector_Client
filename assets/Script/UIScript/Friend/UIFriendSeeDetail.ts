
import UIFriendSeeDetail_Auto from "../../AutoScripts/UIFriendSeeDetail_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import CocosHelper from "../../Utils/CocosHelper";
import ItemFriendSeeDetail from "./ItemFriendSeeDetail";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIFriendSeeDetail extends UIWindow {

    view: UIFriendSeeDetail_Auto;

    @property(ListUtil)
    list_see_detail: ListUtil = null;

    onLoad() {
        // this.list_see_detail.numItems
    }

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
    }

    //垂直列表渲染器
    onListRankRender(item: cc.Node) {
        // item.getComponent(ItemFriendSeeDetail).setData();
    }

}
