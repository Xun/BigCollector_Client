// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ButtonPlus from "../../Common/Components/ButtonPlus";
import { DataItemSeeDetail } from "../../DataModal/DataFriend";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemFriendSeeDetail extends cc.Component {

    @property(cc.Label)
    uname: cc.Label = null;

    @property(cc.Label)
    uid: cc.Label = null;

    @property(cc.Label)
    utime: cc.Label = null;

    @property(cc.Sprite)
    uhead: cc.Sprite = null;

    @property(ButtonPlus)
    seeBtn: ButtonPlus = null;

    start() {
        this.seeBtn.addClick(() => {
            console.log("huifang");
        }, this);
    }

    setData(data: DataItemSeeDetail) {

    }
}
