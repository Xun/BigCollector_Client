// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIInviteGetInfo_Auto from "../../AutoScripts/UIInviteGetInfo_Auto";
import { UIWindow } from "../../Common/UIForm";
import FormMgr from "../../Manager/FormMgr";
import UIConfig from "../../UIConfig";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIInviteGetInfo extends UIWindow {

    view: UIInviteGetInfo_Auto;

    // onLoad () {}

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.SetInfoBtn.addClick(() => {
            FormMgr.open(UIConfig.UISetSocial);
        }, this);

        this.view.CopyQQBtn.addClick(() => {
            console.log("qq");
        }, this);

        this.view.CopyWechatBtn.addClick(() => {
            console.log("wechat");
        }, this);
    }

}
