
import UIDialog_Auto from "../AutoScripts/UIDialog_Auto";
import { ModalType } from "../Common/Struct";
import { ModalOpacity } from "../Common/SysDefine";
import { UIWindow } from "../Common/UIForm";


const { ccclass, property } = cc._decorator;

@ccclass
export default class UIDialog extends UIWindow {

    modalType = new ModalType(ModalOpacity.OpacityHalf, true);

    view: UIDialog_Auto;

    onLoad() {
        this.view.Sure.addClick(() => {
            this.closeSelf();
        }, this)
    }

    onShow(str: string) {
        this.view.Msg.string = str;
    }

}
