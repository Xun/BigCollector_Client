import { GameServer, UserInfo } from "../../../Protocol/bundle";
import UIHome_Auto from "../../AutoScripts/UIHome_Auto";
import { UIScreen } from "../../Common/UIForm";
import { OperationType } from "../../DataModal/DataUser";
import AdapterMgr, { AdapterType } from "../../Manager/AdapterMgr";
import { DataModalMgr } from "../../Manager/DataModalMgr";
import FormMgr from "../../Manager/FormMgr";
import GameMgr from "../../Manager/GameMgr";
import { NativeMgr } from "../../Manager/NativeMgr";
import SoundMgr from "../../Manager/SoundMgr";
import TipsMgr from "../../Manager/TipsMgr";
import { EventCenter } from "../../Net/EventCenter";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import ItemProduce from "../Items/ItemProduce";
import UIRank from "../Rank/UIRank";
import UIToast from "../UIToast";
const TAG_VIRTUAL_MOVE = 10001;  //虚拟道具
const SPEED_VIRUTAL_MOVE = 2000;
const { ccclass, property } = cc._decorator;

@ccclass
export default class UIHome extends UIScreen {

    view: UIHome_Auto;

    @property(cc.Prefab)
    itemProducePrefab: cc.Prefab = null;

    //是否自动合成
    private _isCombining: boolean = false;
    //是否可以拖拽，当前是否在使用
    private _isCanDrag: boolean = false;
    //当前拖拽节点
    private _currentNode = null;
    //拖拽时的虚拟节点
    private _virtualNode = null;
    //拖拽偏移位置
    private _offsetPos: cc.Vec2;
    private item_pos_arr: cc.Vec2[];
    async load() {
        SoundMgr.inst.playMusic("Sounds/bg");
        return null;
    }

    start() {
        // if (AdapterMgr.inst.checkIsIphoneX()) {
        //     AdapterMgr.inst.adapteByType(AdapterType.Top, this.view.topNode, 20);
        // }
        apiClient.listenMsg("LvUp", (data) => {
            this.onLvUp(data);
        });
        apiClient.listenMsg("UpdateUserInfo", (data) => {
            this.upDataUserInfo(data);
        });
        this.item_pos_arr = [new cc.Vec2(0, 360), new cc.Vec2(-175, 270), new cc.Vec2(175, 270), new cc.Vec2(0, 180), new cc.Vec2(-175, 90), new cc.Vec2(175, 90), new cc.Vec2(0, 0), new cc.Vec2(-175, -90), new cc.Vec2(175, -90), new cc.Vec2(0, -180), new cc.Vec2(-175, -270), new cc.Vec2(175, -270)]
        this.initEvent();
        this.initData();
        this.initClickEvent();
        this.initWorkbench();

        // GameMgr.client.on("user_info_modified", async (payload) => {
        //     GameMgr.dataModalMgr.UserInfo.userInfos = UserInfo.decode(payload);
        // })
        // let s: string = NativeMgr.getStr("OneClass", "FucTwo");
        // console.log("sssss:" + s);
        // this.adaptiveNoteLayout();
        // this.scheduleOnce(() => {
        //     EventCenter.emit("shouguide", CocosHelper.findChildInNode("_ButtonPlus$SettingBtn", this.node));
        // }, 1)
    }

    upDataUserInfo(data: any) {
        this.view.CoinTxt.string = data.coin.toString();
        this.view.DiamondTxt.string = data.diamond.toString();
    }

    onLvUp(datas) {
        if (datas.type === 1) {
            FormMgr.open(UIConfig.UIUnlockNewLevel, datas);
        }

    }

    adaptiveNoteLayout() {
        let winSize = cc.winSize;//获取当前游戏窗口大小
        cc.log("--当前游戏窗口大小  w:" + winSize.width + "   h:" + winSize.height);

        let viewSize = cc.view.getFrameSize();
        cc.log("--视图边框尺寸：w:" + viewSize.width + "  h:" + viewSize.height);

        let canvasSize = cc.view.getCanvasSize();//视图中canvas尺寸
        cc.log("--视图中canvas尺寸  w:" + canvasSize.width + "  H:" + canvasSize.height);

        let visibleSize = cc.view.getVisibleSize();
        cc.log("--视图中窗口可见区域的尺寸 w:" + visibleSize.width + "   h:" + visibleSize.height);

        let designSize = cc.view.getDesignResolutionSize();
        cc.log("--设计分辨率：" + designSize.width + "    h: " + designSize.height);

        cc.log("--当前节点的尺寸 w:" + this.node.width + "   h:" + this.node.height);
    }
    //初始化点击事件
    initEvent() {
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    //初始化点击事件
    initClickEvent() {
        this.view.BuyBtn.addClick(this.onBuyProduceClick, this);
        this.view.CombineAutoBtn.addClick(this.onBtnCombineAutoClick, this);
        this.view.BagBtn.addClick(() => {
            FormMgr.open(UIConfig.UIBag);
            // NativeMgr.callNativeClass("OneClass", "FucOne", "test01", 11111, true, (str, num, b) => { console.log(str, num, b); })
        }, this);
        // this.view.SettingBtn.addClick(() => {
        // EventCenter.emit("shouguide", CocosHelper.findChildInNode("_ButtonPlus$BagBtn", this.node));
        // }, this);
        this.view.SignInBtn.addClick(() => {
            FormMgr.open(UIConfig.UISignIn);
        }, this);
        this.view.DrawBtn.addClick(() => {
            FormMgr.open(UIConfig.UILottery);
        }, this);
        this.view.RankBtn.addClick(() => {
            FormMgr.open(UIConfig.UIRank);
        }, this);
        this.view.CollectBtn.addClick(() => {
            FormMgr.open(UIConfig.UICollection);
        }, this);
        this.view.ShopBtn.addClick(() => {
            FormMgr.open(UIConfig.UIShop);
            // FormMgr.open(UIConfig.UIDeal)
        }, this);
        this.view.MyBtn.addClick(() => {
            FormMgr.open(UIConfig.UIMy);
        }, this);
        this.view.TaskBtn.addClick(() => {
            FormMgr.open(UIConfig.UITask);
        }, this);
        this.view.FriendBtn.addClick(() => {
            FormMgr.open(UIConfig.UIFriend)
        }, this);
    }

    //初始化显示数据
    initData() {
        // CocosHelper.loadHead("https://pic.sogou.com/d?query=%E5%A4%B4%E5%83%8F&forbidqc=&entityid=&preQuery=&rawQuery=&queryList=&st=191&did=5", this.view.HeadSp);
        // GameMgr.dataModalMgr.UserInfo.userWorkbench = [1, 1, 2, 3, 4, 4, 5, 5, 6, 7];
        this.view.BuyCoinNumTxt.string = GameMgr.dataModalMgr.UserInfo.userCurNeedBuyCoinNum.toString();
        this.view.MakeMoneyTxt.string = GameMgr.dataModalMgr.UserInfo.userMakeCoin.toString();
        this.view.CoinTxt.string = GameMgr.dataModalMgr.UserInfo.userCoin.toString();
        this.view.DiamondTxt.string = GameMgr.dataModalMgr.UserInfo.userDiamond.toString();
        this.view.MoneyTxt.string = GameMgr.dataModalMgr.UserInfo.userMoney.toString();
    }

    //初始化工作台信息
    initWorkbench() {
        let arrChildren = this.view.WorkContent.children;
        if (arrChildren.length > 0) {
            //已经创建过，不需要再创建
            return;
        }

        for (let idx = 0; idx < GameMgr.dataModalMgr.UserInfo.userWorkbench.length; idx++) {
            let item_node = cc.instantiate(this.itemProducePrefab);
            this.view.WorkContent.addChild(item_node);
            item_node.setPosition(this.item_pos_arr[idx]);
        }

        this.view.WorkContent.children.forEach((itemNode, index) => {
            let itemInfo = itemNode.getComponent('ItemProduce');
            if (index < GameMgr.dataModalMgr.UserInfo.userWorkbench.length) {
                itemInfo.setWorkbenchItemInfo(index, GameMgr.dataModalMgr.UserInfo.userWorkbench[index]);
            }
        }, this);
    }

    onTouchStart(touchEvent: cc.Touch) {
        this._currentNode = this.getCurrentNodeByTouchPos(touchEvent.getLocation());
        if (this._currentNode) {
            let ItemProduce = this._currentNode.getComponent('ItemProduce');
            this._isCanDrag = ItemProduce.dragStart();
            if (this._isCanDrag) {
                let currentWorldPos = this._currentNode.convertToWorldSpaceAR(cc.v2(0, 0));
                this._offsetPos = currentWorldPos.sub(touchEvent.getLocation());
                this.showVirtualItem(ItemProduce.getInfo(), touchEvent.getLocation());
                this.showProduce(ItemProduce);
            }
        }
    }

    onTouchMove(touchEvent: cc.Touch) {
        if (!this._isCanDrag) {
            return;
        }

        this.updateVirutalItemPos(touchEvent.getLocation());
    }

    onTouchEnd(touchEvent: cc.Touch) {
        if (!this._currentNode) {
            return;
        }

        this.hideProduce();
        let cakeItem = this._currentNode.getComponent('ItemProduce');
        cakeItem.dragOver();
        let touchEndNode = this.getCurrentNodeByTouchPos(touchEvent.getLocation());
        if (this._currentNode === touchEndNode) {
            //属于点击自己
            this.hideVirtualItem(true);
            this._currentNode = null;
            return;
        }

        if (!this._isCanDrag) {
            return;
        }

        if (!touchEndNode) {
            //TODO 判断是否拖拽到生产台上，没有则是拖拽到无效位置
            let isDragToRecovery = this.checkIsDragToRecovery(cakeItem._index, touchEvent.getLocation());
            if (!isDragToRecovery) {
                this.hideVirtualItem(true);
            } else {
                this.hideVirtualItem(false);
            }
        } else {
            //判断是交换位置还是与之合成
            this.combinOrSwap(this._currentNode, touchEndNode);
        }

        this._currentNode = null;
        this._isCanDrag = false;
    }

    onTouchCancel(touchEvent: cc.Touch) {
        if (!this._currentNode) {
            return;
        }

        let ItemProduce = this._currentNode.getComponent('ItemProduce');

        if (!ItemProduce._itemId) {
            return;
        }

        this.hideProduce();

        ItemProduce.dragOver();
        if (ItemProduce.isUsed) {
            this._currentNode = null;
            return;
        }

        //检查下是否拖拽到回收站了
        let isDragToRecovery = this.checkIsDragToRecovery(ItemProduce._index, touchEvent.getLocation());
        if (!isDragToRecovery) {
            //拖到了无效位置
            this.hideVirtualItem(true);
        } else {
            //发起回收的请求,此处已由recovery接收走
            this.hideVirtualItem(false);
        }

        this._currentNode = null;
    }

    getCurrentNodeByTouchPos(pos: cc.Vec2) {
        let arrNode = this.view.WorkContent.children;
        return arrNode.find((itemNode, index) => {
            var box = itemNode.getBoundingBoxToWorld();
            if (box.contains(pos)) {
                return true;
            }
        }, this);
    }

    /**
     * 拖拽时原物体半透明，然后拖拽的其实是一个虚拟的半透明物体
     */
    showVirtualItem(info: any, pos: cc.Vec2) {
        if (!this._virtualNode) {
            this._virtualNode = cc.instantiate(this.itemProducePrefab);
            this._virtualNode.parent = this.node.parent;
        } else {
            this._virtualNode.active = true;
        }

        this._virtualNode.stopActionByTag(TAG_VIRTUAL_MOVE);
        let cakeItem = this._virtualNode.getComponent('ItemProduce');
        cakeItem.setWorkbenchItemInfo(-1, info);
        cakeItem.showVirtual();
        this.updateVirutalItemPos(pos);
    }

    /**
     * 隐藏拖拽时的半透明图标
     */
    hideVirtualItem(isGoBack: boolean) {
        if (this._virtualNode && this._virtualNode.active) {
            if (isGoBack) {
                // this.currentNode.converttow
                let posWorld = this._currentNode.convertToWorldSpaceAR(cc.v2);
                let posCurrentNode = this.node.parent.convertToNodeSpaceAR(posWorld);
                let duration = posCurrentNode.sub(this._virtualNode.position).mag() / SPEED_VIRUTAL_MOVE;
                CocosHelper.runTweenSync(this._virtualNode, cc.tween().to(0, duration).call(() => {
                    this._virtualNode.active = false;
                }))
            } else {
                this._virtualNode.active = false;
            }
        }
    }

    /**
     * 更新拖拽时的半透明图标随手指移动位置
     */
    updateVirutalItemPos(pos: cc.Vec2) {
        if (this._virtualNode && this._virtualNode.active) {
            //需要坐标转换
            var posNodeSpace = this.node.parent.convertToNodeSpaceAR(pos);
            this._virtualNode.position = posNodeSpace.add(this._offsetPos);
            this.updateDragPos(pos);
        }
    }

    updateDragPos(pos: cc.Vec2) {
        let box = this.view.RecoveryBtn.node.getBoundingBoxToWorld();
        if (!box.contains(pos)) {
            if (this.view.RecoveryBtn.node.scale === 1.1) {
                this.view.RecoveryBtn.node.scale = 1;
            }
            return false;
        }

        if (this.view.RecoveryBtn.node.scale === 1) {
            this.view.RecoveryBtn.node.scale = 1.1;
        }
    }

    //检查是否拖到回收站了
    async checkIsDragToRecovery(workbenchIdx: number, pos: cc.Vec2) {
        let box = this.view.RecoveryBtn.node.getBoundingBoxToWorld();

        if (!box.contains(pos)) {
            return false;
        }
        let datas = await apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr.dataModalMgr.UserInfo.userAccount, ctrlType: OperationType.sell, curPos: workbenchIdx })
        if (!datas.isSucc) {
            UIToast.popUp(datas.err.message);
            return;
        } else {
            this.updateWorkbench(workbenchIdx, -1, "Recovery");
            return true;
        }
    }

    //点击拖动时播放相同等级物品的动画
    showProduce(itemProduce: ItemProduce) {
        let index = itemProduce._index;
        let itemId = itemProduce._itemId;
        let arrNode = this.view.WorkContent.children;

        for (let idx = 0; idx < arrNode.length; idx++) {
            if (idx === index) {
                continue;
            }

            let produce = arrNode[idx].getComponent('ItemProduce');
            if (!produce.isUsed && produce.itemId === itemId) {
                // produce.playProduceAni(true);
            }
        }
    }

    //隐藏拖拽时的图标
    hideProduce() {
        let arrNode = this.view.WorkContent.children;
        for (let idx = 0; idx < arrNode.length; idx++) {
            let produce = arrNode[idx].getComponent('ItemProduce');
            if (!produce.isUsed) {
                // produce.playProduceAni(false);
            }
        }
    }

    /**
     * 判断是交换还是结合
     */
    async combinOrSwap(originNode: cc.Node, targetNode: cc.Node) {
        let originScript = originNode.getComponent('ItemProduce');
        let targetScript = targetNode.getComponent('ItemProduce');
        if (originScript.isUsed || targetScript.isUsed) {
            this.hideVirtualItem(true);
            return;
        }

        let originItem = GameMgr.dataModalMgr.UserInfo.userWorkbench[originScript._index];
        let targetItem = GameMgr.dataModalMgr.UserInfo.userWorkbench[targetScript._index];
        if (originItem == null || targetItem == null) {
            return false;
        }
        let datas = await apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr.dataModalMgr.UserInfo.userAccount, ctrlType: OperationType.concat, curPos: originScript._index, target: targetScript._index })
        if (!datas.isSucc) {
            UIToast.popUp("数据错误");
            this.updateWorkbench(originScript._index, targetScript._index, "Exchange");
            return;
        }
        if (originScript._itemId === targetScript._itemId) {
            this.combinProduce(originScript._index, targetScript._index, datas.res.userWorkBench[targetScript._index]);
        } else {
            //位置交换
            console.log("jiaohuan")
            if (this.swapProduce(originScript._index, targetScript._index)) {
                this.updateWorkbench(originScript._index, targetScript._index, "Exchange");
            }
        }
        this.hideVirtualItem(false);
    }

    //更新工作台
    updateWorkbench(originIndex: number, targetIndex: number, type: string) {
        if (!type) {
            this.initWorkbench();
            return;
        }

        let originInfo: ItemProduce;
        let targetInfo: ItemProduce;
        if (originIndex >= 0) {
            let originNode = this.view.WorkContent.children[originIndex];
            originInfo = originNode.getComponent('ItemProduce');
        }
        if (targetIndex >= 0) {
            let targetNode = this.view.WorkContent.children[targetIndex];
            targetInfo = targetNode.getComponent('ItemProduce');
        }

        switch (type) {
            case "Exchange":
                console.log("交换")
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                originInfo.setWorkbenchItemInfo(originInfo._index, GameMgr.dataModalMgr.UserInfo.userWorkbench[originInfo._index]);
                break;
            case "Combine":
                console.log("组合");
                originInfo.setWorkbenchItemInfo(originInfo._index, GameMgr.dataModalMgr.UserInfo.userWorkbench[originInfo._index]);
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                break;
            case "Produce":
                console.log("生产");
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                break;
            case "Recovery":
                console.log("回收");
                originInfo.setWorkbenchItemInfo(originInfo._index, 0);
                GameMgr.dataModalMgr.UserInfo.userWorkbench[originInfo._index] = 0;
                break;
        }

    }
    /**
     * 交换生产物位置
     * @param {number} originIndex 
     * @param {number} targetIndex 
     */
    swapProduce(originIndex: number, targetIndex: number) {
        if (!GameMgr.dataModalMgr.UserInfo.userWorkbench) {
            return false;
        }

        if (originIndex >= GameMgr.dataModalMgr.UserInfo.userWorkbench.length || targetIndex >= GameMgr.dataModalMgr.UserInfo.userWorkbench.length) {
            return false;
        }

        let originItemId = GameMgr.dataModalMgr.UserInfo.userWorkbench[originIndex];
        let targetItemId = GameMgr.dataModalMgr.UserInfo.userWorkbench[targetIndex];

        GameMgr.dataModalMgr.UserInfo.userWorkbench[targetIndex] = originItemId;
        GameMgr.dataModalMgr.UserInfo.userWorkbench[originIndex] = targetItemId;

        return true;
    }

    /**
     * 生产物合并
     * @param {Number} originIndex 原位置的index 
     * @param {Number} targetIndex 目标位置的index
     * @param {String} itemId 物品id
     */
    combinProduce(originIndex: number, targetIndex: number, itemId: number) {
        let originNode = this.view.WorkContent.children[originIndex];
        let targetNode = this.view.WorkContent.children[targetIndex];

        let originScript = originNode.getComponent('ItemProduce');
        let targetScript = targetNode.getComponent('ItemProduce');

        originScript.isUsed = true; //要结合，先将两个生产物置为不可拖拽
        targetScript.isUsed = true;

        GameMgr.dataModalMgr.UserInfo.userWorkbench[originIndex] = 0;
        GameMgr.dataModalMgr.UserInfo.userWorkbench[targetIndex] = itemId;

        this.updateWorkbench(originScript._index, targetScript._index, "Combine");
    }

    //购买事件
    async onBuyProduceClick() {
        let isEnough = GameMgr.dataModalMgr.UserInfo.userCoin >= GameMgr.dataModalMgr.CurBuyProduceInfo.buyPrice;
        if (!isEnough) {
            UIToast.popUp("金币不足！");
            return;
        }
        if (!GameMgr.dataModalMgr.UserInfo.hasPosAtWorkbench()) {
            UIToast.popUp("请先出售多余物品！");
            return;
        }

        let index = GameMgr.dataModalMgr.UserInfo.checkNullPos();
        let datas = await apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr.dataModalMgr.UserInfo.userAccount, ctrlType: OperationType.buy, curPos: index })
        if (!datas.isSucc) {
            UIToast.popUp(datas.err.message);
            return;
        }
        GameMgr.dataModalMgr.UserInfo.userWorkbench[index] = datas.res.userWorkBench[index];
        this.updateWorkbench(-1, index, "Produce");
    }

    //自动合成
    onBtnCombineAutoClick() {
        if (this._isCombining) {
            UIToast.popUp("正在自动合成中");
            return;
        }
        let at = GameMgr.dataModalMgr.UserInfo.combineAutoTime;
        if (!at || at <= 0) {
            //停止自动合成
            this.scheduleCombineOver();
            return;
        }

        if (!this._isCombining) {
            this.checkIsCombineAuto();
        }
    }

    //检查是否正在自动合成
    checkIsCombineAuto() {
        let spareTime = GameMgr.dataModalMgr.UserInfo.combineAutoTime;
        if (!spareTime || spareTime <= 0) {
            this.scheduleCombineOver();
            return;
        }
        this._isCombining = true;
        this.schedule(this.combineAuto, 1);
        this.view.CombineAutoTxt.string = CocosHelper.formatTimeForSecond(15);
        this.view.CombineAutoTxt.node.active = true;
    }

    /**
     * 
     * 自动合成 
     */
    combineAuto() {
        GameMgr.dataModalMgr.UserInfo.combineAutoTime -= 1;
        if (GameMgr.dataModalMgr.UserInfo.combineAutoTime <= 0) {
            this.scheduleCombineOver();
        }
        this.view.CombineAutoTxt.string = CocosHelper.formatTimeForSecond(GameMgr.dataModalMgr.UserInfo.combineAutoTime);

        let arrSort = [];
        for (let idx = 0; idx < GameMgr.dataModalMgr.UserInfo.userWorkbench.length; idx++) {
            let cakeNode = this.view.WorkContent.children[idx];
            let cakeItem = cakeNode.getComponent('ItemProduce');
            let isUsed = cakeItem.isUsed;
            if (isUsed) {
                continue;
            }

            let itemId = GameMgr.dataModalMgr.UserInfo.userWorkbench[idx];
            if (itemId === 0) {
                continue; //礼盒或者是空位置要屏蔽掉
            }

            arrSort.push({ itemId: itemId, index: idx });
        }

        //按照蛋糕id进行排序
        arrSort.sort((a, b) => {
            return Number(a.itemId) - Number(b.itemId);
        });
        let originIndex = -1;
        let targetIndex = -1;

        for (let idx = 0; idx < arrSort.length - 1; idx++) {
            if (arrSort[idx].itemId === arrSort[idx + 1].itemId && arrSort[idx].itemId.toString() !== "38") { //排除最高等级，避免最高等级影响后续合成
                //找到等级最低的蛋糕了
                targetIndex = arrSort[idx].index;
                originIndex = arrSort[idx + 1].index;
                break;
            }
        }

        if (originIndex !== -1 && targetIndex !== -1) {
            if (this._currentNode) {
                let cakeItem = this._currentNode.getComponent('ItemProduce');
                if (cakeItem.index === originIndex || cakeItem.index === targetIndex) {
                    cakeItem.dragOver();
                    this.hideProduce();
                    this._currentNode = null;
                    this.hideVirtualItem(false);
                }
            }

            this.combinOrSwap(this.view.WorkContent.children[originIndex], this.view.WorkContent.children[targetIndex]);
        }
    }

    /**
     * 关闭合成倒计时
     */
    scheduleCombineOver() {
        this._isCombining = false;
        this.view.CombineAutoTxt.node.active = false;
        this.unschedule(this.combineAuto);
    }

}