
import UIUnlockNewLevel_Auto from "../../AutoScripts/UIUnlockNewLevel_Auto";
import { ModalType } from "../../Common/Struct";
import { ModalOpacity } from "../../Common/SysDefine";
import { UIWindow } from "../../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIUnlockNewLevel extends UIWindow {


    view: UIUnlockNewLevel_Auto;
    // onLoad () {}
    modalType = new ModalType(ModalOpacity.OpacityHigh);
    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
    }

    public onShow(params: any): void {
        this.view.NewLevelLab.string = "等级" + params.lv;
        this.view.YinPiaoNumLab.string = "银票" + params.money;

    }

}
