// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIGetMoneyLog_Auto from "../../AutoScripts/UIGetMoneyLog_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import { DataItemGetMoneyLog } from "../../DataModal/DataGetMoneyLog";
import GameMgr from "../../Manager/GameMgr";
import ItemGetMoneyLog from "./ItemGetMoneyLog";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIGetMoneyLog extends UIWindow {

    view: UIGetMoneyLog_Auto;
    @property(ListUtil)
    list_log: ListUtil = null;

    // onLoad () {}

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
    }

    //垂直列表渲染器
    onListShopRender(item: cc.Node, idx: number) {
        item.getComponent(ItemGetMoneyLog).setData(GameMgr.dataModalMgr.DataGetMoneyLogInfo.list_get_money_log[idx]);
    }
}
