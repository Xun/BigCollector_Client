// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIMyFriend_Auto from "../../AutoScripts/UIMyFriend_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import { DataItemFriendInfo } from "../../DataModal/DataFriend";
import GameMgr from "../../Manager/GameMgr";
import { apiClient } from "../../Net/RpcConent";
import CocosHelper from "../../Utils/CocosHelper";
import ItemFriendSee from "../Friend/ItemFriendSee";
import UIToast from "../UIToast";
import ItemFriend from "./ItemFriend";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIMyFriend extends UIWindow {

    view: UIMyFriend_Auto;
    @property(ListUtil)
    listFriend: ListUtil = null;

    listData: DataItemFriendInfo[];
    onLoad() {
        this.reflashListData();
    }

    start() {
        this.view.RankToggleNode.children.forEach((item) => {
            item.on(cc.Node.EventType.TOUCH_END, (e: cc.Event.EventTouch) => {
                this.reflashListData();
            }, this)
        })

        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
    }

    async reflashListData() {
        let index = CocosHelper.getContenerindex(this.view.RankToggleNode);
        index += 1;
        let datas = await apiClient.callApi("Linkedln", { userAccount: GameMgr.dataModalMgr.UserInfo.userAccount, friendType: index });
        if (datas.isSucc) {
            this.listFriend.numItems = datas.res.friendArray.length;
            this.listData = datas.res.friendArray;
        } else {
            UIToast.popUp(datas.err.message);
        }
    }

    //垂直列表渲染器
    onListRankRender(item: cc.Node, idx: number) {
        item.getComponent(ItemFriend).setData(this.listData[idx]);
    }
}
