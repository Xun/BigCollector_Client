// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { DataItemFriendInfo } from "../../DataModal/DataFriend";
import CocosHelper from "../../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemFriend extends cc.Component {

    @property(cc.Label)
    user_name: cc.Label = null;

    @property(cc.Label)
    user_time: cc.Label = null;

    @property(cc.Sprite)
    user_head: cc.Sprite = null;

    @property(cc.Label)
    user_level: cc.Label = null;

    setData(data: DataItemFriendInfo) {
        this.user_name.string = data.user_name;
        this.user_level.string = data.user_level;
        this.user_time.string = data.add_time;
        CocosHelper.loadHead(data.user_head, this.user_head);
    }
}
