import { DataItemGetMoneyLog } from "../../DataModal/DataGetMoneyLog";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemGetMoneyLog extends cc.Component {

    @property(cc.Label)
    data_lab: cc.Label = null;

    @property(cc.Label)
    get_money: cc.Label = null;


    setData(data: DataItemGetMoneyLog) {
        this.data_lab.string = data.get_data;
        this.get_money.string = data.get_money_num + "元";
    }

}