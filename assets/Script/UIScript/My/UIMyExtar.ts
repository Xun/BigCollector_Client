
import UIMyExtar_Auto from "../../AutoScripts/UIMyExtar_Auto";
import { UIWindow } from "../../Common/UIForm";
import GameMgr from "../../Manager/GameMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIMyExtar extends UIWindow {

    view: UIMyExtar_Auto;

    // onLoad () {}

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.TodayExtarMoneyNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.today_extar_get_money + "元";
        this.view.TodayExtarAddUserNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.today_extar_add_user + "人";
        this.view.MonthExtarMoneyNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.month_extar_get_money + "元";
        this.view.MonthExtarAddUserNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.month_extar_add_user + "人";
        this.view.TotalExtarMoneyNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.total_extar_get_money + "元";
        this.view.TotalExtarAddUserNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.total_extar_add_user + "人";

        this.view.JieDuanNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.cur_stage;
        this.view.JDCurExtarMoneyNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.cur_stage_money + "元/";
        this.view.JDTotalExtarMoneyNumLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.total_stage_moner + "元";
        this.view.MultLab.string = GameMgr.dataModalMgr.DataMyExtarInfo.add_mult + "倍增速中";
        this.view.CompleteMoneyLab.string = "已完成" + GameMgr.dataModalMgr.DataMyExtarInfo.cur_stage_money / GameMgr.dataModalMgr.DataMyExtarInfo.total_stage_moner + "%，完成" + GameMgr.dataModalMgr.DataMyExtarInfo.total_stage_moner + "元自动转入钱包";

        this.view.JDPS.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.DataMyExtarInfo.cur_stage_money / GameMgr.dataModalMgr.DataMyExtarInfo.total_stage_moner;
    }

}
