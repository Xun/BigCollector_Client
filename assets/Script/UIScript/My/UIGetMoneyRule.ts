// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIGetMoneyRule_Auto from "../../AutoScripts/UIGetMoneyRule_Auto";
import { UIWindow } from "../../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIGetMoneyRule extends UIWindow {

    view: UIGetMoneyRule_Auto;

    // onLoad () {}

    start() {
        this.view.Sure.addClick(() => {
            this.closeSelf();
        }, this);
    }

    // update (dt) {}
}
