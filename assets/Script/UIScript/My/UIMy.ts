// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIMy_Auto from "../../AutoScripts/UIMy_Auto";
import { UIWindow } from "../../Common/UIForm";
import FormMgr from "../../Manager/FormMgr";
import UIConfig from "../../UIConfig";

const { ccclass, property } = cc._decorator;


@ccclass
export default class UIMy extends UIWindow {

    view: UIMy_Auto;

    // onLoad () {}

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.BtnGetMoney.addClick(() => {
            // FormMgr.open(UIConfig.UIGetMoneyRule);
        }, this);

        this.view.BtnFriend.addClick(() => {
            FormMgr.open(UIConfig.UIMyFriend);
        }, this);

        this.view.BtnVip.addClick(() => {
            // FormMgr.open(UIConfig.UIVip);
        }, this);

        this.view.BtnShare.addClick(() => {
            // FormMgr.open(UIConfig.UIShare);
        }, this);

        this.view.BtnSetting.addClick(() => {
            FormMgr.open(UIConfig.UISetting);
        }, this);

        this.view.BtnPlay.addClick(() => {
            // FormMgr.open(UIConfig.UIGame);
        }, this);


    }

    // update (dt) {}
}
