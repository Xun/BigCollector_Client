// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UILotteryGetTip_Auto from "../../AutoScripts/UILotteryGetTip_Auto";
import { UIWindow } from "../../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UILotteryGetTip extends UIWindow {

    view: UILotteryGetTip_Auto;

    // onLoad () {}

    start() {
        this.view.BtnSure.addClick(() => {
            this.closeSelf();
        }, this);
    }

    public onShow(params: any): void {
        console.log("获得类型：" + params)
    }
    // update (dt) {}
}
