import UILottery_Auto from "../../AutoScripts/UILottery_Auto";
import { ModalType } from "../../Common/Struct";
import { ModalOpacity } from "../../Common/SysDefine";
import { UIWindow } from "../../Common/UIForm";
import FormMgr from "../../Manager/FormMgr";
import GameMgr from "../../Manager/GameMgr";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UILottery extends UIWindow {

    modalType = new ModalType(ModalOpacity.OpacityHigh);
    view: UILottery_Auto;
    @property(cc.Prefab)
    lotteryItem: cc.Prefab = null;

    start() {
        this.view.CloseBtn.addClick(() => { this.closeSelf() }, this);
        this.view.GoBtn.addClick(this.onGoClick, this);
        for (let i = 0; i < this.view.LotteryReward.childrenCount; i++) {
            let parentNode = this.view.LotteryReward.children[i];
            let rewardItem = cc.instantiate(this.lotteryItem);
            rewardItem.getComponent("ItemLottery").setInfo({ num: 100 + i, icon: (i + 1).toString() });
            rewardItem.parent = parentNode;
        }
    }

    async onGoClick() {
        this.view.GoBtn.interactable = false;
        let datas = await apiClient.callApi("WheelSurf", {
            userAccount: GameMgr.dataModalMgr.UserInfo.userAccount,
            operationId: 1
        });
        if (!datas.isSucc) {
            UIToast.popUp(datas.err.message);
        } else {
            this.view.NumTxt.string = `每天24：00点恢复抽奖次数，当前剩余${datas.res.drawCount}次`
            let index = (360 / 8 * datas.res.drawId) + 360 * 5;
            CocosHelper.runTweenSync(this.view.LotteryReward, cc.tween()
                .to(5, { angle: -index }, cc.easeInOut(3.0))
            ).then(() => {
                this.view.GoBtn.interactable = true;
                FormMgr.open(UIConfig.UILotteryGetTip, 1);
                this.view.LotteryReward.angle = 0;
            });
        }


    }
}
