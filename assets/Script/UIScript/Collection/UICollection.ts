import UICollection_Auto from "../../AutoScripts/UICollection_Auto";
import { UIWindow } from "../../Common/UIForm";
import { DataModalMgr } from "../../Manager/DataModalMgr";
import GameMgr from "../../Manager/GameMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UICollection extends UIWindow {

    view: UICollection_Auto

    start() {
        this.view.CloseBtn.addClick(() => { this.closeSelf() }, this);
        this.view.WenFangSiBaoProgress.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.CollectionInfo.curWFSBProgress / 4;
        this.view.WenFangSiBaoProgressLab.string = GameMgr.dataModalMgr.CollectionInfo.curWFSBProgress + "/4";
        this.view.WenFangSiBaoGetNumTipLab.string = GameMgr.dataModalMgr.CollectionInfo.WFSBGetMoney.toString();

        this.view.SiJunZiProgress.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.CollectionInfo.curSJZProgress / 4;
        this.view.SiJunZiProgressLab.string = GameMgr.dataModalMgr.CollectionInfo.curSJZProgress + "/4";
        this.view.SiJunZiGetNumTipLab.string = GameMgr.dataModalMgr.CollectionInfo.SJZGetMoney.toString();

        this.view.MingZhuProgress.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.CollectionInfo.curSDMZProgress / 4;
        this.view.MingZhuProgressLab.string = GameMgr.dataModalMgr.CollectionInfo.curSDMZProgress + "/4";
        this.view.MingZhuGetNumTipLab.string = GameMgr.dataModalMgr.CollectionInfo.SDMZGetMoney.toString();

        this.view.ShenShouProgress.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.CollectionInfo.curSDSSProgress / 4;
        this.view.ShenShouProgressLab.string = GameMgr.dataModalMgr.CollectionInfo.curSDSSProgress + "/4";
        this.view.ShenShouGetNumTipLab.string = GameMgr.dataModalMgr.CollectionInfo.SDSSGetMoney.toString();

        this.view.BaDaJiaProgress.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.CollectionInfo.curBDJProgress / 8;
        this.view.BaDaJiaProgressLab.string = GameMgr.dataModalMgr.CollectionInfo.curBDJProgress + "/8";
        this.view.BaDaJiaGetNumTipLab.string = GameMgr.dataModalMgr.CollectionInfo.BDJGetMoney.toString();

        this.view.JinChanProgress.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.CollectionInfo.curJCProgress / 1;
        this.view.JinChanProgressLab.string = GameMgr.dataModalMgr.CollectionInfo.curJCProgress + "/1";
        this.view.JinChanGetNumTipLab.string = GameMgr.dataModalMgr.CollectionInfo.JCDescribe;

        this.view.PiXiuProgress.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.CollectionInfo.curPXProgress / 1;
        this.view.PiXiuProgressLab.string = GameMgr.dataModalMgr.CollectionInfo.curPXProgress + "/1";
        this.view.PiXiuGetNumTipLab.string = GameMgr.dataModalMgr.CollectionInfo.JCDescribe;

        this.view.JinLongProgress.getComponent(cc.ProgressBar).progress = GameMgr.dataModalMgr.CollectionInfo.curJLProgress / 1;
        this.view.JinLongProgressLab.string = GameMgr.dataModalMgr.CollectionInfo.curJLProgress + "/1";
        this.view.JinLongGetNumTipLab.string = GameMgr.dataModalMgr.CollectionInfo.JLDescribe;
    }

    // update (dt) {}
}
