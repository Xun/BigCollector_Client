
import UIColleCtionConversion_Auto from "../../AutoScripts/UIColleCtionConversion_Auto";
import { UIWindow } from "../../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIcolleCtionConversion extends UIWindow {

    view: UIColleCtionConversion_Auto;
    // onLoad () {}

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);
    }

}
