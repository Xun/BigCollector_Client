// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UISetting_Auto from "../AutoScripts/UISetting_Auto";
import { UIWindow } from "../Common/UIForm";
import SoundMgr from "../Manager/SoundMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UISetting extends UIWindow {

    view: UISetting_Auto;

    onLoad() {
        let volume = SoundMgr.inst.getVolume();
        if (volume != null && volume.effectVolume == 0 && volume.musicVolume == 0) {
            this.view.offNode.active = true;
            this.view.onNode.active = false;
        } else {
            this.view.offNode.active = false;
            this.view.onNode.active = true;
        }
    }

    start() {
        this.view.BtnClose.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.BtnMusic.addClick(() => {
            this.view.offNode.active = !this.view.offNode.active;
            this.view.onNode.active = !this.view.onNode.active;
            SoundMgr.inst.setEffectVolume(this.view.onNode.active ? 1 : 0);
            SoundMgr.inst.setMusicVolume(this.view.onNode.active ? 1 : 0);
        }, this);
    }

}
