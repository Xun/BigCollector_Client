
import { UserInfo } from "../../Protocol/bundle";
import { GameServer, InvokeBegin, LoginReply, LoginRequest } from "../../Protocol/bundle";
import UILogin_Auto from "../AutoScripts/UILogin_Auto";
import { UIScreen } from "../Common/UIForm";
import { DataSeeDetail } from "../DataModal/DataFriend";
import { DataModalMgr } from "../Manager/DataModalMgr";
import FormMgr from "../Manager/FormMgr";
import GameMgr from "../Manager/GameMgr";
import { NativeMgr } from "../Manager/NativeMgr";
import TipsMgr from "../Manager/TipsMgr";
import WindowMgr from "../Manager/WindowMgr";
import HttpHelper from "../Net/HttpHelper";
import { apiClient } from "../Net/RpcConent";
import UIConfig from "../UIConfig";
import CocosHelper from "../Utils/CocosHelper";

import UIToast from "./UIToast";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UILogin extends UIScreen {

    view: UILogin_Auto;

    async onLoad() {

        apiClient.flows.postDisconnectFlow.push(v => {
            if (!v.isManual) {
                setTimeout(async () => {
                    let res = await apiClient.connect();
                    if (!res.isSucc) {
                        console.log(result.errMsg);
                        FormMgr.open(UIConfig.UIScoketReconnent);
                    }
                })
            }
            return v;
        })

        apiClient.listenMsg("WorkBench", (msg) => {
            GameMgr.dataModalMgr.UserInfo.userWorkbench = msg.userWorkBench;
            GameMgr.dataModalMgr.UserInfo.userCurNeedBuyCoinNum = msg.buyNeedCoin;
        })


        let result = await apiClient.connect();
        // 连接不一定成功（例如网络错误），所以要记得错误处理
        if (!result.isSucc) {
            console.log(result.errMsg);
            FormMgr.open(UIConfig.UIScoketReconnent);

        }
    }

    public static async wechatCall(code: string) {
        console.log("code:" + code);
        let datas = await apiClient.callApi("Login", {
            code: code
        });

        if (!datas.isSucc) {
            FormMgr.open(UIConfig.UIScoketReconnent);
            return;
        }
        // console.log()
        GameMgr.dataModalMgr.UserInfo.userAccount = datas.res.userInfo.userAccount;
        GameMgr.dataModalMgr.UserInfo.userName = datas.res.userInfo.userName;
        // GameMgr.dataModalMgr.UserInfo.userAvatarUrl = datas.res.userInfo.userAvatarUrl;
        GameMgr.dataModalMgr.UserInfo.userAvatarUrl = "https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=false&word=%E5%A4%B4%E5%83%8F&hs=0&pn=4&spn=0&di=7146857200093233153&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&ie=utf-8&oe=utf-8&cl=2&lm=-1&cs=3172226535%2C2587509282&os=1391451534%2C125733970&simid=59149375%2C589686028&adpicid=0&lpn=0&ln=30&fr=ala&fm=&sme=&cg=head&bdtype=0&oriquery=%E5%A4%B4%E5%83%8F&objurl=https%3A%2F%2Fgimg2.baidu.com%2Fimage_search%2Fsrc%3Dhttp%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202107%2F09%2F20210709142454_dc8dc.thumb.1000_0.jpeg%26refer%3Dhttp%3A%2F%2Fc-ssl.duitang.com%26app%3D2002%26size%3Df9999%2C10000%26q%3Da80%26n%3D0%26g%3D0n%26fmt%3Dauto%3Fsec%3D1670321388%26t%3D27faa2c2687ab622529fa60f58795d7f&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B17tpwg2_z%26e3Bv54AzdH3Fks52AzdH3F%3Ft1%3D8nc000m8an&gsm=&islist=&querylist=&dyTabStr=MCwzLDEsNSwyLDcsOCw2LDQsOQ%3D%3D"
        GameMgr.dataModalMgr.UserInfo.userCoin = datas.res.userInfo.userCoin;
        GameMgr.dataModalMgr.UserInfo.userDiamond = datas.res.userInfo.userDiamond;
        GameMgr.dataModalMgr.UserInfo.userMoney = datas.res.userInfo.userMoney;
        FormMgr.open(UIConfig.UIHome);
    }

    start() {
        this.view.BtnWechat.addClick(async () => {
            this.login();
        }, this)
    }

    private async login() {
        let isAgarn: boolean = this.view.UserAgarn.getComponent(cc.Toggle).isChecked;
        if (!isAgarn) {
            UIToast.popUp("请阅读并同意《用户协议》");
            return;
        }
        // NativeMgr.callNativeClass("AppActivity", "loginWX", UILogin.testRpc);
        // console.log("调用完成")
        UILogin.wechatCall(this.view.testAccount.string);

    }

}