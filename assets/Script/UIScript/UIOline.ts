// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import UIOline_Auto from "../AutoScripts/UIOline_Auto";
import { UIWindow } from "../Common/UIForm";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIOline extends UIWindow {

    view: UIOline_Auto;

    // onLoad () {}

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);
    }

    // update (dt) {}
}
