// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { DataBagItem } from "../../DataModal/DataBag";
import CocosHelper from "../../Utils/CocosHelper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ItemBag extends cc.Component {

    @property(cc.Label)
    item_name: cc.Label = null;

    @property(cc.Sprite)
    item_sp: cc.Sprite = null;

    setData(item: DataBagItem) {

        if (Number(item.itemID) > 1100) {
            let _chipName = CocosHelper.getShopNameByID(item.itemID.toString());
            this.item_name.string = _chipName + "+" + item.itemCount;
            CocosHelper.setShopInfo(_chipName, this.item_sp);
        } else {
            let _chipName = CocosHelper.getChipNameByID(item.itemID.toString());
            this.item_name.string = _chipName + "+" + item.itemCount;
            let _iconName = _chipName.replace("碎片", "");
            CocosHelper.setDealIcon(_iconName, this.item_sp);
        }


    }
}
