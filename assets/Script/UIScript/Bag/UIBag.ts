import UIBag_Auto from "../../AutoScripts/UIBag_Auto";
import ListUtil from "../../Common/Components/ListUtil";
import { UIWindow } from "../../Common/UIForm";
import { DataModalMgr } from "../../Manager/DataModalMgr";
import FormMgr from "../../Manager/FormMgr";
import GameMgr from "../../Manager/GameMgr";
import { EventCenter } from "../../Net/EventCenter";
import { apiClient } from "../../Net/RpcConent";
import UIConfig from "../../UIConfig";
import CocosHelper from "../../Utils/CocosHelper";
import UIToast from "../UIToast";
import ItemBag from "./ItemBag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class UIBag extends UIWindow {

    view: UIBag_Auto;

    @property(ListUtil)
    listBag: ListUtil = null;

    onLoad() {
        // GameMgr.dataModalMgr.BagInfo.list_item = [{ item_sp: "imgs/itemProduce/item1", item_name: "毛笔碎片", item_num: 1 }];
        // this.listBag.numItems = GameMgr.dataModalMgr.BagInfo.list_item.length;
        GameMgr.dataModalMgr.BagInfo.cur_bag_capacity = GameMgr.dataModalMgr.BagInfo.list_item.length;
    }

    start() {
        this.view.CloseBtn.addClick(() => {
            this.closeSelf();
        }, this);

        this.view.AddBtn.addClick(() => {
            FormMgr.open(UIConfig.UIBagAdd);
        }, this);

        this.view.AddBtn.addClick(() => {
            this.onAddClick();
        }, this);

        this.listBag.numItems = GameMgr.dataModalMgr.BagInfo.list_item.length;
        GameMgr.dataModalMgr.BagInfo.cur_bag_capacity = GameMgr.dataModalMgr.BagInfo.list_item.length;
        this.view.BagNumLab.string = GameMgr.dataModalMgr.BagInfo.cur_bag_capacity + "/" + GameMgr.dataModalMgr.BagInfo.total_bag_capacity;
    }

    async onAddClick() {
        let data = await apiClient.callApi("BagInfo", { type: 2 });
        if (!data.isSucc) {
            UIToast.popUp(data.err.message);
            return;
        }
        GameMgr.dataModalMgr.BagInfo.list_item = data.res.bagList;
        GameMgr.dataModalMgr.BagInfo.total_bag_capacity = data.res.bagVol;
    }

    //垂直列表渲染器
    onListBagRender(item: cc.Node, idx: number) {
        item.getComponent(ItemBag).setData(GameMgr.dataModalMgr.BagInfo.list_item[idx]);
    }
}
