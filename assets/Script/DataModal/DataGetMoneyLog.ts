export class DataGetMoneyLog {
    public list_get_money_log: DataItemGetMoneyLog[] = Array<DataItemGetMoneyLog>();
}

export class DataItemGetMoneyLog {
    public get_data: string = "";
    public get_money_num: number = 0;
}