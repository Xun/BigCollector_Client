// import Long = require("Long");

export class DataCurBuyProduce {

    //ID
    private _id: string = '0';
    public get id(): string {
        if (this._id === undefined) {
            this._id = "0";
        }
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }

    //名字
    private _name: string = '0';
    public get name(): string {
        if (this._name === undefined) {
            this._name = "0";
        }
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }

    //图片
    private _img: string = '0';
    public get img(): string {
        if (this._img === undefined) {
            this._img = "0";
        }
        return this._img;
    }
    public set img(value: string) {
        this._img = value;
    }

    //基础购买价格
    private _buyPrice: number = 0;
    public get buyPrice(): number {
        if (this._buyPrice === undefined) {
            this._buyPrice = 0;
        }
        return this._buyPrice;
    }
    public set buyPrice(value: number) {
        this._buyPrice = value;
    }
}

export class ProducesJson {
    itemsInfo: cc.JsonAsset;
}
