export class DataCollection {
    //文房四宝
    private _curWFSBProgress: number = 0;
    public get curWFSBProgress(): number {
        if (this._curWFSBProgress === undefined) {
            this._curWFSBProgress = 0;
        }
        return this._curWFSBProgress;
    }
    public set curWFSBProgress(value: number) {
        this._curWFSBProgress = value;
    }

    private _WFSBGetMoney: number = 0;
    public get WFSBGetMoney(): number {
        if (this._WFSBGetMoney === undefined) {
            this._WFSBGetMoney = 0;
        }
        return this._WFSBGetMoney;
    }
    public set WFSBGetMoney(value: number) {
        this._WFSBGetMoney = value;
    }

    //四君子
    private _curSJZProgress: number = 0;
    public get curSJZProgress(): number {
        if (this._curSJZProgress === undefined) {
            this._curSJZProgress = 0;
        }
        return this._curSJZProgress;
    }
    public set curSJZProgress(value: number) {
        this._curSJZProgress = value;
    }

    private _SJZGetMoney: number = 0;
    public get SJZGetMoney(): number {
        if (this._SJZGetMoney === undefined) {
            this._SJZGetMoney = 0;
        }
        return this._SJZGetMoney;
    }
    public set SJZGetMoney(value: number) {
        this._SJZGetMoney = value;
    }

    //四大名著
    private _curSDMZProgress: number = 0;
    public get curSDMZProgress(): number {
        if (this._curSDMZProgress === undefined) {
            this._curSDMZProgress = 0;
        }
        return this._curSDMZProgress;
    }
    public set curSDMZProgress(value: number) {
        this._curSDMZProgress = value;
    }

    private _SDMZGetMoney: number = 0;
    public get SDMZGetMoney(): number {
        if (this._SDMZGetMoney === undefined) {
            this._SDMZGetMoney = 0;
        }
        return this._SDMZGetMoney;
    }
    public set SDMZGetMoney(value: number) {
        this._SDMZGetMoney = value;
    }

    //四大神兽
    private _curSDSSProgress: number = 0;
    public get curSDSSProgress(): number {
        if (this._curSDSSProgress === undefined) {
            this._curSDSSProgress = 0;
        }
        return this._curSDSSProgress;
    }
    public set curSDSSProgress(value: number) {
        this._curSDSSProgress = value;
    }

    private _SDSSGetMoney: number = 0;
    public get SDSSGetMoney(): number {
        if (this._SDSSGetMoney === undefined) {
            this._SDSSGetMoney = 0;
        }
        return this._SDSSGetMoney;
    }
    public set SDSSGetMoney(value: number) {
        this._SDSSGetMoney = value;
    }

    //唐宋八大家
    private _curBDJProgress: number = 0;
    public get curBDJProgress(): number {
        if (this._curBDJProgress === undefined) {
            this._curBDJProgress = 0;
        }
        return this._curBDJProgress;
    }
    public set curBDJProgress(value: number) {
        this._curBDJProgress = value;
    }

    private _BDJGetMoney: number = 0;
    public get BDJGetMoney(): number {
        if (this._BDJGetMoney === undefined) {
            this._BDJGetMoney = 0;
        }
        return this._BDJGetMoney;
    }
    public set BDJGetMoney(value: number) {
        this._BDJGetMoney = value;
    }

    //金蟾
    private _curJCProgress: number = 0;
    public get curJCProgress(): number {
        if (this._curJCProgress === undefined) {
            this._curJCProgress = 0;
        }
        return this._curJCProgress;
    }
    public set curJCProgress(value: number) {
        this._curJCProgress = value;
    }

    private _JCDescribe: string = "";
    public get JCDescribe(): string {
        if (this._JCDescribe === undefined) {
            this._JCDescribe = "";
        }
        return this._JCDescribe;
    }
    public set JCDescribe(value: string) {
        this._JCDescribe = value;
    }

    //貔貅
    private _curPXProgress: number = 0;
    public get curPXProgress(): number {
        if (this._curPXProgress === undefined) {
            this._curPXProgress = 0;
        }
        return this._curPXProgress;
    }
    public set curPXProgress(value: number) {
        this._curPXProgress = value;
    }

    private _PXDescribe: string = "";
    public get PXDescribe(): string {
        if (this._PXDescribe === undefined) {
            this._PXDescribe = "";
        }
        return this._PXDescribe;
    }
    public set PXDescribe(value: string) {
        this._PXDescribe = value;
    }

    //金龙
    private _curJLProgress: number = 0;
    public get curJLProgress(): number {
        if (this._curJLProgress === undefined) {
            this._curJLProgress = 0;
        }
        return this._curJLProgress;
    }
    public set curJLProgress(value: number) {
        this._curJLProgress = value;
    }

    private _JLDescribe: string = "";
    public get JLDescribe(): string {
        if (this._JLDescribe === undefined) {
            this._JLDescribe = "";
        }
        return this._JLDescribe;
    }
    public set JLDescribe(value: string) {
        this._JLDescribe = value;
    }
}