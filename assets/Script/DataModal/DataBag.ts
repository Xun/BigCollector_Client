export class DataBagInfo {
    public list_item: DataBagItem[] = Array<DataBagItem>();
    public cur_bag_capacity: number = 0;
    public total_bag_capacity: number = 6;
}

export class DataBagItem {
    public itemID: number = 0;
    public itemCount: number = 0;
}