export class DataRank {
    public rank_list: DataRankItem[] = Array<DataRankItem>();
}

export class DataRankItem {
    public uid: string = "";
    public name: string = "";
    public money: number = 0;
    public picUrl: string = "";
}