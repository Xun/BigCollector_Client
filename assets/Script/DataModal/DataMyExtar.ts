export class DataMyExtar {
    public today_extar_get_money: number = 0;
    public today_extar_add_user: number = 0;
    public month_extar_get_money: number = 0;
    public month_extar_add_user: number = 0;
    public total_extar_get_money: number = 0;
    public total_extar_add_user: number = 0;
    public cur_stage: string = "";
    public cur_stage_money: number = 0;
    public total_stage_moner: number = 0;
    public add_mult: number = 0;
    public complete_money: number = 0;
}