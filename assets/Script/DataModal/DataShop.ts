export class DataShop {
    public list_item: DataShopItem[] = Array<DataShopItem>();
}

export class DataShopItem {
    public id: string = "";
    public name: string = "";
    public need_num: string = "";
    public need_type: string = "";
}

export class ChipJson {
    chipInfo: any[] = [];
}

export class ShopJson {
    shopInfo: any[] = [];
}