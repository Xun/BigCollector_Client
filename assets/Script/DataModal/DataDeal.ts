export class DataDeal {
    public dealBuyList: DataDealItem[] = Array<DataDealItem>();
    public dealSellList: DataDealItem[] = Array<DataDealItem>();
    public dealMyBuyList: DataDealItem[] = Array<DataDealItem>();
    public dealMySellList: DataDealItem[] = Array<DataDealItem>();
    public dealDetailList: DataDealDetailItem[] = Array<DataDealDetailItem>();
    // public dealList: [];
}

export class DataDealItem {
    public guid?: string;
    public chipID: number;
    public needCount?: number;
    public currentCount?: number;
    public price?: number;//单价
    public userAccount?: string;
    public status?: number; // -1 取消 0 进行中 1 已完成
}

export class DataDealDetailItem {
    chipID: number = 0;
    time: number = 0;
    type: number = 0;
    count: number = 0;
    price: number = 0;
}