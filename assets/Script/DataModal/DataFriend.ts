export class DataFriend {
    public my_friend_num: number = 0;
    public probabyly_money: number = 0;
    public today_get_money: number = 0;
    public total_get_money: number = 0;
    public today_extar_get_money: number = 0;
    public cur_get_money: number = 0;
    public my_inviter_name: string = "";
    public my_invite_head: string = "";
}

export class DataFriendMoneyDetail {
    public today_get_money: number = 0;
    public total_get_money: number = 0;
    public list_item_friend_money_detail: DataItemFriendMoneyDetail[] = Array<DataItemFriendMoneyDetail>();
}

export class DataItemFriendMoneyDetail {
    public time: number = 0;
    public oneFriendEranings: number = 0;
    public twoFriendEranings: number = 0;
    public todayTotalEranings: number = 0;
}

export class DataFriendGiveDetail {
    public list_item_friend_give_detail: DataItemFriendGiveDetail[] = Array<DataItemFriendGiveDetail>();
}


export class DataItemFriendGiveDetail {
    public user_name: string = "";
    public user_id: number = 0;
    public user_head: string = "";
    public give_time: string = "";
    public give_num: number = 0;
}


export class DataSeeDetail {
    public list_item_friend_give_detail: DataItemSeeDetail[] = Array<DataItemSeeDetail>();
}

export class DataItemSeeDetail {
    public user_name: string = "";
    public user_id: number = 0;
    public user_head: string = "";
    public see_time: string = "";
}

export class DataItemFriendInfo {
    public user_name: string = "";
    public user_level: string = "";
    public user_head: string = "";
    public add_time: string = "";
}