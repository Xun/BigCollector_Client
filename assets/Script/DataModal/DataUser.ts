import { UserInfos } from "../Manager/InterfaceMgr";
import CocosHelper from "../Utils/CocosHelper";
export enum OperationType {
    buy = 1,//购买
    sell = 2,//出售
    concat = 3,//合成
    autoBuy = 4,
    autoSell = 5,
}
export class UserInfo {

    //基础信息
    private _userName: string = "";
    public get userName(): string {
        if (this._userName === undefined) {
            this._userName = "";
        }
        return this._userName;
    }
    public set userName(value: string) {
        this._userName = value;
    }

    private _userAvatarUrl: string = "";
    public get userAvatarUrl(): string {
        if (this._userAvatarUrl === undefined) {
            this._userAvatarUrl = "";
        }
        return this._userAvatarUrl;
    }
    public set userAvatarUrl(value: string) {
        this._userAvatarUrl = value;
    }

    private _userAccount: string = "";
    public get userAccount(): string {
        if (this._userAccount === undefined) {
            this._userAccount = "";
        }
        return this._userAccount;
    }
    public set userAccount(value: string) {
        this._userAccount = value;
    }

    //金币
    private _userCoin: number = 0;
    public get userCoin(): number {
        if (this._userCoin === undefined) {
            this._userCoin = 0;
        }
        return this._userCoin;
    }
    public set userCoin(value: number) {
        this._userCoin = value;
    }

    //钻石
    private _userDiamond: number = 0;
    public get userDiamond(): number {
        if (this._userDiamond === undefined) {
            this._userDiamond = 0;
        }
        return this._userDiamond;
    }
    public set userDiamond(value: number) {
        this._userDiamond = value;
    }

    //金额
    private _userMoney: number = 0;
    public get userMoney(): number {
        if (this._userMoney === undefined) {
            this._userMoney = 0;
        }
        return this._userMoney;
    }
    public set userMoney(value: number) {
        this._userMoney = value;
    }

    //当前购买所需金币
    private _userCurNeedBuyCoinNum: number = 0;
    public get userCurNeedBuyCoinNum(): number {
        if (this._userCurNeedBuyCoinNum === undefined) {
            this._userCurNeedBuyCoinNum = 0;
        }
        return this._userCurNeedBuyCoinNum;
    }
    public set userCurNeedBuyCoinNum(value: number) {
        this._userCurNeedBuyCoinNum = value;
    }

    //当前每秒生产金币
    private _userMakeCoin: number = 0;
    public get userMakeCoin(): number {
        if (this._userMakeCoin === undefined) {
            this._userMakeCoin = 0;
        }
        return this._userMakeCoin;
    }
    public set userMakeCoin(value: number) {
        this._userMakeCoin = value;
    }

    //工作台数据
    private _userWorkbench: Array<number> = new Array();
    public get userWorkbench(): number[] {
        if (this._userWorkbench === undefined) {
            this._userWorkbench = [];
        }
        return this._userWorkbench;
    }
    public set userWorkbench(value: number[]) {
        this._userWorkbench = value;
    }

    //当前解锁等级
    private _userUnlockLevel: number = 1;
    public get userUnlockLevel(): number {
        if (this._userUnlockLevel === undefined) {
            this._userUnlockLevel = 1;
        }
        return this._userUnlockLevel;
    }
    public set userUnlockLevel(value: number) {
        this._userUnlockLevel = value;
    }

    //自动合成时间
    private _combineAutoTime: number = 15;
    public get combineAutoTime(): number {
        if (this._combineAutoTime === undefined) {
            this._combineAutoTime = 15;
        }
        return this._combineAutoTime;
    }
    public set combineAutoTime(value: number) {
        this._combineAutoTime = value;
    }

    private _UserInfos: UserInfos = null;

    public set userInfos(v: UserInfos) {
        this._UserInfos = v;
    }

    public get userInfos(): UserInfos {
        return this._UserInfos
    }

    //是否还有空余位置
    hasPosAtWorkbench() {
        for (let idx = 0; idx < this._userWorkbench.length; idx++) {
            if (this._userWorkbench[idx] === 0) {
                return true
            }
        }
        return false;
    };

    /**
     * 增加物品到工作台
     * @param {string} itemId 
     * @return {number} 添加到哪个位置
     */
    checkNullPos() {
        //检查有没有空位置
        if (!this.hasPosAtWorkbench()) {
            return -1;
        }

        //找到空位置
        for (let idx = 0; idx < this._userWorkbench.length; idx++) {
            if (this._userWorkbench[idx] === 0) {
                // this._userWorkbench[idx] = itemId;
                return idx;
            }
        }
    }

    /**
     * 物品结合
     */
    combineCake(originIndex, targetIndex, callback) {
        if (!this._userWorkbench) {
            callback(false);
            return false;
        }

        if (this._userWorkbench.length <= originIndex) {
            callback(false);
            return false;
        }

        if (this._userWorkbench.length <= targetIndex) {
            callback(false);
            return false;
        }

        let targetItemId = this._userWorkbench[targetIndex];

        if (this._userWorkbench[originIndex] !== targetItemId) {
            callback(false);
            return false;
        }

        let itemInfo = CocosHelper.getItemProduceInfoById(targetItemId);

        if (!itemInfo) {
            //未找到信息，不能判断下一步进化到那一步骤
            callback(false);
            return false;
        }

        if (!itemInfo.nextLevel) {
            callback(false, 'maxLevel');
            return false;
        }

        this._userWorkbench[targetIndex] = itemInfo.nextLevel;
        this._userWorkbench[originIndex] = 0; //将原有的置空

        let nextLevel = itemInfo.nextLevel;
        let isUnlock = this.unlockProduceLevel(nextLevel);
        callback(true, isUnlock);
        return true;
    }

    /**
     * 解锁新等级
     * @param nextLevel --新等级
     */
    unlockProduceLevel(nextLevel: number) {
        if (nextLevel > this._userUnlockLevel) {
            let isOnlyMaxLevel = true;
            for (var idx = 0; idx < this._userWorkbench.length; idx++) {
                if (nextLevel < Number(this._userWorkbench[idx])) {
                    isOnlyMaxLevel = false;
                    break;
                }
            }

            this._userUnlockLevel = nextLevel;
            return isOnlyMaxLevel;
        }

        return false;
    }

}


