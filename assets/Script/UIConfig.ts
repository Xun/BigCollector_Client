export default class UIConfig {
    static UIHome = {
        prefabUrl: "Forms/Screen/UIHome",
        type: "UIScreen"
    }
    static UILogin = {
        prefabUrl: "Forms/Screen/UILogin",
        type: "UIScreen"
    }
    static UIStart = {
        prefabUrl: "Forms/Screen/UIStart",
        type: "UIScreen"
    }
    static UILoading = {
        prefabUrl: "Forms/Tips/UILoading",
        type: "UITips"
    }
    static UIBag = {
        prefabUrl: "Forms/Windows/UIBag",
        type: "UIWindow"
    }
    static UIBagAdd = {
        prefabUrl: "Forms/Windows/UIBagAdd",
        type: "UIWindow"
    }
    static UIBuyTip = {
        prefabUrl: "Forms/Windows/UIBuyTip",
        type: "UIWindow"
    }
    static UIColleCtionConversion = {
        prefabUrl: "Forms/Windows/UIColleCtionConversion",
        type: "UIWindow"
    }
    static UICollection = {
        prefabUrl: "Forms/Windows/UICollection",
        type: "UIWindow"
    }
    static UIDeal = {
        prefabUrl: "Forms/Windows/UIDeal",
        type: "UIWindow"
    }
    static UIDebris = {
        prefabUrl: "Forms/Windows/UIDebris",
        type: "UIWindow"
    }
    static UIDialog = {
        prefabUrl: "Forms/Windows/UIDialog",
        type: "UIWindow"
    }
    static UIExtarCommitSure = {
        prefabUrl: "Forms/Windows/UIExtarCommitSure",
        type: "UIWindow"
    }
    static UIFriend = {
        prefabUrl: "Forms/Windows/UIFriend",
        type: "UIWindow"
    }
    static UIFriendGive = {
        prefabUrl: "Forms/Windows/UIFriendGive",
        type: "UIWindow"
    }
    static UIFriendGiveDetail = {
        prefabUrl: "Forms/Windows/UIFriendGiveDetail",
        type: "UIWindow"
    }
    static UIFriendMonerDetail = {
        prefabUrl: "Forms/Windows/UIFriendMonerDetail",
        type: "UIWindow"
    }
    static UIFriendSee = {
        prefabUrl: "Forms/Windows/UIFriendSee",
        type: "UIWindow"
    }
    static UIFriendSeeDetail = {
        prefabUrl: "Forms/Windows/UIFriendSeeDetail",
        type: "UIWindow"
    }
    static UIFriendSerch = {
        prefabUrl: "Forms/Windows/UIFriendSerch",
        type: "UIWindow"
    }
    static UIGetMoneyLog = {
        prefabUrl: "Forms/Windows/UIGetMoneyLog",
        type: "UIWindow"
    }
    static UIGetMoneyRule = {
        prefabUrl: "Forms/Windows/UIGetMoneyRule",
        type: "UIWindow"
    }
    static UIInviteGetInfo = {
        prefabUrl: "Forms/Windows/UIInviteGetInfo",
        type: "UIWindow"
    }
    static UILottery = {
        prefabUrl: "Forms/Windows/UILottery",
        type: "UIWindow"
    }
    static UILotteryGetTip = {
        prefabUrl: "Forms/Windows/UILotteryGetTip",
        type: "UIWindow"
    }
    static UIMy = {
        prefabUrl: "Forms/Windows/UIMy",
        type: "UIWindow"
    }
    static UIMyExtar = {
        prefabUrl: "Forms/Windows/UIMyExtar",
        type: "UIWindow"
    }
    static UIMyFriend = {
        prefabUrl: "Forms/Windows/UIMyFriend",
        type: "UIWindow"
    }
    static UIMyWallet = {
        prefabUrl: "Forms/Windows/UIMyWallet",
        type: "UIWindow"
    }
    static UIOline = {
        prefabUrl: "Forms/Windows/UIOline",
        type: "UIWindow"
    }
    static UIRank = {
        prefabUrl: "Forms/Windows/UIRank",
        type: "UIWindow"
    }
    static UIRecoverTip = {
        prefabUrl: "Forms/Windows/UIRecoverTip",
        type: "UIWindow"
    }
    static UIRotateAndFlip = {
        prefabUrl: "Forms/Windows/UIRotateAndFlip",
        type: "UIWindow"
    }
    static UIScoketReconnent = {
        prefabUrl: "Forms/Windows/UIScoketReconnent",
        type: "UIWindow"
    }
    static UISetSocial = {
        prefabUrl: "Forms/Windows/UISetSocial",
        type: "UIWindow"
    }
    static UISetting = {
        prefabUrl: "Forms/Windows/UISetting",
        type: "UIWindow"
    }
    static UIShare = {
        prefabUrl: "Forms/Windows/UIShare",
        type: "UIWindow"
    }
    static UIShiMing = {
        prefabUrl: "Forms/Windows/UIShiMing",
        type: "UIWindow"
    }
    static UIShop = {
        prefabUrl: "Forms/Windows/UIShop",
        type: "UIWindow"
    }
    static UISignIn = {
        prefabUrl: "Forms/Windows/UISignIn",
        type: "UIWindow"
    }
    static UITask = {
        prefabUrl: "Forms/Windows/UITask",
        type: "UIWindow"
    }
    static UIUnlockNewLevel = {
        prefabUrl: "Forms/Windows/UIUnlockNewLevel",
        type: "UIWindow"
    }
    static UIDealDetail = {
        prefabUrl: "Forms/Windows/UIDealDetail",
        type: "UIWindow"
    }
    static UIDealMyGet = {
        prefabUrl: "Forms/Windows/UIDealMyGet",
        type: "UIWindow"
    }
    static UIDealAdd = {
        prefabUrl: "Forms/Windows/UIDealAdd",
        type: "UIWindow"
    }
    static UIDealMySell = {
        prefabUrl: "Forms/Windows/UIDealMySell",
        type: "UIWindow"
    }
    static UIDealSell = {
        prefabUrl: "Forms/Windows/UIDealSell",
        type: "UIWindow"
    }
    static UIDealTip = {
        prefabUrl: "Forms/Windows/UIDealTip",
        type: "UIWindow"
    }
    
    }