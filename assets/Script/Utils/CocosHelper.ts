////////////////////////////////cocoscreator  Tween动画接口拓展//////////////////////////
/**
 * 倍速播放
 * @param speed 倍速
 * @example tween.speed(2);
 */
cc.Tween.prototype.speed = function (speed) {
    this._finalAction._speedMethod = true;
    this._finalAction._speed = speed;
}

/**
 * 暂停
 * @example tween.pause();
 */
cc.Tween.prototype.pause = function () {
    this._finalAction.paused = true;
};

/**
* 恢复
* @example tween.resume();
*/
cc.Tween.prototype.resume = function () {
    this._finalAction.paused = false;
};

/**
 * 获取持续时间
 * @example let duration = tween.duration();
 */
cc.Tween.prototype.duration = function () {
    return this._finalAction._duration;
}

/**
* 获取已经进行的时间
* @example let elapsed = tween.elapsed();
*/
cc.Tween.prototype.elapsed = function () {
    return this._finalAction._elapsed;
}

/**
 * 跳转到指定时间
 * @param time 时间(秒)
 * @example tween.goto(2);
 */
cc.Tween.prototype.goto = function (time) {
    this._finalAction._goto = true;
    this._finalAction._elapsed = time;
}


///////////////////////////////////////////////////////////////////////////////////////
import GameMgr from "../Manager/GameMgr";

export class LoadProgress {
    public url: string;
    public completedCount: number;
    public totalCount: number;
    public item: any;
    public cb?: Function;
}

/** 一些cocos api 的封装, promise函数统一加上sync后缀 */
export default class CocosHelper {

    /** 加载进度 */
    public static loadProgress = new LoadProgress();

    /** 等待时间, 秒为单位 */
    public static sleepSync = function (dur: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            cc.Canvas.instance.scheduleOnce(() => {
                resolve(true);
            }, dur);
        });
    }

    /**
     * 
     * @param target 
     * @param repeat -1，表示永久执行
     * @param tweens 
     */
    public static async runRepeatTweenSync(target: any, repeat: number, ...tweens: cc.Tween[]) {
        return new Promise((resolve, reject) => {
            let selfTween = cc.tween(target);
            for (const tmpTween of tweens) {
                selfTween = selfTween.then(tmpTween);
            }
            if (repeat < 0) {
                cc.tween(target).repeatForever(selfTween).start();
            } else {
                cc.tween(target).repeat(repeat, selfTween).call(() => {
                    resolve(true);
                }).start();
            }
        });
    }
    /** 同步的tween */
    public static async runTweenSync(target: any, ...tweens: cc.Tween[]): Promise<void> {
        return new Promise((resolve, reject) => {
            let selfTween = cc.tween(target);
            for (const tmpTween of tweens) {
                selfTween = selfTween.then(tmpTween);
            }
            selfTween.call(() => {
                resolve();
            }).start();
        });
    }
    /** 停止tween */
    public static stopTween(target: any) {
        cc.Tween.stopAllByTarget(target);
    }
    public stopTweenByTag(tag: number) {
        cc.Tween.stopAllByTag(tag);
    }

    /** 同步的动画 */
    public static async runAnimSync(node: cc.Node, animName?: string | number) {
        let anim = node.getComponent(cc.Animation);
        if (!anim) return;
        let clip: cc.AnimationClip = null;
        if (!animName) clip = anim.defaultClip;
        else {
            let clips = anim.getClips();
            if (typeof (animName) === "number") {
                clip = clips[animName];
            } else if (typeof (animName) === "string") {
                for (let i = 0; i < clips.length; i++) {
                    if (clips[i].name === animName) {
                        clip = clips[i];
                        break;
                    }
                }
            }
        }
        if (!clip) return;
        await CocosHelper.sleepSync(clip.duration);
    }

    /** 加载资源异常时抛出错误 */
    public static loadResThrowErrorSync<T>(url: string, type: typeof cc.Asset, onProgress?: (completedCount: number, totalCount: number, item: any) => void): Promise<T> {
        return null;
    }

    private static _loadingMap: { [key: string]: Function[] } = {};
    public static loadRes<T>(url: string, type: typeof cc.Asset, callback: Function) {
        if (this._loadingMap[url]) {
            this._loadingMap[url].push(callback);
            return;
        }
        this._loadingMap[url] = [callback];
        this.loadResSync<T>(url, type).then((data: any) => {
            let arr = this._loadingMap[url];
            for (const func of arr) {
                func(data);
            }
            this._loadingMap[url] = null;
            delete this._loadingMap[url];
        });
    }
    /** 加载资源 */
    public static loadResSync<T>(url: string, type: typeof cc.Asset, onProgress?: (completedCount: number, totalCount: number, item: any) => void): Promise<T> {
        return new Promise((resolve, reject) => {
            if (!onProgress) onProgress = this._onProgress;
            cc.resources.load(url, type, onProgress, (err, asset: any) => {
                if (err) {
                    cc.error(`${url} [资源加载] 错误 ${err}`);
                    resolve(null);
                } else {
                    resolve(asset);
                }
            });
        });
    }
    /** 
     * 加载进度
     * cb方法 其实目的是可以将loader方法的progress
     */
    private static _onProgress(completedCount: number, totalCount: number, item: any) {
        CocosHelper.loadProgress.completedCount = completedCount;
        CocosHelper.loadProgress.totalCount = totalCount;
        CocosHelper.loadProgress.item = item;
        CocosHelper.loadProgress.cb && CocosHelper.loadProgress.cb(completedCount, totalCount, item);
    }
    /**
     * 寻找子节点
     */
    public static findChildInNode(nodeName: string, rootNode: cc.Node): cc.Node {
        if (rootNode.name == nodeName) {
            return rootNode;
        }
        for (let i = 0; i < rootNode.childrenCount; i++) {
            let node = this.findChildInNode(nodeName, rootNode.children[i]);
            if (node) {
                return node;
            }
        }
        return null;
    }

    /** 获得Component的类名 */
    public static getComponentName(com: Function) {
        let arr = com.name.match(/<.*>$/);
        if (arr && arr.length > 0) {
            return arr[0].slice(1, -1);
        }
        return com.name;
    }
    /** 加载bundle */
    public static loadBundleSync(url: string, options: any): Promise<cc.AssetManager.Bundle> {
        return new Promise((resolve, reject) => {
            cc.assetManager.loadBundle(url, options, (err: Error, bundle: cc.AssetManager.Bundle) => {
                if (!err) {
                    cc.error(`加载bundle失败, url: ${url}, err:${err}`);
                    resolve(null);
                } else {
                    resolve(bundle);
                }
            });
        });
    }

    /** 路径是相对分包文件夹路径的相对路径 */
    public static loadAssetFromBundleSync(bundleName: string, url: string | string[]) {
        let bundle = cc.assetManager.getBundle(bundleName);
        if (!bundle) {
            cc.error(`加载bundle中的资源失败, 未找到bundle, bundleUrl:${bundleName}`);
            return null;
        }
        return new Promise((resolve, reject) => {
            bundle.load(url, (err, asset: cc.Asset | cc.Asset[]) => {
                if (err) {
                    cc.error(`加载bundle中的资源失败, 未找到asset, url:${url}, err:${err}`);
                    resolve(null);
                } else {
                    resolve(asset);
                }
            });
        });
    }

    /** 通过路径加载资源, 如果这个资源在bundle内, 会先加载bundle, 在解开bundle获得对应的资源 */
    public static loadAssetSync(url: string | string[]) {
        return new Promise((resolve, reject) => {
            cc.resources.load(url, (err, assets: cc.Asset | cc.Asset[]) => {
                if (!err) {
                    cc.error(`加载asset失败, url:${url}, err: ${err}`);
                    resolve(null);
                } else {
                    this.addRef(assets);
                    resolve(assets);
                }
            });
        });
    }
    /** 释放资源 */
    public static releaseAsset(assets: cc.Asset | cc.Asset[]) {
        this.decRes(assets);
    }
    /** 增加引用计数 */
    private static addRef(assets: cc.Asset | cc.Asset[]) {
        if (assets instanceof Array) {
            for (const a of assets) {
                a.addRef();
            }
        } else {
            assets.addRef();
        }
    }
    /** 减少引用计数, 当引用计数减少到0时,会自动销毁 */
    private static decRes(assets: cc.Asset | cc.Asset[]) {
        if (assets instanceof Array) {
            for (const a of assets) {
                a.decRef();
            }
        } else {
            assets.decRef();
        }
    }

    /** 截图 */
    public static captureScreen(camera: cc.Camera, prop?: cc.Node | cc.Rect) {
        let newTexture = new cc.RenderTexture();
        // let oldTexture = camera.targetTexture;
        let rect: cc.Rect = cc.rect(0, 0, cc.visibleRect.width, cc.visibleRect.height);
        if (prop) {
            if (prop instanceof cc.Node) {
                rect = prop.getBoundingBoxToWorld();
            } else {
                rect = prop;
            }
        }
        newTexture.initWithSize(cc.visibleRect.width, cc.visibleRect.height, cc.game._renderContext.STENCIL_INDEX8);
        camera.targetTexture = newTexture;
        camera.render();
        // camera.targetTexture = oldTexture;

        let buffer = new ArrayBuffer(rect.width * rect.height * 4);
        let data = new Uint8Array(buffer);
        newTexture.readPixels(data, rect.x, rect.y, rect.width, rect.height);
        return data;
    }

    /** 加载json资源 */
    public static loadJsonSync<T>(url: string, type: typeof cc.JsonAsset,): Promise<T> {
        return new Promise((resolve, reject) => {
            cc.resources.load(url, type, (err, asset: any) => {
                if (err) {
                    cc.error(`${url} [资源加载] 错误 ${err}`);
                    resolve(null);
                } else {
                    resolve(asset);
                }
            });
        });
    }

    /** 获得单选项索引 */
    public static getContenerindex(node: cc.Node) {
        if (!node || node.childrenCount < 1) return;

        for (let i = 0; i < node.childrenCount; i++) {
            let item = node.children[i];
            let isChecked = item.getComponent(cc.Toggle).isChecked;
            if (isChecked) {
                return i
            }
        }
    }

    /** 加载头像 */
    public static loadHead(url: string, head: cc.Sprite) {
        return new Promise((resolve, reject) => {
            cc.assetManager.loadRemote(url, (err, res: cc.Texture2D) => {
                if (err) {
                    console.log("headErr:" + err);
                    resolve(null);
                } else {
                    resolve(res);
                }
            });
        }).then((res: cc.Texture2D) => {
            head.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
        })

    }

    /** 设置单个生产物品的图片 */
    public static setItemProduceIcon(id: number, icon: cc.Sprite, callback: Function) {
        return new Promise((resolve, reject) => {
            cc.resources.load("imgs/itemProduce/item_" + id, (err, res: cc.Texture2D) => {
                if (err) {
                    resolve(null);
                } else {
                    resolve(res)
                }
            })
        }).then((res: cc.Texture2D) => {
            icon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
            callback();
        })
    }

    /** 根据物品ID，拿到当前物品信息 */
    public static getItemProduceInfoById(id: number) {
        let produce = GameMgr.dataModalMgr.ItemProfucesJson.itemsInfo.json
        for (let i = 0; i < produce.length; i++) {
            let item = produce[i];
            if (item.id == id) {
                return produce[i];
            }
        }
    }

    /** 根据碎片名字，拿到当前ID */
    public static getChipInfoByName(chipname: string) {
        let chips = GameMgr.dataModalMgr.ChipsInfoJson.chipInfo
        for (let i = 0; i < chips.length; i++) {
            if (chips[i].name == chipname) {
                return chips[i]["chipId"];
            }
        }
    }

    /** 根据碎片ID，拿到当前名字 */
    public static getChipNameByID(chipid: string) {
        let chips = GameMgr.dataModalMgr.ChipsInfoJson.chipInfo;
        for (let i = 0; i < chips.length; i++) {
            if (chips[i].chipId == chipid) {
                return chips[i]["name"];
            }
        }
    }

    /** 根据碎片ID，拿到当前名字 */
    public static getShopNameByID(chipid: string) {
        let shops = GameMgr.dataModalMgr.ShopInfoJson.shopInfo;
        for (let i = 0; i < shops.length; i++) {
            if (shops[i].id == chipid) {
                return shops[i]["name"];
            }
        }
    }

    /** 根据碎片ID，拿到当前名字 */
    public static setShopInfo(name: string, icon: cc.Sprite) {
        return new Promise((resolve, reject) => {
            cc.resources.load("imgs/shops/" + name, (err, res: cc.Texture2D) => {
                if (err) {
                    resolve(null);
                } else {
                    resolve(res);
                }
            })
        }).then((res: cc.Texture2D) => {
            icon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
        })
    }

    /** 设置单个生产物品的图片 */
    public static setLotteryIcon(name: string, icon: cc.Sprite) {
        return new Promise((resolve, reject) => {
            cc.resources.load("imgs/itemProduce/item_" + name, (err, res: cc.Texture2D) => {
                if (err) {
                    resolve(null);
                } else {
                    resolve(res)
                }
            })
        }).then((res: cc.Texture2D) => {
            icon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
        })
    }

    /** 设置物品的图片 */
    public static setDealIcon(name: string, icon: cc.Sprite) {
        return new Promise((resolve, reject) => {
            cc.resources.load("imgs/chip/" + name, (err, res: cc.Texture2D) => {
                if (err) {
                    resolve(null);
                } else {
                    resolve(res)
                }
            })
        }).then((res: cc.Texture2D) => {
            icon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
        })
    }

    /**
     * 根据剩余秒数格式化剩余时间 返回 HH:MM:SS
     * @param {Number} leftSec 
     */
    public static formatTimeForSecond(leftSec: number) {
        var timeStr = '';
        var sec = leftSec % 60;

        var leftMin = Math.floor(leftSec / 60);
        leftMin = leftMin < 0 ? 0 : leftMin;

        var hour = Math.floor(leftMin / 60);
        var min = leftMin % 60;

        if (hour > 0) {
            timeStr += hour > 9 ? hour.toString() : '0' + hour;
            timeStr += ':';
        }

        timeStr += min > 9 ? min.toString() : '0' + min;
        timeStr += ':';
        timeStr += sec > 9 ? sec.toString() : '0' + sec;
        return timeStr;
    }

    /**
     * 时间戳转化
     * @param value 
     * @returns 
     */
    public static changeTime(value: number) {
        let date = new Date(value);
        let YY = date.getFullYear();
        let MM = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        let DD = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        let hh = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        let mm = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        let ss = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

        // 这里修改返回时间的格式
        // return YY + "-" + MM + "-" + DD + " " + hh + ":" + mm + ":" + ss;
        return YY + "-" + MM + "-" + DD
    }

    /**
     * 大数转换
     */
    //     bigNumConver(num: number) {
    //         1000=1K
    // 1000K=1M
    // 1000M=1B
    // 1000B=1T
    // 1000T=1P
    // 1000P=1E
    // 1000E=1Z
    // 1000Z=1Y
    // 1000Y=1S
    // 1000S=1L
    // 1000L=1X
    // 1000X=1D
    //     }
}

