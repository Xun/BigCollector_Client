import { types } from "protobufjs";
import { DataModalMgr } from "./Manager/DataModalMgr";
import FormMgr from "./Manager/FormMgr";
import GameMgr from "./Manager/GameMgr";
import UIManager from "./Manager/UIManager";
import { EventCenter } from "./Net/EventCenter";
import { EventType } from "./Net/EventType";
import { apiClient } from "./Net/RpcConent";
import UIConfig from "./UIConfig";
import UIHome from "./UIScript/Home/UIHome";

const { ccclass, property } = cc._decorator;


@ccclass
export default class Main extends cc.Component {

    onLoad() {

    }

    start() {

        apiClient.listenMsg("UpdateBackpack", (data) => {
            this.upDataChips(data);
        });

        apiClient.listenMsg("GameJson", (data) => {
            this.setGameJsons(data);
        })
    }


    upDataChips(data: any) {
        GameMgr.dataModalMgr.BagInfo.list_item = data.backpack;
        GameMgr.dataModalMgr.BagInfo.total_bag_capacity = data.bagVol;
    }

    setGameJsons(data) {
        for (let key in data.gameJsons.shop) {
            let element = data.gameJsons.shop[key];
            GameMgr.dataModalMgr.ShopInfoJson.shopInfo.push(element);
        }
        GameMgr.dataModalMgr.ChipsInfoJson.chipInfo = data.gameJsons.chips;
    }

    adaptiveNoteLayout() {
        let winSize = cc.winSize;//获取当前游戏窗口大小
        cc.log("--当前游戏窗口大小  w:" + winSize.width + "   h:" + winSize.height);

        let viewSize = cc.view.getFrameSize();
        cc.log("--视图边框尺寸：w:" + viewSize.width + "  h:" + viewSize.height);

        let canvasSize = cc.view.getCanvasSize();//视图中canvas尺寸
        cc.log("--视图中canvas尺寸  w:" + canvasSize.width + "  H:" + canvasSize.height);

        let visibleSize = cc.view.getVisibleSize();
        cc.log("--视图中窗口可见区域的尺寸 w:" + visibleSize.width + "   h:" + visibleSize.height);

        let designSize = cc.view.getDesignResolutionSize();
        cc.log("--设计分辨率：" + designSize.width + "    h: " + designSize.height);

        cc.log("--当前节点的尺寸 w:" + this.node.width + "   h:" + this.node.height);
    }
}
