package com.dscjyx.game.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;

import org.cocos2dx.javascript.AndroidCallJs;
import org.cocos2dx.javascript.AppActivity;

public class WXEntryActivity extends AppActivity implements IWXAPIEventHandler {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppActivity.getInstance().GetAPI().handleIntent(getIntent(), this);
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        AppActivity.getInstance().GetAPI().handleIntent(getIntent(), this);
        finish();
    }
    @Override
    public void onReq(BaseReq req) {
        // TODO Auto-generated method stub
        Log.e("WeiChatLogin", "onReq++++++++++++");
        finish();
    }
    @Override
    //微信登陆成功后，会回调此方法
    public void onResp(BaseResp resp) {

        String result = "";

        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                result = "正确";
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                result = "取消";
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                result = "认证被否决";
                break;
            case BaseResp.ErrCode.ERR_UNSUPPORT:
                result = "不支持错误";
                break;
            default:
                result = "位置错误";
                break;
        }
        Log.e("result：",result);
        switch (resp.getType()) {
            case ConstantsAPI.COMMAND_SENDAUTH:
                final SendAuth.Resp authResp = (SendAuth.Resp) resp;
                //在这里处理的逻辑就可以了
                Log.e("code",authResp.code);
                Log.e("state",authResp.state);
                AndroidCallJs.call(CallWechatFun,authResp.code);
                break;
            case ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX: // 分享给好友、朋友圈
            case ConstantsAPI.COMMAND_PAY_BY_WX:
                break;
        }
        finish();
    }
}
