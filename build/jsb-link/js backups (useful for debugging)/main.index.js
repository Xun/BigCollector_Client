window.__require = function e(t, n, r) {
function o(s, a) {
if (!n[s]) {
if (!t[s]) {
var c = s.split("/");
c = c[c.length - 1];
if (!t[c]) {
var l = "function" == typeof __require && __require;
if (!a && l) return l(c, !0);
if (i) return i(c, !0);
throw new Error("Cannot find module '" + s + "'");
}
s = c;
}
var u = n[s] = {
exports: {}
};
t[s][0].call(u.exports, function(e) {
return o(t[s][1][e] || e);
}, u, u.exports, e, t, n, r);
}
return n[s].exports;
}
for (var i = "function" == typeof __require && __require, s = 0; s < r.length; s++) o(r[s]);
return o;
}({
1: [ function(e, t, n) {
"use strict";
n.byteLength = function(e) {
var t = l(e), n = t[0], r = t[1];
return 3 * (n + r) / 4 - r;
};
n.toByteArray = function(e) {
var t, n, r = l(e), s = r[0], a = r[1], c = new i(u(0, s, a)), p = 0, f = a > 0 ? s - 4 : s;
for (n = 0; n < f; n += 4) {
t = o[e.charCodeAt(n)] << 18 | o[e.charCodeAt(n + 1)] << 12 | o[e.charCodeAt(n + 2)] << 6 | o[e.charCodeAt(n + 3)];
c[p++] = t >> 16 & 255;
c[p++] = t >> 8 & 255;
c[p++] = 255 & t;
}
if (2 === a) {
t = o[e.charCodeAt(n)] << 2 | o[e.charCodeAt(n + 1)] >> 4;
c[p++] = 255 & t;
}
if (1 === a) {
t = o[e.charCodeAt(n)] << 10 | o[e.charCodeAt(n + 1)] << 4 | o[e.charCodeAt(n + 2)] >> 2;
c[p++] = t >> 8 & 255;
c[p++] = 255 & t;
}
return c;
};
n.fromByteArray = function(e) {
for (var t, n = e.length, o = n % 3, i = [], s = 0, a = n - o; s < a; s += 16383) i.push(p(e, s, s + 16383 > a ? a : s + 16383));
if (1 === o) {
t = e[n - 1];
i.push(r[t >> 2] + r[t << 4 & 63] + "==");
} else if (2 === o) {
t = (e[n - 2] << 8) + e[n - 1];
i.push(r[t >> 10] + r[t >> 4 & 63] + r[t << 2 & 63] + "=");
}
return i.join("");
};
for (var r = [], o = [], i = "undefined" != typeof Uint8Array ? Uint8Array : Array, s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", a = 0, c = s.length; a < c; ++a) {
r[a] = s[a];
o[s.charCodeAt(a)] = a;
}
o["-".charCodeAt(0)] = 62;
o["_".charCodeAt(0)] = 63;
function l(e) {
var t = e.length;
if (t % 4 > 0) throw new Error("Invalid string. Length must be a multiple of 4");
var n = e.indexOf("=");
-1 === n && (n = t);
return [ n, n === t ? 0 : 4 - n % 4 ];
}
function u(e, t, n) {
return 3 * (t + n) / 4 - n;
}
function p(e, t, n) {
for (var o, i, s = [], a = t; a < n; a += 3) {
o = (e[a] << 16 & 16711680) + (e[a + 1] << 8 & 65280) + (255 & e[a + 2]);
s.push(r[(i = o) >> 18 & 63] + r[i >> 12 & 63] + r[i >> 6 & 63] + r[63 & i]);
}
return s.join("");
}
}, {} ],
2: [ function(e, t, n) {
(function(t) {
"use strict";
var r = e("base64-js"), o = e("ieee754"), i = e("isarray");
n.Buffer = c;
n.SlowBuffer = function(e) {
+e != e && (e = 0);
return c.alloc(+e);
};
n.INSPECT_MAX_BYTES = 50;
c.TYPED_ARRAY_SUPPORT = void 0 !== t.TYPED_ARRAY_SUPPORT ? t.TYPED_ARRAY_SUPPORT : function() {
try {
var e = new Uint8Array(1);
e.__proto__ = {
__proto__: Uint8Array.prototype,
foo: function() {
return 42;
}
};
return 42 === e.foo() && "function" == typeof e.subarray && 0 === e.subarray(1, 1).byteLength;
} catch (e) {
return !1;
}
}();
n.kMaxLength = s();
function s() {
return c.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823;
}
function a(e, t) {
if (s() < t) throw new RangeError("Invalid typed array length");
if (c.TYPED_ARRAY_SUPPORT) (e = new Uint8Array(t)).__proto__ = c.prototype; else {
null === e && (e = new c(t));
e.length = t;
}
return e;
}
function c(e, t, n) {
if (!(c.TYPED_ARRAY_SUPPORT || this instanceof c)) return new c(e, t, n);
if ("number" == typeof e) {
if ("string" == typeof t) throw new Error("If encoding is specified then the first argument must be a string");
return f(this, e);
}
return l(this, e, t, n);
}
c.poolSize = 8192;
c._augment = function(e) {
e.__proto__ = c.prototype;
return e;
};
function l(e, t, n, r) {
if ("number" == typeof t) throw new TypeError('"value" argument must not be a number');
return "undefined" != typeof ArrayBuffer && t instanceof ArrayBuffer ? y(e, t, n, r) : "string" == typeof t ? d(e, t, n) : v(e, t);
}
c.from = function(e, t, n) {
return l(null, e, t, n);
};
if (c.TYPED_ARRAY_SUPPORT) {
c.prototype.__proto__ = Uint8Array.prototype;
c.__proto__ = Uint8Array;
"undefined" != typeof Symbol && Symbol.species && c[Symbol.species] === c && Object.defineProperty(c, Symbol.species, {
value: null,
configurable: !0
});
}
function u(e) {
if ("number" != typeof e) throw new TypeError('"size" argument must be a number');
if (e < 0) throw new RangeError('"size" argument must not be negative');
}
function p(e, t, n, r) {
u(t);
return t <= 0 ? a(e, t) : void 0 !== n ? "string" == typeof r ? a(e, t).fill(n, r) : a(e, t).fill(n) : a(e, t);
}
c.alloc = function(e, t, n) {
return p(null, e, t, n);
};
function f(e, t) {
u(t);
e = a(e, t < 0 ? 0 : 0 | g(t));
if (!c.TYPED_ARRAY_SUPPORT) for (var n = 0; n < t; ++n) e[n] = 0;
return e;
}
c.allocUnsafe = function(e) {
return f(null, e);
};
c.allocUnsafeSlow = function(e) {
return f(null, e);
};
function d(e, t, n) {
"string" == typeof n && "" !== n || (n = "utf8");
if (!c.isEncoding(n)) throw new TypeError('"encoding" must be a valid string encoding');
var r = 0 | _(t, n), o = (e = a(e, r)).write(t, n);
o !== r && (e = e.slice(0, o));
return e;
}
function h(e, t) {
var n = t.length < 0 ? 0 : 0 | g(t.length);
e = a(e, n);
for (var r = 0; r < n; r += 1) e[r] = 255 & t[r];
return e;
}
function y(e, t, n, r) {
t.byteLength;
if (n < 0 || t.byteLength < n) throw new RangeError("'offset' is out of bounds");
if (t.byteLength < n + (r || 0)) throw new RangeError("'length' is out of bounds");
t = void 0 === n && void 0 === r ? new Uint8Array(t) : void 0 === r ? new Uint8Array(t, n) : new Uint8Array(t, n, r);
c.TYPED_ARRAY_SUPPORT ? (e = t).__proto__ = c.prototype : e = h(e, t);
return e;
}
function v(e, t) {
if (c.isBuffer(t)) {
var n = 0 | g(t.length);
if (0 === (e = a(e, n)).length) return e;
t.copy(e, 0, 0, n);
return e;
}
if (t) {
if ("undefined" != typeof ArrayBuffer && t.buffer instanceof ArrayBuffer || "length" in t) return "number" != typeof t.length || (r = t.length) != r ? a(e, 0) : h(e, t);
if ("Buffer" === t.type && i(t.data)) return h(e, t.data);
}
var r;
throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.");
}
function g(e) {
if (e >= s()) throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + s().toString(16) + " bytes");
return 0 | e;
}
c.isBuffer = function(e) {
return !(null == e || !e._isBuffer);
};
c.compare = function(e, t) {
if (!c.isBuffer(e) || !c.isBuffer(t)) throw new TypeError("Arguments must be Buffers");
if (e === t) return 0;
for (var n = e.length, r = t.length, o = 0, i = Math.min(n, r); o < i; ++o) if (e[o] !== t[o]) {
n = e[o];
r = t[o];
break;
}
return n < r ? -1 : r < n ? 1 : 0;
};
c.isEncoding = function(e) {
switch (String(e).toLowerCase()) {
case "hex":
case "utf8":
case "utf-8":
case "ascii":
case "latin1":
case "binary":
case "base64":
case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return !0;

default:
return !1;
}
};
c.concat = function(e, t) {
if (!i(e)) throw new TypeError('"list" argument must be an Array of Buffers');
if (0 === e.length) return c.alloc(0);
var n;
if (void 0 === t) {
t = 0;
for (n = 0; n < e.length; ++n) t += e[n].length;
}
var r = c.allocUnsafe(t), o = 0;
for (n = 0; n < e.length; ++n) {
var s = e[n];
if (!c.isBuffer(s)) throw new TypeError('"list" argument must be an Array of Buffers');
s.copy(r, o);
o += s.length;
}
return r;
};
function _(e, t) {
if (c.isBuffer(e)) return e.length;
if ("undefined" != typeof ArrayBuffer && "function" == typeof ArrayBuffer.isView && (ArrayBuffer.isView(e) || e instanceof ArrayBuffer)) return e.byteLength;
"string" != typeof e && (e = "" + e);
var n = e.length;
if (0 === n) return 0;
for (var r = !1; ;) switch (t) {
case "ascii":
case "latin1":
case "binary":
return n;

case "utf8":
case "utf-8":
case void 0:
return q(e).length;

case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return 2 * n;

case "hex":
return n >>> 1;

case "base64":
return X(e).length;

default:
if (r) return q(e).length;
t = ("" + t).toLowerCase();
r = !0;
}
}
c.byteLength = _;
function m(e, t, n) {
var r = !1;
(void 0 === t || t < 0) && (t = 0);
if (t > this.length) return "";
(void 0 === n || n > this.length) && (n = this.length);
if (n <= 0) return "";
if ((n >>>= 0) <= (t >>>= 0)) return "";
e || (e = "utf8");
for (;;) switch (e) {
case "hex":
return E(this, t, n);

case "utf8":
case "utf-8":
return x(this, t, n);

case "ascii":
return U(this, t, n);

case "latin1":
case "binary":
return A(this, t, n);

case "base64":
return M(this, t, n);

case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return F(this, t, n);

default:
if (r) throw new TypeError("Unknown encoding: " + e);
e = (e + "").toLowerCase();
r = !0;
}
}
c.prototype._isBuffer = !0;
function b(e, t, n) {
var r = e[t];
e[t] = e[n];
e[n] = r;
}
c.prototype.swap16 = function() {
var e = this.length;
if (e % 2 != 0) throw new RangeError("Buffer size must be a multiple of 16-bits");
for (var t = 0; t < e; t += 2) b(this, t, t + 1);
return this;
};
c.prototype.swap32 = function() {
var e = this.length;
if (e % 4 != 0) throw new RangeError("Buffer size must be a multiple of 32-bits");
for (var t = 0; t < e; t += 4) {
b(this, t, t + 3);
b(this, t + 1, t + 2);
}
return this;
};
c.prototype.swap64 = function() {
var e = this.length;
if (e % 8 != 0) throw new RangeError("Buffer size must be a multiple of 64-bits");
for (var t = 0; t < e; t += 8) {
b(this, t, t + 7);
b(this, t + 1, t + 6);
b(this, t + 2, t + 5);
b(this, t + 3, t + 4);
}
return this;
};
c.prototype.toString = function() {
var e = 0 | this.length;
return 0 === e ? "" : 0 === arguments.length ? x(this, 0, e) : m.apply(this, arguments);
};
c.prototype.equals = function(e) {
if (!c.isBuffer(e)) throw new TypeError("Argument must be a Buffer");
return this === e || 0 === c.compare(this, e);
};
c.prototype.inspect = function() {
var e = "", t = n.INSPECT_MAX_BYTES;
if (this.length > 0) {
e = this.toString("hex", 0, t).match(/.{2}/g).join(" ");
this.length > t && (e += " ... ");
}
return "<Buffer " + e + ">";
};
c.prototype.compare = function(e, t, n, r, o) {
if (!c.isBuffer(e)) throw new TypeError("Argument must be a Buffer");
void 0 === t && (t = 0);
void 0 === n && (n = e ? e.length : 0);
void 0 === r && (r = 0);
void 0 === o && (o = this.length);
if (t < 0 || n > e.length || r < 0 || o > this.length) throw new RangeError("out of range index");
if (r >= o && t >= n) return 0;
if (r >= o) return -1;
if (t >= n) return 1;
if (this === e) return 0;
for (var i = (o >>>= 0) - (r >>>= 0), s = (n >>>= 0) - (t >>>= 0), a = Math.min(i, s), l = this.slice(r, o), u = e.slice(t, n), p = 0; p < a; ++p) if (l[p] !== u[p]) {
i = l[p];
s = u[p];
break;
}
return i < s ? -1 : s < i ? 1 : 0;
};
function w(e, t, n, r, o) {
if (0 === e.length) return -1;
if ("string" == typeof n) {
r = n;
n = 0;
} else n > 2147483647 ? n = 2147483647 : n < -2147483648 && (n = -2147483648);
n = +n;
isNaN(n) && (n = o ? 0 : e.length - 1);
n < 0 && (n = e.length + n);
if (n >= e.length) {
if (o) return -1;
n = e.length - 1;
} else if (n < 0) {
if (!o) return -1;
n = 0;
}
"string" == typeof t && (t = c.from(t, r));
if (c.isBuffer(t)) return 0 === t.length ? -1 : S(e, t, n, r, o);
if ("number" == typeof t) {
t &= 255;
return c.TYPED_ARRAY_SUPPORT && "function" == typeof Uint8Array.prototype.indexOf ? o ? Uint8Array.prototype.indexOf.call(e, t, n) : Uint8Array.prototype.lastIndexOf.call(e, t, n) : S(e, [ t ], n, r, o);
}
throw new TypeError("val must be string, number or Buffer");
}
function S(e, t, n, r, o) {
var i, s = 1, a = e.length, c = t.length;
if (void 0 !== r && ("ucs2" === (r = String(r).toLowerCase()) || "ucs-2" === r || "utf16le" === r || "utf-16le" === r)) {
if (e.length < 2 || t.length < 2) return -1;
s = 2;
a /= 2;
c /= 2;
n /= 2;
}
function l(e, t) {
return 1 === s ? e[t] : e.readUInt16BE(t * s);
}
if (o) {
var u = -1;
for (i = n; i < a; i++) if (l(e, i) === l(t, -1 === u ? 0 : i - u)) {
-1 === u && (u = i);
if (i - u + 1 === c) return u * s;
} else {
-1 !== u && (i -= i - u);
u = -1;
}
} else {
n + c > a && (n = a - c);
for (i = n; i >= 0; i--) {
for (var p = !0, f = 0; f < c; f++) if (l(e, i + f) !== l(t, f)) {
p = !1;
break;
}
if (p) return i;
}
}
return -1;
}
c.prototype.includes = function(e, t, n) {
return -1 !== this.indexOf(e, t, n);
};
c.prototype.indexOf = function(e, t, n) {
return w(this, e, t, n, !0);
};
c.prototype.lastIndexOf = function(e, t, n) {
return w(this, e, t, n, !1);
};
function I(e, t, n, r) {
n = Number(n) || 0;
var o = e.length - n;
r ? (r = Number(r)) > o && (r = o) : r = o;
var i = t.length;
if (i % 2 != 0) throw new TypeError("Invalid hex string");
r > i / 2 && (r = i / 2);
for (var s = 0; s < r; ++s) {
var a = parseInt(t.substr(2 * s, 2), 16);
if (isNaN(a)) return s;
e[n + s] = a;
}
return s;
}
function O(e, t, n, r) {
return K(q(t, e.length - n), e, n, r);
}
function j(e, t, n, r) {
return K(Y(t), e, n, r);
}
function P(e, t, n, r) {
return j(e, t, n, r);
}
function C(e, t, n, r) {
return K(X(t), e, n, r);
}
function T(e, t, n, r) {
return K(Z(t, e.length - n), e, n, r);
}
c.prototype.write = function(e, t, n, r) {
if (void 0 === t) {
r = "utf8";
n = this.length;
t = 0;
} else if (void 0 === n && "string" == typeof t) {
r = t;
n = this.length;
t = 0;
} else {
if (!isFinite(t)) throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
t |= 0;
if (isFinite(n)) {
n |= 0;
void 0 === r && (r = "utf8");
} else {
r = n;
n = void 0;
}
}
var o = this.length - t;
(void 0 === n || n > o) && (n = o);
if (e.length > 0 && (n < 0 || t < 0) || t > this.length) throw new RangeError("Attempt to write outside buffer bounds");
r || (r = "utf8");
for (var i = !1; ;) switch (r) {
case "hex":
return I(this, e, t, n);

case "utf8":
case "utf-8":
return O(this, e, t, n);

case "ascii":
return j(this, e, t, n);

case "latin1":
case "binary":
return P(this, e, t, n);

case "base64":
return C(this, e, t, n);

case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return T(this, e, t, n);

default:
if (i) throw new TypeError("Unknown encoding: " + r);
r = ("" + r).toLowerCase();
i = !0;
}
};
c.prototype.toJSON = function() {
return {
type: "Buffer",
data: Array.prototype.slice.call(this._arr || this, 0)
};
};
function M(e, t, n) {
return 0 === t && n === e.length ? r.fromByteArray(e) : r.fromByteArray(e.slice(t, n));
}
function x(e, t, n) {
n = Math.min(e.length, n);
for (var r = [], o = t; o < n; ) {
var i = e[o], s = null, a = i > 239 ? 4 : i > 223 ? 3 : i > 191 ? 2 : 1;
if (o + a <= n) {
var c, l, u, p;
switch (a) {
case 1:
i < 128 && (s = i);
break;

case 2:
128 == (192 & (c = e[o + 1])) && (p = (31 & i) << 6 | 63 & c) > 127 && (s = p);
break;

case 3:
c = e[o + 1];
l = e[o + 2];
128 == (192 & c) && 128 == (192 & l) && (p = (15 & i) << 12 | (63 & c) << 6 | 63 & l) > 2047 && (p < 55296 || p > 57343) && (s = p);
break;

case 4:
c = e[o + 1];
l = e[o + 2];
u = e[o + 3];
128 == (192 & c) && 128 == (192 & l) && 128 == (192 & u) && (p = (15 & i) << 18 | (63 & c) << 12 | (63 & l) << 6 | 63 & u) > 65535 && p < 1114112 && (s = p);
}
}
if (null === s) {
s = 65533;
a = 1;
} else if (s > 65535) {
s -= 65536;
r.push(s >>> 10 & 1023 | 55296);
s = 56320 | 1023 & s;
}
r.push(s);
o += a;
}
return R(r);
}
var k = 4096;
function R(e) {
var t = e.length;
if (t <= k) return String.fromCharCode.apply(String, e);
for (var n = "", r = 0; r < t; ) n += String.fromCharCode.apply(String, e.slice(r, r += k));
return n;
}
function U(e, t, n) {
var r = "";
n = Math.min(e.length, n);
for (var o = t; o < n; ++o) r += String.fromCharCode(127 & e[o]);
return r;
}
function A(e, t, n) {
var r = "";
n = Math.min(e.length, n);
for (var o = t; o < n; ++o) r += String.fromCharCode(e[o]);
return r;
}
function E(e, t, n) {
var r, o = e.length;
(!t || t < 0) && (t = 0);
(!n || n < 0 || n > o) && (n = o);
for (var i = "", s = t; s < n; ++s) i += (r = e[s]) < 16 ? "0" + r.toString(16) : r.toString(16);
return i;
}
function F(e, t, n) {
for (var r = e.slice(t, n), o = "", i = 0; i < r.length; i += 2) o += String.fromCharCode(r[i] + 256 * r[i + 1]);
return o;
}
c.prototype.slice = function(e, t) {
var n, r = this.length;
(e = ~~e) < 0 ? (e += r) < 0 && (e = 0) : e > r && (e = r);
(t = void 0 === t ? r : ~~t) < 0 ? (t += r) < 0 && (t = 0) : t > r && (t = r);
t < e && (t = e);
if (c.TYPED_ARRAY_SUPPORT) (n = this.subarray(e, t)).__proto__ = c.prototype; else {
var o = t - e;
n = new c(o, void 0);
for (var i = 0; i < o; ++i) n[i] = this[i + e];
}
return n;
};
function B(e, t, n) {
if (e % 1 != 0 || e < 0) throw new RangeError("offset is not uint");
if (e + t > n) throw new RangeError("Trying to access beyond buffer length");
}
c.prototype.readUIntLE = function(e, t, n) {
e |= 0;
t |= 0;
n || B(e, t, this.length);
for (var r = this[e], o = 1, i = 0; ++i < t && (o *= 256); ) r += this[e + i] * o;
return r;
};
c.prototype.readUIntBE = function(e, t, n) {
e |= 0;
t |= 0;
n || B(e, t, this.length);
for (var r = this[e + --t], o = 1; t > 0 && (o *= 256); ) r += this[e + --t] * o;
return r;
};
c.prototype.readUInt8 = function(e, t) {
t || B(e, 1, this.length);
return this[e];
};
c.prototype.readUInt16LE = function(e, t) {
t || B(e, 2, this.length);
return this[e] | this[e + 1] << 8;
};
c.prototype.readUInt16BE = function(e, t) {
t || B(e, 2, this.length);
return this[e] << 8 | this[e + 1];
};
c.prototype.readUInt32LE = function(e, t) {
t || B(e, 4, this.length);
return (this[e] | this[e + 1] << 8 | this[e + 2] << 16) + 16777216 * this[e + 3];
};
c.prototype.readUInt32BE = function(e, t) {
t || B(e, 4, this.length);
return 16777216 * this[e] + (this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3]);
};
c.prototype.readIntLE = function(e, t, n) {
e |= 0;
t |= 0;
n || B(e, t, this.length);
for (var r = this[e], o = 1, i = 0; ++i < t && (o *= 256); ) r += this[e + i] * o;
r >= (o *= 128) && (r -= Math.pow(2, 8 * t));
return r;
};
c.prototype.readIntBE = function(e, t, n) {
e |= 0;
t |= 0;
n || B(e, t, this.length);
for (var r = t, o = 1, i = this[e + --r]; r > 0 && (o *= 256); ) i += this[e + --r] * o;
i >= (o *= 128) && (i -= Math.pow(2, 8 * t));
return i;
};
c.prototype.readInt8 = function(e, t) {
t || B(e, 1, this.length);
return 128 & this[e] ? -1 * (255 - this[e] + 1) : this[e];
};
c.prototype.readInt16LE = function(e, t) {
t || B(e, 2, this.length);
var n = this[e] | this[e + 1] << 8;
return 32768 & n ? 4294901760 | n : n;
};
c.prototype.readInt16BE = function(e, t) {
t || B(e, 2, this.length);
var n = this[e + 1] | this[e] << 8;
return 32768 & n ? 4294901760 | n : n;
};
c.prototype.readInt32LE = function(e, t) {
t || B(e, 4, this.length);
return this[e] | this[e + 1] << 8 | this[e + 2] << 16 | this[e + 3] << 24;
};
c.prototype.readInt32BE = function(e, t) {
t || B(e, 4, this.length);
return this[e] << 24 | this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3];
};
c.prototype.readFloatLE = function(e, t) {
t || B(e, 4, this.length);
return o.read(this, e, !0, 23, 4);
};
c.prototype.readFloatBE = function(e, t) {
t || B(e, 4, this.length);
return o.read(this, e, !1, 23, 4);
};
c.prototype.readDoubleLE = function(e, t) {
t || B(e, 8, this.length);
return o.read(this, e, !0, 52, 8);
};
c.prototype.readDoubleBE = function(e, t) {
t || B(e, 8, this.length);
return o.read(this, e, !1, 52, 8);
};
function L(e, t, n, r, o, i) {
if (!c.isBuffer(e)) throw new TypeError('"buffer" argument must be a Buffer instance');
if (t > o || t < i) throw new RangeError('"value" argument is out of bounds');
if (n + r > e.length) throw new RangeError("Index out of range");
}
c.prototype.writeUIntLE = function(e, t, n, r) {
e = +e;
t |= 0;
n |= 0;
r || L(this, e, t, n, Math.pow(2, 8 * n) - 1, 0);
var o = 1, i = 0;
this[t] = 255 & e;
for (;++i < n && (o *= 256); ) this[t + i] = e / o & 255;
return t + n;
};
c.prototype.writeUIntBE = function(e, t, n, r) {
e = +e;
t |= 0;
n |= 0;
r || L(this, e, t, n, Math.pow(2, 8 * n) - 1, 0);
var o = n - 1, i = 1;
this[t + o] = 255 & e;
for (;--o >= 0 && (i *= 256); ) this[t + o] = e / i & 255;
return t + n;
};
c.prototype.writeUInt8 = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 1, 255, 0);
c.TYPED_ARRAY_SUPPORT || (e = Math.floor(e));
this[t] = 255 & e;
return t + 1;
};
function N(e, t, n, r) {
t < 0 && (t = 65535 + t + 1);
for (var o = 0, i = Math.min(e.length - n, 2); o < i; ++o) e[n + o] = (t & 255 << 8 * (r ? o : 1 - o)) >>> 8 * (r ? o : 1 - o);
}
c.prototype.writeUInt16LE = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 2, 65535, 0);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = 255 & e;
this[t + 1] = e >>> 8;
} else N(this, e, t, !0);
return t + 2;
};
c.prototype.writeUInt16BE = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 2, 65535, 0);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = e >>> 8;
this[t + 1] = 255 & e;
} else N(this, e, t, !1);
return t + 2;
};
function D(e, t, n, r) {
t < 0 && (t = 4294967295 + t + 1);
for (var o = 0, i = Math.min(e.length - n, 4); o < i; ++o) e[n + o] = t >>> 8 * (r ? o : 3 - o) & 255;
}
c.prototype.writeUInt32LE = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 4, 4294967295, 0);
if (c.TYPED_ARRAY_SUPPORT) {
this[t + 3] = e >>> 24;
this[t + 2] = e >>> 16;
this[t + 1] = e >>> 8;
this[t] = 255 & e;
} else D(this, e, t, !0);
return t + 4;
};
c.prototype.writeUInt32BE = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 4, 4294967295, 0);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = e >>> 24;
this[t + 1] = e >>> 16;
this[t + 2] = e >>> 8;
this[t + 3] = 255 & e;
} else D(this, e, t, !1);
return t + 4;
};
c.prototype.writeIntLE = function(e, t, n, r) {
e = +e;
t |= 0;
if (!r) {
var o = Math.pow(2, 8 * n - 1);
L(this, e, t, n, o - 1, -o);
}
var i = 0, s = 1, a = 0;
this[t] = 255 & e;
for (;++i < n && (s *= 256); ) {
e < 0 && 0 === a && 0 !== this[t + i - 1] && (a = 1);
this[t + i] = (e / s >> 0) - a & 255;
}
return t + n;
};
c.prototype.writeIntBE = function(e, t, n, r) {
e = +e;
t |= 0;
if (!r) {
var o = Math.pow(2, 8 * n - 1);
L(this, e, t, n, o - 1, -o);
}
var i = n - 1, s = 1, a = 0;
this[t + i] = 255 & e;
for (;--i >= 0 && (s *= 256); ) {
e < 0 && 0 === a && 0 !== this[t + i + 1] && (a = 1);
this[t + i] = (e / s >> 0) - a & 255;
}
return t + n;
};
c.prototype.writeInt8 = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 1, 127, -128);
c.TYPED_ARRAY_SUPPORT || (e = Math.floor(e));
e < 0 && (e = 255 + e + 1);
this[t] = 255 & e;
return t + 1;
};
c.prototype.writeInt16LE = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 2, 32767, -32768);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = 255 & e;
this[t + 1] = e >>> 8;
} else N(this, e, t, !0);
return t + 2;
};
c.prototype.writeInt16BE = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 2, 32767, -32768);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = e >>> 8;
this[t + 1] = 255 & e;
} else N(this, e, t, !1);
return t + 2;
};
c.prototype.writeInt32LE = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 4, 2147483647, -2147483648);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = 255 & e;
this[t + 1] = e >>> 8;
this[t + 2] = e >>> 16;
this[t + 3] = e >>> 24;
} else D(this, e, t, !0);
return t + 4;
};
c.prototype.writeInt32BE = function(e, t, n) {
e = +e;
t |= 0;
n || L(this, e, t, 4, 2147483647, -2147483648);
e < 0 && (e = 4294967295 + e + 1);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = e >>> 24;
this[t + 1] = e >>> 16;
this[t + 2] = e >>> 8;
this[t + 3] = 255 & e;
} else D(this, e, t, !1);
return t + 4;
};
function G(e, t, n, r) {
if (n + r > e.length) throw new RangeError("Index out of range");
if (n < 0) throw new RangeError("Index out of range");
}
function W(e, t, n, r, i) {
i || G(e, 0, n, 4);
o.write(e, t, n, r, 23, 4);
return n + 4;
}
c.prototype.writeFloatLE = function(e, t, n) {
return W(this, e, t, !0, n);
};
c.prototype.writeFloatBE = function(e, t, n) {
return W(this, e, t, !1, n);
};
function H(e, t, n, r, i) {
i || G(e, 0, n, 8);
o.write(e, t, n, r, 52, 8);
return n + 8;
}
c.prototype.writeDoubleLE = function(e, t, n) {
return H(this, e, t, !0, n);
};
c.prototype.writeDoubleBE = function(e, t, n) {
return H(this, e, t, !1, n);
};
c.prototype.copy = function(e, t, n, r) {
n || (n = 0);
r || 0 === r || (r = this.length);
t >= e.length && (t = e.length);
t || (t = 0);
r > 0 && r < n && (r = n);
if (r === n) return 0;
if (0 === e.length || 0 === this.length) return 0;
if (t < 0) throw new RangeError("targetStart out of bounds");
if (n < 0 || n >= this.length) throw new RangeError("sourceStart out of bounds");
if (r < 0) throw new RangeError("sourceEnd out of bounds");
r > this.length && (r = this.length);
e.length - t < r - n && (r = e.length - t + n);
var o, i = r - n;
if (this === e && n < t && t < r) for (o = i - 1; o >= 0; --o) e[o + t] = this[o + n]; else if (i < 1e3 || !c.TYPED_ARRAY_SUPPORT) for (o = 0; o < i; ++o) e[o + t] = this[o + n]; else Uint8Array.prototype.set.call(e, this.subarray(n, n + i), t);
return i;
};
c.prototype.fill = function(e, t, n, r) {
if ("string" == typeof e) {
if ("string" == typeof t) {
r = t;
t = 0;
n = this.length;
} else if ("string" == typeof n) {
r = n;
n = this.length;
}
if (1 === e.length) {
var o = e.charCodeAt(0);
o < 256 && (e = o);
}
if (void 0 !== r && "string" != typeof r) throw new TypeError("encoding must be a string");
if ("string" == typeof r && !c.isEncoding(r)) throw new TypeError("Unknown encoding: " + r);
} else "number" == typeof e && (e &= 255);
if (t < 0 || this.length < t || this.length < n) throw new RangeError("Out of range index");
if (n <= t) return this;
t >>>= 0;
n = void 0 === n ? this.length : n >>> 0;
e || (e = 0);
var i;
if ("number" == typeof e) for (i = t; i < n; ++i) this[i] = e; else {
var s = c.isBuffer(e) ? e : q(new c(e, r).toString()), a = s.length;
for (i = 0; i < n - t; ++i) this[i + t] = s[i % a];
}
return this;
};
var V = /[^+\/0-9A-Za-z-_]/g;
function z(e) {
if ((e = J(e).replace(V, "")).length < 2) return "";
for (;e.length % 4 != 0; ) e += "=";
return e;
}
function J(e) {
return e.trim ? e.trim() : e.replace(/^\s+|\s+$/g, "");
}
function q(e, t) {
t = t || Infinity;
for (var n, r = e.length, o = null, i = [], s = 0; s < r; ++s) {
if ((n = e.charCodeAt(s)) > 55295 && n < 57344) {
if (!o) {
if (n > 56319) {
(t -= 3) > -1 && i.push(239, 191, 189);
continue;
}
if (s + 1 === r) {
(t -= 3) > -1 && i.push(239, 191, 189);
continue;
}
o = n;
continue;
}
if (n < 56320) {
(t -= 3) > -1 && i.push(239, 191, 189);
o = n;
continue;
}
n = 65536 + (o - 55296 << 10 | n - 56320);
} else o && (t -= 3) > -1 && i.push(239, 191, 189);
o = null;
if (n < 128) {
if ((t -= 1) < 0) break;
i.push(n);
} else if (n < 2048) {
if ((t -= 2) < 0) break;
i.push(n >> 6 | 192, 63 & n | 128);
} else if (n < 65536) {
if ((t -= 3) < 0) break;
i.push(n >> 12 | 224, n >> 6 & 63 | 128, 63 & n | 128);
} else {
if (!(n < 1114112)) throw new Error("Invalid code point");
if ((t -= 4) < 0) break;
i.push(n >> 18 | 240, n >> 12 & 63 | 128, n >> 6 & 63 | 128, 63 & n | 128);
}
}
return i;
}
function Y(e) {
for (var t = [], n = 0; n < e.length; ++n) t.push(255 & e.charCodeAt(n));
return t;
}
function Z(e, t) {
for (var n, r, o, i = [], s = 0; s < e.length && !((t -= 2) < 0); ++s) {
r = (n = e.charCodeAt(s)) >> 8;
o = n % 256;
i.push(o);
i.push(r);
}
return i;
}
function X(e) {
return r.toByteArray(z(e));
}
function K(e, t, n, r) {
for (var o = 0; o < r && !(o + n >= t.length || o >= e.length); ++o) t[o + n] = e[o];
return o;
}
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"base64-js": 1,
ieee754: 4,
isarray: 3
} ],
3: [ function(e, t) {
var n = {}.toString;
t.exports = Array.isArray || function(e) {
return "[object Array]" == n.call(e);
};
}, {} ],
4: [ function(e, t, n) {
n.read = function(e, t, n, r, o) {
var i, s, a = 8 * o - r - 1, c = (1 << a) - 1, l = c >> 1, u = -7, p = n ? o - 1 : 0, f = n ? -1 : 1, d = e[t + p];
p += f;
i = d & (1 << -u) - 1;
d >>= -u;
u += a;
for (;u > 0; i = 256 * i + e[t + p], p += f, u -= 8) ;
s = i & (1 << -u) - 1;
i >>= -u;
u += r;
for (;u > 0; s = 256 * s + e[t + p], p += f, u -= 8) ;
if (0 === i) i = 1 - l; else {
if (i === c) return s ? NaN : Infinity * (d ? -1 : 1);
s += Math.pow(2, r);
i -= l;
}
return (d ? -1 : 1) * s * Math.pow(2, i - r);
};
n.write = function(e, t, n, r, o, i) {
var s, a, c, l = 8 * i - o - 1, u = (1 << l) - 1, p = u >> 1, f = 23 === o ? Math.pow(2, -24) - Math.pow(2, -77) : 0, d = r ? 0 : i - 1, h = r ? 1 : -1, y = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0;
t = Math.abs(t);
if (isNaN(t) || Infinity === t) {
a = isNaN(t) ? 1 : 0;
s = u;
} else {
s = Math.floor(Math.log(t) / Math.LN2);
if (t * (c = Math.pow(2, -s)) < 1) {
s--;
c *= 2;
}
if ((t += s + p >= 1 ? f / c : f * Math.pow(2, 1 - p)) * c >= 2) {
s++;
c /= 2;
}
if (s + p >= u) {
a = 0;
s = u;
} else if (s + p >= 1) {
a = (t * c - 1) * Math.pow(2, o);
s += p;
} else {
a = t * Math.pow(2, p - 1) * Math.pow(2, o);
s = 0;
}
}
for (;o >= 8; e[n + d] = 255 & a, d += h, a /= 256, o -= 8) ;
s = s << o | a;
l += o;
for (;l > 0; e[n + d] = 255 & s, d += h, s /= 256, l -= 8) ;
e[n + d - h] |= 128 * y;
};
}, {} ],
5: [ function(e, t) {
"use strict";
t.exports = function(e, t) {
for (var n = new Array(arguments.length - 1), r = 0, o = 2, i = !0; o < arguments.length; ) n[r++] = arguments[o++];
return new Promise(function(o, s) {
n[r] = function(e) {
if (i) {
i = !1;
if (e) s(e); else {
for (var t = new Array(arguments.length - 1), n = 0; n < t.length; ) t[n++] = arguments[n];
o.apply(null, t);
}
}
};
try {
e.apply(t || null, n);
} catch (e) {
if (i) {
i = !1;
s(e);
}
}
});
};
}, {} ],
6: [ function(e, t, n) {
"use strict";
var r = n;
r.length = function(e) {
var t = e.length;
if (!t) return 0;
for (var n = 0; --t % 4 > 1 && "=" === e.charAt(t); ) ++n;
return Math.ceil(3 * e.length) / 4 - n;
};
for (var o = new Array(64), i = new Array(123), s = 0; s < 64; ) i[o[s] = s < 26 ? s + 65 : s < 52 ? s + 71 : s < 62 ? s - 4 : s - 59 | 43] = s++;
r.encode = function(e, t, n) {
for (var r, i = null, s = [], a = 0, c = 0; t < n; ) {
var l = e[t++];
switch (c) {
case 0:
s[a++] = o[l >> 2];
r = (3 & l) << 4;
c = 1;
break;

case 1:
s[a++] = o[r | l >> 4];
r = (15 & l) << 2;
c = 2;
break;

case 2:
s[a++] = o[r | l >> 6];
s[a++] = o[63 & l];
c = 0;
}
if (a > 8191) {
(i || (i = [])).push(String.fromCharCode.apply(String, s));
a = 0;
}
}
if (c) {
s[a++] = o[r];
s[a++] = 61;
1 === c && (s[a++] = 61);
}
if (i) {
a && i.push(String.fromCharCode.apply(String, s.slice(0, a)));
return i.join("");
}
return String.fromCharCode.apply(String, s.slice(0, a));
};
r.decode = function(e, t, n) {
for (var r, o = n, s = 0, a = 0; a < e.length; ) {
var c = e.charCodeAt(a++);
if (61 === c && s > 1) break;
if (void 0 === (c = i[c])) throw Error("invalid encoding");
switch (s) {
case 0:
r = c;
s = 1;
break;

case 1:
t[n++] = r << 2 | (48 & c) >> 4;
r = c;
s = 2;
break;

case 2:
t[n++] = (15 & r) << 4 | (60 & c) >> 2;
r = c;
s = 3;
break;

case 3:
t[n++] = (3 & r) << 6 | c;
s = 0;
}
}
if (1 === s) throw Error("invalid encoding");
return n - o;
};
r.test = function(e) {
return /^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$/.test(e);
};
}, {} ],
7: [ function(e, t) {
"use strict";
t.exports = n;
function n() {
this._listeners = {};
}
n.prototype.on = function(e, t, n) {
(this._listeners[e] || (this._listeners[e] = [])).push({
fn: t,
ctx: n || this
});
return this;
};
n.prototype.off = function(e, t) {
if (void 0 === e) this._listeners = {}; else if (void 0 === t) this._listeners[e] = []; else for (var n = this._listeners[e], r = 0; r < n.length; ) n[r].fn === t ? n.splice(r, 1) : ++r;
return this;
};
n.prototype.emit = function(e) {
var t = this._listeners[e];
if (t) {
for (var n = [], r = 1; r < arguments.length; ) n.push(arguments[r++]);
for (r = 0; r < t.length; ) t[r].fn.apply(t[r++].ctx, n);
}
return this;
};
}, {} ],
8: [ function(e, t) {
"use strict";
t.exports = n(n);
function n(e) {
"undefined" != typeof Float32Array ? function() {
var t = new Float32Array([ -0 ]), n = new Uint8Array(t.buffer), r = 128 === n[3];
function o(e, r, o) {
t[0] = e;
r[o] = n[0];
r[o + 1] = n[1];
r[o + 2] = n[2];
r[o + 3] = n[3];
}
function i(e, r, o) {
t[0] = e;
r[o] = n[3];
r[o + 1] = n[2];
r[o + 2] = n[1];
r[o + 3] = n[0];
}
e.writeFloatLE = r ? o : i;
e.writeFloatBE = r ? i : o;
function s(e, r) {
n[0] = e[r];
n[1] = e[r + 1];
n[2] = e[r + 2];
n[3] = e[r + 3];
return t[0];
}
function a(e, r) {
n[3] = e[r];
n[2] = e[r + 1];
n[1] = e[r + 2];
n[0] = e[r + 3];
return t[0];
}
e.readFloatLE = r ? s : a;
e.readFloatBE = r ? a : s;
}() : function() {
function t(e, t, n, r) {
var o = t < 0 ? 1 : 0;
o && (t = -t);
if (0 === t) e(1 / t > 0 ? 0 : 2147483648, n, r); else if (isNaN(t)) e(2143289344, n, r); else if (t > 34028234663852886e22) e((o << 31 | 2139095040) >>> 0, n, r); else if (t < 11754943508222875e-54) e((o << 31 | Math.round(t / 1401298464324817e-60)) >>> 0, n, r); else {
var i = Math.floor(Math.log(t) / Math.LN2);
e((o << 31 | i + 127 << 23 | 8388607 & Math.round(t * Math.pow(2, -i) * 8388608)) >>> 0, n, r);
}
}
e.writeFloatLE = t.bind(null, r);
e.writeFloatBE = t.bind(null, o);
function n(e, t, n) {
var r = e(t, n), o = 2 * (r >> 31) + 1, i = r >>> 23 & 255, s = 8388607 & r;
return 255 === i ? s ? NaN : Infinity * o : 0 === i ? 1401298464324817e-60 * o * s : o * Math.pow(2, i - 150) * (s + 8388608);
}
e.readFloatLE = n.bind(null, i);
e.readFloatBE = n.bind(null, s);
}();
"undefined" != typeof Float64Array ? function() {
var t = new Float64Array([ -0 ]), n = new Uint8Array(t.buffer), r = 128 === n[7];
function o(e, r, o) {
t[0] = e;
r[o] = n[0];
r[o + 1] = n[1];
r[o + 2] = n[2];
r[o + 3] = n[3];
r[o + 4] = n[4];
r[o + 5] = n[5];
r[o + 6] = n[6];
r[o + 7] = n[7];
}
function i(e, r, o) {
t[0] = e;
r[o] = n[7];
r[o + 1] = n[6];
r[o + 2] = n[5];
r[o + 3] = n[4];
r[o + 4] = n[3];
r[o + 5] = n[2];
r[o + 6] = n[1];
r[o + 7] = n[0];
}
e.writeDoubleLE = r ? o : i;
e.writeDoubleBE = r ? i : o;
function s(e, r) {
n[0] = e[r];
n[1] = e[r + 1];
n[2] = e[r + 2];
n[3] = e[r + 3];
n[4] = e[r + 4];
n[5] = e[r + 5];
n[6] = e[r + 6];
n[7] = e[r + 7];
return t[0];
}
function a(e, r) {
n[7] = e[r];
n[6] = e[r + 1];
n[5] = e[r + 2];
n[4] = e[r + 3];
n[3] = e[r + 4];
n[2] = e[r + 5];
n[1] = e[r + 6];
n[0] = e[r + 7];
return t[0];
}
e.readDoubleLE = r ? s : a;
e.readDoubleBE = r ? a : s;
}() : function() {
function t(e, t, n, r, o, i) {
var s = r < 0 ? 1 : 0;
s && (r = -r);
if (0 === r) {
e(0, o, i + t);
e(1 / r > 0 ? 0 : 2147483648, o, i + n);
} else if (isNaN(r)) {
e(0, o, i + t);
e(2146959360, o, i + n);
} else if (r > 17976931348623157e292) {
e(0, o, i + t);
e((s << 31 | 2146435072) >>> 0, o, i + n);
} else {
var a;
if (r < 22250738585072014e-324) {
e((a = r / 5e-324) >>> 0, o, i + t);
e((s << 31 | a / 4294967296) >>> 0, o, i + n);
} else {
var c = Math.floor(Math.log(r) / Math.LN2);
1024 === c && (c = 1023);
e(4503599627370496 * (a = r * Math.pow(2, -c)) >>> 0, o, i + t);
e((s << 31 | c + 1023 << 20 | 1048576 * a & 1048575) >>> 0, o, i + n);
}
}
}
e.writeDoubleLE = t.bind(null, r, 0, 4);
e.writeDoubleBE = t.bind(null, o, 4, 0);
function n(e, t, n, r, o) {
var i = e(r, o + t), s = e(r, o + n), a = 2 * (s >> 31) + 1, c = s >>> 20 & 2047, l = 4294967296 * (1048575 & s) + i;
return 2047 === c ? l ? NaN : Infinity * a : 0 === c ? 5e-324 * a * l : a * Math.pow(2, c - 1075) * (l + 4503599627370496);
}
e.readDoubleLE = n.bind(null, i, 0, 4);
e.readDoubleBE = n.bind(null, s, 4, 0);
}();
return e;
}
function r(e, t, n) {
t[n] = 255 & e;
t[n + 1] = e >>> 8 & 255;
t[n + 2] = e >>> 16 & 255;
t[n + 3] = e >>> 24;
}
function o(e, t, n) {
t[n] = e >>> 24;
t[n + 1] = e >>> 16 & 255;
t[n + 2] = e >>> 8 & 255;
t[n + 3] = 255 & e;
}
function i(e, t) {
return (e[t] | e[t + 1] << 8 | e[t + 2] << 16 | e[t + 3] << 24) >>> 0;
}
function s(e, t) {
return (e[t] << 24 | e[t + 1] << 16 | e[t + 2] << 8 | e[t + 3]) >>> 0;
}
}, {} ],
9: [ function(require, module, exports) {
"use strict";
module.exports = inquire;
function inquire(moduleName) {
try {
var mod = eval("quire".replace(/^/, "re"))(moduleName);
if (mod && (mod.length || Object.keys(mod).length)) return mod;
} catch (e) {}
return null;
}
}, {} ],
10: [ function(e, t) {
"use strict";
t.exports = function(e, t, n) {
var r = n || 8192, o = r >>> 1, i = null, s = r;
return function(n) {
if (n < 1 || n > o) return e(n);
if (s + n > r) {
i = e(r);
s = 0;
}
var a = t.call(i, s, s += n);
7 & s && (s = 1 + (7 | s));
return a;
};
};
}, {} ],
11: [ function(e, t, n) {
"use strict";
var r = n;
r.length = function(e) {
for (var t = 0, n = 0, r = 0; r < e.length; ++r) if ((n = e.charCodeAt(r)) < 128) t += 1; else if (n < 2048) t += 2; else if (55296 == (64512 & n) && 56320 == (64512 & e.charCodeAt(r + 1))) {
++r;
t += 4;
} else t += 3;
return t;
};
r.read = function(e, t, n) {
if (n - t < 1) return "";
for (var r, o = null, i = [], s = 0; t < n; ) {
if ((r = e[t++]) < 128) i[s++] = r; else if (r > 191 && r < 224) i[s++] = (31 & r) << 6 | 63 & e[t++]; else if (r > 239 && r < 365) {
r = ((7 & r) << 18 | (63 & e[t++]) << 12 | (63 & e[t++]) << 6 | 63 & e[t++]) - 65536;
i[s++] = 55296 + (r >> 10);
i[s++] = 56320 + (1023 & r);
} else i[s++] = (15 & r) << 12 | (63 & e[t++]) << 6 | 63 & e[t++];
if (s > 8191) {
(o || (o = [])).push(String.fromCharCode.apply(String, i));
s = 0;
}
}
if (o) {
s && o.push(String.fromCharCode.apply(String, i.slice(0, s)));
return o.join("");
}
return String.fromCharCode.apply(String, i.slice(0, s));
};
r.write = function(e, t, n) {
for (var r, o, i = n, s = 0; s < e.length; ++s) if ((r = e.charCodeAt(s)) < 128) t[n++] = r; else if (r < 2048) {
t[n++] = r >> 6 | 192;
t[n++] = 63 & r | 128;
} else if (55296 == (64512 & r) && 56320 == (64512 & (o = e.charCodeAt(s + 1)))) {
r = 65536 + ((1023 & r) << 10) + (1023 & o);
++s;
t[n++] = r >> 18 | 240;
t[n++] = r >> 12 & 63 | 128;
t[n++] = r >> 6 & 63 | 128;
t[n++] = 63 & r | 128;
} else {
t[n++] = r >> 12 | 224;
t[n++] = r >> 6 & 63 | 128;
t[n++] = 63 & r | 128;
}
return n - i;
};
}, {} ],
12: [ function(e, t) {
var n = e("../internals/is-callable"), r = e("../internals/try-to-string"), o = TypeError;
t.exports = function(e) {
if (n(e)) return e;
throw o(r(e) + " is not a function");
};
}, {
"../internals/is-callable": 100,
"../internals/try-to-string": 178
} ],
13: [ function(e, t) {
var n = e("../internals/is-constructor"), r = e("../internals/try-to-string"), o = TypeError;
t.exports = function(e) {
if (n(e)) return e;
throw o(r(e) + " is not a constructor");
};
}, {
"../internals/is-constructor": 101,
"../internals/try-to-string": 178
} ],
14: [ function(e, t) {
var n = e("../internals/is-callable"), r = String, o = TypeError;
t.exports = function(e) {
if ("object" == typeof e || n(e)) return e;
throw o("Can't set " + r(e) + " as a prototype");
};
}, {
"../internals/is-callable": 100
} ],
15: [ function(e, t) {
var n = e("../internals/well-known-symbol"), r = e("../internals/object-create"), o = e("../internals/object-define-property").f, i = n("unscopables"), s = Array.prototype;
null == s[i] && o(s, i, {
configurable: !0,
value: r(null)
});
t.exports = function(e) {
s[i][e] = !0;
};
}, {
"../internals/object-create": 123,
"../internals/object-define-property": 125,
"../internals/well-known-symbol": 189
} ],
16: [ function(e, t) {
"use strict";
var n = e("../internals/string-multibyte").charAt;
t.exports = function(e, t, r) {
return t + (r ? n(e, t).length : 1);
};
}, {
"../internals/string-multibyte": 160
} ],
17: [ function(e, t) {
var n = e("../internals/object-is-prototype-of"), r = TypeError;
t.exports = function(e, t) {
if (n(t, e)) return e;
throw r("Incorrect invocation");
};
}, {
"../internals/object-is-prototype-of": 130
} ],
18: [ function(e, t) {
var n = e("../internals/is-object"), r = String, o = TypeError;
t.exports = function(e) {
if (n(e)) return e;
throw o(r(e) + " is not an object");
};
}, {
"../internals/is-object": 105
} ],
19: [ function(e, t) {
t.exports = "undefined" != typeof ArrayBuffer && "undefined" != typeof DataView;
}, {} ],
20: [ function(e, t) {
"use strict";
var n, r, o, i = e("../internals/array-buffer-basic-detection"), s = e("../internals/descriptors"), a = e("../internals/global"), c = e("../internals/is-callable"), l = e("../internals/is-object"), u = e("../internals/has-own-property"), p = e("../internals/classof"), f = e("../internals/try-to-string"), d = e("../internals/create-non-enumerable-property"), h = e("../internals/define-built-in"), y = e("../internals/object-define-property").f, v = e("../internals/object-is-prototype-of"), g = e("../internals/object-get-prototype-of"), _ = e("../internals/object-set-prototype-of"), m = e("../internals/well-known-symbol"), b = e("../internals/uid"), w = e("../internals/internal-state"), S = w.enforce, I = w.get, O = a.Int8Array, j = O && O.prototype, P = a.Uint8ClampedArray, C = P && P.prototype, T = O && g(O), M = j && g(j), x = Object.prototype, k = a.TypeError, R = m("toStringTag"), U = b("TYPED_ARRAY_TAG"), A = i && !!_ && "Opera" !== p(a.opera), E = !1, F = {
Int8Array: 1,
Uint8Array: 1,
Uint8ClampedArray: 1,
Int16Array: 2,
Uint16Array: 2,
Int32Array: 4,
Uint32Array: 4,
Float32Array: 4,
Float64Array: 8
}, B = {
BigInt64Array: 8,
BigUint64Array: 8
}, L = function(e) {
var t = g(e);
if (l(t)) {
var n = I(t);
return n && u(n, "TypedArrayConstructor") ? n.TypedArrayConstructor : L(t);
}
}, N = function(e) {
if (!l(e)) return !1;
var t = p(e);
return u(F, t) || u(B, t);
};
for (n in F) (o = (r = a[n]) && r.prototype) ? S(o).TypedArrayConstructor = r : A = !1;
for (n in B) (o = (r = a[n]) && r.prototype) && (S(o).TypedArrayConstructor = r);
if (!A || !c(T) || T === Function.prototype) {
T = function() {
throw k("Incorrect invocation");
};
if (A) for (n in F) a[n] && _(a[n], T);
}
if (!A || !M || M === x) {
M = T.prototype;
if (A) for (n in F) a[n] && _(a[n].prototype, M);
}
A && g(C) !== M && _(C, M);
if (s && !u(M, R)) {
E = !0;
y(M, R, {
get: function() {
return l(this) ? this[U] : void 0;
}
});
for (n in F) a[n] && d(a[n], U, n);
}
t.exports = {
NATIVE_ARRAY_BUFFER_VIEWS: A,
TYPED_ARRAY_TAG: E && U,
aTypedArray: function(e) {
if (N(e)) return e;
throw k("Target is not a typed array");
},
aTypedArrayConstructor: function(e) {
if (c(e) && (!_ || v(T, e))) return e;
throw k(f(e) + " is not a typed array constructor");
},
exportTypedArrayMethod: function(e, t, n, r) {
if (s) {
if (n) for (var o in F) {
var i = a[o];
if (i && u(i.prototype, e)) try {
delete i.prototype[e];
} catch (n) {
try {
i.prototype[e] = t;
} catch (e) {}
}
}
M[e] && !n || h(M, e, n ? t : A && j[e] || t, r);
}
},
exportTypedArrayStaticMethod: function(e, t, n) {
var r, o;
if (s) {
if (_) {
if (n) for (r in F) if ((o = a[r]) && u(o, e)) try {
delete o[e];
} catch (e) {}
if (T[e] && !n) return;
try {
return h(T, e, n ? t : A && T[e] || t);
} catch (e) {}
}
for (r in F) !(o = a[r]) || o[e] && !n || h(o, e, t);
}
},
getTypedArrayConstructor: L,
isView: function(e) {
if (!l(e)) return !1;
var t = p(e);
return "DataView" === t || u(F, t) || u(B, t);
},
isTypedArray: N,
TypedArray: T,
TypedArrayPrototype: M
};
}, {
"../internals/array-buffer-basic-detection": 19,
"../internals/classof": 42,
"../internals/create-non-enumerable-property": 47,
"../internals/define-built-in": 51,
"../internals/descriptors": 55,
"../internals/global": 86,
"../internals/has-own-property": 87,
"../internals/internal-state": 96,
"../internals/is-callable": 100,
"../internals/is-object": 105,
"../internals/object-define-property": 125,
"../internals/object-get-prototype-of": 129,
"../internals/object-is-prototype-of": 130,
"../internals/object-set-prototype-of": 134,
"../internals/try-to-string": 178,
"../internals/uid": 184,
"../internals/well-known-symbol": 189
} ],
21: [ function(e, t) {
"use strict";
var n = e("../internals/global"), r = e("../internals/function-uncurry-this"), o = e("../internals/descriptors"), i = e("../internals/array-buffer-basic-detection"), s = e("../internals/function-name"), a = e("../internals/create-non-enumerable-property"), c = e("../internals/define-built-ins"), l = e("../internals/fails"), u = e("../internals/an-instance"), p = e("../internals/to-integer-or-infinity"), f = e("../internals/to-length"), d = e("../internals/to-index"), h = e("../internals/ieee754"), y = e("../internals/object-get-prototype-of"), v = e("../internals/object-set-prototype-of"), g = e("../internals/object-get-own-property-names").f, _ = e("../internals/object-define-property").f, m = e("../internals/array-fill"), b = e("../internals/array-slice-simple"), w = e("../internals/set-to-string-tag"), S = e("../internals/internal-state"), I = s.PROPER, O = s.CONFIGURABLE, j = S.get, P = S.set, C = n.ArrayBuffer, T = C, M = T && T.prototype, x = n.DataView, k = x && x.prototype, R = Object.prototype, U = n.Array, A = n.RangeError, E = r(m), F = r([].reverse), B = h.pack, L = h.unpack, N = function(e) {
return [ 255 & e ];
}, D = function(e) {
return [ 255 & e, e >> 8 & 255 ];
}, G = function(e) {
return [ 255 & e, e >> 8 & 255, e >> 16 & 255, e >> 24 & 255 ];
}, W = function(e) {
return e[3] << 24 | e[2] << 16 | e[1] << 8 | e[0];
}, H = function(e) {
return B(e, 23, 4);
}, V = function(e) {
return B(e, 52, 8);
}, z = function(e, t) {
_(e.prototype, t, {
get: function() {
return j(this)[t];
}
});
}, J = function(e, t, n, r) {
var o = d(n), i = j(e);
if (o + t > i.byteLength) throw A("Wrong index");
var s = j(i.buffer).bytes, a = o + i.byteOffset, c = b(s, a, a + t);
return r ? c : F(c);
}, q = function(e, t, n, r, o, i) {
var s = d(n), a = j(e);
if (s + t > a.byteLength) throw A("Wrong index");
for (var c = j(a.buffer).bytes, l = s + a.byteOffset, u = r(+o), p = 0; p < t; p++) c[l + p] = u[i ? p : t - p - 1];
};
if (i) {
var Y = I && "ArrayBuffer" !== C.name;
if (l(function() {
C(1);
}) && l(function() {
new C(-1);
}) && !l(function() {
new C();
new C(1.5);
new C(NaN);
return 1 != C.length || Y && !O;
})) Y && O && a(C, "name", "ArrayBuffer"); else {
(T = function(e) {
u(this, M);
return new C(d(e));
}).prototype = M;
for (var Z, X = g(C), K = 0; X.length > K; ) (Z = X[K++]) in T || a(T, Z, C[Z]);
M.constructor = T;
}
v && y(k) !== R && v(k, R);
var Q = new x(new T(2)), $ = r(k.setInt8);
Q.setInt8(0, 2147483648);
Q.setInt8(1, 2147483649);
!Q.getInt8(0) && Q.getInt8(1) || c(k, {
setInt8: function(e, t) {
$(this, e, t << 24 >> 24);
},
setUint8: function(e, t) {
$(this, e, t << 24 >> 24);
}
}, {
unsafe: !0
});
} else {
M = (T = function(e) {
u(this, M);
var t = d(e);
P(this, {
bytes: E(U(t), 0),
byteLength: t
});
o || (this.byteLength = t);
}).prototype;
k = (x = function(e, t, n) {
u(this, k);
u(e, M);
var r = j(e).byteLength, i = p(t);
if (i < 0 || i > r) throw A("Wrong offset");
if (i + (n = void 0 === n ? r - i : f(n)) > r) throw A("Wrong length");
P(this, {
buffer: e,
byteLength: n,
byteOffset: i
});
if (!o) {
this.buffer = e;
this.byteLength = n;
this.byteOffset = i;
}
}).prototype;
if (o) {
z(T, "byteLength");
z(x, "buffer");
z(x, "byteLength");
z(x, "byteOffset");
}
c(k, {
getInt8: function(e) {
return J(this, 1, e)[0] << 24 >> 24;
},
getUint8: function(e) {
return J(this, 1, e)[0];
},
getInt16: function(e) {
var t = J(this, 2, e, arguments.length > 1 ? arguments[1] : void 0);
return (t[1] << 8 | t[0]) << 16 >> 16;
},
getUint16: function(e) {
var t = J(this, 2, e, arguments.length > 1 ? arguments[1] : void 0);
return t[1] << 8 | t[0];
},
getInt32: function(e) {
return W(J(this, 4, e, arguments.length > 1 ? arguments[1] : void 0));
},
getUint32: function(e) {
return W(J(this, 4, e, arguments.length > 1 ? arguments[1] : void 0)) >>> 0;
},
getFloat32: function(e) {
return L(J(this, 4, e, arguments.length > 1 ? arguments[1] : void 0), 23);
},
getFloat64: function(e) {
return L(J(this, 8, e, arguments.length > 1 ? arguments[1] : void 0), 52);
},
setInt8: function(e, t) {
q(this, 1, e, N, t);
},
setUint8: function(e, t) {
q(this, 1, e, N, t);
},
setInt16: function(e, t) {
q(this, 2, e, D, t, arguments.length > 2 ? arguments[2] : void 0);
},
setUint16: function(e, t) {
q(this, 2, e, D, t, arguments.length > 2 ? arguments[2] : void 0);
},
setInt32: function(e, t) {
q(this, 4, e, G, t, arguments.length > 2 ? arguments[2] : void 0);
},
setUint32: function(e, t) {
q(this, 4, e, G, t, arguments.length > 2 ? arguments[2] : void 0);
},
setFloat32: function(e, t) {
q(this, 4, e, H, t, arguments.length > 2 ? arguments[2] : void 0);
},
setFloat64: function(e, t) {
q(this, 8, e, V, t, arguments.length > 2 ? arguments[2] : void 0);
}
});
}
w(T, "ArrayBuffer");
w(x, "DataView");
t.exports = {
ArrayBuffer: T,
DataView: x
};
}, {
"../internals/an-instance": 17,
"../internals/array-buffer-basic-detection": 19,
"../internals/array-fill": 23,
"../internals/array-slice-simple": 34,
"../internals/create-non-enumerable-property": 47,
"../internals/define-built-ins": 52,
"../internals/descriptors": 55,
"../internals/fails": 74,
"../internals/function-name": 80,
"../internals/function-uncurry-this": 81,
"../internals/global": 86,
"../internals/ieee754": 92,
"../internals/internal-state": 96,
"../internals/object-define-property": 125,
"../internals/object-get-own-property-names": 127,
"../internals/object-get-prototype-of": 129,
"../internals/object-set-prototype-of": 134,
"../internals/set-to-string-tag": 155,
"../internals/to-index": 167,
"../internals/to-integer-or-infinity": 169,
"../internals/to-length": 170
} ],
22: [ function(e, t) {
"use strict";
var n = e("../internals/to-object"), r = e("../internals/to-absolute-index"), o = e("../internals/length-of-array-like"), i = e("../internals/delete-property-or-throw"), s = Math.min;
t.exports = [].copyWithin || function(e, t) {
var a = n(this), c = o(a), l = r(e, c), u = r(t, c), p = arguments.length > 2 ? arguments[2] : void 0, f = s((void 0 === p ? c : r(p, c)) - u, c - l), d = 1;
if (u < l && l < u + f) {
d = -1;
u += f - 1;
l += f - 1;
}
for (;f-- > 0; ) {
u in a ? a[l] = a[u] : i(a, l);
l += d;
u += d;
}
return a;
};
}, {
"../internals/delete-property-or-throw": 54,
"../internals/length-of-array-like": 115,
"../internals/to-absolute-index": 165,
"../internals/to-object": 171
} ],
23: [ function(e, t) {
"use strict";
var n = e("../internals/to-object"), r = e("../internals/to-absolute-index"), o = e("../internals/length-of-array-like");
t.exports = function(e) {
for (var t = n(this), i = o(t), s = arguments.length, a = r(s > 1 ? arguments[1] : void 0, i), c = s > 2 ? arguments[2] : void 0, l = void 0 === c ? i : r(c, i); l > a; ) t[a++] = e;
return t;
};
}, {
"../internals/length-of-array-like": 115,
"../internals/to-absolute-index": 165,
"../internals/to-object": 171
} ],
24: [ function(e, t) {
"use strict";
var n = e("../internals/array-iteration").forEach, r = e("../internals/array-method-is-strict")("forEach");
t.exports = r ? [].forEach : function(e) {
return n(this, e, arguments.length > 1 ? arguments[1] : void 0);
};
}, {
"../internals/array-iteration": 28,
"../internals/array-method-is-strict": 31
} ],
25: [ function(e, t) {
var n = e("../internals/length-of-array-like");
t.exports = function(e, t) {
for (var r = 0, o = n(t), i = new e(o); o > r; ) i[r] = t[r++];
return i;
};
}, {
"../internals/length-of-array-like": 115
} ],
26: [ function(e, t) {
"use strict";
var n = e("../internals/function-bind-context"), r = e("../internals/function-call"), o = e("../internals/to-object"), i = e("../internals/call-with-safe-iteration-closing"), s = e("../internals/is-array-iterator-method"), a = e("../internals/is-constructor"), c = e("../internals/length-of-array-like"), l = e("../internals/create-property"), u = e("../internals/get-iterator"), p = e("../internals/get-iterator-method"), f = Array;
t.exports = function(e) {
var t = o(e), d = a(this), h = arguments.length, y = h > 1 ? arguments[1] : void 0, v = void 0 !== y;
v && (y = n(y, h > 2 ? arguments[2] : void 0));
var g, _, m, b, w, S, I = p(t), O = 0;
if (!I || this === f && s(I)) {
g = c(t);
_ = d ? new this(g) : f(g);
for (;g > O; O++) {
S = v ? y(t[O], O) : t[O];
l(_, O, S);
}
} else {
w = (b = u(t, I)).next;
_ = d ? new this() : [];
for (;!(m = r(w, b)).done; O++) {
S = v ? i(b, y, [ m.value, O ], !0) : m.value;
l(_, O, S);
}
}
_.length = O;
return _;
};
}, {
"../internals/call-with-safe-iteration-closing": 39,
"../internals/create-property": 49,
"../internals/function-bind-context": 77,
"../internals/function-call": 79,
"../internals/get-iterator": 84,
"../internals/get-iterator-method": 83,
"../internals/is-array-iterator-method": 97,
"../internals/is-constructor": 101,
"../internals/length-of-array-like": 115,
"../internals/to-object": 171
} ],
27: [ function(e, t) {
var n = e("../internals/to-indexed-object"), r = e("../internals/to-absolute-index"), o = e("../internals/length-of-array-like"), i = function(e) {
return function(t, i, s) {
var a, c = n(t), l = o(c), u = r(s, l);
if (e && i != i) {
for (;l > u; ) if ((a = c[u++]) != a) return !0;
} else for (;l > u; u++) if ((e || u in c) && c[u] === i) return e || u || 0;
return !e && -1;
};
};
t.exports = {
includes: i(!0),
indexOf: i(!1)
};
}, {
"../internals/length-of-array-like": 115,
"../internals/to-absolute-index": 165,
"../internals/to-indexed-object": 168
} ],
28: [ function(e, t) {
var n = e("../internals/function-bind-context"), r = e("../internals/function-uncurry-this"), o = e("../internals/indexed-object"), i = e("../internals/to-object"), s = e("../internals/length-of-array-like"), a = e("../internals/array-species-create"), c = r([].push), l = function(e) {
var t = 1 == e, r = 2 == e, l = 3 == e, u = 4 == e, p = 6 == e, f = 7 == e, d = 5 == e || p;
return function(h, y, v, g) {
for (var _, m, b = i(h), w = o(b), S = n(y, v), I = s(w), O = 0, j = g || a, P = t ? j(h, I) : r || f ? j(h, 0) : void 0; I > O; O++) if (d || O in w) {
m = S(_ = w[O], O, b);
if (e) if (t) P[O] = m; else if (m) switch (e) {
case 3:
return !0;

case 5:
return _;

case 6:
return O;

case 2:
c(P, _);
} else switch (e) {
case 4:
return !1;

case 7:
c(P, _);
}
}
return p ? -1 : l || u ? u : P;
};
};
t.exports = {
forEach: l(0),
map: l(1),
filter: l(2),
some: l(3),
every: l(4),
find: l(5),
findIndex: l(6),
filterReject: l(7)
};
}, {
"../internals/array-species-create": 38,
"../internals/function-bind-context": 77,
"../internals/function-uncurry-this": 81,
"../internals/indexed-object": 93,
"../internals/length-of-array-like": 115,
"../internals/to-object": 171
} ],
29: [ function(e, t) {
"use strict";
var n = e("../internals/function-apply"), r = e("../internals/to-indexed-object"), o = e("../internals/to-integer-or-infinity"), i = e("../internals/length-of-array-like"), s = e("../internals/array-method-is-strict"), a = Math.min, c = [].lastIndexOf, l = !!c && 1 / [ 1 ].lastIndexOf(1, -0) < 0, u = s("lastIndexOf"), p = l || !u;
t.exports = p ? function(e) {
if (l) return n(c, this, arguments) || 0;
var t = r(this), s = i(t), u = s - 1;
arguments.length > 1 && (u = a(u, o(arguments[1])));
u < 0 && (u = s + u);
for (;u >= 0; u--) if (u in t && t[u] === e) return u || 0;
return -1;
} : c;
}, {
"../internals/array-method-is-strict": 31,
"../internals/function-apply": 76,
"../internals/length-of-array-like": 115,
"../internals/to-indexed-object": 168,
"../internals/to-integer-or-infinity": 169
} ],
30: [ function(e, t) {
var n = e("../internals/fails"), r = e("../internals/well-known-symbol"), o = e("../internals/engine-v8-version"), i = r("species");
t.exports = function(e) {
return o >= 51 || !n(function() {
var t = [];
(t.constructor = {})[i] = function() {
return {
foo: 1
};
};
return 1 !== t[e](Boolean).foo;
});
};
}, {
"../internals/engine-v8-version": 70,
"../internals/fails": 74,
"../internals/well-known-symbol": 189
} ],
31: [ function(e, t) {
"use strict";
var n = e("../internals/fails");
t.exports = function(e, t) {
var r = [][e];
return !!r && n(function() {
r.call(null, t || function() {
return 1;
}, 1);
});
};
}, {
"../internals/fails": 74
} ],
32: [ function(e, t) {
var n = e("../internals/a-callable"), r = e("../internals/to-object"), o = e("../internals/indexed-object"), i = e("../internals/length-of-array-like"), s = TypeError, a = function(e) {
return function(t, a, c, l) {
n(a);
var u = r(t), p = o(u), f = i(u), d = e ? f - 1 : 0, h = e ? -1 : 1;
if (c < 2) for (;;) {
if (d in p) {
l = p[d];
d += h;
break;
}
d += h;
if (e ? d < 0 : f <= d) throw s("Reduce of empty array with no initial value");
}
for (;e ? d >= 0 : f > d; d += h) d in p && (l = a(l, p[d], d, u));
return l;
};
};
t.exports = {
left: a(!1),
right: a(!0)
};
}, {
"../internals/a-callable": 12,
"../internals/indexed-object": 93,
"../internals/length-of-array-like": 115,
"../internals/to-object": 171
} ],
33: [ function(e, t) {
"use strict";
var n = e("../internals/descriptors"), r = e("../internals/is-array"), o = TypeError, i = Object.getOwnPropertyDescriptor, s = n && !function() {
if (void 0 !== this) return !0;
try {
Object.defineProperty([], "length", {
writable: !1
}).length = 1;
} catch (e) {
return e instanceof TypeError;
}
}();
t.exports = s ? function(e, t) {
if (r(e) && !i(e, "length").writable) throw o("Cannot set read only .length");
return e.length = t;
} : function(e, t) {
return e.length = t;
};
}, {
"../internals/descriptors": 55,
"../internals/is-array": 98
} ],
34: [ function(e, t) {
var n = e("../internals/to-absolute-index"), r = e("../internals/length-of-array-like"), o = e("../internals/create-property"), i = Array, s = Math.max;
t.exports = function(e, t, a) {
for (var c = r(e), l = n(t, c), u = n(void 0 === a ? c : a, c), p = i(s(u - l, 0)), f = 0; l < u; l++, 
f++) o(p, f, e[l]);
p.length = f;
return p;
};
}, {
"../internals/create-property": 49,
"../internals/length-of-array-like": 115,
"../internals/to-absolute-index": 165
} ],
35: [ function(e, t) {
var n = e("../internals/function-uncurry-this");
t.exports = n([].slice);
}, {
"../internals/function-uncurry-this": 81
} ],
36: [ function(e, t) {
var n = e("../internals/array-slice-simple"), r = Math.floor, o = function(e, t) {
var a = e.length, c = r(a / 2);
return a < 8 ? i(e, t) : s(e, o(n(e, 0, c), t), o(n(e, c), t), t);
}, i = function(e, t) {
for (var n, r, o = e.length, i = 1; i < o; ) {
r = i;
n = e[i];
for (;r && t(e[r - 1], n) > 0; ) e[r] = e[--r];
r !== i++ && (e[r] = n);
}
return e;
}, s = function(e, t, n, r) {
for (var o = t.length, i = n.length, s = 0, a = 0; s < o || a < i; ) e[s + a] = s < o && a < i ? r(t[s], n[a]) <= 0 ? t[s++] : n[a++] : s < o ? t[s++] : n[a++];
return e;
};
t.exports = o;
}, {
"../internals/array-slice-simple": 34
} ],
37: [ function(e, t) {
var n = e("../internals/is-array"), r = e("../internals/is-constructor"), o = e("../internals/is-object"), i = e("../internals/well-known-symbol")("species"), s = Array;
t.exports = function(e) {
var t;
if (n(e)) {
t = e.constructor;
r(t) && (t === s || n(t.prototype)) ? t = void 0 : o(t) && null === (t = t[i]) && (t = void 0);
}
return void 0 === t ? s : t;
};
}, {
"../internals/is-array": 98,
"../internals/is-constructor": 101,
"../internals/is-object": 105,
"../internals/well-known-symbol": 189
} ],
38: [ function(e, t) {
var n = e("../internals/array-species-constructor");
t.exports = function(e, t) {
return new (n(e))(0 === t ? 0 : t);
};
}, {
"../internals/array-species-constructor": 37
} ],
39: [ function(e, t) {
var n = e("../internals/an-object"), r = e("../internals/iterator-close");
t.exports = function(e, t, o, i) {
try {
return i ? t(n(o)[0], o[1]) : t(o);
} catch (t) {
r(e, "throw", t);
}
};
}, {
"../internals/an-object": 18,
"../internals/iterator-close": 110
} ],
40: [ function(e, t) {
var n = e("../internals/well-known-symbol")("iterator"), r = !1;
try {
var o = 0, i = {
next: function() {
return {
done: !!o++
};
},
return: function() {
r = !0;
}
};
i[n] = function() {
return this;
};
Array.from(i, function() {
throw 2;
});
} catch (e) {}
t.exports = function(e, t) {
if (!t && !r) return !1;
var o = !1;
try {
var i = {};
i[n] = function() {
return {
next: function() {
return {
done: o = !0
};
}
};
};
e(i);
} catch (e) {}
return o;
};
}, {
"../internals/well-known-symbol": 189
} ],
41: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = n({}.toString), o = n("".slice);
t.exports = function(e) {
return o(r(e), 8, -1);
};
}, {
"../internals/function-uncurry-this": 81
} ],
42: [ function(e, t) {
var n = e("../internals/to-string-tag-support"), r = e("../internals/is-callable"), o = e("../internals/classof-raw"), i = e("../internals/well-known-symbol")("toStringTag"), s = Object, a = "Arguments" == o(function() {
return arguments;
}()), c = function(e, t) {
try {
return e[t];
} catch (e) {}
};
t.exports = n ? o : function(e) {
var t, n, l;
return void 0 === e ? "Undefined" : null === e ? "Null" : "string" == typeof (n = c(t = s(e), i)) ? n : a ? o(t) : "Object" == (l = o(t)) && r(t.callee) ? "Arguments" : l;
};
}, {
"../internals/classof-raw": 41,
"../internals/is-callable": 100,
"../internals/to-string-tag-support": 176,
"../internals/well-known-symbol": 189
} ],
43: [ function(e, t) {
var n = e("../internals/has-own-property"), r = e("../internals/own-keys"), o = e("../internals/object-get-own-property-descriptor"), i = e("../internals/object-define-property");
t.exports = function(e, t, s) {
for (var a = r(t), c = i.f, l = o.f, u = 0; u < a.length; u++) {
var p = a[u];
n(e, p) || s && n(s, p) || c(e, p, l(t, p));
}
};
}, {
"../internals/has-own-property": 87,
"../internals/object-define-property": 125,
"../internals/object-get-own-property-descriptor": 126,
"../internals/own-keys": 138
} ],
44: [ function(e, t) {
var n = e("../internals/well-known-symbol")("match");
t.exports = function(e) {
var t = /./;
try {
"/./"[e](t);
} catch (r) {
try {
t[n] = !1;
return "/./"[e](t);
} catch (e) {}
}
return !1;
};
}, {
"../internals/well-known-symbol": 189
} ],
45: [ function(e, t) {
var n = e("../internals/fails");
t.exports = !n(function() {
function e() {}
e.prototype.constructor = null;
return Object.getPrototypeOf(new e()) !== e.prototype;
});
}, {
"../internals/fails": 74
} ],
46: [ function(e, t) {
t.exports = function(e, t) {
return {
value: e,
done: t
};
};
}, {} ],
47: [ function(e, t) {
var n = e("../internals/descriptors"), r = e("../internals/object-define-property"), o = e("../internals/create-property-descriptor");
t.exports = n ? function(e, t, n) {
return r.f(e, t, o(1, n));
} : function(e, t, n) {
e[t] = n;
return e;
};
}, {
"../internals/create-property-descriptor": 48,
"../internals/descriptors": 55,
"../internals/object-define-property": 125
} ],
48: [ function(e, t) {
t.exports = function(e, t) {
return {
enumerable: !(1 & e),
configurable: !(2 & e),
writable: !(4 & e),
value: t
};
};
}, {} ],
49: [ function(e, t) {
"use strict";
var n = e("../internals/to-property-key"), r = e("../internals/object-define-property"), o = e("../internals/create-property-descriptor");
t.exports = function(e, t, i) {
var s = n(t);
s in e ? r.f(e, s, o(0, i)) : e[s] = i;
};
}, {
"../internals/create-property-descriptor": 48,
"../internals/object-define-property": 125,
"../internals/to-property-key": 175
} ],
50: [ function(e, t) {
var n = e("../internals/make-built-in"), r = e("../internals/object-define-property");
t.exports = function(e, t, o) {
o.get && n(o.get, t, {
getter: !0
});
o.set && n(o.set, t, {
setter: !0
});
return r.f(e, t, o);
};
}, {
"../internals/make-built-in": 116,
"../internals/object-define-property": 125
} ],
51: [ function(e, t) {
var n = e("../internals/is-callable"), r = e("../internals/object-define-property"), o = e("../internals/make-built-in"), i = e("../internals/define-global-property");
t.exports = function(e, t, s, a) {
a || (a = {});
var c = a.enumerable, l = void 0 !== a.name ? a.name : t;
n(s) && o(s, l, a);
if (a.global) c ? e[t] = s : i(t, s); else {
try {
a.unsafe ? e[t] && (c = !0) : delete e[t];
} catch (e) {}
c ? e[t] = s : r.f(e, t, {
value: s,
enumerable: !1,
configurable: !a.nonConfigurable,
writable: !a.nonWritable
});
}
return e;
};
}, {
"../internals/define-global-property": 53,
"../internals/is-callable": 100,
"../internals/make-built-in": 116,
"../internals/object-define-property": 125
} ],
52: [ function(e, t) {
var n = e("../internals/define-built-in");
t.exports = function(e, t, r) {
for (var o in t) n(e, o, t[o], r);
return e;
};
}, {
"../internals/define-built-in": 51
} ],
53: [ function(e, t) {
var n = e("../internals/global"), r = Object.defineProperty;
t.exports = function(e, t) {
try {
r(n, e, {
value: t,
configurable: !0,
writable: !0
});
} catch (r) {
n[e] = t;
}
return t;
};
}, {
"../internals/global": 86
} ],
54: [ function(e, t) {
"use strict";
var n = e("../internals/try-to-string"), r = TypeError;
t.exports = function(e, t) {
if (!delete e[t]) throw r("Cannot delete property " + n(t) + " of " + n(e));
};
}, {
"../internals/try-to-string": 178
} ],
55: [ function(e, t) {
var n = e("../internals/fails");
t.exports = !n(function() {
return 7 != Object.defineProperty({}, 1, {
get: function() {
return 7;
}
})[1];
});
}, {
"../internals/fails": 74
} ],
56: [ function(e, t) {
var n = "object" == typeof document && document.all, r = "undefined" == typeof n && void 0 !== n;
t.exports = {
all: n,
IS_HTMLDDA: r
};
}, {} ],
57: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/is-object"), o = n.document, i = r(o) && r(o.createElement);
t.exports = function(e) {
return i ? o.createElement(e) : {};
};
}, {
"../internals/global": 86,
"../internals/is-object": 105
} ],
58: [ function(e, t) {
var n = TypeError;
t.exports = function(e) {
if (e > 9007199254740991) throw n("Maximum allowed index exceeded");
return e;
};
}, {} ],
59: [ function(e, t) {
t.exports = {
CSSRuleList: 0,
CSSStyleDeclaration: 0,
CSSValueList: 0,
ClientRectList: 0,
DOMRectList: 0,
DOMStringList: 0,
DOMTokenList: 1,
DataTransferItemList: 0,
FileList: 0,
HTMLAllCollection: 0,
HTMLCollection: 0,
HTMLFormElement: 0,
HTMLSelectElement: 0,
MediaList: 0,
MimeTypeArray: 0,
NamedNodeMap: 0,
NodeList: 1,
PaintRequestList: 0,
Plugin: 0,
PluginArray: 0,
SVGLengthList: 0,
SVGNumberList: 0,
SVGPathSegList: 0,
SVGPointList: 0,
SVGStringList: 0,
SVGTransformList: 0,
SourceBufferList: 0,
StyleSheetList: 0,
TextTrackCueList: 0,
TextTrackList: 0,
TouchList: 0
};
}, {} ],
60: [ function(e, t) {
var n = e("../internals/document-create-element")("span").classList, r = n && n.constructor && n.constructor.prototype;
t.exports = r === Object.prototype ? void 0 : r;
}, {
"../internals/document-create-element": 57
} ],
61: [ function(e, t) {
var n = e("../internals/engine-user-agent").match(/firefox\/(\d+)/i);
t.exports = !!n && +n[1];
}, {
"../internals/engine-user-agent": 69
} ],
62: [ function(e, t) {
var n = e("../internals/engine-is-deno"), r = e("../internals/engine-is-node");
t.exports = !n && !r && "object" == typeof window && "object" == typeof document;
}, {
"../internals/engine-is-deno": 63,
"../internals/engine-is-node": 67
} ],
63: [ function(e, t) {
t.exports = "object" == typeof Deno && Deno && "object" == typeof Deno.version;
}, {} ],
64: [ function(e, t) {
var n = e("../internals/engine-user-agent");
t.exports = /MSIE|Trident/.test(n);
}, {
"../internals/engine-user-agent": 69
} ],
65: [ function(e, t) {
var n = e("../internals/engine-user-agent"), r = e("../internals/global");
t.exports = /ipad|iphone|ipod/i.test(n) && void 0 !== r.Pebble;
}, {
"../internals/engine-user-agent": 69,
"../internals/global": 86
} ],
66: [ function(e, t) {
var n = e("../internals/engine-user-agent");
t.exports = /(?:ipad|iphone|ipod).*applewebkit/i.test(n);
}, {
"../internals/engine-user-agent": 69
} ],
67: [ function(e, t) {
var n = e("../internals/classof-raw"), r = e("../internals/global");
t.exports = "process" == n(r.process);
}, {
"../internals/classof-raw": 41,
"../internals/global": 86
} ],
68: [ function(e, t) {
var n = e("../internals/engine-user-agent");
t.exports = /web0s(?!.*chrome)/i.test(n);
}, {
"../internals/engine-user-agent": 69
} ],
69: [ function(e, t) {
var n = e("../internals/get-built-in");
t.exports = n("navigator", "userAgent") || "";
}, {
"../internals/get-built-in": 82
} ],
70: [ function(e, t) {
var n, r, o = e("../internals/global"), i = e("../internals/engine-user-agent"), s = o.process, a = o.Deno, c = s && s.versions || a && a.version, l = c && c.v8;
l && (r = (n = l.split("."))[0] > 0 && n[0] < 4 ? 1 : +(n[0] + n[1]));
!r && i && (!(n = i.match(/Edge\/(\d+)/)) || n[1] >= 74) && (n = i.match(/Chrome\/(\d+)/)) && (r = +n[1]);
t.exports = r;
}, {
"../internals/engine-user-agent": 69,
"../internals/global": 86
} ],
71: [ function(e, t) {
var n = e("../internals/engine-user-agent").match(/AppleWebKit\/(\d+)\./);
t.exports = !!n && +n[1];
}, {
"../internals/engine-user-agent": 69
} ],
72: [ function(e, t) {
t.exports = [ "constructor", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "toLocaleString", "toString", "valueOf" ];
}, {} ],
73: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/object-get-own-property-descriptor").f, o = e("../internals/create-non-enumerable-property"), i = e("../internals/define-built-in"), s = e("../internals/define-global-property"), a = e("../internals/copy-constructor-properties"), c = e("../internals/is-forced");
t.exports = function(e, t) {
var l, u, p, f, d, h = e.target, y = e.global, v = e.stat;
if (l = y ? n : v ? n[h] || s(h, {}) : (n[h] || {}).prototype) for (u in t) {
f = t[u];
p = e.dontCallGetSet ? (d = r(l, u)) && d.value : l[u];
if (!c(y ? u : h + (v ? "." : "#") + u, e.forced) && void 0 !== p) {
if (typeof f == typeof p) continue;
a(f, p);
}
(e.sham || p && p.sham) && o(f, "sham", !0);
i(l, u, f, e);
}
};
}, {
"../internals/copy-constructor-properties": 43,
"../internals/create-non-enumerable-property": 47,
"../internals/define-built-in": 51,
"../internals/define-global-property": 53,
"../internals/global": 86,
"../internals/is-forced": 102,
"../internals/object-get-own-property-descriptor": 126
} ],
74: [ function(e, t) {
t.exports = function(e) {
try {
return !!e();
} catch (e) {
return !0;
}
};
}, {} ],
75: [ function(e, t) {
"use strict";
e("../modules/es.regexp.exec");
var n = e("../internals/function-uncurry-this"), r = e("../internals/define-built-in"), o = e("../internals/regexp-exec"), i = e("../internals/fails"), s = e("../internals/well-known-symbol"), a = e("../internals/create-non-enumerable-property"), c = s("species"), l = RegExp.prototype;
t.exports = function(e, t, u, p) {
var f = s(e), d = !i(function() {
var t = {};
t[f] = function() {
return 7;
};
return 7 != ""[e](t);
}), h = d && !i(function() {
var t = !1, n = /a/;
if ("split" === e) {
(n = {}).constructor = {};
n.constructor[c] = function() {
return n;
};
n.flags = "";
n[f] = /./[f];
}
n.exec = function() {
t = !0;
return null;
};
n[f]("");
return !t;
});
if (!d || !h || u) {
var y = n(/./[f]), v = t(f, ""[e], function(e, t, r, i, s) {
var a = n(e), c = t.exec;
return c === o || c === l.exec ? d && !s ? {
done: !0,
value: y(t, r, i)
} : {
done: !0,
value: a(r, t, i)
} : {
done: !1
};
});
r(String.prototype, e, v[0]);
r(l, f, v[1]);
}
p && a(l[f], "sham", !0);
};
}, {
"../internals/create-non-enumerable-property": 47,
"../internals/define-built-in": 51,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/regexp-exec": 147,
"../internals/well-known-symbol": 189,
"../modules/es.regexp.exec": 225
} ],
76: [ function(e, t) {
var n = e("../internals/function-bind-native"), r = Function.prototype, o = r.apply, i = r.call;
t.exports = "object" == typeof Reflect && Reflect.apply || (n ? i.bind(o) : function() {
return i.apply(o, arguments);
});
}, {
"../internals/function-bind-native": 78
} ],
77: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/a-callable"), o = e("../internals/function-bind-native"), i = n(n.bind);
t.exports = function(e, t) {
r(e);
return void 0 === t ? e : o ? i(e, t) : function() {
return e.apply(t, arguments);
};
};
}, {
"../internals/a-callable": 12,
"../internals/function-bind-native": 78,
"../internals/function-uncurry-this": 81
} ],
78: [ function(e, t) {
var n = e("../internals/fails");
t.exports = !n(function() {
var e = function() {}.bind();
return "function" != typeof e || e.hasOwnProperty("prototype");
});
}, {
"../internals/fails": 74
} ],
79: [ function(e, t) {
var n = e("../internals/function-bind-native"), r = Function.prototype.call;
t.exports = n ? r.bind(r) : function() {
return r.apply(r, arguments);
};
}, {
"../internals/function-bind-native": 78
} ],
80: [ function(e, t) {
var n = e("../internals/descriptors"), r = e("../internals/has-own-property"), o = Function.prototype, i = n && Object.getOwnPropertyDescriptor, s = r(o, "name"), a = s && "something" === function() {}.name, c = s && (!n || n && i(o, "name").configurable);
t.exports = {
EXISTS: s,
PROPER: a,
CONFIGURABLE: c
};
}, {
"../internals/descriptors": 55,
"../internals/has-own-property": 87
} ],
81: [ function(e, t) {
var n = e("../internals/function-bind-native"), r = Function.prototype, o = r.bind, i = r.call, s = n && o.bind(i, i);
t.exports = n ? function(e) {
return e && s(e);
} : function(e) {
return e && function() {
return i.apply(e, arguments);
};
};
}, {
"../internals/function-bind-native": 78
} ],
82: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/is-callable"), o = function(e) {
return r(e) ? e : void 0;
};
t.exports = function(e, t) {
return arguments.length < 2 ? o(n[e]) : n[e] && n[e][t];
};
}, {
"../internals/global": 86,
"../internals/is-callable": 100
} ],
83: [ function(e, t) {
var n = e("../internals/classof"), r = e("../internals/get-method"), o = e("../internals/is-null-or-undefined"), i = e("../internals/iterators"), s = e("../internals/well-known-symbol")("iterator");
t.exports = function(e) {
if (!o(e)) return r(e, s) || r(e, "@@iterator") || i[n(e)];
};
}, {
"../internals/classof": 42,
"../internals/get-method": 85,
"../internals/is-null-or-undefined": 104,
"../internals/iterators": 114,
"../internals/well-known-symbol": 189
} ],
84: [ function(e, t) {
var n = e("../internals/function-call"), r = e("../internals/a-callable"), o = e("../internals/an-object"), i = e("../internals/try-to-string"), s = e("../internals/get-iterator-method"), a = TypeError;
t.exports = function(e, t) {
var c = arguments.length < 2 ? s(e) : t;
if (r(c)) return o(n(c, e));
throw a(i(e) + " is not iterable");
};
}, {
"../internals/a-callable": 12,
"../internals/an-object": 18,
"../internals/function-call": 79,
"../internals/get-iterator-method": 83,
"../internals/try-to-string": 178
} ],
85: [ function(e, t) {
var n = e("../internals/a-callable"), r = e("../internals/is-null-or-undefined");
t.exports = function(e, t) {
var o = e[t];
return r(o) ? void 0 : n(o);
};
}, {
"../internals/a-callable": 12,
"../internals/is-null-or-undefined": 104
} ],
86: [ function(e, t) {
(function(e) {
var n = function(e) {
return e && e.Math == Math && e;
};
t.exports = n("object" == typeof globalThis && globalThis) || n("object" == typeof window && window) || n("object" == typeof self && self) || n("object" == typeof e && e) || function() {
return this;
}() || Function("", "return this")();
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {} ],
87: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/to-object"), o = n({}.hasOwnProperty);
t.exports = Object.hasOwn || function(e, t) {
return o(r(e), t);
};
}, {
"../internals/function-uncurry-this": 81,
"../internals/to-object": 171
} ],
88: [ function(e, t) {
t.exports = {};
}, {} ],
89: [ function(e, t) {
var n = e("../internals/global");
t.exports = function(e, t) {
var r = n.console;
r && r.error && (1 == arguments.length ? r.error(e) : r.error(e, t));
};
}, {
"../internals/global": 86
} ],
90: [ function(e, t) {
var n = e("../internals/get-built-in");
t.exports = n("document", "documentElement");
}, {
"../internals/get-built-in": 82
} ],
91: [ function(e, t) {
var n = e("../internals/descriptors"), r = e("../internals/fails"), o = e("../internals/document-create-element");
t.exports = !n && !r(function() {
return 7 != Object.defineProperty(o("div"), "a", {
get: function() {
return 7;
}
}).a;
});
}, {
"../internals/descriptors": 55,
"../internals/document-create-element": 57,
"../internals/fails": 74
} ],
92: [ function(e, t) {
var n = Array, r = Math.abs, o = Math.pow, i = Math.floor, s = Math.log, a = Math.LN2;
t.exports = {
pack: function(e, t, c) {
var l, u, p, f = n(c), d = 8 * c - t - 1, h = (1 << d) - 1, y = h >> 1, v = 23 === t ? o(2, -24) - o(2, -77) : 0, g = e < 0 || 0 === e && 1 / e < 0 ? 1 : 0, _ = 0;
if ((e = r(e)) != e || Infinity === e) {
u = e != e ? 1 : 0;
l = h;
} else {
l = i(s(e) / a);
if (e * (p = o(2, -l)) < 1) {
l--;
p *= 2;
}
if ((e += l + y >= 1 ? v / p : v * o(2, 1 - y)) * p >= 2) {
l++;
p /= 2;
}
if (l + y >= h) {
u = 0;
l = h;
} else if (l + y >= 1) {
u = (e * p - 1) * o(2, t);
l += y;
} else {
u = e * o(2, y - 1) * o(2, t);
l = 0;
}
}
for (;t >= 8; ) {
f[_++] = 255 & u;
u /= 256;
t -= 8;
}
l = l << t | u;
d += t;
for (;d > 0; ) {
f[_++] = 255 & l;
l /= 256;
d -= 8;
}
f[--_] |= 128 * g;
return f;
},
unpack: function(e, t) {
var n, r = e.length, i = 8 * r - t - 1, s = (1 << i) - 1, a = s >> 1, c = i - 7, l = r - 1, u = e[l--], p = 127 & u;
u >>= 7;
for (;c > 0; ) {
p = 256 * p + e[l--];
c -= 8;
}
n = p & (1 << -c) - 1;
p >>= -c;
c += t;
for (;c > 0; ) {
n = 256 * n + e[l--];
c -= 8;
}
if (0 === p) p = 1 - a; else {
if (p === s) return n ? NaN : u ? -Infinity : Infinity;
n += o(2, t);
p -= a;
}
return (u ? -1 : 1) * n * o(2, p - t);
}
};
}, {} ],
93: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/fails"), o = e("../internals/classof-raw"), i = Object, s = n("".split);
t.exports = r(function() {
return !i("z").propertyIsEnumerable(0);
}) ? function(e) {
return "String" == o(e) ? s(e, "") : i(e);
} : i;
}, {
"../internals/classof-raw": 41,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81
} ],
94: [ function(e, t) {
var n = e("../internals/is-callable"), r = e("../internals/is-object"), o = e("../internals/object-set-prototype-of");
t.exports = function(e, t, i) {
var s, a;
o && n(s = t.constructor) && s !== i && r(a = s.prototype) && a !== i.prototype && o(e, a);
return e;
};
}, {
"../internals/is-callable": 100,
"../internals/is-object": 105,
"../internals/object-set-prototype-of": 134
} ],
95: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/is-callable"), o = e("../internals/shared-store"), i = n(Function.toString);
r(o.inspectSource) || (o.inspectSource = function(e) {
return i(e);
});
t.exports = o.inspectSource;
}, {
"../internals/function-uncurry-this": 81,
"../internals/is-callable": 100,
"../internals/shared-store": 157
} ],
96: [ function(e, t) {
var n, r, o, i = e("../internals/weak-map-basic-detection"), s = e("../internals/global"), a = e("../internals/function-uncurry-this"), c = e("../internals/is-object"), l = e("../internals/create-non-enumerable-property"), u = e("../internals/has-own-property"), p = e("../internals/shared-store"), f = e("../internals/shared-key"), d = e("../internals/hidden-keys"), h = s.TypeError, y = s.WeakMap;
if (i || p.state) {
var v = p.state || (p.state = new y()), g = a(v.get), _ = a(v.has), m = a(v.set);
n = function(e, t) {
if (_(v, e)) throw h("Object already initialized");
t.facade = e;
m(v, e, t);
return t;
};
r = function(e) {
return g(v, e) || {};
};
o = function(e) {
return _(v, e);
};
} else {
var b = f("state");
d[b] = !0;
n = function(e, t) {
if (u(e, b)) throw h("Object already initialized");
t.facade = e;
l(e, b, t);
return t;
};
r = function(e) {
return u(e, b) ? e[b] : {};
};
o = function(e) {
return u(e, b);
};
}
t.exports = {
set: n,
get: r,
has: o,
enforce: function(e) {
return o(e) ? r(e) : n(e, {});
},
getterFor: function(e) {
return function(t) {
var n;
if (!c(t) || (n = r(t)).type !== e) throw h("Incompatible receiver, " + e + " required");
return n;
};
}
};
}, {
"../internals/create-non-enumerable-property": 47,
"../internals/function-uncurry-this": 81,
"../internals/global": 86,
"../internals/has-own-property": 87,
"../internals/hidden-keys": 88,
"../internals/is-object": 105,
"../internals/shared-key": 156,
"../internals/shared-store": 157,
"../internals/weak-map-basic-detection": 188
} ],
97: [ function(e, t) {
var n = e("../internals/well-known-symbol"), r = e("../internals/iterators"), o = n("iterator"), i = Array.prototype;
t.exports = function(e) {
return void 0 !== e && (r.Array === e || i[o] === e);
};
}, {
"../internals/iterators": 114,
"../internals/well-known-symbol": 189
} ],
98: [ function(e, t) {
var n = e("../internals/classof-raw");
t.exports = Array.isArray || function(e) {
return "Array" == n(e);
};
}, {
"../internals/classof-raw": 41
} ],
99: [ function(e, t) {
var n = e("../internals/classof"), r = e("../internals/function-uncurry-this")("".slice);
t.exports = function(e) {
return "Big" === r(n(e), 0, 3);
};
}, {
"../internals/classof": 42,
"../internals/function-uncurry-this": 81
} ],
100: [ function(e, t) {
var n = e("../internals/document-all"), r = n.all;
t.exports = n.IS_HTMLDDA ? function(e) {
return "function" == typeof e || e === r;
} : function(e) {
return "function" == typeof e;
};
}, {
"../internals/document-all": 56
} ],
101: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/fails"), o = e("../internals/is-callable"), i = e("../internals/classof"), s = e("../internals/get-built-in"), a = e("../internals/inspect-source"), c = function() {}, l = [], u = s("Reflect", "construct"), p = /^\s*(?:class|function)\b/, f = n(p.exec), d = !p.exec(c), h = function(e) {
if (!o(e)) return !1;
try {
u(c, l, e);
return !0;
} catch (e) {
return !1;
}
}, y = function(e) {
if (!o(e)) return !1;
switch (i(e)) {
case "AsyncFunction":
case "GeneratorFunction":
case "AsyncGeneratorFunction":
return !1;
}
try {
return d || !!f(p, a(e));
} catch (e) {
return !0;
}
};
y.sham = !0;
t.exports = !u || r(function() {
var e;
return h(h.call) || !h(Object) || !h(function() {
e = !0;
}) || e;
}) ? y : h;
}, {
"../internals/classof": 42,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/get-built-in": 82,
"../internals/inspect-source": 95,
"../internals/is-callable": 100
} ],
102: [ function(e, t) {
var n = e("../internals/fails"), r = e("../internals/is-callable"), o = /#|\.prototype\./, i = function(e, t) {
var o = a[s(e)];
return o == l || o != c && (r(t) ? n(t) : !!t);
}, s = i.normalize = function(e) {
return String(e).replace(o, ".").toLowerCase();
}, a = i.data = {}, c = i.NATIVE = "N", l = i.POLYFILL = "P";
t.exports = i;
}, {
"../internals/fails": 74,
"../internals/is-callable": 100
} ],
103: [ function(e, t) {
var n = e("../internals/is-object"), r = Math.floor;
t.exports = Number.isInteger || function(e) {
return !n(e) && isFinite(e) && r(e) === e;
};
}, {
"../internals/is-object": 105
} ],
104: [ function(e, t) {
t.exports = function(e) {
return null == e;
};
}, {} ],
105: [ function(e, t) {
var n = e("../internals/is-callable"), r = e("../internals/document-all"), o = r.all;
t.exports = r.IS_HTMLDDA ? function(e) {
return "object" == typeof e ? null !== e : n(e) || e === o;
} : function(e) {
return "object" == typeof e ? null !== e : n(e);
};
}, {
"../internals/document-all": 56,
"../internals/is-callable": 100
} ],
106: [ function(e, t) {
t.exports = !1;
}, {} ],
107: [ function(e, t) {
var n = e("../internals/is-object"), r = e("../internals/classof-raw"), o = e("../internals/well-known-symbol")("match");
t.exports = function(e) {
var t;
return n(e) && (void 0 !== (t = e[o]) ? !!t : "RegExp" == r(e));
};
}, {
"../internals/classof-raw": 41,
"../internals/is-object": 105,
"../internals/well-known-symbol": 189
} ],
108: [ function(e, t) {
var n = e("../internals/get-built-in"), r = e("../internals/is-callable"), o = e("../internals/object-is-prototype-of"), i = e("../internals/use-symbol-as-uid"), s = Object;
t.exports = i ? function(e) {
return "symbol" == typeof e;
} : function(e) {
var t = n("Symbol");
return r(t) && o(t.prototype, s(e));
};
}, {
"../internals/get-built-in": 82,
"../internals/is-callable": 100,
"../internals/object-is-prototype-of": 130,
"../internals/use-symbol-as-uid": 185
} ],
109: [ function(e, t) {
var n = e("../internals/function-bind-context"), r = e("../internals/function-call"), o = e("../internals/an-object"), i = e("../internals/try-to-string"), s = e("../internals/is-array-iterator-method"), a = e("../internals/length-of-array-like"), c = e("../internals/object-is-prototype-of"), l = e("../internals/get-iterator"), u = e("../internals/get-iterator-method"), p = e("../internals/iterator-close"), f = TypeError, d = function(e, t) {
this.stopped = e;
this.result = t;
}, h = d.prototype;
t.exports = function(e, t, y) {
var v, g, _, m, b, w, S, I = y && y.that, O = !(!y || !y.AS_ENTRIES), j = !(!y || !y.IS_RECORD), P = !(!y || !y.IS_ITERATOR), C = !(!y || !y.INTERRUPTED), T = n(t, I), M = function(e) {
v && p(v, "normal", e);
return new d(!0, e);
}, x = function(e) {
if (O) {
o(e);
return C ? T(e[0], e[1], M) : T(e[0], e[1]);
}
return C ? T(e, M) : T(e);
};
if (j) v = e.iterator; else if (P) v = e; else {
if (!(g = u(e))) throw f(i(e) + " is not iterable");
if (s(g)) {
for (_ = 0, m = a(e); m > _; _++) if ((b = x(e[_])) && c(h, b)) return b;
return new d(!1);
}
v = l(e, g);
}
w = j ? e.next : v.next;
for (;!(S = r(w, v)).done; ) {
try {
b = x(S.value);
} catch (e) {
p(v, "throw", e);
}
if ("object" == typeof b && b && c(h, b)) return b;
}
return new d(!1);
};
}, {
"../internals/an-object": 18,
"../internals/function-bind-context": 77,
"../internals/function-call": 79,
"../internals/get-iterator": 84,
"../internals/get-iterator-method": 83,
"../internals/is-array-iterator-method": 97,
"../internals/iterator-close": 110,
"../internals/length-of-array-like": 115,
"../internals/object-is-prototype-of": 130,
"../internals/try-to-string": 178
} ],
110: [ function(e, t) {
var n = e("../internals/function-call"), r = e("../internals/an-object"), o = e("../internals/get-method");
t.exports = function(e, t, i) {
var s, a;
r(e);
try {
if (!(s = o(e, "return"))) {
if ("throw" === t) throw i;
return i;
}
s = n(s, e);
} catch (e) {
a = !0;
s = e;
}
if ("throw" === t) throw i;
if (a) throw s;
r(s);
return i;
};
}, {
"../internals/an-object": 18,
"../internals/function-call": 79,
"../internals/get-method": 85
} ],
111: [ function(e, t) {
"use strict";
var n = e("../internals/iterators-core").IteratorPrototype, r = e("../internals/object-create"), o = e("../internals/create-property-descriptor"), i = e("../internals/set-to-string-tag"), s = e("../internals/iterators"), a = function() {
return this;
};
t.exports = function(e, t, c, l) {
var u = t + " Iterator";
e.prototype = r(n, {
next: o(+!l, c)
});
i(e, u, !1, !0);
s[u] = a;
return e;
};
}, {
"../internals/create-property-descriptor": 48,
"../internals/iterators": 114,
"../internals/iterators-core": 113,
"../internals/object-create": 123,
"../internals/set-to-string-tag": 155
} ],
112: [ function(e, t) {
"use strict";
var n = e("../internals/export"), r = e("../internals/function-call"), o = e("../internals/is-pure"), i = e("../internals/function-name"), s = e("../internals/is-callable"), a = e("../internals/iterator-create-constructor"), c = e("../internals/object-get-prototype-of"), l = e("../internals/object-set-prototype-of"), u = e("../internals/set-to-string-tag"), p = e("../internals/create-non-enumerable-property"), f = e("../internals/define-built-in"), d = e("../internals/well-known-symbol"), h = e("../internals/iterators"), y = e("../internals/iterators-core"), v = i.PROPER, g = i.CONFIGURABLE, _ = y.IteratorPrototype, m = y.BUGGY_SAFARI_ITERATORS, b = d("iterator"), w = function() {
return this;
};
t.exports = function(e, t, i, d, y, S, I) {
a(i, t, d);
var O, j, P, C = function(e) {
if (e === y && R) return R;
if (!m && e in x) return x[e];
switch (e) {
case "keys":
case "values":
case "entries":
return function() {
return new i(this, e);
};
}
return function() {
return new i(this);
};
}, T = t + " Iterator", M = !1, x = e.prototype, k = x[b] || x["@@iterator"] || y && x[y], R = !m && k || C(y), U = "Array" == t && x.entries || k;
if (U && (O = c(U.call(new e()))) !== Object.prototype && O.next) {
o || c(O) === _ || (l ? l(O, _) : s(O[b]) || f(O, b, w));
u(O, T, !0, !0);
o && (h[T] = w);
}
if (v && "values" == y && k && "values" !== k.name) if (!o && g) p(x, "name", "values"); else {
M = !0;
R = function() {
return r(k, this);
};
}
if (y) {
j = {
values: C("values"),
keys: S ? R : C("keys"),
entries: C("entries")
};
if (I) for (P in j) !m && !M && P in x || f(x, P, j[P]); else n({
target: t,
proto: !0,
forced: m || M
}, j);
}
o && !I || x[b] === R || f(x, b, R, {
name: y
});
h[t] = R;
return j;
};
}, {
"../internals/create-non-enumerable-property": 47,
"../internals/define-built-in": 51,
"../internals/export": 73,
"../internals/function-call": 79,
"../internals/function-name": 80,
"../internals/is-callable": 100,
"../internals/is-pure": 106,
"../internals/iterator-create-constructor": 111,
"../internals/iterators": 114,
"../internals/iterators-core": 113,
"../internals/object-get-prototype-of": 129,
"../internals/object-set-prototype-of": 134,
"../internals/set-to-string-tag": 155,
"../internals/well-known-symbol": 189
} ],
113: [ function(e, t) {
"use strict";
var n, r, o, i = e("../internals/fails"), s = e("../internals/is-callable"), a = e("../internals/is-object"), c = e("../internals/object-create"), l = e("../internals/object-get-prototype-of"), u = e("../internals/define-built-in"), p = e("../internals/well-known-symbol"), f = e("../internals/is-pure"), d = p("iterator"), h = !1;
[].keys && ("next" in (o = [].keys()) ? (r = l(l(o))) !== Object.prototype && (n = r) : h = !0);
!a(n) || i(function() {
var e = {};
return n[d].call(e) !== e;
}) ? n = {} : f && (n = c(n));
s(n[d]) || u(n, d, function() {
return this;
});
t.exports = {
IteratorPrototype: n,
BUGGY_SAFARI_ITERATORS: h
};
}, {
"../internals/define-built-in": 51,
"../internals/fails": 74,
"../internals/is-callable": 100,
"../internals/is-object": 105,
"../internals/is-pure": 106,
"../internals/object-create": 123,
"../internals/object-get-prototype-of": 129,
"../internals/well-known-symbol": 189
} ],
114: [ function(e, t, n) {
arguments[4][88][0].apply(n, arguments);
}, {
dup: 88
} ],
115: [ function(e, t) {
var n = e("../internals/to-length");
t.exports = function(e) {
return n(e.length);
};
}, {
"../internals/to-length": 170
} ],
116: [ function(e, t) {
var n = e("../internals/fails"), r = e("../internals/is-callable"), o = e("../internals/has-own-property"), i = e("../internals/descriptors"), s = e("../internals/function-name").CONFIGURABLE, a = e("../internals/inspect-source"), c = e("../internals/internal-state"), l = c.enforce, u = c.get, p = Object.defineProperty, f = i && !n(function() {
return 8 !== p(function() {}, "length", {
value: 8
}).length;
}), d = String(String).split("String"), h = t.exports = function(e, t, n) {
"Symbol(" === String(t).slice(0, 7) && (t = "[" + String(t).replace(/^Symbol\(([^)]*)\)/, "$1") + "]");
n && n.getter && (t = "get " + t);
n && n.setter && (t = "set " + t);
(!o(e, "name") || s && e.name !== t) && (i ? p(e, "name", {
value: t,
configurable: !0
}) : e.name = t);
f && n && o(n, "arity") && e.length !== n.arity && p(e, "length", {
value: n.arity
});
try {
n && o(n, "constructor") && n.constructor ? i && p(e, "prototype", {
writable: !1
}) : e.prototype && (e.prototype = void 0);
} catch (e) {}
var r = l(e);
o(r, "source") || (r.source = d.join("string" == typeof t ? t : ""));
return e;
};
Function.prototype.toString = h(function() {
return r(this) && u(this).source || a(this);
}, "toString");
}, {
"../internals/descriptors": 55,
"../internals/fails": 74,
"../internals/function-name": 80,
"../internals/has-own-property": 87,
"../internals/inspect-source": 95,
"../internals/internal-state": 96,
"../internals/is-callable": 100
} ],
117: [ function(e, t) {
var n = Math.ceil, r = Math.floor;
t.exports = Math.trunc || function(e) {
var t = +e;
return (t > 0 ? r : n)(t);
};
}, {} ],
118: [ function(e, t) {
var n, r, o, i, s, a, c, l, u = e("../internals/global"), p = e("../internals/function-bind-context"), f = e("../internals/object-get-own-property-descriptor").f, d = e("../internals/task").set, h = e("../internals/engine-is-ios"), y = e("../internals/engine-is-ios-pebble"), v = e("../internals/engine-is-webos-webkit"), g = e("../internals/engine-is-node"), _ = u.MutationObserver || u.WebKitMutationObserver, m = u.document, b = u.process, w = u.Promise, S = f(u, "queueMicrotask"), I = S && S.value;
if (!I) {
n = function() {
var e, t;
g && (e = b.domain) && e.exit();
for (;r; ) {
t = r.fn;
r = r.next;
try {
t();
} catch (e) {
r ? i() : o = void 0;
throw e;
}
}
o = void 0;
e && e.enter();
};
if (h || g || v || !_ || !m) if (!y && w && w.resolve) {
(c = w.resolve(void 0)).constructor = w;
l = p(c.then, c);
i = function() {
l(n);
};
} else if (g) i = function() {
b.nextTick(n);
}; else {
d = p(d, u);
i = function() {
d(n);
};
} else {
s = !0;
a = m.createTextNode("");
new _(n).observe(a, {
characterData: !0
});
i = function() {
a.data = s = !s;
};
}
}
t.exports = I || function(e) {
var t = {
fn: e,
next: void 0
};
o && (o.next = t);
if (!r) {
r = t;
i();
}
o = t;
};
}, {
"../internals/engine-is-ios": 66,
"../internals/engine-is-ios-pebble": 65,
"../internals/engine-is-node": 67,
"../internals/engine-is-webos-webkit": 68,
"../internals/function-bind-context": 77,
"../internals/global": 86,
"../internals/object-get-own-property-descriptor": 126,
"../internals/task": 163
} ],
119: [ function(e, t) {
"use strict";
var n = e("../internals/a-callable"), r = TypeError, o = function(e) {
var t, o;
this.promise = new e(function(e, n) {
if (void 0 !== t || void 0 !== o) throw r("Bad Promise constructor");
t = e;
o = n;
});
this.resolve = n(t);
this.reject = n(o);
};
t.exports.f = function(e) {
return new o(e);
};
}, {
"../internals/a-callable": 12
} ],
120: [ function(e, t) {
var n = e("../internals/is-regexp"), r = TypeError;
t.exports = function(e) {
if (n(e)) throw r("The method doesn't accept regular expressions");
return e;
};
}, {
"../internals/is-regexp": 107
} ],
121: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/fails"), o = e("../internals/function-uncurry-this"), i = e("../internals/to-string"), s = e("../internals/string-trim").trim, a = e("../internals/whitespaces"), c = n.parseInt, l = n.Symbol, u = l && l.iterator, p = /^[+-]?0x/i, f = o(p.exec), d = 8 !== c(a + "08") || 22 !== c(a + "0x16") || u && !r(function() {
c(Object(u));
});
t.exports = d ? function(e, t) {
var n = s(i(e));
return c(n, t >>> 0 || (f(p, n) ? 16 : 10));
} : c;
}, {
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/global": 86,
"../internals/string-trim": 161,
"../internals/to-string": 177,
"../internals/whitespaces": 190
} ],
122: [ function(e, t) {
"use strict";
var n = e("../internals/descriptors"), r = e("../internals/function-uncurry-this"), o = e("../internals/function-call"), i = e("../internals/fails"), s = e("../internals/object-keys"), a = e("../internals/object-get-own-property-symbols"), c = e("../internals/object-property-is-enumerable"), l = e("../internals/to-object"), u = e("../internals/indexed-object"), p = Object.assign, f = Object.defineProperty, d = r([].concat);
t.exports = !p || i(function() {
if (n && 1 !== p({
b: 1
}, p(f({}, "a", {
enumerable: !0,
get: function() {
f(this, "b", {
value: 3,
enumerable: !1
});
}
}), {
b: 2
})).b) return !0;
var e = {}, t = {}, r = Symbol();
e[r] = 7;
"abcdefghijklmnopqrst".split("").forEach(function(e) {
t[e] = e;
});
return 7 != p({}, e)[r] || "abcdefghijklmnopqrst" != s(p({}, t)).join("");
}) ? function(e) {
for (var t = l(e), r = arguments.length, i = 1, p = a.f, f = c.f; r > i; ) for (var h, y = u(arguments[i++]), v = p ? d(s(y), p(y)) : s(y), g = v.length, _ = 0; g > _; ) {
h = v[_++];
n && !o(f, y, h) || (t[h] = y[h]);
}
return t;
} : p;
}, {
"../internals/descriptors": 55,
"../internals/fails": 74,
"../internals/function-call": 79,
"../internals/function-uncurry-this": 81,
"../internals/indexed-object": 93,
"../internals/object-get-own-property-symbols": 128,
"../internals/object-keys": 132,
"../internals/object-property-is-enumerable": 133,
"../internals/to-object": 171
} ],
123: [ function(e, t) {
var n, r = e("../internals/an-object"), o = e("../internals/object-define-properties"), i = e("../internals/enum-bug-keys"), s = e("../internals/hidden-keys"), a = e("../internals/html"), c = e("../internals/document-create-element"), l = e("../internals/shared-key")("IE_PROTO"), u = function() {}, p = function(e) {
return "<script>" + e + "<\/script>";
}, f = function(e) {
e.write(p(""));
e.close();
var t = e.parentWindow.Object;
e = null;
return t;
}, d = function() {
var e, t = c("iframe");
t.style.display = "none";
a.appendChild(t);
t.src = String("javascript:");
(e = t.contentWindow.document).open();
e.write(p("document.F=Object"));
e.close();
return e.F;
}, h = function() {
try {
n = new ActiveXObject("htmlfile");
} catch (e) {}
h = "undefined" != typeof document ? document.domain && n ? f(n) : d() : f(n);
for (var e = i.length; e--; ) delete h.prototype[i[e]];
return h();
};
s[l] = !0;
t.exports = Object.create || function(e, t) {
var n;
if (null !== e) {
u.prototype = r(e);
n = new u();
u.prototype = null;
n[l] = e;
} else n = h();
return void 0 === t ? n : o.f(n, t);
};
}, {
"../internals/an-object": 18,
"../internals/document-create-element": 57,
"../internals/enum-bug-keys": 72,
"../internals/hidden-keys": 88,
"../internals/html": 90,
"../internals/object-define-properties": 124,
"../internals/shared-key": 156
} ],
124: [ function(e, t, n) {
var r = e("../internals/descriptors"), o = e("../internals/v8-prototype-define-bug"), i = e("../internals/object-define-property"), s = e("../internals/an-object"), a = e("../internals/to-indexed-object"), c = e("../internals/object-keys");
n.f = r && !o ? Object.defineProperties : function(e, t) {
s(e);
for (var n, r = a(t), o = c(t), l = o.length, u = 0; l > u; ) i.f(e, n = o[u++], r[n]);
return e;
};
}, {
"../internals/an-object": 18,
"../internals/descriptors": 55,
"../internals/object-define-property": 125,
"../internals/object-keys": 132,
"../internals/to-indexed-object": 168,
"../internals/v8-prototype-define-bug": 186
} ],
125: [ function(e, t, n) {
var r = e("../internals/descriptors"), o = e("../internals/ie8-dom-define"), i = e("../internals/v8-prototype-define-bug"), s = e("../internals/an-object"), a = e("../internals/to-property-key"), c = TypeError, l = Object.defineProperty, u = Object.getOwnPropertyDescriptor;
n.f = r ? i ? function(e, t, n) {
s(e);
t = a(t);
s(n);
if ("function" == typeof e && "prototype" === t && "value" in n && "writable" in n && !n.writable) {
var r = u(e, t);
if (r && r.writable) {
e[t] = n.value;
n = {
configurable: "configurable" in n ? n.configurable : r.configurable,
enumerable: "enumerable" in n ? n.enumerable : r.enumerable,
writable: !1
};
}
}
return l(e, t, n);
} : l : function(e, t, n) {
s(e);
t = a(t);
s(n);
if (o) try {
return l(e, t, n);
} catch (e) {}
if ("get" in n || "set" in n) throw c("Accessors not supported");
"value" in n && (e[t] = n.value);
return e;
};
}, {
"../internals/an-object": 18,
"../internals/descriptors": 55,
"../internals/ie8-dom-define": 91,
"../internals/to-property-key": 175,
"../internals/v8-prototype-define-bug": 186
} ],
126: [ function(e, t, n) {
var r = e("../internals/descriptors"), o = e("../internals/function-call"), i = e("../internals/object-property-is-enumerable"), s = e("../internals/create-property-descriptor"), a = e("../internals/to-indexed-object"), c = e("../internals/to-property-key"), l = e("../internals/has-own-property"), u = e("../internals/ie8-dom-define"), p = Object.getOwnPropertyDescriptor;
n.f = r ? p : function(e, t) {
e = a(e);
t = c(t);
if (u) try {
return p(e, t);
} catch (e) {}
if (l(e, t)) return s(!o(i.f, e, t), e[t]);
};
}, {
"../internals/create-property-descriptor": 48,
"../internals/descriptors": 55,
"../internals/function-call": 79,
"../internals/has-own-property": 87,
"../internals/ie8-dom-define": 91,
"../internals/object-property-is-enumerable": 133,
"../internals/to-indexed-object": 168,
"../internals/to-property-key": 175
} ],
127: [ function(e, t, n) {
var r = e("../internals/object-keys-internal"), o = e("../internals/enum-bug-keys").concat("length", "prototype");
n.f = Object.getOwnPropertyNames || function(e) {
return r(e, o);
};
}, {
"../internals/enum-bug-keys": 72,
"../internals/object-keys-internal": 131
} ],
128: [ function(e, t, n) {
n.f = Object.getOwnPropertySymbols;
}, {} ],
129: [ function(e, t) {
var n = e("../internals/has-own-property"), r = e("../internals/is-callable"), o = e("../internals/to-object"), i = e("../internals/shared-key"), s = e("../internals/correct-prototype-getter"), a = i("IE_PROTO"), c = Object, l = c.prototype;
t.exports = s ? c.getPrototypeOf : function(e) {
var t = o(e);
if (n(t, a)) return t[a];
var i = t.constructor;
return r(i) && t instanceof i ? i.prototype : t instanceof c ? l : null;
};
}, {
"../internals/correct-prototype-getter": 45,
"../internals/has-own-property": 87,
"../internals/is-callable": 100,
"../internals/shared-key": 156,
"../internals/to-object": 171
} ],
130: [ function(e, t) {
var n = e("../internals/function-uncurry-this");
t.exports = n({}.isPrototypeOf);
}, {
"../internals/function-uncurry-this": 81
} ],
131: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/has-own-property"), o = e("../internals/to-indexed-object"), i = e("../internals/array-includes").indexOf, s = e("../internals/hidden-keys"), a = n([].push);
t.exports = function(e, t) {
var n, c = o(e), l = 0, u = [];
for (n in c) !r(s, n) && r(c, n) && a(u, n);
for (;t.length > l; ) r(c, n = t[l++]) && (~i(u, n) || a(u, n));
return u;
};
}, {
"../internals/array-includes": 27,
"../internals/function-uncurry-this": 81,
"../internals/has-own-property": 87,
"../internals/hidden-keys": 88,
"../internals/to-indexed-object": 168
} ],
132: [ function(e, t) {
var n = e("../internals/object-keys-internal"), r = e("../internals/enum-bug-keys");
t.exports = Object.keys || function(e) {
return n(e, r);
};
}, {
"../internals/enum-bug-keys": 72,
"../internals/object-keys-internal": 131
} ],
133: [ function(e, t, n) {
"use strict";
var r = {}.propertyIsEnumerable, o = Object.getOwnPropertyDescriptor, i = o && !r.call({
1: 2
}, 1);
n.f = i ? function(e) {
var t = o(this, e);
return !!t && t.enumerable;
} : r;
}, {} ],
134: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/an-object"), o = e("../internals/a-possible-prototype");
t.exports = Object.setPrototypeOf || ("__proto__" in {} ? function() {
var e, t = !1, i = {};
try {
(e = n(Object.getOwnPropertyDescriptor(Object.prototype, "__proto__").set))(i, []);
t = i instanceof Array;
} catch (e) {}
return function(n, i) {
r(n);
o(i);
t ? e(n, i) : n.__proto__ = i;
return n;
};
}() : void 0);
}, {
"../internals/a-possible-prototype": 14,
"../internals/an-object": 18,
"../internals/function-uncurry-this": 81
} ],
135: [ function(e, t) {
var n = e("../internals/descriptors"), r = e("../internals/function-uncurry-this"), o = e("../internals/object-keys"), i = e("../internals/to-indexed-object"), s = r(e("../internals/object-property-is-enumerable").f), a = r([].push), c = function(e) {
return function(t) {
for (var r, c = i(t), l = o(c), u = l.length, p = 0, f = []; u > p; ) {
r = l[p++];
n && !s(c, r) || a(f, e ? [ r, c[r] ] : c[r]);
}
return f;
};
};
t.exports = {
entries: c(!0),
values: c(!1)
};
}, {
"../internals/descriptors": 55,
"../internals/function-uncurry-this": 81,
"../internals/object-keys": 132,
"../internals/object-property-is-enumerable": 133,
"../internals/to-indexed-object": 168
} ],
136: [ function(e, t) {
"use strict";
var n = e("../internals/to-string-tag-support"), r = e("../internals/classof");
t.exports = n ? {}.toString : function() {
return "[object " + r(this) + "]";
};
}, {
"../internals/classof": 42,
"../internals/to-string-tag-support": 176
} ],
137: [ function(e, t) {
var n = e("../internals/function-call"), r = e("../internals/is-callable"), o = e("../internals/is-object"), i = TypeError;
t.exports = function(e, t) {
var s, a;
if ("string" === t && r(s = e.toString) && !o(a = n(s, e))) return a;
if (r(s = e.valueOf) && !o(a = n(s, e))) return a;
if ("string" !== t && r(s = e.toString) && !o(a = n(s, e))) return a;
throw i("Can't convert object to primitive value");
};
}, {
"../internals/function-call": 79,
"../internals/is-callable": 100,
"../internals/is-object": 105
} ],
138: [ function(e, t) {
var n = e("../internals/get-built-in"), r = e("../internals/function-uncurry-this"), o = e("../internals/object-get-own-property-names"), i = e("../internals/object-get-own-property-symbols"), s = e("../internals/an-object"), a = r([].concat);
t.exports = n("Reflect", "ownKeys") || function(e) {
var t = o.f(s(e)), n = i.f;
return n ? a(t, n(e)) : t;
};
}, {
"../internals/an-object": 18,
"../internals/function-uncurry-this": 81,
"../internals/get-built-in": 82,
"../internals/object-get-own-property-names": 127,
"../internals/object-get-own-property-symbols": 128
} ],
139: [ function(e, t) {
t.exports = function(e) {
try {
return {
error: !1,
value: e()
};
} catch (e) {
return {
error: !0,
value: e
};
}
};
}, {} ],
140: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/promise-native-constructor"), o = e("../internals/is-callable"), i = e("../internals/is-forced"), s = e("../internals/inspect-source"), a = e("../internals/well-known-symbol"), c = e("../internals/engine-is-browser"), l = e("../internals/engine-is-deno"), u = e("../internals/is-pure"), p = e("../internals/engine-v8-version"), f = r && r.prototype, d = a("species"), h = !1, y = o(n.PromiseRejectionEvent), v = i("Promise", function() {
var e = s(r), t = e !== String(r);
if (!t && 66 === p) return !0;
if (u && (!f.catch || !f.finally)) return !0;
if (!p || p < 51 || !/native code/.test(e)) {
var n = new r(function(e) {
e(1);
}), o = function(e) {
e(function() {}, function() {});
};
(n.constructor = {})[d] = o;
if (!(h = n.then(function() {}) instanceof o)) return !0;
}
return !t && (c || l) && !y;
});
t.exports = {
CONSTRUCTOR: v,
REJECTION_EVENT: y,
SUBCLASSING: h
};
}, {
"../internals/engine-is-browser": 62,
"../internals/engine-is-deno": 63,
"../internals/engine-v8-version": 70,
"../internals/global": 86,
"../internals/inspect-source": 95,
"../internals/is-callable": 100,
"../internals/is-forced": 102,
"../internals/is-pure": 106,
"../internals/promise-native-constructor": 141,
"../internals/well-known-symbol": 189
} ],
141: [ function(e, t) {
var n = e("../internals/global");
t.exports = n.Promise;
}, {
"../internals/global": 86
} ],
142: [ function(e, t) {
var n = e("../internals/an-object"), r = e("../internals/is-object"), o = e("../internals/new-promise-capability");
t.exports = function(e, t) {
n(e);
if (r(t) && t.constructor === e) return t;
var i = o.f(e);
(0, i.resolve)(t);
return i.promise;
};
}, {
"../internals/an-object": 18,
"../internals/is-object": 105,
"../internals/new-promise-capability": 119
} ],
143: [ function(e, t) {
var n = e("../internals/promise-native-constructor"), r = e("../internals/check-correctness-of-iteration"), o = e("../internals/promise-constructor-detection").CONSTRUCTOR;
t.exports = o || !r(function(e) {
n.all(e).then(void 0, function() {});
});
}, {
"../internals/check-correctness-of-iteration": 40,
"../internals/promise-constructor-detection": 140,
"../internals/promise-native-constructor": 141
} ],
144: [ function(e, t) {
var n = e("../internals/object-define-property").f;
t.exports = function(e, t, r) {
r in e || n(e, r, {
configurable: !0,
get: function() {
return t[r];
},
set: function(e) {
t[r] = e;
}
});
};
}, {
"../internals/object-define-property": 125
} ],
145: [ function(e, t) {
var n = function() {
this.head = null;
this.tail = null;
};
n.prototype = {
add: function(e) {
var t = {
item: e,
next: null
};
this.head ? this.tail.next = t : this.head = t;
this.tail = t;
},
get: function() {
var e = this.head;
if (e) {
this.head = e.next;
this.tail === e && (this.tail = null);
return e.item;
}
}
};
t.exports = n;
}, {} ],
146: [ function(e, t) {
var n = e("../internals/function-call"), r = e("../internals/an-object"), o = e("../internals/is-callable"), i = e("../internals/classof-raw"), s = e("../internals/regexp-exec"), a = TypeError;
t.exports = function(e, t) {
var c = e.exec;
if (o(c)) {
var l = n(c, e, t);
null !== l && r(l);
return l;
}
if ("RegExp" === i(e)) return n(s, e, t);
throw a("RegExp#exec called on incompatible receiver");
};
}, {
"../internals/an-object": 18,
"../internals/classof-raw": 41,
"../internals/function-call": 79,
"../internals/is-callable": 100,
"../internals/regexp-exec": 147
} ],
147: [ function(e, t) {
"use strict";
var n = e("../internals/function-call"), r = e("../internals/function-uncurry-this"), o = e("../internals/to-string"), i = e("../internals/regexp-flags"), s = e("../internals/regexp-sticky-helpers"), a = e("../internals/shared"), c = e("../internals/object-create"), l = e("../internals/internal-state").get, u = e("../internals/regexp-unsupported-dot-all"), p = e("../internals/regexp-unsupported-ncg"), f = a("native-string-replace", String.prototype.replace), d = RegExp.prototype.exec, h = d, y = r("".charAt), v = r("".indexOf), g = r("".replace), _ = r("".slice), m = function() {
var e = /a/, t = /b*/g;
n(d, e, "a");
n(d, t, "a");
return 0 !== e.lastIndex || 0 !== t.lastIndex;
}(), b = s.BROKEN_CARET, w = void 0 !== /()??/.exec("")[1];
(m || w || b || u || p) && (h = function(e) {
var t, r, s, a, u, p, S, I = this, O = l(I), j = o(e), P = O.raw;
if (P) {
P.lastIndex = I.lastIndex;
t = n(h, P, j);
I.lastIndex = P.lastIndex;
return t;
}
var C = O.groups, T = b && I.sticky, M = n(i, I), x = I.source, k = 0, R = j;
if (T) {
M = g(M, "y", "");
-1 === v(M, "g") && (M += "g");
R = _(j, I.lastIndex);
if (I.lastIndex > 0 && (!I.multiline || I.multiline && "\n" !== y(j, I.lastIndex - 1))) {
x = "(?: " + x + ")";
R = " " + R;
k++;
}
r = new RegExp("^(?:" + x + ")", M);
}
w && (r = new RegExp("^" + x + "$(?!\\s)", M));
m && (s = I.lastIndex);
a = n(d, T ? r : I, R);
if (T) if (a) {
a.input = _(a.input, k);
a[0] = _(a[0], k);
a.index = I.lastIndex;
I.lastIndex += a[0].length;
} else I.lastIndex = 0; else m && a && (I.lastIndex = I.global ? a.index + a[0].length : s);
w && a && a.length > 1 && n(f, a[0], r, function() {
for (u = 1; u < arguments.length - 2; u++) void 0 === arguments[u] && (a[u] = void 0);
});
if (a && C) {
a.groups = p = c(null);
for (u = 0; u < C.length; u++) p[(S = C[u])[0]] = a[S[1]];
}
return a;
});
t.exports = h;
}, {
"../internals/function-call": 79,
"../internals/function-uncurry-this": 81,
"../internals/internal-state": 96,
"../internals/object-create": 123,
"../internals/regexp-flags": 148,
"../internals/regexp-sticky-helpers": 150,
"../internals/regexp-unsupported-dot-all": 151,
"../internals/regexp-unsupported-ncg": 152,
"../internals/shared": 158,
"../internals/to-string": 177
} ],
148: [ function(e, t) {
"use strict";
var n = e("../internals/an-object");
t.exports = function() {
var e = n(this), t = "";
e.hasIndices && (t += "d");
e.global && (t += "g");
e.ignoreCase && (t += "i");
e.multiline && (t += "m");
e.dotAll && (t += "s");
e.unicode && (t += "u");
e.unicodeSets && (t += "v");
e.sticky && (t += "y");
return t;
};
}, {
"../internals/an-object": 18
} ],
149: [ function(e, t) {
var n = e("../internals/function-call"), r = e("../internals/has-own-property"), o = e("../internals/object-is-prototype-of"), i = e("../internals/regexp-flags"), s = RegExp.prototype;
t.exports = function(e) {
var t = e.flags;
return void 0 !== t || "flags" in s || r(e, "flags") || !o(s, e) ? t : n(i, e);
};
}, {
"../internals/function-call": 79,
"../internals/has-own-property": 87,
"../internals/object-is-prototype-of": 130,
"../internals/regexp-flags": 148
} ],
150: [ function(e, t) {
var n = e("../internals/fails"), r = e("../internals/global").RegExp, o = n(function() {
var e = r("a", "y");
e.lastIndex = 2;
return null != e.exec("abcd");
}), i = o || n(function() {
return !r("a", "y").sticky;
}), s = o || n(function() {
var e = r("^r", "gy");
e.lastIndex = 2;
return null != e.exec("str");
});
t.exports = {
BROKEN_CARET: s,
MISSED_STICKY: i,
UNSUPPORTED_Y: o
};
}, {
"../internals/fails": 74,
"../internals/global": 86
} ],
151: [ function(e, t) {
var n = e("../internals/fails"), r = e("../internals/global").RegExp;
t.exports = n(function() {
var e = r(".", "s");
return !(e.dotAll && e.exec("\n") && "s" === e.flags);
});
}, {
"../internals/fails": 74,
"../internals/global": 86
} ],
152: [ function(e, t) {
var n = e("../internals/fails"), r = e("../internals/global").RegExp;
t.exports = n(function() {
var e = r("(?<a>b)", "g");
return "b" !== e.exec("b").groups.a || "bc" !== "b".replace(e, "$<a>c");
});
}, {
"../internals/fails": 74,
"../internals/global": 86
} ],
153: [ function(e, t) {
var n = e("../internals/is-null-or-undefined"), r = TypeError;
t.exports = function(e) {
if (n(e)) throw r("Can't call method on " + e);
return e;
};
}, {
"../internals/is-null-or-undefined": 104
} ],
154: [ function(e, t) {
"use strict";
var n = e("../internals/get-built-in"), r = e("../internals/object-define-property"), o = e("../internals/well-known-symbol"), i = e("../internals/descriptors"), s = o("species");
t.exports = function(e) {
var t = n(e), o = r.f;
i && t && !t[s] && o(t, s, {
configurable: !0,
get: function() {
return this;
}
});
};
}, {
"../internals/descriptors": 55,
"../internals/get-built-in": 82,
"../internals/object-define-property": 125,
"../internals/well-known-symbol": 189
} ],
155: [ function(e, t) {
var n = e("../internals/object-define-property").f, r = e("../internals/has-own-property"), o = e("../internals/well-known-symbol")("toStringTag");
t.exports = function(e, t, i) {
e && !i && (e = e.prototype);
e && !r(e, o) && n(e, o, {
configurable: !0,
value: t
});
};
}, {
"../internals/has-own-property": 87,
"../internals/object-define-property": 125,
"../internals/well-known-symbol": 189
} ],
156: [ function(e, t) {
var n = e("../internals/shared"), r = e("../internals/uid"), o = n("keys");
t.exports = function(e) {
return o[e] || (o[e] = r(e));
};
}, {
"../internals/shared": 158,
"../internals/uid": 184
} ],
157: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/define-global-property"), o = n["__core-js_shared__"] || r("__core-js_shared__", {});
t.exports = o;
}, {
"../internals/define-global-property": 53,
"../internals/global": 86
} ],
158: [ function(e, t) {
var n = e("../internals/is-pure"), r = e("../internals/shared-store");
(t.exports = function(e, t) {
return r[e] || (r[e] = void 0 !== t ? t : {});
})("versions", []).push({
version: "3.25.2",
mode: n ? "pure" : "global",
copyright: "© 2014-2022 Denis Pushkarev (zloirock.ru)",
license: "https://github.com/zloirock/core-js/blob/v3.25.2/LICENSE",
source: "https://github.com/zloirock/core-js"
});
}, {
"../internals/is-pure": 106,
"../internals/shared-store": 157
} ],
159: [ function(e, t) {
var n = e("../internals/an-object"), r = e("../internals/a-constructor"), o = e("../internals/is-null-or-undefined"), i = e("../internals/well-known-symbol")("species");
t.exports = function(e, t) {
var s, a = n(e).constructor;
return void 0 === a || o(s = n(a)[i]) ? t : r(s);
};
}, {
"../internals/a-constructor": 13,
"../internals/an-object": 18,
"../internals/is-null-or-undefined": 104,
"../internals/well-known-symbol": 189
} ],
160: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/to-integer-or-infinity"), o = e("../internals/to-string"), i = e("../internals/require-object-coercible"), s = n("".charAt), a = n("".charCodeAt), c = n("".slice), l = function(e) {
return function(t, n) {
var l, u, p = o(i(t)), f = r(n), d = p.length;
return f < 0 || f >= d ? e ? "" : void 0 : (l = a(p, f)) < 55296 || l > 56319 || f + 1 === d || (u = a(p, f + 1)) < 56320 || u > 57343 ? e ? s(p, f) : l : e ? c(p, f, f + 2) : u - 56320 + (l - 55296 << 10) + 65536;
};
};
t.exports = {
codeAt: l(!1),
charAt: l(!0)
};
}, {
"../internals/function-uncurry-this": 81,
"../internals/require-object-coercible": 153,
"../internals/to-integer-or-infinity": 169,
"../internals/to-string": 177
} ],
161: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = e("../internals/require-object-coercible"), o = e("../internals/to-string"), i = e("../internals/whitespaces"), s = n("".replace), a = "[" + i + "]", c = RegExp("^" + a + a + "*"), l = RegExp(a + a + "*$"), u = function(e) {
return function(t) {
var n = o(r(t));
1 & e && (n = s(n, c, ""));
2 & e && (n = s(n, l, ""));
return n;
};
};
t.exports = {
start: u(1),
end: u(2),
trim: u(3)
};
}, {
"../internals/function-uncurry-this": 81,
"../internals/require-object-coercible": 153,
"../internals/to-string": 177,
"../internals/whitespaces": 190
} ],
162: [ function(e, t) {
var n = e("../internals/engine-v8-version"), r = e("../internals/fails");
t.exports = !!Object.getOwnPropertySymbols && !r(function() {
var e = Symbol();
return !String(e) || !(Object(e) instanceof Symbol) || !Symbol.sham && n && n < 41;
});
}, {
"../internals/engine-v8-version": 70,
"../internals/fails": 74
} ],
163: [ function(e, t) {
var n, r, o, i, s = e("../internals/global"), a = e("../internals/function-apply"), c = e("../internals/function-bind-context"), l = e("../internals/is-callable"), u = e("../internals/has-own-property"), p = e("../internals/fails"), f = e("../internals/html"), d = e("../internals/array-slice"), h = e("../internals/document-create-element"), y = e("../internals/validate-arguments-length"), v = e("../internals/engine-is-ios"), g = e("../internals/engine-is-node"), _ = s.setImmediate, m = s.clearImmediate, b = s.process, w = s.Dispatch, S = s.Function, I = s.MessageChannel, O = s.String, j = 0, P = {};
try {
n = s.location;
} catch (e) {}
var C = function(e) {
if (u(P, e)) {
var t = P[e];
delete P[e];
t();
}
}, T = function(e) {
return function() {
C(e);
};
}, M = function(e) {
C(e.data);
}, x = function(e) {
s.postMessage(O(e), n.protocol + "//" + n.host);
};
if (!_ || !m) {
_ = function(e) {
y(arguments.length, 1);
var t = l(e) ? e : S(e), n = d(arguments, 1);
P[++j] = function() {
a(t, void 0, n);
};
r(j);
return j;
};
m = function(e) {
delete P[e];
};
if (g) r = function(e) {
b.nextTick(T(e));
}; else if (w && w.now) r = function(e) {
w.now(T(e));
}; else if (I && !v) {
i = (o = new I()).port2;
o.port1.onmessage = M;
r = c(i.postMessage, i);
} else if (s.addEventListener && l(s.postMessage) && !s.importScripts && n && "file:" !== n.protocol && !p(x)) {
r = x;
s.addEventListener("message", M, !1);
} else r = "onreadystatechange" in h("script") ? function(e) {
f.appendChild(h("script")).onreadystatechange = function() {
f.removeChild(this);
C(e);
};
} : function(e) {
setTimeout(T(e), 0);
};
}
t.exports = {
set: _,
clear: m
};
}, {
"../internals/array-slice": 35,
"../internals/document-create-element": 57,
"../internals/engine-is-ios": 66,
"../internals/engine-is-node": 67,
"../internals/fails": 74,
"../internals/function-apply": 76,
"../internals/function-bind-context": 77,
"../internals/global": 86,
"../internals/has-own-property": 87,
"../internals/html": 90,
"../internals/is-callable": 100,
"../internals/validate-arguments-length": 187
} ],
164: [ function(e, t) {
var n = e("../internals/function-uncurry-this");
t.exports = n(1..valueOf);
}, {
"../internals/function-uncurry-this": 81
} ],
165: [ function(e, t) {
var n = e("../internals/to-integer-or-infinity"), r = Math.max, o = Math.min;
t.exports = function(e, t) {
var i = n(e);
return i < 0 ? r(i + t, 0) : o(i, t);
};
}, {
"../internals/to-integer-or-infinity": 169
} ],
166: [ function(e, t) {
var n = e("../internals/to-primitive"), r = TypeError;
t.exports = function(e) {
var t = n(e, "number");
if ("number" == typeof t) throw r("Can't convert number to bigint");
return BigInt(t);
};
}, {
"../internals/to-primitive": 174
} ],
167: [ function(e, t) {
var n = e("../internals/to-integer-or-infinity"), r = e("../internals/to-length"), o = RangeError;
t.exports = function(e) {
if (void 0 === e) return 0;
var t = n(e), i = r(t);
if (t !== i) throw o("Wrong length or index");
return i;
};
}, {
"../internals/to-integer-or-infinity": 169,
"../internals/to-length": 170
} ],
168: [ function(e, t) {
var n = e("../internals/indexed-object"), r = e("../internals/require-object-coercible");
t.exports = function(e) {
return n(r(e));
};
}, {
"../internals/indexed-object": 93,
"../internals/require-object-coercible": 153
} ],
169: [ function(e, t) {
var n = e("../internals/math-trunc");
t.exports = function(e) {
var t = +e;
return t != t || 0 === t ? 0 : n(t);
};
}, {
"../internals/math-trunc": 117
} ],
170: [ function(e, t) {
var n = e("../internals/to-integer-or-infinity"), r = Math.min;
t.exports = function(e) {
return e > 0 ? r(n(e), 9007199254740991) : 0;
};
}, {
"../internals/to-integer-or-infinity": 169
} ],
171: [ function(e, t) {
var n = e("../internals/require-object-coercible"), r = Object;
t.exports = function(e) {
return r(n(e));
};
}, {
"../internals/require-object-coercible": 153
} ],
172: [ function(e, t) {
var n = e("../internals/to-positive-integer"), r = RangeError;
t.exports = function(e, t) {
var o = n(e);
if (o % t) throw r("Wrong offset");
return o;
};
}, {
"../internals/to-positive-integer": 173
} ],
173: [ function(e, t) {
var n = e("../internals/to-integer-or-infinity"), r = RangeError;
t.exports = function(e) {
var t = n(e);
if (t < 0) throw r("The argument can't be less than 0");
return t;
};
}, {
"../internals/to-integer-or-infinity": 169
} ],
174: [ function(e, t) {
var n = e("../internals/function-call"), r = e("../internals/is-object"), o = e("../internals/is-symbol"), i = e("../internals/get-method"), s = e("../internals/ordinary-to-primitive"), a = e("../internals/well-known-symbol"), c = TypeError, l = a("toPrimitive");
t.exports = function(e, t) {
if (!r(e) || o(e)) return e;
var a, u = i(e, l);
if (u) {
void 0 === t && (t = "default");
a = n(u, e, t);
if (!r(a) || o(a)) return a;
throw c("Can't convert object to primitive value");
}
void 0 === t && (t = "number");
return s(e, t);
};
}, {
"../internals/function-call": 79,
"../internals/get-method": 85,
"../internals/is-object": 105,
"../internals/is-symbol": 108,
"../internals/ordinary-to-primitive": 137,
"../internals/well-known-symbol": 189
} ],
175: [ function(e, t) {
var n = e("../internals/to-primitive"), r = e("../internals/is-symbol");
t.exports = function(e) {
var t = n(e, "string");
return r(t) ? t : t + "";
};
}, {
"../internals/is-symbol": 108,
"../internals/to-primitive": 174
} ],
176: [ function(e, t) {
var n = {};
n[e("../internals/well-known-symbol")("toStringTag")] = "z";
t.exports = "[object z]" === String(n);
}, {
"../internals/well-known-symbol": 189
} ],
177: [ function(e, t) {
var n = e("../internals/classof"), r = String;
t.exports = function(e) {
if ("Symbol" === n(e)) throw TypeError("Cannot convert a Symbol value to a string");
return r(e);
};
}, {
"../internals/classof": 42
} ],
178: [ function(e, t) {
var n = String;
t.exports = function(e) {
try {
return n(e);
} catch (e) {
return "Object";
}
};
}, {} ],
179: [ function(e, t) {
"use strict";
var n = e("../internals/export"), r = e("../internals/global"), o = e("../internals/function-call"), i = e("../internals/descriptors"), s = e("../internals/typed-array-constructors-require-wrappers"), a = e("../internals/array-buffer-view-core"), c = e("../internals/array-buffer"), l = e("../internals/an-instance"), u = e("../internals/create-property-descriptor"), p = e("../internals/create-non-enumerable-property"), f = e("../internals/is-integral-number"), d = e("../internals/to-length"), h = e("../internals/to-index"), y = e("../internals/to-offset"), v = e("../internals/to-property-key"), g = e("../internals/has-own-property"), _ = e("../internals/classof"), m = e("../internals/is-object"), b = e("../internals/is-symbol"), w = e("../internals/object-create"), S = e("../internals/object-is-prototype-of"), I = e("../internals/object-set-prototype-of"), O = e("../internals/object-get-own-property-names").f, j = e("../internals/typed-array-from"), P = e("../internals/array-iteration").forEach, C = e("../internals/set-species"), T = e("../internals/object-define-property"), M = e("../internals/object-get-own-property-descriptor"), x = e("../internals/internal-state"), k = e("../internals/inherit-if-required"), R = x.get, U = x.set, A = x.enforce, E = T.f, F = M.f, B = Math.round, L = r.RangeError, N = c.ArrayBuffer, D = N.prototype, G = c.DataView, W = a.NATIVE_ARRAY_BUFFER_VIEWS, H = a.TYPED_ARRAY_TAG, V = a.TypedArray, z = a.TypedArrayPrototype, J = a.aTypedArrayConstructor, q = a.isTypedArray, Y = function(e, t) {
J(e);
for (var n = 0, r = t.length, o = new e(r); r > n; ) o[n] = t[n++];
return o;
}, Z = function(e, t) {
E(e, t, {
get: function() {
return R(this)[t];
}
});
}, X = function(e) {
var t;
return S(D, e) || "ArrayBuffer" == (t = _(e)) || "SharedArrayBuffer" == t;
}, K = function(e, t) {
return q(e) && !b(t) && t in e && f(+t) && t >= 0;
}, Q = function(e, t) {
t = v(t);
return K(e, t) ? u(2, e[t]) : F(e, t);
}, $ = function(e, t, n) {
t = v(t);
if (K(e, t) && m(n) && g(n, "value") && !g(n, "get") && !g(n, "set") && !n.configurable && (!g(n, "writable") || n.writable) && (!g(n, "enumerable") || n.enumerable)) {
e[t] = n.value;
return e;
}
return E(e, t, n);
};
if (i) {
if (!W) {
M.f = Q;
T.f = $;
Z(z, "buffer");
Z(z, "byteOffset");
Z(z, "byteLength");
Z(z, "length");
}
n({
target: "Object",
stat: !0,
forced: !W
}, {
getOwnPropertyDescriptor: Q,
defineProperty: $
});
t.exports = function(e, t, i) {
var a = e.match(/\d+$/)[0] / 8, c = e + (i ? "Clamped" : "") + "Array", u = "get" + e, f = "set" + e, v = r[c], g = v, _ = g && g.prototype, b = {}, S = function(e, t) {
var n = R(e);
return n.view[u](t * a + n.byteOffset, !0);
}, T = function(e, t, n) {
var r = R(e);
i && (n = (n = B(n)) < 0 ? 0 : n > 255 ? 255 : 255 & n);
r.view[f](t * a + r.byteOffset, n, !0);
}, M = function(e, t) {
E(e, t, {
get: function() {
return S(this, t);
},
set: function(e) {
return T(this, t, e);
},
enumerable: !0
});
};
if (W) {
if (s) {
g = t(function(e, t, n, r) {
l(e, _);
return k(m(t) ? X(t) ? void 0 !== r ? new v(t, y(n, a), r) : void 0 !== n ? new v(t, y(n, a)) : new v(t) : q(t) ? Y(g, t) : o(j, g, t) : new v(h(t)), e, g);
});
I && I(g, V);
P(O(v), function(e) {
e in g || p(g, e, v[e]);
});
g.prototype = _;
}
} else {
g = t(function(e, t, n, r) {
l(e, _);
var i, s, c, u = 0, p = 0;
if (m(t)) {
if (!X(t)) return q(t) ? Y(g, t) : o(j, g, t);
i = t;
p = y(n, a);
var f = t.byteLength;
if (void 0 === r) {
if (f % a) throw L("Wrong length");
if ((s = f - p) < 0) throw L("Wrong length");
} else if ((s = d(r) * a) + p > f) throw L("Wrong length");
c = s / a;
} else {
c = h(t);
i = new N(s = c * a);
}
U(e, {
buffer: i,
byteOffset: p,
byteLength: s,
length: c,
view: new G(i)
});
for (;u < c; ) M(e, u++);
});
I && I(g, V);
_ = g.prototype = w(z);
}
_.constructor !== g && p(_, "constructor", g);
A(_).TypedArrayConstructor = g;
H && p(_, H, c);
var x = g != v;
b[c] = g;
n({
global: !0,
constructor: !0,
forced: x,
sham: !W
}, b);
"BYTES_PER_ELEMENT" in g || p(g, "BYTES_PER_ELEMENT", a);
"BYTES_PER_ELEMENT" in _ || p(_, "BYTES_PER_ELEMENT", a);
C(c);
};
} else t.exports = function() {};
}, {
"../internals/an-instance": 17,
"../internals/array-buffer": 21,
"../internals/array-buffer-view-core": 20,
"../internals/array-iteration": 28,
"../internals/classof": 42,
"../internals/create-non-enumerable-property": 47,
"../internals/create-property-descriptor": 48,
"../internals/descriptors": 55,
"../internals/export": 73,
"../internals/function-call": 79,
"../internals/global": 86,
"../internals/has-own-property": 87,
"../internals/inherit-if-required": 94,
"../internals/internal-state": 96,
"../internals/is-integral-number": 103,
"../internals/is-object": 105,
"../internals/is-symbol": 108,
"../internals/object-create": 123,
"../internals/object-define-property": 125,
"../internals/object-get-own-property-descriptor": 126,
"../internals/object-get-own-property-names": 127,
"../internals/object-is-prototype-of": 130,
"../internals/object-set-prototype-of": 134,
"../internals/set-species": 154,
"../internals/to-index": 167,
"../internals/to-length": 170,
"../internals/to-offset": 172,
"../internals/to-property-key": 175,
"../internals/typed-array-constructors-require-wrappers": 180,
"../internals/typed-array-from": 182
} ],
180: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/fails"), o = e("../internals/check-correctness-of-iteration"), i = e("../internals/array-buffer-view-core").NATIVE_ARRAY_BUFFER_VIEWS, s = n.ArrayBuffer, a = n.Int8Array;
t.exports = !i || !r(function() {
a(1);
}) || !r(function() {
new a(-1);
}) || !o(function(e) {
new a();
new a(null);
new a(1.5);
new a(e);
}, !0) || r(function() {
return 1 !== new a(new s(2), 1, void 0).length;
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/check-correctness-of-iteration": 40,
"../internals/fails": 74,
"../internals/global": 86
} ],
181: [ function(e, t) {
var n = e("../internals/array-from-constructor-and-list"), r = e("../internals/typed-array-species-constructor");
t.exports = function(e, t) {
return n(r(e), t);
};
}, {
"../internals/array-from-constructor-and-list": 25,
"../internals/typed-array-species-constructor": 183
} ],
182: [ function(e, t) {
var n = e("../internals/function-bind-context"), r = e("../internals/function-call"), o = e("../internals/a-constructor"), i = e("../internals/to-object"), s = e("../internals/length-of-array-like"), a = e("../internals/get-iterator"), c = e("../internals/get-iterator-method"), l = e("../internals/is-array-iterator-method"), u = e("../internals/is-big-int-array"), p = e("../internals/array-buffer-view-core").aTypedArrayConstructor, f = e("../internals/to-big-int");
t.exports = function(e) {
var t, d, h, y, v, g, _, m, b = o(this), w = i(e), S = arguments.length, I = S > 1 ? arguments[1] : void 0, O = void 0 !== I, j = c(w);
if (j && !l(j)) {
m = (_ = a(w, j)).next;
w = [];
for (;!(g = r(m, _)).done; ) w.push(g.value);
}
O && S > 2 && (I = n(I, arguments[2]));
d = s(w);
h = new (p(b))(d);
y = u(h);
for (t = 0; d > t; t++) {
v = O ? I(w[t], t) : w[t];
h[t] = y ? f(v) : +v;
}
return h;
};
}, {
"../internals/a-constructor": 13,
"../internals/array-buffer-view-core": 20,
"../internals/function-bind-context": 77,
"../internals/function-call": 79,
"../internals/get-iterator": 84,
"../internals/get-iterator-method": 83,
"../internals/is-array-iterator-method": 97,
"../internals/is-big-int-array": 99,
"../internals/length-of-array-like": 115,
"../internals/to-big-int": 166,
"../internals/to-object": 171
} ],
183: [ function(e, t) {
var n = e("../internals/array-buffer-view-core"), r = e("../internals/species-constructor"), o = n.aTypedArrayConstructor, i = n.getTypedArrayConstructor;
t.exports = function(e) {
return o(r(e, i(e)));
};
}, {
"../internals/array-buffer-view-core": 20,
"../internals/species-constructor": 159
} ],
184: [ function(e, t) {
var n = e("../internals/function-uncurry-this"), r = 0, o = Math.random(), i = n(1..toString);
t.exports = function(e) {
return "Symbol(" + (void 0 === e ? "" : e) + ")_" + i(++r + o, 36);
};
}, {
"../internals/function-uncurry-this": 81
} ],
185: [ function(e, t) {
var n = e("../internals/symbol-constructor-detection");
t.exports = n && !Symbol.sham && "symbol" == typeof Symbol.iterator;
}, {
"../internals/symbol-constructor-detection": 162
} ],
186: [ function(e, t) {
var n = e("../internals/descriptors"), r = e("../internals/fails");
t.exports = n && r(function() {
return 42 != Object.defineProperty(function() {}, "prototype", {
value: 42,
writable: !1
}).prototype;
});
}, {
"../internals/descriptors": 55,
"../internals/fails": 74
} ],
187: [ function(e, t) {
var n = TypeError;
t.exports = function(e, t) {
if (e < t) throw n("Not enough arguments");
return e;
};
}, {} ],
188: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/is-callable"), o = n.WeakMap;
t.exports = r(o) && /native code/.test(String(o));
}, {
"../internals/global": 86,
"../internals/is-callable": 100
} ],
189: [ function(e, t) {
var n = e("../internals/global"), r = e("../internals/shared"), o = e("../internals/has-own-property"), i = e("../internals/uid"), s = e("../internals/symbol-constructor-detection"), a = e("../internals/use-symbol-as-uid"), c = r("wks"), l = n.Symbol, u = l && l.for, p = a ? l : l && l.withoutSetter || i;
t.exports = function(e) {
if (!o(c, e) || !s && "string" != typeof c[e]) {
var t = "Symbol." + e;
s && o(l, e) ? c[e] = l[e] : c[e] = a && u ? u(t) : p(t);
}
return c[e];
};
}, {
"../internals/global": 86,
"../internals/has-own-property": 87,
"../internals/shared": 158,
"../internals/symbol-constructor-detection": 162,
"../internals/uid": 184,
"../internals/use-symbol-as-uid": 185
} ],
190: [ function(e, t) {
t.exports = "\t\n\v\f\r                　\u2028\u2029\ufeff";
}, {} ],
191: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/global"), r = e("../internals/array-buffer"), o = e("../internals/set-species"), i = r.ArrayBuffer;
t({
global: !0,
constructor: !0,
forced: n.ArrayBuffer !== i
}, {
ArrayBuffer: i
});
o("ArrayBuffer");
}, {
"../internals/array-buffer": 21,
"../internals/export": 73,
"../internals/global": 86,
"../internals/set-species": 154
} ],
192: [ function(e) {
var t = e("../internals/export"), n = e("../internals/array-buffer-view-core");
t({
target: "ArrayBuffer",
stat: !0,
forced: !n.NATIVE_ARRAY_BUFFER_VIEWS
}, {
isView: n.isView
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/export": 73
} ],
193: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/function-uncurry-this"), r = e("../internals/fails"), o = e("../internals/array-buffer"), i = e("../internals/an-object"), s = e("../internals/to-absolute-index"), a = e("../internals/to-length"), c = e("../internals/species-constructor"), l = o.ArrayBuffer, u = o.DataView, p = u.prototype, f = n(l.prototype.slice), d = n(p.getUint8), h = n(p.setUint8);
t({
target: "ArrayBuffer",
proto: !0,
unsafe: !0,
forced: r(function() {
return !new l(2).slice(1, void 0).byteLength;
})
}, {
slice: function(e, t) {
if (f && void 0 === t) return f(i(this), e);
for (var n = i(this).byteLength, r = s(e, n), o = s(void 0 === t ? n : t, n), p = new (c(this, l))(a(o - r)), y = new u(this), v = new u(p), g = 0; r < o; ) h(v, g++, d(y, r++));
return p;
}
});
}, {
"../internals/an-object": 18,
"../internals/array-buffer": 21,
"../internals/export": 73,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/species-constructor": 159,
"../internals/to-absolute-index": 165,
"../internals/to-length": 170
} ],
194: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/array-iteration").filter;
t({
target: "Array",
proto: !0,
forced: !e("../internals/array-method-has-species-support")("filter")
}, {
filter: function(e) {
return n(this, e, arguments.length > 1 ? arguments[1] : void 0);
}
});
}, {
"../internals/array-iteration": 28,
"../internals/array-method-has-species-support": 30,
"../internals/export": 73
} ],
195: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/array-iteration").findIndex, r = e("../internals/add-to-unscopables"), o = !0;
"findIndex" in [] && Array(1).findIndex(function() {
o = !1;
});
t({
target: "Array",
proto: !0,
forced: o
}, {
findIndex: function(e) {
return n(this, e, arguments.length > 1 ? arguments[1] : void 0);
}
});
r("findIndex");
}, {
"../internals/add-to-unscopables": 15,
"../internals/array-iteration": 28,
"../internals/export": 73
} ],
196: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/array-iteration").find, r = e("../internals/add-to-unscopables"), o = !0;
"find" in [] && Array(1).find(function() {
o = !1;
});
t({
target: "Array",
proto: !0,
forced: o
}, {
find: function(e) {
return n(this, e, arguments.length > 1 ? arguments[1] : void 0);
}
});
r("find");
}, {
"../internals/add-to-unscopables": 15,
"../internals/array-iteration": 28,
"../internals/export": 73
} ],
197: [ function(e) {
var t = e("../internals/export"), n = e("../internals/array-from");
t({
target: "Array",
stat: !0,
forced: !e("../internals/check-correctness-of-iteration")(function(e) {
Array.from(e);
})
}, {
from: n
});
}, {
"../internals/array-from": 26,
"../internals/check-correctness-of-iteration": 40,
"../internals/export": 73
} ],
198: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/array-includes").includes, r = e("../internals/fails"), o = e("../internals/add-to-unscopables");
t({
target: "Array",
proto: !0,
forced: r(function() {
return !Array(1).includes();
})
}, {
includes: function(e) {
return n(this, e, arguments.length > 1 ? arguments[1] : void 0);
}
});
o("includes");
}, {
"../internals/add-to-unscopables": 15,
"../internals/array-includes": 27,
"../internals/export": 73,
"../internals/fails": 74
} ],
199: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/function-uncurry-this"), r = e("../internals/array-includes").indexOf, o = e("../internals/array-method-is-strict"), i = n([].indexOf), s = !!i && 1 / i([ 1 ], 1, -0) < 0, a = o("indexOf");
t({
target: "Array",
proto: !0,
forced: s || !a
}, {
indexOf: function(e) {
var t = arguments.length > 1 ? arguments[1] : void 0;
return s ? i(this, e, t) || 0 : r(this, e, t);
}
});
}, {
"../internals/array-includes": 27,
"../internals/array-method-is-strict": 31,
"../internals/export": 73,
"../internals/function-uncurry-this": 81
} ],
200: [ function(e, t) {
"use strict";
var n = e("../internals/to-indexed-object"), r = e("../internals/add-to-unscopables"), o = e("../internals/iterators"), i = e("../internals/internal-state"), s = e("../internals/object-define-property").f, a = e("../internals/iterator-define"), c = e("../internals/create-iter-result-object"), l = e("../internals/is-pure"), u = e("../internals/descriptors"), p = i.set, f = i.getterFor("Array Iterator");
t.exports = a(Array, "Array", function(e, t) {
p(this, {
type: "Array Iterator",
target: n(e),
index: 0,
kind: t
});
}, function() {
var e = f(this), t = e.target, n = e.kind, r = e.index++;
if (!t || r >= t.length) {
e.target = void 0;
return c(void 0, !0);
}
return c("keys" == n ? r : "values" == n ? t[r] : [ r, t[r] ], !1);
}, "values");
var d = o.Arguments = o.Array;
r("keys");
r("values");
r("entries");
if (!l && u && "values" !== d.name) try {
s(d, "name", {
value: "values"
});
} catch (e) {}
}, {
"../internals/add-to-unscopables": 15,
"../internals/create-iter-result-object": 46,
"../internals/descriptors": 55,
"../internals/internal-state": 96,
"../internals/is-pure": 106,
"../internals/iterator-define": 112,
"../internals/iterators": 114,
"../internals/object-define-property": 125,
"../internals/to-indexed-object": 168
} ],
201: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/function-uncurry-this"), r = e("../internals/indexed-object"), o = e("../internals/to-indexed-object"), i = e("../internals/array-method-is-strict"), s = n([].join), a = r != Object, c = i("join", ",");
t({
target: "Array",
proto: !0,
forced: a || !c
}, {
join: function(e) {
return s(o(this), void 0 === e ? "," : e);
}
});
}, {
"../internals/array-method-is-strict": 31,
"../internals/export": 73,
"../internals/function-uncurry-this": 81,
"../internals/indexed-object": 93,
"../internals/to-indexed-object": 168
} ],
202: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/array-iteration").map;
t({
target: "Array",
proto: !0,
forced: !e("../internals/array-method-has-species-support")("map")
}, {
map: function(e) {
return n(this, e, arguments.length > 1 ? arguments[1] : void 0);
}
});
}, {
"../internals/array-iteration": 28,
"../internals/array-method-has-species-support": 30,
"../internals/export": 73
} ],
203: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/is-array"), r = e("../internals/is-constructor"), o = e("../internals/is-object"), i = e("../internals/to-absolute-index"), s = e("../internals/length-of-array-like"), a = e("../internals/to-indexed-object"), c = e("../internals/create-property"), l = e("../internals/well-known-symbol"), u = e("../internals/array-method-has-species-support"), p = e("../internals/array-slice"), f = u("slice"), d = l("species"), h = Array, y = Math.max;
t({
target: "Array",
proto: !0,
forced: !f
}, {
slice: function(e, t) {
var l, u, f, v = a(this), g = s(v), _ = i(e, g), m = i(void 0 === t ? g : t, g);
if (n(v)) {
l = v.constructor;
r(l) && (l === h || n(l.prototype)) ? l = void 0 : o(l) && null === (l = l[d]) && (l = void 0);
if (l === h || void 0 === l) return p(v, _, m);
}
u = new (void 0 === l ? h : l)(y(m - _, 0));
for (f = 0; _ < m; _++, f++) _ in v && c(u, f, v[_]);
u.length = f;
return u;
}
});
}, {
"../internals/array-method-has-species-support": 30,
"../internals/array-slice": 35,
"../internals/create-property": 49,
"../internals/export": 73,
"../internals/is-array": 98,
"../internals/is-constructor": 101,
"../internals/is-object": 105,
"../internals/length-of-array-like": 115,
"../internals/to-absolute-index": 165,
"../internals/to-indexed-object": 168,
"../internals/well-known-symbol": 189
} ],
204: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/to-object"), r = e("../internals/to-absolute-index"), o = e("../internals/to-integer-or-infinity"), i = e("../internals/length-of-array-like"), s = e("../internals/array-set-length"), a = e("../internals/does-not-exceed-safe-integer"), c = e("../internals/array-species-create"), l = e("../internals/create-property"), u = e("../internals/delete-property-or-throw"), p = e("../internals/array-method-has-species-support")("splice"), f = Math.max, d = Math.min;
t({
target: "Array",
proto: !0,
forced: !p
}, {
splice: function(e, t) {
var p, h, y, v, g, _, m = n(this), b = i(m), w = r(e, b), S = arguments.length;
if (0 === S) p = h = 0; else if (1 === S) {
p = 0;
h = b - w;
} else {
p = S - 2;
h = d(f(o(t), 0), b - w);
}
a(b + p - h);
y = c(m, h);
for (v = 0; v < h; v++) (g = w + v) in m && l(y, v, m[g]);
y.length = h;
if (p < h) {
for (v = w; v < b - h; v++) {
_ = v + p;
(g = v + h) in m ? m[_] = m[g] : u(m, _);
}
for (v = b; v > b - h + p; v--) u(m, v - 1);
} else if (p > h) for (v = b - h; v > w; v--) {
_ = v + p - 1;
(g = v + h - 1) in m ? m[_] = m[g] : u(m, _);
}
for (v = 0; v < p; v++) m[v + w] = arguments[v + 2];
s(m, b - h + p);
return y;
}
});
}, {
"../internals/array-method-has-species-support": 30,
"../internals/array-set-length": 33,
"../internals/array-species-create": 38,
"../internals/create-property": 49,
"../internals/delete-property-or-throw": 54,
"../internals/does-not-exceed-safe-integer": 58,
"../internals/export": 73,
"../internals/length-of-array-like": 115,
"../internals/to-absolute-index": 165,
"../internals/to-integer-or-infinity": 169,
"../internals/to-object": 171
} ],
205: [ function(e) {
var t = e("../internals/descriptors"), n = e("../internals/function-name").EXISTS, r = e("../internals/function-uncurry-this"), o = e("../internals/object-define-property").f, i = Function.prototype, s = r(i.toString), a = /function\b(?:\s|\/\*[\S\s]*?\*\/|\/\/[^\n\r]*[\n\r]+)*([^\s(/]*)/, c = r(a.exec);
t && !n && o(i, "name", {
configurable: !0,
get: function() {
try {
return c(a, s(this))[1];
} catch (e) {
return "";
}
}
});
}, {
"../internals/descriptors": 55,
"../internals/function-name": 80,
"../internals/function-uncurry-this": 81,
"../internals/object-define-property": 125
} ],
206: [ function(e) {
var t = e("../internals/export"), n = e("../internals/get-built-in"), r = e("../internals/function-apply"), o = e("../internals/function-call"), i = e("../internals/function-uncurry-this"), s = e("../internals/fails"), a = e("../internals/is-array"), c = e("../internals/is-callable"), l = e("../internals/is-object"), u = e("../internals/is-symbol"), p = e("../internals/array-slice"), f = e("../internals/symbol-constructor-detection"), d = n("JSON", "stringify"), h = i(/./.exec), y = i("".charAt), v = i("".charCodeAt), g = i("".replace), _ = i(1..toString), m = /[\uD800-\uDFFF]/g, b = /^[\uD800-\uDBFF]$/, w = /^[\uDC00-\uDFFF]$/, S = !f || s(function() {
var e = n("Symbol")();
return "[null]" != d([ e ]) || "{}" != d({
a: e
}) || "{}" != d(Object(e));
}), I = s(function() {
return '"\\udf06\\ud834"' !== d("\udf06\ud834") || '"\\udead"' !== d("\udead");
}), O = function(e, t) {
var n = p(arguments), i = t;
if ((l(t) || void 0 !== e) && !u(e)) {
a(t) || (t = function(e, t) {
c(i) && (t = o(i, this, e, t));
if (!u(t)) return t;
});
n[1] = t;
return r(d, null, n);
}
}, j = function(e, t, n) {
var r = y(n, t - 1), o = y(n, t + 1);
return h(b, e) && !h(w, o) || h(w, e) && !h(b, r) ? "\\u" + _(v(e, 0), 16) : e;
};
d && t({
target: "JSON",
stat: !0,
arity: 3,
forced: S || I
}, {
stringify: function(e, t, n) {
var o = p(arguments), i = r(S ? O : d, null, o);
return I && "string" == typeof i ? g(i, m, j) : i;
}
});
}, {
"../internals/array-slice": 35,
"../internals/export": 73,
"../internals/fails": 74,
"../internals/function-apply": 76,
"../internals/function-call": 79,
"../internals/function-uncurry-this": 81,
"../internals/get-built-in": 82,
"../internals/is-array": 98,
"../internals/is-callable": 100,
"../internals/is-object": 105,
"../internals/is-symbol": 108,
"../internals/symbol-constructor-detection": 162
} ],
207: [ function(e) {
"use strict";
var t = e("../internals/descriptors"), n = e("../internals/global"), r = e("../internals/function-uncurry-this"), o = e("../internals/is-forced"), i = e("../internals/define-built-in"), s = e("../internals/has-own-property"), a = e("../internals/inherit-if-required"), c = e("../internals/object-is-prototype-of"), l = e("../internals/is-symbol"), u = e("../internals/to-primitive"), p = e("../internals/fails"), f = e("../internals/object-get-own-property-names").f, d = e("../internals/object-get-own-property-descriptor").f, h = e("../internals/object-define-property").f, y = e("../internals/this-number-value"), v = e("../internals/string-trim").trim, g = n.Number, _ = g.prototype, m = n.TypeError, b = r("".slice), w = r("".charCodeAt), S = function(e) {
var t = u(e, "number");
return "bigint" == typeof t ? t : I(t);
}, I = function(e) {
var t, n, r, o, i, s, a, c, p = u(e, "number");
if (l(p)) throw m("Cannot convert a Symbol value to a number");
if ("string" == typeof p && p.length > 2) {
p = v(p);
if (43 === (t = w(p, 0)) || 45 === t) {
if (88 === (n = w(p, 2)) || 120 === n) return NaN;
} else if (48 === t) {
switch (w(p, 1)) {
case 66:
case 98:
r = 2;
o = 49;
break;

case 79:
case 111:
r = 8;
o = 55;
break;

default:
return +p;
}
s = (i = b(p, 2)).length;
for (a = 0; a < s; a++) if ((c = w(i, a)) < 48 || c > o) return NaN;
return parseInt(i, r);
}
}
return +p;
};
if (o("Number", !g(" 0o1") || !g("0b1") || g("+0x1"))) {
for (var O, j = function(e) {
var t = arguments.length < 1 ? 0 : g(S(e)), n = this;
return c(_, n) && p(function() {
y(n);
}) ? a(Object(t), n, j) : t;
}, P = t ? f(g) : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,isFinite,isInteger,isNaN,isSafeInteger,parseFloat,parseInt,fromString,range".split(","), C = 0; P.length > C; C++) s(g, O = P[C]) && !s(j, O) && h(j, O, d(g, O));
j.prototype = _;
_.constructor = j;
i(n, "Number", j, {
constructor: !0
});
}
}, {
"../internals/define-built-in": 51,
"../internals/descriptors": 55,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/global": 86,
"../internals/has-own-property": 87,
"../internals/inherit-if-required": 94,
"../internals/is-forced": 102,
"../internals/is-symbol": 108,
"../internals/object-define-property": 125,
"../internals/object-get-own-property-descriptor": 126,
"../internals/object-get-own-property-names": 127,
"../internals/object-is-prototype-of": 130,
"../internals/string-trim": 161,
"../internals/this-number-value": 164,
"../internals/to-primitive": 174
} ],
208: [ function(e) {
e("../internals/export")({
target: "Number",
stat: !0
}, {
isInteger: e("../internals/is-integral-number")
});
}, {
"../internals/export": 73,
"../internals/is-integral-number": 103
} ],
209: [ function(e) {
e("../internals/export")({
target: "Number",
stat: !0,
nonConfigurable: !0,
nonWritable: !0
}, {
MAX_SAFE_INTEGER: 9007199254740991
});
}, {
"../internals/export": 73
} ],
210: [ function(e) {
var t = e("../internals/export"), n = e("../internals/number-parse-int");
t({
target: "Number",
stat: !0,
forced: Number.parseInt != n
}, {
parseInt: n
});
}, {
"../internals/export": 73,
"../internals/number-parse-int": 121
} ],
211: [ function(e) {
var t = e("../internals/export"), n = e("../internals/object-assign");
t({
target: "Object",
stat: !0,
arity: 2,
forced: Object.assign !== n
}, {
assign: n
});
}, {
"../internals/export": 73,
"../internals/object-assign": 122
} ],
212: [ function(e) {
var t = e("../internals/export"), n = e("../internals/object-to-array").entries;
t({
target: "Object",
stat: !0
}, {
entries: function(e) {
return n(e);
}
});
}, {
"../internals/export": 73,
"../internals/object-to-array": 135
} ],
213: [ function(e) {
var t = e("../internals/export"), n = e("../internals/fails"), r = e("../internals/to-object"), o = e("../internals/object-get-prototype-of"), i = e("../internals/correct-prototype-getter");
t({
target: "Object",
stat: !0,
forced: n(function() {
o(1);
}),
sham: !i
}, {
getPrototypeOf: function(e) {
return o(r(e));
}
});
}, {
"../internals/correct-prototype-getter": 45,
"../internals/export": 73,
"../internals/fails": 74,
"../internals/object-get-prototype-of": 129,
"../internals/to-object": 171
} ],
214: [ function(e) {
var t = e("../internals/export"), n = e("../internals/to-object"), r = e("../internals/object-keys");
t({
target: "Object",
stat: !0,
forced: e("../internals/fails")(function() {
r(1);
})
}, {
keys: function(e) {
return r(n(e));
}
});
}, {
"../internals/export": 73,
"../internals/fails": 74,
"../internals/object-keys": 132,
"../internals/to-object": 171
} ],
215: [ function(e) {
var t = e("../internals/to-string-tag-support"), n = e("../internals/define-built-in"), r = e("../internals/object-to-string");
t || n(Object.prototype, "toString", r, {
unsafe: !0
});
}, {
"../internals/define-built-in": 51,
"../internals/object-to-string": 136,
"../internals/to-string-tag-support": 176
} ],
216: [ function(e) {
var t = e("../internals/export"), n = e("../internals/number-parse-int");
t({
global: !0,
forced: parseInt != n
}, {
parseInt: n
});
}, {
"../internals/export": 73,
"../internals/number-parse-int": 121
} ],
217: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/function-call"), r = e("../internals/a-callable"), o = e("../internals/new-promise-capability"), i = e("../internals/perform"), s = e("../internals/iterate");
t({
target: "Promise",
stat: !0,
forced: e("../internals/promise-statics-incorrect-iteration")
}, {
all: function(e) {
var t = this, a = o.f(t), c = a.resolve, l = a.reject, u = i(function() {
var o = r(t.resolve), i = [], a = 0, u = 1;
s(e, function(e) {
var r = a++, s = !1;
u++;
n(o, t, e).then(function(e) {
if (!s) {
s = !0;
i[r] = e;
--u || c(i);
}
}, l);
});
--u || c(i);
});
u.error && l(u.value);
return a.promise;
}
});
}, {
"../internals/a-callable": 12,
"../internals/export": 73,
"../internals/function-call": 79,
"../internals/iterate": 109,
"../internals/new-promise-capability": 119,
"../internals/perform": 139,
"../internals/promise-statics-incorrect-iteration": 143
} ],
218: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/is-pure"), r = e("../internals/promise-constructor-detection").CONSTRUCTOR, o = e("../internals/promise-native-constructor"), i = e("../internals/get-built-in"), s = e("../internals/is-callable"), a = e("../internals/define-built-in"), c = o && o.prototype;
t({
target: "Promise",
proto: !0,
forced: r,
real: !0
}, {
catch: function(e) {
return this.then(void 0, e);
}
});
if (!n && s(o)) {
var l = i("Promise").prototype.catch;
c.catch !== l && a(c, "catch", l, {
unsafe: !0
});
}
}, {
"../internals/define-built-in": 51,
"../internals/export": 73,
"../internals/get-built-in": 82,
"../internals/is-callable": 100,
"../internals/is-pure": 106,
"../internals/promise-constructor-detection": 140,
"../internals/promise-native-constructor": 141
} ],
219: [ function(e) {
"use strict";
var t, n, r, o = e("../internals/export"), i = e("../internals/is-pure"), s = e("../internals/engine-is-node"), a = e("../internals/global"), c = e("../internals/function-call"), l = e("../internals/define-built-in"), u = e("../internals/object-set-prototype-of"), p = e("../internals/set-to-string-tag"), f = e("../internals/set-species"), d = e("../internals/a-callable"), h = e("../internals/is-callable"), y = e("../internals/is-object"), v = e("../internals/an-instance"), g = e("../internals/species-constructor"), _ = e("../internals/task").set, m = e("../internals/microtask"), b = e("../internals/host-report-errors"), w = e("../internals/perform"), S = e("../internals/queue"), I = e("../internals/internal-state"), O = e("../internals/promise-native-constructor"), j = e("../internals/promise-constructor-detection"), P = e("../internals/new-promise-capability"), C = j.CONSTRUCTOR, T = j.REJECTION_EVENT, M = j.SUBCLASSING, x = I.getterFor("Promise"), k = I.set, R = O && O.prototype, U = O, A = R, E = a.TypeError, F = a.document, B = a.process, L = P.f, N = L, D = !!(F && F.createEvent && a.dispatchEvent), G = function(e) {
var t;
return !(!y(e) || !h(t = e.then)) && t;
}, W = function(e, t) {
var n, r, o, i = t.value, s = 1 == t.state, a = s ? e.ok : e.fail, l = e.resolve, u = e.reject, p = e.domain;
try {
if (a) {
if (!s) {
2 === t.rejection && q(t);
t.rejection = 1;
}
if (!0 === a) n = i; else {
p && p.enter();
n = a(i);
if (p) {
p.exit();
o = !0;
}
}
n === e.promise ? u(E("Promise-chain cycle")) : (r = G(n)) ? c(r, n, l, u) : l(n);
} else u(i);
} catch (e) {
p && !o && p.exit();
u(e);
}
}, H = function(e, t) {
if (!e.notified) {
e.notified = !0;
m(function() {
for (var n, r = e.reactions; n = r.get(); ) W(n, e);
e.notified = !1;
t && !e.rejection && z(e);
});
}
}, V = function(e, t, n) {
var r, o;
if (D) {
(r = F.createEvent("Event")).promise = t;
r.reason = n;
r.initEvent(e, !1, !0);
a.dispatchEvent(r);
} else r = {
promise: t,
reason: n
};
!T && (o = a["on" + e]) ? o(r) : "unhandledrejection" === e && b("Unhandled promise rejection", n);
}, z = function(e) {
c(_, a, function() {
var t, n = e.facade, r = e.value;
if (J(e)) {
t = w(function() {
s ? B.emit("unhandledRejection", r, n) : V("unhandledrejection", n, r);
});
e.rejection = s || J(e) ? 2 : 1;
if (t.error) throw t.value;
}
});
}, J = function(e) {
return 1 !== e.rejection && !e.parent;
}, q = function(e) {
c(_, a, function() {
var t = e.facade;
s ? B.emit("rejectionHandled", t) : V("rejectionhandled", t, e.value);
});
}, Y = function(e, t, n) {
return function(r) {
e(t, r, n);
};
}, Z = function(e, t, n) {
if (!e.done) {
e.done = !0;
n && (e = n);
e.value = t;
e.state = 2;
H(e, !0);
}
}, X = function(e, t, n) {
if (!e.done) {
e.done = !0;
n && (e = n);
try {
if (e.facade === t) throw E("Promise can't be resolved itself");
var r = G(t);
if (r) m(function() {
var n = {
done: !1
};
try {
c(r, t, Y(X, n, e), Y(Z, n, e));
} catch (t) {
Z(n, t, e);
}
}); else {
e.value = t;
e.state = 1;
H(e, !1);
}
} catch (t) {
Z({
done: !1
}, t, e);
}
}
};
if (C) {
A = (U = function(e) {
v(this, A);
d(e);
c(t, this);
var n = x(this);
try {
e(Y(X, n), Y(Z, n));
} catch (e) {
Z(n, e);
}
}).prototype;
(t = function() {
k(this, {
type: "Promise",
done: !1,
notified: !1,
parent: !1,
reactions: new S(),
rejection: !1,
state: 0,
value: void 0
});
}).prototype = l(A, "then", function(e, t) {
var n = x(this), r = L(g(this, U));
n.parent = !0;
r.ok = !h(e) || e;
r.fail = h(t) && t;
r.domain = s ? B.domain : void 0;
0 == n.state ? n.reactions.add(r) : m(function() {
W(r, n);
});
return r.promise;
});
n = function() {
var e = new t(), n = x(e);
this.promise = e;
this.resolve = Y(X, n);
this.reject = Y(Z, n);
};
P.f = L = function(e) {
return e === U || void 0 === e ? new n(e) : N(e);
};
if (!i && h(O) && R !== Object.prototype) {
r = R.then;
M || l(R, "then", function(e, t) {
var n = this;
return new U(function(e, t) {
c(r, n, e, t);
}).then(e, t);
}, {
unsafe: !0
});
try {
delete R.constructor;
} catch (e) {}
u && u(R, A);
}
}
o({
global: !0,
constructor: !0,
wrap: !0,
forced: C
}, {
Promise: U
});
p(U, "Promise", !1, !0);
f("Promise");
}, {
"../internals/a-callable": 12,
"../internals/an-instance": 17,
"../internals/define-built-in": 51,
"../internals/engine-is-node": 67,
"../internals/export": 73,
"../internals/function-call": 79,
"../internals/global": 86,
"../internals/host-report-errors": 89,
"../internals/internal-state": 96,
"../internals/is-callable": 100,
"../internals/is-object": 105,
"../internals/is-pure": 106,
"../internals/microtask": 118,
"../internals/new-promise-capability": 119,
"../internals/object-set-prototype-of": 134,
"../internals/perform": 139,
"../internals/promise-constructor-detection": 140,
"../internals/promise-native-constructor": 141,
"../internals/queue": 145,
"../internals/set-species": 154,
"../internals/set-to-string-tag": 155,
"../internals/species-constructor": 159,
"../internals/task": 163
} ],
220: [ function(e) {
e("../modules/es.promise.constructor");
e("../modules/es.promise.all");
e("../modules/es.promise.catch");
e("../modules/es.promise.race");
e("../modules/es.promise.reject");
e("../modules/es.promise.resolve");
}, {
"../modules/es.promise.all": 217,
"../modules/es.promise.catch": 218,
"../modules/es.promise.constructor": 219,
"../modules/es.promise.race": 221,
"../modules/es.promise.reject": 222,
"../modules/es.promise.resolve": 223
} ],
221: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/function-call"), r = e("../internals/a-callable"), o = e("../internals/new-promise-capability"), i = e("../internals/perform"), s = e("../internals/iterate");
t({
target: "Promise",
stat: !0,
forced: e("../internals/promise-statics-incorrect-iteration")
}, {
race: function(e) {
var t = this, a = o.f(t), c = a.reject, l = i(function() {
var o = r(t.resolve);
s(e, function(e) {
n(o, t, e).then(a.resolve, c);
});
});
l.error && c(l.value);
return a.promise;
}
});
}, {
"../internals/a-callable": 12,
"../internals/export": 73,
"../internals/function-call": 79,
"../internals/iterate": 109,
"../internals/new-promise-capability": 119,
"../internals/perform": 139,
"../internals/promise-statics-incorrect-iteration": 143
} ],
222: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/function-call"), r = e("../internals/new-promise-capability");
t({
target: "Promise",
stat: !0,
forced: e("../internals/promise-constructor-detection").CONSTRUCTOR
}, {
reject: function(e) {
var t = r.f(this);
n(t.reject, void 0, e);
return t.promise;
}
});
}, {
"../internals/export": 73,
"../internals/function-call": 79,
"../internals/new-promise-capability": 119,
"../internals/promise-constructor-detection": 140
} ],
223: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/get-built-in"), r = e("../internals/is-pure"), o = e("../internals/promise-native-constructor"), i = e("../internals/promise-constructor-detection").CONSTRUCTOR, s = e("../internals/promise-resolve"), a = n("Promise"), c = r && !i;
t({
target: "Promise",
stat: !0,
forced: r || i
}, {
resolve: function(e) {
return s(c && this === a ? o : this, e);
}
});
}, {
"../internals/export": 73,
"../internals/get-built-in": 82,
"../internals/is-pure": 106,
"../internals/promise-constructor-detection": 140,
"../internals/promise-native-constructor": 141,
"../internals/promise-resolve": 142
} ],
224: [ function(e) {
var t = e("../internals/descriptors"), n = e("../internals/global"), r = e("../internals/function-uncurry-this"), o = e("../internals/is-forced"), i = e("../internals/inherit-if-required"), s = e("../internals/create-non-enumerable-property"), a = e("../internals/object-get-own-property-names").f, c = e("../internals/object-is-prototype-of"), l = e("../internals/is-regexp"), u = e("../internals/to-string"), p = e("../internals/regexp-get-flags"), f = e("../internals/regexp-sticky-helpers"), d = e("../internals/proxy-accessor"), h = e("../internals/define-built-in"), y = e("../internals/fails"), v = e("../internals/has-own-property"), g = e("../internals/internal-state").enforce, _ = e("../internals/set-species"), m = e("../internals/well-known-symbol"), b = e("../internals/regexp-unsupported-dot-all"), w = e("../internals/regexp-unsupported-ncg"), S = m("match"), I = n.RegExp, O = I.prototype, j = n.SyntaxError, P = r(O.exec), C = r("".charAt), T = r("".replace), M = r("".indexOf), x = r("".slice), k = /^\?<[^\s\d!#%&*+<=>@^][^\s!#%&*+<=>@^]*>/, R = /a/g, U = /a/g, A = new I(R) !== R, E = f.MISSED_STICKY, F = f.UNSUPPORTED_Y, B = t && (!A || E || b || w || y(function() {
U[S] = !1;
return I(R) != R || I(U) == U || "/a/i" != I(R, "i");
})), L = function(e) {
for (var t, n = e.length, r = 0, o = "", i = !1; r <= n; r++) if ("\\" !== (t = C(e, r))) if (i || "." !== t) {
"[" === t ? i = !0 : "]" === t && (i = !1);
o += t;
} else o += "[\\s\\S]"; else o += t + C(e, ++r);
return o;
}, N = function(e) {
for (var t, n = e.length, r = 0, o = "", i = [], s = {}, a = !1, c = !1, l = 0, u = ""; r <= n; r++) {
if ("\\" === (t = C(e, r))) t += C(e, ++r); else if ("]" === t) a = !1; else if (!a) switch (!0) {
case "[" === t:
a = !0;
break;

case "(" === t:
if (P(k, x(e, r + 1))) {
r += 2;
c = !0;
}
o += t;
l++;
continue;

case ">" === t && c:
if ("" === u || v(s, u)) throw new j("Invalid capture group name");
s[u] = !0;
i[i.length] = [ u, l ];
c = !1;
u = "";
continue;
}
c ? u += t : o += t;
}
return [ o, i ];
};
if (o("RegExp", B)) {
for (var D = function(e, t) {
var n, r, o, a, f, d, h = c(O, this), y = l(e), v = void 0 === t, _ = [], m = e;
if (!h && y && v && e.constructor === D) return e;
if (y || c(O, e)) {
e = e.source;
v && (t = p(m));
}
e = void 0 === e ? "" : u(e);
t = void 0 === t ? "" : u(t);
m = e;
b && "dotAll" in R && (r = !!t && M(t, "s") > -1) && (t = T(t, /s/g, ""));
n = t;
E && "sticky" in R && (o = !!t && M(t, "y") > -1) && F && (t = T(t, /y/g, ""));
if (w) {
e = (a = N(e))[0];
_ = a[1];
}
f = i(I(e, t), h ? this : O, D);
if (r || o || _.length) {
d = g(f);
if (r) {
d.dotAll = !0;
d.raw = D(L(e), n);
}
o && (d.sticky = !0);
_.length && (d.groups = _);
}
if (e !== m) try {
s(f, "source", "" === m ? "(?:)" : m);
} catch (e) {}
return f;
}, G = a(I), W = 0; G.length > W; ) d(D, I, G[W++]);
O.constructor = D;
D.prototype = O;
h(n, "RegExp", D, {
constructor: !0
});
}
_("RegExp");
}, {
"../internals/create-non-enumerable-property": 47,
"../internals/define-built-in": 51,
"../internals/descriptors": 55,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/global": 86,
"../internals/has-own-property": 87,
"../internals/inherit-if-required": 94,
"../internals/internal-state": 96,
"../internals/is-forced": 102,
"../internals/is-regexp": 107,
"../internals/object-get-own-property-names": 127,
"../internals/object-is-prototype-of": 130,
"../internals/proxy-accessor": 144,
"../internals/regexp-get-flags": 149,
"../internals/regexp-sticky-helpers": 150,
"../internals/regexp-unsupported-dot-all": 151,
"../internals/regexp-unsupported-ncg": 152,
"../internals/set-species": 154,
"../internals/to-string": 177,
"../internals/well-known-symbol": 189
} ],
225: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/regexp-exec");
t({
target: "RegExp",
proto: !0,
forced: /./.exec !== n
}, {
exec: n
});
}, {
"../internals/export": 73,
"../internals/regexp-exec": 147
} ],
226: [ function(e) {
var t = e("../internals/descriptors"), n = e("../internals/regexp-sticky-helpers").MISSED_STICKY, r = e("../internals/classof-raw"), o = e("../internals/define-built-in-accessor"), i = e("../internals/internal-state").get, s = RegExp.prototype, a = TypeError;
t && n && o(s, "sticky", {
configurable: !0,
get: function() {
if (this !== s) {
if ("RegExp" === r(this)) return !!i(this).sticky;
throw a("Incompatible receiver, RegExp required");
}
}
});
}, {
"../internals/classof-raw": 41,
"../internals/define-built-in-accessor": 50,
"../internals/descriptors": 55,
"../internals/internal-state": 96,
"../internals/regexp-sticky-helpers": 150
} ],
227: [ function(e) {
"use strict";
e("../modules/es.regexp.exec");
var t = e("../internals/export"), n = e("../internals/function-call"), r = e("../internals/is-callable"), o = e("../internals/an-object"), i = e("../internals/to-string"), s = function() {
var e = !1, t = /[ac]/;
t.exec = function() {
e = !0;
return /./.exec.apply(this, arguments);
};
return !0 === t.test("abc") && e;
}(), a = /./.test;
t({
target: "RegExp",
proto: !0,
forced: !s
}, {
test: function(e) {
var t = o(this), s = i(e), c = t.exec;
if (!r(c)) return n(a, t, s);
var l = n(c, t, s);
if (null === l) return !1;
o(l);
return !0;
}
});
}, {
"../internals/an-object": 18,
"../internals/export": 73,
"../internals/function-call": 79,
"../internals/is-callable": 100,
"../internals/to-string": 177,
"../modules/es.regexp.exec": 225
} ],
228: [ function(e) {
"use strict";
var t = e("../internals/function-name").PROPER, n = e("../internals/define-built-in"), r = e("../internals/an-object"), o = e("../internals/to-string"), i = e("../internals/fails"), s = e("../internals/regexp-get-flags"), a = RegExp.prototype.toString, c = i(function() {
return "/a/b" != a.call({
source: "a",
flags: "b"
});
}), l = t && "toString" != a.name;
(c || l) && n(RegExp.prototype, "toString", function() {
var e = r(this);
return "/" + o(e.source) + "/" + o(s(e));
}, {
unsafe: !0
});
}, {
"../internals/an-object": 18,
"../internals/define-built-in": 51,
"../internals/fails": 74,
"../internals/function-name": 80,
"../internals/regexp-get-flags": 149,
"../internals/to-string": 177
} ],
229: [ function(e) {
"use strict";
var t, n = e("../internals/export"), r = e("../internals/function-uncurry-this"), o = e("../internals/object-get-own-property-descriptor").f, i = e("../internals/to-length"), s = e("../internals/to-string"), a = e("../internals/not-a-regexp"), c = e("../internals/require-object-coercible"), l = e("../internals/correct-is-regexp-logic"), u = e("../internals/is-pure"), p = r("".endsWith), f = r("".slice), d = Math.min, h = l("endsWith");
n({
target: "String",
proto: !0,
forced: !(!u && !h && (t = o(String.prototype, "endsWith"), t && !t.writable) || h)
}, {
endsWith: function(e) {
var t = s(c(this));
a(e);
var n = arguments.length > 1 ? arguments[1] : void 0, r = t.length, o = void 0 === n ? r : d(i(n), r), l = s(e);
return p ? p(t, l, o) : f(t, o - l.length, o) === l;
}
});
}, {
"../internals/correct-is-regexp-logic": 44,
"../internals/export": 73,
"../internals/function-uncurry-this": 81,
"../internals/is-pure": 106,
"../internals/not-a-regexp": 120,
"../internals/object-get-own-property-descriptor": 126,
"../internals/require-object-coercible": 153,
"../internals/to-length": 170,
"../internals/to-string": 177
} ],
230: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/function-uncurry-this"), r = e("../internals/not-a-regexp"), o = e("../internals/require-object-coercible"), i = e("../internals/to-string"), s = e("../internals/correct-is-regexp-logic"), a = n("".indexOf);
t({
target: "String",
proto: !0,
forced: !s("includes")
}, {
includes: function(e) {
return !!~a(i(o(this)), i(r(e)), arguments.length > 1 ? arguments[1] : void 0);
}
});
}, {
"../internals/correct-is-regexp-logic": 44,
"../internals/export": 73,
"../internals/function-uncurry-this": 81,
"../internals/not-a-regexp": 120,
"../internals/require-object-coercible": 153,
"../internals/to-string": 177
} ],
231: [ function(e) {
"use strict";
var t = e("../internals/string-multibyte").charAt, n = e("../internals/to-string"), r = e("../internals/internal-state"), o = e("../internals/iterator-define"), i = e("../internals/create-iter-result-object"), s = r.set, a = r.getterFor("String Iterator");
o(String, "String", function(e) {
s(this, {
type: "String Iterator",
string: n(e),
index: 0
});
}, function() {
var e, n = a(this), r = n.string, o = n.index;
if (o >= r.length) return i(void 0, !0);
e = t(r, o);
n.index += e.length;
return i(e, !1);
});
}, {
"../internals/create-iter-result-object": 46,
"../internals/internal-state": 96,
"../internals/iterator-define": 112,
"../internals/string-multibyte": 160,
"../internals/to-string": 177
} ],
232: [ function(e) {
"use strict";
var t = e("../internals/function-call"), n = e("../internals/fix-regexp-well-known-symbol-logic"), r = e("../internals/an-object"), o = e("../internals/is-null-or-undefined"), i = e("../internals/to-length"), s = e("../internals/to-string"), a = e("../internals/require-object-coercible"), c = e("../internals/get-method"), l = e("../internals/advance-string-index"), u = e("../internals/regexp-exec-abstract");
n("match", function(e, n, p) {
return [ function(n) {
var r = a(this), i = o(n) ? void 0 : c(n, e);
return i ? t(i, n, r) : new RegExp(n)[e](s(r));
}, function(e) {
var t = r(this), o = s(e), a = p(n, t, o);
if (a.done) return a.value;
if (!t.global) return u(t, o);
var c = t.unicode;
t.lastIndex = 0;
for (var f, d = [], h = 0; null !== (f = u(t, o)); ) {
var y = s(f[0]);
d[h] = y;
"" === y && (t.lastIndex = l(o, i(t.lastIndex), c));
h++;
}
return 0 === h ? null : d;
} ];
});
}, {
"../internals/advance-string-index": 16,
"../internals/an-object": 18,
"../internals/fix-regexp-well-known-symbol-logic": 75,
"../internals/function-call": 79,
"../internals/get-method": 85,
"../internals/is-null-or-undefined": 104,
"../internals/regexp-exec-abstract": 146,
"../internals/require-object-coercible": 153,
"../internals/to-length": 170,
"../internals/to-string": 177
} ],
233: [ function(e) {
"use strict";
var t, n = e("../internals/export"), r = e("../internals/function-uncurry-this"), o = e("../internals/object-get-own-property-descriptor").f, i = e("../internals/to-length"), s = e("../internals/to-string"), a = e("../internals/not-a-regexp"), c = e("../internals/require-object-coercible"), l = e("../internals/correct-is-regexp-logic"), u = e("../internals/is-pure"), p = r("".startsWith), f = r("".slice), d = Math.min, h = l("startsWith");
n({
target: "String",
proto: !0,
forced: !(!u && !h && (t = o(String.prototype, "startsWith"), t && !t.writable) || h)
}, {
startsWith: function(e) {
var t = s(c(this));
a(e);
var n = i(d(arguments.length > 1 ? arguments[1] : void 0, t.length)), r = s(e);
return p ? p(t, r, n) : f(t, n, n + r.length) === r;
}
});
}, {
"../internals/correct-is-regexp-logic": 44,
"../internals/export": 73,
"../internals/function-uncurry-this": 81,
"../internals/is-pure": 106,
"../internals/not-a-regexp": 120,
"../internals/object-get-own-property-descriptor": 126,
"../internals/require-object-coercible": 153,
"../internals/to-length": 170,
"../internals/to-string": 177
} ],
234: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/length-of-array-like"), r = e("../internals/to-integer-or-infinity"), o = t.aTypedArray;
(0, t.exportTypedArrayMethod)("at", function(e) {
var t = o(this), i = n(t), s = r(e), a = s >= 0 ? s : i + s;
return a < 0 || a >= i ? void 0 : t[a];
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/length-of-array-like": 115,
"../internals/to-integer-or-infinity": 169
} ],
235: [ function(e) {
"use strict";
var t = e("../internals/function-uncurry-this"), n = e("../internals/array-buffer-view-core"), r = t(e("../internals/array-copy-within")), o = n.aTypedArray;
(0, n.exportTypedArrayMethod)("copyWithin", function(e, t) {
return r(o(this), e, t, arguments.length > 2 ? arguments[2] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-copy-within": 22,
"../internals/function-uncurry-this": 81
} ],
236: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-iteration").every, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("every", function(e) {
return n(r(this), e, arguments.length > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-iteration": 28
} ],
237: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-fill"), r = e("../internals/to-big-int"), o = e("../internals/classof"), i = e("../internals/function-call"), s = e("../internals/function-uncurry-this"), a = e("../internals/fails"), c = t.aTypedArray, l = t.exportTypedArrayMethod, u = s("".slice);
l("fill", function(e) {
var t = arguments.length;
c(this);
var s = "Big" === u(o(this), 0, 3) ? r(e) : +e;
return i(n, this, s, t > 1 ? arguments[1] : void 0, t > 2 ? arguments[2] : void 0);
}, a(function() {
var e = 0;
new Int8Array(2).fill({
valueOf: function() {
return e++;
}
});
return 1 !== e;
}));
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-fill": 23,
"../internals/classof": 42,
"../internals/fails": 74,
"../internals/function-call": 79,
"../internals/function-uncurry-this": 81,
"../internals/to-big-int": 166
} ],
238: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-iteration").filter, r = e("../internals/typed-array-from-species-and-list"), o = t.aTypedArray;
(0, t.exportTypedArrayMethod)("filter", function(e) {
var t = n(o(this), e, arguments.length > 1 ? arguments[1] : void 0);
return r(this, t);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-iteration": 28,
"../internals/typed-array-from-species-and-list": 181
} ],
239: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-iteration").findIndex, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("findIndex", function(e) {
return n(r(this), e, arguments.length > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-iteration": 28
} ],
240: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-iteration").find, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("find", function(e) {
return n(r(this), e, arguments.length > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-iteration": 28
} ],
241: [ function(e) {
e("../internals/typed-array-constructor")("Float32", function(e) {
return function(t, n, r) {
return e(this, t, n, r);
};
});
}, {
"../internals/typed-array-constructor": 179
} ],
242: [ function(e) {
e("../internals/typed-array-constructor")("Float64", function(e) {
return function(t, n, r) {
return e(this, t, n, r);
};
});
}, {
"../internals/typed-array-constructor": 179
} ],
243: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-iteration").forEach, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("forEach", function(e) {
n(r(this), e, arguments.length > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-iteration": 28
} ],
244: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-includes").includes, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("includes", function(e) {
return n(r(this), e, arguments.length > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-includes": 27
} ],
245: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-includes").indexOf, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("indexOf", function(e) {
return n(r(this), e, arguments.length > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-includes": 27
} ],
246: [ function(e) {
e("../internals/typed-array-constructor")("Int16", function(e) {
return function(t, n, r) {
return e(this, t, n, r);
};
});
}, {
"../internals/typed-array-constructor": 179
} ],
247: [ function(e) {
e("../internals/typed-array-constructor")("Int32", function(e) {
return function(t, n, r) {
return e(this, t, n, r);
};
});
}, {
"../internals/typed-array-constructor": 179
} ],
248: [ function(e) {
e("../internals/typed-array-constructor")("Int8", function(e) {
return function(t, n, r) {
return e(this, t, n, r);
};
});
}, {
"../internals/typed-array-constructor": 179
} ],
249: [ function(e) {
"use strict";
var t = e("../internals/global"), n = e("../internals/fails"), r = e("../internals/function-uncurry-this"), o = e("../internals/array-buffer-view-core"), i = e("../modules/es.array.iterator"), s = e("../internals/well-known-symbol")("iterator"), a = t.Uint8Array, c = r(i.values), l = r(i.keys), u = r(i.entries), p = o.aTypedArray, f = o.exportTypedArrayMethod, d = a && a.prototype, h = !n(function() {
d[s].call([ 1 ]);
}), y = !!d && d.values && d[s] === d.values && "values" === d.values.name, v = function() {
return c(p(this));
};
f("entries", function() {
return u(p(this));
}, h);
f("keys", function() {
return l(p(this));
}, h);
f("values", v, h || !y, {
name: "values"
});
f(s, v, h || !y, {
name: "values"
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/global": 86,
"../internals/well-known-symbol": 189,
"../modules/es.array.iterator": 200
} ],
250: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/function-uncurry-this"), r = t.aTypedArray, o = t.exportTypedArrayMethod, i = n([].join);
o("join", function(e) {
return i(r(this), e);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/function-uncurry-this": 81
} ],
251: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/function-apply"), r = e("../internals/array-last-index-of"), o = t.aTypedArray;
(0, t.exportTypedArrayMethod)("lastIndexOf", function(e) {
var t = arguments.length;
return n(r, o(this), t > 1 ? [ e, arguments[1] ] : [ e ]);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-last-index-of": 29,
"../internals/function-apply": 76
} ],
252: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-iteration").map, r = e("../internals/typed-array-species-constructor"), o = t.aTypedArray;
(0, t.exportTypedArrayMethod)("map", function(e) {
return n(o(this), e, arguments.length > 1 ? arguments[1] : void 0, function(e, t) {
return new (r(e))(t);
});
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-iteration": 28,
"../internals/typed-array-species-constructor": 183
} ],
253: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-reduce").right, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("reduceRight", function(e) {
var t = arguments.length;
return n(r(this), e, t, t > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-reduce": 32
} ],
254: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-reduce").left, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("reduce", function(e) {
var t = arguments.length;
return n(r(this), e, t, t > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-reduce": 32
} ],
255: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = t.aTypedArray, r = t.exportTypedArrayMethod, o = Math.floor;
r("reverse", function() {
for (var e, t = n(this).length, r = o(t / 2), i = 0; i < r; ) {
e = this[i];
this[i++] = this[--t];
this[t] = e;
}
return this;
});
}, {
"../internals/array-buffer-view-core": 20
} ],
256: [ function(e) {
"use strict";
var t = e("../internals/global"), n = e("../internals/function-call"), r = e("../internals/array-buffer-view-core"), o = e("../internals/length-of-array-like"), i = e("../internals/to-offset"), s = e("../internals/to-object"), a = e("../internals/fails"), c = t.RangeError, l = t.Int8Array, u = l && l.prototype, p = u && u.set, f = r.aTypedArray, d = r.exportTypedArrayMethod, h = !a(function() {
var e = new Uint8ClampedArray(2);
n(p, e, {
length: 1,
0: 3
}, 1);
return 3 !== e[1];
}), y = h && r.NATIVE_ARRAY_BUFFER_VIEWS && a(function() {
var e = new l(2);
e.set(1);
e.set("2", 1);
return 0 !== e[0] || 2 !== e[1];
});
d("set", function(e) {
f(this);
var t = i(arguments.length > 1 ? arguments[1] : void 0, 1), r = s(e);
if (h) return n(p, this, r, t);
var a = this.length, l = o(r), u = 0;
if (l + t > a) throw c("Wrong length");
for (;u < l; ) this[t + u] = r[u++];
}, !h || y);
}, {
"../internals/array-buffer-view-core": 20,
"../internals/fails": 74,
"../internals/function-call": 79,
"../internals/global": 86,
"../internals/length-of-array-like": 115,
"../internals/to-object": 171,
"../internals/to-offset": 172
} ],
257: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/typed-array-species-constructor"), r = e("../internals/fails"), o = e("../internals/array-slice"), i = t.aTypedArray;
(0, t.exportTypedArrayMethod)("slice", function(e, t) {
for (var r = o(i(this), e, t), s = n(this), a = 0, c = r.length, l = new s(c); c > a; ) l[a] = r[a++];
return l;
}, r(function() {
new Int8Array(1).slice();
}));
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-slice": 35,
"../internals/fails": 74,
"../internals/typed-array-species-constructor": 183
} ],
258: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/array-iteration").some, r = t.aTypedArray;
(0, t.exportTypedArrayMethod)("some", function(e) {
return n(r(this), e, arguments.length > 1 ? arguments[1] : void 0);
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-iteration": 28
} ],
259: [ function(e) {
"use strict";
var t = e("../internals/global"), n = e("../internals/function-uncurry-this"), r = e("../internals/fails"), o = e("../internals/a-callable"), i = e("../internals/array-sort"), s = e("../internals/array-buffer-view-core"), a = e("../internals/engine-ff-version"), c = e("../internals/engine-is-ie-or-edge"), l = e("../internals/engine-v8-version"), u = e("../internals/engine-webkit-version"), p = s.aTypedArray, f = s.exportTypedArrayMethod, d = t.Uint16Array, h = d && n(d.prototype.sort), y = !(!h || r(function() {
h(new d(2), null);
}) && r(function() {
h(new d(2), {});
})), v = !!h && !r(function() {
if (l) return l < 74;
if (a) return a < 67;
if (c) return !0;
if (u) return u < 602;
var e, t, n = new d(516), r = Array(516);
for (e = 0; e < 516; e++) {
t = e % 4;
n[e] = 515 - e;
r[e] = e - 2 * t + 3;
}
h(n, function(e, t) {
return (e / 4 | 0) - (t / 4 | 0);
});
for (e = 0; e < 516; e++) if (n[e] !== r[e]) return !0;
}), g = function(e) {
return function(t, n) {
return void 0 !== e ? +e(t, n) || 0 : n != n ? -1 : t != t ? 1 : 0 === t && 0 === n ? 1 / t > 0 && 1 / n < 0 ? 1 : -1 : t > n;
};
};
f("sort", function(e) {
void 0 !== e && o(e);
return v ? h(this, e) : i(p(this), g(e));
}, !v || y);
}, {
"../internals/a-callable": 12,
"../internals/array-buffer-view-core": 20,
"../internals/array-sort": 36,
"../internals/engine-ff-version": 61,
"../internals/engine-is-ie-or-edge": 64,
"../internals/engine-v8-version": 70,
"../internals/engine-webkit-version": 71,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/global": 86
} ],
260: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core"), n = e("../internals/to-length"), r = e("../internals/to-absolute-index"), o = e("../internals/typed-array-species-constructor"), i = t.aTypedArray;
(0, t.exportTypedArrayMethod)("subarray", function(e, t) {
var s = i(this), a = s.length, c = r(e, a);
return new (o(s))(s.buffer, s.byteOffset + c * s.BYTES_PER_ELEMENT, n((void 0 === t ? a : r(t, a)) - c));
});
}, {
"../internals/array-buffer-view-core": 20,
"../internals/to-absolute-index": 165,
"../internals/to-length": 170,
"../internals/typed-array-species-constructor": 183
} ],
261: [ function(e) {
"use strict";
var t = e("../internals/global"), n = e("../internals/function-apply"), r = e("../internals/array-buffer-view-core"), o = e("../internals/fails"), i = e("../internals/array-slice"), s = t.Int8Array, a = r.aTypedArray, c = r.exportTypedArrayMethod, l = [].toLocaleString, u = !!s && o(function() {
l.call(new s(1));
});
c("toLocaleString", function() {
return n(l, u ? i(a(this)) : a(this), i(arguments));
}, o(function() {
return [ 1, 2 ].toLocaleString() != new s([ 1, 2 ]).toLocaleString();
}) || !o(function() {
s.prototype.toLocaleString.call([ 1, 2 ]);
}));
}, {
"../internals/array-buffer-view-core": 20,
"../internals/array-slice": 35,
"../internals/fails": 74,
"../internals/function-apply": 76,
"../internals/global": 86
} ],
262: [ function(e) {
"use strict";
var t = e("../internals/array-buffer-view-core").exportTypedArrayMethod, n = e("../internals/fails"), r = e("../internals/global"), o = e("../internals/function-uncurry-this"), i = r.Uint8Array, s = i && i.prototype || {}, a = [].toString, c = o([].join);
n(function() {
a.call({});
}) && (a = function() {
return c(this);
});
var l = s.toString != a;
t("toString", a, l);
}, {
"../internals/array-buffer-view-core": 20,
"../internals/fails": 74,
"../internals/function-uncurry-this": 81,
"../internals/global": 86
} ],
263: [ function(e) {
e("../internals/typed-array-constructor")("Uint16", function(e) {
return function(t, n, r) {
return e(this, t, n, r);
};
});
}, {
"../internals/typed-array-constructor": 179
} ],
264: [ function(e) {
e("../internals/typed-array-constructor")("Uint32", function(e) {
return function(t, n, r) {
return e(this, t, n, r);
};
});
}, {
"../internals/typed-array-constructor": 179
} ],
265: [ function(e) {
e("../internals/typed-array-constructor")("Uint8", function(e) {
return function(t, n, r) {
return e(this, t, n, r);
};
});
}, {
"../internals/typed-array-constructor": 179
} ],
266: [ function(e) {
e("../modules/es.typed-array.at");
}, {
"../modules/es.typed-array.at": 234
} ],
267: [ function(e) {
var t = e("../internals/global"), n = e("../internals/dom-iterables"), r = e("../internals/dom-token-list-prototype"), o = e("../internals/array-for-each"), i = e("../internals/create-non-enumerable-property"), s = function(e) {
if (e && e.forEach !== o) try {
i(e, "forEach", o);
} catch (t) {
e.forEach = o;
}
};
for (var a in n) n[a] && s(t[a] && t[a].prototype);
s(r);
}, {
"../internals/array-for-each": 24,
"../internals/create-non-enumerable-property": 47,
"../internals/dom-iterables": 59,
"../internals/dom-token-list-prototype": 60,
"../internals/global": 86
} ],
268: [ function(e) {
var t = e("../internals/global"), n = e("../internals/dom-iterables"), r = e("../internals/dom-token-list-prototype"), o = e("../modules/es.array.iterator"), i = e("../internals/create-non-enumerable-property"), s = e("../internals/well-known-symbol"), a = s("iterator"), c = s("toStringTag"), l = o.values, u = function(e, t) {
if (e) {
if (e[a] !== l) try {
i(e, a, l);
} catch (t) {
e[a] = l;
}
e[c] || i(e, c, t);
if (n[t]) for (var r in o) if (e[r] !== o[r]) try {
i(e, r, o[r]);
} catch (t) {
e[r] = o[r];
}
}
};
for (var p in n) u(t[p] && t[p].prototype, p);
u(r, "DOMTokenList");
}, {
"../internals/create-non-enumerable-property": 47,
"../internals/dom-iterables": 59,
"../internals/dom-token-list-prototype": 60,
"../internals/global": 86,
"../internals/well-known-symbol": 189,
"../modules/es.array.iterator": 200
} ],
269: [ function(e) {
"use strict";
var t = e("../internals/export"), n = e("../internals/function-call");
t({
target: "URL",
proto: !0,
enumerable: !0
}, {
toJSON: function() {
return n(URL.prototype.toString, this);
}
});
}, {
"../internals/export": 73,
"../internals/function-call": 79
} ],
270: [ function(e) {
"use strict";
e("k8w-linq-array");
e("k8w-super-date");
e("k8w-super-object");
if ("undefined" != typeof window && !window.console) {
window.console = {};
console.log = console.debug = console.info = console.warn = console.error = console.time = console.timeEnd = function() {};
}
console.debug || (console.debug = console.log);
}, {
"k8w-linq-array": 271,
"k8w-super-date": 272,
"k8w-super-object": 273
} ],
271: [ function() {
"use strict";
var e, t = {
remove: function(e) {
if ("function" == typeof e) for (var t = this.length - 1; t > -1; --t) e(this[t], t, this) && this.splice(t, 1); else for (t = this.length - 1; t > -1; --t) this[t] === e && this.splice(t, 1);
return this;
},
removeOne: function(e) {
if ("function" == typeof e) {
for (var t = 0; t < this.length; ++t) if (e(this[t], t, this)) {
this.splice(t, 1);
return this;
}
} else for (t = 0; t < this.length; ++t) if (this[t] === e) {
this.splice(t, 1);
return this;
}
return this;
},
first: function() {
return this.length ? this[0] : null;
},
last: function() {
return this.length ? this[this.length - 1] : null;
},
max: function(e) {
if (!this.length) return null;
if ("function" == typeof e) {
for (var t = e(this[0], 0, this), n = 1; n < this.length; ++n) {
var r = e(this[n], n, this);
t = r > t ? r : t;
}
return t;
}
return this.reduce(function(e, t) {
return e > t ? e : t;
});
},
min: function(e) {
if (!this.length) return null;
if ("function" == typeof e) {
for (var t = e(this[0], 0, this), n = 1; n < this.length; ++n) {
var r = e(this[n], n, this);
t = r < t ? r : t;
}
return t;
}
return this.reduce(function(e, t) {
return (n = e) < (r = t) ? n : r;
var n, r;
});
},
distinct: function() {
return this.filter(function(e, t, n) {
return n.indexOf(e) === t;
});
},
filterIndex: function(e) {
for (var t = [], n = 0; n < this.length; ++n) e(this[n], n, this) && t.push(n);
return t;
},
count: function(e) {
for (var t = 0, n = 0; n < this.length; ++n) e(this[n], n, this) && ++t;
return t;
},
sum: function(e) {
for (var t = 0, n = 0; n < this.length; ++n) t += e ? e(this[n], n, this) : this[n];
return t;
},
average: function(e) {
return this.sum(e) / this.length;
},
orderBy: function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
return this.slice().sort(function(t, n) {
for (var r = 0; r < e.length; ++r) {
var o = e[r](t), i = e[r](n);
if (o > i) return 1;
if (o < i) return -1;
}
return 0;
});
},
orderByDesc: function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
return this.slice().sort(function(t, n) {
for (var r = 0; r < e.length; ++r) {
var o = e[r](t), i = e[r](n);
if (o > i) return -1;
if (o < i) return 1;
}
return 0;
});
},
binarySearch: function(e, t) {
for (var n = 0, r = this.length - 1; n <= r; ) {
var o = (r + n) / 2 | 0, i = t ? t(this[o]) : this[o];
if (e === i) return o;
e > i ? n = o + 1 : e < i && (r = o - 1);
}
return -1;
},
binaryInsert: function(e, t, n) {
if ("boolean" == typeof t) {
n = t;
t = void 0;
}
for (var r = 0, o = this.length - 1, i = NaN, s = t ? t(e) : e; r <= o; ) {
i = (o + r) / 2 | 0;
var a = t ? t(this[i]) : this[i];
if (s === a) {
if (n) return i;
break;
}
s > a ? r = i + 1 : s < a && (o = i - 1);
}
var c = r > i ? i + 1 : i;
this.splice(c, 0, e);
return c;
},
binaryDistinct: function(e) {
return this.filter(function(t, n, r) {
return r.binarySearch(t, e) === n;
});
},
findLast: function(e) {
for (var t = this.length - 1; t > -1; --t) if (e(this[t], t, this)) return this[t];
},
findLastIndex: function(e) {
for (var t = this.length - 1; t > -1; --t) if (e(this[t], t, this)) return t;
return -1;
},
groupBy: function(e) {
var t = this.reduce(function(t, n) {
var r = e(n);
t[r] || (t[r] = []);
t[r].push(n);
return t;
}, {});
return Object.keys(t).map(function(e) {
var n = t[e];
n.key = e;
return n;
});
},
__k8w_extended: {
value: !0
}
};
if (!Array.prototype.__k8w_extended) for (var n in t) Object.defineProperties(Array.prototype, ((e = {})[n] = {
value: t[n],
writable: !0
}, e));
}, {} ],
272: [ function() {
"use strict";
function e(e, t) {
return e.length > 1 && t < 10 ? "0" + t : "" + t;
}
Date.prototype.format = function(t) {
var n = this;
void 0 === t && (t = "YYYY-MM-DD hh:mm:ss");
return t.replace(/y{2,}|Y{2,}/, function(e) {
return (n.getFullYear() + "").substr(4 - e.length);
}).replace(/M{1,2}/, function(t) {
return e(t, n.getMonth() + 1);
}).replace(/D{1,2}|d{1,2}/, function(t) {
return e(t, n.getDate());
}).replace(/Q|q/, function(t) {
return e(t, Math.ceil((n.getMonth() + 1) / 3));
}).replace(/h{1,2}|H{1,2}/, function(t) {
return e(t, n.getHours());
}).replace(/m{1,2}/, function(t) {
return e(t, n.getMinutes());
}).replace(/s{1,2}/, function(t) {
return e(t, n.getSeconds());
}).replace(/SSS|S/, function(e) {
var t = "" + n.getMilliseconds();
return 1 === e.length ? t : (1 === t.length ? "00" : 2 === t.length ? "0" : "") + t;
});
};
Date.today = function() {
var e = new Date();
return new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime();
};
}, {} ],
273: [ function() {
"use strict";
Object.merge = function(e) {
for (var t = [], n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
for (var r = 0; r < t.length; ++r) {
var o = t[r];
if ("object" == typeof o && null != o) for (var i in o) o.hasOwnProperty(i) && (o[i] instanceof Date ? e[i] = new Date(o[i]) : "object" == typeof e[i] && null != e[i] && "object" == typeof o[i] && null != o[i] ? Object.merge(e[i], o[i]) : Array.isArray(o[i]) ? e[i] = Object.merge([], o[i]) : "object" == typeof o[i] && null !== o[i] ? e[i] = Object.merge({}, o[i]) : e[i] = o[i]);
}
return e;
};
Object.values || (Object.values = function(e) {
var t = [];
for (var n in e) e.hasOwnProperty(n) && t.push(e[n]);
return t;
});
Object.entries || (Object.entries = function(e) {
var t = [];
for (var n in e) e.hasOwnProperty(n) && t.push([ n, e[n] ]);
return t;
});
Object.forEach = function(e, t) {
for (var n in e) {
if (!e.hasOwnProperty(n)) return;
t(e[n], n, e);
}
};
}, {} ],
274: [ function(e, t) {
"use strict";
t.exports = e("./src/index-minimal");
}, {
"./src/index-minimal": 275
} ],
275: [ function(e, t, n) {
"use strict";
var r = n;
r.build = "minimal";
r.Writer = e("./writer");
r.BufferWriter = e("./writer_buffer");
r.Reader = e("./reader");
r.BufferReader = e("./reader_buffer");
r.util = e("./util/minimal");
r.rpc = e("./rpc");
r.roots = e("./roots");
r.configure = o;
function o() {
r.util._configure();
r.Writer._configure(r.BufferWriter);
r.Reader._configure(r.BufferReader);
}
o();
}, {
"./reader": 276,
"./reader_buffer": 277,
"./roots": 278,
"./rpc": 279,
"./util/minimal": 282,
"./writer": 283,
"./writer_buffer": 284
} ],
276: [ function(e, t) {
"use strict";
t.exports = a;
var n, r = e("./util/minimal"), o = r.LongBits, i = r.utf8;
function s(e, t) {
return RangeError("index out of range: " + e.pos + " + " + (t || 1) + " > " + e.len);
}
function a(e) {
this.buf = e;
this.pos = 0;
this.len = e.length;
}
var c, l = "undefined" != typeof Uint8Array ? function(e) {
if (e instanceof Uint8Array || Array.isArray(e)) return new a(e);
throw Error("illegal buffer");
} : function(e) {
if (Array.isArray(e)) return new a(e);
throw Error("illegal buffer");
}, u = function() {
return r.Buffer ? function(e) {
return (a.create = function(e) {
return r.Buffer.isBuffer(e) ? new n(e) : l(e);
})(e);
} : l;
};
a.create = u();
a.prototype._slice = r.Array.prototype.subarray || r.Array.prototype.slice;
a.prototype.uint32 = (c = 4294967295, function() {
c = (127 & this.buf[this.pos]) >>> 0;
if (this.buf[this.pos++] < 128) return c;
c = (c | (127 & this.buf[this.pos]) << 7) >>> 0;
if (this.buf[this.pos++] < 128) return c;
c = (c | (127 & this.buf[this.pos]) << 14) >>> 0;
if (this.buf[this.pos++] < 128) return c;
c = (c | (127 & this.buf[this.pos]) << 21) >>> 0;
if (this.buf[this.pos++] < 128) return c;
c = (c | (15 & this.buf[this.pos]) << 28) >>> 0;
if (this.buf[this.pos++] < 128) return c;
if ((this.pos += 5) > this.len) {
this.pos = this.len;
throw s(this, 10);
}
return c;
});
a.prototype.int32 = function() {
return 0 | this.uint32();
};
a.prototype.sint32 = function() {
var e = this.uint32();
return e >>> 1 ^ -(1 & e) | 0;
};
function p() {
var e = new o(0, 0), t = 0;
if (!(this.len - this.pos > 4)) {
for (;t < 3; ++t) {
if (this.pos >= this.len) throw s(this);
e.lo = (e.lo | (127 & this.buf[this.pos]) << 7 * t) >>> 0;
if (this.buf[this.pos++] < 128) return e;
}
e.lo = (e.lo | (127 & this.buf[this.pos++]) << 7 * t) >>> 0;
return e;
}
for (;t < 4; ++t) {
e.lo = (e.lo | (127 & this.buf[this.pos]) << 7 * t) >>> 0;
if (this.buf[this.pos++] < 128) return e;
}
e.lo = (e.lo | (127 & this.buf[this.pos]) << 28) >>> 0;
e.hi = (e.hi | (127 & this.buf[this.pos]) >> 4) >>> 0;
if (this.buf[this.pos++] < 128) return e;
t = 0;
if (this.len - this.pos > 4) for (;t < 5; ++t) {
e.hi = (e.hi | (127 & this.buf[this.pos]) << 7 * t + 3) >>> 0;
if (this.buf[this.pos++] < 128) return e;
} else for (;t < 5; ++t) {
if (this.pos >= this.len) throw s(this);
e.hi = (e.hi | (127 & this.buf[this.pos]) << 7 * t + 3) >>> 0;
if (this.buf[this.pos++] < 128) return e;
}
throw Error("invalid varint encoding");
}
a.prototype.bool = function() {
return 0 !== this.uint32();
};
function f(e, t) {
return (e[t - 4] | e[t - 3] << 8 | e[t - 2] << 16 | e[t - 1] << 24) >>> 0;
}
a.prototype.fixed32 = function() {
if (this.pos + 4 > this.len) throw s(this, 4);
return f(this.buf, this.pos += 4);
};
a.prototype.sfixed32 = function() {
if (this.pos + 4 > this.len) throw s(this, 4);
return 0 | f(this.buf, this.pos += 4);
};
function d() {
if (this.pos + 8 > this.len) throw s(this, 8);
return new o(f(this.buf, this.pos += 4), f(this.buf, this.pos += 4));
}
a.prototype.float = function() {
if (this.pos + 4 > this.len) throw s(this, 4);
var e = r.float.readFloatLE(this.buf, this.pos);
this.pos += 4;
return e;
};
a.prototype.double = function() {
if (this.pos + 8 > this.len) throw s(this, 4);
var e = r.float.readDoubleLE(this.buf, this.pos);
this.pos += 8;
return e;
};
a.prototype.bytes = function() {
var e = this.uint32(), t = this.pos, n = this.pos + e;
if (n > this.len) throw s(this, e);
this.pos += e;
return Array.isArray(this.buf) ? this.buf.slice(t, n) : t === n ? new this.buf.constructor(0) : this._slice.call(this.buf, t, n);
};
a.prototype.string = function() {
var e = this.bytes();
return i.read(e, 0, e.length);
};
a.prototype.skip = function(e) {
if ("number" == typeof e) {
if (this.pos + e > this.len) throw s(this, e);
this.pos += e;
} else do {
if (this.pos >= this.len) throw s(this);
} while (128 & this.buf[this.pos++]);
return this;
};
a.prototype.skipType = function(e) {
switch (e) {
case 0:
this.skip();
break;

case 1:
this.skip(8);
break;

case 2:
this.skip(this.uint32());
break;

case 3:
for (;4 != (e = 7 & this.uint32()); ) this.skipType(e);
break;

case 5:
this.skip(4);
break;

default:
throw Error("invalid wire type " + e + " at offset " + this.pos);
}
return this;
};
a._configure = function(e) {
n = e;
a.create = u();
n._configure();
var t = r.Long ? "toLong" : "toNumber";
r.merge(a.prototype, {
int64: function() {
return p.call(this)[t](!1);
},
uint64: function() {
return p.call(this)[t](!0);
},
sint64: function() {
return p.call(this).zzDecode()[t](!1);
},
fixed64: function() {
return d.call(this)[t](!0);
},
sfixed64: function() {
return d.call(this)[t](!1);
}
});
};
}, {
"./util/minimal": 282
} ],
277: [ function(e, t) {
"use strict";
t.exports = o;
var n = e("./reader");
(o.prototype = Object.create(n.prototype)).constructor = o;
var r = e("./util/minimal");
function o(e) {
n.call(this, e);
}
o._configure = function() {
r.Buffer && (o.prototype._slice = r.Buffer.prototype.slice);
};
o.prototype.string = function() {
var e = this.uint32();
return this.buf.utf8Slice ? this.buf.utf8Slice(this.pos, this.pos = Math.min(this.pos + e, this.len)) : this.buf.toString("utf-8", this.pos, this.pos = Math.min(this.pos + e, this.len));
};
o._configure();
}, {
"./reader": 276,
"./util/minimal": 282
} ],
278: [ function(e, t) {
"use strict";
t.exports = {};
}, {} ],
279: [ function(e, t, n) {
"use strict";
n.Service = e("./rpc/service");
}, {
"./rpc/service": 280
} ],
280: [ function(e, t) {
"use strict";
t.exports = r;
var n = e("../util/minimal");
(r.prototype = Object.create(n.EventEmitter.prototype)).constructor = r;
function r(e, t, r) {
if ("function" != typeof e) throw TypeError("rpcImpl must be a function");
n.EventEmitter.call(this);
this.rpcImpl = e;
this.requestDelimited = Boolean(t);
this.responseDelimited = Boolean(r);
}
r.prototype.rpcCall = function e(t, r, o, i, s) {
if (!i) throw TypeError("request must be specified");
var a = this;
if (!s) return n.asPromise(e, a, t, r, o, i);
if (a.rpcImpl) try {
return a.rpcImpl(t, r[a.requestDelimited ? "encodeDelimited" : "encode"](i).finish(), function(e, n) {
if (e) {
a.emit("error", e, t);
return s(e);
}
if (null !== n) {
if (!(n instanceof o)) try {
n = o[a.responseDelimited ? "decodeDelimited" : "decode"](n);
} catch (e) {
a.emit("error", e, t);
return s(e);
}
a.emit("data", n, t);
return s(null, n);
}
a.end(!0);
});
} catch (e) {
a.emit("error", e, t);
setTimeout(function() {
s(e);
}, 0);
return;
} else setTimeout(function() {
s(Error("already ended"));
}, 0);
};
r.prototype.end = function(e) {
if (this.rpcImpl) {
e || this.rpcImpl(null, null, null);
this.rpcImpl = null;
this.emit("end").off();
}
return this;
};
}, {
"../util/minimal": 282
} ],
281: [ function(e, t) {
"use strict";
t.exports = r;
var n = e("../util/minimal");
function r(e, t) {
this.lo = e >>> 0;
this.hi = t >>> 0;
}
var o = r.zero = new r(0, 0);
o.toNumber = function() {
return 0;
};
o.zzEncode = o.zzDecode = function() {
return this;
};
o.length = function() {
return 1;
};
var i = r.zeroHash = "\0\0\0\0\0\0\0\0";
r.fromNumber = function(e) {
if (0 === e) return o;
var t = e < 0;
t && (e = -e);
var n = e >>> 0, i = (e - n) / 4294967296 >>> 0;
if (t) {
i = ~i >>> 0;
n = ~n >>> 0;
if (++n > 4294967295) {
n = 0;
++i > 4294967295 && (i = 0);
}
}
return new r(n, i);
};
r.from = function(e) {
if ("number" == typeof e) return r.fromNumber(e);
if (n.isString(e)) {
if (!n.Long) return r.fromNumber(parseInt(e, 10));
e = n.Long.fromString(e);
}
return e.low || e.high ? new r(e.low >>> 0, e.high >>> 0) : o;
};
r.prototype.toNumber = function(e) {
if (!e && this.hi >>> 31) {
var t = 1 + ~this.lo >>> 0, n = ~this.hi >>> 0;
t || (n = n + 1 >>> 0);
return -(t + 4294967296 * n);
}
return this.lo + 4294967296 * this.hi;
};
r.prototype.toLong = function(e) {
return n.Long ? new n.Long(0 | this.lo, 0 | this.hi, Boolean(e)) : {
low: 0 | this.lo,
high: 0 | this.hi,
unsigned: Boolean(e)
};
};
var s = String.prototype.charCodeAt;
r.fromHash = function(e) {
return e === i ? o : new r((s.call(e, 0) | s.call(e, 1) << 8 | s.call(e, 2) << 16 | s.call(e, 3) << 24) >>> 0, (s.call(e, 4) | s.call(e, 5) << 8 | s.call(e, 6) << 16 | s.call(e, 7) << 24) >>> 0);
};
r.prototype.toHash = function() {
return String.fromCharCode(255 & this.lo, this.lo >>> 8 & 255, this.lo >>> 16 & 255, this.lo >>> 24, 255 & this.hi, this.hi >>> 8 & 255, this.hi >>> 16 & 255, this.hi >>> 24);
};
r.prototype.zzEncode = function() {
var e = this.hi >> 31;
this.hi = ((this.hi << 1 | this.lo >>> 31) ^ e) >>> 0;
this.lo = (this.lo << 1 ^ e) >>> 0;
return this;
};
r.prototype.zzDecode = function() {
var e = -(1 & this.lo);
this.lo = ((this.lo >>> 1 | this.hi << 31) ^ e) >>> 0;
this.hi = (this.hi >>> 1 ^ e) >>> 0;
return this;
};
r.prototype.length = function() {
var e = this.lo, t = (this.lo >>> 28 | this.hi << 4) >>> 0, n = this.hi >>> 24;
return 0 === n ? 0 === t ? e < 16384 ? e < 128 ? 1 : 2 : e < 2097152 ? 3 : 4 : t < 16384 ? t < 128 ? 5 : 6 : t < 2097152 ? 7 : 8 : n < 128 ? 9 : 10;
};
}, {
"../util/minimal": 282
} ],
282: [ function(e, t, n) {
(function(t) {
"use strict";
var r = n;
r.asPromise = e("@protobufjs/aspromise");
r.base64 = e("@protobufjs/base64");
r.EventEmitter = e("@protobufjs/eventemitter");
r.float = e("@protobufjs/float");
r.inquire = e("@protobufjs/inquire");
r.utf8 = e("@protobufjs/utf8");
r.pool = e("@protobufjs/pool");
r.LongBits = e("./longbits");
r.isNode = Boolean("undefined" != typeof t && t && t.process && t.process.versions && t.process.versions.node);
r.global = r.isNode && t || "undefined" != typeof window && window || "undefined" != typeof self && self || this;
r.emptyArray = Object.freeze ? Object.freeze([]) : [];
r.emptyObject = Object.freeze ? Object.freeze({}) : {};
r.isInteger = Number.isInteger || function(e) {
return "number" == typeof e && isFinite(e) && Math.floor(e) === e;
};
r.isString = function(e) {
return "string" == typeof e || e instanceof String;
};
r.isObject = function(e) {
return e && "object" == typeof e;
};
r.isset = r.isSet = function(e, t) {
var n = e[t];
return !(null == n || !e.hasOwnProperty(t)) && ("object" != typeof n || (Array.isArray(n) ? n.length : Object.keys(n).length) > 0);
};
r.Buffer = function() {
try {
var e = r.inquire("buffer").Buffer;
return e.prototype.utf8Write ? e : null;
} catch (e) {
return null;
}
}();
r._Buffer_from = null;
r._Buffer_allocUnsafe = null;
r.newBuffer = function(e) {
return "number" == typeof e ? r.Buffer ? r._Buffer_allocUnsafe(e) : new r.Array(e) : r.Buffer ? r._Buffer_from(e) : "undefined" == typeof Uint8Array ? e : new Uint8Array(e);
};
r.Array = "undefined" != typeof Uint8Array ? Uint8Array : Array;
r.Long = r.global.dcodeIO && r.global.dcodeIO.Long || r.global.Long || r.inquire("long");
r.key2Re = /^true|false|0|1$/;
r.key32Re = /^-?(?:0|[1-9][0-9]*)$/;
r.key64Re = /^(?:[\\x00-\\xff]{8}|-?(?:0|[1-9][0-9]*))$/;
r.longToHash = function(e) {
return e ? r.LongBits.from(e).toHash() : r.LongBits.zeroHash;
};
r.longFromHash = function(e, t) {
var n = r.LongBits.fromHash(e);
return r.Long ? r.Long.fromBits(n.lo, n.hi, t) : n.toNumber(Boolean(t));
};
function o(e, t, n) {
for (var r = Object.keys(t), o = 0; o < r.length; ++o) void 0 !== e[r[o]] && n || (e[r[o]] = t[r[o]]);
return e;
}
r.merge = o;
r.lcFirst = function(e) {
return e.charAt(0).toLowerCase() + e.substring(1);
};
function i(e) {
function t(e, n) {
if (!(this instanceof t)) return new t(e, n);
Object.defineProperty(this, "message", {
get: function() {
return e;
}
});
Error.captureStackTrace ? Error.captureStackTrace(this, t) : Object.defineProperty(this, "stack", {
value: new Error().stack || ""
});
n && o(this, n);
}
(t.prototype = Object.create(Error.prototype)).constructor = t;
Object.defineProperty(t.prototype, "name", {
get: function() {
return e;
}
});
t.prototype.toString = function() {
return this.name + ": " + this.message;
};
return t;
}
r.newError = i;
r.ProtocolError = i("ProtocolError");
r.oneOfGetter = function(e) {
for (var t = {}, n = 0; n < e.length; ++n) t[e[n]] = 1;
return function() {
for (var e = Object.keys(this), n = e.length - 1; n > -1; --n) if (1 === t[e[n]] && void 0 !== this[e[n]] && null !== this[e[n]]) return e[n];
};
};
r.oneOfSetter = function(e) {
return function(t) {
for (var n = 0; n < e.length; ++n) e[n] !== t && delete this[e[n]];
};
};
r.toJSONOptions = {
longs: String,
enums: String,
bytes: String,
json: !0
};
r._configure = function() {
var e = r.Buffer;
if (e) {
r._Buffer_from = e.from !== Uint8Array.from && e.from || function(t, n) {
return new e(t, n);
};
r._Buffer_allocUnsafe = e.allocUnsafe || function(t) {
return new e(t);
};
} else r._Buffer_from = r._Buffer_allocUnsafe = null;
};
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"./longbits": 281,
"@protobufjs/aspromise": 5,
"@protobufjs/base64": 6,
"@protobufjs/eventemitter": 7,
"@protobufjs/float": 8,
"@protobufjs/inquire": 9,
"@protobufjs/pool": 10,
"@protobufjs/utf8": 11
} ],
283: [ function(e, t) {
"use strict";
t.exports = u;
var n, r = e("./util/minimal"), o = r.LongBits, i = r.base64, s = r.utf8;
function a(e, t, n) {
this.fn = e;
this.len = t;
this.next = void 0;
this.val = n;
}
function c() {}
function l(e) {
this.head = e.head;
this.tail = e.tail;
this.len = e.len;
this.next = e.states;
}
function u() {
this.len = 0;
this.head = new a(c, 0, 0);
this.tail = this.head;
this.states = null;
}
var p = function() {
return r.Buffer ? function() {
return (u.create = function() {
return new n();
})();
} : function() {
return new u();
};
};
u.create = p();
u.alloc = function(e) {
return new r.Array(e);
};
r.Array !== Array && (u.alloc = r.pool(u.alloc, r.Array.prototype.subarray));
u.prototype._push = function(e, t, n) {
this.tail = this.tail.next = new a(e, t, n);
this.len += t;
return this;
};
function f(e, t, n) {
t[n] = 255 & e;
}
function d(e, t) {
this.len = e;
this.next = void 0;
this.val = t;
}
d.prototype = Object.create(a.prototype);
d.prototype.fn = function(e, t, n) {
for (;e > 127; ) {
t[n++] = 127 & e | 128;
e >>>= 7;
}
t[n] = e;
};
u.prototype.uint32 = function(e) {
this.len += (this.tail = this.tail.next = new d((e >>>= 0) < 128 ? 1 : e < 16384 ? 2 : e < 2097152 ? 3 : e < 268435456 ? 4 : 5, e)).len;
return this;
};
u.prototype.int32 = function(e) {
return e < 0 ? this._push(h, 10, o.fromNumber(e)) : this.uint32(e);
};
u.prototype.sint32 = function(e) {
return this.uint32((e << 1 ^ e >> 31) >>> 0);
};
function h(e, t, n) {
for (;e.hi; ) {
t[n++] = 127 & e.lo | 128;
e.lo = (e.lo >>> 7 | e.hi << 25) >>> 0;
e.hi >>>= 7;
}
for (;e.lo > 127; ) {
t[n++] = 127 & e.lo | 128;
e.lo = e.lo >>> 7;
}
t[n++] = e.lo;
}
u.prototype.uint64 = function(e) {
var t = o.from(e);
return this._push(h, t.length(), t);
};
u.prototype.int64 = u.prototype.uint64;
u.prototype.sint64 = function(e) {
var t = o.from(e).zzEncode();
return this._push(h, t.length(), t);
};
u.prototype.bool = function(e) {
return this._push(f, 1, e ? 1 : 0);
};
function y(e, t, n) {
t[n] = 255 & e;
t[n + 1] = e >>> 8 & 255;
t[n + 2] = e >>> 16 & 255;
t[n + 3] = e >>> 24;
}
u.prototype.fixed32 = function(e) {
return this._push(y, 4, e >>> 0);
};
u.prototype.sfixed32 = u.prototype.fixed32;
u.prototype.fixed64 = function(e) {
var t = o.from(e);
return this._push(y, 4, t.lo)._push(y, 4, t.hi);
};
u.prototype.sfixed64 = u.prototype.fixed64;
u.prototype.float = function(e) {
return this._push(r.float.writeFloatLE, 4, e);
};
u.prototype.double = function(e) {
return this._push(r.float.writeDoubleLE, 8, e);
};
var v = r.Array.prototype.set ? function(e, t, n) {
t.set(e, n);
} : function(e, t, n) {
for (var r = 0; r < e.length; ++r) t[n + r] = e[r];
};
u.prototype.bytes = function(e) {
var t = e.length >>> 0;
if (!t) return this._push(f, 1, 0);
if (r.isString(e)) {
var n = u.alloc(t = i.length(e));
i.decode(e, n, 0);
e = n;
}
return this.uint32(t)._push(v, t, e);
};
u.prototype.string = function(e) {
var t = s.length(e);
return t ? this.uint32(t)._push(s.write, t, e) : this._push(f, 1, 0);
};
u.prototype.fork = function() {
this.states = new l(this);
this.head = this.tail = new a(c, 0, 0);
this.len = 0;
return this;
};
u.prototype.reset = function() {
if (this.states) {
this.head = this.states.head;
this.tail = this.states.tail;
this.len = this.states.len;
this.states = this.states.next;
} else {
this.head = this.tail = new a(c, 0, 0);
this.len = 0;
}
return this;
};
u.prototype.ldelim = function() {
var e = this.head, t = this.tail, n = this.len;
this.reset().uint32(n);
if (n) {
this.tail.next = e.next;
this.tail = t;
this.len += n;
}
return this;
};
u.prototype.finish = function() {
for (var e = this.head.next, t = this.constructor.alloc(this.len), n = 0; e; ) {
e.fn(e.val, t, n);
n += e.len;
e = e.next;
}
return t;
};
u._configure = function(e) {
n = e;
u.create = p();
n._configure();
};
}, {
"./util/minimal": 282
} ],
284: [ function(e, t) {
"use strict";
t.exports = o;
var n = e("./writer");
(o.prototype = Object.create(n.prototype)).constructor = o;
var r = e("./util/minimal");
function o() {
n.call(this);
}
o._configure = function() {
o.alloc = r._Buffer_allocUnsafe;
o.writeBytesBuffer = r.Buffer && r.Buffer.prototype instanceof Uint8Array && "set" === r.Buffer.prototype.set.name ? function(e, t, n) {
t.set(e, n);
} : function(e, t, n) {
if (e.copy) e.copy(t, n, 0, e.length); else for (var r = 0; r < e.length; ) t[n++] = e[r++];
};
};
o.prototype.bytes = function(e) {
r.isString(e) && (e = r._Buffer_from(e, "base64"));
var t = e.length >>> 0;
this.uint32(t);
t && this._push(o.writeBytesBuffer, t, e);
return this;
};
function i(e, t, n) {
e.length < 40 ? r.utf8.write(e, t, n) : t.utf8Write ? t.utf8Write(e, n) : t.write(e, n);
}
o.prototype.string = function(e) {
var t = r.Buffer.byteLength(e);
this.uint32(t);
t && this._push(i, t, e);
return this;
};
o._configure();
}, {
"./util/minimal": 282,
"./writer": 283
} ],
285: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e() {}
e.Boolean = "Boolean";
e.Number = "Number";
e.String = "String";
e.Array = "Array";
e.Tuple = "Tuple";
e.Enum = "Enum";
e.Any = "Any";
e.Literal = "Literal";
e.Object = "Object";
e.Interface = "Interface";
e.Buffer = "Buffer";
e.IndexedAccess = "IndexedAccess";
e.Reference = "Reference";
e.Keyof = "Keyof";
e.Union = "Union";
e.Intersection = "Intersection";
e.NonNullable = "NonNullable";
e.Date = "Date";
e.Pick = "Pick";
e.Partial = "Partial";
e.Omit = "Omit";
e.Overwrite = "Overwrite";
e.Custom = "Custom";
return e;
}();
n.SchemaType = r;
}, {} ],
286: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
e("k8w-extend-native");
var r, o, i = e("tslib"), s = e("tsbuffer-schema"), a = function() {
function e(e) {
this._schemaWithUuids = [];
this._unionPropertiesCache = {};
this._flatInterfaceSchemaCache = {};
this.proto = e;
}
e.prototype.parseReference = function(e) {
if (e.type === s.SchemaType.Reference) {
var t = this.proto[e.target];
if (!t) throw new Error("Cannot find reference target: ".concat(e.target));
return this.isTypeReference(t) ? this.parseReference(t) : t;
}
if (e.type === s.SchemaType.IndexedAccess) {
if (!this.isInterface(e.objectType)) throw new Error("Error objectType: ".concat(e.objectType.type));
var n = this.getFlatInterfaceSchema(e.objectType), r = n.properties.find(function(t) {
return t.name === e.index;
}), o = void 0;
if (r) o = r.type; else {
if (!n.indexSignature) throw new Error("Error index: ".concat(e.index));
o = n.indexSignature.type;
}
r && r.optional && (r.type.type !== s.SchemaType.Union || -1 === r.type.members.findIndex(function(e) {
return e.type.type === s.SchemaType.Literal && void 0 === e.type.literal;
})) && (o = {
type: s.SchemaType.Union,
members: [ {
id: 0,
type: o
}, {
id: 1,
type: {
type: s.SchemaType.Literal,
literal: void 0
}
} ]
});
return this.isTypeReference(o) ? this.parseReference(o) : o;
}
if (e.type === s.SchemaType.Keyof) {
var i = this.getFlatInterfaceSchema(e.target);
return {
type: s.SchemaType.Union,
members: i.properties.map(function(e, t) {
return {
id: t,
type: {
type: s.SchemaType.Literal,
literal: e.name
}
};
})
};
}
return e;
};
e.prototype.isInterface = function(e, t) {
void 0 === t && (t = !1);
if (!t && this.isTypeReference(e)) {
var n = this.parseReference(e);
return this.isInterface(n, t);
}
return e.type === s.SchemaType.Interface || this.isMappedType(e) && this.parseMappedType(e).type === s.SchemaType.Interface;
};
e.prototype.isMappedType = function(e) {
return e.type === s.SchemaType.Pick || e.type === s.SchemaType.Partial || e.type === s.SchemaType.Omit || e.type === s.SchemaType.Overwrite;
};
e.prototype.isTypeReference = function(e) {
return e.type === s.SchemaType.Reference || e.type === s.SchemaType.IndexedAccess;
};
e.prototype._getSchemaUuid = function(e) {
var t = e;
t.uuid || (t.uuid = this._schemaWithUuids.push(t));
return t.uuid;
};
e.prototype.getUnionProperties = function(e) {
var t = this._getSchemaUuid(e);
this._unionPropertiesCache[t] || (this._unionPropertiesCache[t] = this._addUnionProperties([], e.members.map(function(e) {
return e.type;
})));
return this._unionPropertiesCache[t];
};
e.prototype._addUnionProperties = function(e, t) {
for (var n = 0, r = t.length; n < r; ++n) {
var o = this.parseReference(t[n]);
if (this.isInterface(o)) {
var i = this.getFlatInterfaceSchema(o);
i.properties.forEach(function(t) {
e.binaryInsert(t.name, !0);
});
if (i.indexSignature) {
var a = "[[".concat(i.indexSignature.keyType, "]]");
e.binaryInsert(a, !0);
}
} else o.type === s.SchemaType.Intersection || o.type === s.SchemaType.Union ? this._addUnionProperties(e, o.members.map(function(e) {
return e.type;
})) : this.isMappedType(o) && this._addUnionProperties(e, [ this.parseMappedType(o) ]);
}
return e;
};
e.prototype.applyUnionProperties = function(e, t) {
for (var n = i.__assign(i.__assign({}, e), {
properties: e.properties.slice()
}), r = function(t) {
"[[String]]" === t ? n.indexSignature = n.indexSignature || {
keyType: s.SchemaType.String,
type: {
type: s.SchemaType.Any
}
} : "[[Number]]" === t ? n.indexSignature = n.indexSignature || {
keyType: s.SchemaType.Number,
type: {
type: s.SchemaType.Any
}
} : e.properties.find(function(e) {
return e.name === t;
}) || n.properties.push({
id: -1,
name: t,
optional: !0,
type: {
type: s.SchemaType.Any
}
});
}, o = 0, a = t; o < a.length; o++) r(a[o]);
return n;
};
e.prototype.getFlatInterfaceSchema = function(e) {
var t = this._getSchemaUuid(e);
if (this._flatInterfaceSchemaCache[t]) return this._flatInterfaceSchemaCache[t];
if (this.isTypeReference(e)) {
var n = this.parseReference(e);
if (n.type !== s.SchemaType.Interface) throw new Error("Cannot flatten non interface type: ".concat(n.type));
this._flatInterfaceSchemaCache[t] = this.getFlatInterfaceSchema(n);
} else if (e.type === s.SchemaType.Interface) this._flatInterfaceSchemaCache[t] = this._flattenInterface(e); else {
if (!this.isMappedType(e)) throw new Error("Invalid interface type: " + e.type);
this._flatInterfaceSchemaCache[t] = this._flattenMappedType(e);
}
return this._flatInterfaceSchemaCache[t];
};
e.prototype._flattenInterface = function(e) {
var t, n = {};
if (e.properties) for (var r = 0, o = e.properties; r < o.length; r++) n[(d = o[r]).name] = {
optional: d.optional,
type: d.type
};
e.indexSignature && (t = e.indexSignature);
if (e.extends) for (var i = 0, a = e.extends; i < a.length; i++) {
var c = a[i], l = this.parseReference(c.type);
this.isMappedType(l) && (l = this._flattenMappedType(l));
if (!this.isInterface(l)) throw new Error("SchemaError: extends must from interface but from " + l.type);
var u = this.getFlatInterfaceSchema(l);
if (u.properties) for (var p = 0, f = u.properties; p < f.length; p++) {
var d;
n[(d = f[p]).name] || (n[d.name] = {
optional: d.optional,
type: d.type
});
}
u.indexSignature && !t && (t = u.indexSignature);
}
return {
type: s.SchemaType.Interface,
properties: Object.entries(n).map(function(e, t) {
return {
id: t,
name: e[0],
optional: e[1].optional,
type: e[1].type
};
}),
indexSignature: t
};
};
e.prototype._flattenMappedType = function(e) {
var t, n;
if ((t = this.isTypeReference(e.target) ? this.parseReference(e.target) : e.target).type === s.SchemaType.Pick || t.type === s.SchemaType.Partial || t.type === s.SchemaType.Omit || t.type === s.SchemaType.Overwrite) n = this._flattenMappedType(t); else {
if (t.type !== s.SchemaType.Interface) throw new Error("Invalid target.type: ".concat(t.type));
n = this._flattenInterface(t);
}
if (e.type === s.SchemaType.Pick) {
for (var r = [], o = function(e) {
var t = n.properties.find(function(t) {
return t.name === e;
});
t ? r.push({
id: r.length,
name: e,
optional: t.optional,
type: t.type
}) : n.indexSignature && r.push({
id: r.length,
name: e,
type: n.indexSignature.type
});
}, i = 0, a = e.keys; i < a.length; i++) o(a[i]);
return {
type: s.SchemaType.Interface,
properties: r
};
}
if (e.type === s.SchemaType.Partial) {
for (var c = 0, l = n.properties; c < l.length; c++) l[c].optional = !0;
return n;
}
if (e.type === s.SchemaType.Omit) {
for (var u = function(e) {
n.properties.removeOne(function(t) {
return t.name === e;
});
}, p = 0, f = e.keys; p < f.length; p++) u(f[p]);
return n;
}
if (e.type === s.SchemaType.Overwrite) {
var d = this.getFlatInterfaceSchema(e.overwrite);
d.indexSignature && (n.indexSignature = d.indexSignature);
for (var h = function(e) {
n.properties.removeOne(function(t) {
return t.name === e.name;
});
n.properties.push(e);
}, y = 0, v = d.properties; y < v.length; y++) h(v[y]);
return n;
}
throw new Error("Unknown type: ".concat(e.type));
};
e.prototype.parseMappedType = function(e) {
var t = [], n = e;
do {
t.push(n);
n = this.parseReference(n.target);
} while (this.isMappedType(n));
if (n.type === s.SchemaType.Interface) return n;
if (n.type === s.SchemaType.Union || n.type === s.SchemaType.Intersection) return {
type: n.type,
members: n.members.map(function(e) {
for (var n = e.type, r = t.length - 1; r > -1; --r) {
var o = t[r];
n = i.__assign(i.__assign({}, o), {
target: n
});
}
return {
id: e.id,
type: n
};
})
};
throw new Error("Unsupported pattern ".concat(e.type, "<").concat(n.type, ">"));
};
return e;
}();
(function(e) {
e.TypeError = "TypeError";
e.InvalidScalarType = "InvalidScalarType";
e.TupleOverLength = "TupleOverLength";
e.InvalidEnumValue = "InvalidEnumValue";
e.InvalidLiteralValue = "InvalidLiteralValue";
e.MissingRequiredProperty = "MissingRequiredProperty";
e.ExcessProperty = "ExcessProperty";
e.InvalidNumberKey = "InvalidNumberKey";
e.UnionTypesNotMatch = "UnionTypesNotMatch";
e.UnionMembersNotMatch = "UnionMembersNotMatch";
e.CustomError = "CustomError";
})(o || (o = {}));
var c = ((r = {})[o.TypeError] = function(e, t) {
return "Expected type to be `".concat(e, "`, actually `").concat(t, "`.");
}, r[o.InvalidScalarType] = function(e, t) {
return "`".concat(e, "` is not a valid `").concat(t, "`.");
}, r[o.TupleOverLength] = function(e, t) {
return "Value has ".concat(e, " elements but schema allows only ").concat(t, ".");
}, r[o.InvalidEnumValue] = function(e) {
return "`".concat(e, "` is not a valid enum member.");
}, r[o.InvalidLiteralValue] = function(e, t) {
return "Expected to equals `".concat(l(e), "`, actually `").concat(l(t), "`");
}, r[o.MissingRequiredProperty] = function(e) {
return "Missing required property `".concat(e, "`.");
}, r[o.ExcessProperty] = function(e) {
return "Excess property `".concat(e, "` should not exists.");
}, r[o.InvalidNumberKey] = function(e) {
return "`".concat(e, "` is not a valid key, the key here should be a `number`.");
}, r[o.UnionTypesNotMatch] = function(e, t) {
return "`".concat(l(e), "` is not matched to `").concat(t.join(" | "), "`");
}, r[o.UnionMembersNotMatch] = function(e) {
return "No union member matched, detail:\n".concat(e.map(function(e, t) {
return "  <".concat(t, "> ").concat(e.errMsg);
}).join("\n"));
}, r[o.CustomError] = function(e) {
return e;
}, r);
function l(e) {
if ("string" == typeof e) {
var t = JSON.stringify(e);
return "'" + t.substr(1, t.length - 2) + "'";
}
return JSON.stringify(e);
}
var u = function() {
function e(e) {
this.isSucc = !1;
this.error = e;
}
Object.defineProperty(e.prototype, "errMsg", {
get: function() {
return e.getErrMsg(this.error);
},
enumerable: !1,
configurable: !0
});
e.getErrMsg = function(e) {
var t, n = c[e.type].apply(c, e.params);
return (null === (t = e.inner) || void 0 === t ? void 0 : t.property.length) ? "Property `".concat(e.inner.property.join("."), "`: ").concat(n) : n;
};
return e;
}(), p = function() {
function e() {}
e.error = function(e) {
for (var t = [], n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
return new u({
type: e,
params: t
});
};
e.innerError = function(e, t, n, r) {
var o;
r.error.inner ? "string" == typeof e ? r.error.inner.property.unshift(e) : (o = r.error.inner.property).unshift.apply(o, e) : r.error.inner = {
property: "string" == typeof e ? [ e ] : e,
value: t,
schema: n
};
return r;
};
e.succ = {
isSucc: !0
};
return e;
}(), f = {
Int8Array: Int8Array,
Int16Array: Int16Array,
Int32Array: Int32Array,
BigInt64Array: "undefined" != typeof BigInt64Array ? BigInt64Array : void 0,
Uint8Array: Uint8Array,
Uint16Array: Uint16Array,
Uint32Array: Uint32Array,
BigUint64Array: "undefined" != typeof BigUint64Array ? BigUint64Array : void 0,
Float32Array: Float32Array,
Float64Array: Float64Array
}, d = function() {
function e(e, t) {
this.options = {
excessPropertyChecks: !0,
strictNullChecks: !1,
cloneProto: !0
};
t && (this.options = i.__assign(i.__assign({}, this.options), t));
this.proto = this.options.cloneProto ? Object.merge({}, e) : e;
this.protoHelper = new a(this.proto);
}
e.prototype.validate = function(e, t, n) {
var r, o, s, a;
if ("string" == typeof t) {
a = t;
if (!(s = this.proto[a])) throw new Error("Cannot find schema: ".concat(a));
} else s = t;
return this._validate(e, s, i.__assign(i.__assign({}, n), {
excessPropertyChecks: null !== (r = null == n ? void 0 : n.excessPropertyChecks) && void 0 !== r ? r : this.options.excessPropertyChecks,
strictNullChecks: null !== (o = null == n ? void 0 : n.strictNullChecks) && void 0 !== o ? o : this.options.strictNullChecks
}));
};
e.prototype._validate = function(e, t, n) {
var r, i;
switch (t.type) {
case s.SchemaType.Boolean:
i = this._validateBooleanType(e, t);
break;

case s.SchemaType.Number:
i = this._validateNumberType(e, t);
break;

case s.SchemaType.String:
i = this._validateStringType(e, t);
break;

case s.SchemaType.Array:
i = this._validateArrayType(e, t, n);
break;

case s.SchemaType.Tuple:
i = this._validateTupleType(e, t, n);
break;

case s.SchemaType.Enum:
i = this._validateEnumType(e, t);
break;

case s.SchemaType.Any:
i = this._validateAnyType(e);
break;

case s.SchemaType.Literal:
i = this._validateLiteralType(e, t, null !== (r = null == n ? void 0 : n.strictNullChecks) && void 0 !== r ? r : this.options.strictNullChecks);
break;

case s.SchemaType.Object:
i = this._validateObjectType(e, t);
break;

case s.SchemaType.Interface:
i = this._validateInterfaceType(e, t, n);
break;

case s.SchemaType.Buffer:
i = this._validateBufferType(e, t);
break;

case s.SchemaType.IndexedAccess:
case s.SchemaType.Reference:
i = this._validateReferenceType(e, t, n);
break;

case s.SchemaType.Union:
i = this._validateUnionType(e, t, n);
break;

case s.SchemaType.Intersection:
i = this._validateIntersectionType(e, t, n);
break;

case s.SchemaType.Pick:
case s.SchemaType.Omit:
case s.SchemaType.Partial:
case s.SchemaType.Overwrite:
i = this._validateMappedType(e, t, n);
break;

case s.SchemaType.Date:
i = this._validateDateType(e);
break;

case s.SchemaType.NonNullable:
i = this._validateNonNullableType(e, t, n);
break;

case s.SchemaType.Custom:
var a = t.validate(e);
i = a.isSucc ? p.succ : p.error(o.CustomError, a.errMsg);
break;

default:
throw new Error("Unsupported schema type: ".concat(t.type));
}
if (null == n ? void 0 : n.prune) {
void 0 === n.prune.output && (n.prune.output = e);
n.prune.parent && (n.prune.parent.value[n.prune.parent.key] = n.prune.output);
}
return i;
};
e.prototype.prune = function(e, t, n) {
var r, o = "string" == typeof t ? this.proto[t] : t;
if (!o) throw new Error("Cannot find schema: " + t);
var s = {}, a = this._validate(e, o, i.__assign(i.__assign({}, n), {
prune: s,
excessPropertyChecks: !1,
strictNullChecks: null !== (r = null == n ? void 0 : n.strictNullChecks) && void 0 !== r ? r : this.options.strictNullChecks
}));
a.isSucc && (a.pruneOutput = s.output);
return a;
};
e.prototype._validateBooleanType = function(e) {
var t = this._getTypeof(e);
return "boolean" === t ? p.succ : p.error(o.TypeError, "boolean", t);
};
e.prototype._validateNumberType = function(e, t) {
var n = t.scalarType || "double", r = this._getTypeof(e), i = n.indexOf("big") > -1 ? "bigint" : "number";
return r !== i ? p.error(o.TypeError, i, r) : "double" === n || "number" !== r || Number.isInteger(e) ? n.indexOf("uint") > -1 && e < 0 ? p.error(o.InvalidScalarType, e, n) : p.succ : p.error(o.InvalidScalarType, e, n);
};
e.prototype._validateStringType = function(e) {
var t = this._getTypeof(e);
return "string" === t ? p.succ : p.error(o.TypeError, "string", t);
};
e.prototype._validateArrayType = function(e, t, n) {
var r = this._getTypeof(e);
if (r !== s.SchemaType.Array) return p.error(o.TypeError, s.SchemaType.Array, r);
var a = n.prune;
a && (a.output = Array.from({
length: e.length
}));
for (var c = 0; c < e.length; ++c) {
var l = this._validate(e[c], t.elementType, i.__assign(i.__assign({}, n), {
prune: (null == a ? void 0 : a.output) ? {
parent: {
value: a.output,
key: c
}
} : void 0
}));
if (!l.isSucc) return p.innerError("" + c, e[c], t.elementType, l);
}
return p.succ;
};
e.prototype._validateTupleType = function(e, t, n) {
var r = this._getTypeof(e);
if (r !== s.SchemaType.Array) return p.error(o.TypeError, s.SchemaType.Array, r);
var i = n.prune;
if (!i && n.excessPropertyChecks && e.length > t.elementTypes.length) return p.error(o.TupleOverLength, e.length, t.elementTypes.length);
i && (i.output = Array.from({
length: Math.min(e.length, t.elementTypes.length)
}));
for (var a = 0; a < t.elementTypes.length; ++a) {
if (void 0 === e[a] || null === e[a] && !n.strictNullChecks) {
var c = this._canBeNull(t.elementTypes[a]), l = void 0 !== t.optionalStartIndex && a >= t.optionalStartIndex || this._canBeUndefined(t.elementTypes[a]);
if (l || !n.strictNullChecks && c) {
(null == i ? void 0 : i.output) && (null === e[a] && c || void 0 === e[a] && !l && c) && (i.output[a] = null);
continue;
}
return p.error(o.MissingRequiredProperty, a);
}
var u = this._validate(e[a], t.elementTypes[a], {
prune: (null == i ? void 0 : i.output) ? {
parent: {
value: i.output,
key: a
}
} : void 0,
strictNullChecks: n.strictNullChecks,
excessPropertyChecks: n.excessPropertyChecks
});
if (!u.isSucc) return p.innerError("" + a, e[a], t.elementTypes[a], u);
}
return p.succ;
};
e.prototype._canBeUndefined = function(e) {
var t = this;
return e.type === s.SchemaType.Union ? e.members.some(function(e) {
return t._canBeUndefined(e.type);
}) : e.type === s.SchemaType.Literal && void 0 === e.literal;
};
e.prototype._canBeNull = function(e) {
var t = this;
return e.type === s.SchemaType.Union ? e.members.some(function(e) {
return t._canBeNull(e.type);
}) : e.type === s.SchemaType.Literal && null === e.literal;
};
e.prototype._validateEnumType = function(e, t) {
var n = this._getTypeof(e);
return "string" !== n && "number" !== n ? p.error(o.TypeError, "string | number", n) : t.members.some(function(t) {
return t.value === e;
}) ? p.succ : p.error(o.InvalidEnumValue, e);
};
e.prototype._validateAnyType = function() {
return p.succ;
};
e.prototype._validateLiteralType = function(e, t, n) {
return n || null !== t.literal && void 0 !== t.literal ? e === t.literal ? p.succ : p.error(o.InvalidLiteralValue, t.literal, e) : null == e ? p.succ : p.error(o.InvalidLiteralValue, t.literal, e);
};
e.prototype._validateObjectType = function(e) {
var t = this._getTypeof(e);
return "Object" === t || "Array" === t ? p.succ : p.error(o.TypeError, "Object", t);
};
e.prototype._validateInterfaceType = function(e, t, n) {
var r = this._getTypeof(e);
if ("Object" !== r) return p.error(o.TypeError, "Object", r);
var i = this.protoHelper.getFlatInterfaceSchema(t);
n.unionProperties && (i = this.protoHelper.applyUnionProperties(i, n.unionProperties));
return this._validateFlatInterface(e, i, n);
};
e.prototype._validateMappedType = function(e, t, n) {
var r = this.protoHelper.parseMappedType(t);
if (r.type === s.SchemaType.Interface) return this._validateInterfaceType(e, t, n);
if (r.type === s.SchemaType.Union) return this._validateUnionType(e, r, n);
if (r.type === s.SchemaType.Intersection) return this._validateIntersectionType(e, r, n);
throw new Error("Invalid ".concat(t.type, " target type: ").concat(r.type));
};
e.prototype._validateFlatInterface = function(e, t, n) {
if (t.indexSignature && t.indexSignature.keyType === s.SchemaType.Number) for (var r in e) if (!this._isNumberKey(r)) return p.error(o.InvalidNumberKey, r);
var i = n.prune;
i && (i.output = {});
if (!i && n.excessPropertyChecks && !t.indexSignature) {
var a = t.properties.map(function(e) {
return e.name;
}), c = Object.keys(e).find(function(e) {
return -1 === a.indexOf(e);
});
if (c) return p.error(o.ExcessProperty, c);
}
if (t.properties) for (var l = 0, u = t.properties; l < u.length; l++) {
var f = u[l];
if (void 0 === e[f.name] || null === e[f.name] && !n.strictNullChecks) {
var d = this._canBeNull(f.type), h = f.optional || this._canBeUndefined(f.type);
if (h || !n.strictNullChecks && d) {
(null == i ? void 0 : i.output) && (null === e[f.name] && d || void 0 === e[f.name] && !h && d) && (i.output[f.name] = null);
continue;
}
return p.error(o.MissingRequiredProperty, f.name);
}
if (!(y = this._validate(e[f.name], f.type, {
prune: (null == i ? void 0 : i.output) && f.id > -1 ? {
parent: {
value: i.output,
key: f.name
}
} : void 0,
strictNullChecks: n.strictNullChecks,
excessPropertyChecks: n.excessPropertyChecks
})).isSucc) return p.innerError(f.name, e[f.name], f.type, y);
}
if (t.indexSignature) for (var r in e) {
var y;
if (!(y = this._validate(e[r], t.indexSignature.type, {
prune: (null == i ? void 0 : i.output) ? {
parent: {
value: i.output,
key: r
}
} : void 0,
strictNullChecks: n.strictNullChecks,
excessPropertyChecks: n.excessPropertyChecks
})).isSucc) return p.innerError(r, e[r], t.indexSignature.type, y);
}
return p.succ;
};
e.prototype._validateBufferType = function(e, t) {
var n, r, i = this._getTypeof(e);
if ("Object" !== i) return p.error(o.TypeError, t.arrayType || "ArrayBuffer", i);
if (t.arrayType) {
var s = f[t.arrayType];
if (!s) throw new Error("Error TypedArray type: ".concat(t.arrayType));
return e instanceof s ? p.succ : p.error(o.TypeError, t.arrayType, null === (n = null == e ? void 0 : e.constructor) || void 0 === n ? void 0 : n.name);
}
return e instanceof ArrayBuffer ? p.succ : p.error(o.TypeError, "ArrayBuffer", null === (r = null == e ? void 0 : e.constructor) || void 0 === r ? void 0 : r.name);
};
e.prototype._validateReferenceType = function(e, t, n) {
return this._validate(e, this.protoHelper.parseReference(t), n);
};
e.prototype._validateUnionType = function(e, t, n) {
var r = this;
n.unionProperties = n.unionProperties || this.protoHelper.getUnionProperties(t);
var a = !1, c = n.prune;
if (c && e && Object.getPrototypeOf(e) === Object.prototype) {
a = !0;
c.output = {};
}
for (var u = !1, f = [], d = 0; d < t.members.length; ++d) {
var h = t.members[d], y = this.protoHelper.isTypeReference(h.type) ? this.protoHelper.parseReference(h.type) : h.type, v = c ? {} : void 0, g = this._validate(e, y, i.__assign(i.__assign({}, n), {
prune: v
}));
if (g.isSucc) {
u = !0;
if (!a) break;
c.output = i.__assign(i.__assign({}, c.output), v.output);
} else f.push(g);
}
if (u) return p.succ;
var _ = f[0].errMsg;
if (f.every(function(e) {
return e.errMsg === _;
})) return f[0];
var m = f.filter(function(e) {
return e.error.type !== o.InvalidLiteralValue;
});
if (1 === m.length) return m[0];
if (f.every(function(e) {
return !e.error.inner && (e.error.type === o.TypeError || e.error.type === o.InvalidLiteralValue);
})) {
var b = this._getTypeof(e), w = f.map(function(e) {
return e.error.type === o.TypeError ? e.error.params[0] : r._getTypeof(e.error.params[0]);
}).distinct();
if (-1 === w.indexOf(b)) return p.error(o.TypeError, w.join(" | "), this._getTypeof(e));
if ("Object" !== b && b !== s.SchemaType.Array) {
var S = f.map(function(e) {
return e.error.type === o.TypeError ? e.error.params[0] : l(e.error.params[0]);
}).distinct();
return p.error(o.UnionTypesNotMatch, e, S);
}
}
return p.error(o.UnionMembersNotMatch, f);
};
e.prototype._validateIntersectionType = function(e, t, n) {
n.unionProperties = n.unionProperties || this.protoHelper.getUnionProperties(t);
var r = !1, o = n.prune;
if (o && e && Object.getPrototypeOf(e) === Object.prototype) {
o.output = {};
r = !0;
}
for (var s = 0, a = t.members.length; s < a; ++s) {
var c = t.members[s].type;
c = this.protoHelper.isTypeReference(c) ? this.protoHelper.parseReference(c) : c;
var l = o ? {} : void 0, u = this._validate(e, c, i.__assign(i.__assign({}, n), {
prune: l
}));
if (!u.isSucc) return u;
r && (o.output = i.__assign(i.__assign({}, o.output), l.output));
}
return p.succ;
};
e.prototype._validateDateType = function(e) {
return e instanceof Date ? p.succ : p.error(o.TypeError, "Date", this._getTypeof(e));
};
e.prototype._validateNonNullableType = function(e, t, n) {
var r = this._getTypeof(e);
return "null" !== r && "undefined" !== r || "Any" === t.target.type ? this._validate(e, t.target, n) : p.error(o.TypeError, "NonNullable", r);
};
e.prototype._isNumberKey = function(e) {
var t = parseInt(e);
return !(isNaN(t) || "" + t !== e);
};
e.prototype._getTypeof = function(e) {
var t = typeof e;
return "object" === t ? null === e ? "null" : Array.isArray(e) ? s.SchemaType.Array : "Object" : t;
};
return e;
}();
n.ProtoHelper = a;
n.TSBufferValidator = d;
}, {
"k8w-extend-native": 270,
"tsbuffer-schema": 285,
tslib: 288
} ],
287: [ function(e, t, n) {
(function(t) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
e("k8w-extend-native");
for (var r = e("tslib"), o = e("tsbuffer-validator"), i = e("tsbuffer-schema"), s = function() {
function e() {}
e.bufferToBase64 = function(e) {
if ("undefined" != typeof t) return t.from(e).toString("base64");
for (var n = "", r = e.length, o = 0; o < r; o += 3) {
n += a[e[o] >> 2];
n += a[(3 & e[o]) << 4 | e[o + 1] >> 4];
n += a[(15 & e[o + 1]) << 2 | e[o + 2] >> 6];
n += a[63 & e[o + 2]];
}
r % 3 == 2 ? n = n.substring(0, n.length - 1) + "=" : r % 3 == 1 && (n = n.substring(0, n.length - 2) + "==");
return n;
};
e.base64ToBuffer = function(e) {
if ("undefined" != typeof t) return new Uint8Array(t.from(e, "base64"));
var n, r, o, i, s = .75 * e.length, a = e.length, l = 0;
if ("=" === e[e.length - 1]) {
s--;
"=" === e[e.length - 2] && s--;
}
for (var u = new Uint8Array(s), p = 0; p < a; p += 4) {
n = c[e.charCodeAt(p)];
r = c[e.charCodeAt(p + 1)];
o = c[e.charCodeAt(p + 2)];
i = c[e.charCodeAt(p + 3)];
u[l++] = n << 2 | r >> 4;
u[l++] = (15 & r) << 4 | o >> 2;
u[l++] = (3 & o) << 6 | 63 & i;
}
return u;
};
return e;
}(), a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", c = "undefined" == typeof Uint8Array ? [] : new Uint8Array(256), l = 0; l < a.length; l++) c[a.charCodeAt(l)] = l;
var u, p = function() {
function e() {}
e.isJsonCompatible = function(e, t, n) {
var r = this, o = e, s = "encode" === t ? "isJsonEncodable" : "isJsonDecodable";
if (void 0 === o[s]) switch (e.type) {
case i.SchemaType.Array:
o[s] = this.isJsonCompatible(e.elementType, t, n);
break;

case i.SchemaType.Tuple:
o[s] = e.elementTypes.every(function(e) {
return r.isJsonCompatible(e, t, n);
});
break;

case i.SchemaType.Interface:
var a = n.getFlatInterfaceSchema(e);
o[s] = a.properties.every(function(e) {
return r.isJsonCompatible(e.type, t, n);
});
a.indexSignature && (o[s] = o[s] && this.isJsonCompatible(a.indexSignature.type, t, n));
break;

case i.SchemaType.IndexedAccess:
case i.SchemaType.Reference:
var c = n.parseReference(e);
o[s] = this.isJsonCompatible(c, t, n);
break;

case i.SchemaType.Union:
case i.SchemaType.Intersection:
o[s] = e.members.every(function(e) {
return r.isJsonCompatible(e.type, t, n);
});
break;

case i.SchemaType.NonNullable:
o[s] = this.isJsonCompatible(e.target, t, n);
break;

case i.SchemaType.Pick:
case i.SchemaType.Partial:
case i.SchemaType.Omit:
case i.SchemaType.Overwrite:
c = n.parseMappedType(e);
o[s] = this.isJsonCompatible(c, t, n);
break;

case i.SchemaType.Custom:
case i.SchemaType.Date:
case i.SchemaType.Buffer:
o[s] = !1;
break;

default:
o[s] = !0;
}
return o[s];
};
return e;
}(), f = function() {
function e() {}
e.getPayloadLengthInfo = function(e, t) {
switch (e.type) {
case i.SchemaType.Boolean:
case i.SchemaType.Enum:
return {
lengthType: u.Varint
};

case i.SchemaType.Number:
return !e.scalarType || e.scalarType.includes("64") || "double" === e.scalarType ? {
lengthType: u.Bit64
} : e.scalarType && e.scalarType.startsWith("big") ? {
lengthType: u.LengthDelimited
} : {
lengthType: u.Varint
};

case i.SchemaType.Buffer:
case i.SchemaType.String:
case i.SchemaType.Any:
case i.SchemaType.Object:
return {
lengthType: u.LengthDelimited
};

case i.SchemaType.Interface:
case i.SchemaType.Pick:
case i.SchemaType.Partial:
case i.SchemaType.Omit:
case i.SchemaType.Union:
case i.SchemaType.Intersection:
return {
lengthType: u.IdBlock
};

case i.SchemaType.Array:
case i.SchemaType.Overwrite:
case i.SchemaType.Tuple:
return {
lengthType: u.LengthDelimited,
needLengthPrefix: !0
};

case i.SchemaType.Literal:
return {
lengthType: u.LengthDelimited,
needLengthPrefix: !1
};

case i.SchemaType.Date:
return {
lengthType: u.Varint
};

case i.SchemaType.NonNullable:
return this.getPayloadLengthInfo(t.parseReference(e.target), t);

case i.SchemaType.Custom:
return {
lengthType: u.LengthDelimited
};

default:
throw new Error("Unrecognized schema type: ".concat(e.type));
}
};
return e;
}();
(function(e) {
e[e.LengthDelimited = 0] = "LengthDelimited";
e[e.Varint = 1] = "Varint";
e[e.Bit64 = 2] = "Bit64";
e[e.IdBlock = 3] = "IdBlock";
})(u || (u = {}));
var d = function() {
function e() {}
e.canBeLiteral = function(e, t) {
var n = this;
return e.type === i.SchemaType.Union ? e.members.some(function(e) {
return n.canBeLiteral(e.type, t);
}) : e.type === i.SchemaType.Any || e.type === i.SchemaType.Literal && e.literal === t;
};
return e;
}(), h = {
Int8Array: Int8Array,
Int16Array: Int16Array,
Int32Array: Int32Array,
Uint8Array: Uint8Array,
Uint16Array: Uint16Array,
Uint32Array: Uint32Array,
Float32Array: Float32Array,
Float64Array: Float64Array
}, y = {
measureLength: function(e) {
for (var t = 0, n = 0, r = 0; r < e.length; ++r) if ((n = e.charCodeAt(r)) < 128) t += 1; else if (n < 2048) t += 2; else if (55296 == (64512 & n) && 56320 == (64512 & e.charCodeAt(r + 1))) {
++r;
t += 4;
} else t += 3;
return t;
},
write: function(e, t, n) {
for (var r, o, i = n, s = 0; s < e.length; ++s) if ((r = e.charCodeAt(s)) < 128) t[n++] = r; else if (r < 2048) {
t[n++] = r >> 6 | 192;
t[n++] = 63 & r | 128;
} else if (55296 == (64512 & r) && 56320 == (64512 & (o = e.charCodeAt(s + 1)))) {
r = 65536 + ((1023 & r) << 10) + (1023 & o);
++s;
t[n++] = r >> 18 | 240;
t[n++] = r >> 12 & 63 | 128;
t[n++] = r >> 6 & 63 | 128;
t[n++] = 63 & r | 128;
} else {
t[n++] = r >> 12 | 224;
t[n++] = r >> 6 & 63 | 128;
t[n++] = 63 & r | 128;
}
return n - i;
},
read: function(e, t, n) {
if (n < 1) return "";
for (var r = "", o = t, i = t + n; o < i; ) {
var s = e[o++];
if (s <= 127) r += String.fromCharCode(s); else if (s >= 192 && s < 224) r += String.fromCharCode((31 & s) << 6 | 63 & e[o++]); else if (s >= 224 && s < 240) r += String.fromCharCode((15 & s) << 12 | (63 & e[o++]) << 6 | 63 & e[o++]); else if (s >= 240) {
var a = ((7 & s) << 18 | (63 & e[o++]) << 12 | (63 & e[o++]) << 6 | 63 & e[o++]) - 65536;
r += String.fromCharCode(55296 + (a >> 10));
r += String.fromCharCode(56320 + (1023 & a));
}
}
return r;
}
}, v = {
measureLength: function(e) {
return t.byteLength(e, "utf-8");
},
write: function(e, n, r) {
return t.from(n.buffer, n.byteOffset, n.byteLength).write(e, r, "utf-8");
},
read: function(e, n, r) {
return t.from(e.buffer, e.byteOffset, e.byteLength).toString("utf-8", n, n + r);
}
}, g = "undefined" != typeof t && t.from && t.prototype.write ? v : y, _ = function() {
function e(e, t, n) {
this.uint32s = new Uint32Array([ e, t ]);
void 0 !== n && (this._byteLength = n);
}
e.from = function(t) {
if (0 === t) return this.Zero;
var n = t < 0;
n && (t = -t);
var r = t >>> 0, o = (t - r) / 4294967296 >>> 0;
if (n) {
o = ~o >>> 0;
r = ~r >>> 0;
if (++r > 4294967295) {
r = 0;
++o > 4294967295 && (o = 0);
}
}
return new e(o, r);
};
e.prototype.toNumber = function(e) {
if (!e && this.uint32s[0] >>> 31) {
var t = 1 + ~this.uint32s[1] >>> 0, n = ~this.uint32s[0] >>> 0;
t || (n = n + 1 >>> 0);
return -(t + 4294967296 * n);
}
return this.uint32s[1] + 4294967296 * this.uint32s[0];
};
e.prototype.zzEncode = function() {
var e = this.uint32s[0] >> 31;
this.uint32s[0] = ((this.uint32s[0] << 1 | this.uint32s[1] >>> 31) ^ e) >>> 0;
this.uint32s[1] = (this.uint32s[1] << 1 ^ e) >>> 0;
return this;
};
e.prototype.zzDecode = function() {
var e = -(1 & this.uint32s[1]);
this.uint32s[1] = ((this.uint32s[1] >>> 1 | this.uint32s[0] << 31) ^ e) >>> 0;
this.uint32s[0] = (this.uint32s[0] >>> 1 ^ e) >>> 0;
return this;
};
Object.defineProperty(e.prototype, "byteLength", {
get: function() {
if (void 0 === this._byteLength) {
var e = this.uint32s[1], t = (this.uint32s[1] >>> 28 | this.uint32s[0] << 4) >>> 0, n = this.uint32s[0] >>> 24;
this._byteLength = 0 === n ? 0 === t ? e < 16384 ? e < 128 ? 1 : 2 : e < 2097152 ? 3 : 4 : t < 16384 ? t < 128 ? 5 : 6 : t < 2097152 ? 7 : 8 : n < 128 ? 9 : 10;
}
return this._byteLength;
},
enumerable: !1,
configurable: !0
});
e.prototype.writeToBuffer = function(e, t) {
for (;this.uint32s[0]; ) {
e[t++] = 127 & this.uint32s[1] | 128;
this.uint32s[1] = (this.uint32s[1] >>> 7 | this.uint32s[0] << 25) >>> 0;
this.uint32s[0] >>>= 7;
}
for (;this.uint32s[1] > 127; ) {
e[t++] = 127 & this.uint32s[1] | 128;
this.uint32s[1] = this.uint32s[1] >>> 7;
}
e[t++] = this.uint32s[1];
return t;
};
e.readFromBuffer = function(t, n) {
var r = n, o = 0, i = 0, s = 0;
if (!(t.byteLength - n > 4)) {
for (;s < 3; ++s) {
if (n >= t.byteLength) throw new Error("Read varint error: index out of range");
i = (i | (127 & t[n]) << 7 * s) >>> 0;
if (t[n++] < 128) return new e(o, i, n - r);
}
return new e(o, i = (i | (127 & t[n++]) << 7 * s) >>> 0, n - r);
}
for (;s < 4; ++s) {
i = (i | (127 & t[n]) << 7 * s) >>> 0;
if (t[n++] < 128) return new e(o, i, n - r);
}
i = (i | (127 & t[n]) << 28) >>> 0;
o = (o | (127 & t[n]) >> 4) >>> 0;
if (t[n++] < 128) return new e(o, i, n - r);
s = 0;
if (t.byteLength - n > 4) for (;s < 5; ++s) {
o = (o | (127 & t[n]) << 7 * s + 3) >>> 0;
if (t[n++] < 128) return new e(o, i, n - r);
} else for (;s < 5; ++s) {
if (n >= t.byteLength) throw new Error("Read varint error: index out of range");
o = (o | (127 & t[n]) << 7 * s + 3) >>> 0;
if (t[n++] < 128) return new e(o, i, n - r);
}
throw Error("invalid varint encoding");
};
e.Zero = new e(0, 0);
return e;
}(), m = function() {
function e() {
this._pos = 0;
}
e.prototype.load = function(e, t) {
void 0 === t && (t = 0);
this._buf = e;
this._pos = t;
this._view = new DataView(e.buffer);
};
e.prototype.readVarint = function() {
var e = _.readFromBuffer(this._buf, this._pos);
this._pos += e.byteLength;
return e;
};
e.prototype.readUint = function() {
return this.readVarint().toNumber(!0);
};
e.prototype.readInt = function() {
return this.readVarint().zzDecode().toNumber();
};
e.prototype.readDouble = function() {
var e = this._pos;
this._pos += 8;
return this._view.getFloat64(this._buf.byteOffset + e);
};
e.prototype.readString = function() {
var e = this.readUint(), t = g.read(this._buf, this._pos, e);
this._pos += e;
return t;
};
e.prototype.readBuffer = function() {
var e = this.readUint(), t = this._buf.subarray(this._pos, this._pos + e);
this._pos += e;
return t;
};
e.prototype.skip = function(e) {
this._pos += e;
};
e.prototype.skipByLengthType = function(e) {
if (e === u.Bit64) this._pos += 8; else if (e === u.Varint) this.readVarint(); else if (e === u.LengthDelimited) {
var t = this.readUint();
this._pos += t;
} else {
if (e !== u.IdBlock) throw new Error("Unknown lengthType: " + e);
this.skipIdBlock();
}
};
e.prototype.skipIdBlock = function() {
for (var e = this.readUint(), t = 0; t < e; ++t) {
var n = 3 & this.readUint();
this.skipByLengthType(n);
}
};
e.prototype.readBoolean = function() {
var e = this._view.getUint8(this._buf.byteOffset + this._pos++);
if (255 === e) return !0;
if (0 === e) return !1;
throw new Error("Invalid boolean encoding [".concat(e, "] at pos ").concat(this._pos - 1));
};
Object.defineProperty(e.prototype, "unreadByteLength", {
get: function() {
return this._buf.byteLength - this._pos;
},
enumerable: !1,
configurable: !0
});
e.prototype.dispose = function() {
this._buf = this._view = void 0;
};
return e;
}(), b = function() {
function e(e) {
this._options = e;
this._reader = new m();
this._validator = e.validator;
}
e.prototype.decode = function(e, t) {
this._reader.load(e);
return this._read(t);
};
e.prototype.decodeJSON = function(e, t) {
var n = this;
if (null === e || p.isJsonCompatible(t, "decode", this._validator.protoHelper)) return e;
switch (t.type) {
case i.SchemaType.Array:
if (!Array.isArray(e)) break;
return e.map(function(e) {
return n.decodeJSON(e, t.elementType);
});

case i.SchemaType.Tuple:
if (!Array.isArray(e)) break;
return e.map(function(e, r) {
return n.decodeJSON(e, t.elementTypes[r]);
});

case i.SchemaType.Interface:
if (e.constructor !== Object) break;
e = Object.assign({}, e);
var r = this._validator.protoHelper.getFlatInterfaceSchema(t), o = function(t) {
var n = r.properties.find(function(e) {
return e.name === t;
});
n ? e[t] = a.decodeJSON(e[t], n.type) : r.indexSignature && (e[t] = a.decodeJSON(e[t], r.indexSignature.type));
}, a = this;
for (var c in e) o(c);
return e;

case i.SchemaType.Date:
if ("string" != typeof e && "number" != typeof e) break;
return new Date(e);

case i.SchemaType.Partial:
case i.SchemaType.Pick:
case i.SchemaType.Omit:
case i.SchemaType.Overwrite:
var l = this._validator.protoHelper.parseMappedType(t);
return this.decodeJSON(e, l);

case i.SchemaType.Buffer:
if ("string" != typeof e) break;
var u = s.base64ToBuffer(e);
return this._getBufferValue(u, t);

case i.SchemaType.IndexedAccess:
case i.SchemaType.Reference:
case i.SchemaType.Keyof:
return this.decodeJSON(e, this._validator.protoHelper.parseReference(t));

case i.SchemaType.Union:
case i.SchemaType.Intersection:
for (var f = 0, d = t.members; f < d.length; f++) {
var h = d[f];
e = this.decodeJSON(e, h.type);
}
return e;

case i.SchemaType.NonNullable:
return this.decodeJSON(e, t.target);

case i.SchemaType.Custom:
if (t.decodeJSON) return t.decodeJSON(e);
break;

default:
t.type;
}
return e;
};
e.prototype._read = function(e) {
switch (e.type) {
case i.SchemaType.Boolean:
return this._reader.readBoolean();

case i.SchemaType.Number:
return this._readNumber(e);

case i.SchemaType.String:
return this._reader.readString();

case i.SchemaType.Array:
for (var t = [], n = this._reader.readUint(), r = 0; r < n; ++r) {
var o = this._read(e.elementType);
t.push(o);
}
return t;

case i.SchemaType.Tuple:
if (e.elementTypes.length > 64) throw new Error("Elements oversized, maximum supported tuple elements is 64, now get " + e.elementTypes.length);
t = [];
var s = this._reader.readVarint(), a = [];
for (r = 0; r < 32; ++r) s.uint32s[1] & 1 << r && a.push(r);
for (r = 0; r < 32; ++r) s.uint32s[0] & 1 << r && a.push(r + 32);
if (!a.length) return [];
for (var c = a.last(), l = (r = 0, 0), u = a[0]; r <= c; ++r) if (r === u) {
t[r] = this._read(e.elementTypes[r]);
u = a[++l];
} else t[r] = void 0;
for (r = 0; r < e.elementTypes.length; ++r) this._undefinedAsNull(t[r], e.elementTypes[r], void 0 !== e.optionalStartIndex && r >= e.optionalStartIndex) && (t[r] = null);
return t;

case i.SchemaType.Enum:
var p = this._reader.readVarint().toNumber(), f = e.members.find(function(e) {
return e.id === p;
});
if (!f) throw new Error("Invalid enum encoding: unexpected id ".concat(p));
return f.value;

case i.SchemaType.Any:
case i.SchemaType.Object:
var d = this._reader.readString();
if ("undefined" === d) return;
return JSON.parse(d);

case i.SchemaType.Literal:
return e.literal;

case i.SchemaType.Interface:
return this._readInterface(e);

case i.SchemaType.Buffer:
var h = this._reader.readBuffer();
return this._getBufferValue(h, e);

case i.SchemaType.IndexedAccess:
case i.SchemaType.Reference:
case i.SchemaType.Keyof:
return this._read(this._validator.protoHelper.parseReference(e));

case i.SchemaType.Partial:
case i.SchemaType.Pick:
case i.SchemaType.Omit:
case i.SchemaType.Overwrite:
var y = this._validator.protoHelper.parseMappedType(e);
return y.type === i.SchemaType.Interface ? this._readPureMappedType(e) : this._readUnionOrIntersection(y);

case i.SchemaType.Union:
case i.SchemaType.Intersection:
return this._readUnionOrIntersection(e);

case i.SchemaType.Date:
return new Date(this._reader.readUint());

case i.SchemaType.NonNullable:
return this._read(e.target);

case i.SchemaType.Custom:
if (!e.decode) throw new Error("Missing decode method for CustomTypeSchema");
var v = this._reader.readBuffer();
return e.decode(v);

default:
throw new Error("Unrecognized schema type: ".concat(e.type));
}
};
e.prototype._readPureMappedType = function(e) {
var t, n;
"Overwrite" === e.type && (n = this._read(e.overwrite));
var r = this._validator.protoHelper.parseReference(e.target);
if ("Interface" === r.type) t = this._readInterface(r); else {
if ("Pick" !== r.type && "Omit" !== r.type && "Partial" !== r.type && "Overwrite" !== r.type) throw new Error("Invalid PureMappedType child: " + e.type);
t = this._readPureMappedType(r);
}
if ("Pick" === e.type) for (var o in t) -1 === e.keys.indexOf(o) && delete t[o]; else if ("Omit" === e.type) for (var o in t) e.keys.indexOf(o) > -1 && delete t[o]; else "Overwrite" === e.type && Object.assign(t, n);
return t;
};
e.prototype._readNumber = function(e) {
var t = e.scalarType || "double";
switch (t) {
case "double":
return this._reader.readDouble();

case "int":
return this._reader.readInt();

case "uint":
return this._reader.readUint();

default:
throw new Error("Scalar type not support : " + t);
}
};
e.prototype._readInterface = function(e) {
for (var t = {}, n = this._validator.protoHelper.getFlatInterfaceSchema(e), r = this._reader.readUint(), o = function() {
var r = i._reader.readUint(), o = 3 & r, s = r >> 2;
if (0 === s) if (n.indexSignature) {
var a = n.indexSignature.type, c = i._reader.readString();
i._skipIdLengthPrefix(i._validator.protoHelper.parseReference(a));
t[c] = i._read(a);
} else {
i._reader.skipByLengthType(u.LengthDelimited);
i._reader.skipByLengthType(o);
} else if (s <= 9) {
var l = s - 1, p = e.extends && e.extends.find(function(e) {
return e.id === l;
});
if (p) {
i._skipIdLengthPrefix(i._validator.protoHelper.parseReference(p.type));
var f = i._read(p.type);
Object.assign(t, f);
} else i._reader.skipByLengthType(o);
} else {
var d = s - 10, h = e.properties && e.properties.find(function(e) {
return e.id === d;
});
if (h) {
i._skipIdLengthPrefix(i._validator.protoHelper.parseReference(h.type));
t[h.name] = i._read(h.type);
} else i._reader.skipByLengthType(o);
}
}, i = this, s = 0; s < r; ++s) o();
for (var a = 0, c = n.properties; a < c.length; a++) {
var l = c[a];
if (!t.hasOwnProperty(l.name)) {
var p = this._validator.protoHelper.parseReference(l.type);
"Literal" !== p.type ? this._undefinedAsNull(t[l.name], p, l.optional) && (t[l.name] = null) : t[l.name] = p.literal;
}
}
return t;
};
e.prototype._undefinedAsNull = function(e, t, n) {
return void 0 === e && this._options.undefinedAsNull && !d.canBeLiteral(t, void 0) && !n && d.canBeLiteral(t, null);
};
e.prototype._skipIdLengthPrefix = function(e) {
f.getPayloadLengthInfo(e, this._validator.protoHelper).needLengthPrefix && this._reader.skipByLengthType(u.Varint);
};
e.prototype._readUnionOrIntersection = function(e) {
for (var t, n = this._reader.readUint(), r = function() {
var n = o._reader.readUint(), r = 3 & n, i = n >> 2, s = e.members.find(function(e) {
return e.id === i;
});
if (!s) {
o._reader.skipByLengthType(r);
return "continue";
}
o._skipIdLengthPrefix(o._validator.protoHelper.parseReference(s.type));
var a = o._read(s.type);
o._isObject(t) && o._isObject(a) ? Object.assign(t, a) : t = a;
}, o = this, i = 0; i < n; ++i) r();
this._undefinedAsNull(t, e) && (t = null);
return t;
};
e.prototype._isObject = function(e) {
return "object" == typeof e && null !== e;
};
e.prototype._getBufferValue = function(e, t) {
if (t.arrayType) {
if ("BigInt64Array" === t.arrayType || "BigUint64Array" === t.arrayType) throw new Error("Unsupported arrayType: " + t.arrayType);
if ("Uint8Array" === t.arrayType) return e;
var n = h[t.arrayType];
return e.byteOffset % n.BYTES_PER_ELEMENT == 0 ? new n(e.buffer, e.byteOffset, e.byteLength / n.BYTES_PER_ELEMENT) : new n(e.buffer.slice(e.byteOffset, e.byteOffset + e.byteLength));
}
return e.byteLength === e.buffer.byteLength && 0 === e.byteOffset ? e.buffer : e.buffer.slice(e.byteOffset, e.byteOffset + e.byteLength);
};
return e;
}(), w = 9, S = function() {
function e() {
this._ops = [];
}
Object.defineProperty(e.prototype, "ops", {
get: function() {
return this._ops;
},
enumerable: !1,
configurable: !0
});
e.prototype.clear = function() {
this._ops = [];
};
e.prototype.push = function(e) {
this._ops.push(this.req2op(e));
return this;
};
e.prototype.req2op = function(e) {
if ("string" === e.type || "buffer" === e.type) {
var t = this.measureLength(e);
this.push({
type: "varint",
value: _.from(t)
});
return r.__assign(r.__assign({}, e), {
length: t
});
}
var n = this.measureLength(e);
return r.__assign(r.__assign({}, e), {
length: n
});
};
e.prototype.measureLength = function(e) {
switch (e.type) {
case "varint":
return e.value.byteLength;

case "string":
return g.measureLength(e.value);

case "buffer":
return e.value.byteLength;

case "double":
return 8;

case "boolean":
return 1;

default:
return NaN;
}
};
e.prototype.finish = function() {
for (var e = this._ops.sum(function(e) {
return e.length;
}), t = 0, n = new Uint8Array(e), r = new DataView(n.buffer), o = 0, i = this._ops; o < i.length; o++) {
var s = i[o];
switch (s.type) {
case "varint":
var a = s.value.writeToBuffer(n, t);
if (a !== t + s.length) throw new Error("Error varint measuredLength ".concat(s.length, ", actual is ").concat(a - t, ", value is ").concat(s.value.toNumber()));
break;

case "double":
r.setFloat64(n.byteOffset + t, s.value);
break;

case "string":
var c = g.write(s.value, n, t);
if (c !== s.length) throw new Error("Expect ".concat(s.length, " bytes but encoded ").concat(c, " bytes"));
break;

case "buffer":
n.subarray(t, t + s.length).set(s.value);
break;

case "boolean":
r.setUint8(n.byteOffset + t, s.value ? 255 : 0);
}
t += s.length;
}
return n;
};
return e;
}(), I = function() {
function e(e) {
this._options = e;
this._writer = new S();
this._validator = e.validator;
}
e.prototype.encode = function(e, t) {
this._writer.clear();
this._write(e, t);
return this._writer.finish();
};
e.prototype.encodeJSON = function(e, t) {
var n = this;
if ("object" != typeof e || null === e || p.isJsonCompatible(t, "encode", this._validator.protoHelper)) return e;
switch (t.type) {
case i.SchemaType.Array:
if (!Array.isArray(e)) break;
return e.map(function(e) {
return n.encodeJSON(e, t.elementType);
});

case i.SchemaType.Tuple:
if (!Array.isArray(e)) break;
return e.map(function(e, r) {
return n.encodeJSON(e, t.elementTypes[r]);
});

case i.SchemaType.Interface:
if (e.constructor !== Object) break;
e = Object.assign({}, e);
var r = this._validator.protoHelper.getFlatInterfaceSchema(t), o = function(t) {
var n = r.properties.find(function(e) {
return e.name === t;
});
n ? e[t] = a.encodeJSON(e[t], n.type) : r.indexSignature && (e[t] = a.encodeJSON(e[t], r.indexSignature.type));
}, a = this;
for (var c in e) o(c);
return e;

case i.SchemaType.Partial:
case i.SchemaType.Pick:
case i.SchemaType.Omit:
case i.SchemaType.Overwrite:
var l = this._validator.protoHelper.parseMappedType(t);
return this.encodeJSON(e, l);

case i.SchemaType.Buffer:
if (!(e instanceof ArrayBuffer || ArrayBuffer.isView(e))) break;
if (t.arrayType) {
if ("Uint8Array" === t.arrayType) return s.bufferToBase64(e);
var u = e, f = u.byteLength === u.buffer.byteLength && 0 === u.byteOffset ? u.buffer : u.buffer.slice(u.byteOffset, u.byteOffset + u.byteLength);
return s.bufferToBase64(new Uint8Array(f));
}
return s.bufferToBase64(new Uint8Array(e));

case i.SchemaType.IndexedAccess:
case i.SchemaType.Reference:
case i.SchemaType.Keyof:
return this.encodeJSON(e, this._validator.protoHelper.parseReference(t));

case i.SchemaType.Union:
case i.SchemaType.Intersection:
for (var d = 0, h = t.members; d < h.length; d++) {
var y = h[d];
e = this.encodeJSON(e, y.type);
}
return e;

case i.SchemaType.NonNullable:
return this.encodeJSON(e, t.target);

case i.SchemaType.Date:
if (!(e instanceof Date)) break;
return e.toJSON();

case i.SchemaType.Custom:
return t.encodeJSON ? t.encodeJSON(e) : "function" == typeof (null == e ? void 0 : e.toJSON) ? e.toJSON() : "function" == typeof (null == e ? void 0 : e.toString) ? e.toString() : e;

default:
t.type;
}
return e;
};
e.prototype._write = function(e, t, n) {
switch (t.type) {
case i.SchemaType.Boolean:
this._writer.push({
type: "boolean",
value: e
});
break;

case i.SchemaType.Number:
this._writeNumber(e, t);
break;

case i.SchemaType.String:
this._writer.push({
type: "string",
value: e
});
break;

case i.SchemaType.Array:
var r = e;
this._writer.push({
type: "varint",
value: _.from(r.length)
});
for (var o = 0; o < r.length; ++o) this._write(r[o], t.elementType);
break;

case i.SchemaType.Tuple:
if (t.elementTypes.length > 64) throw new Error("Elements oversized, maximum supported tuple elements is 64, now get " + t.elementTypes.length);
r = e;
var s = [];
for (o = 0; o < r.length; ++o) void 0 === r[o] || this._nullAsUndefined(r[o], t.elementTypes[o]) || s.push(o);
for (var a = 0, c = 0, l = 0, u = s; l < u.length; l++) {
var p = u[l];
p < 32 ? a |= 1 << p : c |= 1 << p - 32;
}
this._writer.push({
type: "varint",
value: new _(c, a)
});
for (var f = 0, d = s; f < d.length; f++) {
o = d[f];
this._write(r[o], t.elementTypes[o]);
}
break;

case i.SchemaType.Enum:
var h = t.members.find(function(t) {
return t.value === e;
});
if (!h) throw new Error("Unexpect enum value: ".concat(e));
this._writer.push({
type: "varint",
value: _.from(h.id)
});
break;

case i.SchemaType.Any:
void 0 === e ? this._writer.push({
type: "string",
value: "undefined"
}) : this._writer.push({
type: "string",
value: JSON.stringify(e)
});
break;

case i.SchemaType.Object:
this._writer.push({
type: "string",
value: JSON.stringify(e)
});
break;

case i.SchemaType.Literal:
break;

case i.SchemaType.Interface:
this._writeInterface(e, t, n);
break;

case i.SchemaType.Buffer:
this._writeBuffer(e);
break;

case i.SchemaType.IndexedAccess:
case i.SchemaType.Reference:
case i.SchemaType.Keyof:
this._write(e, this._validator.protoHelper.parseReference(t), n);
break;

case i.SchemaType.Partial:
case i.SchemaType.Pick:
case i.SchemaType.Omit:
case i.SchemaType.Overwrite:
var y = this._validator.protoHelper.parseMappedType(t);
y.type === i.SchemaType.Interface ? this._writePureMappedType(e, t, n) : y.type === i.SchemaType.Union ? this._writeUnion(e, y, null == n ? void 0 : n.skipFields) : y.type === i.SchemaType.Intersection && this._writeIntersection(e, y, null == n ? void 0 : n.skipFields);
break;

case i.SchemaType.Union:
this._writeUnion(e, t, null == n ? void 0 : n.skipFields);
break;

case i.SchemaType.Intersection:
this._writeIntersection(e, t, null == n ? void 0 : n.skipFields);
break;

case i.SchemaType.Date:
this._writer.push({
type: "varint",
value: _.from(e.getTime())
});
break;

case i.SchemaType.NonNullable:
this._write(e, t.target, n);
break;

case i.SchemaType.Custom:
if (!t.encode) throw new Error("Missing encode method for CustomTypeSchema");
var v = t.encode(e);
this._writeBuffer(v);
break;

default:
throw new Error("Unrecognized schema type: ".concat(t.type));
}
};
e.prototype._writePureMappedType = function(e, t, n) {
n || (n = {});
if ("Pick" === t.type) if (n.pickFields) {
for (var r = {}, o = 0, i = t.keys; o < i.length; o++) {
var s = i[o];
n.pickFields[s] && (r[s] = 1);
}
n.pickFields = r;
} else {
n.pickFields = {};
for (var a = 0, c = t.keys; a < c.length; a++) {
s = c[a];
n.pickFields[s] = 1;
}
} else if ("Omit" === t.type) {
if (!(null == n ? void 0 : n.skipFields)) {
n || (n = {});
n.skipFields = {};
}
for (var l = 0, u = t.keys; l < u.length; l++) {
s = u[l];
n.skipFields[s] = 1;
}
} else if ("Overwrite" === t.type) {
var p = this._parseOverwrite(e, t);
this._write(p.overwriteValue, p.overwrite, n);
} else if ("Partial" !== t.type) throw new Error("Invalid PureMappedType child: " + t.type);
var f = this._validator.protoHelper.parseReference(t.target);
"Interface" === f.type ? this._writeInterface(e, f, n) : this._writePureMappedType(e, f, n);
};
e.prototype._writeNumber = function(e, t) {
var n = t.scalarType || "double";
switch (n) {
case "double":
this._writer.push({
type: n,
value: e
});
break;

case "int":
this._writer.push({
type: "varint",
value: _.from(e).zzEncode()
});
break;

case "uint":
this._writer.push({
type: "varint",
value: _.from(e)
});
break;

default:
throw new Error("Scalar type not support : " + n);
}
};
e.prototype._writeInterface = function(e, t, n) {
n || (n = {});
n.skipFields || (n.skipFields = {});
var o = this._writer.ops.length, i = 0;
if (t.extends) {
if (t.extends.length > w) throw new Error("Max support ".concat(w, " extends, actual: ").concat(t.extends.length));
for (var s = 0, a = t.extends; s < a.length; s++) {
var c = a[s], l = c.id + 1;
this._writer.push({
type: "varint",
value: _.from(l)
});
var u = this._writer.ops.length - 1, p = this._writer.ops.length, f = this._validator.protoHelper.parseReference(c.type);
this._writeInterface(e, f, r.__assign(r.__assign({}, n), {
skipIndexSignature: !!t.indexSignature || n.skipIndexSignature
}));
if (this._writer.ops.length === p + 1) this._writer.ops.splice(this._writer.ops.length - 2, 2); else {
++i;
this._processIdWithLengthType(u, c.type);
}
}
}
if (t.properties) for (var d = 0, h = t.properties; d < h.length; d++) {
var y = h[d], v = this._validator.protoHelper.parseReference(y.type), g = e[y.name];
if (!n.pickFields || n.pickFields[y.name]) if ("Literal" !== v.type) {
this._nullAsUndefined(g, y.type) && (g = void 0);
if (void 0 !== g && !n.skipFields[y.name]) {
n.skipFields[y.name] = 1;
l = y.id + w + 1;
this._writer.push({
type: "varint",
value: _.from(l)
});
u = this._writer.ops.length - 1;
this._write(g, v);
++i;
this._processIdWithLengthType(u, v);
}
} else n.skipFields[y.name] = 1;
}
if (!n.skipIndexSignature) {
var m = this._validator.protoHelper.getFlatInterfaceSchema(t);
if (m.indexSignature) for (var b in e) if (void 0 !== e[b] && !this._nullAsUndefined(e[b], m.indexSignature.type) && (!n.pickFields || n.pickFields[b]) && !n.skipFields[b]) {
n.skipFields[b] = 1;
this._writer.push({
type: "varint",
value: _.from(0)
});
u = this._writer.ops.length - 1;
this._writer.push({
type: "string",
value: b
});
var S = this._writer.ops.length;
this._write(e[b], m.indexSignature.type);
++i;
this._processIdWithLengthType(u, m.indexSignature.type, S);
}
}
this._writer.ops.splice(o, 0, this._writer.req2op({
type: "varint",
value: _.from(i)
}));
};
e.prototype._nullAsUndefined = function(e, t) {
return null === e && this._options.nullAsUndefined && !d.canBeLiteral(t, null);
};
e.prototype._parseOverwrite = function(e, t) {
var n, r, o = {}, i = this._validator.protoHelper.parseReference(t.target), s = this._validator.protoHelper.parseReference(t.overwrite), a = this._validator.protoHelper.getFlatInterfaceSchema(i), c = this._validator.protoHelper.getFlatInterfaceSchema(s), l = {}, u = {};
if (c.properties) for (var p = 0, f = c.properties; p < f.length; p++) if (void 0 !== e[(y = f[p]).name] && !o[y.name]) {
l[y.name] = e[y.name];
o[y.name] = 1;
}
if (a.properties) for (var d = 0, h = a.properties; d < h.length; d++) {
var y;
if (void 0 !== e[(y = h[d]).name] && !o[y.name]) {
u[y.name] = e[y.name];
o[y.name] = 1;
}
}
if (c.indexSignature) {
r = c.indexSignature;
n = l;
} else if (a.indexSignature) {
r = a.indexSignature;
n = u;
}
if (r) for (var v in e) if (!o[v]) {
n[v] = e[v];
o[v] = 1;
}
return {
target: i,
targetValue: u,
overwrite: s,
overwriteValue: l
};
};
e.prototype._writeUnion = function(e, t, n) {
void 0 === n && (n = {});
var r = this._writer.ops.length, o = 0;
this._nullAsUndefined(e, t) && (e = void 0);
for (var i = 0, s = t.members; i < s.length; i++) {
var a = s[i];
if (this._validator.validate(e, a.type, {
excessPropertyChecks: !1
}).isSucc) {
this._writer.push({
type: "varint",
value: _.from(a.id)
});
var c = this._writer.ops.length - 1;
"Union" === a.type.type ? this._writeUnion(e, a.type, n) : this._write(e, a.type, {
skipFields: n
});
o++;
this._processIdWithLengthType(c, a.type);
if ("object" != typeof e) break;
}
}
if (!(o > 0)) throw new Error("Non member is satisfied for union type");
this._writer.ops.splice(r, 0, this._writer.req2op({
type: "varint",
value: _.from(o)
}));
};
e.prototype._writeIntersection = function(e, t, n) {
void 0 === n && (n = {});
this._writer.push({
type: "varint",
value: _.from(t.members.length)
});
for (var r = 0, o = t.members; r < o.length; r++) {
var i = o[r];
this._writer.push({
type: "varint",
value: _.from(i.id)
});
var s = this._writer.ops.length - 1;
this._write(e, i.type, {
skipFields: n
});
this._processIdWithLengthType(s, i.type);
}
};
e.prototype._writeBuffer = function(e) {
if (e instanceof ArrayBuffer) this._writer.push({
type: "buffer",
value: new Uint8Array(e)
}); else if (e instanceof Uint8Array) this._writer.push({
type: "buffer",
value: e
}); else {
var t = e.constructor.name, n = h[t], r = new Uint8Array(e.buffer, e.byteOffset, e.length * n.BYTES_PER_ELEMENT);
this._writer.push({
type: "buffer",
value: r
});
}
};
e.prototype._processIdWithLengthType = function(e, t, n) {
var r = this._writer.ops[e];
if ("varint" !== r.type) throw new Error("Error idPos: " + e);
var o = this._validator.protoHelper.parseReference(t), i = f.getPayloadLengthInfo(o, this._validator.protoHelper), s = (r.value.toNumber() << 2) + i.lengthType;
this._writer.ops[e] = this._writer.req2op({
type: "varint",
value: _.from(s)
});
if (i.needLengthPrefix) {
var a = this._writer.ops.filter(function(t, n) {
return n > e;
}).sum(function(e) {
return e.length;
});
this._writer.ops.splice(null == n ? e + 1 : n, 0, this._writer.req2op({
type: "varint",
value: _.from(a)
}));
}
};
return e;
}(), O = function() {
function e(e, t) {
this._options = {
excessPropertyChecks: !0,
strictNullChecks: !1,
skipEncodeValidate: !1,
skipDecodeValidate: !1,
cloneProto: !0
};
this._options = r.__assign(r.__assign({}, this._options), t);
this._proto = this._options.cloneProto ? Object.merge({}, e) : e;
Object.assign(this._proto, Object.merge({}, null == t ? void 0 : t.customTypes));
this._validator = new o.TSBufferValidator(this._proto, {
excessPropertyChecks: this._options.excessPropertyChecks,
strictNullChecks: this._options.strictNullChecks,
cloneProto: !1
});
this.validate = this._validator.validate.bind(this._validator);
this.prune = this._validator.prune.bind(this._validator);
this._encoder = new I({
validator: this._validator,
nullAsUndefined: !this._options.strictNullChecks
});
this._decoder = new b({
validator: this._validator,
undefinedAsNull: !this._options.strictNullChecks
});
}
e.prototype.encode = function(e, t, n) {
var r, o, i;
if ("string" == typeof t) {
if (!(o = this._proto[t])) return {
isSucc: !1,
errMsg: "Cannot find schema： ".concat(t)
};
} else o = t;
if (!(null !== (r = null == n ? void 0 : n.skipValidate) && void 0 !== r ? r : this._options.skipEncodeValidate)) {
var s = this._validator.validate(e, o, {
excessPropertyChecks: !1
});
if (!s.isSucc) return s;
}
try {
i = this._encoder.encode(e, o);
} catch (e) {
return {
isSucc: !1,
errMsg: e.message
};
}
return {
isSucc: !0,
buf: i
};
};
e.prototype.decode = function(e, t, n) {
var r, o, i;
if ("string" == typeof t) {
if (!(o = this._proto[t])) return {
isSucc: !1,
errMsg: "Cannot find schema： ".concat(t)
};
} else o = t;
try {
i = this._decoder.decode(e, o);
} catch (e) {
return {
isSucc: !1,
errMsg: e.message
};
}
if (!(null !== (r = null == n ? void 0 : n.skipValidate) && void 0 !== r ? r : this._options.skipDecodeValidate)) {
var s = this._validator.validate(i, o);
if (!s.isSucc) return s;
}
return {
isSucc: !0,
value: i
};
};
e.prototype.encodeJSON = function(e, t, n) {
var r, o, i;
if ("string" == typeof t) {
if (!(o = this._proto[t])) return {
isSucc: !1,
errMsg: "Cannot find schema： ".concat(t)
};
} else o = t;
if (!(null !== (r = null == n ? void 0 : n.skipValidate) && void 0 !== r ? r : this._options.skipEncodeValidate)) {
var s = this._validator.prune(e, o);
if (!s.isSucc) return s;
e = s.pruneOutput;
}
try {
i = this._encoder.encodeJSON(e, o);
} catch (e) {
return {
isSucc: !1,
errMsg: e.message
};
}
return {
isSucc: !0,
json: i
};
};
e.prototype.decodeJSON = function(e, t, n) {
var r, o, i;
if ("string" == typeof t) {
if (!(o = this._proto[t])) return {
isSucc: !1,
errMsg: "Cannot find schema： ".concat(t)
};
} else o = t;
try {
i = this._decoder.decodeJSON(e, o);
} catch (e) {
return {
isSucc: !1,
errMsg: e.message
};
}
if (!(null !== (r = null == n ? void 0 : n.skipValidate) && void 0 !== r ? r : this._options.skipDecodeValidate)) {
var s = this._validator.prune(i, o);
return s.isSucc ? {
isSucc: !0,
value: s.pruneOutput
} : s;
}
return {
isSucc: !0,
value: i
};
};
return e;
}();
n.Base64Util = s;
n.TSBuffer = O;
}).call(this, e("buffer").Buffer);
}, {
buffer: 2,
"k8w-extend-native": 270,
"tsbuffer-schema": 285,
"tsbuffer-validator": 286,
tslib: 288
} ],
288: [ function(e, t) {
(function(e) {
var n, r, o, i, s, a, c, l, u, p, f, d, h, y, v, g, _, m, b, w, S, I, O, j, P;
(function(n) {
var r = "object" == typeof e ? e : "object" == typeof self ? self : "object" == typeof this ? this : {};
"function" == typeof define && define.amd ? define("tslib", [ "exports" ], function(e) {
n(o(r, o(e)));
}) : "object" == typeof t && "object" == typeof t.exports ? n(o(r, o(t.exports))) : n(o(r));
function o(e, t) {
e !== r && ("function" == typeof Object.create ? Object.defineProperty(e, "__esModule", {
value: !0
}) : e.__esModule = !0);
return function(n, r) {
return e[n] = t ? t(n, r) : r;
};
}
})(function(e) {
var t = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
};
n = function(e, n) {
if ("function" != typeof n && null !== n) throw new TypeError("Class extends value " + String(n) + " is not a constructor or null");
t(e, n);
function r() {
this.constructor = e;
}
e.prototype = null === n ? Object.create(n) : (r.prototype = n.prototype, new r());
};
r = Object.assign || function(e) {
for (var t, n = 1, r = arguments.length; n < r; n++) {
t = arguments[n];
for (var o in t) Object.prototype.hasOwnProperty.call(t, o) && (e[o] = t[o]);
}
return e;
};
o = function(e, t) {
var n = {};
for (var r in e) Object.prototype.hasOwnProperty.call(e, r) && t.indexOf(r) < 0 && (n[r] = e[r]);
if (null != e && "function" == typeof Object.getOwnPropertySymbols) {
var o = 0;
for (r = Object.getOwnPropertySymbols(e); o < r.length; o++) t.indexOf(r[o]) < 0 && Object.prototype.propertyIsEnumerable.call(e, r[o]) && (n[r[o]] = e[r[o]]);
}
return n;
};
i = function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
s = function(e, t) {
return function(n, r) {
t(n, r, e);
};
};
a = function(e, t) {
if ("object" == typeof Reflect && "function" == typeof Reflect.metadata) return Reflect.metadata(e, t);
};
c = function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
};
l = function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
u = function(e, t) {
for (var n in e) "default" === n || Object.prototype.hasOwnProperty.call(t, n) || P(t, e, n);
};
P = Object.create ? function(e, t, n, r) {
void 0 === r && (r = n);
var o = Object.getOwnPropertyDescriptor(t, n);
o && ("get" in o ? t.__esModule : !o.writable && !o.configurable) || (o = {
enumerable: !0,
get: function() {
return t[n];
}
});
Object.defineProperty(e, r, o);
} : function(e, t, n, r) {
void 0 === r && (r = n);
e[r] = t[n];
};
p = function(e) {
var t = "function" == typeof Symbol && Symbol.iterator, n = t && e[t], r = 0;
if (n) return n.call(e);
if (e && "number" == typeof e.length) return {
next: function() {
e && r >= e.length && (e = void 0);
return {
value: e && e[r++],
done: !e
};
}
};
throw new TypeError(t ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
f = function(e, t) {
var n = "function" == typeof Symbol && e[Symbol.iterator];
if (!n) return e;
var r, o, i = n.call(e), s = [];
try {
for (;(void 0 === t || t-- > 0) && !(r = i.next()).done; ) s.push(r.value);
} catch (e) {
o = {
error: e
};
} finally {
try {
r && !r.done && (n = i.return) && n.call(i);
} finally {
if (o) throw o.error;
}
}
return s;
};
d = function() {
for (var e = [], t = 0; t < arguments.length; t++) e = e.concat(f(arguments[t]));
return e;
};
h = function() {
for (var e = 0, t = 0, n = arguments.length; t < n; t++) e += arguments[t].length;
var r = Array(e), o = 0;
for (t = 0; t < n; t++) for (var i = arguments[t], s = 0, a = i.length; s < a; s++, 
o++) r[o] = i[s];
return r;
};
y = function(e, t, n) {
if (n || 2 === arguments.length) for (var r, o = 0, i = t.length; o < i; o++) if (r || !(o in t)) {
r || (r = Array.prototype.slice.call(t, 0, o));
r[o] = t[o];
}
return e.concat(r || Array.prototype.slice.call(t));
};
v = function(e) {
return this instanceof v ? (this.v = e, this) : new v(e);
};
g = function(e, t, n) {
if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
var r, o = n.apply(e, t || []), i = [];
return r = {}, s("next"), s("throw"), s("return"), r[Symbol.asyncIterator] = function() {
return this;
}, r;
function s(e) {
o[e] && (r[e] = function(t) {
return new Promise(function(n, r) {
i.push([ e, t, n, r ]) > 1 || a(e, t);
});
});
}
function a(e, t) {
try {
(n = o[e](t)).value instanceof v ? Promise.resolve(n.value.v).then(c, l) : u(i[0][2], n);
} catch (e) {
u(i[0][3], e);
}
var n;
}
function c(e) {
a("next", e);
}
function l(e) {
a("throw", e);
}
function u(e, t) {
(e(t), i.shift(), i.length) && a(i[0][0], i[0][1]);
}
};
_ = function(e) {
var t, n;
return t = {}, r("next"), r("throw", function(e) {
throw e;
}), r("return"), t[Symbol.iterator] = function() {
return this;
}, t;
function r(r, o) {
t[r] = e[r] ? function(t) {
return (n = !n) ? {
value: v(e[r](t)),
done: "return" === r
} : o ? o(t) : t;
} : o;
}
};
m = function(e) {
if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
var t, n = e[Symbol.asyncIterator];
return n ? n.call(e) : (e = "function" == typeof p ? p(e) : e[Symbol.iterator](), 
t = {}, r("next"), r("throw"), r("return"), t[Symbol.asyncIterator] = function() {
return this;
}, t);
function r(n) {
t[n] = e[n] && function(t) {
return new Promise(function(r, i) {
o(r, i, (t = e[n](t)).done, t.value);
});
};
}
function o(e, t, n, r) {
Promise.resolve(r).then(function(t) {
e({
value: t,
done: n
});
}, t);
}
};
b = function(e, t) {
Object.defineProperty ? Object.defineProperty(e, "raw", {
value: t
}) : e.raw = t;
return e;
};
var C = Object.create ? function(e, t) {
Object.defineProperty(e, "default", {
enumerable: !0,
value: t
});
} : function(e, t) {
e.default = t;
};
w = function(e) {
if (e && e.__esModule) return e;
var t = {};
if (null != e) for (var n in e) "default" !== n && Object.prototype.hasOwnProperty.call(e, n) && P(t, e, n);
C(t, e);
return t;
};
S = function(e) {
return e && e.__esModule ? e : {
default: e
};
};
I = function(e, t, n, r) {
if ("a" === n && !r) throw new TypeError("Private accessor was defined without a getter");
if ("function" == typeof t ? e !== t || !r : !t.has(e)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
return "m" === n ? r : "a" === n ? r.call(e) : r ? r.value : t.get(e);
};
O = function(e, t, n, r, o) {
if ("m" === r) throw new TypeError("Private method is not writable");
if ("a" === r && !o) throw new TypeError("Private accessor was defined without a setter");
if ("function" == typeof t ? e !== t || !o : !t.has(e)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
return "a" === r ? o.call(e, n) : o ? o.value = n : t.set(e, n), n;
};
j = function(e, t) {
if (null === t || "object" != typeof t && "function" != typeof t) throw new TypeError("Cannot use 'in' operator on non-object");
return "function" == typeof e ? t === e : e.has(t);
};
e("__extends", n);
e("__assign", r);
e("__rest", o);
e("__decorate", i);
e("__param", s);
e("__metadata", a);
e("__awaiter", c);
e("__generator", l);
e("__exportStar", u);
e("__createBinding", P);
e("__values", p);
e("__read", f);
e("__spread", d);
e("__spreadArrays", h);
e("__spreadArray", y);
e("__await", v);
e("__asyncGenerator", g);
e("__asyncDelegator", _);
e("__asyncValues", m);
e("__makeTemplateObject", b);
e("__importStar", w);
e("__importDefault", S);
e("__classPrivateFieldGet", I);
e("__classPrivateFieldSet", O);
e("__classPrivateFieldIn", j);
});
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {} ],
289: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
e("k8w-extend-native");
var r = e("tslib"), o = e("tsbuffer"), i = e("tsrpc-proto"), s = e("tsbuffer-schema"), a = function() {
function e(e, t) {
void 0 === e && (e = 1);
void 0 === t && (t = Number.MAX_SAFE_INTEGER);
this._min = e;
this._max = t;
this._last = t;
}
e.prototype.reset = function() {
this._last = this._max;
};
e.prototype.getNext = function(e) {
return this._last >= this._max ? this._last = this._min : e ? this._last : ++this._last;
};
Object.defineProperty(e.prototype, "last", {
get: function() {
return this._last;
},
enumerable: !1,
configurable: !0
});
return e;
}(), c = function() {
function e() {
this.nodes = [];
this.onError = function(e, t, n, r) {
null == r || r.error("Uncaught FlowError:", e);
};
}
e.prototype.exec = function(e, t) {
return r.__awaiter(this, void 0, void 0, function() {
var n, o, i;
return r.__generator(this, function(r) {
switch (r.label) {
case 0:
n = e;
o = 0;
r.label = 1;

case 1:
if (!(o < this.nodes.length)) return [ 3, 7 ];
r.label = 2;

case 2:
r.trys.push([ 2, 4, , 5 ]);
return [ 4, this.nodes[o](n) ];

case 3:
n = r.sent();
return [ 3, 5 ];

case 4:
i = r.sent();
this.onError(i, n, e, t);
return [ 2, void 0 ];

case 5:
if (null == n) return [ 2, n ];
r.label = 6;

case 6:
++o;
return [ 3, 1 ];

case 7:
return [ 2, n ];
}
});
});
};
e.prototype.push = function(e) {
this.nodes.push(e);
return e;
};
e.prototype.remove = function(e) {
return this.nodes.remove(function(t) {
return t === e;
});
};
return e;
}();
function l(e) {
var t = {};
e === String ? t["?mongodb/ObjectId"] = {
type: s.SchemaType.Custom,
validate: function(e) {
return "string" != typeof e ? {
isSucc: !1,
errMsg: "Expected type to be `string`, actually `".concat(typeof e, "`.")
} : /[0-9a-fA-F]{24}/.test(e) ? {
isSucc: !0
} : {
isSucc: !1,
errMsg: "ObjectId must be a string of 24 hex characters"
};
},
encode: function(e) {
return new Uint8Array(Array.from({
length: 12
}, function(t, n) {
return Number.parseInt("0x" + e.substr(2 * n, 2));
}));
},
decode: function(e) {
return Array.from(e, function(e) {
var t = e.toString(16);
1 === t.length && (t = "0" + t);
return t;
}).join("");
}
} : t["?mongodb/ObjectId"] = {
type: s.SchemaType.Custom,
validate: function(t) {
return t instanceof e ? {
isSucc: !0
} : {
isSucc: !1,
errMsg: "Expected to be instance of `ObjectId`, actually not."
};
},
encode: function(e) {
return new Uint8Array(e.id);
},
decode: function(t) {
return new e(t);
},
decodeJSON: function(t) {
return new e(t);
}
};
t["?mongodb/ObjectID"] = t["?mongodb/ObjectId"];
t["?bson/ObjectId"] = t["?mongodb/ObjectId"];
t["?bson/ObjectID"] = t["?mongodb/ObjectId"];
return t;
}
var u = function() {
function e() {
this._handlers = {};
}
e.prototype.forEachHandler = function(e, t) {
for (var n = [], r = 2; r < arguments.length; r++) n[r - 2] = arguments[r];
var o = this._handlers[e];
if (!o) return [];
for (var i = [], s = 0, a = o; s < a.length; s++) {
var c = a[s];
try {
i.push(c.apply(void 0, n));
} catch (e) {
null == t || t.error("[MsgHandlerError]", e);
}
}
return i;
};
e.prototype.addHandler = function(e, t) {
var n = this._handlers[e];
if (n) {
if (n.some(function(e) {
return e === t;
})) return;
} else n = this._handlers[e] = [];
n.push(t);
};
e.prototype.removeHandler = function(e, t) {
var n = this._handlers[e];
n && n.removeOne(function(e) {
return e === t;
});
};
e.prototype.removeAllHandlers = function(e) {
this._handlers[e] = void 0;
};
return e;
}(), p = function() {
function e() {}
e.getServiceMap = function(e) {
for (var t = {
id2Service: {},
apiName2Service: {},
msgName2Service: {}
}, n = 0, o = e.services; n < o.length; n++) {
var i = o[n], s = i.name.match(/(.+\/)?([^\/]+)$/), a = s[1] || "", c = s[2];
if ("api" === i.type) {
var l = r.__assign(r.__assign({}, i), {
reqSchemaId: "".concat(a, "Ptl").concat(c, "/Req").concat(c),
resSchemaId: "".concat(a, "Ptl").concat(c, "/Res").concat(c)
});
t.apiName2Service[i.name] = l;
t.id2Service[i.id] = l;
} else {
l = r.__assign(r.__assign({}, i), {
msgSchemaId: "".concat(a, "Msg").concat(c, "/Msg").concat(c)
});
t.msgName2Service[i.name] = l;
t.id2Service[i.id] = l;
}
}
return t;
};
return e;
}(), f = function() {
function e() {}
Object.defineProperty(e, "tsbuffer", {
get: function() {
this._tsbuffer || (this._tsbuffer = new o.TSBuffer(i.TransportDataProto));
return this._tsbuffer;
},
enumerable: !1,
configurable: !0
});
e.encodeClientMsg = function(e, t, n, r, o) {
if ("buffer" === r) {
if (!(a = e.encode(n, t.msgSchemaId)).isSucc) return a;
var i = {
serviceId: t.id,
buffer: a.buf
}, s = this.tsbuffer.encode(i, "ServerInputData");
return s.isSucc ? {
isSucc: !0,
output: s.buf
} : {
isSucc: !1,
errMsg: s.errMsg
};
}
var a;
if (!(a = e.encodeJSON(n, t.msgSchemaId)).isSucc) return a;
var c = "SHORT" === o ? a.json : [ t.name, a.json ];
return {
isSucc: !0,
output: "json" === r ? c : JSON.stringify(c)
};
};
e.encodeApiReq = function(e, t, n, r, o) {
if ("buffer" === r) {
if (!(a = e.encode(n, t.reqSchemaId)).isSucc) return a;
var i = {
serviceId: t.id,
buffer: a.buf,
sn: o
}, s = this.tsbuffer.encode(i, "ServerInputData");
return s.isSucc ? {
isSucc: !0,
output: s.buf
} : {
isSucc: !1,
errMsg: s.errMsg
};
}
var a;
if (!(a = e.encodeJSON(n, t.reqSchemaId)).isSucc) return a;
var c = void 0 === o ? a.json : [ t.name, a.json, o ];
return {
isSucc: !0,
output: "json" === r ? c : JSON.stringify(c)
};
};
e.encodeServerMsg = function(e, t, n, r, o) {
if ("buffer" === r) {
if (!(a = e.encode(n, t.msgSchemaId)).isSucc) return a;
var i = {
serviceId: t.id,
buffer: a.buf
}, s = this.tsbuffer.encode(i, "ServerOutputData");
return s.isSucc ? {
isSucc: !0,
output: s.buf
} : {
isSucc: !1,
errMsg: s.errMsg
};
}
var a;
if (!(a = e.encodeJSON(n, t.msgSchemaId)).isSucc) return a;
var c = "SHORT" === o ? a.json : [ t.name, a.json ];
return {
isSucc: !0,
output: "json" === r ? c : JSON.stringify(c)
};
};
e.parseServerOutout = function(e, t, n, r) {
var o;
if (n instanceof Uint8Array) {
var s = this.tsbuffer.decode(n, "ServerOutputData");
if (!s.isSucc) return s;
var a = s.value;
if (void 0 === (r = null != r ? r : a.serviceId)) return {
isSucc: !1,
errMsg: "Missing 'serviceId' in ServerOutput"
};
if (!(d = t.id2Service[r])) return {
isSucc: !1,
errMsg: "Invalid service ID: ".concat(r, " (from ServerOutput)")
};
if ("msg" === d.type) {
if (!a.buffer) return {
isSucc: !1,
errMsg: "Empty msg buffer (from ServerOutput)"
};
var c = e.decode(a.buffer, d.msgSchemaId);
return c.isSucc ? {
isSucc: !0,
result: {
type: "msg",
service: d,
msg: c.value
}
} : c;
}
if (a.error) return {
isSucc: !0,
result: {
type: "api",
service: d,
sn: a.sn,
ret: {
isSucc: !1,
err: new i.TsrpcError(a.error)
}
}
};
if (!a.buffer) return {
isSucc: !1,
errMsg: "Empty API res buffer (from ServerOutput)"
};
var l = e.decode(a.buffer, d.resSchemaId);
return l.isSucc ? {
isSucc: !0,
result: {
type: "api",
service: d,
sn: a.sn,
ret: {
isSucc: !0,
res: l.value
}
}
} : l;
}
var u = void 0;
if ("string" == typeof n) try {
u = JSON.parse(n);
} catch (e) {
return {
isSucc: !1,
errMsg: "Invalid input JSON: ".concat(e.message)
};
} else u = n;
var p = void 0, f = void 0, d = void 0;
if (null == r) {
if (!Array.isArray(u)) return {
isSucc: !1,
errMsg: "Invalid server output format"
};
var h = u[0];
if (!(d = null !== (o = t.apiName2Service[h]) && void 0 !== o ? o : t.msgName2Service[h])) return {
isSucc: !1,
errMsg: "Invalid service name: ".concat(h, " (from ServerOutputData)")
};
p = u[1];
f = u[2];
} else {
if (!(d = t.id2Service[r])) return {
isSucc: !1,
errMsg: "Invalid service ID: ".concat(r)
};
p = u;
}
if ("api" === d.type) {
if (p.isSucc && "res" in p) {
var y;
if (!(y = e.decodeJSON(p.res, d.resSchemaId)).isSucc) return y;
p.res = y.value;
} else {
if (!p.err) return {
isSucc: !1,
errMsg: "Invalid server output format"
};
p.err = new i.TsrpcError(p.err);
}
return {
isSucc: !0,
result: {
type: "api",
service: d,
sn: f,
ret: p
}
};
}
return (y = e.decodeJSON(p, d.msgSchemaId)).isSucc ? {
isSucc: !0,
result: {
type: "msg",
service: d,
msg: y.value
}
} : y;
};
e.HeartbeatPacket = new Uint8Array([ 0 ]);
return e;
}(), d = function() {
function e(e, t) {
this._msgHandlers = new u();
this.flows = {
preCallApiFlow: new c(),
preApiReturnFlow: new c(),
postApiReturnFlow: new c(),
preSendMsgFlow: new c(),
postSendMsgFlow: new c(),
preRecvMsgFlow: new c(),
postRecvMsgFlow: new c(),
preSendDataFlow: new c(),
preRecvDataFlow: new c(),
preSendBufferFlow: new c(),
preRecvBufferFlow: new c(),
preConnectFlow: new c(),
postConnectFlow: new c(),
postDisconnectFlow: new c()
};
this._apiSnCounter = new a(1);
this._pendingApis = [];
this._onRecvBuf = this._onRecvData;
this.options = t;
this.serviceMap = p.getServiceMap(e);
this.dataType = this.options.json ? "text" : "buffer";
var n = r.__assign({}, e.types);
t.customObjectIdClass && (n = r.__assign(r.__assign({}, n), l(t.customObjectIdClass)));
this.tsbuffer = new o.TSBuffer(n);
this.logger = this.options.logger;
this.logger && i.setLogLevel(this.logger, this.options.logLevel);
}
Object.defineProperty(e.prototype, "lastSN", {
get: function() {
return this._apiSnCounter.last;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "nextSN", {
get: function() {
return this._apiSnCounter.getNext(!0);
},
enumerable: !1,
configurable: !0
});
e.prototype.callApi = function(e, t, n) {
void 0 === n && (n = {});
return r.__awaiter(this, void 0, void 0, function() {
var o, s, a, c = this;
return r.__generator(this, function() {
o = this._apiSnCounter.getNext();
s = {
sn: o,
abortKey: n.abortKey,
service: this.serviceMap.apiName2Service[e]
};
this._pendingApis.push(s);
(a = new Promise(function(o) {
return r.__awaiter(c, void 0, void 0, function() {
var a, c, l, u, p;
return r.__generator(this, function(f) {
switch (f.label) {
case 0:
return [ 4, this.flows.preCallApiFlow.exec({
apiName: e,
req: t,
options: n
}, this.logger) ];

case 1:
if (!(a = f.sent()) || s.isAborted) {
this.abort(s.sn);
return [ 2 ];
}
if (!a.return) return [ 3, 2 ];
c = a.return;
return [ 3, 4 ];

case 2:
return [ 4, this._doCallApi(a.apiName, a.req, a.options, s) ];

case 3:
c = f.sent();
f.label = 4;

case 4:
if (s.isAborted) return [ 2 ];
c.isSucc ? this.options.logApi && (null === (u = this.logger) || void 0 === u || u.log("[ApiRes] #".concat(s.sn, " ").concat(e), c.res)) : this.options.logApi && (null === (p = this.logger) || void 0 === p || p[c.err.type === i.TsrpcError.Type.ApiError ? "log" : "error"]("[ApiErr] #".concat(s.sn, " ").concat(e), c.err));
return [ 4, this.flows.preApiReturnFlow.exec(r.__assign(r.__assign({}, a), {
return: c
}), this.logger) ];

case 5:
if (!(l = f.sent())) {
this.abort(s.sn);
return [ 2 ];
}
o(l.return);
this.flows.postApiReturnFlow.exec(l, this.logger);
return [ 2 ];
}
});
});
})).catch().then(function() {
c._pendingApis.removeOne(function(e) {
return e.sn === s.sn;
});
});
return [ 2, a ];
});
});
};
e.prototype._doCallApi = function(e, t, n, o) {
var s;
void 0 === n && (n = {});
return r.__awaiter(this, void 0, void 0, function() {
var a = this;
return r.__generator(this, function() {
this.options.logApi && (null === (s = this.logger) || void 0 === s || s.log("[ApiReq] #".concat(o.sn), e, t));
return [ 2, new Promise(function(s) {
return r.__awaiter(a, void 0, void 0, function() {
var a, c, l, u, p, d;
return r.__generator(this, function(r) {
switch (r.label) {
case 0:
if (!(a = this.serviceMap.apiName2Service[e])) {
s({
isSucc: !1,
err: new i.TsrpcError("Invalid api name: " + e, {
code: "INVALID_API_NAME",
type: i.TsrpcErrorType.ClientError
})
});
return [ 2 ];
}
o.service = a;
if (!(c = f.encodeApiReq(this.tsbuffer, a, t, this.dataType, "LONG" === this.type ? o.sn : void 0)).isSucc) {
s({
isSucc: !1,
err: new i.TsrpcError(c.errMsg, {
type: i.TsrpcErrorType.ClientError,
code: "INPUT_DATA_ERR"
})
});
return [ 2 ];
}
l = this._waitApiReturn(o, null !== (d = n.timeout) && void 0 !== d ? d : this.options.timeout);
return [ 4, this.sendData(c.output, n, a.id, o) ];

case 1:
if ((u = r.sent()).err) {
s({
isSucc: !1,
err: u.err
});
return [ 2 ];
}
return [ 4, l ];

case 2:
p = r.sent();
if (o.isAborted) return [ 2 ];
s(p);
return [ 2 ];
}
});
});
}) ];
});
});
};
e.prototype.sendMsg = function(e, t, n) {
var o = this;
void 0 === n && (n = {});
var s = new Promise(function(s) {
return r.__awaiter(o, void 0, void 0, function() {
var o, a, c, l, u, p;
return r.__generator(this, function(r) {
switch (r.label) {
case 0:
return [ 4, this.flows.preSendMsgFlow.exec({
msgName: e,
msg: t,
options: n
}, this.logger) ];

case 1:
if (!(o = r.sent())) return [ 2 ];
this.options.logMsg && (null === (u = this.logger) || void 0 === u || u.log("[SendMsg]", e, t));
if (!(a = this.serviceMap.msgName2Service[e])) {
null === (p = this.logger) || void 0 === p || p.error("Invalid msg name: " + e);
s({
isSucc: !1,
err: new i.TsrpcError("Invalid msg name: " + e, {
code: "INVALID_MSG_NAME",
type: i.TsrpcErrorType.ClientError
})
});
return [ 2 ];
}
if (!(c = f.encodeClientMsg(this.tsbuffer, a, t, this.dataType, this.type)).isSucc) {
s({
isSucc: !1,
err: new i.TsrpcError(c.errMsg, {
type: i.TsrpcErrorType.ClientError,
code: "ENCODE_MSG_ERR"
})
});
return [ 2 ];
}
return [ 4, this.sendData(c.output, n, a.id) ];

case 2:
if ((l = r.sent()).err) {
s({
isSucc: !1,
err: l.err
});
return [ 2 ];
}
s({
isSucc: !0
});
this.flows.postSendMsgFlow.exec(o, this.logger);
return [ 2 ];
}
});
});
});
s.then(function(e) {
var t;
e.isSucc || (null !== (t = o.logger) && void 0 !== t ? t : console).error("[SendMsgErr]", e.err);
});
return s;
};
e.prototype.listenMsg = function(e, t) {
var n = this;
e instanceof RegExp ? Object.keys(this.serviceMap.msgName2Service).filter(function(t) {
return e.test(t);
}).forEach(function(e) {
n._msgHandlers.addHandler(e, t);
}) : this._msgHandlers.addHandler(e, t);
return t;
};
e.prototype.unlistenMsg = function(e, t) {
var n = this;
e instanceof RegExp ? Object.keys(this.serviceMap.msgName2Service).filter(function(t) {
return e.test(t);
}).forEach(function(e) {
n._msgHandlers.removeHandler(e, t);
}) : this._msgHandlers.removeHandler(e, t);
};
e.prototype.unlistenMsgAll = function(e) {
var t = this;
e instanceof RegExp ? Object.keys(this.serviceMap.msgName2Service).filter(function(t) {
return e.test(t);
}).forEach(function(e) {
t._msgHandlers.removeAllHandlers(e);
}) : this._msgHandlers.removeAllHandlers(e);
};
e.prototype.abort = function(e) {
var t, n, r = this._pendingApis.findIndex(function(t) {
return t.sn === e;
});
if (-1 !== r) {
var o = this._pendingApis[r];
this._pendingApis.splice(r, 1);
o.onReturn = void 0;
o.isAborted = !0;
null === (t = this.logger) || void 0 === t || t.log("[ApiAbort] #".concat(o.sn, " ").concat(o.service.name));
null === (n = o.onAbort) || void 0 === n || n.call(o);
}
};
e.prototype.abortByKey = function(e) {
var t = this;
this._pendingApis.filter(function(t) {
return t.abortKey === e;
}).forEach(function(e) {
t.abort(e.sn);
});
};
e.prototype.abortAll = function() {
var e = this;
this._pendingApis.slice().forEach(function(t) {
return e.abort(t.sn);
});
};
e.prototype.sendData = function(e, t, n, o) {
var i, s, a;
return r.__awaiter(this, void 0, void 0, function() {
var c, l;
return r.__generator(this, function(r) {
switch (r.label) {
case 0:
return [ 4, this.flows.preSendDataFlow.exec({
data: e,
sn: null == o ? void 0 : o.sn
}, this.logger) ];

case 1:
return (c = r.sent()) ? (e = c.data) instanceof Uint8Array ? [ 4, this.flows.preSendBufferFlow.exec({
buf: e,
sn: null == o ? void 0 : o.sn
}, this.logger) ] : [ 3, 3 ] : [ 2, new Promise(function() {}) ];

case 2:
if (!(l = r.sent())) return [ 2, new Promise(function() {}) ];
e = l.buf;
r.label = 3;

case 3:
this.options.debugBuf && ("string" == typeof e ? null === (i = this.logger) || void 0 === i || i.debug("[SendText]" + (o ? " #" + o.sn : "") + " length=".concat(e.length), e) : e instanceof Uint8Array ? null === (s = this.logger) || void 0 === s || s.debug("[SendBuf]" + (o ? " #" + o.sn : "") + " length=".concat(e.length), e) : null === (a = this.logger) || void 0 === a || a.debug("[SendJSON]" + (o ? " #" + o.sn : ""), e));
return [ 2, this._sendData(e, t, n, o) ];
}
});
});
};
e.prototype._onRecvData = function(e, t) {
var n, o, s, a, c, l, u, p, d;
return r.__awaiter(this, void 0, void 0, function() {
var h, y, v, g, _, m;
return r.__generator(this, function(r) {
switch (r.label) {
case 0:
h = null == t ? void 0 : t.sn;
return [ 4, this.flows.preRecvDataFlow.exec({
data: e,
sn: h
}, this.logger) ];

case 1:
if (!(y = r.sent())) return [ 2 ];
if ("string" != typeof (e = y.data)) return [ 3, 2 ];
this.options.debugBuf && (null === (n = this.logger) || void 0 === n || n.debug("[RecvText]" + (h ? " #" + h : ""), e));
return [ 3, 5 ];

case 2:
if (!(e instanceof Uint8Array)) return [ 3, 4 ];
this.options.debugBuf && (null === (o = this.logger) || void 0 === o || o.debug("[RecvBuf]" + (h ? " #" + h : ""), "length=" + e.length, e));
return [ 4, this.flows.preRecvBufferFlow.exec({
buf: e,
sn: h
}, this.logger) ];

case 3:
if (!(v = r.sent())) return [ 2 ];
e = v.buf;
return [ 3, 5 ];

case 4:
this.options.debugBuf && (null === (s = this.logger) || void 0 === s || s.debug("[RecvJSON]" + (h ? " #" + h : ""), e));
r.label = 5;

case 5:
if (!(g = f.parseServerOutout(this.tsbuffer, this.serviceMap, e, null == t ? void 0 : t.service.id)).isSucc) {
null === (a = this.logger) || void 0 === a || a.error("ParseServerOutputError: " + g.errMsg);
e instanceof Uint8Array && (null === (c = this.logger) || void 0 === c || c.error("Please check the version of serviceProto between server and client"));
t && (null === (l = t.onReturn) || void 0 === l || l.call(t, {
isSucc: !1,
err: new i.TsrpcError("Parse server output error", {
type: i.TsrpcErrorType.ServerError
})
}));
return [ 2 ];
}
if ("api" !== (_ = g.result).type) return [ 3, 6 ];
h = null != h ? h : _.sn;
null === (p = null === (u = this._pendingApis.find(function(e) {
return e.sn === h;
})) || void 0 === u ? void 0 : u.onReturn) || void 0 === p || p.call(u, _.ret);
return [ 3, 9 ];

case 6:
if ("msg" !== _.type) return [ 3, 9 ];
this.options.logMsg && (null === (d = this.logger) || void 0 === d || d.log("[RecvMsg] ".concat(_.service.name), _.msg));
return [ 4, this.flows.preRecvMsgFlow.exec({
msgName: _.service.name,
msg: _.msg
}, this.logger) ];

case 7:
if (!(m = r.sent())) return [ 2 ];
this._msgHandlers.forEachHandler(m.msgName, this.logger, m.msg, m.msgName);
return [ 4, this.flows.postRecvMsgFlow.exec(m, this.logger) ];

case 8:
r.sent();
r.label = 9;

case 9:
return [ 2 ];
}
});
});
};
e.prototype._waitApiReturn = function(e, t) {
return r.__awaiter(this, void 0, void 0, function() {
var n = this;
return r.__generator(this, function() {
return [ 2, new Promise(function(r) {
var o;
t && (o = setTimeout(function() {
o = void 0;
n._pendingApis.removeOne(function(t) {
return t.sn === e.sn;
});
r({
isSucc: !1,
err: new i.TsrpcError("Request Timeout", {
type: i.TsrpcErrorType.NetworkError,
code: "TIMEOUT"
})
});
}, t));
e.onReturn = function(t) {
if (o) {
clearTimeout(o);
o = void 0;
}
n._pendingApis.removeOne(function(t) {
return t.sn === e.sn;
});
r(t);
};
}) ];
});
});
};
return e;
}(), h = {
logLevel: "debug",
logApi: !0,
logMsg: !0,
json: !1,
timeout: 15e3,
debugBuf: !1
}, y = function(e) {
r.__extends(t, e);
function t(t, n, o) {
var i, s = this;
(s = e.call(this, t, r.__assign(r.__assign({}, v), o)) || this).type = "SHORT";
s._http = n;
s._jsonServer = s.options.server + (s.options.server.endsWith("/") ? "" : "/");
null === (i = s.logger) || void 0 === i || i.log("TSRPC HTTP Client :", s.options.server);
return s;
}
t.prototype._sendData = function(e, t, n, o) {
return r.__awaiter(this, void 0, void 0, function() {
var i, s = this;
return r.__generator(this, function() {
(i = r.__awaiter(s, void 0, void 0, function() {
var i, s, a, c, l, u, p;
return r.__generator(this, function(r) {
switch (r.label) {
case 0:
i = this.serviceMap.id2Service[n];
s = "msg" === i.type ? "?type=msg" : "";
a = "string" == typeof e ? this._jsonServer + i.name + s : this.options.server;
c = this._http.fetch({
url: a,
data: e,
method: "POST",
timeout: t.timeout || this.options.timeout,
headers: {
"Content-Type": "string" == typeof e ? "application/json" : "application/octet-stream"
},
transportOptions: t,
responseType: "string" == typeof e ? "text" : "arraybuffer"
}), l = c.promise, u = c.abort;
o && (o.onAbort = function() {
u();
});
return (null == o ? void 0 : o.isAborted) ? [ 2, new Promise(function() {}) ] : [ 4, l ];

case 1:
return (p = r.sent()).isSucc ? [ 2, {
res: p.res
} ] : [ 2, {
err: p.err
} ];
}
});
})).then(function(e) {
o && e.res && s._onRecvData(e.res, o);
});
i.catch(function() {}).then(function() {
o && (o.onAbort = void 0);
});
return [ 2, i ];
});
});
};
return t;
}(d), v = r.__assign(r.__assign({}, h), {
server: "http://localhost:3000",
jsonPrune: !0
}), g = function(e) {
r.__extends(t, e);
function t(t, o, s) {
var a, c = this;
(c = e.call(this, t, r.__assign(r.__assign({}, _), s)) || this).type = "LONG";
c._onWsOpen = function() {
var e;
if (c._connecting) {
c._status = n.WsClientStatus.Opened;
c._connecting.rs({
isSucc: !0
});
c._connecting = void 0;
null === (e = c.logger) || void 0 === e || e.log("WebSocket connection to server successful");
c.flows.postConnectFlow.exec({}, c.logger);
c.options.heartbeat && c._heartbeat();
}
};
c._onWsClose = function(e, t) {
var r, o, s;
if (c._status !== n.WsClientStatus.Closed) {
var a = !!c._rsDisconnecting, l = c.isConnected || a;
c._status = n.WsClientStatus.Closed;
if (c._connecting) {
c._connecting.rs({
isSucc: !1,
errMsg: "Failed to connect to WebSocket server: ".concat(c.options.server)
});
c._connecting = void 0;
null === (r = c.logger) || void 0 === r || r.error("Failed to connect to WebSocket server: ".concat(c.options.server));
}
if (c._pendingHeartbeat) {
clearTimeout(c._pendingHeartbeat.timeoutTimer);
c._pendingHeartbeat = void 0;
}
c._nextHeartbeatTimer && clearTimeout(c._nextHeartbeatTimer);
if (c._rsDisconnecting) {
c._rsDisconnecting();
c._rsDisconnecting = void 0;
null === (o = c.logger) || void 0 === o || o.log("Disconnected succ", "code=".concat(e, " reason=").concat(t));
} else l && (null === (s = c.logger) || void 0 === s || s.log("Lost connection to ".concat(c.options.server), "code=".concat(e, " reason=").concat(t)));
l && c.flows.postDisconnectFlow.exec({
reason: t,
isManual: a
}, c.logger);
c._pendingApis.slice().forEach(function(e) {
var n;
null === (n = e.onReturn) || void 0 === n || n.call(e, {
isSucc: !1,
err: new i.TsrpcError(t || "Lost connection to server", {
type: i.TsrpcErrorType.NetworkError,
code: "LOST_CONN"
})
});
});
}
};
c._onWsError = function(e) {
var t, n;
null === (t = c.logger) || void 0 === t || t.error("[WebSocket Error]", e);
if (c._connecting) {
c._connecting.rs({
isSucc: !1,
errMsg: "Failed to connect to WebSocket server: ".concat(c.options.server)
});
c._connecting = void 0;
null === (n = c.logger) || void 0 === n || n.error("Failed to connect to WebSocket server: ".concat(c.options.server));
}
};
c._onWsMessage = function(e) {
c._status === n.WsClientStatus.Opened && (e instanceof Uint8Array && e.length === f.HeartbeatPacket.length && e.every(function(e, t) {
return e === f.HeartbeatPacket[t];
}) ? c._onHeartbeatAnswer(e) : c._onRecvData(e));
};
c.lastHeartbeatLatency = 0;
c._status = n.WsClientStatus.Closed;
c._wsp = o;
o.options = {
onOpen: c._onWsOpen,
onClose: c._onWsClose,
onError: c._onWsError,
onMessage: c._onWsMessage,
logger: c.logger
};
null === (a = c.logger) || void 0 === a || a.log("TSRPC WebSocket Client :", c.options.server);
return c;
}
t.prototype._sendData = function(e) {
return r.__awaiter(this, void 0, void 0, function() {
var t = this;
return r.__generator(this, function() {
return [ 2, new Promise(function(n) {
return r.__awaiter(t, void 0, void 0, function() {
return r.__generator(this, function() {
if (!this.isConnected) {
n({
err: new i.TsrpcError("WebSocket is not connected", {
code: "WS_NOT_OPEN",
type: i.TsrpcError.Type.ClientError
})
});
return [ 2 ];
}
n(this._wsp.send(e));
return [ 2 ];
});
});
}) ];
});
});
};
t.prototype._heartbeat = function() {
var e, t = this;
if (!this._pendingHeartbeat && this._status === n.WsClientStatus.Opened && this.options.heartbeat) {
this._pendingHeartbeat = {
startTime: Date.now(),
timeoutTimer: setTimeout(function() {
var e;
t._pendingHeartbeat = void 0;
null === (e = t.logger) || void 0 === e || e.error("[Heartbeat] Heartbeat timeout, the connection disconnected automatically.");
if (t._status === n.WsClientStatus.Opened) {
t._wsp.close(3e3, "Heartbeat timeout");
t._wsp.options.onClose(3e3, "Heartbeat timeout");
}
}, this.options.heartbeat.timeout)
};
this.options.debugBuf && (null === (e = this.logger) || void 0 === e || e.log("[Heartbeat] Send ping", f.HeartbeatPacket));
this._sendData(f.HeartbeatPacket);
}
};
t.prototype._onHeartbeatAnswer = function(e) {
var t, r = this;
if (this._pendingHeartbeat && this._status === n.WsClientStatus.Opened && this.options.heartbeat) {
this.lastHeartbeatLatency = Date.now() - this._pendingHeartbeat.startTime;
this.options.debugBuf && (null === (t = this.logger) || void 0 === t || t.log("[Heartbeat] Recv pong, latency=".concat(this.lastHeartbeatLatency, "ms"), e));
clearTimeout(this._pendingHeartbeat.timeoutTimer);
this._pendingHeartbeat = void 0;
this._nextHeartbeatTimer = setTimeout(function() {
r._heartbeat();
}, this.options.heartbeat.interval);
}
};
Object.defineProperty(t.prototype, "status", {
get: function() {
return this._status;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "isConnected", {
get: function() {
return this._status === n.WsClientStatus.Opened;
},
enumerable: !1,
configurable: !0
});
t.prototype.connect = function() {
var e, t;
return r.__awaiter(this, void 0, void 0, function() {
var o, i, s = this;
return r.__generator(this, function(r) {
switch (r.label) {
case 0:
return this.isConnected ? [ 2, {
isSucc: !0
} ] : this._connecting ? [ 2, this._connecting.promise ] : [ 4, this.flows.preConnectFlow.exec({}, this.logger) ];

case 1:
if (null == (o = r.sent()) ? void 0 : o.return) return [ 2, o.return ];
if (!o) return [ 2, new Promise(function() {}) ];
try {
this._wsp.connect(this.options.server, [ this.options.json ? "text" : "buffer" ]);
} catch (t) {
null === (e = this.logger) || void 0 === e || e.error(t);
return [ 2, {
isSucc: !1,
errMsg: t.message
} ];
}
this._status = n.WsClientStatus.Opening;
null === (t = this.logger) || void 0 === t || t.log("Start connecting ".concat(this.options.server, "..."));
this._connecting = {};
i = new Promise(function(e) {
s._connecting.rs = e;
});
this._connecting.promise = i;
return [ 2, i ];
}
});
});
};
t.prototype.disconnect = function(e, t) {
var o;
return r.__awaiter(this, void 0, void 0, function() {
var i = this;
return r.__generator(this, function() {
if (this._status === n.WsClientStatus.Closed) return [ 2 ];
this._status = n.WsClientStatus.Closing;
null === (o = this.logger) || void 0 === o || o.log("Start disconnecting...");
return [ 2, new Promise(function(n) {
i._rsDisconnecting = n;
void 0 === e && void 0 === t ? i._wsp.close() : void 0 === t ? i._wsp.close(e) : i._wsp.close(e, t);
}) ];
});
});
};
return t;
}(d), _ = r.__assign(r.__assign({}, h), {
server: "ws://localhost:3000"
});
n.WsClientStatus = void 0;
(function(e) {
e.Opening = "OPENING";
e.Opened = "OPENED";
e.Closing = "CLOSING";
e.Closed = "CLOSED";
})(n.WsClientStatus || (n.WsClientStatus = {}));
n.BaseClient = d;
n.BaseHttpClient = y;
n.BaseWsClient = g;
n.Counter = a;
n.Flow = c;
n.MsgHandlerManager = u;
n.ServiceMapUtil = p;
n.TransportDataUtil = f;
n.defaultBaseClientOptions = h;
n.defaultBaseHttpClientOptions = v;
n.defaultBaseWsClientOptions = _;
n.getCustomObjectIdTypes = l;
}, {
"k8w-extend-native": 270,
tsbuffer: 287,
"tsbuffer-schema": 285,
tslib: 288,
"tsrpc-proto": 291
} ],
290: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
e("k8w-extend-native");
e("core-js/modules/es.object.to-string.js");
e("core-js/modules/es.regexp.to-string.js");
e("core-js/modules/es.number.max-safe-integer.js");
e("core-js/modules/es.number.constructor.js");
e("core-js/modules/es.regexp.exec.js");
e("core-js/modules/es.regexp.test.js");
e("core-js/modules/es.array.iterator.js");
e("core-js/modules/es.array-buffer.slice.js");
e("core-js/modules/es.typed-array.uint8-array.js");
e("core-js/modules/esnext.typed-array.at.js");
e("core-js/modules/es.typed-array.copy-within.js");
e("core-js/modules/es.typed-array.every.js");
e("core-js/modules/es.typed-array.fill.js");
e("core-js/modules/es.typed-array.filter.js");
e("core-js/modules/es.typed-array.find.js");
e("core-js/modules/es.typed-array.find-index.js");
e("core-js/modules/es.typed-array.for-each.js");
e("core-js/modules/es.typed-array.includes.js");
e("core-js/modules/es.typed-array.index-of.js");
e("core-js/modules/es.typed-array.iterator.js");
e("core-js/modules/es.typed-array.join.js");
e("core-js/modules/es.typed-array.last-index-of.js");
e("core-js/modules/es.typed-array.map.js");
e("core-js/modules/es.typed-array.reduce.js");
e("core-js/modules/es.typed-array.reduce-right.js");
e("core-js/modules/es.typed-array.reverse.js");
e("core-js/modules/es.typed-array.set.js");
e("core-js/modules/es.typed-array.slice.js");
e("core-js/modules/es.typed-array.some.js");
e("core-js/modules/es.typed-array.sort.js");
e("core-js/modules/es.typed-array.subarray.js");
e("core-js/modules/es.typed-array.to-locale-string.js");
e("core-js/modules/es.typed-array.to-string.js");
e("core-js/modules/es.array.from.js");
e("core-js/modules/es.string.iterator.js");
e("core-js/modules/es.number.parse-int.js");
e("core-js/modules/es.array.join.js");
e("core-js/modules/es.string.match.js");
e("core-js/modules/es.function.name.js");
e("core-js/modules/es.json.stringify.js");
e("core-js/modules/es.promise.js");
e("core-js/modules/es.regexp.constructor.js");
e("core-js/modules/es.regexp.sticky.js");
e("core-js/modules/web.dom-collections.for-each.js");
e("core-js/modules/es.array.filter.js");
e("core-js/modules/es.object.keys.js");
e("core-js/modules/es.array.find-index.js");
e("core-js/modules/es.array.splice.js");
e("core-js/modules/es.array.slice.js");
e("core-js/modules/es.array.find.js");
e("core-js/modules/es.string.ends-with.js");
e("core-js/modules/es.array.includes.js");
e("core-js/modules/es.string.includes.js");
e("core-js/modules/es.string.starts-with.js");
e("core-js/modules/es.typed-array.int8-array.js");
e("core-js/modules/es.typed-array.int16-array.js");
e("core-js/modules/es.typed-array.int32-array.js");
e("core-js/modules/es.typed-array.uint16-array.js");
e("core-js/modules/es.typed-array.uint32-array.js");
e("core-js/modules/es.typed-array.float32-array.js");
e("core-js/modules/es.typed-array.float64-array.js");
e("core-js/modules/es.array.map.js");
e("core-js/modules/es.object.assign.js");
e("core-js/modules/es.array.index-of.js");
e("core-js/modules/web.dom-collections.iterator.js");
e("core-js/modules/es.array-buffer.constructor.js");
e("core-js/modules/es.array-buffer.is-view.js");
e("core-js/modules/web.url.to-json.js");
e("core-js/modules/es.object.entries.js");
e("core-js/modules/es.number.is-integer.js");
e("core-js/modules/es.object.get-prototype-of.js");
e("core-js/modules/es.parse-int.js");
var r = e("tsrpc-proto"), o = e("tslib"), i = e("tsrpc-base-client"), s = function() {
function e() {}
e.prototype.fetch = function(e) {
var t, n, i = this, s = new Promise(function(e) {
n = e;
}), a = new XMLHttpRequest();
if ("undefined" != typeof navigator && (null === (t = null === navigator || void 0 === navigator ? void 0 : navigator.userAgent) || void 0 === t ? void 0 : t.indexOf("MSIE 8.0;")) > -1) a.onreadystatechange = function() {
return o.__awaiter(i, void 0, void 0, function() {
return o.__generator(this, function() {
if (4 == a.readyState) {
if (0 == a.status || null == a.response && null == a.responseText) {
n({
isSucc: !1,
err: new r.TsrpcError("Network Error", {
type: r.TsrpcError.Type.NetworkError,
httpCode: a.status
})
});
return [ 2 ];
}
if (12029 == a.status) {
n({
isSucc: !1,
err: new r.TsrpcError({
message: "Network Error",
type: r.TsrpcError.Type.NetworkError,
httpCode: a.status
})
});
return [ 2 ];
}
n({
isSucc: !0,
res: "text" === e.responseType ? a.responseText : new Uint8Array(a.response)
});
}
return [ 2 ];
});
});
}; else {
a.onerror = function() {
n({
isSucc: !1,
err: new r.TsrpcError({
message: "Network Error",
type: r.TsrpcError.Type.NetworkError,
httpCode: a.status
})
});
};
a.ontimeout = function() {
n({
isSucc: !1,
err: new r.TsrpcError({
message: "Request Timeout",
type: r.TsrpcError.Type.NetworkError,
code: "TIMEOUT"
})
});
};
a.onload = function() {
return o.__awaiter(i, void 0, void 0, function() {
return o.__generator(this, function() {
200 === a.status || 500 === a.status ? n({
isSucc: !0,
res: a.response && ("text" === e.responseType ? a.responseText : new Uint8Array(a.response))
}) : n({
isSucc: !1,
err: new r.TsrpcError({
message: "HTTP Error " + a.status,
type: r.TsrpcError.Type.ServerError,
httpCode: a.status
})
});
return [ 2 ];
});
});
};
var c = e.transportOptions;
c.onProgress && (a.upload.onprogress = function(e) {
var t;
null === (t = c.onProgress) || void 0 === t || t.call(c, e.loaded / e.total);
});
}
a.open(e.method, e.url, !0);
if (e.headers) for (var l in e.headers) a.setRequestHeader(l, e.headers[l]);
a.responseType = e.responseType;
var u = e.timeout;
u && (a.timeout = u);
a.send(e.data);
return {
promise: s,
abort: a.abort.bind(a)
};
};
return e;
}(), a = function(e) {
o.__extends(t, e);
function t(t, n) {
var r = new s();
return e.call(this, t, r, o.__assign(o.__assign({}, c), n)) || this;
}
t.prototype.callApi = function(t, n, r) {
void 0 === r && (r = {});
return e.prototype.callApi.call(this, t, n, r);
};
t.prototype.sendMsg = function(t, n, r) {
void 0 === r && (r = {});
return e.prototype.sendMsg.call(this, t, n, r);
};
return t;
}(i.BaseHttpClient), c = o.__assign(o.__assign({}, i.defaultBaseHttpClientOptions), {
customObjectIdClass: String
}), l = function() {
function e() {}
e.prototype.connect = function(e, t) {
var n = this;
this._ws = new WebSocket(e, t);
this._ws.binaryType = "arraybuffer";
this._ws.onopen = this.options.onOpen;
this._ws.onerror = this.options.onError;
this._ws.onclose = function(e) {
n.options.onClose(e.code, e.reason);
n._ws = void 0;
};
this._ws.onmessage = function(e) {
var t;
e.data instanceof ArrayBuffer ? n.options.onMessage(new Uint8Array(e.data)) : "string" == typeof e.data ? n.options.onMessage(e.data) : null === (t = n.options.logger) || void 0 === t || t.warn("[Unresolved Recv]", e.data);
};
};
e.prototype.close = function(e, t) {
var n;
null === (n = this._ws) || void 0 === n || n.close(e, t);
this._ws = void 0;
};
e.prototype.send = function(e) {
return o.__awaiter(this, void 0, void 0, function() {
var t, n;
return o.__generator(this, function() {
try {
void 0;
t = "string" == typeof e ? e : 0 === (n = e).byteOffset && n.byteLength === n.buffer.byteLength ? n.buffer : n.buffer.slice(n.byteOffset, n.byteOffset + n.byteLength);
this._ws.send(t);
return [ 2, {} ];
} catch (e) {
return [ 2, {
err: new r.TsrpcError("Network Error", {
code: "SEND_BUF_ERR",
type: r.TsrpcError.Type.NetworkError,
innerErr: e
})
} ];
}
return [ 2 ];
});
});
};
return e;
}(), u = function(e) {
o.__extends(t, e);
function t(t, n) {
var r = new l();
return e.call(this, t, r, o.__assign(o.__assign({}, p), n)) || this;
}
return t;
}(i.BaseWsClient), p = o.__assign(o.__assign({}, i.defaultBaseWsClientOptions), {
customObjectIdClass: String
});
n.HttpClient = a;
n.WsClient = u;
Object.keys(r).forEach(function(e) {
"default" === e || n.hasOwnProperty(e) || Object.defineProperty(n, e, {
enumerable: !0,
get: function() {
return r[e];
}
});
});
}, {
"core-js/modules/es.array-buffer.constructor.js": 191,
"core-js/modules/es.array-buffer.is-view.js": 192,
"core-js/modules/es.array-buffer.slice.js": 193,
"core-js/modules/es.array.filter.js": 194,
"core-js/modules/es.array.find-index.js": 195,
"core-js/modules/es.array.find.js": 196,
"core-js/modules/es.array.from.js": 197,
"core-js/modules/es.array.includes.js": 198,
"core-js/modules/es.array.index-of.js": 199,
"core-js/modules/es.array.iterator.js": 200,
"core-js/modules/es.array.join.js": 201,
"core-js/modules/es.array.map.js": 202,
"core-js/modules/es.array.slice.js": 203,
"core-js/modules/es.array.splice.js": 204,
"core-js/modules/es.function.name.js": 205,
"core-js/modules/es.json.stringify.js": 206,
"core-js/modules/es.number.constructor.js": 207,
"core-js/modules/es.number.is-integer.js": 208,
"core-js/modules/es.number.max-safe-integer.js": 209,
"core-js/modules/es.number.parse-int.js": 210,
"core-js/modules/es.object.assign.js": 211,
"core-js/modules/es.object.entries.js": 212,
"core-js/modules/es.object.get-prototype-of.js": 213,
"core-js/modules/es.object.keys.js": 214,
"core-js/modules/es.object.to-string.js": 215,
"core-js/modules/es.parse-int.js": 216,
"core-js/modules/es.promise.js": 220,
"core-js/modules/es.regexp.constructor.js": 224,
"core-js/modules/es.regexp.exec.js": 225,
"core-js/modules/es.regexp.sticky.js": 226,
"core-js/modules/es.regexp.test.js": 227,
"core-js/modules/es.regexp.to-string.js": 228,
"core-js/modules/es.string.ends-with.js": 229,
"core-js/modules/es.string.includes.js": 230,
"core-js/modules/es.string.iterator.js": 231,
"core-js/modules/es.string.match.js": 232,
"core-js/modules/es.string.starts-with.js": 233,
"core-js/modules/es.typed-array.copy-within.js": 235,
"core-js/modules/es.typed-array.every.js": 236,
"core-js/modules/es.typed-array.fill.js": 237,
"core-js/modules/es.typed-array.filter.js": 238,
"core-js/modules/es.typed-array.find-index.js": 239,
"core-js/modules/es.typed-array.find.js": 240,
"core-js/modules/es.typed-array.float32-array.js": 241,
"core-js/modules/es.typed-array.float64-array.js": 242,
"core-js/modules/es.typed-array.for-each.js": 243,
"core-js/modules/es.typed-array.includes.js": 244,
"core-js/modules/es.typed-array.index-of.js": 245,
"core-js/modules/es.typed-array.int16-array.js": 246,
"core-js/modules/es.typed-array.int32-array.js": 247,
"core-js/modules/es.typed-array.int8-array.js": 248,
"core-js/modules/es.typed-array.iterator.js": 249,
"core-js/modules/es.typed-array.join.js": 250,
"core-js/modules/es.typed-array.last-index-of.js": 251,
"core-js/modules/es.typed-array.map.js": 252,
"core-js/modules/es.typed-array.reduce-right.js": 253,
"core-js/modules/es.typed-array.reduce.js": 254,
"core-js/modules/es.typed-array.reverse.js": 255,
"core-js/modules/es.typed-array.set.js": 256,
"core-js/modules/es.typed-array.slice.js": 257,
"core-js/modules/es.typed-array.some.js": 258,
"core-js/modules/es.typed-array.sort.js": 259,
"core-js/modules/es.typed-array.subarray.js": 260,
"core-js/modules/es.typed-array.to-locale-string.js": 261,
"core-js/modules/es.typed-array.to-string.js": 262,
"core-js/modules/es.typed-array.uint16-array.js": 263,
"core-js/modules/es.typed-array.uint32-array.js": 264,
"core-js/modules/es.typed-array.uint8-array.js": 265,
"core-js/modules/esnext.typed-array.at.js": 266,
"core-js/modules/web.dom-collections.for-each.js": 267,
"core-js/modules/web.dom-collections.iterator.js": 268,
"core-js/modules/web.url.to-json.js": 269,
"k8w-extend-native": 270,
tslib: 288,
"tsrpc-base-client": 289,
"tsrpc-proto": 291
} ],
291: [ function(e, t, n) {
"use strict";
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("tsbuffer-schema"), o = e("tslib"), i = function() {};
n.TsrpcErrorType = void 0;
(function(e) {
e.NetworkError = "NetworkError";
e.ServerError = "ServerError";
e.ClientError = "ClientError";
e.ApiError = "ApiError";
})(n.TsrpcErrorType || (n.TsrpcErrorType = {}));
var s = {
ServerInputData: {
type: r.SchemaType.Interface,
properties: [ {
id: 0,
name: "serviceId",
type: {
type: r.SchemaType.Number,
scalarType: "uint"
}
}, {
id: 1,
name: "buffer",
type: {
type: r.SchemaType.Buffer,
arrayType: "Uint8Array"
}
}, {
id: 2,
name: "sn",
type: {
type: r.SchemaType.Number,
scalarType: "uint"
},
optional: !0
} ]
},
ServerOutputData: {
type: r.SchemaType.Interface,
properties: [ {
id: 0,
name: "buffer",
type: {
type: r.SchemaType.Buffer,
arrayType: "Uint8Array"
},
optional: !0
}, {
id: 1,
name: "error",
type: {
type: r.SchemaType.Reference,
target: "TsrpcErrorData"
},
optional: !0
}, {
id: 2,
name: "serviceId",
type: {
type: r.SchemaType.Number,
scalarType: "uint"
},
optional: !0
}, {
id: 3,
name: "sn",
type: {
type: r.SchemaType.Number,
scalarType: "uint"
},
optional: !0
} ]
},
TsrpcErrorData: {
type: r.SchemaType.Interface,
properties: [ {
id: 0,
name: "message",
type: {
type: r.SchemaType.String
}
}, {
id: 1,
name: "type",
type: {
type: r.SchemaType.Reference,
target: "TsrpcErrorType"
}
}, {
id: 2,
name: "code",
type: {
type: r.SchemaType.Union,
members: [ {
id: 0,
type: {
type: r.SchemaType.String
}
}, {
id: 1,
type: {
type: r.SchemaType.Number,
scalarType: "int"
}
} ]
},
optional: !0
} ],
indexSignature: {
keyType: "String",
type: {
type: r.SchemaType.Any
}
}
},
TsrpcErrorType: {
type: r.SchemaType.Enum,
members: [ {
id: 0,
value: "NetworkError"
}, {
id: 1,
value: "ServerError"
}, {
id: 2,
value: "ClientError"
}, {
id: 3,
value: "ApiError"
} ]
}
}, a = function() {
function e(e, t) {
var r;
if ("string" == typeof e) {
this.message = e;
this.type = null !== (r = null == t ? void 0 : t.type) && void 0 !== r ? r : n.TsrpcErrorType.ApiError;
o.__assign(this, t);
} else o.__assign(this, e);
}
e.prototype.toString = function() {
return "[TSRPC ".concat(this.type, "]: ").concat(this.message);
};
e.Type = n.TsrpcErrorType;
return e;
}();
n.TransportDataProto = s;
n.TsrpcError = a;
n.setLogLevel = function(e, t) {
switch (t) {
case "none":
return {
debug: i,
log: i,
warn: i,
error: i
};

case "error":
return {
debug: i,
log: i,
warn: i,
error: e.error.bind(e)
};

case "warn":
return {
debug: i,
log: i,
warn: e.warn.bind(e),
error: e.error.bind(e)
};

case "info":
return {
debug: i,
log: e.log.bind(e),
warn: e.warn.bind(e),
error: e.error.bind(e)
};

case "debug":
return e;

default:
throw new Error("Invalid logLevel: '".concat(t, "'"));
}
};
}, {
"tsbuffer-schema": 285,
tslib: 288
} ],
AdapterMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "9848co1bh1O76pCrg2kIN9s", "AdapterMgr");
var r = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
n.AdapterType = void 0;
var o = 0, i = 1 << o++, s = 1 << o++, a = 1 << o++, c = 1 << o++, l = 1 << o++, u = s | a, p = c | l, f = 1 << o++, d = 1 << o++;
o++;
(function(e) {
e[e.Top = c] = "Top";
e[e.Bottom = l] = "Bottom";
e[e.Left = s] = "Left";
e[e.Right = a] = "Right";
e[e.StretchWidth = u] = "StretchWidth";
e[e.StretchHeight = p] = "StretchHeight";
e[e.FullWidth = f] = "FullWidth";
e[e.FullHeight = d] = "FullHeight";
})(n.AdapterType || (n.AdapterType = {}));
var h = cc._decorator, y = h.ccclass, v = (h.property, function() {
function e() {}
t = e;
Object.defineProperty(e, "inst", {
get: function() {
if (null == this._instance) {
this._instance = new t();
this._instance.visibleSize = cc.view.getVisibleSize();
console.log("visiable size: " + this._instance.visibleSize);
}
return this._instance;
},
enumerable: !1,
configurable: !0
});
e.prototype.adapteByType = function(e, t, n) {
void 0 === n && (n = 0);
var r = t.getComponent(cc.Widget);
this._doAdapte(e, t, n);
r.updateAlignment();
};
e.prototype._doAdapte = function(e, t, n) {
void 0 === n && (n = 0);
var r = t.getComponent(cc.Widget);
r || (r = t.addComponent(cc.Widget));
switch (e) {
case i:
break;

case s:
r.left = n || 0;
break;

case a:
r.right = n || 0;
break;

case c:
r.top = n || 0;
break;

case l:
r.bottom = n || 0;
break;

case f:
t.height /= t.width / this.visibleSize.width;
t.width = this.visibleSize.width;
break;

case d:
t.width /= t.height / this.visibleSize.height;
t.height = this.visibleSize.height;
}
};
e.prototype.removeAdaptater = function(e) {
e.getComponent(cc.Widget) && e.removeComponent(cc.Widget);
};
e.prototype.checkIsIphoneX = function() {
return 375 === cc.view.getFrameSize().width && 812 === cc.view.getFrameSize().height || 414 === cc.view.getFrameSize().width && 896 === cc.view.getFrameSize().height || 390 === cc.view.getFrameSize().width && 844 === cc.view.getFrameSize().height || 428 === cc.view.getFrameSize().width && 926 === cc.view.getFrameSize().height;
};
var t;
e._instance = null;
return t = r([ y ], e);
}());
n.default = v;
cc._RF.pop();
}, {} ],
ApiSend: [ function(e, t) {
"use strict";
cc._RF.push(t, "cb4f2bInyVDNb1H1Esey01E", "ApiSend");
cc._RF.pop();
}, {} ],
BackgroundAdapter: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "ee460pjB71J/bnLQYJxtlBA", "BackgroundAdapter");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = (s.property, function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.onLoad = function() {
var e = Math.min(cc.view.getCanvasSize().width / this.node.width, cc.view.getCanvasSize().height / this.node.height), t = this.node.width * e, n = this.node.height * e;
this.node.scale = Math.max(cc.view.getCanvasSize().width / t, cc.view.getCanvasSize().height / n);
};
return i([ a ], t);
}(cc.Component));
n.default = c;
cc._RF.pop();
}, {} ],
ButtonPlus: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "3eaf8iLxgtDEKVcEmFvrbqy", "ButtonPlus");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Manager/SoundMgr"), a = cc._decorator, c = a.ccclass, l = a.property, u = a.executeInEditMode, p = a.menu, f = a.help, d = a.inspector, h = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.audioUrl = "";
t.openContinuous = !0;
t.continuousTime = 1;
t.continuous = !1;
t._continuousTimer = null;
t.openLongPress = !1;
t.longPressTime = 1;
t.longPressFlag = !1;
t.longPressTimer = null;
return t;
}
t.prototype.onEnable = function() {
this.continuous = !1;
e.prototype.onEnable.call(this);
};
t.prototype.onDisable = function() {
if (this._continuousTimer) {
clearTimeout(this._continuousTimer);
this._continuousTimer = null;
}
if (this.longPressTimer) {
clearTimeout(this.longPressTimer);
this.longPressTimer = null;
}
e.prototype.onDisable.call(this);
};
t.prototype._onTouchBegan = function(e) {
if (this.interactable && this.enabledInHierarchy) {
if (this.openLongPress && !this.longPressFlag) {
this.longPressTimer && clearTimeout(this.longPressTimer);
this.longPressTimer = setTimeout(function() {
if (this._pressed) {
this.node.emit("longclickStart", this);
this.longPressFlag = !0;
}
}.bind(this), 1e3 * this.longPressTime);
}
this._pressed = !0;
this._updateState();
e.stopPropagation();
}
};
t.prototype._onTouchEnded = function(e) {
if (this.interactable && this.enabledInHierarchy) {
if (this._pressed && this.longPressFlag) {
this.node.emit("longclickEnd", this);
this.longPressFlag = !1;
} else if (this._pressed && !this.continuous) {
this.continuous = !!this.openContinuous;
cc.Component.EventHandler.emitEvents(this.clickEvents, e);
this.node.emit("click", e);
s.default.inst.playEffect(this.audioUrl);
this.openContinuous && (this._continuousTimer = setTimeout(function() {
this.continuous = !1;
}.bind(this), 1e3 * this.continuousTime));
}
this._pressed = !1;
this._updateState();
e.stopPropagation();
}
};
t.prototype._onTouchCancel = function() {
if (this.interactable && this.enabledInHierarchy) {
if (this._pressed && this.longPressFlag) {
this.node.emit("longclickEnd", this);
this.longPressFlag = !1;
}
this._pressed = !1;
this._updateState();
}
};
t.prototype.addClick = function(e, t) {
this.node.off("click");
this.node.on("click", e, t);
};
t.prototype.addLongClick = function(e, t, n) {
this.node.off("longclickStart");
this.node.off("longclickEnd");
this.node.on("longclickStart", e, n);
this.node.on("longclickEnd", t, n);
};
i([ l({
tooltip: "音效路径",
type: "",
multiline: !0,
formerlySerializedAs: "_N$string"
}) ], t.prototype, "audioUrl", void 0);
i([ l({
tooltip: "屏蔽连续点击"
}) ], t.prototype, "openContinuous", void 0);
i([ l({
tooltip: "屏蔽时间, 单位:秒"
}) ], t.prototype, "continuousTime", void 0);
i([ l({
tooltip: "是否开启长按事件"
}) ], t.prototype, "openLongPress", void 0);
i([ l({
tooltip: "长按时间"
}) ], t.prototype, "longPressTime", void 0);
return i([ c, p("i18n:MAIN_MENU.component.ui/ButtonPlus"), u, f("i18n:COMPONENT.help_url.button"), d("packages://buttonplus/inspector.js") ], t);
}(cc.Button);
n.default = h;
cc._RF.pop();
}, {
"../../Manager/SoundMgr": "SoundMgr"
} ],
CardArrayFlip_CardLayout: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "34a14VJ+FVL85T5e314dkhm", "CardArrayFlip_CardLayout");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./CardArrayFlip_Card"), a = cc._decorator, c = a.ccclass, l = a.property, u = a.executeInEditMode, p = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t._radius = 350;
t._offset = 90;
t._k = 0;
t.cards = null;
return t;
}
Object.defineProperty(t.prototype, "radius", {
get: function() {
return this._radius;
},
set: function(e) {
this._radius = e;
this.updateLayout();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "offset", {
get: function() {
return this._offset;
},
set: function(e) {
this._offset = e;
this.updateLayout();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "k", {
get: function() {
return this._k;
},
set: function(e) {
this._k = e;
this.updateKValue();
},
enumerable: !1,
configurable: !0
});
t.prototype.onLoad = function() {
this.init();
this.registerEvent();
};
t.prototype.onDisable = function() {
this.unregisterEvent();
};
t.prototype.init = function() {
this.onChildChange();
};
t.prototype.registerEvent = function() {
this.node.on(cc.Node.EventType.CHILD_ADDED, this.onChildChange, this);
this.node.on(cc.Node.EventType.CHILD_REMOVED, this.onChildChange, this);
this.node.on(cc.Node.EventType.ROTATION_CHANGED, this.onRotationChange, this);
};
t.prototype.unregisterEvent = function() {
this.node.off(cc.Node.EventType.CHILD_ADDED, this.onChildChange, this);
this.node.off(cc.Node.EventType.CHILD_REMOVED, this.onChildChange, this);
this.node.off(cc.Node.EventType.ROTATION_CHANGED, this.onRotationChange, this);
};
t.prototype.onChildChange = function() {
this.cards = this.getComponentsInChildren(s.default);
this.updateKValue();
this.updateLayout();
};
t.prototype.onRotationChange = function() {
this.updateHierarchy();
};
t.prototype.updateLayout = function() {
for (var e = this.node.children, t = e.length, n = this._radius, r = this._offset, o = 360 / t, i = 0; i < t; i++) {
var s = e[i], a = -o * i, c = Math.PI / 180 * (a - r);
s.x = n * Math.cos(c);
s.z = -n * Math.sin(c);
var l = s.eulerAngles, u = l.x, p = l.z;
s.eulerAngles = cc.v3(u, a, p);
}
this.updateHierarchy();
};
t.prototype.updateHierarchy = function() {
for (var e = this.cards, t = e.length, n = 0; n < t; n++) e[n].updateWorldZ();
e.sort(function(e, t) {
return e.z - t.z;
});
for (n = 0; n < t; n++) e[n].setSiblingIndex(n);
};
t.prototype.updateKValue = function() {
for (var e = this.cards, t = 0, n = e.length; t < n; t++) e[t].k = this._k;
};
i([ l ], t.prototype, "_radius", void 0);
i([ l({
displayName: !1
}) ], t.prototype, "radius", null);
i([ l ], t.prototype, "_offset", void 0);
i([ l({
displayName: !1
}) ], t.prototype, "offset", null);
i([ l ], t.prototype, "_k", void 0);
i([ l({
displayName: !1
}) ], t.prototype, "k", null);
return i([ c, u ], t);
}(cc.Component);
n.default = p;
cc._RF.pop();
}, {
"./CardArrayFlip_Card": "CardArrayFlip_Card"
} ],
CardArrayFlip_Card: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "659b8MDsJFPz5aiPsq4V0Q2", "CardArrayFlip_Card");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = s.executeInEditMode, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.back = null;
t.front = null;
t.k = 0;
t._z = 0;
return t;
}
Object.defineProperty(t.prototype, "z", {
get: function() {
return this._z;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "facingScreen", {
get: function() {
return this.node.forward.z >= this.k;
},
enumerable: !1,
configurable: !0
});
t.prototype.onEnable = function() {
this.updateWorldZ();
};
t.prototype.update = function() {
this.updateDisplay();
};
t.prototype.updateDisplay = function() {
var e = this.facingScreen;
this.front.active = e;
this.back.active = !e;
};
t.prototype.updateWorldZ = function() {
var e = this.node.parent.convertToWorldSpaceAR(this.node.position);
this._z = e.z;
};
t.prototype.setSiblingIndex = function(e) {
this.node.setSiblingIndex(e);
};
i([ c(cc.Node) ], t.prototype, "back", void 0);
i([ c(cc.Node) ], t.prototype, "front", void 0);
i([ c ], t.prototype, "k", void 0);
return i([ a, l ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {} ],
CardArrayFlip_FrontCard2D: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "301c82uYRJB0IHWbyIdk4kE", "CardArrayFlip_FrontCard2D");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./CardArrayFlip_FrontCardBase"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.flipToFront = function() {
var e = this;
this.node.is3DNode = !0;
return new Promise(function(t) {
var n = cc.tween;
n(e.node).to(1, {
scale: 1.1
}).start();
n(e.main).parallel(n().to(.5, {
scaleX: 0
}, {
easing: "quadIn"
}), n().to(.5, {
eulerAngles: cc.v3(e.main.x, -15, e.main.z)
}, {
easing: "quadOut"
})).call(function() {
e.front.active = !0;
e.back.active = !1;
}).parallel(n().to(.5, {
scaleX: -1
}, {
easing: "quadOut"
}), n().to(.5, {
eulerAngles: cc.v3(e.main.x, 0, e.main.z)
}, {
easing: "quadIn"
})).call(t).start();
});
};
t.prototype.flipToBack = function() {
var e = this;
return new Promise(function(t) {
var n = cc.tween;
n(e.node).to(1, {
scale: .8
}).start();
n(e.main).parallel(n().to(.5, {
scaleX: 0
}, {
easing: "quadIn"
}), n().to(.5, {
eulerAngles: cc.v3(e.main.x, 15, e.main.z)
}, {
easing: "quadOut"
})).call(function() {
e.front.active = !1;
e.back.active = !0;
}).parallel(n().to(.5, {
scaleX: 1
}, {
easing: "quadOut"
}), n().to(.5, {
eulerAngles: cc.v3(e.main.x, 0, e.main.z)
}, {
easing: "quadIn"
})).call(t).start();
});
};
return i([ c ], t);
}(s.default));
n.default = l;
cc._RF.pop();
}, {
"./CardArrayFlip_FrontCardBase": "CardArrayFlip_FrontCardBase"
} ],
CardArrayFlip_FrontCardBase: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "7d811Mp9K1Gxotu8UyGs7cC", "CardArrayFlip_FrontCardBase");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.main = null;
t.back = null;
t.front = null;
return t;
}
t.prototype.onLoad = function() {
this.init();
};
t.prototype.init = function() {
this.hide();
this.front.active = !1;
this.back.active = !0;
};
t.prototype.show = function() {
this.main.active = !0;
};
t.prototype.hide = function() {
this.main.active = !1;
};
t.prototype.flipToFront = function() {
return null;
};
t.prototype.flipToBack = function() {
return null;
};
i([ c(cc.Node) ], t.prototype, "main", void 0);
i([ c(cc.Node) ], t.prototype, "back", void 0);
i([ c(cc.Node) ], t.prototype, "front", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
ClientChannel: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "818a0wYWQlBhYHv8FTeLSxY", "ClientChannel");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.ClientChannel = void 0;
var r = e("../../Protocol/bundle"), o = e("../Manager/FormMgr"), i = e("../UIConfig"), s = e("../UIScript/UIToast"), a = function() {
function e(e) {
this.WebSocket = e;
this.invokeId = 1;
this.callbacks = new Map();
this.listeners = new Map();
}
e.prototype.connect = function(e) {
var t = this;
return new Promise(function(n, a) {
var c = new t.WebSocket(e);
c.binaryType = "arraybuffer";
var l = !1;
c.onopen = function() {
t.websocket = c;
l = !0;
o.default.close(i.default.UIScoketReconnent);
n();
};
c.onclose = function() {
if (l) t.close(); else {
o.default.open(i.default.UIScoketReconnent);
a(new Error("WebSocket failed to connect"));
}
};
c.onerror = function() {
o.default.open(i.default.UIScoketReconnent);
};
c.onmessage = function(e) {
var n, o, i;
try {
var a = r.Envelope.decode(new Uint8Array(e.data));
if (null === (n = a.header) || void 0 === n ? void 0 : n.invokeEnd) {
var c = a.header.invokeEnd;
if (!(l = t.callbacks.get(c.invokeId))) return;
t.callbacks.delete(c.invokeId);
0 == c.status ? l(null, a.payload) : l(new Error(c.error || "rpc: remote system error"), null);
} else if (null === (o = a.header) || void 0 === o ? void 0 : o.closeConnection) t.close(); else if (null === (i = a.header) || void 0 === i ? void 0 : i.event) {
if (!t.listeners.has(a.header.event)) return;
for (var l in t.listeners.get(a.header.event)) setTimeout(l, 0, a.payload);
}
} catch (e) {
s.default.popUp("" + e);
console.log("消息错误：" + e);
}
};
});
};
e.prototype.disconnect = function() {
this.websocket && this.close();
};
e.prototype.send = function(e) {
var t;
if ((null === (t = this.websocket) || void 0 === t ? void 0 : t.readyState) != this.WebSocket.OPEN) throw new Error("WebSocket is not in the OPEN state");
this.websocket.send(e);
};
e.prototype.required = function(e) {
var t = this;
return new e(function(n, o, i) {
try {
var s = t.invokeId++;
t.send(r.Envelope.encode({
header: {
invokeBegin: {
invokeId: s,
method: e.name + "." + n.name
}
},
payload: o
}).finish());
t.callbacks.set(s, i);
} catch (e) {
e instanceof Error ? i(e, null) : i(new Error("rpc: encode error"), null);
}
}, !1, !1);
};
e.prototype.on = function(e, t) {
this.listeners.has(e) || this.listeners.set(e, []);
this.listeners.get(e).push(t);
return this;
};
e.prototype.off = function(e, t) {
void 0 === e ? this.listeners.clear() : void 0 === t ? this.listeners.delete(e) : this.listeners.has(e) && this.listeners.set(e, this.listeners.get(e).filter(function(e) {
return e != t;
}));
return this;
};
e.prototype.emit = function(e, t) {
this.send(r.Envelope.encode({
header: {
event: e
},
payload: t
}).finish());
};
e.prototype.close = function() {
if (this.websocket) {
this.websocket.onclose = function() {};
this.websocket.onmessage = function() {};
this.websocket.onerror = function() {};
this.websocket.close();
this.websocket = void 0;
}
var e = new Error("WebSocket closed");
for (var t in this.callbacks.values()) setTimeout(this.callbacks[t], 0, e, null);
this.callbacks.clear();
};
return e;
}();
n.ClientChannel = a;
cc._RF.pop();
}, {
"../../Protocol/bundle": "bundle",
"../Manager/FormMgr": "FormMgr",
"../UIConfig": "UIConfig",
"../UIScript/UIToast": "UIToast"
} ],
CocosHelper: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "7d011DLhkxFPakrCRw6PZqk", "CocosHelper");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
n.LoadProgress = void 0;
cc.Tween.prototype.speed = function(e) {
this._finalAction._speedMethod = !0;
this._finalAction._speed = e;
};
cc.Tween.prototype.pause = function() {
this._finalAction.paused = !0;
};
cc.Tween.prototype.resume = function() {
this._finalAction.paused = !1;
};
cc.Tween.prototype.duration = function() {
return this._finalAction._duration;
};
cc.Tween.prototype.elapsed = function() {
return this._finalAction._elapsed;
};
cc.Tween.prototype.goto = function(e) {
this._finalAction._goto = !0;
this._finalAction._elapsed = e;
};
var i = e("../Manager/GameMgr"), s = function() {};
n.LoadProgress = s;
var a = function() {
function e() {}
e.runRepeatTweenSync = function(e, t) {
for (var n = [], i = 2; i < arguments.length; i++) n[i - 2] = arguments[i];
return r(this, void 0, void 0, function() {
return o(this, function() {
return [ 2, new Promise(function(r) {
for (var o = cc.tween(e), i = 0, s = n; i < s.length; i++) {
var a = s[i];
o = o.then(a);
}
t < 0 ? cc.tween(e).repeatForever(o).start() : cc.tween(e).repeat(t, o).call(function() {
r(!0);
}).start();
}) ];
});
});
};
e.runTweenSync = function(e) {
for (var t = [], n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
return r(this, void 0, Promise, function() {
return o(this, function() {
return [ 2, new Promise(function(n) {
for (var r = cc.tween(e), o = 0, i = t; o < i.length; o++) {
var s = i[o];
r = r.then(s);
}
r.call(function() {
n();
}).start();
}) ];
});
});
};
e.stopTween = function(e) {
cc.Tween.stopAllByTarget(e);
};
e.prototype.stopTweenByTag = function(e) {
cc.Tween.stopAllByTag(e);
};
e.runAnimSync = function(t, n) {
return r(this, void 0, void 0, function() {
var r, i, s, a;
return o(this, function(o) {
switch (o.label) {
case 0:
if (!(r = t.getComponent(cc.Animation))) return [ 2 ];
i = null;
if (n) {
s = r.getClips();
if ("number" == typeof n) i = s[n]; else if ("string" == typeof n) for (a = 0; a < s.length; a++) if (s[a].name === n) {
i = s[a];
break;
}
} else i = r.defaultClip;
return i ? [ 4, e.sleepSync(i.duration) ] : [ 2 ];

case 1:
o.sent();
return [ 2 ];
}
});
});
};
e.loadResThrowErrorSync = function() {
return null;
};
e.loadRes = function(e, t, n) {
var r = this;
if (this._loadingMap[e]) this._loadingMap[e].push(n); else {
this._loadingMap[e] = [ n ];
this.loadResSync(e, t).then(function(t) {
for (var n = 0, o = r._loadingMap[e]; n < o.length; n++) (0, o[n])(t);
r._loadingMap[e] = null;
delete r._loadingMap[e];
});
}
};
e.loadResSync = function(e, t, n) {
var r = this;
return new Promise(function(o) {
n || (n = r._onProgress);
cc.resources.load(e, t, n, function(t, n) {
if (t) {
cc.error(e + " [资源加载] 错误 " + t);
o(null);
} else o(n);
});
});
};
e._onProgress = function(t, n, r) {
e.loadProgress.completedCount = t;
e.loadProgress.totalCount = n;
e.loadProgress.item = r;
e.loadProgress.cb && e.loadProgress.cb(t, n, r);
};
e.findChildInNode = function(e, t) {
if (t.name == e) return t;
for (var n = 0; n < t.childrenCount; n++) {
var r = this.findChildInNode(e, t.children[n]);
if (r) return r;
}
return null;
};
e.getComponentName = function(e) {
var t = e.name.match(/<.*>$/);
return t && t.length > 0 ? t[0].slice(1, -1) : e.name;
};
e.loadBundleSync = function(e, t) {
return new Promise(function(n) {
cc.assetManager.loadBundle(e, t, function(t, r) {
if (t) n(r); else {
cc.error("加载bundle失败, url: " + e + ", err:" + t);
n(null);
}
});
});
};
e.loadAssetFromBundleSync = function(e, t) {
var n = cc.assetManager.getBundle(e);
if (!n) {
cc.error("加载bundle中的资源失败, 未找到bundle, bundleUrl:" + e);
return null;
}
return new Promise(function(e) {
n.load(t, function(n, r) {
if (n) {
cc.error("加载bundle中的资源失败, 未找到asset, url:" + t + ", err:" + n);
e(null);
} else e(r);
});
});
};
e.loadAssetSync = function(e) {
var t = this;
return new Promise(function(n) {
cc.resources.load(e, function(r, o) {
if (r) {
t.addRef(o);
n(o);
} else {
cc.error("加载asset失败, url:" + e + ", err: " + r);
n(null);
}
});
});
};
e.releaseAsset = function(e) {
this.decRes(e);
};
e.addRef = function(e) {
if (e instanceof Array) for (var t = 0, n = e; t < n.length; t++) n[t].addRef(); else e.addRef();
};
e.decRes = function(e) {
if (e instanceof Array) for (var t = 0, n = e; t < n.length; t++) n[t].decRef(); else e.decRef();
};
e.captureScreen = function(e, t) {
var n = new cc.RenderTexture(), r = cc.rect(0, 0, cc.visibleRect.width, cc.visibleRect.height);
t && (r = t instanceof cc.Node ? t.getBoundingBoxToWorld() : t);
n.initWithSize(cc.visibleRect.width, cc.visibleRect.height, cc.game._renderContext.STENCIL_INDEX8);
e.targetTexture = n;
e.render();
var o = new ArrayBuffer(r.width * r.height * 4), i = new Uint8Array(o);
n.readPixels(i, r.x, r.y, r.width, r.height);
return i;
};
e.loadJsonSync = function(e, t) {
return new Promise(function(n) {
cc.resources.load(e, t, function(t, r) {
if (t) {
cc.error(e + " [资源加载] 错误 " + t);
n(null);
} else n(r);
});
});
};
e.getContenerindex = function(e) {
if (e && !(e.childrenCount < 1)) for (var t = 0; t < e.childrenCount; t++) if (e.children[t].getComponent(cc.Toggle).isChecked) return t;
};
e.loadHead = function(e, t) {
return new Promise(function(t) {
cc.assetManager.loadRemote(e, function(e, n) {
if (e) {
console.log("headErr:" + e);
t(null);
} else t(n);
});
}).then(function(e) {
t.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(e);
});
};
e.setItemProduceIcon = function(e, t, n) {
return new Promise(function(t) {
cc.resources.load("imgs/itemProduce/item_" + e, function(e, n) {
t(e ? null : n);
});
}).then(function(e) {
t.spriteFrame = new cc.SpriteFrame(e);
n();
});
};
e.getItemProduceInfoById = function(e) {
for (var t = i.default.dataModalMgr.ItemProfucesJson.itemsInfo.json, n = 0; n < t.length; n++) if (t[n].id == e) return t[n];
};
e.setLotteryIcon = function(e, t) {
return new Promise(function(t) {
cc.resources.load("imgs/itemProduce/item_" + e, function(e, n) {
t(e ? null : n);
});
}).then(function(e) {
t.spriteFrame = new cc.SpriteFrame(e);
});
};
e.formatTimeForSecond = function(e) {
var t = "", n = e % 60, r = Math.floor(e / 60);
r = r < 0 ? 0 : r;
var o = Math.floor(r / 60), i = r % 60;
if (o > 0) {
t += o > 9 ? o.toString() : "0" + o;
t += ":";
}
t += i > 9 ? i.toString() : "0" + i;
return (t += ":") + (n > 9 ? n.toString() : "0" + n);
};
e.loadProgress = new s();
e.sleepSync = function(e) {
return new Promise(function(t) {
cc.Canvas.instance.scheduleOnce(function() {
t(!0);
}, e);
});
};
e._loadingMap = {};
return e;
}();
n.default = a;
cc._RF.pop();
}, {
"../Manager/GameMgr": "GameMgr"
} ],
ConfigMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "99faeZt2e5AobpvQuyUBgaI", "ConfigMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("../Utils/CocosHelper"), s = e("./GameMgr"), a = function() {
function e() {}
e.prototype.loadConfigs = function() {
return r(this, void 0, void 0, function() {
var e;
return o(this, function(t) {
switch (t.label) {
case 0:
e = s.default.dataModalMgr.ItemProfucesJson;
return [ 4, i.default.loadJsonSync("JsonFile/items", cc.JsonAsset) ];

case 1:
e.itemsInfo = t.sent();
console.log("");
return [ 2 ];
}
});
});
};
return e;
}();
n.default = a;
cc._RF.pop();
}, {
"../Utils/CocosHelper": "CocosHelper",
"./GameMgr": "GameMgr"
} ],
ContentAdapter: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "7463e2/Hf1Hjq2+JgTMTs7t", "ContentAdapter");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = (s.property, function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.onLoad = function() {
var e = Math.min(cc.view.getCanvasSize().width / this.node.width, cc.view.getCanvasSize().height / this.node.height), t = this.node.width * e, n = this.node.height * e;
this.node.width = this.node.width * (cc.view.getCanvasSize().width / t);
this.node.height = this.node.height * (cc.view.getCanvasSize().height / n);
};
return i([ a ], t);
}(cc.Component));
n.default = c;
cc._RF.pop();
}, {} ],
DataBag: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "52db2OIKg1MD6nep22RXot2", "DataBag");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.DataBagItem = n.DataBagInfo = void 0;
n.DataBagInfo = function() {
this.list_item = Array();
this.cur_bag_capacity = 0;
this.total_bag_capacity = 6;
};
n.DataBagItem = function() {};
cc._RF.pop();
}, {} ],
DataCollection: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "5e849aoGq9Gf5KX+/JssUhF", "DataCollection");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.DataCollection = void 0;
var r = function() {
function e() {
this._curWFSBProgress = 0;
this._WFSBGetMoney = 0;
this._curSJZProgress = 0;
this._SJZGetMoney = 0;
this._curSDMZProgress = 0;
this._SDMZGetMoney = 0;
this._curSDSSProgress = 0;
this._SDSSGetMoney = 0;
this._curBDJProgress = 0;
this._BDJGetMoney = 0;
this._curJCProgress = 0;
this._JCDescribe = "";
this._curPXProgress = 0;
this._PXDescribe = "";
this._curJLProgress = 0;
this._JLDescribe = "";
}
Object.defineProperty(e.prototype, "curWFSBProgress", {
get: function() {
void 0 === this._curWFSBProgress && (this._curWFSBProgress = 0);
return this._curWFSBProgress;
},
set: function(e) {
this._curWFSBProgress = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "WFSBGetMoney", {
get: function() {
void 0 === this._WFSBGetMoney && (this._WFSBGetMoney = 0);
return this._WFSBGetMoney;
},
set: function(e) {
this._WFSBGetMoney = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "curSJZProgress", {
get: function() {
void 0 === this._curSJZProgress && (this._curSJZProgress = 0);
return this._curSJZProgress;
},
set: function(e) {
this._curSJZProgress = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "SJZGetMoney", {
get: function() {
void 0 === this._SJZGetMoney && (this._SJZGetMoney = 0);
return this._SJZGetMoney;
},
set: function(e) {
this._SJZGetMoney = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "curSDMZProgress", {
get: function() {
void 0 === this._curSDMZProgress && (this._curSDMZProgress = 0);
return this._curSDMZProgress;
},
set: function(e) {
this._curSDMZProgress = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "SDMZGetMoney", {
get: function() {
void 0 === this._SDMZGetMoney && (this._SDMZGetMoney = 0);
return this._SDMZGetMoney;
},
set: function(e) {
this._SDMZGetMoney = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "curSDSSProgress", {
get: function() {
void 0 === this._curSDSSProgress && (this._curSDSSProgress = 0);
return this._curSDSSProgress;
},
set: function(e) {
this._curSDSSProgress = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "SDSSGetMoney", {
get: function() {
void 0 === this._SDSSGetMoney && (this._SDSSGetMoney = 0);
return this._SDSSGetMoney;
},
set: function(e) {
this._SDSSGetMoney = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "curBDJProgress", {
get: function() {
void 0 === this._curBDJProgress && (this._curBDJProgress = 0);
return this._curBDJProgress;
},
set: function(e) {
this._curBDJProgress = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "BDJGetMoney", {
get: function() {
void 0 === this._BDJGetMoney && (this._BDJGetMoney = 0);
return this._BDJGetMoney;
},
set: function(e) {
this._BDJGetMoney = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "curJCProgress", {
get: function() {
void 0 === this._curJCProgress && (this._curJCProgress = 0);
return this._curJCProgress;
},
set: function(e) {
this._curJCProgress = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "JCDescribe", {
get: function() {
void 0 === this._JCDescribe && (this._JCDescribe = "");
return this._JCDescribe;
},
set: function(e) {
this._JCDescribe = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "curPXProgress", {
get: function() {
void 0 === this._curPXProgress && (this._curPXProgress = 0);
return this._curPXProgress;
},
set: function(e) {
this._curPXProgress = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "PXDescribe", {
get: function() {
void 0 === this._PXDescribe && (this._PXDescribe = "");
return this._PXDescribe;
},
set: function(e) {
this._PXDescribe = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "curJLProgress", {
get: function() {
void 0 === this._curJLProgress && (this._curJLProgress = 0);
return this._curJLProgress;
},
set: function(e) {
this._curJLProgress = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "JLDescribe", {
get: function() {
void 0 === this._JLDescribe && (this._JLDescribe = "");
return this._JLDescribe;
},
set: function(e) {
this._JLDescribe = e;
},
enumerable: !1,
configurable: !0
});
return e;
}();
n.DataCollection = r;
cc._RF.pop();
}, {} ],
DataFriend: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a9d27ygiTFIVKNNK8n/l1hH", "DataFriend");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.DataItemSeeDetail = n.DataSeeDetail = n.DataItemFriendGiveDetail = n.DataFriendGiveDetail = n.DataItemFriendMoneyDetail = n.DataFriendMoneyDetail = n.DataFriend = void 0;
n.DataFriend = function() {
this.my_friend_num = 0;
this.probabyly_money = 0;
this.today_get_money = 0;
this.total_get_money = 0;
this.today_extar_get_money = 0;
this.cur_get_money = 0;
this.my_inviter_name = "";
this.my_invite_head = "";
};
n.DataFriendMoneyDetail = function() {
this.today_get_money = 0;
this.total_get_money = 0;
this.list_item_friend_money_detail = Array();
};
n.DataItemFriendMoneyDetail = function() {
this.get_data = "";
this.one_friend = 0;
this.two_friend = 0;
this.get_total = 0;
};
n.DataFriendGiveDetail = function() {
this.list_item_friend_give_detail = Array();
};
n.DataItemFriendGiveDetail = function() {
this.user_name = "";
this.user_id = 0;
this.user_head = "";
this.give_time = "";
this.give_num = 0;
};
n.DataSeeDetail = function() {
this.list_item_friend_give_detail = Array();
};
n.DataItemSeeDetail = function() {
this.user_name = "";
this.user_id = 0;
this.user_head = "";
this.see_time = "";
};
cc._RF.pop();
}, {} ],
DataGetMoneyLog: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "8fdd6kyxBJPu53TUeDnNh5t", "DataGetMoneyLog");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.DataItemGetMoneyLog = n.DataGetMoneyLog = void 0;
n.DataGetMoneyLog = function() {
this.list_get_money_log = Array();
};
n.DataItemGetMoneyLog = function() {
this.get_data = "";
this.get_money_num = 0;
};
cc._RF.pop();
}, {} ],
DataModalMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "d47afYEAFhDBI9nPCEezcSK", "DataModalMgr");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.DataModalMgr = void 0;
var r = e("../DataModal/DataBag"), o = e("../DataModal/DataCollection"), i = e("../DataModal/DataFriend"), s = e("../DataModal/DataGetMoneyLog"), a = e("../DataModal/DataMyExtar"), c = e("../DataModal/DataProduce"), l = e("../DataModal/DataShop"), u = e("../DataModal/DataUser");
n.DataModalMgr = function() {
this.UserInfo = new u.UserInfo();
this.CurBuyProduceInfo = new c.DataCurBuyProduce();
this.ItemProfucesJson = new c.ProducesJson();
this.CollectionInfo = new o.DataCollection();
this.BagInfo = new r.DataBagInfo();
this.ShopInfo = new l.DataShop();
this.FriendInfo = new i.DataFriend();
this.FriendMonerDetailInfo = new i.DataFriendMoneyDetail();
this.DataGetMoneyLogInfo = new s.DataGetMoneyLog();
this.DataMyExtarInfo = new a.DataMyExtar();
this.DataFriendGiveDetailInfo = new i.DataFriendGiveDetail();
this.DataSeeDetailInfo = new i.DataSeeDetail();
};
cc._RF.pop();
}, {
"../DataModal/DataBag": "DataBag",
"../DataModal/DataCollection": "DataCollection",
"../DataModal/DataFriend": "DataFriend",
"../DataModal/DataGetMoneyLog": "DataGetMoneyLog",
"../DataModal/DataMyExtar": "DataMyExtar",
"../DataModal/DataProduce": "DataProduce",
"../DataModal/DataShop": "DataShop",
"../DataModal/DataUser": "DataUser"
} ],
DataMyExtar: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "0f0afHSrQJNkrg6A0LbouVY", "DataMyExtar");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.DataMyExtar = void 0;
n.DataMyExtar = function() {
this.today_extar_get_money = 0;
this.today_extar_add_user = 0;
this.month_extar_get_money = 0;
this.month_extar_add_user = 0;
this.total_extar_get_money = 0;
this.total_extar_add_user = 0;
this.cur_stage = "";
this.cur_stage_money = 0;
this.total_stage_moner = 0;
this.add_mult = 0;
this.complete_money = 0;
};
cc._RF.pop();
}, {} ],
DataProduce: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "cb104U/zSdKjZtkItAsSNb3", "DataProduce");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.ProducesJson = n.DataCurBuyProduce = void 0;
var r = function() {
function e() {
this._id = "0";
this._name = "0";
this._img = "0";
this._buyPrice = 0;
}
Object.defineProperty(e.prototype, "id", {
get: function() {
void 0 === this._id && (this._id = "0");
return this._id;
},
set: function(e) {
this._id = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "name", {
get: function() {
void 0 === this._name && (this._name = "0");
return this._name;
},
set: function(e) {
this._name = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "img", {
get: function() {
void 0 === this._img && (this._img = "0");
return this._img;
},
set: function(e) {
this._img = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "buyPrice", {
get: function() {
void 0 === this._buyPrice && (this._buyPrice = 0);
return this._buyPrice;
},
set: function(e) {
this._buyPrice = e;
},
enumerable: !1,
configurable: !0
});
return e;
}();
n.DataCurBuyProduce = r;
n.ProducesJson = function() {};
cc._RF.pop();
}, {} ],
DataShop: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "bd5f86TYsJLkanbpbzBMa08", "DataShop");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.DataShopItem = n.DataShop = void 0;
n.DataShop = function() {
this.list_item = Array();
};
n.DataShopItem = function() {
this.item_sp = "";
this.item_name = "";
this.item_buy_money = 0;
this.item_buy_sp = "";
};
cc._RF.pop();
}, {} ],
DataUser: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "bffc2P1v01J06LhjMuNsq97", "DataUser");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.UserInfo = void 0;
var r = e("../Utils/CocosHelper"), o = function() {
function e() {
this._userName = "";
this._userAvatarUrl = "";
this._userAccount = "";
this._userCoin = 0;
this._userDiamond = 0;
this._userMoneyUints = 0;
this._userMoneyNanos = 0;
this._userCurNeedBuyCoinNum = 0;
this._userMakeCoin = 0;
this._userWorkbench = new Array();
this._userUnlockLevel = 1;
this._combineAutoTime = 15;
this._UserInfos = null;
}
Object.defineProperty(e.prototype, "userName", {
get: function() {
void 0 === this._userName && (this._userName = "");
return this._userName;
},
set: function(e) {
this._userName = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userAvatarUrl", {
get: function() {
void 0 === this._userAvatarUrl && (this._userAvatarUrl = "");
return this._userAvatarUrl;
},
set: function(e) {
this._userAvatarUrl = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userAccount", {
get: function() {
void 0 === this._userAccount && (this._userAccount = "");
return this._userAccount;
},
set: function(e) {
this._userAccount = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userCoin", {
get: function() {
void 0 === this._userCoin && (this._userCoin = 0);
return this._userCoin;
},
set: function(e) {
this._userCoin = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userDiamond", {
get: function() {
void 0 === this._userDiamond && (this._userDiamond = 0);
return this._userDiamond;
},
set: function(e) {
this._userDiamond = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userMoneyUints", {
get: function() {
void 0 === this._userMoneyUints && (this._userMoneyUints = 0);
return this._userMoneyUints;
},
set: function(e) {
this._userMoneyUints = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userMoneyNanos", {
get: function() {
void 0 === this._userMoneyNanos && (this._userMoneyNanos = 0);
return this._userMoneyNanos;
},
set: function(e) {
this._userMoneyNanos = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userCurNeedBuyCoinNum", {
get: function() {
void 0 === this._userCurNeedBuyCoinNum && (this._userCurNeedBuyCoinNum = 0);
return this._userCurNeedBuyCoinNum;
},
set: function(e) {
this._userCurNeedBuyCoinNum = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userMakeCoin", {
get: function() {
void 0 === this._userMakeCoin && (this._userMakeCoin = 0);
return this._userMakeCoin;
},
set: function(e) {
this._userMakeCoin = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userWorkbench", {
get: function() {
void 0 === this._userWorkbench && (this._userWorkbench = []);
return this._userWorkbench;
},
set: function(e) {
this._userWorkbench = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userUnlockLevel", {
get: function() {
void 0 === this._userUnlockLevel && (this._userUnlockLevel = 1);
return this._userUnlockLevel;
},
set: function(e) {
this._userUnlockLevel = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "combineAutoTime", {
get: function() {
void 0 === this._combineAutoTime && (this._combineAutoTime = 15);
return this._combineAutoTime;
},
set: function(e) {
this._combineAutoTime = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(e.prototype, "userInfos", {
get: function() {
return this._UserInfos;
},
set: function(e) {
this._UserInfos = e;
},
enumerable: !1,
configurable: !0
});
e.prototype.hasPosAtWorkbench = function() {
for (var e = 0; e < this._userWorkbench.length; e++) if (0 === this._userWorkbench[e]) return !0;
return !1;
};
e.prototype.addCakeToWorkbench = function(e) {
if (!this.hasPosAtWorkbench()) return -1;
for (var t = 0; t < this._userWorkbench.length; t++) if (0 === this._userWorkbench[t]) {
this._userWorkbench[t] = e;
return t;
}
};
e.prototype.combineCake = function(e, t, n) {
if (!this._userWorkbench) {
n(!1);
return !1;
}
if (this._userWorkbench.length <= e) {
n(!1);
return !1;
}
if (this._userWorkbench.length <= t) {
n(!1);
return !1;
}
var o = this._userWorkbench[t];
if (this._userWorkbench[e] !== o) {
n(!1);
return !1;
}
var i = r.default.getItemProduceInfoById(o);
if (!i) {
n(!1);
return !1;
}
if (!i.nextLevel) {
n(!1, "maxLevel");
return !1;
}
this._userWorkbench[t] = i.nextLevel;
this._userWorkbench[e] = 0;
var s = i.nextLevel;
n(!0, this.unlockProduceLevel(s));
return !0;
};
e.prototype.unlockProduceLevel = function(e) {
if (e > this._userUnlockLevel) {
for (var t = !0, n = 0; n < this._userWorkbench.length; n++) if (e < Number(this._userWorkbench[n])) {
t = !1;
break;
}
this._userUnlockLevel = e;
return t;
}
return !1;
};
return e;
}();
n.UserInfo = o;
cc._RF.pop();
}, {
"../Utils/CocosHelper": "CocosHelper"
} ],
EventCenter: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "15cf5C/OS9IyY5TeN5ujTW/", "EventCenter");
var r = this && this.__spreadArrays || function() {
for (var e = 0, t = 0, n = arguments.length; t < n; t++) e += arguments[t].length;
var r = Array(e), o = 0;
for (t = 0; t < n; t++) for (var i = arguments[t], s = 0, a = i.length; s < a; s++, 
o++) r[o] = i[s];
return r;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
n.EventCenter = n.EventInfo = void 0;
var o = e("../Common/Pool"), i = function() {
function e() {}
e.prototype.free = function() {
this.callback = null;
this.target = null;
this.once = !1;
};
e.prototype.init = function(e, t, n) {
this.callback = e;
this.target = t;
this.once = n;
};
return e;
}();
n.EventInfo = i;
var s = function(e, t, n) {
this.eventName = e;
this.callback = t;
this.targetId = n;
}, a = 1, c = function() {
function e() {}
e.on = function(e, t, n, r) {
void 0 === n && (n = void 0);
void 0 === r && (r = !1);
var o = (n = n || this).uuid || n.id;
void 0 === o && (n.uuid = o = "" + a++);
this.onById(e, o, n, t, r);
};
e.once = function(e, t, n) {
void 0 === n && (n = void 0);
this.on(e, t, n, !0);
};
e.onById = function(e, t, n, r, o) {
var i = this._listeners[e];
i || (i = this._listeners[e] = {});
var s = i[t];
s || (s = i[t] = []);
var a = this._eventPool.alloc();
a.init(r, n, o);
s.push(a);
};
e.off = function(e, t, n) {
void 0 === n && (n = void 0);
var r = (n = n || this).uuid || n.id;
r && this.offById(e, t, r);
};
e.targetOff = function(e) {
var t = (e = e || this).uuid || e.id;
if (t) for (var n in this._listeners) {
var r = this._listeners[n];
void 0 !== r[t] && delete r[t];
}
};
e.offById = function(e, t, n) {
if (this._dispatching > 0) {
var r = new s(e, t, n);
this._removeCommands.push(r);
} else this.doOff(e, t, n);
};
e.doOff = function(e, t, n) {
var r = this._listeners[e];
if (r) {
var o = r[n];
if (o) {
for (var i = o.length - 1; i >= 0; i--) o[i].callback === t && o.splice(i, 1);
if (0 === o.length) {
r[n] = null;
delete r[n];
}
}
}
};
e.doRemoveCommands = function() {
if (0 === this._dispatching) {
for (var e = 0, t = this._removeCommands; e < t.length; e++) {
var n = t[e];
this.doOff(n.eventName, n.callback, n.targetId);
}
this._removeCommands.length = 0;
}
};
e.emit = function(e) {
for (var t, n = [], o = 1; o < arguments.length; o++) n[o - 1] = arguments[o];
var i = this._listeners[e];
if (!i) return !1;
this._dispatching++;
for (var a in i) for (var c = 0, l = i[a]; c < l.length; c++) {
var u = l[c];
(t = u.callback).call.apply(t, r([ u.target ], n));
if (u.once) {
var p = new s(e, u.callback, a);
this._removeCommands.push(p);
}
}
this._dispatching--;
this.doRemoveCommands();
};
e._listeners = cc.js.createMap();
e._dispatching = 0;
e._removeCommands = [];
e._eventPool = new o.Pool(function() {
return new i();
}, 10);
return e;
}();
n.EventCenter = c;
cc._RF.pop();
}, {
"../Common/Pool": "Pool"
} ],
EventType: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "d91c3NJBEVLOImFlZf1j8Q7", "EventType");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.EventType = void 0;
(function(e) {
e.GameShow = "GameShow";
e.GameHide = "GameHide";
e.WindowClosed = "WindowClosed";
e.FormClosed = "FormClosed";
})(n.EventType || (n.EventType = {}));
cc._RF.pop();
}, {} ],
FixedMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "6d5adVytGZORIxY7M3VpixZ", "FixedMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./UIManager"), s = function() {
function e() {}
e.prototype.open = function(e, t, n) {
return r(this, void 0, void 0, function() {
return o(this, function(r) {
switch (r.label) {
case 0:
return [ 4, i.default.getInstance().openForm(e, t, n) ];

case 1:
return [ 2, r.sent() ];
}
});
});
};
e.prototype.close = function(e) {
return r(this, void 0, void 0, function() {
return o(this, function(t) {
switch (t.label) {
case 0:
return [ 4, i.default.getInstance().closeForm(e) ];

case 1:
return [ 2, t.sent() ];
}
});
});
};
return e;
}();
n.default = new s();
cc._RF.pop();
}, {
"./UIManager": "UIManager"
} ],
FormMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "76868mpc19BXqdXK3hLV50J", "FormMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("../Common/SysDefine"), s = e("./FixedMgr"), a = e("./SceneMgr"), c = e("./TipsMgr"), l = e("./WindowMgr"), u = function() {
function e() {}
e.prototype.open = function(e, t, n) {
return r(this, void 0, void 0, function() {
return o(this, function(r) {
switch (r.label) {
case 0:
switch (e.type) {
case i.FormType.Screen:
return [ 3, 1 ];

case i.FormType.Window:
return [ 3, 3 ];

case i.FormType.Fixed:
return [ 3, 5 ];

case i.FormType.Tips:
return [ 3, 7 ];
}
return [ 3, 9 ];

case 1:
return [ 4, a.default.open(e.prefabUrl, t, n) ];

case 2:
return [ 2, r.sent() ];

case 3:
return [ 4, l.default.open(e.prefabUrl, t, n) ];

case 4:
return [ 2, r.sent() ];

case 5:
return [ 4, s.default.open(e.prefabUrl, t, n) ];

case 6:
return [ 2, r.sent() ];

case 7:
return [ 4, c.default.open(e.prefabUrl, t, n) ];

case 8:
return [ 2, r.sent() ];

case 9:
cc.error("未知类型的窗体: " + e.type);
return [ 2, null ];
}
});
});
};
e.prototype.close = function(e) {
return r(this, void 0, void 0, function() {
return o(this, function(t) {
switch (t.label) {
case 0:
switch (e.type) {
case i.FormType.Screen:
return [ 3, 1 ];

case i.FormType.Window:
return [ 3, 3 ];

case i.FormType.Fixed:
return [ 3, 5 ];

case i.FormType.Tips:
return [ 3, 7 ];
}
return [ 3, 9 ];

case 1:
return [ 4, a.default.close(e.prefabUrl) ];

case 2:
return [ 2, t.sent() ];

case 3:
return [ 4, l.default.close(e.prefabUrl) ];

case 4:
return [ 2, t.sent() ];

case 5:
return [ 4, s.default.close(e.prefabUrl) ];

case 6:
return [ 2, t.sent() ];

case 7:
return [ 4, c.default.close(e.prefabUrl) ];

case 8:
return [ 2, t.sent() ];

case 9:
cc.error("未知类型的窗体: " + e.type);
return [ 2, null ];
}
});
});
};
e.prototype.backScene = function(e, t) {
return r(this, void 0, void 0, function() {
return o(this, function() {
return [ 2, a.default.back(e, t) ];
});
});
};
e.prototype.closeAllWindows = function() {
return r(this, void 0, void 0, function() {
return o(this, function(e) {
switch (e.label) {
case 0:
return [ 4, l.default.closeAll() ];

case 1:
e.sent();
return [ 2 ];
}
});
});
};
return e;
}();
n.default = new u();
cc._RF.pop();
}, {
"../Common/SysDefine": "SysDefine",
"./FixedMgr": "FixedMgr",
"./SceneMgr": "SceneMgr",
"./TipsMgr": "TipsMgr",
"./WindowMgr": "WindowMgr"
} ],
GameConfig: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "f46b13WB51JHaffBN5ie56i", "GameConfig");
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e() {}
Object.defineProperty(e, "debugUserId", {
get: function() {
return this._debugUserId;
},
enumerable: !1,
configurable: !0
});
e.gameId = "";
e.version = "0.0.1";
e._debugUserId = "";
return e;
}();
n.default = r;
cc._RF.pop();
}, {} ],
GameMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "30ba5eNytVM3pckqs/AGbqm", "GameMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./ConfigMgr"), s = e("./DataModalMgr"), a = function() {
function e() {
this.inited = !1;
this.configMgr = null;
this.clientChannel = null;
this.dataModalMgr = null;
}
e.prototype.init = function() {
return r(this, void 0, void 0, function() {
return o(this, function(e) {
switch (e.label) {
case 0:
this.configMgr = new i.default();
this.dataModalMgr = new s.DataModalMgr();
return [ 4, this.configMgr.loadConfigs() ];

case 1:
e.sent();
this.inited = !0;
return [ 2 ];
}
});
});
};
e.prototype.onGameShow = function() {};
e.prototype.onGameHide = function() {};
e.prototype.update = function() {
this.inited;
};
return e;
}();
n.default = new a();
cc._RF.pop();
}, {
"./ConfigMgr": "ConfigMgr",
"./DataModalMgr": "DataModalMgr"
} ],
GenerateQR: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "2bb095/a31FHo6dokcSVtRb", "GenerateQR");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = (s.property, function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.onLoad = function() {
this.init("http://baidu.com");
};
t.prototype.init = function(e) {
var t = this.node.addComponent(cc.Graphics);
"string" == typeof e ? this.createQR(t, e) : console.log("url is not string", e);
};
t.prototype.createQR = function(e, t) {
var n = new QRCode(-1, QRErrorCorrectLevel.H);
n.addData(t);
n.make();
e.fillColor = cc.Color.BLACK;
for (var r = this.node.width / n.getModuleCount(), o = this.node.height / n.getModuleCount(), i = 0; i < n.getModuleCount(); i++) for (var s = 0; s < n.getModuleCount(); s++) if (n.isDark(i, s)) {
var a = Math.ceil((s + 1) * r) - Math.floor(s * r), c = Math.ceil((i + 1) * r) - Math.floor(i * r);
e.rect(Math.round(s * r) - this.node.width / 2, Math.round(i * o) - this.node.height / 2, a, c);
e.fill();
}
};
return i([ a ], t);
}(cc.Component));
n.default = c;
cc._RF.pop();
}, {} ],
HotUpdateMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "78d3a8ppNJDa7ONQzcot2ZE", "HotUpdateMgr");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.label = null;
t.versionStr = null;
t.storagePath = "";
t.am = null;
t.updating = !1;
t.mainifestUrl = null;
t.remoteVersion = "";
t.HotUpdateSearchPaths = "HotUpdateSearchPaths";
t.remotePath = "remotePath";
t.curVersion = "curVersion";
return t;
}
t.prototype.onLoad = function() {
this.versionStr.string = this.getCurVesion();
this.storagePath = this.getRootPath();
cc.log("远程版本缓存路径 : " + this.storagePath);
this.am = new jsb.AssetsManager("", this.storagePath, this.versionCompareHanle);
this.am.setVerifyCallback(this.setVerifycb.bind(this));
cc.sys.isNative && this.checkUpdate();
};
t.prototype.setVerifycb = function(e, t) {
var n = t.compressed, r = t.md5, o = t.path, i = t.size;
cc.log("assetPath", e);
cc.log("assetSize:", i);
if (n) {
this.label.string = "检查压缩 : " + o;
return !0;
}
this.label.string = "检查压缩 : " + o + " (" + r + ")";
return !0;
};
t.prototype.versionCompareHanle = function(e, t) {
console.log("当前版本 :  " + e + " , 远程版本 : " + t);
for (var n = e.split("."), r = t.split("."), o = 0; o < n.length && o < r.length; ++o) if (parseInt(n[o]) !== parseInt(r[o])) return -1;
return r.length > n.length ? -1 : 0;
};
t.prototype.checkUpdate = function() {
if (!this.updating) {
var e = this.mainifestUrl.nativeUrl;
cc.log("原包版本信息url:", this.mainifestUrl.nativeUrl);
if (this.am.getState() === jsb.AssetsManager.State.UNINITED) {
cc.loader.md5Pipe && (e = cc.loader.md5Pipe.transformURL(e));
this.am.loadLocalManifest(e);
}
if (this.am.getLocalManifest() && this.am.getLocalManifest().isLoaded()) {
this.am.setEventCallback(this.checkCb.bind(this));
this.am.checkUpdate();
this.updating = !0;
} else this.label.string = "加载本地manifest文件失败";
}
};
t.prototype.checkCb = function(e) {
cc.log("Code: " + e.getEventCode());
switch (e.getEventCode()) {
case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
this.label.string = "没有本地manifest文件，跳过热更.";
break;

case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
this.label.string = "下载远程manifest文件失败，跳过热更.";
break;

case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
this.label.string = "已经更新到远程最新版本.";
break;

case jsb.EventAssetsManager.NEW_VERSION_FOUND:
this.label.string = "发现新版本，请尝试热更";
break;

default:
return;
}
this.am.setEventCallback(null);
this.updating = !1;
};
t.prototype.hotUpdate = function() {
if (this.am && !this.updating) {
this.am.setEventCallback(this.updateCb.bind(this));
if (this.am.getState() === jsb.AssetsManager.State.UNINITED) {
var e = this.mainifestUrl.nativeUrl;
cc.loader.md5Pipe && (e = cc.loader.md5Pipe.transformURL(e));
this.am.loadLocalManifest(e);
}
this.am.update();
this.updating = !0;
}
};
t.prototype.updateCb = function(e) {
cc.log("热更回调");
var t = !1, n = !1, r = e.getMessage();
r && (this.label.string = "Updated file: " + r);
switch (e.getEventCode()) {
case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
this.label.string = "没有本地manifest文件，跳过热更.";
n = !0;
break;

case jsb.EventAssetsManager.UPDATE_PROGRESSION:
console.log("当前下载文件数", e.getDownloadedFiles());
console.log("总文件数", e.getTotalFiles());
var o = e.getMessage();
o && (this.label.string = "更新的文件：: " + o);
break;

case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
this.label.string = "下载远程manifest文件失败，跳过热更.";
n = !0;
break;

case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
this.label.string = "已经更新到远程最新版本.";
n = !0;
break;

case jsb.EventAssetsManager.UPDATE_FINISHED:
this.label.string = "更新完成，即将重启游戏. " + e.getMessage();
t = !0;
break;

case jsb.EventAssetsManager.UPDATE_FAILED:
this.label.string = "更新失败. " + e.getMessage();
this.updating = !1;
break;

case jsb.EventAssetsManager.ERROR_UPDATING:
this.label.string = "Asset 更新错误: " + e.getAssetId() + ", " + e.getMessage();
break;

case jsb.EventAssetsManager.ERROR_DECOMPRESS:
this.label.string = e.getMessage();
}
n && (this.updating = !1);
if (t) {
this.remoteVersion = this.getRemoteVersion();
cc.log("remoteversion:", this.remoteVersion);
this.am.setEventCallback(null);
var i = jsb.fileUtils.getSearchPaths(), s = this.am.getLocalManifest().getSearchPaths();
Array.prototype.unshift(i, s);
cc.sys.localStorage.setItem(this.HotUpdateSearchPaths, JSON.stringify(i));
jsb.fileUtils.setSearchPaths(i);
cc.sys.localStorage.setItem(this.curVersion, this.remoteVersion);
cc.game.restart();
}
};
t.prototype.getRemoteVersion = function() {
var e = this.getRootPath();
console.log("有下载的manifest文件", e);
var t = jsb.fileUtils.getStringFromFile(e + "/project.manifest");
return JSON.parse(t).version;
};
t.prototype.getRootPath = function() {
return (jsb.fileUtils ? jsb.fileUtils.getWritablePath() : "/") + this.remotePath;
};
t.prototype.getCurVesion = function() {
var e = cc.sys.localStorage.getItem(this.curVersion);
cc.log("curversion", e);
if (e) return e;
var t = this.getRootPath();
if (t = this.mainifestUrl.nativeUrl) {
var n = jsb.fileUtils.getStringFromFile(t);
e = JSON.parse(n).version;
console.log("当前版本号origin：", e);
}
return e;
};
t.prototype.configUpdate = function() {
cc.sys.localStorage.removeItem("curVersion");
var e = this.getCurVesion();
0 != this.versionCompareHanle(e, "1.1.0") ? this._modifyAppLoadUrlForManifestFile("远程服务器地址", this.mainifestUrl.nativeUrl) : console.log("小版本相同 进入登陆界面");
};
t.prototype._modifyAppLoadUrlForManifestFile = function(e, t) {
var n = !1;
if (jsb.fileUtils.isFileExist(jsb.fileUtils.getWritablePath() + "plane/project.manifest")) {
var r = this.getRootPath();
console.log("有下载的manifest文件", r);
var o = jsb.fileUtils.getStringFromFile(r + "/project.manifest"), i = JSON.parse(o);
i.packageUrl = e;
i.remoteManifestUrl = i.packageUrl + "project.manifest";
i.remoteVersionUrl = i.packageUrl + "version.manifest";
var s = JSON.stringify(i);
n = jsb.fileUtils.writeStringToFile(s, r + "/project.manifest");
} else {
var a = this.getRootPath();
jsb.fileUtils.isDirectoryExist(a) || jsb.fileUtils.createDirectory(a);
var c = t, l = jsb.fileUtils.getStringFromFile(c), u = JSON.parse(l);
u.packageUrl = e;
u.remoteManifestUrl = u.packageUrl + "project.manifest";
u.remoteVersionUrl = u.packageUrl + "version.manifest";
s = JSON.stringify(u);
n = jsb.fileUtils.writeStringToFile(s, a + "/project.manifest");
}
cc.log("Written Status : ", n);
n && this.checkUpdate();
};
i([ c(cc.Label) ], t.prototype, "label", void 0);
i([ c(cc.Label) ], t.prototype, "versionStr", void 0);
i([ c(cc.Asset) ], t.prototype, "mainifestUrl", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
HttpHelper: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "88ad0BlA9lMAY1NP0RXaOJQ", "HttpHelper");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = (s.property, function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.httpGet = function(e, t) {
var n = cc.loader.getXMLHttpRequest();
n.onreadystatechange = function() {
if (4 === n.readyState && 200 == n.status) {
var e = n.responseText;
cc.log("httpGet response:" + e);
var r = JSON.parse(e);
t(r);
} else 4 === n.readyState && 401 == n.status ? cc.log("httpGet response:", n.readyState) : cc.log("httpGet response:" + n.readyState);
};
cc.log("get url ", e);
n.open("GET", e, !0);
n.setRequestHeader("Access-Control-Allow-Origin", "*");
n.setRequestHeader("Access-Control-Allow-Methods", "GET, POST");
n.setRequestHeader("Access-Control-Allow-Headers", "x-requested-with,content-type,authorization");
n.setRequestHeader("Content-Type", "application/json");
n.timeout = 8e3;
n.send();
};
t.prototype.httpPost = function(e, t, n) {
var r = cc.loader.getXMLHttpRequest();
r.onreadystatechange = function() {
if (4 === r.readyState && 200 == r.status) {
var e = r.responseText;
cc.log("response:", e);
var t = JSON.parse(e);
n(t);
} else cc.log("httpPost response error ", r.readyState);
};
r.open("POST", e, !0);
r.setRequestHeader("Access-Control-Allow-Origin", "*");
r.setRequestHeader("Access-Control-Allow-Methods", "GET, POST");
r.setRequestHeader("Access-Control-Allow-Headers", "x-requested-with,content-type");
r.setRequestHeader("Content-Type", "application/json");
r.timeout = 8e3;
cc.log("send:", JSON.stringify(t));
r.send(JSON.stringify(t));
};
t.POST = function(e, t, n) {
void 0 === t && (t = {});
var r = cc.loader.getXMLHttpRequest();
r.open("POST", e, !0);
r.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
r.onreadystatechange = function() {
if (4 === r.readyState) {
var e = r.responseText;
r.status >= 200 && r.status < 300 ? n(!0, e) : n(!1, e);
}
};
cc.log("send param :" + t);
r.send(JSON.stringify(t));
};
return i([ a ], t);
}(cc.Component));
n.default = c;
cc._RF.pop();
}, {} ],
InterfaceMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "4a5c15ZXLJH9J7cFLh3WcX6", "InterfaceMgr");
Object.defineProperty(n, "__esModule", {
value: !0
});
cc._RF.pop();
}, {} ],
ItemBag: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "2c89dxwm1lJSYlLwMI663N5", "ItemBag");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Utils/CocosHelper"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.item_name = null;
t.item_sp = null;
return t;
}
t.prototype.setData = function(e) {
s.default.loadHead(e.item_sp, this.item_sp);
this.item_name.string = e.item_name + "+" + e.item_num;
};
i([ l(cc.Label) ], t.prototype, "item_name", void 0);
i([ l(cc.Sprite) ], t.prototype, "item_sp", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"../../Utils/CocosHelper": "CocosHelper"
} ],
ItemFriendExtraDetail: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "fb9c3YtKd9Lio7M6uVogfCp", "ItemFriendExtraDetail");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.data_lab = null;
t.friend_one_lab = null;
t.friend_two_lab = null;
t.total_lab = null;
return t;
}
t.prototype.setData = function(e) {
this.data_lab.string = e.get_data;
this.friend_one_lab.string = e.one_friend + "元";
this.friend_two_lab.string = e.two_friend + "元";
this.total_lab.string = e.get_total + "元";
};
i([ c(cc.Label) ], t.prototype, "data_lab", void 0);
i([ c(cc.Label) ], t.prototype, "friend_one_lab", void 0);
i([ c(cc.Label) ], t.prototype, "friend_two_lab", void 0);
i([ c(cc.Label) ], t.prototype, "total_lab", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
ItemFriendGiveDetail: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "7a59c7Lx65KT4x5ZdxMYmuJ", "ItemFriendGiveDetail");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.uname = null;
t.uid = null;
t.utime = null;
t.uhead = null;
t.givenum = null;
return t;
}
t.prototype.start = function() {};
t.prototype.setData = function() {};
i([ c(cc.Label) ], t.prototype, "uname", void 0);
i([ c(cc.Label) ], t.prototype, "uid", void 0);
i([ c(cc.Label) ], t.prototype, "utime", void 0);
i([ c(cc.Sprite) ], t.prototype, "uhead", void 0);
i([ c(cc.Label) ], t.prototype, "givenum", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
ItemFriendSeeDetail: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "2bda8vJmM1CbaeYq3QgXRRS", "ItemFriendSeeDetail");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.uname = null;
t.uid = null;
t.utime = null;
t.uhead = null;
t.seeBtn = null;
return t;
}
t.prototype.start = function() {
this.seeBtn.addClick(function() {
console.log("huifang");
}, this);
};
t.prototype.setData = function() {};
i([ l(cc.Label) ], t.prototype, "uname", void 0);
i([ l(cc.Label) ], t.prototype, "uid", void 0);
i([ l(cc.Label) ], t.prototype, "utime", void 0);
i([ l(cc.Sprite) ], t.prototype, "uhead", void 0);
i([ l(s.default) ], t.prototype, "seeBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"../../Common/Components/ButtonPlus": "ButtonPlus"
} ],
ItemFriendSee: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "c8311M1hzRAw68nMoNfGR8/", "ItemFriendSee");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.user_name = null;
t.user_id = null;
t.user_head = null;
t.see_btn = null;
return t;
}
t.prototype.start = function() {
this.see_btn.addClick(function() {
console.log("baifang");
}, this);
};
t.prototype.setData = function() {};
i([ l(cc.Label) ], t.prototype, "user_name", void 0);
i([ l(cc.Label) ], t.prototype, "user_id", void 0);
i([ l(cc.Sprite) ], t.prototype, "user_head", void 0);
i([ l(s.default) ], t.prototype, "see_btn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"../../Common/Components/ButtonPlus": "ButtonPlus"
} ],
ItemGetMoneyLog: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "86b442uxNVLvIjwO5NhtVRP", "ItemGetMoneyLog");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.data_lab = null;
t.get_money = null;
return t;
}
t.prototype.setData = function(e) {
this.data_lab.string = e.get_data;
this.get_money.string = e.get_money_num + "元";
};
i([ c(cc.Label) ], t.prototype, "data_lab", void 0);
i([ c(cc.Label) ], t.prototype, "get_money", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
ItemLottery: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "33722j4CsdN26lwVVhV89fi", "ItemLottery");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Utils/CocosHelper"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.icon = null;
return t;
}
t.prototype.setInfo = function(e) {
s.default.setLotteryIcon(e.icon, this.icon);
};
i([ l(cc.Sprite) ], t.prototype, "icon", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"../../Utils/CocosHelper": "CocosHelper"
} ],
ItemProduce: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "833b9VVoiRBu4HdjfWzaMYu", "ItemProduce");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Utils/CocosHelper"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.nodeIcon = null;
t.nodeBg = null;
t.nodeLevel = null;
t.txtLevel = null;
t.isDrag = !1;
t.isUsed = !1;
t._itemId = 0;
t._index = -1;
t._itemType = -1;
t._nextLevel = -1;
t._itemInfo = null;
return t;
}
t.prototype.start = function() {};
t.prototype.setWorkbenchItemInfo = function(e, t) {
var n = this;
this._index = e;
this._itemId = t;
this._itemType = 1;
this.isUsed = !1;
if (this._itemId) {
this.nodeIcon.active = !1;
this._itemInfo = s.default.getItemProduceInfoById(this._itemId);
s.default.setItemProduceIcon(this._itemId, this.nodeIcon.getComponent(cc.Sprite), function() {
n.nodeIcon.active = !0;
n.txtLevel.string = n._itemInfo.id.toString();
n.nodeLevel.active = !0;
});
} else {
this.nodeIcon.active = !1;
this.nodeLevel.active = !1;
}
this.isUsed ? this.nodeIcon.opacity = 150 : this.isDrag || (this.nodeIcon.opacity = 255);
};
t.prototype.dragStart = function() {
if (this.isUsed || !this._itemId) return !1;
this.isDrag = !0;
this.nodeIcon.opacity = 150;
return !0;
};
t.prototype.dragOver = function() {
if (this.isDrag) {
this.isDrag = !1;
this.nodeIcon.opacity = 255;
}
};
t.prototype.getInfo = function() {
return this._itemId;
};
t.prototype.showVirtual = function() {
this.nodeBg.active = !1;
};
t.prototype.playProduceAni = function() {};
i([ l(cc.Node) ], t.prototype, "nodeIcon", void 0);
i([ l(cc.Node) ], t.prototype, "nodeBg", void 0);
i([ l(cc.Node) ], t.prototype, "nodeLevel", void 0);
i([ l(cc.Label) ], t.prototype, "txtLevel", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"../../Utils/CocosHelper": "CocosHelper"
} ],
ItemRank: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "3e019pVVitIV5ppWULQY8hz", "ItemRank");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.level_bg = null;
t.user_level = null;
t.user_name = null;
t.user_num = null;
t.user_head = null;
t.icon = null;
t.level_bgs = Array();
t.icons = Array();
return t;
}
t.prototype.setData = function(e, t) {
this.icon.spriteFrame = 1 === e ? this.icons[0] : this.icons[1];
this.level_bg.active = !1;
this.user_num.node.color = new cc.Color(0, 0, 0);
if (t.level < 4) {
this.level_bg.getComponent(cc.Sprite).spriteFrame = this.level_bgs[t.level - 1];
this.level_bg.active = !0;
this.user_num.node.color = new cc.Color(229, 27, 27);
}
this.user_level.string = t.level.toString();
this.user_name.string = t.name;
this.user_num.string = t.num.toString();
};
i([ c(cc.Node) ], t.prototype, "level_bg", void 0);
i([ c(cc.Label) ], t.prototype, "user_level", void 0);
i([ c(cc.Label) ], t.prototype, "user_name", void 0);
i([ c(cc.Label) ], t.prototype, "user_num", void 0);
i([ c(cc.Sprite) ], t.prototype, "user_head", void 0);
i([ c(cc.Sprite) ], t.prototype, "icon", void 0);
i([ c([ cc.SpriteFrame ]) ], t.prototype, "level_bgs", void 0);
i([ c([ cc.SpriteFrame ]) ], t.prototype, "icons", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
ItemShop: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "50b6c2NQC1NJqTAcSDt/p8s", "ItemShop");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ButtonPlus"), a = e("../../Manager/FormMgr"), c = e("../../UIConfig"), l = cc._decorator, u = l.ccclass, p = l.property, f = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.item_name = null;
t.item_icon = null;
t.buy_type_sp = null;
t.buy_type_lab = null;
t.buy_btn = null;
return t;
}
t.prototype.start = function() {
this.buy_btn.addClick(function() {
a.default.open(c.default.UIBuyTip);
}, this);
};
t.prototype.setData = function(e) {
this.item_name.string = e.item_name;
this.buy_type_lab.string = e.item_buy_money.toString();
};
i([ p(cc.Label) ], t.prototype, "item_name", void 0);
i([ p(cc.Sprite) ], t.prototype, "item_icon", void 0);
i([ p(cc.Sprite) ], t.prototype, "buy_type_sp", void 0);
i([ p(cc.Label) ], t.prototype, "buy_type_lab", void 0);
i([ p(s.default) ], t.prototype, "buy_btn", void 0);
return i([ u ], t);
}(cc.Component);
n.default = f;
cc._RF.pop();
}, {
"../../Common/Components/ButtonPlus": "ButtonPlus",
"../../Manager/FormMgr": "FormMgr",
"../../UIConfig": "UIConfig"
} ],
ListItemUtil: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "8b1ee3jLpdD/47IU53hu4Bl", "ListItemUtil");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s, a = cc._decorator, c = a.ccclass, l = a.property, u = a.disallowMultiple, p = a.menu, f = a.executionOrder;
(function(e) {
e[e.NONE = 0] = "NONE";
e[e.TOGGLE = 1] = "TOGGLE";
e[e.SWITCH = 2] = "SWITCH";
})(s || (s = {}));
var d = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.icon = null;
t.title = null;
t.selectedMode = s.NONE;
t.selectedFlag = null;
t.selectedSpriteFrame = null;
t._unselectedSpriteFrame = null;
t.adaptiveSize = !1;
t._selected = !1;
t._eventReg = !1;
return t;
}
Object.defineProperty(t.prototype, "selected", {
get: function() {
return this._selected;
},
set: function(e) {
this._selected = e;
if (this.selectedFlag) switch (this.selectedMode) {
case s.TOGGLE:
this.selectedFlag.active = e;
break;

case s.SWITCH:
var t = this.selectedFlag.getComponent(cc.Sprite);
t && (t.spriteFrame = e ? this.selectedSpriteFrame : this._unselectedSpriteFrame);
}
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "btnCom", {
get: function() {
this._btnCom || (this._btnCom = this.node.getComponent(cc.Button));
return this._btnCom;
},
enumerable: !1,
configurable: !0
});
t.prototype.onLoad = function() {
if (this.selectedMode == s.SWITCH) {
var e = this.selectedFlag.getComponent(cc.Sprite);
this._unselectedSpriteFrame = e.spriteFrame;
}
};
t.prototype.onDestroy = function() {
this.node.off(cc.Node.EventType.SIZE_CHANGED, this._onSizeChange, this);
};
t.prototype._registerEvent = function() {
if (!this._eventReg) {
this.btnCom && this.list.selectedMode > 0 && this.btnCom.clickEvents.unshift(this.createEvt(this, "onClickThis"));
this.adaptiveSize && this.node.on(cc.Node.EventType.SIZE_CHANGED, this._onSizeChange, this);
this._eventReg = !0;
}
};
t.prototype._onSizeChange = function() {
this.list._onItemAdaptive(this.node);
};
t.prototype.createEvt = function(e, t, n) {
void 0 === n && (n = null);
if (e.isValid) {
e.comName = e.comName || e.name.match(/\<(.*?)\>/g).pop().replace(/\<|>/g, "");
var r = new cc.Component.EventHandler();
r.target = n || e.node;
r.component = e.comName;
r.handler = t;
return r;
}
};
t.prototype.showAni = function(e, t, n) {
var r, o = this;
switch (e) {
case 0:
r = cc.tween(o.node).to(.2, {
scale: .7
}).by(.3, {
y: 2 * o.node.height
});
break;

case 1:
r = cc.tween(o.node).to(.2, {
scale: .7
}).by(.3, {
x: 2 * o.node.width
});
break;

case 2:
r = cc.tween(o.node).to(.2, {
scale: .7
}).by(.3, {
y: -2 * o.node.height
});
break;

case 3:
r = cc.tween(o.node).to(.2, {
scale: .7
}).by(.3, {
x: -2 * o.node.width
});
break;

default:
r = cc.tween(o.node).to(.3, {
scale: .1
});
}
(t || n) && r.call(function() {
if (n) {
o.list._delSingleItem(o.node);
for (var e = o.list.displayData.length - 1; e >= 0; e--) if (o.list.displayData[e].id == o.listId) {
o.list.displayData.splice(e, 1);
break;
}
}
t();
});
r.start();
};
t.prototype.onClickThis = function() {
this.list.selectedId = this.listId;
};
i([ l({
type: cc.Sprite,
tooltip: !1
}) ], t.prototype, "icon", void 0);
i([ l({
type: cc.Node,
tooltip: !1
}) ], t.prototype, "title", void 0);
i([ l({
type: cc.Enum(s),
tooltip: !1
}) ], t.prototype, "selectedMode", void 0);
i([ l({
type: cc.Node,
tooltip: !1,
visible: function() {
return this.selectedMode > s.NONE;
}
}) ], t.prototype, "selectedFlag", void 0);
i([ l({
type: cc.SpriteFrame,
tooltip: !1,
visible: function() {
return this.selectedMode == s.SWITCH;
}
}) ], t.prototype, "selectedSpriteFrame", void 0);
i([ l({
tooltip: !1
}) ], t.prototype, "adaptiveSize", void 0);
return i([ c, u(), p("自定义组件/List Item"), f(-5001) ], t);
}(cc.Component);
n.default = d;
cc._RF.pop();
}, {} ],
ListUtil: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "8daa6rpbMhATpYjCCgvhfC6", "ListUtil");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s, a, c, l = cc._decorator, u = l.ccclass, p = l.property, f = l.disallowMultiple, d = l.menu, h = l.executionOrder, y = l.requireComponent, v = e("./ListItemUtil");
(function(e) {
e[e.NODE = 1] = "NODE";
e[e.PREFAB = 2] = "PREFAB";
})(s || (s = {}));
(function(e) {
e[e.NORMAL = 1] = "NORMAL";
e[e.ADHERING = 2] = "ADHERING";
e[e.PAGE = 3] = "PAGE";
})(a || (a = {}));
(function(e) {
e[e.NONE = 0] = "NONE";
e[e.SINGLE = 1] = "SINGLE";
e[e.MULT = 2] = "MULT";
})(c || (c = {}));
var g = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.templateType = s.NODE;
t.tmpNode = null;
t.tmpPrefab = null;
t._slideMode = a.NORMAL;
t.pageDistance = .3;
t.pageChangeEvent = new cc.Component.EventHandler();
t._virtual = !0;
t.cyclic = !1;
t.lackCenter = !1;
t.lackSlide = !1;
t._updateRate = 0;
t.frameByFrameRenderNum = 0;
t.renderEvent = new cc.Component.EventHandler();
t.selectedMode = c.NONE;
t.repeatEventSingle = !1;
t.selectedEvent = new cc.Component.EventHandler();
t._selectedId = -1;
t._forceUpdate = !1;
t._updateDone = !0;
t._numItems = 0;
t._inited = !1;
t._needUpdateWidget = !1;
t._aniDelRuning = !1;
t._doneAfterUpdate = !1;
t.adhering = !1;
t._adheringBarrier = !1;
t.curPageNum = 0;
return t;
}
Object.defineProperty(t.prototype, "slideMode", {
get: function() {
return this._slideMode;
},
set: function(e) {
this._slideMode = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "virtual", {
get: function() {
return this._virtual;
},
set: function(e) {
null != e && (this._virtual = e);
0 != this._numItems && this._onScrolling();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "updateRate", {
get: function() {
return this._updateRate;
},
set: function(e) {
e >= 0 && e <= 6 && (this._updateRate = e);
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "selectedId", {
get: function() {
return this._selectedId;
},
set: function(e) {
var t, n = this;
switch (n.selectedMode) {
case c.SINGLE:
if (!n.repeatEventSingle && e == n._selectedId) return;
t = n.getItemByListId(e);
var r = void 0;
n._selectedId >= 0 ? n._lastSelectedId = n._selectedId : n._lastSelectedId = null;
n._selectedId = e;
t && ((r = t.getComponent(v.default)).selected = !0);
if (n._lastSelectedId >= 0 && n._lastSelectedId != n._selectedId) {
var o = n.getItemByListId(n._lastSelectedId);
o && (o.getComponent(v.default).selected = !1);
}
n.selectedEvent && cc.Component.EventHandler.emitEvents([ n.selectedEvent ], t, e % this._actualNumItems, null == n._lastSelectedId ? null : n._lastSelectedId % this._actualNumItems);
break;

case c.MULT:
if (!(t = n.getItemByListId(e))) return;
r = t.getComponent(v.default);
n._selectedId >= 0 && (n._lastSelectedId = n._selectedId);
n._selectedId = e;
var i = !r.selected;
r.selected = i;
var s = n.multSelected.indexOf(e);
i && s < 0 ? n.multSelected.push(e) : !i && s >= 0 && n.multSelected.splice(s, 1);
n.selectedEvent && cc.Component.EventHandler.emitEvents([ n.selectedEvent ], t, e % this._actualNumItems, null == n._lastSelectedId ? null : n._lastSelectedId % this._actualNumItems, i);
}
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "numItems", {
get: function() {
return this._actualNumItems;
},
set: function(e) {
var t = this;
if (t.checkInited(!1)) if (null == e || e < 0) cc.error("numItems set the wrong::", e); else {
t._actualNumItems = t._numItems = e;
t._forceUpdate = !0;
if (t._virtual) {
t._resizeContent();
t.cyclic && (t._numItems = t._cyclicNum * t._numItems);
t._onScrolling();
t.frameByFrameRenderNum || t.slideMode != a.PAGE || (t.curPageNum = t.nearestListId);
} else {
if (t.cyclic) {
t._resizeContent();
t._numItems = t._cyclicNum * t._numItems;
}
var n = t.content.getComponent(cc.Layout);
n && (n.enabled = !0);
t._delRedundantItem();
t.firstListId = 0;
if (t.frameByFrameRenderNum > 0) {
for (var r = t.frameByFrameRenderNum > t._numItems ? t._numItems : t.frameByFrameRenderNum, o = 0; o < r; o++) t._createOrUpdateItem2(o);
if (t.frameByFrameRenderNum < t._numItems) {
t._updateCounter = t.frameByFrameRenderNum;
t._updateDone = !1;
}
} else {
for (o = 0; o < t._numItems; o++) t._createOrUpdateItem2(o);
t.displayItemNum = t._numItems;
}
}
}
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "scrollView", {
get: function() {
return this._scrollView;
},
enumerable: !1,
configurable: !0
});
t.prototype.onLoad = function() {
this._init();
};
t.prototype.onDestroy = function() {
var e = this;
cc.isValid(e._itemTmp) && e._itemTmp.destroy();
cc.isValid(e.tmpNode) && e.tmpNode.destroy();
e._pool && e._pool.clear();
};
t.prototype.onEnable = function() {
this._registerEvent();
this._init();
if (this._aniDelRuning) {
this._aniDelRuning = !1;
if (this._aniDelItem) {
if (this._aniDelBeforePos) {
this._aniDelItem.position = this._aniDelBeforePos;
delete this._aniDelBeforePos;
}
if (this._aniDelBeforeScale) {
this._aniDelItem.scale = this._aniDelBeforeScale;
delete this._aniDelBeforeScale;
}
delete this._aniDelItem;
}
if (this._aniDelCB) {
this._aniDelCB();
delete this._aniDelCB;
}
}
};
t.prototype.onDisable = function() {
this._unregisterEvent();
};
t.prototype._registerEvent = function() {
var e = this;
e.node.on(cc.Node.EventType.TOUCH_START, e._onTouchStart, e, !0);
e.node.on("touch-up", e._onTouchUp, e);
e.node.on(cc.Node.EventType.TOUCH_CANCEL, e._onTouchCancelled, e, !0);
e.node.on("scroll-began", e._onScrollBegan, e, !0);
e.node.on("scroll-ended", e._onScrollEnded, e, !0);
e.node.on("scrolling", e._onScrolling, e, !0);
e.node.on(cc.Node.EventType.SIZE_CHANGED, e._onSizeChanged, e);
};
t.prototype._unregisterEvent = function() {
var e = this;
e.node.off(cc.Node.EventType.TOUCH_START, e._onTouchStart, e, !0);
e.node.off("touch-up", e._onTouchUp, e);
e.node.off(cc.Node.EventType.TOUCH_CANCEL, e._onTouchCancelled, e, !0);
e.node.off("scroll-began", e._onScrollBegan, e, !0);
e.node.off("scroll-ended", e._onScrollEnded, e, !0);
e.node.off("scrolling", e._onScrolling, e, !0);
e.node.off(cc.Node.EventType.SIZE_CHANGED, e._onSizeChanged, e);
};
t.prototype._init = function() {
var e = this;
if (!e._inited) {
e._scrollView = e.node.getComponent(cc.ScrollView);
e.content = e._scrollView.content;
if (e.content) {
e._layout = e.content.getComponent(cc.Layout);
e._align = e._layout.type;
e._resizeMode = e._layout.resizeMode;
e._startAxis = e._layout.startAxis;
e._topGap = e._layout.paddingTop;
e._rightGap = e._layout.paddingRight;
e._bottomGap = e._layout.paddingBottom;
e._leftGap = e._layout.paddingLeft;
e._columnGap = e._layout.spacingX;
e._lineGap = e._layout.spacingY;
e._colLineNum;
e._verticalDir = e._layout.verticalDirection;
e._horizontalDir = e._layout.horizontalDirection;
e.setTemplateItem(cc.instantiate(e.templateType == s.PREFAB ? e.tmpPrefab : e.tmpNode));
if (e._slideMode == a.ADHERING || e._slideMode == a.PAGE) {
e._scrollView.inertia = !1;
e._scrollView._onMouseWheel = function() {};
}
e.virtual || (e.lackCenter = !1);
e._lastDisplayData = [];
e.displayData = [];
e._pool = new cc.NodePool();
e._forceUpdate = !1;
e._updateCounter = 0;
e._updateDone = !0;
e.curPageNum = 0;
if (e.cyclic) {
e._scrollView._processAutoScrolling = this._processAutoScrolling.bind(e);
e._scrollView._startBounceBackIfNeeded = function() {
return !1;
};
}
switch (e._align) {
case cc.Layout.Type.HORIZONTAL:
switch (e._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
e._alignCalcType = 1;
break;

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
e._alignCalcType = 2;
}
break;

case cc.Layout.Type.VERTICAL:
switch (e._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
e._alignCalcType = 3;
break;

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
e._alignCalcType = 4;
}
break;

case cc.Layout.Type.GRID:
switch (e._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
switch (e._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
e._alignCalcType = 3;
break;

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
e._alignCalcType = 4;
}
break;

case cc.Layout.AxisDirection.VERTICAL:
switch (e._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
e._alignCalcType = 1;
break;

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
e._alignCalcType = 2;
}
}
}
e.content.removeAllChildren();
e._inited = !0;
} else cc.error(e.node.name + "'s cc.ScrollView unset content!");
}
};
t.prototype._processAutoScrolling = function(e) {
this._scrollView._autoScrollAccumulatedTime += 1 * e;
var t = Math.min(1, this._scrollView._autoScrollAccumulatedTime / this._scrollView._autoScrollTotalTime);
if (this._scrollView._autoScrollAttenuate) {
var n = t - 1;
t = n * n * n * n * n + 1;
}
var r = this._scrollView._autoScrollStartPosition.add(this._scrollView._autoScrollTargetDelta.mul(t)), o = this._scrollView.getScrollEndedEventTiming(), i = Math.abs(t - 1) <= o;
if (Math.abs(t - 1) <= this._scrollView.getScrollEndedEventTiming() && !this._scrollView._isScrollEndedWithThresholdEventFired) {
this._scrollView._dispatchEvent("scroll-ended-with-threshold");
this._scrollView._isScrollEndedWithThresholdEventFired = !0;
}
i && (this._scrollView._autoScrolling = !1);
var s = r.sub(this._scrollView.getContentPosition());
this._scrollView._moveContent(this._scrollView._clampDelta(s), i);
this._scrollView._dispatchEvent("scrolling");
if (!this._scrollView._autoScrolling) {
this._scrollView._isBouncing = !1;
this._scrollView._scrolling = !1;
this._scrollView._dispatchEvent("scroll-ended");
}
};
t.prototype.setTemplateItem = function(e) {
if (e) {
var t = this;
t._itemTmp = e;
t._resizeMode == cc.Layout.ResizeMode.CHILDREN ? t._itemSize = t._layout.cellSize : t._itemSize = cc.size(e.width, e.height);
var n = e.getComponent(v.default), r = !1;
n || (r = !0);
r && (t.selectedMode = c.NONE);
(n = e.getComponent(cc.Widget)) && n.enabled && (t._needUpdateWidget = !0);
t.selectedMode == c.MULT && (t.multSelected = []);
switch (t._align) {
case cc.Layout.Type.HORIZONTAL:
t._colLineNum = 1;
t._sizeType = !1;
break;

case cc.Layout.Type.VERTICAL:
t._colLineNum = 1;
t._sizeType = !0;
break;

case cc.Layout.Type.GRID:
switch (t._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
var o = t.content.width - t._leftGap - t._rightGap;
t._colLineNum = Math.floor((o + t._columnGap) / (t._itemSize.width + t._columnGap));
t._sizeType = !0;
break;

case cc.Layout.AxisDirection.VERTICAL:
var i = t.content.height - t._topGap - t._bottomGap;
t._colLineNum = Math.floor((i + t._lineGap) / (t._itemSize.height + t._lineGap));
t._sizeType = !1;
}
}
}
};
t.prototype.checkInited = function(e) {
void 0 === e && (e = !0);
if (!this._inited) {
e && cc.error("List initialization not completed!");
return !1;
}
return !0;
};
t.prototype._resizeContent = function() {
var e, t = this;
switch (t._align) {
case cc.Layout.Type.HORIZONTAL:
if (t._customSize) {
var n = t._getFixedSize(null);
e = t._leftGap + n.val + t._itemSize.width * (t._numItems - n.count) + t._columnGap * (t._numItems - 1) + t._rightGap;
} else e = t._leftGap + t._itemSize.width * t._numItems + t._columnGap * (t._numItems - 1) + t._rightGap;
break;

case cc.Layout.Type.VERTICAL:
if (t._customSize) {
n = t._getFixedSize(null);
e = t._topGap + n.val + t._itemSize.height * (t._numItems - n.count) + t._lineGap * (t._numItems - 1) + t._bottomGap;
} else e = t._topGap + t._itemSize.height * t._numItems + t._lineGap * (t._numItems - 1) + t._bottomGap;
break;

case cc.Layout.Type.GRID:
t.lackCenter && (t.lackCenter = !1);
switch (t._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
var r = Math.ceil(t._numItems / t._colLineNum);
e = t._topGap + t._itemSize.height * r + t._lineGap * (r - 1) + t._bottomGap;
break;

case cc.Layout.AxisDirection.VERTICAL:
var o = Math.ceil(t._numItems / t._colLineNum);
e = t._leftGap + t._itemSize.width * o + t._columnGap * (o - 1) + t._rightGap;
}
}
var i = t.content.getComponent(cc.Layout);
i && (i.enabled = !1);
t._allItemSize = e;
t._allItemSizeNoEdge = t._allItemSize - (t._sizeType ? t._topGap + t._bottomGap : t._leftGap + t._rightGap);
if (t.cyclic) {
var s = t._sizeType ? t.node.height : t.node.width;
t._cyclicPos1 = 0;
s -= t._cyclicPos1;
t._cyclicNum = Math.ceil(s / t._allItemSizeNoEdge) + 1;
var a = t._sizeType ? t._lineGap : t._columnGap;
t._cyclicPos2 = t._cyclicPos1 + t._allItemSizeNoEdge + a;
t._cyclicAllItemSize = t._allItemSize + t._allItemSizeNoEdge * (t._cyclicNum - 1) + a * (t._cyclicNum - 1);
t._cycilcAllItemSizeNoEdge = t._allItemSizeNoEdge * t._cyclicNum;
t._cycilcAllItemSizeNoEdge += a * (t._cyclicNum - 1);
}
t._lack = !t.cyclic && t._allItemSize < (t._sizeType ? t.node.height : t.node.width);
var c = t._lack && t.lackCenter || !t.lackSlide ? .1 : 0, l = t._lack ? (t._sizeType ? t.node.height : t.node.width) - c : t.cyclic ? t._cyclicAllItemSize : t._allItemSize;
l < 0 && (l = 0);
t._sizeType ? t.content.height = l : t.content.width = l;
};
t.prototype._onScrolling = function(e) {
void 0 === e && (e = null);
null == this.frameCount && (this.frameCount = this._updateRate);
if (!this._forceUpdate && e && "scroll-ended" != e.type && this.frameCount > 0) this.frameCount--; else {
this.frameCount = this._updateRate;
if (!this._aniDelRuning) {
if (this.cyclic) {
var t = this.content.getPosition();
t = this._sizeType ? t.y : t.x;
var n = this._allItemSizeNoEdge + (this._sizeType ? this._lineGap : this._columnGap), r = this._sizeType ? cc.v2(0, n) : cc.v2(n, 0);
switch (this._alignCalcType) {
case 1:
if (t > -this._cyclicPos1) {
this.content.x = -this._cyclicPos2;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.sub(r));
} else if (t < -this._cyclicPos2) {
this.content.x = -this._cyclicPos1;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.add(r));
}
break;

case 2:
if (t < this._cyclicPos1) {
this.content.x = this._cyclicPos2;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.add(r));
} else if (t > this._cyclicPos2) {
this.content.x = this._cyclicPos1;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.sub(r));
}
break;

case 3:
if (t < this._cyclicPos1) {
this.content.y = this._cyclicPos2;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.add(r));
} else if (t > this._cyclicPos2) {
this.content.y = this._cyclicPos1;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.sub(r));
}
break;

case 4:
if (t > -this._cyclicPos1) {
this.content.y = -this._cyclicPos2;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.sub(r));
} else if (t < -this._cyclicPos2) {
this.content.y = -this._cyclicPos1;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.add(r));
}
}
}
this._calcViewPos();
var o, i, s, a;
if (this._sizeType) {
o = this.viewTop;
s = this.viewBottom;
} else {
i = this.viewRight;
a = this.viewLeft;
}
if (this._virtual) {
this.displayData = [];
var c = void 0, l = 0, u = this._numItems - 1;
if (this._customSize) for (var p = !1; l <= u && !p; l++) {
c = this._calcItemPos(l);
switch (this._align) {
case cc.Layout.Type.HORIZONTAL:
c.right >= a && c.left <= i ? this.displayData.push(c) : 0 != l && this.displayData.length > 0 && (p = !0);
break;

case cc.Layout.Type.VERTICAL:
c.bottom <= o && c.top >= s ? this.displayData.push(c) : 0 != l && this.displayData.length > 0 && (p = !0);
break;

case cc.Layout.Type.GRID:
switch (this._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
c.bottom <= o && c.top >= s ? this.displayData.push(c) : 0 != l && this.displayData.length > 0 && (p = !0);
break;

case cc.Layout.AxisDirection.VERTICAL:
c.right >= a && c.left <= i ? this.displayData.push(c) : 0 != l && this.displayData.length > 0 && (p = !0);
}
}
} else {
var f = this._itemSize.width + this._columnGap, d = this._itemSize.height + this._lineGap;
switch (this._alignCalcType) {
case 1:
l = (a - this._leftGap) / f;
u = (i - this._leftGap) / f;
break;

case 2:
l = (-i - this._rightGap) / f;
u = (-a - this._rightGap) / f;
break;

case 3:
l = (-o - this._topGap) / d;
u = (-s - this._topGap) / d;
break;

case 4:
l = (s - this._bottomGap) / d;
u = (o - this._bottomGap) / d;
}
l = Math.floor(l) * this._colLineNum;
u = Math.ceil(u) * this._colLineNum;
l < 0 && (l = 0);
--u >= this._numItems && (u = this._numItems - 1);
for (;l <= u; l++) this.displayData.push(this._calcItemPos(l));
}
this._delRedundantItem();
if (this.displayData.length <= 0 || !this._numItems) {
this._lastDisplayData = [];
return;
}
this.firstListId = this.displayData[0].id;
this.displayItemNum = this.displayData.length;
var h = this._lastDisplayData.length, y = this.displayItemNum != h;
if (y) {
this.frameByFrameRenderNum > 0 && this._lastDisplayData.sort(function(e, t) {
return e - t;
});
y = this.firstListId != this._lastDisplayData[0] || this.displayData[this.displayItemNum - 1].id != this._lastDisplayData[h - 1];
}
if (this._forceUpdate || y) if (this.frameByFrameRenderNum > 0) if (this._numItems > 0) {
this._updateDone ? this._updateCounter = 0 : this._doneAfterUpdate = !0;
this._updateDone = !1;
} else {
this._updateCounter = 0;
this._updateDone = !0;
} else {
this._lastDisplayData = [];
for (var v = 0; v < this.displayItemNum; v++) this._createOrUpdateItem(this.displayData[v]);
this._forceUpdate = !1;
}
this._calcNearestItem();
}
}
}
};
t.prototype._calcViewPos = function() {
var e = this.content.getPosition();
switch (this._alignCalcType) {
case 1:
this.elasticLeft = e.x > 0 ? e.x : 0;
this.viewLeft = (e.x < 0 ? -e.x : 0) - this.elasticLeft;
this.viewRight = this.viewLeft + this.node.width;
this.elasticRight = this.viewRight > this.content.width ? Math.abs(this.viewRight - this.content.width) : 0;
this.viewRight += this.elasticRight;
break;

case 2:
this.elasticRight = e.x < 0 ? -e.x : 0;
this.viewRight = (e.x > 0 ? -e.x : 0) + this.elasticRight;
this.viewLeft = this.viewRight - this.node.width;
this.elasticLeft = this.viewLeft < -this.content.width ? Math.abs(this.viewLeft + this.content.width) : 0;
this.viewLeft -= this.elasticLeft;
break;

case 3:
this.elasticTop = e.y < 0 ? Math.abs(e.y) : 0;
this.viewTop = (e.y > 0 ? -e.y : 0) + this.elasticTop;
this.viewBottom = this.viewTop - this.node.height;
this.elasticBottom = this.viewBottom < -this.content.height ? Math.abs(this.viewBottom + this.content.height) : 0;
this.viewBottom += this.elasticBottom;
break;

case 4:
this.elasticBottom = e.y > 0 ? Math.abs(e.y) : 0;
this.viewBottom = (e.y < 0 ? -e.y : 0) - this.elasticBottom;
this.viewTop = this.viewBottom + this.node.height;
this.elasticTop = this.viewTop > this.content.height ? Math.abs(this.viewTop - this.content.height) : 0;
this.viewTop -= this.elasticTop;
}
};
t.prototype._calcItemPos = function(e) {
var t, n, r, o, i, s, a, c;
switch (this._align) {
case cc.Layout.Type.HORIZONTAL:
switch (this._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
if (this._customSize) {
var l = this._getFixedSize(e);
i = this._leftGap + (this._itemSize.width + this._columnGap) * (e - l.count) + (l.val + this._columnGap * l.count);
t = (u = this._customSize[e]) > 0 ? u : this._itemSize.width;
} else {
i = this._leftGap + (this._itemSize.width + this._columnGap) * e;
t = this._itemSize.width;
}
if (this.lackCenter) {
i -= this._leftGap;
i += this.content.width / 2 - this._allItemSizeNoEdge / 2;
}
return {
id: e,
left: i,
right: s = i + t,
x: i + this._itemTmp.anchorX * t,
y: this._itemTmp.y
};

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
if (this._customSize) {
l = this._getFixedSize(e);
s = -this._rightGap - (this._itemSize.width + this._columnGap) * (e - l.count) - (l.val + this._columnGap * l.count);
t = (u = this._customSize[e]) > 0 ? u : this._itemSize.width;
} else {
s = -this._rightGap - (this._itemSize.width + this._columnGap) * e;
t = this._itemSize.width;
}
if (this.lackCenter) {
s += this._rightGap;
s -= this.content.width / 2 - this._allItemSizeNoEdge / 2;
}
return {
id: e,
right: s,
left: i = s - t,
x: i + this._itemTmp.anchorX * t,
y: this._itemTmp.y
};
}
break;

case cc.Layout.Type.VERTICAL:
switch (this._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
if (this._customSize) {
l = this._getFixedSize(e);
r = -this._topGap - (this._itemSize.height + this._lineGap) * (e - l.count) - (l.val + this._lineGap * l.count);
n = (u = this._customSize[e]) > 0 ? u : this._itemSize.height;
} else {
r = -this._topGap - (this._itemSize.height + this._lineGap) * e;
n = this._itemSize.height;
}
if (this.lackCenter) {
r += this._topGap;
r -= this.content.height / 2 - this._allItemSizeNoEdge / 2;
}
return {
id: e,
top: r,
bottom: o = r - n,
x: this._itemTmp.x,
y: o + this._itemTmp.anchorY * n
};

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
if (this._customSize) {
var u;
l = this._getFixedSize(e);
o = this._bottomGap + (this._itemSize.height + this._lineGap) * (e - l.count) + (l.val + this._lineGap * l.count);
n = (u = this._customSize[e]) > 0 ? u : this._itemSize.height;
} else {
o = this._bottomGap + (this._itemSize.height + this._lineGap) * e;
n = this._itemSize.height;
}
if (this.lackCenter) {
o -= this._bottomGap;
o += this.content.height / 2 - this._allItemSizeNoEdge / 2;
}
return {
id: e,
top: r = o + n,
bottom: o,
x: this._itemTmp.x,
y: o + this._itemTmp.anchorY * n
};
}

case cc.Layout.Type.GRID:
var p = Math.floor(e / this._colLineNum);
switch (this._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
switch (this._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
c = (o = (r = -this._topGap - (this._itemSize.height + this._lineGap) * p) - this._itemSize.height) + this._itemTmp.anchorY * this._itemSize.height;
break;

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
r = (o = this._bottomGap + (this._itemSize.height + this._lineGap) * p) + this._itemSize.height;
c = o + this._itemTmp.anchorY * this._itemSize.height;
}
a = this._leftGap + e % this._colLineNum * (this._itemSize.width + this._columnGap);
switch (this._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
a += this._itemTmp.anchorX * this._itemSize.width;
a -= this.content.anchorX * this.content.width;
break;

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
a += (1 - this._itemTmp.anchorX) * this._itemSize.width;
a -= (1 - this.content.anchorX) * this.content.width;
a *= -1;
}
return {
id: e,
top: r,
bottom: o,
x: a,
y: c
};

case cc.Layout.AxisDirection.VERTICAL:
switch (this._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
s = (i = this._leftGap + (this._itemSize.width + this._columnGap) * p) + this._itemSize.width;
a = i + this._itemTmp.anchorX * this._itemSize.width;
a -= this.content.anchorX * this.content.width;
break;

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
a = (i = (s = -this._rightGap - (this._itemSize.width + this._columnGap) * p) - this._itemSize.width) + this._itemTmp.anchorX * this._itemSize.width;
a += (1 - this.content.anchorX) * this.content.width;
}
c = -this._topGap - e % this._colLineNum * (this._itemSize.height + this._lineGap);
switch (this._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
c -= (1 - this._itemTmp.anchorY) * this._itemSize.height;
c += (1 - this.content.anchorY) * this.content.height;
break;

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
c -= this._itemTmp.anchorY * this._itemSize.height;
c += this.content.anchorY * this.content.height;
c *= -1;
}
return {
id: e,
left: i,
right: s,
x: a,
y: c
};
}
}
};
t.prototype._calcExistItemPos = function(e) {
var t = this.getItemByListId(e);
if (!t) return null;
var n = {
id: e,
x: t.x,
y: t.y
};
if (this._sizeType) {
n.top = t.y + t.height * (1 - t.anchorY);
n.bottom = t.y - t.height * t.anchorY;
} else {
n.left = t.x - t.width * t.anchorX;
n.right = t.x + t.width * (1 - t.anchorX);
}
return n;
};
t.prototype.getItemPos = function(e) {
return this._virtual ? this._calcItemPos(e) : this.frameByFrameRenderNum ? this._calcItemPos(e) : this._calcExistItemPos(e);
};
t.prototype._getFixedSize = function(e) {
if (!this._customSize) return null;
null == e && (e = this._numItems);
var t = 0, n = 0;
for (var r in this._customSize) if (parseInt(r) < e) {
t += this._customSize[r];
n++;
}
return {
val: t,
count: n
};
};
t.prototype._onScrollBegan = function() {
this._beganPos = this._sizeType ? this.viewTop : this.viewLeft;
};
t.prototype._onScrollEnded = function() {
var e = this;
e.curScrollIsTouch = !1;
if (null != e.scrollToListId) {
var t = e.getItemByListId(e.scrollToListId);
e.scrollToListId = null;
t && cc.tween(t).to(.1, {
scale: 1.06
}).to(.1, {
scale: 1
}).start();
}
e._onScrolling();
e._slideMode != a.ADHERING || e.adhering ? e._slideMode == a.PAGE && (null != e._beganPos && e.curScrollIsTouch ? this._pageAdhere() : e.adhere()) : e.adhere();
};
t.prototype._onTouchStart = function(e, t) {
if (!this._scrollView.hasNestedViewGroup(e, t)) {
this.curScrollIsTouch = !0;
if (e.eventPhase !== cc.Event.AT_TARGET || e.target !== this.node) {
for (var n = e.target; null == n._listId && n.parent; ) n = n.parent;
this._scrollItem = null != n._listId ? n : e.target;
}
}
};
t.prototype._onTouchUp = function() {
var e = this;
e._scrollPos = null;
if (e._slideMode == a.ADHERING) {
this.adhering && (this._adheringBarrier = !0);
e.adhere();
} else e._slideMode == a.PAGE && (null != e._beganPos ? this._pageAdhere() : e.adhere());
this._scrollItem = null;
};
t.prototype._onTouchCancelled = function(e, t) {
var n = this;
if (!n._scrollView.hasNestedViewGroup(e, t) && !e.simulate) {
n._scrollPos = null;
if (n._slideMode == a.ADHERING) {
n.adhering && (n._adheringBarrier = !0);
n.adhere();
} else n._slideMode == a.PAGE && (null != n._beganPos ? n._pageAdhere() : n.adhere());
this._scrollItem = null;
}
};
t.prototype._onSizeChanged = function() {
this.checkInited(!1) && this._onScrolling();
};
t.prototype._onItemAdaptive = function(e) {
if (!this._sizeType && e.width != this._itemSize.width || this._sizeType && e.height != this._itemSize.height) {
this._customSize || (this._customSize = {});
var t = this._sizeType ? e.height : e.width;
if (this._customSize[e._listId] != t) {
this._customSize[e._listId] = t;
this._resizeContent();
this.updateAll();
if (null != this._scrollToListId) {
this._scrollPos = null;
this.unschedule(this._scrollToSo);
this.scrollTo(this._scrollToListId, Math.max(0, this._scrollToEndTime - new Date().getTime() / 1e3));
}
}
}
};
t.prototype._pageAdhere = function() {
var e = this;
if (e.cyclic || !(e.elasticTop > 0 || e.elasticRight > 0 || e.elasticBottom > 0 || e.elasticLeft > 0)) {
var t = e._sizeType ? e.viewTop : e.viewLeft, n = (e._sizeType ? e.node.height : e.node.width) * e.pageDistance;
if (Math.abs(e._beganPos - t) > n) switch (e._alignCalcType) {
case 1:
case 4:
e._beganPos > t ? e.prePage(.5) : e.nextPage(.5);
break;

case 2:
case 3:
e._beganPos < t ? e.prePage(.5) : e.nextPage(.5);
} else e.elasticTop <= 0 && e.elasticRight <= 0 && e.elasticBottom <= 0 && e.elasticLeft <= 0 && e.adhere();
e._beganPos = null;
}
};
t.prototype.adhere = function() {
var e = this;
if (e.checkInited() && !(e.elasticTop > 0 || e.elasticRight > 0 || e.elasticBottom > 0 || e.elasticLeft > 0)) {
e.adhering = !0;
e._calcNearestItem();
var t = (e._sizeType ? e._topGap : e._leftGap) / (e._sizeType ? e.node.height : e.node.width);
e.scrollTo(e.nearestListId, .7, t);
}
};
t.prototype.update = function() {
if (!(this.frameByFrameRenderNum <= 0 || this._updateDone)) if (this._virtual) {
for (var e = this._updateCounter + this.frameByFrameRenderNum > this.displayItemNum ? this.displayItemNum : this._updateCounter + this.frameByFrameRenderNum, t = this._updateCounter; t < e; t++) {
var n = this.displayData[t];
n && this._createOrUpdateItem(n);
}
if (this._updateCounter >= this.displayItemNum - 1) if (this._doneAfterUpdate) {
this._updateCounter = 0;
this._updateDone = !1;
this._doneAfterUpdate = !1;
} else {
this._updateDone = !0;
this._delRedundantItem();
this._forceUpdate = !1;
this._calcNearestItem();
this.slideMode == a.PAGE && (this.curPageNum = this.nearestListId);
} else this._updateCounter += this.frameByFrameRenderNum;
} else if (this._updateCounter < this._numItems) {
for (e = this._updateCounter + this.frameByFrameRenderNum > this._numItems ? this._numItems : this._updateCounter + this.frameByFrameRenderNum, 
t = this._updateCounter; t < e; t++) this._createOrUpdateItem2(t);
this._updateCounter += this.frameByFrameRenderNum;
} else {
this._updateDone = !0;
this._calcNearestItem();
this.slideMode == a.PAGE && (this.curPageNum = this.nearestListId);
}
};
t.prototype._createOrUpdateItem = function(e) {
var t = this.getItemByListId(e.id);
if (t) {
if (this._forceUpdate && this.renderEvent) {
t.setPosition(cc.v2(e.x, e.y));
this._resetItemSize(t);
this.renderEvent && cc.Component.EventHandler.emitEvents([ this.renderEvent ], t, e.id % this._actualNumItems);
}
} else {
var n = this._pool.size() > 0;
t = n ? this._pool.get() : cc.instantiate(this._itemTmp);
if (!n || !cc.isValid(t)) {
t = cc.instantiate(this._itemTmp);
n = !1;
}
if (t._listId != e.id) {
t._listId = e.id;
t.setContentSize(this._itemSize);
}
t.setPosition(cc.v2(e.x, e.y));
this._resetItemSize(t);
this.content.addChild(t);
if (n && this._needUpdateWidget) {
var r = t.getComponent(cc.Widget);
r && r.updateAlignment();
}
t.setSiblingIndex(this.content.childrenCount - 1);
var o = t.getComponent(v.default);
t.listItem = o;
if (o) {
o.listId = e.id;
o.list = this;
o._registerEvent();
}
this.renderEvent && cc.Component.EventHandler.emitEvents([ this.renderEvent ], t, e.id % this._actualNumItems);
}
this._resetItemSize(t);
this._updateListItem(t.listItem);
this._lastDisplayData.indexOf(e.id) < 0 && this._lastDisplayData.push(e.id);
};
t.prototype._createOrUpdateItem2 = function(e) {
var t, n = this.content.children[e];
if (n) {
if (this._forceUpdate && this.renderEvent) {
n._listId = e;
t && (t.listId = e);
this.renderEvent && cc.Component.EventHandler.emitEvents([ this.renderEvent ], n, e % this._actualNumItems);
}
} else {
(n = cc.instantiate(this._itemTmp))._listId = e;
this.content.addChild(n);
t = n.getComponent(v.default);
n.listItem = t;
if (t) {
t.listId = e;
t.list = this;
t._registerEvent();
}
this.renderEvent && cc.Component.EventHandler.emitEvents([ this.renderEvent ], n, e % this._actualNumItems);
}
this._updateListItem(t);
this._lastDisplayData.indexOf(e) < 0 && this._lastDisplayData.push(e);
};
t.prototype._updateListItem = function(e) {
if (e && this.selectedMode > c.NONE) {
var t = e.node;
switch (this.selectedMode) {
case c.SINGLE:
e.selected = this.selectedId == t._listId;
break;

case c.MULT:
e.selected = this.multSelected.indexOf(t._listId) >= 0;
}
}
};
t.prototype._resetItemSize = function() {};
t.prototype._updateItemPos = function(e) {
var t = isNaN(e) ? e : this.getItemByListId(e), n = this.getItemPos(t._listId);
t.setPosition(n.x, n.y);
};
t.prototype.setMultSelected = function(e, t) {
var n = this;
if (n.checkInited()) {
Array.isArray(e) || (e = [ e ]);
if (null == t) n.multSelected = e; else {
var r = void 0, o = void 0;
if (t) for (var i = e.length - 1; i >= 0; i--) {
r = e[i];
(o = n.multSelected.indexOf(r)) < 0 && n.multSelected.push(r);
} else for (i = e.length - 1; i >= 0; i--) {
r = e[i];
(o = n.multSelected.indexOf(r)) >= 0 && n.multSelected.splice(o, 1);
}
}
n._forceUpdate = !0;
n._onScrolling();
}
};
t.prototype.getMultSelected = function() {
return this.multSelected;
};
t.prototype.hasMultSelected = function(e) {
return this.multSelected && this.multSelected.indexOf(e) >= 0;
};
t.prototype.updateItem = function(e) {
if (this.checkInited()) {
Array.isArray(e) || (e = [ e ]);
for (var t = 0, n = e.length; t < n; t++) {
var r = e[t], o = this.getItemByListId(r);
o && cc.Component.EventHandler.emitEvents([ this.renderEvent ], o, r % this._actualNumItems);
}
}
};
t.prototype.updateAll = function() {
this.checkInited() && (this.numItems = this.numItems);
};
t.prototype.getItemByListId = function(e) {
if (this.content) for (var t = this.content.childrenCount - 1; t >= 0; t--) {
var n = this.content.children[t];
if (n._listId == e) return n;
}
};
t.prototype._getOutsideItem = function() {
for (var e, t = [], n = this.content.childrenCount - 1; n >= 0; n--) {
e = this.content.children[n];
this.displayData.find(function(t) {
return t.id == e._listId;
}) || t.push(e);
}
return t;
};
t.prototype._delRedundantItem = function() {
if (this._virtual) for (var e = this._getOutsideItem(), t = e.length - 1; t >= 0; t--) {
var n = e[t];
if (!this._scrollItem || n._listId != this._scrollItem._listId) {
n.isCached = !0;
this._pool.put(n);
for (var r = this._lastDisplayData.length - 1; r >= 0; r--) if (this._lastDisplayData[r] == n._listId) {
this._lastDisplayData.splice(r, 1);
break;
}
}
} else for (;this.content.childrenCount > this._numItems; ) this._delSingleItem(this.content.children[this.content.childrenCount - 1]);
};
t.prototype._delSingleItem = function(e) {
e.removeFromParent();
e.destroy && e.destroy();
e = null;
};
t.prototype.aniDelItem = function(e, t, n) {
var r = this;
if (!r.checkInited() || r.cyclic || !r._virtual) return cc.error("This function is not allowed to be called!");
if (!t) return cc.error("CallFunc are not allowed to be NULL, You need to delete the corresponding index in the data array in the CallFunc!");
if (r._aniDelRuning) return cc.warn("Please wait for the current deletion to finish!");
var o, i = r.getItemByListId(e);
if (i) {
o = i.getComponent(v.default);
r._aniDelRuning = !0;
r._aniDelCB = t;
r._aniDelItem = i;
r._aniDelBeforePos = i.position;
r._aniDelBeforeScale = i.scale;
var s = r.displayData[r.displayData.length - 1].id, a = o.selected;
o.showAni(n, function() {
var n, o, l;
s < r._numItems - 2 && (n = s + 1);
if (null != n) {
var u = r._calcItemPos(n);
r.displayData.push(u);
r._virtual ? r._createOrUpdateItem(u) : r._createOrUpdateItem2(n);
} else r._numItems--;
if (r.selectedMode == c.SINGLE) a ? r._selectedId = -1 : r._selectedId - 1 >= 0 && r._selectedId--; else if (r.selectedMode == c.MULT && r.multSelected.length) {
var p = r.multSelected.indexOf(e);
p >= 0 && r.multSelected.splice(p, 1);
for (var f = r.multSelected.length - 1; f >= 0; f--) (y = r.multSelected[f]) >= e && r.multSelected[f]--;
}
if (r._customSize) {
r._customSize[e] && delete r._customSize[e];
var d = {}, h = void 0;
for (var y in r._customSize) {
h = r._customSize[y];
var v = parseInt(y);
d[v - (v >= e ? 1 : 0)] = h;
}
r._customSize = d;
}
for (f = null != n ? n : s; f >= e + 1; f--) if (i = r.getItemByListId(f)) {
var g = r._calcItemPos(f - 1);
o = cc.tween(i).to(.2333, {
position: cc.v2(g.x, g.y)
});
if (f <= e + 1) {
l = !0;
o.call(function() {
r._aniDelRuning = !1;
t(e);
delete r._aniDelCB;
});
}
o.start();
}
if (!l) {
r._aniDelRuning = !1;
t(e);
r._aniDelCB = null;
}
}, !0);
} else t(e);
};
t.prototype.scrollTo = function(e, t, n, r) {
void 0 === t && (t = .5);
void 0 === n && (n = null);
void 0 === r && (r = !1);
var o = this;
if (o.checkInited(!1)) {
null == t ? t = .5 : t < 0 && (t = 0);
e < 0 ? e = 0 : e >= o._numItems && (e = o._numItems - 1);
!o._virtual && o._layout && o._layout.enabled && o._layout.updateLayout();
var i, s, a = o.getItemPos(e);
if (!a) return !1;
switch (o._alignCalcType) {
case 1:
i = a.left;
i -= null != n ? o.node.width * n : o._leftGap;
a = cc.v2(i, 0);
break;

case 2:
i = a.right - o.node.width;
i += null != n ? o.node.width * n : o._rightGap;
a = cc.v2(i + o.content.width, 0);
break;

case 3:
s = a.top;
s += null != n ? o.node.height * n : o._topGap;
a = cc.v2(0, -s);
break;

case 4:
s = a.bottom + o.node.height;
s -= null != n ? o.node.height * n : o._bottomGap;
a = cc.v2(0, -s + o.content.height);
}
var c = o.content.getPosition();
c = Math.abs(o._sizeType ? c.y : c.x);
var l = o._sizeType ? a.y : a.x;
if (Math.abs((null != o._scrollPos ? o._scrollPos : c) - l) > .5) {
o._scrollView.scrollToOffset(a, t);
o._scrollToListId = e;
o._scrollToEndTime = new Date().getTime() / 1e3 + t;
o._scrollToSo = o.scheduleOnce(function() {
o._adheringBarrier || (o.adhering = o._adheringBarrier = !1);
o._scrollPos = o._scrollToListId = o._scrollToEndTime = o._scrollToSo = null;
if (r) {
var t = o.getItemByListId(e);
t && cc.tween(t).to(.1, {
scale: 1.05
}).to(.1, {
scale: 1
}).start();
}
}, t + .1);
t <= 0 && o._onScrolling();
}
}
};
t.prototype._calcNearestItem = function() {
var e, t, n, r, o, i, s = this;
s.nearestListId = null;
s._virtual && s._calcViewPos();
n = s.viewTop;
r = s.viewRight;
o = s.viewBottom;
i = s.viewLeft;
for (var a = !1, c = 0; c < s.content.childrenCount && !a; c += s._colLineNum) if (e = s._virtual ? s.displayData[c] : s._calcExistItemPos(c)) {
t = s._sizeType ? (e.top + e.bottom) / 2 : t = (e.left + e.right) / 2;
switch (s._alignCalcType) {
case 1:
if (e.right >= i) {
s.nearestListId = e.id;
i > t && (s.nearestListId += s._colLineNum);
a = !0;
}
break;

case 2:
if (e.left <= r) {
s.nearestListId = e.id;
r < t && (s.nearestListId += s._colLineNum);
a = !0;
}
break;

case 3:
if (e.bottom <= n) {
s.nearestListId = e.id;
n < t && (s.nearestListId += s._colLineNum);
a = !0;
}
break;

case 4:
if (e.top >= o) {
s.nearestListId = e.id;
o > t && (s.nearestListId += s._colLineNum);
a = !0;
}
}
}
if ((e = s._virtual ? s.displayData[s.displayItemNum - 1] : s._calcExistItemPos(s._numItems - 1)) && e.id == s._numItems - 1) {
t = s._sizeType ? (e.top + e.bottom) / 2 : t = (e.left + e.right) / 2;
switch (s._alignCalcType) {
case 1:
r > t && (s.nearestListId = e.id);
break;

case 2:
i < t && (s.nearestListId = e.id);
break;

case 3:
o < t && (s.nearestListId = e.id);
break;

case 4:
n > t && (s.nearestListId = e.id);
}
}
};
t.prototype.prePage = function(e) {
void 0 === e && (e = .5);
this.checkInited() && this.skipPage(this.curPageNum - 1, e);
};
t.prototype.nextPage = function(e) {
void 0 === e && (e = .5);
this.checkInited() && this.skipPage(this.curPageNum + 1, e);
};
t.prototype.skipPage = function(e, t) {
var n = this;
if (n.checkInited()) {
if (n._slideMode != a.PAGE) return cc.error("This function is not allowed to be called, Must SlideMode = PAGE!");
if (!(e < 0 || e >= n._numItems) && n.curPageNum != e) {
n.curPageNum = e;
n.pageChangeEvent && cc.Component.EventHandler.emitEvents([ n.pageChangeEvent ], e);
n.scrollTo(e, t);
}
}
};
t.prototype.calcCustomSize = function(e) {
var t = this;
if (t.checkInited()) {
if (!t._itemTmp) return cc.error("Unset template item!");
if (!t.renderEvent) return cc.error("Unset Render-Event!");
t._customSize = {};
var n = cc.instantiate(t._itemTmp);
t.content.addChild(n);
for (var r = 0; r < e; r++) {
cc.Component.EventHandler.emitEvents([ t.renderEvent ], n, r);
n.height == t._itemSize.height && n.width == t._itemSize.width || (t._customSize[r] = t._sizeType ? n.height : n.width);
}
Object.keys(t._customSize).length || (t._customSize = null);
n.removeFromParent();
n.destroy && n.destroy();
return t._customSize;
}
};
i([ p({
type: cc.Enum(s),
tooltip: !1
}) ], t.prototype, "templateType", void 0);
i([ p({
type: cc.Node,
tooltip: !1,
visible: function() {
return this.templateType == s.NODE;
}
}) ], t.prototype, "tmpNode", void 0);
i([ p({
type: cc.Prefab,
tooltip: !1,
visible: function() {
return this.templateType == s.PREFAB;
}
}) ], t.prototype, "tmpPrefab", void 0);
i([ p() ], t.prototype, "_slideMode", void 0);
i([ p({
type: cc.Enum(a),
tooltip: !1
}) ], t.prototype, "slideMode", null);
i([ p({
type: cc.Float,
range: [ 0, 1, .1 ],
tooltip: !1,
slide: !0,
visible: function() {
return this._slideMode == a.PAGE;
}
}) ], t.prototype, "pageDistance", void 0);
i([ p({
type: cc.Component.EventHandler,
tooltip: !1,
visible: function() {
return this._slideMode == a.PAGE;
}
}) ], t.prototype, "pageChangeEvent", void 0);
i([ p() ], t.prototype, "_virtual", void 0);
i([ p({
type: cc.Boolean,
tooltip: !1
}) ], t.prototype, "virtual", null);
i([ p({
tooltip: !1,
visible: function() {
var e = this.slideMode == a.NORMAL;
e || (this.cyclic = !1);
return e;
}
}) ], t.prototype, "cyclic", void 0);
i([ p({
tooltip: !1,
visible: function() {
return this.virtual;
}
}) ], t.prototype, "lackCenter", void 0);
i([ p({
tooltip: !1,
visible: function() {
var e = this.virtual && !this.lackCenter;
e || (this.lackSlide = !1);
return e;
}
}) ], t.prototype, "lackSlide", void 0);
i([ p({
type: cc.Integer
}) ], t.prototype, "_updateRate", void 0);
i([ p({
type: cc.Integer,
range: [ 0, 6, 1 ],
tooltip: !1,
slide: !0
}) ], t.prototype, "updateRate", null);
i([ p({
type: cc.Integer,
range: [ 0, 12, 1 ],
tooltip: !1,
slide: !0
}) ], t.prototype, "frameByFrameRenderNum", void 0);
i([ p({
type: cc.Component.EventHandler,
tooltip: !1
}) ], t.prototype, "renderEvent", void 0);
i([ p({
type: cc.Enum(c),
tooltip: !1
}) ], t.prototype, "selectedMode", void 0);
i([ p({
tooltip: !1,
visible: function() {
return this.selectedMode == c.SINGLE;
}
}) ], t.prototype, "repeatEventSingle", void 0);
i([ p({
type: cc.Component.EventHandler,
tooltip: !1,
visible: function() {
return this.selectedMode > c.NONE;
}
}) ], t.prototype, "selectedEvent", void 0);
i([ p({
serializable: !1
}) ], t.prototype, "_numItems", void 0);
return i([ u, f(), d("自定义组件/ListUtil"), y(cc.ScrollView), h(-5e3) ], t);
}(cc.Component);
n.default = g;
cc._RF.pop();
}, {
"./ListItemUtil": "ListItemUtil"
} ],
LocalStorageMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "8a430yDeHJLG78dMGiAA1LI", "LocalStorageMgr");
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e() {}
e.setItem = function(e, t) {
cc.sys.localStorage.setItem("model_" + e, JSON.stringify(t));
};
e.getItem = function(e) {
var t = JSON.parse(cc.sys.localStorage.getItem("model_" + e));
return t && "" !== t ? t : {};
};
e.removeItem = function(e) {
cc.sys.localStorage.removeItem("model_" + e);
};
e.clearAll = function() {
cc.sys.localStorage.clear();
};
return e;
}();
n.default = r;
cc._RF.pop();
}, {} ],
Main: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "e1b90/rohdEk4SdmmEZANaD", "Main");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = (s.property, function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.onLoad = function() {};
t.prototype.start = function() {};
t.prototype.adaptiveNoteLayout = function() {
var e = cc.winSize;
cc.log("--当前游戏窗口大小  w:" + e.width + "   h:" + e.height);
var t = cc.view.getFrameSize();
cc.log("--视图边框尺寸：w:" + t.width + "  h:" + t.height);
var n = cc.view.getCanvasSize();
cc.log("--视图中canvas尺寸  w:" + n.width + "  H:" + n.height);
var r = cc.view.getVisibleSize();
cc.log("--视图中窗口可见区域的尺寸 w:" + r.width + "   h:" + r.height);
var o = cc.view.getDesignResolutionSize();
cc.log("--设计分辨率：" + o.width + "    h: " + o.height);
cc.log("--当前节点的尺寸 w:" + this.node.width + "   h:" + this.node.height);
};
return i([ a ], t);
}(cc.Component));
n.default = c;
cc._RF.pop();
}, {} ],
ModalMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a6cfbGOowFD6rGCFIN9QFMs", "ModalMgr");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../Common/SysDefine"), l = e("../Common/UIModalScript"), u = cc._decorator, p = u.ccclass, f = (u.property, 
function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.uiModal = null;
return t;
}
n = t;
Object.defineProperty(t, "inst", {
get: function() {
if (null == this._inst) {
this._inst = new n();
var e = new cc.Node("UIModalNode");
cc.find(n.popUpRoot).addChild(e);
n.inst.uiModal = e.addComponent(l.default);
n.inst.uiModal.init();
}
return this._inst;
},
enumerable: !1,
configurable: !0
});
t.prototype.showModal = function(e) {
return s(this, void 0, void 0, function() {
return a(this, function(t) {
switch (t.label) {
case 0:
return [ 4, this.uiModal.showModal(e.opacity, e.easingTime, e.isEasing) ];

case 1:
t.sent();
return [ 2 ];
}
});
});
};
t.prototype.checkModalWindow = function(e) {
if (e.length <= 0) this.uiModal.node.active = !1; else {
this.uiModal.node.active = !0;
this.uiModal.node.parent && this.uiModal.node.removeFromParent();
for (var t = e.length - 1; t >= 0; t--) if (e[t].modalType.opacity > 0) {
cc.find(n.popUpRoot).addChild(this.uiModal.node, Math.max(e[t].node.zIndex - 1, 0));
this.uiModal.fid = e[t].fid;
this.showModal(e[t].modalType);
break;
}
}
};
var n;
t.popUpRoot = c.SysDefine.SYS_UIROOT_NAME + "/" + c.SysDefine.SYS_POPUP_NODE;
t._inst = null;
return n = i([ p ], t);
}(cc.Component));
n.default = f;
cc._RF.pop();
}, {
"../Common/SysDefine": "SysDefine",
"../Common/UIModalScript": "UIModalScript"
} ],
MsgChat: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "b7ab3C8sUJFFZ1WaEG+Ke/U", "MsgChat");
Object.defineProperty(n, "__esModule", {
value: !0
});
cc._RF.pop();
}, {} ],
NativeMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "3a5e4ZLEzlMspl1zSdRocfn", "NativeMgr");
var r, o = this && this.__spreadArrays || function() {
for (var e = 0, t = 0, n = arguments.length; t < n; t++) e += arguments[t].length;
var r = Array(e), o = 0;
for (t = 0; t < n; t++) for (var i = arguments[t], s = 0, a = i.length; s < a; s++, 
o++) r[o] = i[s];
return r;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
n.NativeMgr = n.AndrodSign = void 0;
(function(e) {
e.Void = "V";
e.String = "Ljava/lang/String;";
e.Boolean = "Z";
e.Float = "F";
e.Double = "D";
e.Int = "I";
})(r = n.AndrodSign || (n.AndrodSign = {}));
var i = function() {
function e() {}
e.callback = function(e) {
for (var t = [], n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
var r = this.cbs[e];
if (r) {
delete this.cbs[e];
r.apply(void 0, t);
} else cc.log("no func ", e);
};
e._newCB = function(e) {
this.cbIdx++;
var t = "" + this.cbIdx;
this.cbs[t] = e;
return t;
};
e.getStr = function(e, t) {
for (var n = [], i = 2; i < arguments.length; i++) n[i - 2] = arguments[i];
return this.callWithPackage.apply(this, o([ this.defaultPackage, e, t, r.String ], n));
};
e.callNativeClass = function(e, t) {
for (var n = [], i = 2; i < arguments.length; i++) n[i - 2] = arguments[i];
console.log("调用1：" + e + "   " + t);
this.callWithPackage.apply(this, o([ this.defaultPackage, e, t, r.Void ], n));
};
e.callWithPackage = function(e, t, n, i) {
for (var s, a, c = [], l = 4; l < arguments.length; l++) c[l - 4] = arguments[l];
var u = [];
console.log("调用2：" + e + t + "   " + n);
cc.log("clazz:", t);
cc.log("method:", n);
if (cc.sys.os == cc.sys.OS_ANDROID) {
for (var p = "", f = 0; f < c.length; f++) switch (typeof (d = c[f])) {
case "boolean":
p += r.Boolean;
u.push(d + "");
break;

case "string":
p += r.String;
u.push(d);
break;

case "number":
p += r.Float;
u.push(d);
break;

case "function":
p += r.String;
u.push(this._newCB(d));
}
return (s = jsb.reflection).callStaticMethod.apply(s, o([ e + t, n, "(" + p + ")" + i ], u));
}
if (cc.sys.os == cc.sys.OS_IOS) {
for (f = 0; f < c.length; f++) {
var d;
"function" == typeof (d = c[f]) ? u.push(this._newCB(d)) : u.push(d);
n += 0 == f ? ":" : "arg" + f + ":";
}
console.log("clazz:" + t);
console.log("method:" + n);
return (a = jsb.reflection).callStaticMethod.apply(a, o([ t, n ], u));
}
};
e.cbIdx = 0;
e.cbs = {};
e.defaultPackage = "org/cocos2dx/javascript/";
return e;
}();
n.NativeMgr = i;
window.nativeMgr = i;
cc._RF.pop();
}, {} ],
NetMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "3d5eaVhMq5IfYImrOWQEXkk", "NetMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("../../Protocol/bundle"), s = e("./GameMgr"), a = function() {
function e() {}
e.prototype.onLoginReq = function(e) {
return r(this, void 0, void 0, function() {
var t;
return o(this, function(n) {
switch (n.label) {
case 0:
return [ 4, s.default.clientChannel.required(i.GameServer).login({
accessToken: e
}) ];

case 1:
t = n.sent();
console.log(t);
return [ 2 ];
}
});
});
};
return e;
}();
n.default = new a();
cc._RF.pop();
}, {
"../../Protocol/bundle": "bundle",
"./GameMgr": "GameMgr"
} ],
Pool: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "284d8zxqTdJtYjT+CGS5XDE", "Pool");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.Pool = void 0;
var r = function() {
function e(e, t) {
this._fn = e;
this._idx = t - 1;
this._frees = new Array(t);
for (var n = 0; n < t; n++) this._frees[n] = e();
}
Object.defineProperty(e.prototype, "freeCount", {
get: function() {
return this._frees.length;
},
enumerable: !1,
configurable: !0
});
e.prototype.alloc = function() {
this._idx < 0 && this._expand(Math.round(1.2 * this._frees.length) + 1);
var e = this._frees[this._idx];
this._frees.splice(this._idx);
--this._idx;
e.use && e.use();
return e;
};
e.prototype.free = function(e) {
++this._idx;
e.free && e.free();
this._frees[this._idx] = e;
};
e.prototype.clear = function(e) {
for (var t = 0; t < this._idx; t++) e && e(this._frees[t]);
this._frees.splice(0);
this._idx = -1;
};
e.prototype._expand = function(e) {
var t = this._frees;
this._frees = new Array(e);
for (var n = e - t.length, r = 0; r < n; r++) this._frees[r] = this._fn();
r = n;
for (var o = 0; r < e; ++r, ++o) this._frees[r] = t[o];
this._idx += n;
};
return e;
}();
n.Pool = r;
cc._RF.pop();
}, {} ],
PriorityQueue: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "f2318nn5GJChLoRevUhhLkg", "PriorityQueue");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.PriorityElement = void 0;
var r = function(e, t) {
this.data = e;
this.priority = t;
};
n.PriorityElement = r;
var o = function() {
function e() {
this.queue = new Array(32);
this._size = 0;
}
Object.defineProperty(e.prototype, "size", {
get: function() {
return this._size;
},
enumerable: !1,
configurable: !0
});
e.prototype.hasElement = function(e) {
for (var t = 0, n = this.queue; t < n.length; t++) if (n[t].data === e) return !0;
return !1;
};
e.prototype.enqueue = function(e, t) {
void 0 === t && (t = 0);
this._size > this.queue.length && this._expand();
this.queue[this._size++] = new r(e, t);
this.upAdjust();
};
e.prototype.dequeue = function() {
if (this._size <= 0) return null;
var e = this.queue[0];
this.queue[0] = this.queue[--this._size];
this.downAdjust();
return e.data;
};
e.prototype.clear = function() {
this.queue = [];
this._size = 0;
};
e.prototype.upAdjust = function() {
for (var e = this._size - 1, t = Math.floor(e / 2), n = this.queue[e]; e > 0 && n.priority > this.queue[t].priority; ) {
this.queue[e] = this.queue[t];
e = t;
t = Math.floor(t / 2);
}
this.queue[e] = n;
};
e.prototype.downAdjust = function() {
for (var e = 0, t = this.queue[e], n = 1; n < this._size; ) {
n + 1 < this._size && this.queue[n + 1].priority > this.queue[n].priority && n++;
if (t.priority >= this.queue[n].priority) break;
this.queue[e] = this.queue[n];
e = n;
n = 2 * n + 1;
}
this.queue[e] = t;
};
e.prototype._expand = function() {
var e = Math.round(1.2 * this.queue.length) + 1, t = this.queue;
this.queue = new Array(e);
for (var n = 0; n < t.length; n++) this.queue[n] = t[n];
};
e.prototype.toString = function() {
for (var e = "", t = 0; t < this._size; t++) {
var n = this.queue[t].data;
n.toString ? e += n.toString() : e += "object" == typeof n ? JSON.stringify(n) : n;
}
return e;
};
return e;
}();
n.default = o;
cc._RF.pop();
}, {} ],
PriorityStack: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "9ed15uYIzxGna5Bj3+zkuo8", "PriorityStack");
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = e("./PriorityQueue"), o = function() {
function e() {
this.stack = new Array();
this._size = 0;
}
Object.defineProperty(e.prototype, "size", {
get: function() {
return this._size;
},
enumerable: !1,
configurable: !0
});
e.prototype.clear = function() {
this.stack.length = 0;
this._size = 0;
return !0;
};
e.prototype.getTopEPriority = function() {
return this.stack.length <= 0 ? -1 : this.stack[this.stack.length - 1].priority;
};
e.prototype.getTopElement = function() {
return this.stack.length <= 0 ? null : this.stack[this.stack.length - 1].data;
};
e.prototype.getElements = function() {
for (var e = [], t = 0, n = this.stack; t < n.length; t++) {
var r = n[t];
e.push(r.data);
}
return e;
};
e.prototype.push = function(e, t) {
void 0 === t && (t = 0);
this.stack.push(new r.PriorityElement(e, t));
this._size++;
this._adjust();
};
e.prototype.pop = function() {
if (this.stack.length <= 0) return null;
this._size--;
return this.stack.pop().data;
};
e.prototype._adjust = function() {
for (var e = this.stack.length - 1; e > 0; e--) this.stack[e] < this.stack[e - 1] && this._swap(e, e - 1);
};
e.prototype._swap = function(e, t) {
var n = this.stack[e];
this.stack[e] = this.stack[t];
this.stack[t] = n;
};
e.prototype.hasElement = function(e) {
for (var t = 0, n = this.stack; t < n.length; t++) if (n[t].data === e) return !0;
return !1;
};
e.prototype.remove = function(e) {
for (var t = this.stack.length - 1; t >= 0; t--) if (this.stack[t].data === e) {
this.stack.splice(t, 1);
this._size--;
return !0;
}
return !1;
};
return e;
}();
n.default = o;
cc._RF.pop();
}, {
"./PriorityQueue": "PriorityQueue"
} ],
PtlLogin: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "16391fGPDFLzJBtiNjG8SgG", "PtlLogin");
Object.defineProperty(n, "__esModule", {
value: !0
});
cc._RF.pop();
}, {} ],
PtlSend: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "946c1k6z65MAbkqTdqMqadd", "PtlSend");
Object.defineProperty(n, "__esModule", {
value: !0
});
cc._RF.pop();
}, {} ],
ResMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a1946LwxMZELpC3XyersJGl", "ResMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("../Net/EventCenter"), s = e("../Utils/CocosHelper"), a = function() {
function e() {
this._prefabDepends = cc.js.createMap();
this._dynamicTags = cc.js.createMap();
this._tmpAssetsDepends = [];
this._assetsReference = cc.js.createMap();
}
Object.defineProperty(e, "inst", {
get: function() {
null === this.instance && (this.instance = new e());
return this.instance;
},
enumerable: !1,
configurable: !0
});
e.prototype.loadForm = function(e) {
return r(this, void 0, void 0, function() {
var t, n, r;
return o(this, function(o) {
switch (o.label) {
case 0:
return [ 4, this._loadResWithReference(e, cc.Prefab) ];

case 1:
t = o.sent(), n = t.res, r = t.deps;
this._prefabDepends[e] = r;
return [ 2, cc.instantiate(n) ];
}
});
});
};
e.prototype.destoryForm = function(e) {
if (e) {
i.EventCenter.targetOff(e);
this._destoryResWithReference(this._prefabDepends[e.fid]);
this._prefabDepends[e.fid] = null;
delete this._prefabDepends[e.fid];
e.node.destroy();
}
};
e.prototype.loadDynamicRes = function(e, t, n) {
return r(this, void 0, void 0, function() {
var r, i, s, a;
return o(this, function(o) {
switch (o.label) {
case 0:
return [ 4, this._loadResWithReference(e, t) ];

case 1:
r = o.sent(), i = r.res, s = r.deps;
this._dynamicTags[n] || (this._dynamicTags[n] = []);
(a = this._dynamicTags[n]).push.apply(a, s);
return [ 2, i ];
}
});
});
};
e.prototype.destoryDynamicRes = function(e) {
if (!this._dynamicTags[e]) return !1;
this._destoryResWithReference(this._dynamicTags[e]);
this._dynamicTags[e] = null;
delete this._dynamicTags[e];
return !0;
};
e.prototype._loadResWithReference = function(e, t) {
return r(this, void 0, void 0, function() {
var n, r;
return o(this, function(o) {
switch (o.label) {
case 0:
return [ 4, s.default.loadResSync(e, t, this._addTmpAssetsDepends.bind(this)) ];

case 1:
if (!(n = o.sent())) {
this._clearTmpAssetsDepends();
return [ 2, null ];
}
this._clearTmpAssetsDepends();
(r = cc.assetManager.dependUtil.getDepsRecursively(n._uuid) || []).push(n._uuid);
this.addAssetsDepends(r);
return [ 2, {
res: n,
deps: r
} ];
}
});
});
};
e.prototype._destoryResWithReference = function(e) {
var t = this.removeAssetsDepends(e);
this._destoryAssets(t);
return !0;
};
e.prototype.addAssetsDepends = function(e) {
for (var t = 0, n = e; t < n.length; t++) {
var r = n[t];
this._checkIsBuiltinAssets(r) || (this._assetsReference[r] ? this._assetsReference[r] += 1 : this._assetsReference[r] = 1);
}
};
e.prototype.removeAssetsDepends = function(e) {
for (var t = [], n = 0, r = e; n < r.length; n++) {
var o = r[n];
if (this._assetsReference[o] && 0 !== this._assetsReference[o]) {
this._assetsReference[o]--;
if (0 === this._assetsReference[o]) {
t.push(o);
delete this._assetsReference[o];
}
}
}
return t;
};
e.prototype._destoryAssets = function(e) {
for (var t = 0, n = e; t < n.length; t++) {
var r = n[t];
this._destoryAsset(r);
}
};
e.prototype._destoryAsset = function(e) {
if (!this._checkIsBuiltinAssets(e)) {
cc.assetManager.assets.remove(e);
var t = cc.assetManager.assets.get(e);
t && t.destroy();
cc.assetManager.dependUtil.remove(e);
}
};
e.prototype._addTmpAssetsDepends = function(e, t, n) {
var r, o = cc.assetManager.dependUtil.getDepsRecursively(n.uuid) || [];
o.push(n.uuid);
this.addAssetsDepends(o);
(r = this._tmpAssetsDepends).push.apply(r, o);
};
e.prototype._clearTmpAssetsDepends = function() {
for (var e = 0, t = this._tmpAssetsDepends; e < t.length; e++) {
var n = t[e];
if (this._assetsReference[n] && 0 !== this._assetsReference[n]) {
this._assetsReference[n]--;
0 === this._assetsReference[n] && delete this._assetsReference[n];
}
}
this._tmpAssetsDepends = [];
};
e.prototype._checkIsBuiltinAssets = function(e) {
var t = cc.assetManager.assets.get(e);
return !(!t || -1 == t._name.indexOf("builtin"));
};
e.prototype.computeTextureCache = function() {
var e = cc.assetManager.assets, t = 0, n = 0;
e.forEach(function(e) {
if ("cc.Texture2D" == (e && e.__classname__ ? e.__classname__ : "")) {
var r = e, o = r.width * r.height * ((".jpg" === r._native ? 3 : 4) / 1024 / 1024);
t += o;
n++;
}
});
return "缓存 [纹理总数:" + n + "][纹理缓存:" + t.toFixed(2) + "M]";
};
e.instance = null;
return e;
}();
n.default = a;
cc._RF.pop();
}, {
"../Net/EventCenter": "EventCenter",
"../Utils/CocosHelper": "CocosHelper"
} ],
RpcConent: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "24e90+N+BJCD69wJUWlBqdD", "RpcConent");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.apiClient = void 0;
var r = e("tsrpc-browser"), o = e("./shared/protocols/serviceProto");
n.apiClient = new r.WsClient(o.serviceProto, {
server: "ws://10.0.0.11:3000",
json: !1,
logger: console
});
cc._RF.pop();
}, {
"./shared/protocols/serviceProto": "serviceProto",
"tsrpc-browser": 290
} ],
SceneMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "8337d/EvnBHjrW/BJTtgaeW", "SceneMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("../Common/SysDefine"), s = e("./TipsMgr"), a = e("./UIManager"), c = function() {
function e() {
this._scenes = [];
this._currScene = "";
}
e.prototype.getCurrScene = function() {
return a.default.getInstance().getForm(this._currScene);
};
e.prototype.open = function(e, t, n) {
return r(this, void 0, void 0, function() {
var r, i, s;
return o(this, function(o) {
switch (o.label) {
case 0:
if (this._currScene == e) {
cc.warn("SceneMgr", "当前场景和需要open的场景是同一个");
return [ 2, null ];
}
return n && n.isShowLoading ? [ 4, this.openLoading(null == n ? void 0 : n.loadingForm, t, n) ] : [ 3, 2 ];

case 1:
o.sent();
o.label = 2;

case 2:
if (!(this._scenes.length > 0)) return [ 3, 4 ];
r = this._scenes[this._scenes.length - 1];
return [ 4, a.default.getInstance().closeForm(r) ];

case 3:
o.sent();
o.label = 4;

case 4:
-1 == (i = this._scenes.indexOf(e)) ? this._scenes.push(e) : this._scenes.length = i + 1;
this._currScene = e;
return [ 4, a.default.getInstance().openForm(e, t, n) ];

case 5:
s = o.sent();
return n && n.isShowLoading ? [ 4, this.closeLoading(null == n ? void 0 : n.loadingForm) ] : [ 3, 7 ];

case 6:
o.sent();
o.label = 7;

case 7:
return [ 2, s ];
}
});
});
};
e.prototype.back = function(e, t) {
return r(this, void 0, void 0, function() {
var n;
return o(this, function(r) {
switch (r.label) {
case 0:
if (this._scenes.length <= 1) {
cc.warn("SceneMgr", "已经是最后一个场景了, 无处可退");
return [ 2 ];
}
return t && t.isShowLoading ? [ 4, this.openLoading(null == t ? void 0 : t.loadingForm, e, t) ] : [ 3, 2 ];

case 1:
r.sent();
r.label = 2;

case 2:
n = this._scenes.pop();
return [ 4, a.default.getInstance().closeForm(n) ];

case 3:
r.sent();
this._currScene = this._scenes[this._scenes.length - 1];
return [ 4, a.default.getInstance().openForm(this._currScene, e, t) ];

case 4:
r.sent();
return t && t.isShowLoading ? [ 4, this.closeLoading(null == t ? void 0 : t.loadingForm) ] : [ 3, 6 ];

case 5:
r.sent();
r.label = 6;

case 6:
return [ 2 ];
}
});
});
};
e.prototype.close = function(e) {
return r(this, void 0, void 0, function() {
return o(this, function() {
return a.default.getInstance().getForm(e) ? [ 2, a.default.getInstance().closeForm(e) ] : [ 2 ];
});
});
};
e.prototype.openLoading = function(e, t, n) {
return r(this, void 0, void 0, function() {
var r;
return o(this, function(o) {
switch (o.label) {
case 0:
return (r = e || i.SysDefine.defaultLoadingForm) ? [ 4, s.default.open(r.prefabUrl, t, n) ] : [ 2 ];

case 1:
o.sent();
return [ 2 ];
}
});
});
};
e.prototype.closeLoading = function(e) {
return r(this, void 0, void 0, function() {
var t;
return o(this, function(n) {
switch (n.label) {
case 0:
return (t = e || i.SysDefine.defaultLoadingForm) ? [ 4, s.default.close(t.prefabUrl) ] : [ 2 ];

case 1:
n.sent();
return [ 2 ];
}
});
});
};
return e;
}();
n.default = new c();
cc._RF.pop();
}, {
"../Common/SysDefine": "SysDefine",
"./TipsMgr": "TipsMgr",
"./UIManager": "UIManager"
} ],
Scene: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "b13f3yncLhOvJSRs1QLB0sR", "Scene");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../Manager/FormMgr"), l = e("../Manager/GameMgr"), u = e("../Net/EventCenter"), p = e("../Net/EventType"), f = e("../UIConfig"), d = cc._decorator, h = d.ccclass, y = (d.property, 
function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.ndBlock = null;
t._block = 0;
return t;
}
n = t;
t.prototype.onLoad = function() {
this.initBlockNode();
};
t.prototype.initBlockNode = function() {
this.ndBlock = new cc.Node("block");
this.ndBlock.addComponent(cc.BlockInputEvents);
this.node.addChild(this.ndBlock, cc.macro.MAX_ZINDEX);
};
t.prototype.start = function() {
return s(this, void 0, void 0, function() {
return a(this, function(e) {
switch (e.label) {
case 0:
n.inst = this;
return [ 4, this.onGameInit() ];

case 1:
e.sent();
this.registerEvent();
return [ 2 ];
}
});
});
};
t.prototype.onGameInit = function() {
return s(this, void 0, void 0, function() {
return a(this, function(e) {
switch (e.label) {
case 0:
c.default.open(f.default.UILogin);
return [ 4, l.default.init(this.node) ];

case 1:
e.sent();
return [ 2 ];
}
});
});
};
t.prototype.registerEvent = function() {
cc.game.on(cc.game.EVENT_SHOW, this.onGameShow, this);
cc.game.on(cc.game.EVENT_HIDE, this.onGameHide, this);
};
t.prototype.onGameShow = function(e) {
u.EventCenter.emit(p.EventType.GameShow, e);
cc.director.resume();
};
t.prototype.onGameHide = function() {
u.EventCenter.emit(p.EventType.GameHide);
cc.director.pause();
};
t.prototype.onLogin = function() {
console.log("登陆成功===============");
c.default.open(f.default.UIHome);
};
t.prototype.update = function(e) {
l.default.update(e);
};
t.prototype.lateUpdate = function() {};
t.prototype.setInputBlock = function(e) {
if (this.ndBlock) {
e ? ++this._block : --this._block;
this.ndBlock.active = this._block > 0;
} else cc.warn("未启用 block input");
};
var n;
t.inst = null;
return n = i([ h ], t);
}(cc.Component));
n.default = y;
cc._RF.pop();
}, {
"../Manager/FormMgr": "FormMgr",
"../Manager/GameMgr": "GameMgr",
"../Net/EventCenter": "EventCenter",
"../Net/EventType": "EventType",
"../UIConfig": "UIConfig"
} ],
SoundMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "712729Tx0JBoI/gunNGv3kP", "SoundMgr");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../Common/SysDefine"), l = e("../Utils/CocosHelper"), u = e("./LocalStorageMgr"), p = cc._decorator, f = p.ccclass, d = (p.property, 
function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.audioCache = cc.js.createMap();
t.currEffectId = -1;
t.currMusicId = -1;
t.volume = new h();
return t;
}
Object.defineProperty(t, "inst", {
get: function() {
null == this._inst && (this._inst = cc.find(c.SysDefine.SYS_UIROOT_NAME).addComponent(this));
return this._inst;
},
enumerable: !1,
configurable: !0
});
t.prototype.onLoad = function() {
var e = this.getVolumeToLocal();
if (e) this.volume = e; else {
this.volume.musicVolume = 1;
this.volume.effectVolume = 1;
}
this.setVolumeToLocal();
cc.game.on(cc.game.EVENT_HIDE, function() {
cc.audioEngine.pauseAll();
}, this);
cc.game.on(cc.game.EVENT_SHOW, function() {
cc.audioEngine.resumeAll();
}, this);
};
t.prototype.getVolume = function() {
return this.volume;
};
t.prototype.start = function() {};
t.prototype.setMusicVolume = function(e) {
this.volume.musicVolume = e;
this.setVolumeToLocal();
};
t.prototype.setEffectVolume = function(e) {
this.volume.effectVolume = e;
this.setVolumeToLocal();
};
t.prototype.playMusic = function(e, t) {
void 0 === t && (t = !0);
return s(this, void 0, void 0, function() {
var n;
return a(this, function(r) {
switch (r.label) {
case 0:
if (!e || "" === e) return [ 2 ];
if (this.audioCache[e]) {
cc.audioEngine.playMusic(this.audioCache[e], t);
return [ 2 ];
}
return [ 4, l.default.loadResSync(e, cc.AudioClip) ];

case 1:
n = r.sent();
this.audioCache[e] = n;
this.currMusicId = cc.audioEngine.playMusic(n, t);
return [ 2 ];
}
});
});
};
t.prototype.playEffect = function(e, t) {
void 0 === t && (t = !1);
return s(this, void 0, void 0, function() {
var n;
return a(this, function(r) {
switch (r.label) {
case 0:
if (!e || "" === e) return [ 2 ];
if (this.audioCache[e]) {
cc.audioEngine.playEffect(this.audioCache[e], t);
return [ 2 ];
}
return [ 4, l.default.loadResSync(e, cc.AudioClip) ];

case 1:
n = r.sent();
this.audioCache[e] = n;
this.currEffectId = cc.audioEngine.playEffect(n, t);
return [ 2 ];
}
});
});
};
t.prototype.getVolumeToLocal = function() {
return u.default.getItem("Volume") || null;
};
t.prototype.setVolumeToLocal = function() {
cc.audioEngine.setMusicVolume(this.volume.musicVolume);
cc.audioEngine.setEffectsVolume(this.volume.effectVolume);
u.default.setItem("Volume", this.volume);
};
t.prototype.setEffectActive = function(e, t) {
void 0 === t && (t = -1);
e ? cc.audioEngine.stop(t < 0 ? this.currEffectId : t) : cc.audioEngine.resume(t < 0 ? this.currEffectId : t);
};
t._inst = null;
return i([ f ], t);
}(cc.Component));
n.default = d;
var h = function() {};
cc._RF.pop();
}, {
"../Common/SysDefine": "SysDefine",
"../Utils/CocosHelper": "CocosHelper",
"./LocalStorageMgr": "LocalStorageMgr"
} ],
Struct: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "294d4cuj8FOwrGZgHqxSfsR", "Struct");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.EPriority = n.ModalType = void 0;
var r = e("./SysDefine");
n.ModalType = function(e, t, n, o) {
void 0 === e && (e = r.ModalOpacity.OpacityHalf);
void 0 === t && (t = !1);
void 0 === n && (n = !0);
void 0 === o && (o = .2);
this.opacity = r.ModalOpacity.OpacityHalf;
this.clickMaskClose = !1;
this.isEasing = !0;
this.easingTime = .2;
this.opacity = e;
this.clickMaskClose = t;
this.isEasing = n;
this.easingTime = o;
};
(function(e) {
e[e.ZERO = 0] = "ZERO";
e[e.ONE = 1] = "ONE";
e[e.TWO = 2] = "TWO";
e[e.THREE = 3] = "THREE";
e[e.FOUR = 4] = "FOUR";
e[e.FIVE = 5] = "FIVE";
e[e.SIX = 6] = "SIX";
e[e.SEVEN = 7] = "SEVEN";
e[e.EIGHT = 8] = "EIGHT";
e[e.NINE = 9] = "NINE";
})(n.EPriority || (n.EPriority = {}));
cc._RF.pop();
}, {
"./SysDefine": "SysDefine"
} ],
SysDefine: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "7f8ffNRvZxCz4zNi1L6dBHr", "SysDefine");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.SysDefine = n.UIState = n.ModalOpacity = n.FormType = void 0;
var r = e("../UIConfig");
(function(e) {
e.Screen = "UIScreen";
e.Fixed = "UIFixed";
e.Window = "UIWindow";
e.Tips = "UITips";
})(n.FormType || (n.FormType = {}));
(function(e) {
e[e.None = 0] = "None";
e[e.OpacityZero = 1] = "OpacityZero";
e[e.OpacityLow = 2] = "OpacityLow";
e[e.OpacityHalf = 3] = "OpacityHalf";
e[e.OpacityHigh = 4] = "OpacityHigh";
e[e.OpacityFull = 5] = "OpacityFull";
})(n.ModalOpacity || (n.ModalOpacity = {}));
(function(e) {
e[e.None = 0] = "None";
e[e.Loading = 1] = "Loading";
e[e.Showing = 2] = "Showing";
e[e.Hiding = 3] = "Hiding";
})(n.UIState || (n.UIState = {}));
var o = function() {
function e() {}
e.defaultLoadingForm = r.default.UILoading;
e.SYS_PATH_CANVAS = "Canvas";
e.SYS_PATH_UIFORMS_CONFIG_INFO = "UIFormsConfigInfo";
e.SYS_PATH_CONFIG_INFO = "SysConfigInfo";
e.SYS_UIROOT_NAME = "Canvas/Scene/UIROOT";
e.SYS_UIMODAL_NAME = "Canvas/Scene/UIROOT/UIModalManager";
e.SYS_UIAdapter_NAME = "Canvas/Scene/UIROOT/UIAdapterManager";
e.SYS_SCENE_NODE = "Scene";
e.SYS_UIROOT_NODE = "UIROOT";
e.SYS_SCREEN_NODE = "Screen";
e.SYS_FIXED_NODE = "FixedUI";
e.SYS_POPUP_NODE = "PopUp";
e.SYS_TOPTIPS_NODE = "TopTips";
e.SYS_STANDARD_Prefix = "_";
e.SYS_STANDARD_Separator = "$";
e.SYS_STANDARD_End = "#";
e.UI_PATH_ROOT = "UIForms/";
e.SeparatorMap = {
_Node: "cc.Node",
_Label: "cc.Label",
_Button: "cc.Button",
_Sprite: "cc.Sprite",
_RichText: "cc.RichText",
_Mask: "cc.Mask",
_MotionStreak: "cc.MotionStreak",
_TiledMap: "cc.TiledMap",
_TiledTile: "cc.TiledTile",
_Spine: "sp.Skeleton",
_Graphics: "cc.Graphics",
_Animation: "cc.Animation",
_WebView: "cc.WebView",
_EditBox: "cc.EditBox",
_ScrollView: "cc.ScrollView",
_VideoPlayer: "cc.VideoPlayer",
_ProgressBar: "cc.ProgressBar",
_PageView: "cc.PageView",
_Slider: "cc.Slider",
_Toggle: "cc.Toggle",
_ButtonPlus: "ButtonPlus"
};
return e;
}();
n.SysDefine = o;
cc._RF.pop();
}, {
"../UIConfig": "UIConfig"
} ],
TipsMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "6203812cXtNHJ8vqmVbOWHK", "TipsMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./UIManager"), s = function() {
function e() {}
e.prototype.open = function(e, t, n) {
return r(this, void 0, void 0, function() {
return o(this, function(r) {
switch (r.label) {
case 0:
return [ 4, i.default.getInstance().openForm(e, t, n) ];

case 1:
return [ 2, r.sent() ];
}
});
});
};
e.prototype.close = function(e) {
return r(this, void 0, void 0, function() {
return o(this, function(t) {
switch (t.label) {
case 0:
return [ 4, i.default.getInstance().closeForm(e) ];

case 1:
return [ 2, t.sent() ];
}
});
});
};
return e;
}();
n.default = new s();
cc._RF.pop();
}, {
"./UIManager": "UIManager"
} ],
TouchPlus: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "1c88eM+YFpH45yVr93vRF2l", "TouchPlus");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = (s.property, function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.offset = 15;
t.isTouch = !1;
t.isClick = !0;
return t;
}
t.prototype.addEvent = function(e, t) {
this.clickEvent = e;
this.slideEvent = t;
};
t.prototype.start = function() {
this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCancel, this);
};
t.prototype.touchStart = function(e) {
this.isTouch = !0;
this.startPosition = e.getLocation();
};
t.prototype.touchMove = function(e) {
if (this.isTouch && e.getLocation().sub(this.startPosition).mag() > this.offset) {
this.isClick = !1;
this.slideEvent && this.slideEvent(e);
}
};
t.prototype.touchEnd = function(e) {
if (this.isTouch) {
this.isTouch = !1;
this.isClick && this.clickEvent && this.clickEvent(e);
this.isClick = !0;
}
};
t.prototype.touchCancel = function() {
if (this.isTouch) {
this.isTouch = !1;
this.isClick = !0;
}
};
t.prototype.onDestroy = function() {
this.node.off(cc.Node.EventType.TOUCH_START, this.touchStart, this);
this.node.off(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
this.node.off(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.touchCancel, this);
};
return i([ a ], t);
}(cc.Component));
n.default = c;
cc._RF.pop();
}, {} ],
UIBagAdd_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "4ba4cGwhz9NZbN+sB7XryfO", "UIBagAdd_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.GetBtn = null;
t.NeedMoneyLab = null;
t.WatchBtn = null;
t.CloseBtn = null;
return t;
}
i([ l(s.default) ], t.prototype, "GetBtn", void 0);
i([ l(cc.Label) ], t.prototype, "NeedMoneyLab", void 0);
i([ l(s.default) ], t.prototype, "WatchBtn", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIBagAdd: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "22750YjxHtLIKGi0qEGQ/nH", "UIBagAdd");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIBag_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "56190PrXEZD0Je6FIjFFr3V", "UIBag_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.ContentViewNode = null;
t.BagNumLab = null;
t.CloseBtn = null;
t.AddBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "ContentViewNode", void 0);
i([ l(cc.Label) ], t.prototype, "BagNumLab", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
i([ l(s.default) ], t.prototype, "AddBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIBag: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "74ea71pHThDZ6ERDnoqDKMw", "UIBag");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ListUtil"), a = e("../../Common/UIForm"), c = e("../../Manager/FormMgr"), l = e("../../Manager/GameMgr"), u = e("../../UIConfig"), p = e("./ItemBag"), f = cc._decorator, d = f.ccclass, h = f.property, y = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.listBag = null;
return t;
}
t.prototype.onLoad = function() {
l.default.dataModalMgr.BagInfo.list_item = [ {
item_sp: "imgs/itemProduce/item1",
item_name: "毛笔碎片",
item_num: 1
} ];
this.listBag.numItems = l.default.dataModalMgr.BagInfo.list_item.length;
l.default.dataModalMgr.BagInfo.cur_bag_capacity = l.default.dataModalMgr.BagInfo.list_item.length;
};
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
this.view.AddBtn.addClick(function() {
c.default.open(u.default.UIBagAdd);
}, this);
this.view.BagNumLab.string = l.default.dataModalMgr.BagInfo.cur_bag_capacity + "/" + l.default.dataModalMgr.BagInfo.total_bag_capacity;
};
t.prototype.onListBagRender = function(e, t) {
e.getComponent(p.default).setData(l.default.dataModalMgr.BagInfo.list_item[t]);
};
i([ h(s.default) ], t.prototype, "listBag", void 0);
return i([ d ], t);
}(a.UIWindow);
n.default = y;
cc._RF.pop();
}, {
"../../Common/Components/ListUtil": "ListUtil",
"../../Common/UIForm": "UIForm",
"../../Manager/FormMgr": "FormMgr",
"../../Manager/GameMgr": "GameMgr",
"../../UIConfig": "UIConfig",
"./ItemBag": "ItemBag"
} ],
UIBase: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "5ba19xmlHJNg4x0oLeSB7Ay", "UIBase");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, s = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var a = e("../Manager/AdapterMgr"), c = e("../Manager/ResMgr"), l = e("../Manager/UIManager"), u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.formData = null;
t.willDestory = !1;
t._inited = !1;
t.view = null;
t.model = null;
t._blocker = null;
return t;
}
t.prototype._preInit = function(e) {
return i(this, void 0, void 0, function() {
var t;
return s(this, function(n) {
switch (n.label) {
case 0:
if (this._inited) return [ 2 ];
this._inited = !0;
this.view = this.getComponent(this.node.name + "_Auto");
return [ 4, this.load(e) ];

case 1:
if (t = n.sent()) {
cc.error(t);
this.closeSelf();
return [ 2 ];
}
this.onInit(e);
return [ 2 ];
}
});
});
};
t.prototype.load = function() {
return i(this, void 0, Promise, function() {
return s(this, function() {
return [ 2, null ];
});
});
};
t.prototype.onInit = function() {};
t.prototype.onShow = function() {};
t.prototype.onAfterShow = function() {};
t.prototype.onHide = function() {};
t.prototype.onAfterHide = function() {};
t.prototype.closeSelf = function() {
return i(this, void 0, Promise, function() {
return s(this, function(e) {
switch (e.label) {
case 0:
return [ 4, l.default.getInstance().closeForm(this.fid) ];

case 1:
return [ 2, e.sent() ];
}
});
});
};
t.prototype.showEffect = function() {
return i(this, void 0, void 0, function() {
return s(this, function() {
return [ 2 ];
});
});
};
t.prototype.hideEffect = function() {
return i(this, void 0, void 0, function() {
return s(this, function() {
return [ 2 ];
});
});
};
t.prototype.setBlockInput = function(e) {
if (!this._blocker) {
var t = new cc.Node("block_input_events");
this._blocker = t.addComponent(cc.BlockInputEvents);
this._blocker.node.setContentSize(a.default.inst.visibleSize);
this.node.addChild(this._blocker.node, cc.macro.MAX_ZINDEX);
}
this._blocker.node.active = e;
};
t.prototype.loadRes = function(e, t) {
return i(this, void 0, void 0, function() {
return s(this, function(n) {
switch (n.label) {
case 0:
return [ 4, c.default.inst.loadDynamicRes(e, t, this.fid) ];

case 1:
return [ 2, n.sent() ];
}
});
});
};
return t;
}(cc.Component);
n.default = u;
cc.UIBase = u;
cc._RF.pop();
}, {
"../Manager/AdapterMgr": "AdapterMgr",
"../Manager/ResMgr": "ResMgr",
"../Manager/UIManager": "UIManager"
} ],
UIBuyTip_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "5a9d6iPD+BFKJM7cZ885TPW", "UIBuyTip_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BuyTipLab = null;
t.BuyMoneyTypeSp = null;
t.BuyName = null;
t.BtnSure = null;
t.BuyIconTypeSp = null;
t.BtnClose = null;
return t;
}
i([ l(cc.Label) ], t.prototype, "BuyTipLab", void 0);
i([ l(cc.Sprite) ], t.prototype, "BuyMoneyTypeSp", void 0);
i([ l(cc.Label) ], t.prototype, "BuyName", void 0);
i([ l(s.default) ], t.prototype, "BtnSure", void 0);
i([ l(cc.Sprite) ], t.prototype, "BuyIconTypeSp", void 0);
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIBuyTip: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "27ffa5pc9VGLr77GWR76qNs", "UIBuyTip");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIColleCtionConversion_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "6d5d4lMWydEZZCbxR6GZL2Y", "UIColleCtionConversion_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.Msg = null;
t.MoneyNameLab = null;
t.BtnSure = null;
t.BtnClose = null;
t.MoneyIconSp = null;
return t;
}
i([ l(cc.Label) ], t.prototype, "Msg", void 0);
i([ l(cc.Label) ], t.prototype, "MoneyNameLab", void 0);
i([ l(s.default) ], t.prototype, "BtnSure", void 0);
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(cc.Sprite) ], t.prototype, "MoneyIconSp", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIColleCtionConversion: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "0d70eaL36hMJ4wqeyrm+WRj", "UIColleCtionConversion");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UICollection_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "4c62a0jAhdDIp65t6olWl8V", "UICollection_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.ContentNode = null;
t.WenFangSiBaoProgress = null;
t.WenFangSiBaoProgressLab = null;
t.BtnWenFangSiBao = null;
t.WenFangSiBaoGetNumTipLab = null;
t.SiJunZiProgress = null;
t.SiJunZiProgressLab = null;
t.BtnSiJunZi = null;
t.SiJunZiGetNumTipLab = null;
t.MingZhuProgress = null;
t.MingZhuProgressLab = null;
t.BtnMingZhu = null;
t.MingZhuGetNumTipLab = null;
t.ShenShouProgress = null;
t.ShenShouProgressLab = null;
t.BtnShenShou = null;
t.ShenShouGetNumTipLab = null;
t.BaDaJiaProgress = null;
t.BaDaJiaProgressLab = null;
t.BtnBaDaJia = null;
t.BaDaJiaGetNumTipLab = null;
t.JinChanProgress = null;
t.JinChanProgressLab = null;
t.JinChanGetNumTipLab = null;
t.PiXiuProgress = null;
t.PiXiuProgressLab = null;
t.BtnPiXiu = null;
t.PiXiuGetNumTipLab = null;
t.JinLongProgress = null;
t.JinLongProgressLab = null;
t.BtnJinLong = null;
t.JinLongGetNumTipLab = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "ContentNode", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "WenFangSiBaoProgress", void 0);
i([ l(cc.Label) ], t.prototype, "WenFangSiBaoProgressLab", void 0);
i([ l(s.default) ], t.prototype, "BtnWenFangSiBao", void 0);
i([ l(cc.Label) ], t.prototype, "WenFangSiBaoGetNumTipLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "SiJunZiProgress", void 0);
i([ l(cc.Label) ], t.prototype, "SiJunZiProgressLab", void 0);
i([ l(s.default) ], t.prototype, "BtnSiJunZi", void 0);
i([ l(cc.Label) ], t.prototype, "SiJunZiGetNumTipLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "MingZhuProgress", void 0);
i([ l(cc.Label) ], t.prototype, "MingZhuProgressLab", void 0);
i([ l(s.default) ], t.prototype, "BtnMingZhu", void 0);
i([ l(cc.Label) ], t.prototype, "MingZhuGetNumTipLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "ShenShouProgress", void 0);
i([ l(cc.Label) ], t.prototype, "ShenShouProgressLab", void 0);
i([ l(s.default) ], t.prototype, "BtnShenShou", void 0);
i([ l(cc.Label) ], t.prototype, "ShenShouGetNumTipLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "BaDaJiaProgress", void 0);
i([ l(cc.Label) ], t.prototype, "BaDaJiaProgressLab", void 0);
i([ l(s.default) ], t.prototype, "BtnBaDaJia", void 0);
i([ l(cc.Label) ], t.prototype, "BaDaJiaGetNumTipLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "JinChanProgress", void 0);
i([ l(cc.Label) ], t.prototype, "JinChanProgressLab", void 0);
i([ l(cc.Label) ], t.prototype, "JinChanGetNumTipLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "PiXiuProgress", void 0);
i([ l(cc.Label) ], t.prototype, "PiXiuProgressLab", void 0);
i([ l(s.default) ], t.prototype, "BtnPiXiu", void 0);
i([ l(cc.Label) ], t.prototype, "PiXiuGetNumTipLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "JinLongProgress", void 0);
i([ l(cc.Label) ], t.prototype, "JinLongProgressLab", void 0);
i([ l(s.default) ], t.prototype, "BtnJinLong", void 0);
i([ l(cc.Label) ], t.prototype, "JinLongGetNumTipLab", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UICollection: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "dda071AclpG/LHrHJaGISlC", "UICollection");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = e("../../Manager/GameMgr"), c = cc._decorator, l = c.ccclass, u = (c.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
this.view.WenFangSiBaoProgress.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.CollectionInfo.curWFSBProgress / 4;
this.view.WenFangSiBaoProgressLab.string = a.default.dataModalMgr.CollectionInfo.curWFSBProgress + "/4";
this.view.WenFangSiBaoGetNumTipLab.string = a.default.dataModalMgr.CollectionInfo.WFSBGetMoney.toString();
this.view.SiJunZiProgress.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.CollectionInfo.curSJZProgress / 4;
this.view.SiJunZiProgressLab.string = a.default.dataModalMgr.CollectionInfo.curSJZProgress + "/4";
this.view.SiJunZiGetNumTipLab.string = a.default.dataModalMgr.CollectionInfo.SJZGetMoney.toString();
this.view.MingZhuProgress.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.CollectionInfo.curSDMZProgress / 4;
this.view.MingZhuProgressLab.string = a.default.dataModalMgr.CollectionInfo.curSDMZProgress + "/4";
this.view.MingZhuGetNumTipLab.string = a.default.dataModalMgr.CollectionInfo.SDMZGetMoney.toString();
this.view.ShenShouProgress.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.CollectionInfo.curSDSSProgress / 4;
this.view.ShenShouProgressLab.string = a.default.dataModalMgr.CollectionInfo.curSDSSProgress + "/4";
this.view.ShenShouGetNumTipLab.string = a.default.dataModalMgr.CollectionInfo.SDSSGetMoney.toString();
this.view.BaDaJiaProgress.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.CollectionInfo.curBDJProgress / 8;
this.view.BaDaJiaProgressLab.string = a.default.dataModalMgr.CollectionInfo.curBDJProgress + "/8";
this.view.BaDaJiaGetNumTipLab.string = a.default.dataModalMgr.CollectionInfo.BDJGetMoney.toString();
this.view.JinChanProgress.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.CollectionInfo.curJCProgress / 1;
this.view.JinChanProgressLab.string = a.default.dataModalMgr.CollectionInfo.curJCProgress + "/1";
this.view.JinChanGetNumTipLab.string = a.default.dataModalMgr.CollectionInfo.JCDescribe;
this.view.PiXiuProgress.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.CollectionInfo.curPXProgress / 1;
this.view.PiXiuProgressLab.string = a.default.dataModalMgr.CollectionInfo.curPXProgress + "/1";
this.view.PiXiuGetNumTipLab.string = a.default.dataModalMgr.CollectionInfo.JCDescribe;
this.view.JinLongProgress.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.CollectionInfo.curJLProgress / 1;
this.view.JinLongProgressLab.string = a.default.dataModalMgr.CollectionInfo.curJLProgress + "/1";
this.view.JinLongGetNumTipLab.string = a.default.dataModalMgr.CollectionInfo.JLDescribe;
};
return i([ l ], t);
}(s.UIWindow));
n.default = u;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm",
"../../Manager/GameMgr": "GameMgr"
} ],
UIConfig: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "64643IPUkZDGb5aHw/lReOt", "UIConfig");
Object.defineProperty(n, "__esModule", {
value: !0
});
var r = function() {
function e() {}
e.UIHome = {
prefabUrl: "Forms/Screen/UIHome",
type: "UIScreen"
};
e.UILogin = {
prefabUrl: "Forms/Screen/UILogin",
type: "UIScreen"
};
e.UIStart = {
prefabUrl: "Forms/Screen/UIStart",
type: "UIScreen"
};
e.UILoading = {
prefabUrl: "Forms/Tips/UILoading",
type: "UITips"
};
e.UIBag = {
prefabUrl: "Forms/Windows/UIBag",
type: "UIWindow"
};
e.UIBagAdd = {
prefabUrl: "Forms/Windows/UIBagAdd",
type: "UIWindow"
};
e.UIBuyTip = {
prefabUrl: "Forms/Windows/UIBuyTip",
type: "UIWindow"
};
e.UICollection = {
prefabUrl: "Forms/Windows/UICollection",
type: "UIWindow"
};
e.UIDebris = {
prefabUrl: "Forms/Windows/UIDebris",
type: "UIWindow"
};
e.UIDialog = {
prefabUrl: "Forms/Windows/UIDialog",
type: "UIWindow"
};
e.UIFriend = {
prefabUrl: "Forms/Windows/UIFriend",
type: "UIWindow"
};
e.UIGetMoneyLog = {
prefabUrl: "Forms/Windows/UIGetMoneyLog",
type: "UIWindow"
};
e.UIGetMoneyRule = {
prefabUrl: "Forms/Windows/UIGetMoneyRule",
type: "UIWindow"
};
e.UILottery = {
prefabUrl: "Forms/Windows/UILottery",
type: "UIWindow"
};
e.UILotteryGetTip = {
prefabUrl: "Forms/Windows/UILotteryGetTip",
type: "UIWindow"
};
e.UIMy = {
prefabUrl: "Forms/Windows/UIMy",
type: "UIWindow"
};
e.UIMyFriend = {
prefabUrl: "Forms/Windows/UIMyFriend",
type: "UIWindow"
};
e.UIMyWallet = {
prefabUrl: "Forms/Windows/UIMyWallet",
type: "UIWindow"
};
e.UIOline = {
prefabUrl: "Forms/Windows/UIOline",
type: "UIWindow"
};
e.UIRank = {
prefabUrl: "Forms/Windows/UIRank",
type: "UIWindow"
};
e.UIRecoverTip = {
prefabUrl: "Forms/Windows/UIRecoverTip",
type: "UIWindow"
};
e.UIRotateAndFlip = {
prefabUrl: "Forms/Windows/UIRotateAndFlip",
type: "UIWindow"
};
e.UIScoketReconnent = {
prefabUrl: "Forms/Windows/UIScoketReconnent",
type: "UIWindow"
};
e.UISetting = {
prefabUrl: "Forms/Windows/UISetting",
type: "UIWindow"
};
e.UIShare = {
prefabUrl: "Forms/Windows/UIShare",
type: "UIWindow"
};
e.UIShiMing = {
prefabUrl: "Forms/Windows/UIShiMing",
type: "UIWindow"
};
e.UIShop = {
prefabUrl: "Forms/Windows/UIShop",
type: "UIWindow"
};
e.UITask = {
prefabUrl: "Forms/Windows/UITask",
type: "UIWindow"
};
e.UIUnlockNewLevel = {
prefabUrl: "Forms/Windows/UIUnlockNewLevel",
type: "UIWindow"
};
e.UIcolleCtionConversion = {
prefabUrl: "Forms/Windows/UIcolleCtionConversion",
type: "UIWindow"
};
e.UIFriendMonerDetail = {
prefabUrl: "Forms/Windows/UIFriendMonerDetail",
type: "UIWindow"
};
e.UIMyExtar = {
prefabUrl: "Forms/Windows/UIMyExtar",
type: "UIWindow"
};
e.UIInviteGetInfo = {
prefabUrl: "Forms/Windows/UIInviteGetInfo",
type: "UIWindow"
};
e.UIExtarCommitSure = {
prefabUrl: "Forms/Windows/UIExtarCommitSure",
type: "UIWindow"
};
e.UISetSocial = {
prefabUrl: "Forms/Windows/UISetSocial",
type: "UIWindow"
};
e.UIColleCtionConversion = {
prefabUrl: "Forms/Windows/UIColleCtionConversion",
type: "UIWindow"
};
e.UISignIn = {
prefabUrl: "Forms/Windows/UISignIn",
type: "UIWindow"
};
e.UIFriendSee = {
prefabUrl: "Forms/Windows/UIFriendSee",
type: "UIWindow"
};
e.UIFriendSeeDetail = {
prefabUrl: "Forms/Windows/UIFriendSeeDetail",
type: "UIWindow"
};
e.UIFriendGiveDetail = {
prefabUrl: "Forms/Windows/UIFriendGiveDetail",
type: "UIWindow"
};
e.UIFriendSerch = {
prefabUrl: "Forms/Windows/UIFriendSerch",
type: "UIWindow"
};
e.UIFriendGive = {
prefabUrl: "Forms/Windows/UIFriendGive",
type: "UIWindow"
};
return e;
}();
n.default = r;
cc._RF.pop();
}, {} ],
UIDebrisGetTip_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "ffa8fnRY3BCJ6vJIic+/8Mf", "UIDebrisGetTip_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.DebrisLab = null;
t.DebrisIconSp = null;
t.BtnClose = null;
t.BtnGoToBag = null;
return t;
}
i([ l(cc.Label) ], t.prototype, "DebrisLab", void 0);
i([ l(cc.Sprite) ], t.prototype, "DebrisIconSp", void 0);
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(s.default) ], t.prototype, "BtnGoToBag", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIDebrisGetTip: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "c222d0xWwBGjJw1RuA1daLb", "UIDebrisGetTip");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIDialog_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "7bda58G4wJP+qv/EiR/HvLf", "UIDialog_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.Msg = null;
t.Sure = null;
return t;
}
i([ l(cc.Label) ], t.prototype, "Msg", void 0);
i([ l(s.default) ], t.prototype, "Sure", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIDialog: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "fb4a5ZfY11CILd1ph9+IuFV", "UIDialog");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../Common/Struct"), a = e("../Common/SysDefine"), c = e("../Common/UIForm"), l = cc._decorator, u = l.ccclass, p = (l.property, 
function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.modalType = new s.ModalType(a.ModalOpacity.OpacityHalf, !0);
return t;
}
t.prototype.onLoad = function() {
var e = this;
this.view.Sure.addClick(function() {
e.closeSelf();
}, this);
};
t.prototype.onShow = function(e) {
this.view.Msg.string = e;
};
return i([ u ], t);
}(c.UIWindow));
n.default = p;
cc._RF.pop();
}, {
"../Common/Struct": "Struct",
"../Common/SysDefine": "SysDefine",
"../Common/UIForm": "UIForm"
} ],
UIExtarCommitSure_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a2d70xxX35MYYwYU0ijVVhj", "UIExtarCommitSure_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.CloseBtn = null;
return t;
}
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIExtarCommitSure: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "927f52xj15PP4+Wi75Paakd", "UIExtarCommitSure");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIForm: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "fecebM0tgRBP4NNiSEz6NLd", "UIForm");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, s = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
n.UITips = n.UIFixed = n.UIWindow = n.UIScreen = void 0;
var a = e("../Manager/FormMgr"), c = e("../Utils/CocosHelper"), l = e("./Struct"), u = e("./SysDefine"), p = e("./UIBase"), f = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.formType = u.FormType.Screen;
t.willDestory = !0;
return t;
}
t.prototype.closeSelf = function() {
return i(this, void 0, Promise, function() {
return s(this, function(e) {
switch (e.label) {
case 0:
return [ 4, a.default.close({
prefabUrl: this.fid,
type: this.formType
}) ];

case 1:
return [ 2, e.sent() ];
}
});
});
};
return t;
}(p.default);
n.UIScreen = f;
var d = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.formType = u.FormType.Window;
t.modalType = new l.ModalType();
t.willDestory = !0;
return t;
}
t.prototype.showEffect = function() {
return i(this, void 0, void 0, function() {
return s(this, function(e) {
switch (e.label) {
case 0:
this.node.scale = 0;
return [ 4, c.default.runTweenSync(this.node, cc.tween().to(.3, {
scale: 1
}, cc.easeBackOut())) ];

case 1:
e.sent();
return [ 2 ];
}
});
});
};
t.prototype.closeSelf = function() {
return i(this, void 0, Promise, function() {
return s(this, function(e) {
switch (e.label) {
case 0:
return [ 4, a.default.close({
prefabUrl: this.fid,
type: this.formType
}) ];

case 1:
return [ 2, e.sent() ];
}
});
});
};
return t;
}(p.default);
n.UIWindow = d;
var h = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.formType = u.FormType.Fixed;
t.willDestory = !0;
return t;
}
t.prototype.closeSelf = function() {
return i(this, void 0, Promise, function() {
return s(this, function(e) {
switch (e.label) {
case 0:
return [ 4, a.default.close({
prefabUrl: this.fid,
type: this.formType
}) ];

case 1:
return [ 2, e.sent() ];
}
});
});
};
return t;
}(p.default);
n.UIFixed = h;
var y = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.formType = u.FormType.Tips;
t.willDestory = !0;
return t;
}
t.prototype.closeSelf = function() {
return i(this, void 0, Promise, function() {
return s(this, function(e) {
switch (e.label) {
case 0:
return [ 4, a.default.close({
prefabUrl: this.fid,
type: this.formType
}) ];

case 1:
return [ 2, e.sent() ];
}
});
});
};
return t;
}(p.default);
n.UITips = y;
cc.UIScreen = f;
cc.UIWindow = d;
cc.UIFixed = h;
cc.UITips = y;
cc._RF.pop();
}, {
"../Manager/FormMgr": "FormMgr",
"../Utils/CocosHelper": "CocosHelper",
"./Struct": "Struct",
"./SysDefine": "SysDefine",
"./UIBase": "UIBase"
} ],
UIFriendGiveDetail_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "dc325s42LBKQKBBHyLjUiRp", "UIFriendGiveDetail_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.FriendSeeContentNode = null;
t.CoinContentViewNode = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "FriendSeeContentNode", void 0);
i([ l(cc.Node) ], t.prototype, "CoinContentViewNode", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIFriendGiveDetail: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "50afdmJCaRGY5OypTZiuNxm", "UIFriendGiveDetail");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIFriendGive_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "77825T9BPFOe7LJYPu+lNrY", "UIFriendGive_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnClose = null;
t.BtnSerch = null;
t.BtnGive = null;
t.IDBox = null;
t.GiveBox = null;
t.NameLab = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(s.default) ], t.prototype, "BtnSerch", void 0);
i([ l(s.default) ], t.prototype, "BtnGive", void 0);
i([ l(cc.EditBox) ], t.prototype, "IDBox", void 0);
i([ l(cc.EditBox) ], t.prototype, "GiveBox", void 0);
i([ l(cc.Label) ], t.prototype, "NameLab", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIFriendGive: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a291c4siCBG+71COhJFJCcd", "UIFriendGive");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIFriendMonerDetail_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "dbcbbCGpTpNgrFvVqCfFAK2", "UIFriendMonerDetail_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnClose = null;
t.TodayGetMoneyLab = null;
t.TotalGetMoneyLab = null;
t.ContentNode = null;
t.ContentViewNode = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(cc.Label) ], t.prototype, "TodayGetMoneyLab", void 0);
i([ l(cc.Label) ], t.prototype, "TotalGetMoneyLab", void 0);
i([ l(cc.Node) ], t.prototype, "ContentNode", void 0);
i([ l(cc.Node) ], t.prototype, "ContentViewNode", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIFriendMonerDetail: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "66ccfKnIDNFMbBuB+Ousnab", "UIFriendMonerDetail");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ListUtil"), a = e("../../Common/UIForm"), c = e("../../Manager/GameMgr"), l = e("./ItemFriendExtraDetail"), u = cc._decorator, p = u.ccclass, f = u.property, d = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.listFriendMonerDetail = null;
return t;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
this.listFriendMonerDetail.numItems = c.default.dataModalMgr.FriendMonerDetailInfo.list_item_friend_money_detail.length;
};
t.prototype.onListShopRender = function(e, t) {
e.getComponent(l.default).setData(c.default.dataModalMgr.FriendMonerDetailInfo.list_item_friend_money_detail[t]);
};
i([ f(s.default) ], t.prototype, "listFriendMonerDetail", void 0);
return i([ p ], t);
}(a.UIWindow);
n.default = d;
cc._RF.pop();
}, {
"../../Common/Components/ListUtil": "ListUtil",
"../../Common/UIForm": "UIForm",
"../../Manager/GameMgr": "GameMgr",
"./ItemFriendExtraDetail": "ItemFriendExtraDetail"
} ],
UIFriendSeeDetail_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "dc76a7WSKVJ/IIyQS+lc9B5", "UIFriendSeeDetail_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.FriendSeeContentNode = null;
t.CoinContentViewNode = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "FriendSeeContentNode", void 0);
i([ l(cc.Node) ], t.prototype, "CoinContentViewNode", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIFriendSeeDetail: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "9f8fcAhCUtAq5OTeNZjyvJQ", "UIFriendSeeDetail");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ListUtil"), a = e("../../Common/UIForm"), c = cc._decorator, l = c.ccclass, u = c.property, p = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.list_see_detail = null;
return t;
}
t.prototype.onLoad = function() {};
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
t.prototype.onListRankRender = function() {};
i([ u(s.default) ], t.prototype, "list_see_detail", void 0);
return i([ l ], t);
}(a.UIWindow);
n.default = p;
cc._RF.pop();
}, {
"../../Common/Components/ListUtil": "ListUtil",
"../../Common/UIForm": "UIForm"
} ],
UIFriendSee_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "e84e9e0KjZLxacECA/SKApq", "UIFriendSee_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnClose = null;
t.AddFriendBtn = null;
t.ContentNode = null;
t.ContentViewNode = null;
t.SeeDetailBtn = null;
t.InviteBtn = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(s.default) ], t.prototype, "AddFriendBtn", void 0);
i([ l(cc.Node) ], t.prototype, "ContentNode", void 0);
i([ l(cc.Node) ], t.prototype, "ContentViewNode", void 0);
i([ l(s.default) ], t.prototype, "SeeDetailBtn", void 0);
i([ l(s.default) ], t.prototype, "InviteBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIFriendSee: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "051619VVkpPCZo5puArAdJa", "UIFriendSee");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = e("../../Manager/FormMgr"), c = e("../../UIConfig"), l = cc._decorator, u = l.ccclass, p = (l.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
this.view.InviteBtn.addClick(function() {
a.default.open(c.default.UIShare);
}, this);
this.view.AddFriendBtn.addClick(function() {
a.default.open(c.default.UIFriendSerch);
}, this);
this.view.SeeDetailBtn.addClick(function() {
a.default.open(c.default.UIFriendSeeDetail);
}, this);
};
return i([ u ], t);
}(s.UIWindow));
n.default = p;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm",
"../../Manager/FormMgr": "FormMgr",
"../../UIConfig": "UIConfig"
} ],
UIFriendSerch_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "84f1aytnrREUIxrLr2UYwFA", "UIFriendSerch_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnSure = null;
t.BtnClose = null;
t.BtnSerch = null;
t.IDBox = null;
t.NameLab = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnSure", void 0);
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(s.default) ], t.prototype, "BtnSerch", void 0);
i([ l(cc.EditBox) ], t.prototype, "IDBox", void 0);
i([ l(cc.Label) ], t.prototype, "NameLab", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIFriendSerch: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "6cdffqj5/xI9pXx3nVKdJCs", "UIFriendSerch");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIFriend_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "1e1abFX9YpOM4gB7QJl3WyR", "UIFriend_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnClose = null;
t.BtnInvoedFriend = null;
t.BtnMyFriend = null;
t.MyFriendNumLab = null;
t.ProbablyMoneyLab = null;
t.ProbablyDetail = null;
t.TodayGetMoneyLab = null;
t.TotalGetMoneyLab = null;
t.UpChannel = null;
t.ExtarTip = null;
t.TodayExtarGetMoneyLab = null;
t.TodayExtarGetMoneyTipsLab = null;
t.Call = null;
t.Name = null;
t.HeadSp = null;
t.TodayAddYushi = null;
t.TotalYushi = null;
t.BtnGive = null;
t.BtnVisit = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(s.default) ], t.prototype, "BtnInvoedFriend", void 0);
i([ l(s.default) ], t.prototype, "BtnMyFriend", void 0);
i([ l(cc.Label) ], t.prototype, "MyFriendNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "ProbablyMoneyLab", void 0);
i([ l(s.default) ], t.prototype, "ProbablyDetail", void 0);
i([ l(cc.Label) ], t.prototype, "TodayGetMoneyLab", void 0);
i([ l(cc.Label) ], t.prototype, "TotalGetMoneyLab", void 0);
i([ l(s.default) ], t.prototype, "UpChannel", void 0);
i([ l(s.default) ], t.prototype, "ExtarTip", void 0);
i([ l(cc.Label) ], t.prototype, "TodayExtarGetMoneyLab", void 0);
i([ l(cc.Label) ], t.prototype, "TodayExtarGetMoneyTipsLab", void 0);
i([ l(s.default) ], t.prototype, "Call", void 0);
i([ l(cc.Label) ], t.prototype, "Name", void 0);
i([ l(cc.Sprite) ], t.prototype, "HeadSp", void 0);
i([ l(cc.Label) ], t.prototype, "TodayAddYushi", void 0);
i([ l(cc.Label) ], t.prototype, "TotalYushi", void 0);
i([ l(s.default) ], t.prototype, "BtnGive", void 0);
i([ l(s.default) ], t.prototype, "BtnVisit", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIFriend: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "12f854yj6NDEIDfuIsgedN/", "UIFriend");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = e("../../Manager/FormMgr"), c = e("../../Manager/GameMgr"), l = e("../../UIConfig"), u = e("../../Utils/CocosHelper"), p = cc._decorator, f = p.ccclass, d = (p.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnInvoedFriend.addClick(function() {
a.default.open(l.default.UIShare);
}, this);
this.view.BtnMyFriend.addClick(function() {
a.default.open(l.default.UIMyFriend);
}, this);
this.view.ProbablyDetail.addClick(function() {
a.default.open(l.default.UIFriendMonerDetail);
}, this);
this.view.UpChannel.addClick(function() {
a.default.open(l.default.UIExtarCommitSure);
}, this);
this.view.Call.addClick(function() {
a.default.open(l.default.UIInviteGetInfo);
}, this);
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
this.view.BtnGive.addClick(function() {
a.default.open(l.default.UIFriendGive);
}, this);
this.view.BtnVisit.addClick(function() {
a.default.open(l.default.UIFriendSee);
}, this);
this.view.MyFriendNumLab.string = "我的好友：" + c.default.dataModalMgr.FriendInfo.my_friend_num + "人";
this.view.ProbablyMoneyLab.string = "预计每个好友每天为你产出" + c.default.dataModalMgr.FriendInfo.probabyly_money + "元";
this.view.TodayGetMoneyLab.string = c.default.dataModalMgr.FriendInfo.today_get_money + "元";
this.view.TotalGetMoneyLab.string = c.default.dataModalMgr.FriendInfo.total_get_money + "元";
this.view.TodayExtarGetMoneyLab.string = c.default.dataModalMgr.FriendInfo.today_extar_get_money + "元";
this.view.TodayExtarGetMoneyTipsLab.string = "累计获得8000元后成为渠道，当前已获得" + c.default.dataModalMgr.FriendInfo.cur_get_money + "元";
this.view.Name.string = c.default.dataModalMgr.FriendInfo.my_inviter_name;
u.default.loadHead(c.default.dataModalMgr.FriendInfo.my_invite_head, this.view.HeadSp);
};
return i([ f ], t);
}(s.UIWindow));
n.default = d;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm",
"../../Manager/FormMgr": "FormMgr",
"../../Manager/GameMgr": "GameMgr",
"../../UIConfig": "UIConfig",
"../../Utils/CocosHelper": "CocosHelper"
} ],
UIGaide: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "4d9093UAbJHWYQvaRKxoPg1", "UIGaide");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../Net/EventCenter"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.maskNode = null;
return t;
}
t.prototype.start = function() {
this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBlockEvent, this);
this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchBlockEvent, this);
this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchBlockEvent, this);
s.EventCenter.on("shouguide", this.Show, this);
this.node.active = !1;
};
t.prototype.Show = function(e) {
var t = e.getContentSize();
this.maskNode.width = t.width + 20;
this.maskNode.height = t.height + 20;
var n = e.parent.convertToWorldSpaceAR(e.position), r = this.maskNode.parent.convertToNodeSpaceAR(n);
this.maskNode.setPosition(cc.v2(r.x, r.y));
this.node.active = !0;
};
t.prototype.onTouchBlockEvent = function(e) {
var t = this.maskNode.convertToNodeSpaceAR(e.getLocation());
if (cc.rect(0, 0, this.maskNode.width, this.maskNode.height).contains(t)) {
console.log("命中");
this.node._touchListener.setSwallowTouches(!1);
this.node.active = !1;
} else {
console.log("没有命中");
this.node._touchListener.setSwallowTouches(!0);
e.stopPropagationImmediate();
}
};
i([ l(cc.Node) ], t.prototype, "maskNode", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"../Net/EventCenter": "EventCenter"
} ],
UIGetMoneyLog_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "0f042WW0LpDWo2MKJBqkkTX", "UIGetMoneyLog_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.ContentViewNode = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "ContentViewNode", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIGetMoneyLog: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "60a43zuBFtAkJxyXpgR2jX+", "UIGetMoneyLog");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ListUtil"), a = e("../../Common/UIForm"), c = e("../../Manager/GameMgr"), l = e("./ItemGetMoneyLog"), u = cc._decorator, p = u.ccclass, f = u.property, d = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.list_log = null;
return t;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
t.prototype.onListShopRender = function(e, t) {
e.getComponent(l.default).setData(c.default.dataModalMgr.DataGetMoneyLogInfo.list_get_money_log[t]);
};
i([ f(s.default) ], t.prototype, "list_log", void 0);
return i([ p ], t);
}(a.UIWindow);
n.default = d;
cc._RF.pop();
}, {
"../../Common/Components/ListUtil": "ListUtil",
"../../Common/UIForm": "UIForm",
"../../Manager/GameMgr": "GameMgr",
"./ItemGetMoneyLog": "ItemGetMoneyLog"
} ],
UIGetMoneyRule_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "396c3YfNrpCOrGmr4F+qpBb", "UIGetMoneyRule_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.Tip = null;
t.Sure = null;
return t;
}
i([ l(cc.Label) ], t.prototype, "Tip", void 0);
i([ l(s.default) ], t.prototype, "Sure", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIGetMoneyRule: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "5775ehe5GpGQp1xAYostDI7", "UIGetMoneyRule");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.Sure.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIHome_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "eab67NVkkJG2pd3DYRReg9U", "UIHome_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.accEffect = null;
t.Accelerate = null;
t.RecoveryBtn = null;
t.topNode = null;
t.MakeMoneyTxt = null;
t.CoinTxt = null;
t.DiamondTxt = null;
t.MoneyTxt = null;
t.HeadSp = null;
t.RankBtn = null;
t.CollectBtn = null;
t.BagBtn = null;
t.ShopBtn = null;
t.DrawBtn = null;
t.WorkContent = null;
t.FriendBtn = null;
t.SignInBtn = null;
t.BuyBtn = null;
t.BtnCakeSp = null;
t.BuyCoinNumTxt = null;
t.MyBtn = null;
t.TaskBtn = null;
t.CombineAutoBtn = null;
t.CombineAutoTxt = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "accEffect", void 0);
i([ l(s.default) ], t.prototype, "Accelerate", void 0);
i([ l(s.default) ], t.prototype, "RecoveryBtn", void 0);
i([ l(cc.Node) ], t.prototype, "topNode", void 0);
i([ l(cc.Label) ], t.prototype, "MakeMoneyTxt", void 0);
i([ l(cc.Label) ], t.prototype, "CoinTxt", void 0);
i([ l(cc.Label) ], t.prototype, "DiamondTxt", void 0);
i([ l(cc.Label) ], t.prototype, "MoneyTxt", void 0);
i([ l(cc.Sprite) ], t.prototype, "HeadSp", void 0);
i([ l(s.default) ], t.prototype, "RankBtn", void 0);
i([ l(s.default) ], t.prototype, "CollectBtn", void 0);
i([ l(s.default) ], t.prototype, "BagBtn", void 0);
i([ l(s.default) ], t.prototype, "ShopBtn", void 0);
i([ l(s.default) ], t.prototype, "DrawBtn", void 0);
i([ l(cc.Node) ], t.prototype, "WorkContent", void 0);
i([ l(s.default) ], t.prototype, "FriendBtn", void 0);
i([ l(s.default) ], t.prototype, "SignInBtn", void 0);
i([ l(s.default) ], t.prototype, "BuyBtn", void 0);
i([ l(cc.Sprite) ], t.prototype, "BtnCakeSp", void 0);
i([ l(cc.Label) ], t.prototype, "BuyCoinNumTxt", void 0);
i([ l(s.default) ], t.prototype, "MyBtn", void 0);
i([ l(s.default) ], t.prototype, "TaskBtn", void 0);
i([ l(s.default) ], t.prototype, "CombineAutoBtn", void 0);
i([ l(cc.Label) ], t.prototype, "CombineAutoTxt", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIHome: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "ce700EVbt5MTrg0Llyxj9zb", "UIHome");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../../../Protocol/bundle"), l = e("../../Common/UIForm"), u = e("../../Manager/FormMgr"), p = e("../../Manager/GameMgr"), f = e("../../Manager/SoundMgr"), d = e("../../UIConfig"), h = e("../../Utils/CocosHelper"), y = e("../UIToast"), v = cc._decorator, g = v.ccclass, _ = v.property, m = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.itemProducePrefab = null;
t._isCombining = !1;
t._isCanDrag = !1;
t._currentNode = null;
t._virtualNode = null;
return t;
}
t.prototype.load = function() {
return s(this, void 0, void 0, function() {
return a(this, function() {
f.default.inst.playMusic("Sounds/bg");
return [ 2, null ];
});
});
};
t.prototype.start = function() {
this.item_pos_arr = [ new cc.Vec2(0, 360), new cc.Vec2(-175, 270), new cc.Vec2(175, 270), new cc.Vec2(0, 180), new cc.Vec2(-175, 90), new cc.Vec2(175, 90), new cc.Vec2(0, 0), new cc.Vec2(-175, -90), new cc.Vec2(175, -90), new cc.Vec2(0, -180), new cc.Vec2(-175, -270), new cc.Vec2(175, -270) ];
this.initEvent();
this.initData();
this.initClickEvent();
this.initWorkbench();
};
t.prototype.adaptiveNoteLayout = function() {
var e = cc.winSize;
cc.log("--当前游戏窗口大小  w:" + e.width + "   h:" + e.height);
var t = cc.view.getFrameSize();
cc.log("--视图边框尺寸：w:" + t.width + "  h:" + t.height);
var n = cc.view.getCanvasSize();
cc.log("--视图中canvas尺寸  w:" + n.width + "  H:" + n.height);
var r = cc.view.getVisibleSize();
cc.log("--视图中窗口可见区域的尺寸 w:" + r.width + "   h:" + r.height);
var o = cc.view.getDesignResolutionSize();
cc.log("--设计分辨率：" + o.width + "    h: " + o.height);
cc.log("--当前节点的尺寸 w:" + this.node.width + "   h:" + this.node.height);
};
t.prototype.initEvent = function() {
this.view.WorkContent.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
this.view.WorkContent.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
this.view.WorkContent.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
this.view.WorkContent.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
};
t.prototype.initClickEvent = function() {
this.view.BuyBtn.addClick(this.onBuyProduceClick, this);
this.view.CombineAutoBtn.addClick(this.onBtnCombineAutoClick, this);
this.view.BagBtn.addClick(function() {
u.default.open(d.default.UIBag);
}, this);
this.view.SignInBtn.addClick(function() {
u.default.open(d.default.UISignIn);
}, this);
this.view.DrawBtn.addClick(function() {
u.default.open(d.default.UILottery);
}, this);
this.view.RankBtn.addClick(function() {
u.default.open(d.default.UIRank);
}, this);
this.view.CollectBtn.addClick(function() {
u.default.open(d.default.UICollection);
}, this);
this.view.ShopBtn.addClick(function() {
u.default.open(d.default.UIShop);
}, this);
this.view.MyBtn.addClick(function() {
u.default.open(d.default.UIMy);
}, this);
this.view.TaskBtn.addClick(function() {
u.default.open(d.default.UITask);
}, this);
this.view.FriendBtn.addClick(function() {
u.default.open(d.default.UIFriend);
}, this);
};
t.prototype.initData = function() {
h.default.loadHead(p.default.dataModalMgr.UserInfo.userAvatarUrl, this.view.HeadSp);
p.default.dataModalMgr.UserInfo.userWorkbench = [ 1, 1, 2, 3, 4, 4, 5, 5, 6, 7 ];
this.view.BuyCoinNumTxt.string = p.default.dataModalMgr.UserInfo.userCurNeedBuyCoinNum.toString();
this.view.MakeMoneyTxt.string = p.default.dataModalMgr.UserInfo.userMakeCoin.toString();
this.view.CoinTxt.string = p.default.dataModalMgr.UserInfo.userCoin.toString();
this.view.DiamondTxt.string = p.default.dataModalMgr.UserInfo.userDiamond.toString();
this.view.MoneyTxt.string = p.default.dataModalMgr.UserInfo.userMoneyUints + "." + p.default.dataModalMgr.UserInfo.userMoneyNanos;
};
t.prototype.initWorkbench = function() {
if (!(this.view.WorkContent.children.length > 0)) {
for (var e = 0; e < p.default.dataModalMgr.UserInfo.userWorkbench.length; e++) {
var t = cc.instantiate(this.itemProducePrefab);
this.view.WorkContent.addChild(t);
t.setPosition(this.item_pos_arr[e]);
}
this.view.WorkContent.children.forEach(function(e, t) {
var n = e.getComponent("ItemProduce");
t < p.default.dataModalMgr.UserInfo.userWorkbench.length && n.setWorkbenchItemInfo(t, p.default.dataModalMgr.UserInfo.userWorkbench[t]);
}, this);
}
};
t.prototype.onTouchStart = function(e) {
this._currentNode = this.getCurrentNodeByTouchPos(e.getLocation());
if (this._currentNode) {
var t = this._currentNode.getComponent("ItemProduce");
this._isCanDrag = t.dragStart();
if (this._isCanDrag) {
var n = this._currentNode.convertToWorldSpaceAR(cc.v2(0, 0));
this._offsetPos = n.sub(e.getLocation());
this.showVirtualItem(t.getInfo(), e.getLocation());
this.showProduce(t);
}
}
};
t.prototype.onTouchMove = function(e) {
this._isCanDrag && this.updateVirutalItemPos(e.getLocation());
};
t.prototype.onTouchEnd = function(e) {
if (this._currentNode) {
this.hideProduce();
var t = this._currentNode.getComponent("ItemProduce");
t.dragOver();
var n = this.getCurrentNodeByTouchPos(e.getLocation());
if (this._currentNode !== n) {
if (this._isCanDrag) {
n ? this.combinOrSwap(this._currentNode, n) : this.checkIsDragToRecovery(t._index, e.getLocation()) ? this.hideVirtualItem(!1) : this.hideVirtualItem(!0);
this._currentNode = null;
this._isCanDrag = !1;
}
} else {
this.hideVirtualItem(!0);
this._currentNode = null;
}
}
};
t.prototype.onTouchCancel = function(e) {
if (this._currentNode) {
var t = this._currentNode.getComponent("ItemProduce");
if (t._itemId) {
this.hideProduce();
t.dragOver();
if (t.isUsed) this._currentNode = null; else {
this.checkIsDragToRecovery(t._index, e.getLocation()) ? this.hideVirtualItem(!1) : this.hideVirtualItem(!0);
this._currentNode = null;
}
}
}
};
t.prototype.getCurrentNodeByTouchPos = function(e) {
return this.view.WorkContent.children.find(function(t) {
if (t.getBoundingBoxToWorld().contains(e)) return !0;
}, this);
};
t.prototype.showVirtualItem = function(e, t) {
if (this._virtualNode) this._virtualNode.active = !0; else {
this._virtualNode = cc.instantiate(this.itemProducePrefab);
this._virtualNode.parent = this.node.parent;
}
this._virtualNode.stopActionByTag(10001);
var n = this._virtualNode.getComponent("ItemProduce");
n.setWorkbenchItemInfo(-1, e);
n.showVirtual();
this.updateVirutalItemPos(t);
};
t.prototype.hideVirtualItem = function(e) {
var t = this;
if (this._virtualNode && this._virtualNode.active) if (e) {
var n = this._currentNode.convertToWorldSpaceAR(cc.v2), r = this.node.parent.convertToNodeSpaceAR(n).sub(this._virtualNode.position).mag() / 2e3;
h.default.runTweenSync(this._virtualNode, cc.tween().to(0, r).call(function() {
t._virtualNode.active = !1;
}));
} else this._virtualNode.active = !1;
};
t.prototype.updateVirutalItemPos = function(e) {
if (this._virtualNode && this._virtualNode.active) {
var t = this.node.parent.convertToNodeSpaceAR(e);
this._virtualNode.position = t.add(this._offsetPos);
this.updateDragPos(e);
}
};
t.prototype.updateDragPos = function(e) {
if (!this.view.RecoveryBtn.node.getBoundingBoxToWorld().contains(e)) {
1.1 === this.view.RecoveryBtn.node.scale && (this.view.RecoveryBtn.node.scale = 1);
return !1;
}
1 === this.view.RecoveryBtn.node.scale && (this.view.RecoveryBtn.node.scale = 1.1);
};
t.prototype.checkIsDragToRecovery = function(e, t) {
if (!this.view.RecoveryBtn.node.getBoundingBoxToWorld().contains(t)) return !1;
this.updateWorkbench(e, -1, "Recovery");
return !0;
};
t.prototype.showProduce = function(e) {
for (var t = e._index, n = (e._itemId, this.view.WorkContent.children), r = 0; r < n.length; r++) if (r !== t) {
var o = n[r].getComponent("ItemProduce");
!o.isUsed && o.itemId;
}
};
t.prototype.hideProduce = function() {
for (var e = this.view.WorkContent.children, t = 0; t < e.length; t++) e[t].getComponent("ItemProduce").isUsed;
};
t.prototype.combinOrSwap = function(e, t) {
return s(this, void 0, void 0, function() {
var n, r, o, i, s;
return a(this, function(a) {
switch (a.label) {
case 0:
n = e.getComponent("ItemProduce");
r = t.getComponent("ItemProduce");
if (n.isUsed || r.isUsed) {
this.hideVirtualItem(!0);
return [ 2 ];
}
o = p.default.dataModalMgr.UserInfo.userWorkbench[n._index];
i = p.default.dataModalMgr.UserInfo.userWorkbench[r._index];
return o && i ? n._itemId !== r._itemId ? [ 3, 2 ] : [ 4, p.default.clientChannel.required(c.GameServer).combineProduce({
positionBase: n._index,
positionTarget: r._index
}) ] : [ 2, !1 ];

case 1:
0 === (s = a.sent()).errcode && this.combinProduce(n._index, s.positionNew, s.produceLevel);
return [ 3, 3 ];

case 2:
this.swapProduce(n._index, r._index) && this.updateWorkbench(n._index, r._index, "Exchange");
a.label = 3;

case 3:
this.hideVirtualItem(!1);
return [ 2 ];
}
});
});
};
t.prototype.updateWorkbench = function(e, t, n) {
if (n) {
var r, o;
e >= 0 && (r = this.view.WorkContent.children[e].getComponent("ItemProduce"));
t >= 0 && (o = this.view.WorkContent.children[t].getComponent("ItemProduce"));
switch (n) {
case "Exchange":
console.log("交换");
o.setWorkbenchItemInfo(o._index, p.default.dataModalMgr.UserInfo.userWorkbench[o._index]);
r.setWorkbenchItemInfo(r._index, p.default.dataModalMgr.UserInfo.userWorkbench[r._index]);
break;

case "Combine":
console.log("组合");
r.setWorkbenchItemInfo(r._index, p.default.dataModalMgr.UserInfo.userWorkbench[r._index]);
o.setWorkbenchItemInfo(o._index, p.default.dataModalMgr.UserInfo.userWorkbench[o._index]);
break;

case "Produce":
console.log("生产");
o.setWorkbenchItemInfo(o._index, p.default.dataModalMgr.UserInfo.userWorkbench[o._index]);
break;

case "Recovery":
console.log("回收");
r.setWorkbenchItemInfo(r._index, 0);
p.default.dataModalMgr.UserInfo.userWorkbench[r._index] = 0;
}
} else this.initWorkbench();
};
t.prototype.swapProduce = function(e, t) {
if (!p.default.dataModalMgr.UserInfo.userWorkbench) return !1;
if (e >= p.default.dataModalMgr.UserInfo.userWorkbench.length || t >= p.default.dataModalMgr.UserInfo.userWorkbench.length) return !1;
var n = p.default.dataModalMgr.UserInfo.userWorkbench[e], r = p.default.dataModalMgr.UserInfo.userWorkbench[t];
p.default.dataModalMgr.UserInfo.userWorkbench[t] = n;
p.default.dataModalMgr.UserInfo.userWorkbench[e] = r;
return !0;
};
t.prototype.combinProduce = function(e, t, n) {
var r = this.view.WorkContent.children[e], o = this.view.WorkContent.children[t], i = r.getComponent("ItemProduce"), s = o.getComponent("ItemProduce");
i.isUsed = !0;
s.isUsed = !0;
p.default.dataModalMgr.UserInfo.userWorkbench[e] = 0;
p.default.dataModalMgr.UserInfo.userWorkbench[t] = n;
this.updateWorkbench(i._index, s._index, "Combine");
};
t.prototype.onBuyProduceClick = function() {
return s(this, void 0, void 0, function() {
var e, t;
return a(this, function(n) {
switch (n.label) {
case 0:
if (!(p.default.dataModalMgr.UserInfo.userCoin >= p.default.dataModalMgr.CurBuyProduceInfo.buyPrice)) {
y.default.popUp("金币不足！");
return [ 2 ];
}
if (!p.default.dataModalMgr.UserInfo.hasPosAtWorkbench()) {
y.default.popUp("请先出售多余物品！");
return [ 2 ];
}
return [ 4, p.default.clientChannel.required(c.GameServer).buyProduce({}) ];

case 1:
if (0 == (e = n.sent()).errcode) {
t = p.default.dataModalMgr.UserInfo.addCakeToWorkbench(e.position);
p.default.dataModalMgr.UserInfo.userWorkbench[t] = e.produceLevel;
this.updateWorkbench(-1, t, "Produce");
}
return [ 2 ];
}
});
});
};
t.prototype.onBtnCombineAutoClick = function() {
if (this._isCombining) y.default.popUp("正在自动合成中"); else {
var e = p.default.dataModalMgr.UserInfo.combineAutoTime;
!e || e <= 0 ? this.scheduleCombineOver() : this._isCombining || this.checkIsCombineAuto();
}
};
t.prototype.checkIsCombineAuto = function() {
var e = p.default.dataModalMgr.UserInfo.combineAutoTime;
if (!e || e <= 0) this.scheduleCombineOver(); else {
this._isCombining = !0;
this.schedule(this.combineAuto, 1);
this.view.CombineAutoTxt.string = h.default.formatTimeForSecond(15);
this.view.CombineAutoTxt.node.active = !0;
}
};
t.prototype.combineAuto = function() {
p.default.dataModalMgr.UserInfo.combineAutoTime -= 1;
p.default.dataModalMgr.UserInfo.combineAutoTime <= 0 && this.scheduleCombineOver();
this.view.CombineAutoTxt.string = h.default.formatTimeForSecond(p.default.dataModalMgr.UserInfo.combineAutoTime);
for (var e = [], t = 0; t < p.default.dataModalMgr.UserInfo.userWorkbench.length; t++) if (!(i = this.view.WorkContent.children[t].getComponent("ItemProduce")).isUsed) {
var n = p.default.dataModalMgr.UserInfo.userWorkbench[t];
0 !== n && e.push({
itemId: n,
index: t
});
}
e.sort(function(e, t) {
return Number(e.itemId) - Number(t.itemId);
});
var r = -1, o = -1;
for (t = 0; t < e.length - 1; t++) if (e[t].itemId === e[t + 1].itemId && "38" !== e[t].itemId.toString()) {
o = e[t].index;
r = e[t + 1].index;
break;
}
if (-1 !== r && -1 !== o) {
if (this._currentNode) {
var i;
if ((i = this._currentNode.getComponent("ItemProduce")).index === r || i.index === o) {
i.dragOver();
this.hideProduce();
this._currentNode = null;
this.hideVirtualItem(!1);
}
}
this.combinOrSwap(this.view.WorkContent.children[r], this.view.WorkContent.children[o]);
}
};
t.prototype.scheduleCombineOver = function() {
this._isCombining = !1;
this.view.CombineAutoTxt.node.active = !1;
this.unschedule(this.combineAuto);
};
i([ _(cc.Prefab) ], t.prototype, "itemProducePrefab", void 0);
return i([ g ], t);
}(l.UIScreen);
n.default = m;
cc._RF.pop();
}, {
"../../../Protocol/bundle": "bundle",
"../../Common/UIForm": "UIForm",
"../../Manager/FormMgr": "FormMgr",
"../../Manager/GameMgr": "GameMgr",
"../../Manager/SoundMgr": "SoundMgr",
"../../UIConfig": "UIConfig",
"../../Utils/CocosHelper": "CocosHelper",
"../UIToast": "UIToast"
} ],
UIInviteGetInfo_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "ce2a9E9K3BMpICnFv+csbqK", "UIInviteGetInfo_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.SetInfoBtn = null;
t.CopyWechatBtn = null;
t.CopyQQBtn = null;
t.CloseBtn = null;
t.HeadSP = null;
t.NameLab = null;
return t;
}
i([ l(s.default) ], t.prototype, "SetInfoBtn", void 0);
i([ l(s.default) ], t.prototype, "CopyWechatBtn", void 0);
i([ l(s.default) ], t.prototype, "CopyQQBtn", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
i([ l(cc.Sprite) ], t.prototype, "HeadSP", void 0);
i([ l(cc.Label) ], t.prototype, "NameLab", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIInviteGetInfo: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "67d50FHbUBFlaaUadFP3y0b", "UIInviteGetInfo");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = e("../../Manager/FormMgr"), c = e("../../UIConfig"), l = cc._decorator, u = l.ccclass, p = (l.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
this.view.SetInfoBtn.addClick(function() {
a.default.open(c.default.UISetSocial);
}, this);
this.view.CopyQQBtn.addClick(function() {
console.log("qq");
}, this);
this.view.CopyWechatBtn.addClick(function() {
console.log("wechat");
}, this);
};
return i([ u ], t);
}(s.UIWindow));
n.default = p;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm",
"../../Manager/FormMgr": "FormMgr",
"../../UIConfig": "UIConfig"
} ],
UILoading_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "d6a9c7+3lFNSZtgW9z8QvXg", "UILoading_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.Right = null;
t.Left = null;
return t;
}
i([ c(cc.Node) ], t.prototype, "Right", void 0);
i([ c(cc.Node) ], t.prototype, "Left", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
UILoading: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "2580cHflTxBzYtR5gDT9n2h", "UILoading");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../Common/UIForm"), l = e("../Manager/AdapterMgr"), u = e("../Utils/CocosHelper"), p = cc._decorator, f = p.ccclass, d = (p.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {};
t.prototype.showEffect = function() {
return s(this, void 0, void 0, function() {
var e;
return a(this, function(t) {
switch (t.label) {
case 0:
e = l.default.inst.visibleSize.width / 2 + this.view.Left.width / 2;
this.view.Left.x = -e;
this.view.Right.x = e;
return [ 4, Promise.all([ u.default.runTweenSync(this.view.Left, cc.tween().to(.3, {
x: -228
}, cc.easeIn(3))), u.default.runTweenSync(this.view.Right, cc.tween().to(.3, {
x: 228
}, cc.easeIn(3))) ]) ];

case 1:
t.sent();
return [ 2 ];
}
});
});
};
t.prototype.hideEffect = function() {
return s(this, void 0, void 0, function() {
var e;
return a(this, function(t) {
switch (t.label) {
case 0:
e = l.default.inst.visibleSize.width / 2 + this.view.Left.width / 2;
this.view.Left.x = -228;
this.view.Right.x = 228;
return [ 4, u.default.sleepSync(.5) ];

case 1:
t.sent();
return [ 4, Promise.all([ u.default.runTweenSync(this.view.Left, cc.tween().to(.3, {
x: -e
}, cc.easeIn(3))), u.default.runTweenSync(this.view.Right, cc.tween().to(.3, {
x: e
}, cc.easeIn(3))) ]) ];

case 2:
t.sent();
return [ 2 ];
}
});
});
};
return i([ f ], t);
}(c.UITips));
n.default = d;
cc._RF.pop();
}, {
"../Common/UIForm": "UIForm",
"../Manager/AdapterMgr": "AdapterMgr",
"../Utils/CocosHelper": "CocosHelper"
} ],
UILogin_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a88begnTmZOIY7ImNZFjPCO", "UILogin_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnWechat = null;
t.UserAgarn = null;
t.BtnXieYi = null;
t.BtnZhengCe = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnWechat", void 0);
i([ l(cc.Toggle) ], t.prototype, "UserAgarn", void 0);
i([ l(s.default) ], t.prototype, "BtnXieYi", void 0);
i([ l(s.default) ], t.prototype, "BtnZhengCe", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UILogin: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "458d9swLH9Pm7SGFvP8V6H5", "UILogin");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../Common/UIForm"), l = e("../Manager/FormMgr"), u = e("../Manager/NativeMgr"), p = e("../Manager/NetMgr"), f = e("../Net/RpcConent"), d = e("../UIConfig"), h = e("./UIToast"), y = cc._decorator, v = y.ccclass, g = (y.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
n = t;
t.prototype.onLoad = function() {
return s(this, void 0, void 0, function() {
var e;
return a(this, function(t) {
switch (t.label) {
case 0:
return [ 4, f.apiClient.connect() ];

case 1:
(e = t.sent()).isSucc || console.log(e.errMsg);
return [ 2 ];
}
});
});
};
t.testRpc = function(e) {
return s(this, void 0, void 0, function() {
var t;
return a(this, function(n) {
switch (n.label) {
case 0:
console.log("code:" + e);
return [ 4, f.apiClient.callApi("Login", {
code: e
}) ];

case 1:
t = n.sent();
console.log(t);
return [ 2 ];
}
});
});
};
t.prototype.start = function() {
var e = this;
this.view.BtnWechat.addClick(function() {
return s(e, void 0, void 0, function() {
return a(this, function() {
this.login();
return [ 2 ];
});
});
}, this);
};
t.prototype.login = function() {
return s(this, void 0, void 0, function() {
return a(this, function() {
if (!this.view.UserAgarn.getComponent(cc.Toggle).isChecked) {
h.default.popUp("请阅读并同意《用户协议》");
return [ 2 ];
}
u.NativeMgr.callNativeClass("AppActivity", "loginWX", n.testRpc);
console.log("调用完成");
l.default.open(d.default.UIHome);
return [ 2 ];
});
});
};
t.prototype.getToken = function(e) {
e.success && p.default.onLoginReq(e.token);
};
var n;
return n = i([ v ], t);
}(c.UIScreen));
n.default = g;
cc._RF.pop();
}, {
"../Common/UIForm": "UIForm",
"../Manager/FormMgr": "FormMgr",
"../Manager/NativeMgr": "NativeMgr",
"../Manager/NetMgr": "NetMgr",
"../Net/RpcConent": "RpcConent",
"../UIConfig": "UIConfig",
"./UIToast": "UIToast"
} ],
UILotteryGetTip_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "cac2dv25FZOgr8p0cj8WwKV", "UILotteryGetTip_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.GetNumLab = null;
t.BtnSure = null;
t.GetIconSp = null;
return t;
}
i([ l(cc.Label) ], t.prototype, "GetNumLab", void 0);
i([ l(s.default) ], t.prototype, "BtnSure", void 0);
i([ l(cc.Sprite) ], t.prototype, "GetIconSp", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UILotteryGetTip: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "e361cy6Ih1C5JAmRoLs82SR", "UILotteryGetTip");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnSure.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UILottery_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "9fcdfEoocNAqIUplBLQBh+c", "UILottery_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.LotteryReward = null;
t.CloseBtn = null;
t.GoBtn = null;
t.NumTxt = null;
t.CDTxt = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "LotteryReward", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
i([ l(s.default) ], t.prototype, "GoBtn", void 0);
i([ l(cc.Label) ], t.prototype, "NumTxt", void 0);
i([ l(cc.Label) ], t.prototype, "CDTxt", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UILottery: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a2975EwmBRNKaMDx+6NEG7u", "UILottery");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Struct"), a = e("../../Common/SysDefine"), c = e("../../Common/UIForm"), l = e("../../Utils/CocosHelper"), u = cc._decorator, p = u.ccclass, f = u.property, d = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.modalType = new s.ModalType(a.ModalOpacity.OpacityHigh);
t.lotteryItem = null;
return t;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
this.view.GoBtn.addClick(this.onGoClick, this);
for (var t = 0; t < this.view.LotteryReward.childrenCount; t++) {
var n = this.view.LotteryReward.children[t], r = cc.instantiate(this.lotteryItem);
r.getComponent("ItemLottery").setInfo({
num: 100 + t,
icon: (t + 1).toString()
});
r.parent = n;
}
};
t.prototype.onGoClick = function() {
l.default.runTweenSync(this.view.LotteryReward, cc.tween().to(5, {
angle: -2070
}, cc.easeInOut(3)));
};
i([ f(cc.Prefab) ], t.prototype, "lotteryItem", void 0);
return i([ p ], t);
}(c.UIWindow);
n.default = d;
cc._RF.pop();
}, {
"../../Common/Struct": "Struct",
"../../Common/SysDefine": "SysDefine",
"../../Common/UIForm": "UIForm",
"../../Utils/CocosHelper": "CocosHelper"
} ],
UIManager: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "f18581vC/JBkoXNDtnFIDo4", "UIManager");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("./ResMgr"), s = e("./ModalMgr"), a = e("../Scene/Scene"), c = e("../Net/EventCenter"), l = e("../Net/EventType"), u = e("../Common/UIBase"), p = e("../Common/SysDefine"), f = e("../Adapter/ContentAdapter"), d = function() {
function e() {
this._ndScreen = null;
this._ndFixed = null;
this._ndPopUp = null;
this._ndTips = null;
this._windows = [];
this._allForms = cc.js.createMap();
this._showingForms = cc.js.createMap();
this._tipsForms = cc.js.createMap();
this._loadingForm = cc.js.createMap();
}
e.getInstance = function() {
var t = this;
if (null == this.instance) {
this.instance = new e();
var n = cc.director.getScene().getChildByName("Canvas"), r = n.getChildByName(p.SysDefine.SYS_SCENE_NODE);
if (!r) {
(r = new cc.Node(p.SysDefine.SYS_SCENE_NODE)).addComponent(a.default);
r.parent = n;
}
var o = new cc.Node(p.SysDefine.SYS_UIROOT_NODE);
r.addChild(o);
o.addChild(this.instance._ndScreen = new cc.Node(p.SysDefine.SYS_SCREEN_NODE));
o.addChild(this.instance._ndFixed = new cc.Node(p.SysDefine.SYS_FIXED_NODE));
o.addChild(this.instance._ndPopUp = new cc.Node(p.SysDefine.SYS_POPUP_NODE));
o.addChild(this.instance._ndTips = new cc.Node(p.SysDefine.SYS_TOPTIPS_NODE));
cc.director.once(cc.Director.EVENT_BEFORE_SCENE_LAUNCH, function() {
t.instance = null;
});
}
return this.instance;
};
e.prototype.loadUIForm = function(e) {
return r(this, void 0, void 0, function() {
var t;
return o(this, function(n) {
switch (n.label) {
case 0:
return [ 4, this.loadForm(e) ];

case 1:
if (!(t = n.sent())) {
console.warn(t + "没有被成功加载");
return [ 2, null ];
}
return [ 2, t ];
}
});
});
};
e.prototype.openForm = function(e, t, n) {
return r(this, void 0, void 0, function() {
var r;
return o(this, function(o) {
switch (o.label) {
case 0:
if (!e || e.length <= 0) {
cc.warn(e + ", 参数错误");
return [ 2 ];
}
if (this.checkFormShowing(e)) {
cc.warn(e + ", 窗体正在显示中");
return [ 2, null ];
}
return [ 4, this.loadForm(e) ];

case 1:
if (!(r = o.sent())) {
cc.warn(e + " 加载失败了!");
return [ 2, null ];
}
r.fid = e;
r.formData = n;
switch (r.formType) {
case p.FormType.Screen:
return [ 3, 2 ];

case p.FormType.Fixed:
return [ 3, 4 ];

case p.FormType.Window:
return [ 3, 6 ];

case p.FormType.Tips:
return [ 3, 8 ];
}
return [ 3, 10 ];

case 2:
return [ 4, this.enterToScreen(r.fid, t) ];

case 3:
o.sent();
return [ 3, 10 ];

case 4:
return [ 4, this.enterToFixed(r.fid, t) ];

case 5:
o.sent();
return [ 3, 10 ];

case 6:
return [ 4, this.enterToPopup(r.fid, t) ];

case 7:
o.sent();
return [ 3, 10 ];

case 8:
return [ 4, this.enterToTips(r.fid, t) ];

case 9:
o.sent();
return [ 3, 10 ];

case 10:
return [ 2, r ];
}
});
});
};
e.prototype.closeForm = function(e) {
return r(this, void 0, void 0, function() {
var t;
return o(this, function(n) {
switch (n.label) {
case 0:
if (!e || e.length <= 0) {
cc.warn(e + ", 参数错误");
return [ 2 ];
}
if (!(t = this._allForms[e])) return [ 2, !1 ];
switch (t.formType) {
case p.FormType.Screen:
return [ 3, 1 ];

case p.FormType.Fixed:
return [ 3, 3 ];

case p.FormType.Window:
return [ 3, 5 ];

case p.FormType.Tips:
return [ 3, 7 ];
}
return [ 3, 9 ];

case 1:
return [ 4, this.exitToScreen(e) ];

case 2:
n.sent();
return [ 3, 9 ];

case 3:
return [ 4, this.exitToFixed(e) ];

case 4:
n.sent();
return [ 3, 9 ];

case 5:
return [ 4, this.exitToPopup(e) ];

case 6:
n.sent();
c.EventCenter.emit(l.EventType.WindowClosed, e);
return [ 3, 9 ];

case 7:
return [ 4, this.exitToTips(e) ];

case 8:
n.sent();
return [ 3, 9 ];

case 9:
c.EventCenter.emit(l.EventType.FormClosed, e);
t.formData && t.formData.onClose && t.formData.onClose();
t.willDestory && this.destoryForm(t);
return [ 2, !0 ];
}
});
});
};
e.prototype.loadForm = function(e) {
return r(this, void 0, Promise, function() {
var t, n = this;
return o(this, function() {
return (t = this._allForms[e]) ? [ 2, t ] : [ 2, new Promise(function(t) {
if (n._loadingForm[e]) n._loadingForm[e].push(t); else {
n._loadingForm[e] = [ t ];
n._doLoadUIForm(e).then(function(t) {
for (var r = 0, o = n._loadingForm[e]; r < o.length; r++) (0, o[r])(t);
n._loadingForm[e] = null;
delete n._loadingForm[e];
});
}
}) ];
});
});
};
e.prototype._doLoadUIForm = function(e) {
return r(this, void 0, void 0, function() {
var t, n;
return o(this, function(r) {
switch (r.label) {
case 0:
return [ 4, i.default.inst.loadForm(e) ];

case 1:
t = r.sent();
if (!(n = t.getComponent(u.default))) {
cc.warn(e + " 结点没有绑定UIBase");
return [ 2, null ];
}
t.active = !1;
t.addComponent(f.default);
switch (n.formType) {
case p.FormType.Screen:
this._ndScreen.addChild(t);
break;

case p.FormType.Fixed:
this._ndFixed.addChild(t);
break;

case p.FormType.Window:
this._ndPopUp.addChild(t);
break;

case p.FormType.Tips:
this._ndTips.addChild(t);
}
this._allForms[e] = n;
return [ 2, n ];
}
});
});
};
e.prototype.enterToScreen = function(e, t) {
return r(this, void 0, void 0, function() {
var n, r, i;
return o(this, function(o) {
switch (o.label) {
case 0:
n = [];
for (r in this._showingForms) n.push(this._showingForms[r].closeSelf());
return [ 4, Promise.all(n) ];

case 1:
o.sent();
if (!(i = this._allForms[e])) return [ 2 ];
this._showingForms[e] = i;
return [ 4, i._preInit(t) ];

case 2:
o.sent();
i.onShow(t);
return [ 4, this.showEffect(i) ];

case 3:
o.sent();
i.onAfterShow(t);
return [ 2 ];
}
});
});
};
e.prototype.enterToFixed = function(e, t) {
return r(this, void 0, void 0, function() {
var n;
return o(this, function(r) {
switch (r.label) {
case 0:
return (n = this._allForms[e]) ? [ 4, n._preInit(t) ] : [ 2 ];

case 1:
r.sent();
n.onShow(t);
this._showingForms[e] = n;
return [ 4, this.showEffect(n) ];

case 2:
r.sent();
n.onAfterShow(t);
return [ 2 ];
}
});
});
};
e.prototype.enterToPopup = function(e, t) {
return r(this, void 0, void 0, function() {
var n, r;
return o(this, function(o) {
switch (o.label) {
case 0:
return (n = this._allForms[e]) ? [ 4, n._preInit(t) ] : [ 2 ];

case 1:
o.sent();
this._windows.push(n);
for (r = 0; r < this._windows.length; r++) this._windows[r].node.zIndex = r + 1;
n.onShow(t);
this._showingForms[e] = n;
s.default.inst.checkModalWindow(this._windows);
return [ 4, this.showEffect(n) ];

case 2:
o.sent();
n.onAfterShow(t);
return [ 2 ];
}
});
});
};
e.prototype.enterToTips = function(e, t) {
return r(this, void 0, void 0, function() {
var n;
return o(this, function(r) {
switch (r.label) {
case 0:
return (n = this._allForms[e]) ? [ 4, n._preInit(t) ] : [ 2 ];

case 1:
r.sent();
this._tipsForms[e] = n;
n.onShow(t);
return [ 4, this.showEffect(n) ];

case 2:
r.sent();
n.onAfterShow(t);
return [ 2 ];
}
});
});
};
e.prototype.exitToScreen = function(e) {
return r(this, void 0, void 0, function() {
var t;
return o(this, function(n) {
switch (n.label) {
case 0:
if (!(t = this._showingForms[e])) return [ 2 ];
t.onHide();
return [ 4, this.hideEffect(t) ];

case 1:
n.sent();
t.onAfterHide();
this._showingForms[e] = null;
delete this._showingForms[e];
return [ 2 ];
}
});
});
};
e.prototype.exitToFixed = function(e) {
return r(this, void 0, void 0, function() {
var t;
return o(this, function(n) {
switch (n.label) {
case 0:
if (!(t = this._allForms[e])) return [ 2 ];
t.onHide();
return [ 4, this.hideEffect(t) ];

case 1:
n.sent();
t.onAfterHide();
this._showingForms[e] = null;
delete this._showingForms[e];
return [ 2 ];
}
});
});
};
e.prototype.exitToPopup = function(e) {
return r(this, void 0, void 0, function() {
var t, n;
return o(this, function(r) {
switch (r.label) {
case 0:
if (this._windows.length <= 0) return [ 2 ];
t = null;
for (n = this._windows.length - 1; n >= 0; n--) if (this._windows[n].fid === e) {
t = this._windows[n];
this._windows.splice(n, 1);
}
if (!t) return [ 2 ];
t.onHide();
s.default.inst.checkModalWindow(this._windows);
return [ 4, this.hideEffect(t) ];

case 1:
r.sent();
t.onAfterHide();
this._showingForms[e] = null;
delete this._showingForms[e];
return [ 2 ];
}
});
});
};
e.prototype.exitToTips = function(e) {
return r(this, void 0, void 0, function() {
var t;
return o(this, function(n) {
switch (n.label) {
case 0:
if (!(t = this._allForms[e])) return [ 2 ];
t.onHide();
return [ 4, this.hideEffect(t) ];

case 1:
n.sent();
t.onAfterHide();
this._tipsForms[e] = null;
delete this._tipsForms[e];
return [ 2 ];
}
});
});
};
e.prototype.showEffect = function(e) {
return r(this, void 0, void 0, function() {
return o(this, function(t) {
switch (t.label) {
case 0:
e.node.active = !0;
return [ 4, e.showEffect() ];

case 1:
t.sent();
return [ 2 ];
}
});
});
};
e.prototype.hideEffect = function(e) {
return r(this, void 0, void 0, function() {
return o(this, function(t) {
switch (t.label) {
case 0:
return [ 4, e.hideEffect() ];

case 1:
t.sent();
e.node.active = !1;
return [ 2 ];
}
});
});
};
e.prototype.destoryForm = function(e) {
i.default.inst.destoryDynamicRes(e.fid);
i.default.inst.destoryForm(e);
this._allForms[e.fid] = null;
delete this._allForms[e.fid];
};
e.prototype.checkFormShowing = function(e) {
var t = this._allForms[e];
return !!t && t.node.active;
};
e.prototype.checkFormLoading = function(e) {
return !!this._loadingForm[e];
};
e.prototype.getForm = function(e) {
return this._allForms[e];
};
e.instance = null;
return e;
}();
n.default = d;
cc._RF.pop();
}, {
"../Adapter/ContentAdapter": "ContentAdapter",
"../Common/SysDefine": "SysDefine",
"../Common/UIBase": "UIBase",
"../Net/EventCenter": "EventCenter",
"../Net/EventType": "EventType",
"../Scene/Scene": "Scene",
"./ModalMgr": "ModalMgr",
"./ResMgr": "ResMgr"
} ],
UIModalScript: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a6a5a+wskdJZJdS5tPl1qBP", "UIModalScript");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../Adapter/ContentAdapter"), l = e("../Manager/UIManager"), u = e("../Manager/WindowMgr"), p = e("../Utils/CocosHelper"), f = e("./SysDefine"), d = cc._decorator, h = d.ccclass, y = (d.property, 
function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t._texture = null;
return t;
}
t.prototype.getSingleTexture = function() {
if (this._texture) return this._texture;
for (var e = new Uint8Array(16), t = 0; t < 2; t++) for (var n = 0; n < 2; n++) {
e[8 * t + 4 * n + 0] = 255;
e[8 * t + 4 * n + 1] = 255;
e[8 * t + 4 * n + 2] = 255;
e[8 * t + 4 * n + 3] = 255;
}
var r = new cc.Texture2D();
r.name = "single color";
r.initWithData(e, cc.Texture2D.PixelFormat.RGBA8888, 2, 2);
r.handleLoadedTexture();
this._texture = r;
r.addRef();
return this._texture;
};
t.prototype.init = function() {
var e = this.getSingleTexture(), t = cc.view.getVisibleSize();
this.node.height = t.height;
this.node.width = t.width;
this.node.addComponent(cc.Button);
this.node.addComponent(c.default);
this.node.on("click", this.clickMaskWindow, this);
var n = this.node.addComponent(cc.Sprite);
n.sizeMode = cc.Sprite.SizeMode.CUSTOM;
n.type = cc.Sprite.Type.SIMPLE;
n.spriteFrame = new cc.SpriteFrame(e);
this.node.color = new cc.Color(0, 0, 0);
this.node.opacity = 0;
this.node.active = !1;
};
t.prototype.showModal = function(e, t, n) {
void 0 === t && (t = .6);
void 0 === n && (n = !0);
return s(this, void 0, void 0, function() {
var r;
return a(this, function(o) {
switch (o.label) {
case 0:
r = 0;
switch (e) {
case f.ModalOpacity.None:
this.node.active = !1;
break;

case f.ModalOpacity.OpacityZero:
r = 0;
break;

case f.ModalOpacity.OpacityLow:
r = 63;
break;

case f.ModalOpacity.OpacityHalf:
r = 126;
break;

case f.ModalOpacity.OpacityHigh:
r = 189;
break;

case f.ModalOpacity.OpacityFull:
r = 255;
}
return this.node.active ? n ? [ 4, p.default.runTweenSync(this.node, cc.tween().to(t, {
opacity: r
})) ] : [ 3, 2 ] : [ 2 ];

case 1:
o.sent();
return [ 3, 3 ];

case 2:
this.node.opacity = r;
o.label = 3;

case 3:
return [ 2 ];
}
});
});
};
t.prototype.clickMaskWindow = function() {
return s(this, void 0, void 0, function() {
var e;
return a(this, function(t) {
switch (t.label) {
case 0:
return (e = l.default.getInstance().getForm(this.fid)) && e.modalType.clickMaskClose ? [ 4, u.default.close(this.fid) ] : [ 3, 2 ];

case 1:
t.sent();
t.label = 2;

case 2:
return [ 2 ];
}
});
});
};
return i([ h ], t);
}(cc.Component));
n.default = y;
cc._RF.pop();
}, {
"../Adapter/ContentAdapter": "ContentAdapter",
"../Manager/UIManager": "UIManager",
"../Manager/WindowMgr": "WindowMgr",
"../Utils/CocosHelper": "CocosHelper",
"./SysDefine": "SysDefine"
} ],
UIMyExtar_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "ecf0c8eTQxFtLgIHpoFKg8C", "UIMyExtar_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnClose = null;
t.TodayExtarMoneyNumLab = null;
t.TodayExtarAddUserNumLab = null;
t.MonthExtarMoneyNumLab = null;
t.MonthExtarAddUserNumLab = null;
t.TotalExtarMoneyNumLab = null;
t.TotalExtarAddUserNumLab = null;
t.JieDuanNumLab = null;
t.JDCurExtarMoneyNumLab = null;
t.JDTotalExtarMoneyNumLab = null;
t.CompleteMoneyLab = null;
t.JDPS = null;
t.MultLab = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(cc.Label) ], t.prototype, "TodayExtarMoneyNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "TodayExtarAddUserNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "MonthExtarMoneyNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "MonthExtarAddUserNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "TotalExtarMoneyNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "TotalExtarAddUserNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "JieDuanNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "JDCurExtarMoneyNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "JDTotalExtarMoneyNumLab", void 0);
i([ l(cc.Label) ], t.prototype, "CompleteMoneyLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "JDPS", void 0);
i([ l(cc.Label) ], t.prototype, "MultLab", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIMyExtar: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "ec32a86RdVPv6wX2/nK6MG6", "UIMyExtar");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = e("../../Manager/GameMgr"), c = cc._decorator, l = c.ccclass, u = (c.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
this.view.TodayExtarMoneyNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.today_extar_get_money + "元";
this.view.TodayExtarAddUserNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.today_extar_add_user + "人";
this.view.MonthExtarMoneyNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.month_extar_get_money + "元";
this.view.MonthExtarAddUserNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.month_extar_add_user + "人";
this.view.TotalExtarMoneyNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.total_extar_get_money + "元";
this.view.TotalExtarAddUserNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.total_extar_add_user + "人";
this.view.JieDuanNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.cur_stage;
this.view.JDCurExtarMoneyNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.cur_stage_money + "元/";
this.view.JDTotalExtarMoneyNumLab.string = a.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "元";
this.view.MultLab.string = a.default.dataModalMgr.DataMyExtarInfo.add_mult + "倍增速中";
this.view.CompleteMoneyLab.string = "已完成" + a.default.dataModalMgr.DataMyExtarInfo.cur_stage_money / a.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "%，完成" + a.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "元自动转入钱包";
this.view.JDPS.getComponent(cc.ProgressBar).progress = a.default.dataModalMgr.DataMyExtarInfo.cur_stage_money / a.default.dataModalMgr.DataMyExtarInfo.total_stage_moner;
};
return i([ l ], t);
}(s.UIWindow));
n.default = u;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm",
"../../Manager/GameMgr": "GameMgr"
} ],
UIMyFriend_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "4ef20lq6GRFR5BOhBXfRnUE", "UIMyFriend_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.RankToggleNode = null;
t.CoinContentNode = null;
t.CoinContentViewNode = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "RankToggleNode", void 0);
i([ l(cc.Node) ], t.prototype, "CoinContentNode", void 0);
i([ l(cc.Node) ], t.prototype, "CoinContentViewNode", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIMyFriend: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "adf2cS9dgRF97Ri892cRJya", "UIMyFriend");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = e("../../Utils/CocosHelper"), c = cc._decorator, l = c.ccclass, u = (c.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.RankToggleNode.children.forEach(function(t) {
t.on(cc.Node.EventType.TOUCH_END, function() {
e.getIndex();
}, e);
});
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
t.prototype.getIndex = function() {
a.default.getContenerindex(this.view.RankToggleNode);
};
return i([ l ], t);
}(s.UIWindow));
n.default = u;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm",
"../../Utils/CocosHelper": "CocosHelper"
} ],
UIMyWallet_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "0ba37eeEWlGJrYOCXeBstW3", "UIMyWallet_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnClose = null;
t.MyMoneyLab = null;
t.BtnTips = null;
t.BtnGetMoney = null;
t.BtnGetMoneyLog = null;
t.MoneyToggleContainer = null;
t.BtnWeiXin = null;
t.WeiXinShiMingStateLab = null;
t.BtnZhiFuBao = null;
t.ZhiFuBaoShiMingStateLab = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(cc.Label) ], t.prototype, "MyMoneyLab", void 0);
i([ l(s.default) ], t.prototype, "BtnTips", void 0);
i([ l(s.default) ], t.prototype, "BtnGetMoney", void 0);
i([ l(s.default) ], t.prototype, "BtnGetMoneyLog", void 0);
i([ l(cc.Node) ], t.prototype, "MoneyToggleContainer", void 0);
i([ l(s.default) ], t.prototype, "BtnWeiXin", void 0);
i([ l(cc.Label) ], t.prototype, "WeiXinShiMingStateLab", void 0);
i([ l(s.default) ], t.prototype, "BtnZhiFuBao", void 0);
i([ l(cc.Label) ], t.prototype, "ZhiFuBaoShiMingStateLab", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIMyWallet: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "df13fAPT6VIGZ+ZIrOfU8O/", "UIMyWallet");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIMy_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "ac0411e/YpCgrETAlXWXJNy", "UIMy_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.HeadSp = null;
t.NameLab = null;
t.IDLab = null;
t.BtnClose = null;
t.MoneyNode = null;
t.MyMoneyLab = null;
t.BtnGetMoney = null;
t.BtnFriend = null;
t.BtnVip = null;
t.BtnShare = null;
t.BtnPlay = null;
t.BtnSetting = null;
return t;
}
i([ l(cc.Sprite) ], t.prototype, "HeadSp", void 0);
i([ l(cc.Label) ], t.prototype, "NameLab", void 0);
i([ l(cc.Label) ], t.prototype, "IDLab", void 0);
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(cc.Node) ], t.prototype, "MoneyNode", void 0);
i([ l(cc.Label) ], t.prototype, "MyMoneyLab", void 0);
i([ l(s.default) ], t.prototype, "BtnGetMoney", void 0);
i([ l(s.default) ], t.prototype, "BtnFriend", void 0);
i([ l(s.default) ], t.prototype, "BtnVip", void 0);
i([ l(s.default) ], t.prototype, "BtnShare", void 0);
i([ l(s.default) ], t.prototype, "BtnPlay", void 0);
i([ l(s.default) ], t.prototype, "BtnSetting", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIMy: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "830665HxcBGgL4WbMMsN6ET", "UIMy");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = e("../../Manager/FormMgr"), c = e("../../UIConfig"), l = cc._decorator, u = l.ccclass, p = (l.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
this.view.BtnGetMoney.addClick(function() {}, this);
this.view.BtnFriend.addClick(function() {
a.default.open(c.default.UIMyFriend);
}, this);
this.view.BtnVip.addClick(function() {}, this);
this.view.BtnShare.addClick(function() {}, this);
this.view.BtnSetting.addClick(function() {
a.default.open(c.default.UISetting);
}, this);
this.view.BtnPlay.addClick(function() {}, this);
};
return i([ u ], t);
}(s.UIWindow));
n.default = p;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm",
"../../Manager/FormMgr": "FormMgr",
"../../UIConfig": "UIConfig"
} ],
UIOline_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "b7b0fXYG9BBtq/Avd32Qps3", "UIOline_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.OnlineTimeLab = null;
t.GetNumLab = null;
t.GetBtn = null;
t.GetDoubelBtn = null;
t.TipBtn = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Label) ], t.prototype, "OnlineTimeLab", void 0);
i([ l(cc.Label) ], t.prototype, "GetNumLab", void 0);
i([ l(s.default) ], t.prototype, "GetBtn", void 0);
i([ l(s.default) ], t.prototype, "GetDoubelBtn", void 0);
i([ l(s.default) ], t.prototype, "TipBtn", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIOline: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "85572GQtqtFKZVAKiDIO51U", "UIOline");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../Common/UIForm": "UIForm"
} ],
UIRank_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "7d2b3b0k/1H76o2r8ZjjMXG", "UIRank_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.RankToggleNode = null;
t.CoinContentNode = null;
t.CoinContentViewNode = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "RankToggleNode", void 0);
i([ l(cc.Node) ], t.prototype, "CoinContentNode", void 0);
i([ l(cc.Node) ], t.prototype, "CoinContentViewNode", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIRank: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "247beHs7LJE8Zb9iy/PRZFc", "UIRank");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ListUtil"), a = e("../../Common/Struct"), c = e("../../Common/SysDefine"), l = e("../../Common/UIForm"), u = e("../../Utils/CocosHelper"), p = e("./ItemRank"), f = cc._decorator, d = f.ccclass, h = f.property, y = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.modalType = new a.ModalType(c.ModalOpacity.OpacityHigh);
t.listRank = null;
t.list_data1 = [];
t.list_data2 = [];
t.type = 0;
t.willDestory = !1;
return t;
}
t.prototype.onLoad = function() {
this.list_data1 = [];
for (var e = 1; e <= 50; e++) this.list_data1.push(e);
this.list_data2 = [];
for (e = 1; e <= 40; e++) this.list_data2.push(e);
this.changeType();
};
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
this.view.RankToggleNode.children.forEach(function(t) {
t.on(cc.Node.EventType.TOUCH_END, function() {
e.changeType();
}, e);
});
};
t.prototype.changeType = function() {
var e = u.default.getContenerindex(this.view.RankToggleNode);
this.listRank.numItems = 0 === e ? this.list_data1.length : this.list_data2.length;
};
t.prototype.onListRankRender = function(e, t) {
var n = u.default.getContenerindex(this.view.RankToggleNode);
0 === n ? e.getComponent(p.default).setData(n, {
level: this.list_data1[t],
num: 5623,
name: "小小新"
}) : e.getComponent(p.default).setData(n, {
level: this.list_data2[t],
num: 24323,
name: "阿明"
});
};
i([ h(s.default) ], t.prototype, "listRank", void 0);
return i([ d ], t);
}(l.UIWindow);
n.default = y;
cc._RF.pop();
}, {
"../../Common/Components/ListUtil": "ListUtil",
"../../Common/Struct": "Struct",
"../../Common/SysDefine": "SysDefine",
"../../Common/UIForm": "UIForm",
"../../Utils/CocosHelper": "CocosHelper",
"./ItemRank": "ItemRank"
} ],
UIRecoverTip_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "b1aecLFsalKWrCZShRN4eGA", "UIRecoverTip_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnSure = null;
t.GetIconTypeSp = null;
t.BtnClose = null;
t.BuyMoneyTypeSp = null;
t.GetNumLab = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnSure", void 0);
i([ l(cc.Sprite) ], t.prototype, "GetIconTypeSp", void 0);
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(cc.Sprite) ], t.prototype, "BuyMoneyTypeSp", void 0);
i([ l(cc.Label) ], t.prototype, "GetNumLab", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIRecoverTip: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "fb03aQy53xIE5oJAsH31g7H", "UIRecoverTip");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIRotateAndFlip_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "dbbf8mUrB5CzLbTTftYA+/u", "UIRotateAndFlip_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.ContainerNode = null;
t.CardNode = null;
return t;
}
i([ c(cc.Node) ], t.prototype, "ContainerNode", void 0);
i([ c(cc.Node) ], t.prototype, "CardNode", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
UIRotateAndFlip: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "935b68j161MWKlF/Jcki/N7", "UIRotateAndFlip");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../../Common/UIForm"), l = e("../../Utils/CocosHelper"), u = e("./CardArrayFlip_FrontCardBase"), p = cc._decorator, f = p.ccclass, d = (p.property, 
function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.card = null;
return t;
}
Object.defineProperty(t.prototype, "frontArrayCard", {
get: function() {
return this.view.ContainerNode.children[this.view.ContainerNode.childrenCount - 1];
},
enumerable: !1,
configurable: !0
});
t.prototype.start = function() {
this.card = this.view.CardNode.getComponent(u.default);
this.play();
};
t.prototype.play = function() {
return s(this, void 0, void 0, function() {
var e;
return a(this, function(t) {
switch (t.label) {
case 0:
e = this.card;
return [ 4, this.rotate(2) ];

case 1:
t.sent();
return [ 4, l.default.sleepSync(.2) ];

case 2:
t.sent();
e.show();
this.frontArrayCard.active = !1;
return [ 4, e.flipToFront() ];

case 3:
t.sent();
return [ 4, l.default.sleepSync(.2) ];

case 4:
t.sent();
return [ 4, e.flipToBack() ];

case 5:
t.sent();
this.frontArrayCard.active = !0;
e.hide();
return [ 4, l.default.sleepSync(.2) ];

case 6:
t.sent();
this.play();
return [ 2 ];
}
});
});
};
t.prototype.rotate = function(e) {
var t = this;
return new Promise(function(n) {
var r = t.view.ContainerNode, o = 1 * e, i = t.node.eulerAngles, s = i.x, a = i.z, c = cc.v3(s, 360 * e, a);
cc.tween(r).by(o, {
eulerAngles: c
}, {
easing: "quadOut"
}).call(n).start();
});
};
return i([ f ], t);
}(c.UIWindow));
n.default = d;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm",
"../../Utils/CocosHelper": "CocosHelper",
"./CardArrayFlip_FrontCardBase": "CardArrayFlip_FrontCardBase"
} ],
UIScoketReconnent_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "66ee6ZUCsJGx6t+CeWKiwZf", "UIScoketReconnent_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.pan = null;
return t;
}
i([ c(cc.Node) ], t.prototype, "pan", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
UIScoketReconnent: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "2a844K4K1FCX6AJ3H4zgXnS", "UIScoketReconnent");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../Common/Struct"), a = e("../Common/SysDefine"), c = e("../Common/UIForm"), l = e("../Utils/CocosHelper"), u = cc._decorator, p = u.ccclass, f = (u.property, 
function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.modalType = new s.ModalType(a.ModalOpacity.OpacityHigh);
return t;
}
t.prototype.onLoad = function() {
l.default.runRepeatTweenSync(this.view.pan, -1, cc.tween().to(6, {
angle: -360
}).to(0, {
angle: 0
}));
};
return i([ p ], t);
}(c.UIWindow));
n.default = f;
cc._RF.pop();
}, {
"../Common/Struct": "Struct",
"../Common/SysDefine": "SysDefine",
"../Common/UIForm": "UIForm",
"../Utils/CocosHelper": "CocosHelper"
} ],
UISetSocial_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "947ae4UKy5O74jyMmWUleZv", "UISetSocial_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.CloseBtn = null;
t.SureBtn = null;
t.Wechat = null;
t.QQ = null;
return t;
}
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
i([ l(s.default) ], t.prototype, "SureBtn", void 0);
i([ l(cc.EditBox) ], t.prototype, "Wechat", void 0);
i([ l(cc.EditBox) ], t.prototype, "QQ", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UISetSocial: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "9e973g5RGZP6pnznOyQce3A", "UISetSocial");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UISetting_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "b62fduA9cFGRoBIGa3DKEPd", "UISetting_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BtnClose = null;
t.BtnOutLogin = null;
t.BtnMusic = null;
t.offNode = null;
t.onNode = null;
t.BtnShiMing = null;
t.BtnYinSi = null;
t.BtnXieYi = null;
return t;
}
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(s.default) ], t.prototype, "BtnOutLogin", void 0);
i([ l(s.default) ], t.prototype, "BtnMusic", void 0);
i([ l(cc.Node) ], t.prototype, "offNode", void 0);
i([ l(cc.Node) ], t.prototype, "onNode", void 0);
i([ l(s.default) ], t.prototype, "BtnShiMing", void 0);
i([ l(s.default) ], t.prototype, "BtnYinSi", void 0);
i([ l(s.default) ], t.prototype, "BtnXieYi", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UISetting: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "6794aDGudpOn7XTM7oDTUsV", "UISetting");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../Common/UIForm"), a = e("../Manager/SoundMgr"), c = cc._decorator, l = c.ccclass, u = (c.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.onLoad = function() {
var e = a.default.inst.getVolume();
if (null != e && 0 == e.effectVolume && 0 == e.musicVolume) {
this.view.offNode.active = !0;
this.view.onNode.active = !1;
} else {
this.view.offNode.active = !1;
this.view.onNode.active = !0;
}
};
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
this.view.BtnMusic.addClick(function() {
e.view.offNode.active = !e.view.offNode.active;
e.view.onNode.active = !e.view.onNode.active;
a.default.inst.setEffectVolume(e.view.onNode.active ? 1 : 0);
a.default.inst.setMusicVolume(e.view.onNode.active ? 1 : 0);
}, this);
};
return i([ l ], t);
}(s.UIWindow));
n.default = u;
cc._RF.pop();
}, {
"../Common/UIForm": "UIForm",
"../Manager/SoundMgr": "SoundMgr"
} ],
UIShare_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "ee1aaKtjQFA64ct+eNWOX7L", "UIShare_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.CloseBtn = null;
t.WechatBtn = null;
t.PengYouQuanBtn = null;
t.PhotoBtn = null;
t.contentNode = null;
t.HeadSp1 = null;
t.NameLab1 = null;
t.HeadSp2 = null;
t.NameLab2 = null;
return t;
}
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
i([ l(s.default) ], t.prototype, "WechatBtn", void 0);
i([ l(s.default) ], t.prototype, "PengYouQuanBtn", void 0);
i([ l(s.default) ], t.prototype, "PhotoBtn", void 0);
i([ l(cc.Node) ], t.prototype, "contentNode", void 0);
i([ l(cc.Sprite) ], t.prototype, "HeadSp1", void 0);
i([ l(cc.Label) ], t.prototype, "NameLab1", void 0);
i([ l(cc.Sprite) ], t.prototype, "HeadSp2", void 0);
i([ l(cc.Label) ], t.prototype, "NameLab2", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIShare: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "fcc2aY0G6VMe58TO/xv8+KX", "UIShare");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.camereCapture = null;
t.texture2 = null;
t._width = 0;
t._height = 0;
return t;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
this.view.PhotoBtn.addClick(this.onCapture, this);
};
t.prototype.onCapture = function() {
var e = this.screenShot(this.view.contentNode);
e.x = 0;
e.y = 0;
e.parent = cc.director.getScene();
this.captureAction(e, e.width, e.height);
};
t.prototype.screenShot = function(e) {
void 0 === e && (e = null);
var t = new cc.RenderTexture();
t.initWithSize(cc.winSize.width, cc.winSize.height, cc.game._renderContext.STENCIL_INDEX8);
var n = new cc.SpriteFrame(), r = cc.winSize.width / 2 + e.x - e.width / 2, o = cc.winSize.height / 2 + e.y - e.height / 2, i = e.width, s = e.height;
null == e ? n.setTexture(t) : n.setTexture(t, new cc.Rect(r, o, i, s));
var a = new cc.Node(), c = a.addComponent(cc.Sprite);
c.spriteFrame = n;
c.spriteFrame.setFlipY(!0);
this.camereCapture.targetTexture = t;
this.camereCapture.render();
this.camereCapture.targetTexture = null;
return a;
};
t.prototype.captureAction = function(e, t, n) {
var r = cc.scaleTo(1, .5), o = cc.v2(this.node.width / 2 + t / 2 - 50, n + n / 2), i = cc.moveTo(1, o), s = cc.spawn(r, i);
e.runAction(s);
var a = cc.blink(.1, 1);
this.node.runAction(a);
};
t.prototype.saveFile = function() {};
i([ l(cc.Camera) ], t.prototype, "camereCapture", void 0);
return i([ c ], t);
}(s.UIWindow);
n.default = u;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIShiMing_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "c89c36JSJdGd6aBCzqhTRC4", "UIShiMing_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.Name = null;
t.IDCard = null;
t.BtnClose = null;
t.BtnSubMit = null;
return t;
}
i([ l(cc.EditBox) ], t.prototype, "Name", void 0);
i([ l(cc.EditBox) ], t.prototype, "IDCard", void 0);
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(s.default) ], t.prototype, "BtnSubMit", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIShiMing: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "a0623q92ddCwIgZmn29Xg8o", "UIShiMing");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIShop_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "8dd169JXbJOcJei5D96HyYP", "UIShop_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.ContentViewNode = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "ContentViewNode", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIShop: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "8796eXQYoBPbo5MRJ7Mh+EB", "UIShop");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/Components/ListUtil"), a = e("../../Common/UIForm"), c = e("../../Manager/GameMgr"), l = e("./ItemShop"), u = cc._decorator, p = u.ccclass, f = u.property, d = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.listShop = null;
return t;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
c.default.dataModalMgr.ShopInfo.list_item = [ {
item_buy_money: 9990,
item_buy_sp: "",
item_name: "毛笔碎片",
item_sp: ""
}, {
item_buy_money: 999,
item_buy_sp: "",
item_name: "毛笔碎片",
item_sp: ""
} ];
this.listShop.numItems = c.default.dataModalMgr.ShopInfo.list_item.length;
};
t.prototype.onListShopRender = function(e, t) {
e.getComponent(l.default).setData(c.default.dataModalMgr.ShopInfo.list_item[t]);
};
i([ f(s.default) ], t.prototype, "listShop", void 0);
return i([ p ], t);
}(a.UIWindow);
n.default = d;
cc._RF.pop();
}, {
"../../Common/Components/ListUtil": "ListUtil",
"../../Common/UIForm": "UIForm",
"../../Manager/GameMgr": "GameMgr",
"./ItemShop": "ItemShop"
} ],
UISignIn_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "beb74obdj1MF42+8prxkLOQ", "UISignIn_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.JDPS = null;
t.BtnClose = null;
t.SignInDayNumLab = null;
return t;
}
i([ l(cc.ProgressBar) ], t.prototype, "JDPS", void 0);
i([ l(s.default) ], t.prototype, "BtnClose", void 0);
i([ l(cc.Label) ], t.prototype, "SignInDayNumLab", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UISignIn: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "73948XacU5GgbB6pSauTSdb", "UISignIn");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.BtnClose.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIStart_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "6699a0LSNdDt4RO3MsYk6QD", "UIStart_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = cc._decorator, a = s.ccclass, c = s.property, l = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.LoginBar = null;
t.PragressLab = null;
return t;
}
i([ c(cc.ProgressBar) ], t.prototype, "LoginBar", void 0);
i([ c(cc.Label) ], t.prototype, "PragressLab", void 0);
return i([ a ], t);
}(cc.Component);
n.default = l;
cc._RF.pop();
}, {} ],
UIStart: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "6578d08ZX9F9JWz5X6YZGrt", "UIStart");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {};
return i([ c ], t);
}(s.UIScreen));
n.default = l;
cc._RF.pop();
}, {
"../Common/UIForm": "UIForm"
} ],
UITask_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "2d4069+FnJElb/aRerfWBEx", "UITask_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.ContentViewNode = null;
t.TipLab = null;
t.TaskProgress = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Node) ], t.prototype, "ContentViewNode", void 0);
i([ l(cc.Label) ], t.prototype, "TipLab", void 0);
i([ l(cc.ProgressBar) ], t.prototype, "TaskProgress", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UITask: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "254adlij2dCc6kZVodEYBTu", "UITask");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
UIToast: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "b744fdwl65BvaaTzy2b64Qj", "UIToast");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
}, s = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, a = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var c = e("../Common/SysDefine"), l = e("../Scene/Scene"), u = e("../Utils/CocosHelper"), p = cc._decorator, f = p.ccclass, d = (p.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
n = t;
t.popUp = function(e) {
var t;
return s(this, void 0, void 0, function() {
var r, o, i, s;
return a(this, function(a) {
switch (a.label) {
case 0:
l.default.inst.setInputBlock(!0);
a.label = 1;

case 1:
a.trys.push([ 1, 3, , 4 ]);
return [ 4, u.default.loadResSync("UIToast", cc.Prefab) ];

case 2:
r = a.sent();
o = cc.instantiate(r);
if (!(i = cc.find(c.SysDefine.SYS_UIROOT_NAME + "/" + c.SysDefine.SYS_TOPTIPS_NODE))) return [ 2 ];
i.addChild(o);
(s = null === (t = o.children[0].getChildByName("label")) || void 0 === t ? void 0 : t.getComponent(cc.Label)) && (s.string = e);
o.getComponent(n).showAnim();
return [ 3, 4 ];

case 3:
a.sent();
l.default.inst.setInputBlock(!1);
return [ 2 ];

case 4:
l.default.inst.setInputBlock(!1);
return [ 2 ];
}
});
});
};
t.prototype.showAnim = function() {
var e = this;
this.node.y = 0;
this.node.opacity = 255;
cc.tween(this.node).by(2, {
position: cc.v3(0, 80, 0)
}).to(.3, {
opacity: 0
}).call(function() {
e.node.destroy();
e.node.removeFromParent();
}).start();
};
var n;
return n = i([ f ], t);
}(cc.Component));
n.default = d;
cc._RF.pop();
}, {
"../Common/SysDefine": "SysDefine",
"../Scene/Scene": "Scene",
"../Utils/CocosHelper": "CocosHelper"
} ],
UIUnlockNewLevel_Auto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "5d508kJilBGOpda13fF4eYa", "UIUnlockNewLevel_Auto");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("./../Common/Components/ButtonPlus"), a = cc._decorator, c = a.ccclass, l = a.property, u = function(e) {
o(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.NewLevelLab = null;
t.NewNameLab = null;
t.CurLevelProductionNumLab = null;
t.CurLevelProductionIconSp = null;
t.YinPiaoNode = null;
t.YinPiaoNumLab = null;
t.SuiPianNode = null;
t.SuiPianNameLab = null;
t.SuiPianIconSp = null;
t.CloseBtn = null;
return t;
}
i([ l(cc.Label) ], t.prototype, "NewLevelLab", void 0);
i([ l(cc.Label) ], t.prototype, "NewNameLab", void 0);
i([ l(cc.Label) ], t.prototype, "CurLevelProductionNumLab", void 0);
i([ l(cc.Sprite) ], t.prototype, "CurLevelProductionIconSp", void 0);
i([ l(cc.Node) ], t.prototype, "YinPiaoNode", void 0);
i([ l(cc.Label) ], t.prototype, "YinPiaoNumLab", void 0);
i([ l(cc.Node) ], t.prototype, "SuiPianNode", void 0);
i([ l(cc.Label) ], t.prototype, "SuiPianNameLab", void 0);
i([ l(cc.Sprite) ], t.prototype, "SuiPianIconSp", void 0);
i([ l(s.default) ], t.prototype, "CloseBtn", void 0);
return i([ c ], t);
}(cc.Component);
n.default = u;
cc._RF.pop();
}, {
"./../Common/Components/ButtonPlus": "ButtonPlus"
} ],
UIUnlockNewLevel: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "49da5xdVQtLO6+ecL6HL4ks", "UIUnlockNewLevel");
var r, o = this && this.__extends || (r = function(e, t) {
return (r = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
})(e, t);
}, function(e, t) {
r(e, t);
function n() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (n.prototype = t.prototype, new n());
}), i = this && this.__decorate || function(e, t, n, r) {
var o, i = arguments.length, s = i < 3 ? t : null === r ? r = Object.getOwnPropertyDescriptor(t, n) : r;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) s = Reflect.decorate(e, t, n, r); else for (var a = e.length - 1; a >= 0; a--) (o = e[a]) && (s = (i < 3 ? o(s) : i > 3 ? o(t, n, s) : o(t, n)) || s);
return i > 3 && s && Object.defineProperty(t, n, s), s;
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var s = e("../../Common/UIForm"), a = cc._decorator, c = a.ccclass, l = (a.property, 
function(e) {
o(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.start = function() {
var e = this;
this.view.CloseBtn.addClick(function() {
e.closeSelf();
}, this);
};
return i([ c ], t);
}(s.UIWindow));
n.default = l;
cc._RF.pop();
}, {
"../../Common/UIForm": "UIForm"
} ],
WindowMgr: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "14f25mz3sdN6Yu+aE4UQvmc", "WindowMgr");
var r = this && this.__awaiter || function(e, t, n, r) {
return new (n || (n = Promise))(function(o, i) {
function s(e) {
try {
c(r.next(e));
} catch (e) {
i(e);
}
}
function a(e) {
try {
c(r.throw(e));
} catch (e) {
i(e);
}
}
function c(e) {
e.done ? o(e.value) : (t = e.value, t instanceof n ? t : new n(function(e) {
e(t);
})).then(s, a);
var t;
}
c((r = r.apply(e, t || [])).next());
});
}, o = this && this.__generator || function(e, t) {
var n, r, o, i, s = {
label: 0,
sent: function() {
if (1 & o[0]) throw o[1];
return o[1];
},
trys: [],
ops: []
};
return i = {
next: a(0),
throw: a(1),
return: a(2)
}, "function" == typeof Symbol && (i[Symbol.iterator] = function() {
return this;
}), i;
function a(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(i) {
if (n) throw new TypeError("Generator is already executing.");
for (;s; ) try {
if (n = 1, r && (o = 2 & i[0] ? r.return : i[0] ? r.throw || ((o = r.return) && o.call(r), 
0) : r.next) && !(o = o.call(r, i[1])).done) return o;
(r = 0, o) && (i = [ 2 & i[0], o.value ]);
switch (i[0]) {
case 0:
case 1:
o = i;
break;

case 4:
s.label++;
return {
value: i[1],
done: !1
};

case 5:
s.label++;
r = i[1];
i = [ 0 ];
continue;

case 7:
i = s.ops.pop();
s.trys.pop();
continue;

default:
if (!(o = s.trys, o = o.length > 0 && o[o.length - 1]) && (6 === i[0] || 2 === i[0])) {
s = 0;
continue;
}
if (3 === i[0] && (!o || i[1] > o[0] && i[1] < o[3])) {
s.label = i[1];
break;
}
if (6 === i[0] && s.label < o[1]) {
s.label = o[1];
o = i;
break;
}
if (o && s.label < o[2]) {
s.label = o[2];
s.ops.push(i);
break;
}
o[2] && s.ops.pop();
s.trys.pop();
continue;
}
i = t.call(e, s);
} catch (e) {
i = [ 6, e ];
r = 0;
} finally {
n = o = 0;
}
if (5 & i[0]) throw i[1];
return {
value: i[0] ? i[1] : void 0,
done: !0
};
}
};
Object.defineProperty(n, "__esModule", {
value: !0
});
var i = e("../Common/PriorityQueue"), s = e("../Common/PriorityStack"), a = e("../Common/Struct"), c = e("../UIConfig"), l = e("./TipsMgr"), u = e("./UIManager"), p = function() {
function e() {
this._showingList = new s.default();
this._waitingList = new i.default();
this._currWindow = "";
}
Object.defineProperty(e.prototype, "currWindow", {
get: function() {
return this._currWindow;
},
enumerable: !1,
configurable: !0
});
e.prototype.getWindows = function() {
return this._showingList.getElements();
};
e.prototype.open = function(e, t, n) {
void 0 === n && (n = {
showWait: !1,
priority: a.EPriority.FIVE
});
return r(this, void 0, void 0, function() {
return o(this, function(r) {
switch (r.label) {
case 0:
return n && n.isShowLoading ? [ 4, l.default.open(c.default.UILoading.prefabUrl, t, n) ] : [ 3, 2 ];

case 1:
r.sent();
r.label = 2;

case 2:
this._formatFormData(n);
return n && n.isShowLoading ? [ 4, l.default.close(c.default.UILoading.prefabUrl) ] : [ 3, 4 ];

case 3:
r.sent();
r.label = 4;

case 4:
if (!(this._showingList.size <= 0 || !n.showWait && n.priority >= this._showingList.getTopEPriority())) return [ 3, 6 ];
this._showingList.push(e, n.priority);
this._currWindow = this._showingList.getTopElement();
return [ 4, u.default.getInstance().openForm(e, t, n) ];

case 5:
return [ 2, r.sent() ];

case 6:
this._waitingList.enqueue({
prefabPath: e,
params: t,
formData: n
});
return [ 4, u.default.getInstance().loadUIForm(e) ];

case 7:
return [ 2, r.sent() ];
}
});
});
};
e.prototype.close = function(e) {
return r(this, void 0, void 0, function() {
var t;
return o(this, function(n) {
switch (n.label) {
case 0:
return this._showingList.remove(e) ? [ 4, u.default.getInstance().closeForm(e) ] : [ 2, !1 ];

case 1:
n.sent();
if (this._showingList.size <= 0 && this._waitingList.size > 0) {
t = this._waitingList.dequeue();
this.open(t.prefabPath, t.params, t.formData);
}
return [ 2, !0 ];
}
});
});
};
e.prototype.closeAll = function() {
return r(this, void 0, void 0, function() {
var e, t, n;
return o(this, function(r) {
switch (r.label) {
case 0:
this._waitingList.clear();
e = 0, t = this._showingList.getElements();
r.label = 1;

case 1:
if (!(e < t.length)) return [ 3, 4 ];
n = t[e];
return [ 4, u.default.getInstance().closeForm(n) ];

case 2:
r.sent();
r.label = 3;

case 3:
e++;
return [ 3, 1 ];

case 4:
this._showingList.clear();
return [ 2, !0 ];
}
});
});
};
e.prototype._formatFormData = function(e) {
e || (e = {});
e.hasOwnProperty("showWait") || (e.showWait = !1);
e.hasOwnProperty("priority") || (e.priority = a.EPriority.FIVE);
return e;
};
return e;
}();
n.default = new p();
cc._RF.pop();
}, {
"../Common/PriorityQueue": "PriorityQueue",
"../Common/PriorityStack": "PriorityStack",
"../Common/Struct": "Struct",
"../UIConfig": "UIConfig",
"./TipsMgr": "TipsMgr",
"./UIManager": "UIManager"
} ],
base: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "8efb6NfKMhFZIuCsouAhLK3", "base");
Object.defineProperty(n, "__esModule", {
value: !0
});
cc._RF.pop();
}, {} ],
bundle: [ function(e, t) {
"use strict";
cc._RF.push(t, "fa57e3VykxJ7pwIODLII/zq", "bundle");
var n = e("protobufjs/minimal"), r = n.Reader, o = n.Writer, i = n.util, s = n.roots.default || (n.roots.default = {});
s.InvokeBegin = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.invokeId = 0;
e.prototype.method = "";
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.invokeId && Object.hasOwnProperty.call(e, "invokeId") && t.uint32(8).int32(e.invokeId);
null != e.method && Object.hasOwnProperty.call(e, "method") && t.uint32(18).string(e.method);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.InvokeBegin(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.invokeId = e.int32();
break;

case 2:
o.method = e.string();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.invokeId && e.hasOwnProperty("invokeId") && !i.isInteger(e.invokeId) ? "invokeId: integer expected" : null != e.method && e.hasOwnProperty("method") && !i.isString(e.method) ? "method: string expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.InvokeBegin) return e;
var t = new s.InvokeBegin();
null != e.invokeId && (t.invokeId = 0 | e.invokeId);
null != e.method && (t.method = String(e.method));
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.invokeId = 0;
n.method = "";
}
null != e.invokeId && e.hasOwnProperty("invokeId") && (n.invokeId = e.invokeId);
null != e.method && e.hasOwnProperty("method") && (n.method = e.method);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.InvokeEnd = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.invokeId = 0;
e.prototype.status = 0;
e.prototype.error = "";
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.invokeId && Object.hasOwnProperty.call(e, "invokeId") && t.uint32(8).int32(e.invokeId);
null != e.status && Object.hasOwnProperty.call(e, "status") && t.uint32(16).int32(e.status);
null != e.error && Object.hasOwnProperty.call(e, "error") && t.uint32(26).string(e.error);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.InvokeEnd(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.invokeId = e.int32();
break;

case 2:
o.status = e.int32();
break;

case 3:
o.error = e.string();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.invokeId && e.hasOwnProperty("invokeId") && !i.isInteger(e.invokeId) ? "invokeId: integer expected" : null != e.status && e.hasOwnProperty("status") && !i.isInteger(e.status) ? "status: integer expected" : null != e.error && e.hasOwnProperty("error") && !i.isString(e.error) ? "error: string expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.InvokeEnd) return e;
var t = new s.InvokeEnd();
null != e.invokeId && (t.invokeId = 0 | e.invokeId);
null != e.status && (t.status = 0 | e.status);
null != e.error && (t.error = String(e.error));
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.invokeId = 0;
n.status = 0;
n.error = "";
}
null != e.invokeId && e.hasOwnProperty("invokeId") && (n.invokeId = e.invokeId);
null != e.status && e.hasOwnProperty("status") && (n.status = e.status);
null != e.error && e.hasOwnProperty("error") && (n.error = e.error);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.CloseConnection = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.code = 0;
e.prototype.reason = "";
e.prototype.allowReconnect = !1;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.code && Object.hasOwnProperty.call(e, "code") && t.uint32(8).int32(e.code);
null != e.reason && Object.hasOwnProperty.call(e, "reason") && t.uint32(18).string(e.reason);
null != e.allowReconnect && Object.hasOwnProperty.call(e, "allowReconnect") && t.uint32(24).bool(e.allowReconnect);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.CloseConnection(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.code = e.int32();
break;

case 2:
o.reason = e.string();
break;

case 3:
o.allowReconnect = e.bool();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.code && e.hasOwnProperty("code") && !i.isInteger(e.code) ? "code: integer expected" : null != e.reason && e.hasOwnProperty("reason") && !i.isString(e.reason) ? "reason: string expected" : null != e.allowReconnect && e.hasOwnProperty("allowReconnect") && "boolean" != typeof e.allowReconnect ? "allowReconnect: boolean expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.CloseConnection) return e;
var t = new s.CloseConnection();
null != e.code && (t.code = 0 | e.code);
null != e.reason && (t.reason = String(e.reason));
null != e.allowReconnect && (t.allowReconnect = Boolean(e.allowReconnect));
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.code = 0;
n.reason = "";
n.allowReconnect = !1;
}
null != e.code && e.hasOwnProperty("code") && (n.code = e.code);
null != e.reason && e.hasOwnProperty("reason") && (n.reason = e.reason);
null != e.allowReconnect && e.hasOwnProperty("allowReconnect") && (n.allowReconnect = e.allowReconnect);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.Envelope = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.header = null;
e.prototype.payload = i.newBuffer([]);
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.header && Object.hasOwnProperty.call(e, "header") && s.Envelope.Header.encode(e.header, t.uint32(10).fork()).ldelim();
null != e.payload && Object.hasOwnProperty.call(e, "payload") && t.uint32(18).bytes(e.payload);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.Envelope(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.header = s.Envelope.Header.decode(e, e.uint32());
break;

case 2:
o.payload = e.bytes();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
if ("object" != typeof e || null === e) return "object expected";
if (null != e.header && e.hasOwnProperty("header")) {
var t = s.Envelope.Header.verify(e.header);
if (t) return "header." + t;
}
return null != e.payload && e.hasOwnProperty("payload") && !(e.payload && "number" == typeof e.payload.length || i.isString(e.payload)) ? "payload: buffer expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.Envelope) return e;
var t = new s.Envelope();
if (null != e.header) {
if ("object" != typeof e.header) throw TypeError(".Envelope.header: object expected");
t.header = s.Envelope.Header.fromObject(e.header);
}
null != e.payload && ("string" == typeof e.payload ? i.base64.decode(e.payload, t.payload = i.newBuffer(i.base64.length(e.payload)), 0) : e.payload.length && (t.payload = e.payload));
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.header = null;
if (t.bytes === String) n.payload = ""; else {
n.payload = [];
t.bytes !== Array && (n.payload = i.newBuffer(n.payload));
}
}
null != e.header && e.hasOwnProperty("header") && (n.header = s.Envelope.Header.toObject(e.header, t));
null != e.payload && e.hasOwnProperty("payload") && (n.payload = t.bytes === String ? i.base64.encode(e.payload, 0, e.payload.length) : t.bytes === Array ? Array.prototype.slice.call(e.payload) : e.payload);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
e.Header = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.invokeBegin = null;
e.prototype.invokeEnd = null;
e.prototype.closeConnection = null;
e.prototype.event = null;
var t;
Object.defineProperty(e.prototype, "kind", {
get: i.oneOfGetter(t = [ "invokeBegin", "invokeEnd", "closeConnection", "event" ]),
set: i.oneOfSetter(t)
});
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.invokeBegin && Object.hasOwnProperty.call(e, "invokeBegin") && s.InvokeBegin.encode(e.invokeBegin, t.uint32(10).fork()).ldelim();
null != e.invokeEnd && Object.hasOwnProperty.call(e, "invokeEnd") && s.InvokeEnd.encode(e.invokeEnd, t.uint32(18).fork()).ldelim();
null != e.closeConnection && Object.hasOwnProperty.call(e, "closeConnection") && s.CloseConnection.encode(e.closeConnection, t.uint32(26).fork()).ldelim();
null != e.event && Object.hasOwnProperty.call(e, "event") && t.uint32(34).string(e.event);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.Envelope.Header(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.invokeBegin = s.InvokeBegin.decode(e, e.uint32());
break;

case 2:
o.invokeEnd = s.InvokeEnd.decode(e, e.uint32());
break;

case 3:
o.closeConnection = s.CloseConnection.decode(e, e.uint32());
break;

case 4:
o.event = e.string();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
if ("object" != typeof e || null === e) return "object expected";
var t = {};
if (null != e.invokeBegin && e.hasOwnProperty("invokeBegin")) {
t.kind = 1;
if (n = s.InvokeBegin.verify(e.invokeBegin)) return "invokeBegin." + n;
}
if (null != e.invokeEnd && e.hasOwnProperty("invokeEnd")) {
if (1 === t.kind) return "kind: multiple values";
t.kind = 1;
if (n = s.InvokeEnd.verify(e.invokeEnd)) return "invokeEnd." + n;
}
if (null != e.closeConnection && e.hasOwnProperty("closeConnection")) {
if (1 === t.kind) return "kind: multiple values";
t.kind = 1;
var n;
if (n = s.CloseConnection.verify(e.closeConnection)) return "closeConnection." + n;
}
if (null != e.event && e.hasOwnProperty("event")) {
if (1 === t.kind) return "kind: multiple values";
t.kind = 1;
if (!i.isString(e.event)) return "event: string expected";
}
return null;
};
e.fromObject = function(e) {
if (e instanceof s.Envelope.Header) return e;
var t = new s.Envelope.Header();
if (null != e.invokeBegin) {
if ("object" != typeof e.invokeBegin) throw TypeError(".Envelope.Header.invokeBegin: object expected");
t.invokeBegin = s.InvokeBegin.fromObject(e.invokeBegin);
}
if (null != e.invokeEnd) {
if ("object" != typeof e.invokeEnd) throw TypeError(".Envelope.Header.invokeEnd: object expected");
t.invokeEnd = s.InvokeEnd.fromObject(e.invokeEnd);
}
if (null != e.closeConnection) {
if ("object" != typeof e.closeConnection) throw TypeError(".Envelope.Header.closeConnection: object expected");
t.closeConnection = s.CloseConnection.fromObject(e.closeConnection);
}
null != e.event && (t.event = String(e.event));
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (null != e.invokeBegin && e.hasOwnProperty("invokeBegin")) {
n.invokeBegin = s.InvokeBegin.toObject(e.invokeBegin, t);
t.oneofs && (n.kind = "invokeBegin");
}
if (null != e.invokeEnd && e.hasOwnProperty("invokeEnd")) {
n.invokeEnd = s.InvokeEnd.toObject(e.invokeEnd, t);
t.oneofs && (n.kind = "invokeEnd");
}
if (null != e.closeConnection && e.hasOwnProperty("closeConnection")) {
n.closeConnection = s.CloseConnection.toObject(e.closeConnection, t);
t.oneofs && (n.kind = "closeConnection");
}
if (null != e.event && e.hasOwnProperty("event")) {
n.event = e.event;
t.oneofs && (n.kind = "event");
}
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
return e;
}();
s.GameServer = function() {
function e(e, t, r) {
n.rpc.Service.call(this, e, t, r);
}
(e.prototype = Object.create(n.rpc.Service.prototype)).constructor = e;
e.create = function(e, t, n) {
return new this(e, t, n);
};
Object.defineProperty(e.prototype.login = function e(t, n) {
return this.rpcCall(e, s.LoginRequest, s.LoginReply, t, n);
}, "name", {
value: "Login"
});
Object.defineProperty(e.prototype.buyProduce = function e(t, n) {
return this.rpcCall(e, s.BuyProduceRequest, s.BuyProduceReply, t, n);
}, "name", {
value: "BuyProduce"
});
Object.defineProperty(e.prototype.combineProduce = function e(t, n) {
return this.rpcCall(e, s.CombineProduceRequest, s.CombineProduceReply, t, n);
}, "name", {
value: "CombineProduce"
});
Object.defineProperty(e.prototype.refreshWorkBench = function e(t, n) {
return this.rpcCall(e, s.RefreshWorkBenchRequest, s.RefreshWorkBenchReply, t, n);
}, "name", {
value: "RefreshWorkBench"
});
return e;
}();
s.LoginRequest = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.accessToken = "";
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.accessToken && Object.hasOwnProperty.call(e, "accessToken") && t.uint32(10).string(e.accessToken);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.LoginRequest(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.accessToken = e.string();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.accessToken && e.hasOwnProperty("accessToken") && !i.isString(e.accessToken) ? "accessToken: string expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.LoginRequest) return e;
var t = new s.LoginRequest();
null != e.accessToken && (t.accessToken = String(e.accessToken));
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
t.defaults && (n.accessToken = "");
null != e.accessToken && e.hasOwnProperty("accessToken") && (n.accessToken = e.accessToken);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.LoginReply = function() {
function e(e) {
this.workbenchItems = [];
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.userInfo = null;
e.prototype.serverTime = 0;
e.prototype.coin = i.Long ? i.Long.fromBits(0, 0, !0) : 0;
e.prototype.diamond = i.Long ? i.Long.fromBits(0, 0, !0) : 0;
e.prototype.workbenchItems = i.emptyArray;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.userInfo && Object.hasOwnProperty.call(e, "userInfo") && s.UserInfo.encode(e.userInfo, t.uint32(10).fork()).ldelim();
null != e.serverTime && Object.hasOwnProperty.call(e, "serverTime") && t.uint32(16).uint32(e.serverTime);
null != e.coin && Object.hasOwnProperty.call(e, "coin") && t.uint32(24).uint64(e.coin);
null != e.diamond && Object.hasOwnProperty.call(e, "diamond") && t.uint32(32).uint64(e.diamond);
if (null != e.workbenchItems && e.workbenchItems.length) for (var n = 0; n < e.workbenchItems.length; ++n) s.WorkBenchItemInfo.encode(e.workbenchItems[n], t.uint32(42).fork()).ldelim();
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.LoginReply(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.userInfo = s.UserInfo.decode(e, e.uint32());
break;

case 2:
o.serverTime = e.uint32();
break;

case 3:
o.coin = e.uint64();
break;

case 4:
o.diamond = e.uint64();
break;

case 5:
o.workbenchItems && o.workbenchItems.length || (o.workbenchItems = []);
o.workbenchItems.push(s.WorkBenchItemInfo.decode(e, e.uint32()));
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
if ("object" != typeof e || null === e) return "object expected";
if (null != e.userInfo && e.hasOwnProperty("userInfo") && (n = s.UserInfo.verify(e.userInfo))) return "userInfo." + n;
if (null != e.serverTime && e.hasOwnProperty("serverTime") && !i.isInteger(e.serverTime)) return "serverTime: integer expected";
if (null != e.coin && e.hasOwnProperty("coin") && !(i.isInteger(e.coin) || e.coin && i.isInteger(e.coin.low) && i.isInteger(e.coin.high))) return "coin: integer|Long expected";
if (null != e.diamond && e.hasOwnProperty("diamond") && !(i.isInteger(e.diamond) || e.diamond && i.isInteger(e.diamond.low) && i.isInteger(e.diamond.high))) return "diamond: integer|Long expected";
if (null != e.workbenchItems && e.hasOwnProperty("workbenchItems")) {
if (!Array.isArray(e.workbenchItems)) return "workbenchItems: array expected";
for (var t = 0; t < e.workbenchItems.length; ++t) {
var n;
if (n = s.WorkBenchItemInfo.verify(e.workbenchItems[t])) return "workbenchItems." + n;
}
}
return null;
};
e.fromObject = function(e) {
if (e instanceof s.LoginReply) return e;
var t = new s.LoginReply();
if (null != e.userInfo) {
if ("object" != typeof e.userInfo) throw TypeError(".LoginReply.userInfo: object expected");
t.userInfo = s.UserInfo.fromObject(e.userInfo);
}
null != e.serverTime && (t.serverTime = e.serverTime >>> 0);
null != e.coin && (i.Long ? (t.coin = i.Long.fromValue(e.coin)).unsigned = !0 : "string" == typeof e.coin ? t.coin = parseInt(e.coin, 10) : "number" == typeof e.coin ? t.coin = e.coin : "object" == typeof e.coin && (t.coin = new i.LongBits(e.coin.low >>> 0, e.coin.high >>> 0).toNumber(!0)));
null != e.diamond && (i.Long ? (t.diamond = i.Long.fromValue(e.diamond)).unsigned = !0 : "string" == typeof e.diamond ? t.diamond = parseInt(e.diamond, 10) : "number" == typeof e.diamond ? t.diamond = e.diamond : "object" == typeof e.diamond && (t.diamond = new i.LongBits(e.diamond.low >>> 0, e.diamond.high >>> 0).toNumber(!0)));
if (e.workbenchItems) {
if (!Array.isArray(e.workbenchItems)) throw TypeError(".LoginReply.workbenchItems: array expected");
t.workbenchItems = [];
for (var n = 0; n < e.workbenchItems.length; ++n) {
if ("object" != typeof e.workbenchItems[n]) throw TypeError(".LoginReply.workbenchItems: object expected");
t.workbenchItems[n] = s.WorkBenchItemInfo.fromObject(e.workbenchItems[n]);
}
}
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
(t.arrays || t.defaults) && (n.workbenchItems = []);
if (t.defaults) {
n.userInfo = null;
n.serverTime = 0;
if (i.Long) {
var r = new i.Long(0, 0, !0);
n.coin = t.longs === String ? r.toString() : t.longs === Number ? r.toNumber() : r;
} else n.coin = t.longs === String ? "0" : 0;
if (i.Long) {
r = new i.Long(0, 0, !0);
n.diamond = t.longs === String ? r.toString() : t.longs === Number ? r.toNumber() : r;
} else n.diamond = t.longs === String ? "0" : 0;
}
null != e.userInfo && e.hasOwnProperty("userInfo") && (n.userInfo = s.UserInfo.toObject(e.userInfo, t));
null != e.serverTime && e.hasOwnProperty("serverTime") && (n.serverTime = e.serverTime);
null != e.coin && e.hasOwnProperty("coin") && ("number" == typeof e.coin ? n.coin = t.longs === String ? String(e.coin) : e.coin : n.coin = t.longs === String ? i.Long.prototype.toString.call(e.coin) : t.longs === Number ? new i.LongBits(e.coin.low >>> 0, e.coin.high >>> 0).toNumber(!0) : e.coin);
null != e.diamond && e.hasOwnProperty("diamond") && ("number" == typeof e.diamond ? n.diamond = t.longs === String ? String(e.diamond) : e.diamond : n.diamond = t.longs === String ? i.Long.prototype.toString.call(e.diamond) : t.longs === Number ? new i.LongBits(e.diamond.low >>> 0, e.diamond.high >>> 0).toNumber(!0) : e.diamond);
if (e.workbenchItems && e.workbenchItems.length) {
n.workbenchItems = [];
for (var o = 0; o < e.workbenchItems.length; ++o) n.workbenchItems[o] = s.WorkBenchItemInfo.toObject(e.workbenchItems[o], t);
}
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.BuyProduceRequest = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.produceLevel = 0;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.produceLevel && Object.hasOwnProperty.call(e, "produceLevel") && t.uint32(8).int32(e.produceLevel);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.BuyProduceRequest(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.produceLevel = e.int32();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.produceLevel && e.hasOwnProperty("produceLevel") && !i.isInteger(e.produceLevel) ? "produceLevel: integer expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.BuyProduceRequest) return e;
var t = new s.BuyProduceRequest();
null != e.produceLevel && (t.produceLevel = 0 | e.produceLevel);
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
t.defaults && (n.produceLevel = 0);
null != e.produceLevel && e.hasOwnProperty("produceLevel") && (n.produceLevel = e.produceLevel);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.BuyProduceReply = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.errcode = 0;
e.prototype.produceLevel = 0;
e.prototype.position = 0;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.errcode && Object.hasOwnProperty.call(e, "errcode") && t.uint32(8).int32(e.errcode);
null != e.produceLevel && Object.hasOwnProperty.call(e, "produceLevel") && t.uint32(16).int32(e.produceLevel);
null != e.position && Object.hasOwnProperty.call(e, "position") && t.uint32(24).int32(e.position);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.BuyProduceReply(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.errcode = e.int32();
break;

case 2:
o.produceLevel = e.int32();
break;

case 3:
o.position = e.int32();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.errcode && e.hasOwnProperty("errcode") && !i.isInteger(e.errcode) ? "errcode: integer expected" : null != e.produceLevel && e.hasOwnProperty("produceLevel") && !i.isInteger(e.produceLevel) ? "produceLevel: integer expected" : null != e.position && e.hasOwnProperty("position") && !i.isInteger(e.position) ? "position: integer expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.BuyProduceReply) return e;
var t = new s.BuyProduceReply();
null != e.errcode && (t.errcode = 0 | e.errcode);
null != e.produceLevel && (t.produceLevel = 0 | e.produceLevel);
null != e.position && (t.position = 0 | e.position);
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.errcode = 0;
n.produceLevel = 0;
n.position = 0;
}
null != e.errcode && e.hasOwnProperty("errcode") && (n.errcode = e.errcode);
null != e.produceLevel && e.hasOwnProperty("produceLevel") && (n.produceLevel = e.produceLevel);
null != e.position && e.hasOwnProperty("position") && (n.position = e.position);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.CombineProduceRequest = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.positionBase = 0;
e.prototype.positionTarget = 0;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.positionBase && Object.hasOwnProperty.call(e, "positionBase") && t.uint32(8).int32(e.positionBase);
null != e.positionTarget && Object.hasOwnProperty.call(e, "positionTarget") && t.uint32(16).int32(e.positionTarget);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.CombineProduceRequest(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.positionBase = e.int32();
break;

case 2:
o.positionTarget = e.int32();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.positionBase && e.hasOwnProperty("positionBase") && !i.isInteger(e.positionBase) ? "positionBase: integer expected" : null != e.positionTarget && e.hasOwnProperty("positionTarget") && !i.isInteger(e.positionTarget) ? "positionTarget: integer expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.CombineProduceRequest) return e;
var t = new s.CombineProduceRequest();
null != e.positionBase && (t.positionBase = 0 | e.positionBase);
null != e.positionTarget && (t.positionTarget = 0 | e.positionTarget);
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.positionBase = 0;
n.positionTarget = 0;
}
null != e.positionBase && e.hasOwnProperty("positionBase") && (n.positionBase = e.positionBase);
null != e.positionTarget && e.hasOwnProperty("positionTarget") && (n.positionTarget = e.positionTarget);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.CombineProduceReply = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.errcode = 0;
e.prototype.produceLevel = 0;
e.prototype.positionNew = 0;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.errcode && Object.hasOwnProperty.call(e, "errcode") && t.uint32(8).int32(e.errcode);
null != e.produceLevel && Object.hasOwnProperty.call(e, "produceLevel") && t.uint32(16).int32(e.produceLevel);
null != e.positionNew && Object.hasOwnProperty.call(e, "positionNew") && t.uint32(24).int32(e.positionNew);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.CombineProduceReply(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.errcode = e.int32();
break;

case 2:
o.produceLevel = e.int32();
break;

case 3:
o.positionNew = e.int32();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.errcode && e.hasOwnProperty("errcode") && !i.isInteger(e.errcode) ? "errcode: integer expected" : null != e.produceLevel && e.hasOwnProperty("produceLevel") && !i.isInteger(e.produceLevel) ? "produceLevel: integer expected" : null != e.positionNew && e.hasOwnProperty("positionNew") && !i.isInteger(e.positionNew) ? "positionNew: integer expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.CombineProduceReply) return e;
var t = new s.CombineProduceReply();
null != e.errcode && (t.errcode = 0 | e.errcode);
null != e.produceLevel && (t.produceLevel = 0 | e.produceLevel);
null != e.positionNew && (t.positionNew = 0 | e.positionNew);
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.errcode = 0;
n.produceLevel = 0;
n.positionNew = 0;
}
null != e.errcode && e.hasOwnProperty("errcode") && (n.errcode = e.errcode);
null != e.produceLevel && e.hasOwnProperty("produceLevel") && (n.produceLevel = e.produceLevel);
null != e.positionNew && e.hasOwnProperty("positionNew") && (n.positionNew = e.positionNew);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.RefreshWorkBenchRequest = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.RefreshWorkBenchRequest(); e.pos < n; ) {
var i = e.uint32();
e.skipType(7 & i);
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null;
};
e.fromObject = function(e) {
return e instanceof s.RefreshWorkBenchRequest ? e : new s.RefreshWorkBenchRequest();
};
e.toObject = function() {
return {};
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.RefreshWorkBenchReply = function() {
function e(e) {
this.workbenchInfo = [];
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.workbenchInfo = i.emptyArray;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
if (null != e.workbenchInfo && e.workbenchInfo.length) {
t.uint32(10).fork();
for (var n = 0; n < e.workbenchInfo.length; ++n) t.int32(e.workbenchInfo[n]);
t.ldelim();
}
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.RefreshWorkBenchReply(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.workbenchInfo && o.workbenchInfo.length || (o.workbenchInfo = []);
if (2 == (7 & i)) for (var a = e.uint32() + e.pos; e.pos < a; ) o.workbenchInfo.push(e.int32()); else o.workbenchInfo.push(e.int32());
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
if ("object" != typeof e || null === e) return "object expected";
if (null != e.workbenchInfo && e.hasOwnProperty("workbenchInfo")) {
if (!Array.isArray(e.workbenchInfo)) return "workbenchInfo: array expected";
for (var t = 0; t < e.workbenchInfo.length; ++t) if (!i.isInteger(e.workbenchInfo[t])) return "workbenchInfo: integer[] expected";
}
return null;
};
e.fromObject = function(e) {
if (e instanceof s.RefreshWorkBenchReply) return e;
var t = new s.RefreshWorkBenchReply();
if (e.workbenchInfo) {
if (!Array.isArray(e.workbenchInfo)) throw TypeError(".RefreshWorkBenchReply.workbenchInfo: array expected");
t.workbenchInfo = [];
for (var n = 0; n < e.workbenchInfo.length; ++n) t.workbenchInfo[n] = 0 | e.workbenchInfo[n];
}
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
(t.arrays || t.defaults) && (n.workbenchInfo = []);
if (e.workbenchInfo && e.workbenchInfo.length) {
n.workbenchInfo = [];
for (var r = 0; r < e.workbenchInfo.length; ++r) n.workbenchInfo[r] = e.workbenchInfo[r];
}
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.DecimalValue = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.uints = i.Long ? i.Long.fromBits(0, 0, !1) : 0;
e.prototype.nanos = 0;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.uints && Object.hasOwnProperty.call(e, "uints") && t.uint32(8).int64(e.uints);
null != e.nanos && Object.hasOwnProperty.call(e, "nanos") && t.uint32(21).sfixed32(e.nanos);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.DecimalValue(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.uints = e.int64();
break;

case 2:
o.nanos = e.sfixed32();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.uints && e.hasOwnProperty("uints") && !(i.isInteger(e.uints) || e.uints && i.isInteger(e.uints.low) && i.isInteger(e.uints.high)) ? "uints: integer|Long expected" : null != e.nanos && e.hasOwnProperty("nanos") && !i.isInteger(e.nanos) ? "nanos: integer expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.DecimalValue) return e;
var t = new s.DecimalValue();
null != e.uints && (i.Long ? (t.uints = i.Long.fromValue(e.uints)).unsigned = !1 : "string" == typeof e.uints ? t.uints = parseInt(e.uints, 10) : "number" == typeof e.uints ? t.uints = e.uints : "object" == typeof e.uints && (t.uints = new i.LongBits(e.uints.low >>> 0, e.uints.high >>> 0).toNumber()));
null != e.nanos && (t.nanos = 0 | e.nanos);
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
if (i.Long) {
var r = new i.Long(0, 0, !1);
n.uints = t.longs === String ? r.toString() : t.longs === Number ? r.toNumber() : r;
} else n.uints = t.longs === String ? "0" : 0;
n.nanos = 0;
}
null != e.uints && e.hasOwnProperty("uints") && ("number" == typeof e.uints ? n.uints = t.longs === String ? String(e.uints) : e.uints : n.uints = t.longs === String ? i.Long.prototype.toString.call(e.uints) : t.longs === Number ? new i.LongBits(e.uints.low >>> 0, e.uints.high >>> 0).toNumber() : e.uints);
null != e.nanos && e.hasOwnProperty("nanos") && (n.nanos = e.nanos);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.UserInfo = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.account = "";
e.prototype.nickname = "";
e.prototype.avatarUrl = "";
e.prototype.money = null;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.account && Object.hasOwnProperty.call(e, "account") && t.uint32(10).string(e.account);
null != e.nickname && Object.hasOwnProperty.call(e, "nickname") && t.uint32(18).string(e.nickname);
null != e.avatarUrl && Object.hasOwnProperty.call(e, "avatarUrl") && t.uint32(26).string(e.avatarUrl);
null != e.money && Object.hasOwnProperty.call(e, "money") && s.DecimalValue.encode(e.money, t.uint32(34).fork()).ldelim();
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.UserInfo(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.account = e.string();
break;

case 2:
o.nickname = e.string();
break;

case 3:
o.avatarUrl = e.string();
break;

case 4:
o.money = s.DecimalValue.decode(e, e.uint32());
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
if ("object" != typeof e || null === e) return "object expected";
if (null != e.account && e.hasOwnProperty("account") && !i.isString(e.account)) return "account: string expected";
if (null != e.nickname && e.hasOwnProperty("nickname") && !i.isString(e.nickname)) return "nickname: string expected";
if (null != e.avatarUrl && e.hasOwnProperty("avatarUrl") && !i.isString(e.avatarUrl)) return "avatarUrl: string expected";
if (null != e.money && e.hasOwnProperty("money")) {
var t = s.DecimalValue.verify(e.money);
if (t) return "money." + t;
}
return null;
};
e.fromObject = function(e) {
if (e instanceof s.UserInfo) return e;
var t = new s.UserInfo();
null != e.account && (t.account = String(e.account));
null != e.nickname && (t.nickname = String(e.nickname));
null != e.avatarUrl && (t.avatarUrl = String(e.avatarUrl));
if (null != e.money) {
if ("object" != typeof e.money) throw TypeError(".UserInfo.money: object expected");
t.money = s.DecimalValue.fromObject(e.money);
}
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.account = "";
n.nickname = "";
n.avatarUrl = "";
n.money = null;
}
null != e.account && e.hasOwnProperty("account") && (n.account = e.account);
null != e.nickname && e.hasOwnProperty("nickname") && (n.nickname = e.nickname);
null != e.avatarUrl && e.hasOwnProperty("avatarUrl") && (n.avatarUrl = e.avatarUrl);
null != e.money && e.hasOwnProperty("money") && (n.money = s.DecimalValue.toObject(e.money, t));
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.UserMoney = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.coin = 0;
e.prototype.diamond = 0;
e.prototype.money = 0;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.coin && Object.hasOwnProperty.call(e, "coin") && t.uint32(9).double(e.coin);
null != e.diamond && Object.hasOwnProperty.call(e, "diamond") && t.uint32(17).double(e.diamond);
null != e.money && Object.hasOwnProperty.call(e, "money") && t.uint32(29).float(e.money);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.UserMoney(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.coin = e.double();
break;

case 2:
o.diamond = e.double();
break;

case 3:
o.money = e.float();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.coin && e.hasOwnProperty("coin") && "number" != typeof e.coin ? "coin: number expected" : null != e.diamond && e.hasOwnProperty("diamond") && "number" != typeof e.diamond ? "diamond: number expected" : null != e.money && e.hasOwnProperty("money") && "number" != typeof e.money ? "money: number expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.UserMoney) return e;
var t = new s.UserMoney();
null != e.coin && (t.coin = Number(e.coin));
null != e.diamond && (t.diamond = Number(e.diamond));
null != e.money && (t.money = Number(e.money));
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.coin = 0;
n.diamond = 0;
n.money = 0;
}
null != e.coin && e.hasOwnProperty("coin") && (n.coin = t.json && !isFinite(e.coin) ? String(e.coin) : e.coin);
null != e.diamond && e.hasOwnProperty("diamond") && (n.diamond = t.json && !isFinite(e.diamond) ? String(e.diamond) : e.diamond);
null != e.money && e.hasOwnProperty("money") && (n.money = t.json && !isFinite(e.money) ? String(e.money) : e.money);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
s.WorkBenchItemInfo = function() {
function e(e) {
if (e) for (var t = Object.keys(e), n = 0; n < t.length; ++n) null != e[t[n]] && (this[t[n]] = e[t[n]]);
}
e.prototype.level = 0;
e.prototype.coinCost = i.Long ? i.Long.fromBits(0, 0, !1) : 0;
e.prototype.coinIncome = i.Long ? i.Long.fromBits(0, 0, !1) : 0;
e.create = function(t) {
return new e(t);
};
e.encode = function(e, t) {
t || (t = o.create());
null != e.level && Object.hasOwnProperty.call(e, "level") && t.uint32(8).int32(e.level);
null != e.coinCost && Object.hasOwnProperty.call(e, "coinCost") && t.uint32(16).int64(e.coinCost);
null != e.coinIncome && Object.hasOwnProperty.call(e, "coinIncome") && t.uint32(24).int64(e.coinIncome);
return t;
};
e.encodeDelimited = function(e, t) {
return this.encode(e, t).ldelim();
};
e.decode = function(e, t) {
e instanceof r || (e = r.create(e));
for (var n = void 0 === t ? e.len : e.pos + t, o = new s.WorkBenchItemInfo(); e.pos < n; ) {
var i = e.uint32();
switch (i >>> 3) {
case 1:
o.level = e.int32();
break;

case 2:
o.coinCost = e.int64();
break;

case 3:
o.coinIncome = e.int64();
break;

default:
e.skipType(7 & i);
}
}
return o;
};
e.decodeDelimited = function(e) {
e instanceof r || (e = new r(e));
return this.decode(e, e.uint32());
};
e.verify = function(e) {
return "object" != typeof e || null === e ? "object expected" : null != e.level && e.hasOwnProperty("level") && !i.isInteger(e.level) ? "level: integer expected" : null != e.coinCost && e.hasOwnProperty("coinCost") && !(i.isInteger(e.coinCost) || e.coinCost && i.isInteger(e.coinCost.low) && i.isInteger(e.coinCost.high)) ? "coinCost: integer|Long expected" : null != e.coinIncome && e.hasOwnProperty("coinIncome") && !(i.isInteger(e.coinIncome) || e.coinIncome && i.isInteger(e.coinIncome.low) && i.isInteger(e.coinIncome.high)) ? "coinIncome: integer|Long expected" : null;
};
e.fromObject = function(e) {
if (e instanceof s.WorkBenchItemInfo) return e;
var t = new s.WorkBenchItemInfo();
null != e.level && (t.level = 0 | e.level);
null != e.coinCost && (i.Long ? (t.coinCost = i.Long.fromValue(e.coinCost)).unsigned = !1 : "string" == typeof e.coinCost ? t.coinCost = parseInt(e.coinCost, 10) : "number" == typeof e.coinCost ? t.coinCost = e.coinCost : "object" == typeof e.coinCost && (t.coinCost = new i.LongBits(e.coinCost.low >>> 0, e.coinCost.high >>> 0).toNumber()));
null != e.coinIncome && (i.Long ? (t.coinIncome = i.Long.fromValue(e.coinIncome)).unsigned = !1 : "string" == typeof e.coinIncome ? t.coinIncome = parseInt(e.coinIncome, 10) : "number" == typeof e.coinIncome ? t.coinIncome = e.coinIncome : "object" == typeof e.coinIncome && (t.coinIncome = new i.LongBits(e.coinIncome.low >>> 0, e.coinIncome.high >>> 0).toNumber()));
return t;
};
e.toObject = function(e, t) {
t || (t = {});
var n = {};
if (t.defaults) {
n.level = 0;
if (i.Long) {
var r = new i.Long(0, 0, !1);
n.coinCost = t.longs === String ? r.toString() : t.longs === Number ? r.toNumber() : r;
} else n.coinCost = t.longs === String ? "0" : 0;
if (i.Long) {
r = new i.Long(0, 0, !1);
n.coinIncome = t.longs === String ? r.toString() : t.longs === Number ? r.toNumber() : r;
} else n.coinIncome = t.longs === String ? "0" : 0;
}
null != e.level && e.hasOwnProperty("level") && (n.level = e.level);
null != e.coinCost && e.hasOwnProperty("coinCost") && ("number" == typeof e.coinCost ? n.coinCost = t.longs === String ? String(e.coinCost) : e.coinCost : n.coinCost = t.longs === String ? i.Long.prototype.toString.call(e.coinCost) : t.longs === Number ? new i.LongBits(e.coinCost.low >>> 0, e.coinCost.high >>> 0).toNumber() : e.coinCost);
null != e.coinIncome && e.hasOwnProperty("coinIncome") && ("number" == typeof e.coinIncome ? n.coinIncome = t.longs === String ? String(e.coinIncome) : e.coinIncome : n.coinIncome = t.longs === String ? i.Long.prototype.toString.call(e.coinIncome) : t.longs === Number ? new i.LongBits(e.coinIncome.low >>> 0, e.coinIncome.high >>> 0).toNumber() : e.coinIncome);
return n;
};
e.prototype.toJSON = function() {
return this.constructor.toObject(this, n.util.toJSONOptions);
};
return e;
}();
t.exports = s;
cc._RF.pop();
}, {
"protobufjs/minimal": 274
} ],
serviceProto: [ function(e, t, n) {
"use strict";
cc._RF.push(t, "cf520EQdM1Bv7ZY01WPSsFp", "serviceProto");
Object.defineProperty(n, "__esModule", {
value: !0
});
n.serviceProto = void 0;
n.serviceProto = {
version: 5,
services: [ {
id: 0,
name: "Chat",
type: "msg"
}, {
id: 2,
name: "Login",
type: "api"
}, {
id: 1,
name: "Send",
type: "api"
} ],
types: {
"MsgChat/MsgChat": {
type: "Interface",
properties: [ {
id: 0,
name: "content",
type: {
type: "String"
}
}, {
id: 1,
name: "time",
type: {
type: "Date"
}
} ]
},
"PtlLogin/ReqLogin": {
type: "Interface",
properties: [ {
id: 0,
name: "code",
type: {
type: "String"
}
} ]
},
"PtlLogin/ResLogin": {
type: "Interface",
properties: [ {
id: 2,
name: "nickname",
type: {
type: "String"
}
} ]
},
"PtlSend/ReqSend": {
type: "Interface",
properties: [ {
id: 0,
name: "content",
type: {
type: "String"
}
} ]
},
"PtlSend/ResSend": {
type: "Interface",
properties: [ {
id: 0,
name: "time",
type: {
type: "Date"
}
} ]
}
}
};
cc._RF.pop();
}, {} ]
}, {}, [ "bundle", "BackgroundAdapter", "ContentAdapter", "UIBagAdd_Auto", "UIBag_Auto", "UIBuyTip_Auto", "UIColleCtionConversion_Auto", "UICollection_Auto", "UIDebrisGetTip_Auto", "UIDialog_Auto", "UIExtarCommitSure_Auto", "UIFriendGiveDetail_Auto", "UIFriendGive_Auto", "UIFriendMonerDetail_Auto", "UIFriendSeeDetail_Auto", "UIFriendSee_Auto", "UIFriendSerch_Auto", "UIFriend_Auto", "UIGetMoneyLog_Auto", "UIGetMoneyRule_Auto", "UIHome_Auto", "UIInviteGetInfo_Auto", "UILoading_Auto", "UILogin_Auto", "UILotteryGetTip_Auto", "UILottery_Auto", "UIMyExtar_Auto", "UIMyFriend_Auto", "UIMyWallet_Auto", "UIMy_Auto", "UIOline_Auto", "UIRank_Auto", "UIRecoverTip_Auto", "UIRotateAndFlip_Auto", "UIScoketReconnent_Auto", "UISetSocial_Auto", "UISetting_Auto", "UIShare_Auto", "UIShiMing_Auto", "UIShop_Auto", "UISignIn_Auto", "UIStart_Auto", "UITask_Auto", "UIUnlockNewLevel_Auto", "ButtonPlus", "ListItemUtil", "ListUtil", "TouchPlus", "GameConfig", "Pool", "PriorityQueue", "PriorityStack", "Struct", "SysDefine", "UIBase", "UIForm", "UIModalScript", "DataBag", "DataCollection", "DataFriend", "DataGetMoneyLog", "DataMyExtar", "DataProduce", "DataShop", "DataUser", "UIGaide", "Main", "AdapterMgr", "ConfigMgr", "DataModalMgr", "FixedMgr", "FormMgr", "GameMgr", "HotUpdateMgr", "InterfaceMgr", "LocalStorageMgr", "ModalMgr", "NativeMgr", "NetMgr", "ResMgr", "SceneMgr", "SoundMgr", "TipsMgr", "UIManager", "WindowMgr", "ClientChannel", "EventCenter", "EventType", "HttpHelper", "RpcConent", "ApiSend", "MsgChat", "PtlLogin", "PtlSend", "base", "serviceProto", "Scene", "UIConfig", "ItemBag", "UIBag", "UIBagAdd", "UIColleCtionConversion", "UICollection", "ItemFriendExtraDetail", "ItemFriendGiveDetail", "ItemFriendSee", "ItemFriendSeeDetail", "UIFriend", "UIFriendGive", "UIFriendGiveDetail", "UIFriendMonerDetail", "UIFriendSee", "UIFriendSeeDetail", "UIFriendSerch", "UIInviteGetInfo", "UISetSocial", "UIDebrisGetTip", "UIHome", "UIRecoverTip", "UIUnlockNewLevel", "ItemLottery", "ItemProduce", "UILottery", "UILotteryGetTip", "ItemGetMoneyLog", "UIExtarCommitSure", "UIGetMoneyLog", "UIGetMoneyRule", "UIMy", "UIMyExtar", "UIMyFriend", "UIMyWallet", "UIShiMing", "ItemRank", "UIRank", "CardArrayFlip_Card", "CardArrayFlip_CardLayout", "CardArrayFlip_FrontCard2D", "CardArrayFlip_FrontCardBase", "UIRotateAndFlip", "GenerateQR", "UIShare", "ItemShop", "UIBuyTip", "UIShop", "UISignIn", "UITask", "UIDialog", "UILoading", "UILogin", "UIOline", "UIScoketReconnent", "UISetting", "UIStart", "UIToast", "CocosHelper" ]);