
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIDealDetail_Auto extends cc.Component {
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
	@property(cc.Node)
	ContentViewNode: cc.Node = null;
 
}