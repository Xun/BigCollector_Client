
import ButtonPlus from "./../Common/Components/ButtonPlus"

const {ccclass, property} = cc._decorator;
@ccclass
export default class UIDealAdd_Auto extends cc.Component {
	@property(ButtonPlus)
	CloseBtn: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSelectType: ButtonPlus = null;
	@property(cc.Sprite)
	IconType: cc.Sprite = null;
	@property(cc.Label)
	IconName: cc.Label = null;
	@property(cc.Label)
	LabBtn: cc.Label = null;
	@property(ButtonPlus)
	BuyAdd: ButtonPlus = null;
	@property(cc.EditBox)
	EditePrice: cc.EditBox = null;
	@property(cc.EditBox)
	EditeNum: cc.EditBox = null;
	@property(cc.Label)
	LabRemain: cc.Label = null;
	@property(cc.Label)
	LabPay: cc.Label = null;
	@property(cc.Node)
	NodeDealItems: cc.Node = null;
	@property(ButtonPlus)
	BtnCloseNodeItems: ButtonPlus = null;
	@property(ButtonPlus)
	BtnBi: ButtonPlus = null;
	@property(ButtonPlus)
	BtnMo: ButtonPlus = null;
	@property(ButtonPlus)
	BtnZhi: ButtonPlus = null;
	@property(ButtonPlus)
	BtnYan: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSGYY: ButtonPlus = null;
	@property(ButtonPlus)
	BtnHLM: ButtonPlus = null;
	@property(ButtonPlus)
	BtnXYJ: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSHZ: ButtonPlus = null;
	@property(ButtonPlus)
	BtnL: ButtonPlus = null;
	@property(ButtonPlus)
	BtnM: ButtonPlus = null;
	@property(ButtonPlus)
	BtnZ: ButtonPlus = null;
	@property(ButtonPlus)
	BtnJ: ButtonPlus = null;
	@property(ButtonPlus)
	BtnZQ: ButtonPlus = null;
	@property(ButtonPlus)
	BtnXW: ButtonPlus = null;
	@property(ButtonPlus)
	BtnBH: ButtonPlus = null;
	@property(ButtonPlus)
	BtnQL: ButtonPlus = null;
	@property(ButtonPlus)
	BtnZG: ButtonPlus = null;
	@property(ButtonPlus)
	BtnLZY: ButtonPlus = null;
	@property(ButtonPlus)
	BtnOYX: ButtonPlus = null;
	@property(ButtonPlus)
	BtnWAS: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSX: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSS: ButtonPlus = null;
	@property(ButtonPlus)
	BtnSZ: ButtonPlus = null;
	@property(ButtonPlus)
	BtnHY: ButtonPlus = null;
	@property(ButtonPlus)
	BtnJC: ButtonPlus = null;
	@property(ButtonPlus)
	BtnPX: ButtonPlus = null;
	@property(ButtonPlus)
	BtnJL: ButtonPlus = null;
 
}