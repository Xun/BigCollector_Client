
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Protocol/bundle.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fa57e3VykxJ7pwIODLII/zq', 'bundle');
// Protocol/bundle.js

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal"); // Common aliases


var $Reader = $protobuf.Reader,
    $Writer = $protobuf.Writer,
    $util = $protobuf.util; // Exported root namespace

var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.InvokeBegin = function () {
  /**
   * Properties of an InvokeBegin.
   * @exports IInvokeBegin
   * @interface IInvokeBegin
   * @property {number|null} [invokeId] InvokeBegin invokeId
   * @property {string|null} [method] InvokeBegin method
   */

  /**
   * Constructs a new InvokeBegin.
   * @exports InvokeBegin
   * @classdesc Represents an InvokeBegin.
   * @implements IInvokeBegin
   * @constructor
   * @param {IInvokeBegin=} [properties] Properties to set
   */
  function InvokeBegin(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * InvokeBegin invokeId.
   * @member {number} invokeId
   * @memberof InvokeBegin
   * @instance
   */


  InvokeBegin.prototype.invokeId = 0;
  /**
   * InvokeBegin method.
   * @member {string} method
   * @memberof InvokeBegin
   * @instance
   */

  InvokeBegin.prototype.method = "";
  /**
   * Creates a new InvokeBegin instance using the specified properties.
   * @function create
   * @memberof InvokeBegin
   * @static
   * @param {IInvokeBegin=} [properties] Properties to set
   * @returns {InvokeBegin} InvokeBegin instance
   */

  InvokeBegin.create = function create(properties) {
    return new InvokeBegin(properties);
  };
  /**
   * Encodes the specified InvokeBegin message. Does not implicitly {@link InvokeBegin.verify|verify} messages.
   * @function encode
   * @memberof InvokeBegin
   * @static
   * @param {IInvokeBegin} message InvokeBegin message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  InvokeBegin.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.invokeId != null && Object.hasOwnProperty.call(message, "invokeId")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.invokeId);
    if (message.method != null && Object.hasOwnProperty.call(message, "method")) writer.uint32(
    /* id 2, wireType 2 =*/
    18).string(message.method);
    return writer;
  };
  /**
   * Encodes the specified InvokeBegin message, length delimited. Does not implicitly {@link InvokeBegin.verify|verify} messages.
   * @function encodeDelimited
   * @memberof InvokeBegin
   * @static
   * @param {IInvokeBegin} message InvokeBegin message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  InvokeBegin.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes an InvokeBegin message from the specified reader or buffer.
   * @function decode
   * @memberof InvokeBegin
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {InvokeBegin} InvokeBegin
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  InvokeBegin.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.InvokeBegin();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.invokeId = reader.int32();
          break;

        case 2:
          message.method = reader.string();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes an InvokeBegin message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof InvokeBegin
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {InvokeBegin} InvokeBegin
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  InvokeBegin.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies an InvokeBegin message.
   * @function verify
   * @memberof InvokeBegin
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  InvokeBegin.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.invokeId != null && message.hasOwnProperty("invokeId")) if (!$util.isInteger(message.invokeId)) return "invokeId: integer expected";
    if (message.method != null && message.hasOwnProperty("method")) if (!$util.isString(message.method)) return "method: string expected";
    return null;
  };
  /**
   * Creates an InvokeBegin message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof InvokeBegin
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {InvokeBegin} InvokeBegin
   */


  InvokeBegin.fromObject = function fromObject(object) {
    if (object instanceof $root.InvokeBegin) return object;
    var message = new $root.InvokeBegin();
    if (object.invokeId != null) message.invokeId = object.invokeId | 0;
    if (object.method != null) message.method = String(object.method);
    return message;
  };
  /**
   * Creates a plain object from an InvokeBegin message. Also converts values to other types if specified.
   * @function toObject
   * @memberof InvokeBegin
   * @static
   * @param {InvokeBegin} message InvokeBegin
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  InvokeBegin.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.invokeId = 0;
      object.method = "";
    }

    if (message.invokeId != null && message.hasOwnProperty("invokeId")) object.invokeId = message.invokeId;
    if (message.method != null && message.hasOwnProperty("method")) object.method = message.method;
    return object;
  };
  /**
   * Converts this InvokeBegin to JSON.
   * @function toJSON
   * @memberof InvokeBegin
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  InvokeBegin.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return InvokeBegin;
}();

$root.InvokeEnd = function () {
  /**
   * Properties of an InvokeEnd.
   * @exports IInvokeEnd
   * @interface IInvokeEnd
   * @property {number|null} [invokeId] InvokeEnd invokeId
   * @property {number|null} [status] InvokeEnd status
   * @property {string|null} [error] InvokeEnd error
   */

  /**
   * Constructs a new InvokeEnd.
   * @exports InvokeEnd
   * @classdesc Represents an InvokeEnd.
   * @implements IInvokeEnd
   * @constructor
   * @param {IInvokeEnd=} [properties] Properties to set
   */
  function InvokeEnd(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * InvokeEnd invokeId.
   * @member {number} invokeId
   * @memberof InvokeEnd
   * @instance
   */


  InvokeEnd.prototype.invokeId = 0;
  /**
   * InvokeEnd status.
   * @member {number} status
   * @memberof InvokeEnd
   * @instance
   */

  InvokeEnd.prototype.status = 0;
  /**
   * InvokeEnd error.
   * @member {string} error
   * @memberof InvokeEnd
   * @instance
   */

  InvokeEnd.prototype.error = "";
  /**
   * Creates a new InvokeEnd instance using the specified properties.
   * @function create
   * @memberof InvokeEnd
   * @static
   * @param {IInvokeEnd=} [properties] Properties to set
   * @returns {InvokeEnd} InvokeEnd instance
   */

  InvokeEnd.create = function create(properties) {
    return new InvokeEnd(properties);
  };
  /**
   * Encodes the specified InvokeEnd message. Does not implicitly {@link InvokeEnd.verify|verify} messages.
   * @function encode
   * @memberof InvokeEnd
   * @static
   * @param {IInvokeEnd} message InvokeEnd message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  InvokeEnd.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.invokeId != null && Object.hasOwnProperty.call(message, "invokeId")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.invokeId);
    if (message.status != null && Object.hasOwnProperty.call(message, "status")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int32(message.status);
    if (message.error != null && Object.hasOwnProperty.call(message, "error")) writer.uint32(
    /* id 3, wireType 2 =*/
    26).string(message.error);
    return writer;
  };
  /**
   * Encodes the specified InvokeEnd message, length delimited. Does not implicitly {@link InvokeEnd.verify|verify} messages.
   * @function encodeDelimited
   * @memberof InvokeEnd
   * @static
   * @param {IInvokeEnd} message InvokeEnd message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  InvokeEnd.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes an InvokeEnd message from the specified reader or buffer.
   * @function decode
   * @memberof InvokeEnd
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {InvokeEnd} InvokeEnd
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  InvokeEnd.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.InvokeEnd();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.invokeId = reader.int32();
          break;

        case 2:
          message.status = reader.int32();
          break;

        case 3:
          message.error = reader.string();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes an InvokeEnd message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof InvokeEnd
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {InvokeEnd} InvokeEnd
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  InvokeEnd.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies an InvokeEnd message.
   * @function verify
   * @memberof InvokeEnd
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  InvokeEnd.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.invokeId != null && message.hasOwnProperty("invokeId")) if (!$util.isInteger(message.invokeId)) return "invokeId: integer expected";
    if (message.status != null && message.hasOwnProperty("status")) if (!$util.isInteger(message.status)) return "status: integer expected";
    if (message.error != null && message.hasOwnProperty("error")) if (!$util.isString(message.error)) return "error: string expected";
    return null;
  };
  /**
   * Creates an InvokeEnd message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof InvokeEnd
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {InvokeEnd} InvokeEnd
   */


  InvokeEnd.fromObject = function fromObject(object) {
    if (object instanceof $root.InvokeEnd) return object;
    var message = new $root.InvokeEnd();
    if (object.invokeId != null) message.invokeId = object.invokeId | 0;
    if (object.status != null) message.status = object.status | 0;
    if (object.error != null) message.error = String(object.error);
    return message;
  };
  /**
   * Creates a plain object from an InvokeEnd message. Also converts values to other types if specified.
   * @function toObject
   * @memberof InvokeEnd
   * @static
   * @param {InvokeEnd} message InvokeEnd
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  InvokeEnd.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.invokeId = 0;
      object.status = 0;
      object.error = "";
    }

    if (message.invokeId != null && message.hasOwnProperty("invokeId")) object.invokeId = message.invokeId;
    if (message.status != null && message.hasOwnProperty("status")) object.status = message.status;
    if (message.error != null && message.hasOwnProperty("error")) object.error = message.error;
    return object;
  };
  /**
   * Converts this InvokeEnd to JSON.
   * @function toJSON
   * @memberof InvokeEnd
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  InvokeEnd.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return InvokeEnd;
}();

$root.CloseConnection = function () {
  /**
   * Properties of a CloseConnection.
   * @exports ICloseConnection
   * @interface ICloseConnection
   * @property {number|null} [code] CloseConnection code
   * @property {string|null} [reason] CloseConnection reason
   * @property {boolean|null} [allowReconnect] CloseConnection allowReconnect
   */

  /**
   * Constructs a new CloseConnection.
   * @exports CloseConnection
   * @classdesc Represents a CloseConnection.
   * @implements ICloseConnection
   * @constructor
   * @param {ICloseConnection=} [properties] Properties to set
   */
  function CloseConnection(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * CloseConnection code.
   * @member {number} code
   * @memberof CloseConnection
   * @instance
   */


  CloseConnection.prototype.code = 0;
  /**
   * CloseConnection reason.
   * @member {string} reason
   * @memberof CloseConnection
   * @instance
   */

  CloseConnection.prototype.reason = "";
  /**
   * CloseConnection allowReconnect.
   * @member {boolean} allowReconnect
   * @memberof CloseConnection
   * @instance
   */

  CloseConnection.prototype.allowReconnect = false;
  /**
   * Creates a new CloseConnection instance using the specified properties.
   * @function create
   * @memberof CloseConnection
   * @static
   * @param {ICloseConnection=} [properties] Properties to set
   * @returns {CloseConnection} CloseConnection instance
   */

  CloseConnection.create = function create(properties) {
    return new CloseConnection(properties);
  };
  /**
   * Encodes the specified CloseConnection message. Does not implicitly {@link CloseConnection.verify|verify} messages.
   * @function encode
   * @memberof CloseConnection
   * @static
   * @param {ICloseConnection} message CloseConnection message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CloseConnection.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.code != null && Object.hasOwnProperty.call(message, "code")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.code);
    if (message.reason != null && Object.hasOwnProperty.call(message, "reason")) writer.uint32(
    /* id 2, wireType 2 =*/
    18).string(message.reason);
    if (message.allowReconnect != null && Object.hasOwnProperty.call(message, "allowReconnect")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).bool(message.allowReconnect);
    return writer;
  };
  /**
   * Encodes the specified CloseConnection message, length delimited. Does not implicitly {@link CloseConnection.verify|verify} messages.
   * @function encodeDelimited
   * @memberof CloseConnection
   * @static
   * @param {ICloseConnection} message CloseConnection message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CloseConnection.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a CloseConnection message from the specified reader or buffer.
   * @function decode
   * @memberof CloseConnection
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {CloseConnection} CloseConnection
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CloseConnection.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.CloseConnection();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.code = reader.int32();
          break;

        case 2:
          message.reason = reader.string();
          break;

        case 3:
          message.allowReconnect = reader.bool();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a CloseConnection message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof CloseConnection
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {CloseConnection} CloseConnection
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CloseConnection.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a CloseConnection message.
   * @function verify
   * @memberof CloseConnection
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  CloseConnection.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.code != null && message.hasOwnProperty("code")) if (!$util.isInteger(message.code)) return "code: integer expected";
    if (message.reason != null && message.hasOwnProperty("reason")) if (!$util.isString(message.reason)) return "reason: string expected";
    if (message.allowReconnect != null && message.hasOwnProperty("allowReconnect")) if (typeof message.allowReconnect !== "boolean") return "allowReconnect: boolean expected";
    return null;
  };
  /**
   * Creates a CloseConnection message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof CloseConnection
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {CloseConnection} CloseConnection
   */


  CloseConnection.fromObject = function fromObject(object) {
    if (object instanceof $root.CloseConnection) return object;
    var message = new $root.CloseConnection();
    if (object.code != null) message.code = object.code | 0;
    if (object.reason != null) message.reason = String(object.reason);
    if (object.allowReconnect != null) message.allowReconnect = Boolean(object.allowReconnect);
    return message;
  };
  /**
   * Creates a plain object from a CloseConnection message. Also converts values to other types if specified.
   * @function toObject
   * @memberof CloseConnection
   * @static
   * @param {CloseConnection} message CloseConnection
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  CloseConnection.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.code = 0;
      object.reason = "";
      object.allowReconnect = false;
    }

    if (message.code != null && message.hasOwnProperty("code")) object.code = message.code;
    if (message.reason != null && message.hasOwnProperty("reason")) object.reason = message.reason;
    if (message.allowReconnect != null && message.hasOwnProperty("allowReconnect")) object.allowReconnect = message.allowReconnect;
    return object;
  };
  /**
   * Converts this CloseConnection to JSON.
   * @function toJSON
   * @memberof CloseConnection
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  CloseConnection.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return CloseConnection;
}();

$root.Envelope = function () {
  /**
   * Properties of an Envelope.
   * @exports IEnvelope
   * @interface IEnvelope
   * @property {Envelope.IHeader|null} [header] Envelope header
   * @property {Uint8Array|null} [payload] Envelope payload
   */

  /**
   * Constructs a new Envelope.
   * @exports Envelope
   * @classdesc Represents an Envelope.
   * @implements IEnvelope
   * @constructor
   * @param {IEnvelope=} [properties] Properties to set
   */
  function Envelope(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * Envelope header.
   * @member {Envelope.IHeader|null|undefined} header
   * @memberof Envelope
   * @instance
   */


  Envelope.prototype.header = null;
  /**
   * Envelope payload.
   * @member {Uint8Array} payload
   * @memberof Envelope
   * @instance
   */

  Envelope.prototype.payload = $util.newBuffer([]);
  /**
   * Creates a new Envelope instance using the specified properties.
   * @function create
   * @memberof Envelope
   * @static
   * @param {IEnvelope=} [properties] Properties to set
   * @returns {Envelope} Envelope instance
   */

  Envelope.create = function create(properties) {
    return new Envelope(properties);
  };
  /**
   * Encodes the specified Envelope message. Does not implicitly {@link Envelope.verify|verify} messages.
   * @function encode
   * @memberof Envelope
   * @static
   * @param {IEnvelope} message Envelope message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  Envelope.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.header != null && Object.hasOwnProperty.call(message, "header")) $root.Envelope.Header.encode(message.header, writer.uint32(
    /* id 1, wireType 2 =*/
    10).fork()).ldelim();
    if (message.payload != null && Object.hasOwnProperty.call(message, "payload")) writer.uint32(
    /* id 2, wireType 2 =*/
    18).bytes(message.payload);
    return writer;
  };
  /**
   * Encodes the specified Envelope message, length delimited. Does not implicitly {@link Envelope.verify|verify} messages.
   * @function encodeDelimited
   * @memberof Envelope
   * @static
   * @param {IEnvelope} message Envelope message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  Envelope.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes an Envelope message from the specified reader or buffer.
   * @function decode
   * @memberof Envelope
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {Envelope} Envelope
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  Envelope.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Envelope();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.header = $root.Envelope.Header.decode(reader, reader.uint32());
          break;

        case 2:
          message.payload = reader.bytes();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes an Envelope message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof Envelope
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {Envelope} Envelope
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  Envelope.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies an Envelope message.
   * @function verify
   * @memberof Envelope
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  Envelope.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";

    if (message.header != null && message.hasOwnProperty("header")) {
      var error = $root.Envelope.Header.verify(message.header);
      if (error) return "header." + error;
    }

    if (message.payload != null && message.hasOwnProperty("payload")) if (!(message.payload && typeof message.payload.length === "number" || $util.isString(message.payload))) return "payload: buffer expected";
    return null;
  };
  /**
   * Creates an Envelope message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof Envelope
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {Envelope} Envelope
   */


  Envelope.fromObject = function fromObject(object) {
    if (object instanceof $root.Envelope) return object;
    var message = new $root.Envelope();

    if (object.header != null) {
      if (typeof object.header !== "object") throw TypeError(".Envelope.header: object expected");
      message.header = $root.Envelope.Header.fromObject(object.header);
    }

    if (object.payload != null) if (typeof object.payload === "string") $util.base64.decode(object.payload, message.payload = $util.newBuffer($util.base64.length(object.payload)), 0);else if (object.payload.length) message.payload = object.payload;
    return message;
  };
  /**
   * Creates a plain object from an Envelope message. Also converts values to other types if specified.
   * @function toObject
   * @memberof Envelope
   * @static
   * @param {Envelope} message Envelope
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  Envelope.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.header = null;
      if (options.bytes === String) object.payload = "";else {
        object.payload = [];
        if (options.bytes !== Array) object.payload = $util.newBuffer(object.payload);
      }
    }

    if (message.header != null && message.hasOwnProperty("header")) object.header = $root.Envelope.Header.toObject(message.header, options);
    if (message.payload != null && message.hasOwnProperty("payload")) object.payload = options.bytes === String ? $util.base64.encode(message.payload, 0, message.payload.length) : options.bytes === Array ? Array.prototype.slice.call(message.payload) : message.payload;
    return object;
  };
  /**
   * Converts this Envelope to JSON.
   * @function toJSON
   * @memberof Envelope
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  Envelope.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  Envelope.Header = function () {
    /**
     * Properties of a Header.
     * @memberof Envelope
     * @interface IHeader
     * @property {IInvokeBegin|null} [invokeBegin] Header invokeBegin
     * @property {IInvokeEnd|null} [invokeEnd] Header invokeEnd
     * @property {ICloseConnection|null} [closeConnection] Header closeConnection
     * @property {string|null} [event] Header event
     */

    /**
     * Constructs a new Header.
     * @memberof Envelope
     * @classdesc Represents a Header.
     * @implements IHeader
     * @constructor
     * @param {Envelope.IHeader=} [properties] Properties to set
     */
    function Header(properties) {
      if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
        if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
      }
    }
    /**
     * Header invokeBegin.
     * @member {IInvokeBegin|null|undefined} invokeBegin
     * @memberof Envelope.Header
     * @instance
     */


    Header.prototype.invokeBegin = null;
    /**
     * Header invokeEnd.
     * @member {IInvokeEnd|null|undefined} invokeEnd
     * @memberof Envelope.Header
     * @instance
     */

    Header.prototype.invokeEnd = null;
    /**
     * Header closeConnection.
     * @member {ICloseConnection|null|undefined} closeConnection
     * @memberof Envelope.Header
     * @instance
     */

    Header.prototype.closeConnection = null;
    /**
     * Header event.
     * @member {string|null|undefined} event
     * @memberof Envelope.Header
     * @instance
     */

    Header.prototype.event = null; // OneOf field names bound to virtual getters and setters

    var $oneOfFields;
    /**
     * Header kind.
     * @member {"invokeBegin"|"invokeEnd"|"closeConnection"|"event"|undefined} kind
     * @memberof Envelope.Header
     * @instance
     */

    Object.defineProperty(Header.prototype, "kind", {
      get: $util.oneOfGetter($oneOfFields = ["invokeBegin", "invokeEnd", "closeConnection", "event"]),
      set: $util.oneOfSetter($oneOfFields)
    });
    /**
     * Creates a new Header instance using the specified properties.
     * @function create
     * @memberof Envelope.Header
     * @static
     * @param {Envelope.IHeader=} [properties] Properties to set
     * @returns {Envelope.Header} Header instance
     */

    Header.create = function create(properties) {
      return new Header(properties);
    };
    /**
     * Encodes the specified Header message. Does not implicitly {@link Envelope.Header.verify|verify} messages.
     * @function encode
     * @memberof Envelope.Header
     * @static
     * @param {Envelope.IHeader} message Header message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Header.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.invokeBegin != null && Object.hasOwnProperty.call(message, "invokeBegin")) $root.InvokeBegin.encode(message.invokeBegin, writer.uint32(
      /* id 1, wireType 2 =*/
      10).fork()).ldelim();
      if (message.invokeEnd != null && Object.hasOwnProperty.call(message, "invokeEnd")) $root.InvokeEnd.encode(message.invokeEnd, writer.uint32(
      /* id 2, wireType 2 =*/
      18).fork()).ldelim();
      if (message.closeConnection != null && Object.hasOwnProperty.call(message, "closeConnection")) $root.CloseConnection.encode(message.closeConnection, writer.uint32(
      /* id 3, wireType 2 =*/
      26).fork()).ldelim();
      if (message.event != null && Object.hasOwnProperty.call(message, "event")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.event);
      return writer;
    };
    /**
     * Encodes the specified Header message, length delimited. Does not implicitly {@link Envelope.Header.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Envelope.Header
     * @static
     * @param {Envelope.IHeader} message Header message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Header.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Header message from the specified reader or buffer.
     * @function decode
     * @memberof Envelope.Header
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Envelope.Header} Header
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Header.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Envelope.Header();

      while (reader.pos < end) {
        var tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.invokeBegin = $root.InvokeBegin.decode(reader, reader.uint32());
            break;

          case 2:
            message.invokeEnd = $root.InvokeEnd.decode(reader, reader.uint32());
            break;

          case 3:
            message.closeConnection = $root.CloseConnection.decode(reader, reader.uint32());
            break;

          case 4:
            message.event = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Header message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Envelope.Header
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Envelope.Header} Header
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Header.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Header message.
     * @function verify
     * @memberof Envelope.Header
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Header.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      var properties = {};

      if (message.invokeBegin != null && message.hasOwnProperty("invokeBegin")) {
        properties.kind = 1;
        {
          var error = $root.InvokeBegin.verify(message.invokeBegin);
          if (error) return "invokeBegin." + error;
        }
      }

      if (message.invokeEnd != null && message.hasOwnProperty("invokeEnd")) {
        if (properties.kind === 1) return "kind: multiple values";
        properties.kind = 1;
        {
          var error = $root.InvokeEnd.verify(message.invokeEnd);
          if (error) return "invokeEnd." + error;
        }
      }

      if (message.closeConnection != null && message.hasOwnProperty("closeConnection")) {
        if (properties.kind === 1) return "kind: multiple values";
        properties.kind = 1;
        {
          var error = $root.CloseConnection.verify(message.closeConnection);
          if (error) return "closeConnection." + error;
        }
      }

      if (message.event != null && message.hasOwnProperty("event")) {
        if (properties.kind === 1) return "kind: multiple values";
        properties.kind = 1;
        if (!$util.isString(message.event)) return "event: string expected";
      }

      return null;
    };
    /**
     * Creates a Header message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Envelope.Header
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Envelope.Header} Header
     */


    Header.fromObject = function fromObject(object) {
      if (object instanceof $root.Envelope.Header) return object;
      var message = new $root.Envelope.Header();

      if (object.invokeBegin != null) {
        if (typeof object.invokeBegin !== "object") throw TypeError(".Envelope.Header.invokeBegin: object expected");
        message.invokeBegin = $root.InvokeBegin.fromObject(object.invokeBegin);
      }

      if (object.invokeEnd != null) {
        if (typeof object.invokeEnd !== "object") throw TypeError(".Envelope.Header.invokeEnd: object expected");
        message.invokeEnd = $root.InvokeEnd.fromObject(object.invokeEnd);
      }

      if (object.closeConnection != null) {
        if (typeof object.closeConnection !== "object") throw TypeError(".Envelope.Header.closeConnection: object expected");
        message.closeConnection = $root.CloseConnection.fromObject(object.closeConnection);
      }

      if (object.event != null) message.event = String(object.event);
      return message;
    };
    /**
     * Creates a plain object from a Header message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Envelope.Header
     * @static
     * @param {Envelope.Header} message Header
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Header.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};

      if (message.invokeBegin != null && message.hasOwnProperty("invokeBegin")) {
        object.invokeBegin = $root.InvokeBegin.toObject(message.invokeBegin, options);
        if (options.oneofs) object.kind = "invokeBegin";
      }

      if (message.invokeEnd != null && message.hasOwnProperty("invokeEnd")) {
        object.invokeEnd = $root.InvokeEnd.toObject(message.invokeEnd, options);
        if (options.oneofs) object.kind = "invokeEnd";
      }

      if (message.closeConnection != null && message.hasOwnProperty("closeConnection")) {
        object.closeConnection = $root.CloseConnection.toObject(message.closeConnection, options);
        if (options.oneofs) object.kind = "closeConnection";
      }

      if (message.event != null && message.hasOwnProperty("event")) {
        object.event = message.event;
        if (options.oneofs) object.kind = "event";
      }

      return object;
    };
    /**
     * Converts this Header to JSON.
     * @function toJSON
     * @memberof Envelope.Header
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Header.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Header;
  }();

  return Envelope;
}();

$root.GameServer = function () {
  /**
   * Constructs a new GameServer service.
   * @exports GameServer
   * @classdesc Represents a GameServer
   * @extends $protobuf.rpc.Service
   * @constructor
   * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
   * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
   * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
   */
  function GameServer(rpcImpl, requestDelimited, responseDelimited) {
    $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
  }

  (GameServer.prototype = Object.create($protobuf.rpc.Service.prototype)).constructor = GameServer;
  /**
   * Creates new GameServer service using the specified rpc implementation.
   * @function create
   * @memberof GameServer
   * @static
   * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
   * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
   * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
   * @returns {GameServer} RPC service. Useful where requests and/or responses are streamed.
   */

  GameServer.create = function create(rpcImpl, requestDelimited, responseDelimited) {
    return new this(rpcImpl, requestDelimited, responseDelimited);
  };
  /**
   * Callback as used by {@link GameServer#login}.
   * @memberof GameServer
   * @typedef LoginCallback
   * @type {function}
   * @param {Error|null} error Error, if any
   * @param {LoginReply} [response] LoginReply
   */

  /**
   * Calls Login.
   * @function login
   * @memberof GameServer
   * @instance
   * @param {ILoginRequest} request LoginRequest message or plain object
   * @param {GameServer.LoginCallback} callback Node-style callback called with the error, if any, and LoginReply
   * @returns {undefined}
   * @variation 1
   */


  Object.defineProperty(GameServer.prototype.login = function login(request, callback) {
    return this.rpcCall(login, $root.LoginRequest, $root.LoginReply, request, callback);
  }, "name", {
    value: "Login"
  });
  /**
   * Calls Login.
   * @function login
   * @memberof GameServer
   * @instance
   * @param {ILoginRequest} request LoginRequest message or plain object
   * @returns {Promise<LoginReply>} Promise
   * @variation 2
   */

  /**
   * Callback as used by {@link GameServer#buyProduce}.
   * @memberof GameServer
   * @typedef BuyProduceCallback
   * @type {function}
   * @param {Error|null} error Error, if any
   * @param {BuyProduceReply} [response] BuyProduceReply
   */

  /**
   * Calls BuyProduce.
   * @function buyProduce
   * @memberof GameServer
   * @instance
   * @param {IBuyProduceRequest} request BuyProduceRequest message or plain object
   * @param {GameServer.BuyProduceCallback} callback Node-style callback called with the error, if any, and BuyProduceReply
   * @returns {undefined}
   * @variation 1
   */

  Object.defineProperty(GameServer.prototype.buyProduce = function buyProduce(request, callback) {
    return this.rpcCall(buyProduce, $root.BuyProduceRequest, $root.BuyProduceReply, request, callback);
  }, "name", {
    value: "BuyProduce"
  });
  /**
   * Calls BuyProduce.
   * @function buyProduce
   * @memberof GameServer
   * @instance
   * @param {IBuyProduceRequest} request BuyProduceRequest message or plain object
   * @returns {Promise<BuyProduceReply>} Promise
   * @variation 2
   */

  /**
   * Callback as used by {@link GameServer#combineProduce}.
   * @memberof GameServer
   * @typedef CombineProduceCallback
   * @type {function}
   * @param {Error|null} error Error, if any
   * @param {CombineProduceReply} [response] CombineProduceReply
   */

  /**
   * Calls CombineProduce.
   * @function combineProduce
   * @memberof GameServer
   * @instance
   * @param {ICombineProduceRequest} request CombineProduceRequest message or plain object
   * @param {GameServer.CombineProduceCallback} callback Node-style callback called with the error, if any, and CombineProduceReply
   * @returns {undefined}
   * @variation 1
   */

  Object.defineProperty(GameServer.prototype.combineProduce = function combineProduce(request, callback) {
    return this.rpcCall(combineProduce, $root.CombineProduceRequest, $root.CombineProduceReply, request, callback);
  }, "name", {
    value: "CombineProduce"
  });
  /**
   * Calls CombineProduce.
   * @function combineProduce
   * @memberof GameServer
   * @instance
   * @param {ICombineProduceRequest} request CombineProduceRequest message or plain object
   * @returns {Promise<CombineProduceReply>} Promise
   * @variation 2
   */

  /**
   * Callback as used by {@link GameServer#refreshWorkBench}.
   * @memberof GameServer
   * @typedef RefreshWorkBenchCallback
   * @type {function}
   * @param {Error|null} error Error, if any
   * @param {RefreshWorkBenchReply} [response] RefreshWorkBenchReply
   */

  /**
   * Calls RefreshWorkBench.
   * @function refreshWorkBench
   * @memberof GameServer
   * @instance
   * @param {IRefreshWorkBenchRequest} request RefreshWorkBenchRequest message or plain object
   * @param {GameServer.RefreshWorkBenchCallback} callback Node-style callback called with the error, if any, and RefreshWorkBenchReply
   * @returns {undefined}
   * @variation 1
   */

  Object.defineProperty(GameServer.prototype.refreshWorkBench = function refreshWorkBench(request, callback) {
    return this.rpcCall(refreshWorkBench, $root.RefreshWorkBenchRequest, $root.RefreshWorkBenchReply, request, callback);
  }, "name", {
    value: "RefreshWorkBench"
  });
  /**
   * Calls RefreshWorkBench.
   * @function refreshWorkBench
   * @memberof GameServer
   * @instance
   * @param {IRefreshWorkBenchRequest} request RefreshWorkBenchRequest message or plain object
   * @returns {Promise<RefreshWorkBenchReply>} Promise
   * @variation 2
   */

  return GameServer;
}();

$root.LoginRequest = function () {
  /**
   * Properties of a LoginRequest.
   * @exports ILoginRequest
   * @interface ILoginRequest
   * @property {string|null} [accessToken] LoginRequest accessToken
   */

  /**
   * Constructs a new LoginRequest.
   * @exports LoginRequest
   * @classdesc Represents a LoginRequest.
   * @implements ILoginRequest
   * @constructor
   * @param {ILoginRequest=} [properties] Properties to set
   */
  function LoginRequest(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * LoginRequest accessToken.
   * @member {string} accessToken
   * @memberof LoginRequest
   * @instance
   */


  LoginRequest.prototype.accessToken = "";
  /**
   * Creates a new LoginRequest instance using the specified properties.
   * @function create
   * @memberof LoginRequest
   * @static
   * @param {ILoginRequest=} [properties] Properties to set
   * @returns {LoginRequest} LoginRequest instance
   */

  LoginRequest.create = function create(properties) {
    return new LoginRequest(properties);
  };
  /**
   * Encodes the specified LoginRequest message. Does not implicitly {@link LoginRequest.verify|verify} messages.
   * @function encode
   * @memberof LoginRequest
   * @static
   * @param {ILoginRequest} message LoginRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  LoginRequest.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.accessToken != null && Object.hasOwnProperty.call(message, "accessToken")) writer.uint32(
    /* id 1, wireType 2 =*/
    10).string(message.accessToken);
    return writer;
  };
  /**
   * Encodes the specified LoginRequest message, length delimited. Does not implicitly {@link LoginRequest.verify|verify} messages.
   * @function encodeDelimited
   * @memberof LoginRequest
   * @static
   * @param {ILoginRequest} message LoginRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  LoginRequest.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a LoginRequest message from the specified reader or buffer.
   * @function decode
   * @memberof LoginRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {LoginRequest} LoginRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  LoginRequest.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.LoginRequest();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.accessToken = reader.string();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a LoginRequest message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof LoginRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {LoginRequest} LoginRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  LoginRequest.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a LoginRequest message.
   * @function verify
   * @memberof LoginRequest
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  LoginRequest.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.accessToken != null && message.hasOwnProperty("accessToken")) if (!$util.isString(message.accessToken)) return "accessToken: string expected";
    return null;
  };
  /**
   * Creates a LoginRequest message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof LoginRequest
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {LoginRequest} LoginRequest
   */


  LoginRequest.fromObject = function fromObject(object) {
    if (object instanceof $root.LoginRequest) return object;
    var message = new $root.LoginRequest();
    if (object.accessToken != null) message.accessToken = String(object.accessToken);
    return message;
  };
  /**
   * Creates a plain object from a LoginRequest message. Also converts values to other types if specified.
   * @function toObject
   * @memberof LoginRequest
   * @static
   * @param {LoginRequest} message LoginRequest
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  LoginRequest.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};
    if (options.defaults) object.accessToken = "";
    if (message.accessToken != null && message.hasOwnProperty("accessToken")) object.accessToken = message.accessToken;
    return object;
  };
  /**
   * Converts this LoginRequest to JSON.
   * @function toJSON
   * @memberof LoginRequest
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  LoginRequest.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return LoginRequest;
}();

$root.LoginReply = function () {
  /**
   * Properties of a LoginReply.
   * @exports ILoginReply
   * @interface ILoginReply
   * @property {IUserInfo|null} [userInfo] LoginReply userInfo
   * @property {number|null} [serverTime] LoginReply serverTime
   * @property {number|Long|null} [coin] LoginReply coin
   * @property {number|Long|null} [diamond] LoginReply diamond
   * @property {Array.<IWorkBenchItemInfo>|null} [workbenchItems] LoginReply workbenchItems
   */

  /**
   * Constructs a new LoginReply.
   * @exports LoginReply
   * @classdesc Represents a LoginReply.
   * @implements ILoginReply
   * @constructor
   * @param {ILoginReply=} [properties] Properties to set
   */
  function LoginReply(properties) {
    this.workbenchItems = [];
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * LoginReply userInfo.
   * @member {IUserInfo|null|undefined} userInfo
   * @memberof LoginReply
   * @instance
   */


  LoginReply.prototype.userInfo = null;
  /**
   * LoginReply serverTime.
   * @member {number} serverTime
   * @memberof LoginReply
   * @instance
   */

  LoginReply.prototype.serverTime = 0;
  /**
   * LoginReply coin.
   * @member {number|Long} coin
   * @memberof LoginReply
   * @instance
   */

  LoginReply.prototype.coin = $util.Long ? $util.Long.fromBits(0, 0, true) : 0;
  /**
   * LoginReply diamond.
   * @member {number|Long} diamond
   * @memberof LoginReply
   * @instance
   */

  LoginReply.prototype.diamond = $util.Long ? $util.Long.fromBits(0, 0, true) : 0;
  /**
   * LoginReply workbenchItems.
   * @member {Array.<IWorkBenchItemInfo>} workbenchItems
   * @memberof LoginReply
   * @instance
   */

  LoginReply.prototype.workbenchItems = $util.emptyArray;
  /**
   * Creates a new LoginReply instance using the specified properties.
   * @function create
   * @memberof LoginReply
   * @static
   * @param {ILoginReply=} [properties] Properties to set
   * @returns {LoginReply} LoginReply instance
   */

  LoginReply.create = function create(properties) {
    return new LoginReply(properties);
  };
  /**
   * Encodes the specified LoginReply message. Does not implicitly {@link LoginReply.verify|verify} messages.
   * @function encode
   * @memberof LoginReply
   * @static
   * @param {ILoginReply} message LoginReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  LoginReply.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.userInfo != null && Object.hasOwnProperty.call(message, "userInfo")) $root.UserInfo.encode(message.userInfo, writer.uint32(
    /* id 1, wireType 2 =*/
    10).fork()).ldelim();
    if (message.serverTime != null && Object.hasOwnProperty.call(message, "serverTime")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).uint32(message.serverTime);
    if (message.coin != null && Object.hasOwnProperty.call(message, "coin")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).uint64(message.coin);
    if (message.diamond != null && Object.hasOwnProperty.call(message, "diamond")) writer.uint32(
    /* id 4, wireType 0 =*/
    32).uint64(message.diamond);
    if (message.workbenchItems != null && message.workbenchItems.length) for (var i = 0; i < message.workbenchItems.length; ++i) {
      $root.WorkBenchItemInfo.encode(message.workbenchItems[i], writer.uint32(
      /* id 5, wireType 2 =*/
      42).fork()).ldelim();
    }
    return writer;
  };
  /**
   * Encodes the specified LoginReply message, length delimited. Does not implicitly {@link LoginReply.verify|verify} messages.
   * @function encodeDelimited
   * @memberof LoginReply
   * @static
   * @param {ILoginReply} message LoginReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  LoginReply.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a LoginReply message from the specified reader or buffer.
   * @function decode
   * @memberof LoginReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {LoginReply} LoginReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  LoginReply.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.LoginReply();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.userInfo = $root.UserInfo.decode(reader, reader.uint32());
          break;

        case 2:
          message.serverTime = reader.uint32();
          break;

        case 3:
          message.coin = reader.uint64();
          break;

        case 4:
          message.diamond = reader.uint64();
          break;

        case 5:
          if (!(message.workbenchItems && message.workbenchItems.length)) message.workbenchItems = [];
          message.workbenchItems.push($root.WorkBenchItemInfo.decode(reader, reader.uint32()));
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a LoginReply message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof LoginReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {LoginReply} LoginReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  LoginReply.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a LoginReply message.
   * @function verify
   * @memberof LoginReply
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  LoginReply.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";

    if (message.userInfo != null && message.hasOwnProperty("userInfo")) {
      var error = $root.UserInfo.verify(message.userInfo);
      if (error) return "userInfo." + error;
    }

    if (message.serverTime != null && message.hasOwnProperty("serverTime")) if (!$util.isInteger(message.serverTime)) return "serverTime: integer expected";
    if (message.coin != null && message.hasOwnProperty("coin")) if (!$util.isInteger(message.coin) && !(message.coin && $util.isInteger(message.coin.low) && $util.isInteger(message.coin.high))) return "coin: integer|Long expected";
    if (message.diamond != null && message.hasOwnProperty("diamond")) if (!$util.isInteger(message.diamond) && !(message.diamond && $util.isInteger(message.diamond.low) && $util.isInteger(message.diamond.high))) return "diamond: integer|Long expected";

    if (message.workbenchItems != null && message.hasOwnProperty("workbenchItems")) {
      if (!Array.isArray(message.workbenchItems)) return "workbenchItems: array expected";

      for (var i = 0; i < message.workbenchItems.length; ++i) {
        var error = $root.WorkBenchItemInfo.verify(message.workbenchItems[i]);
        if (error) return "workbenchItems." + error;
      }
    }

    return null;
  };
  /**
   * Creates a LoginReply message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof LoginReply
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {LoginReply} LoginReply
   */


  LoginReply.fromObject = function fromObject(object) {
    if (object instanceof $root.LoginReply) return object;
    var message = new $root.LoginReply();

    if (object.userInfo != null) {
      if (typeof object.userInfo !== "object") throw TypeError(".LoginReply.userInfo: object expected");
      message.userInfo = $root.UserInfo.fromObject(object.userInfo);
    }

    if (object.serverTime != null) message.serverTime = object.serverTime >>> 0;
    if (object.coin != null) if ($util.Long) (message.coin = $util.Long.fromValue(object.coin)).unsigned = true;else if (typeof object.coin === "string") message.coin = parseInt(object.coin, 10);else if (typeof object.coin === "number") message.coin = object.coin;else if (typeof object.coin === "object") message.coin = new $util.LongBits(object.coin.low >>> 0, object.coin.high >>> 0).toNumber(true);
    if (object.diamond != null) if ($util.Long) (message.diamond = $util.Long.fromValue(object.diamond)).unsigned = true;else if (typeof object.diamond === "string") message.diamond = parseInt(object.diamond, 10);else if (typeof object.diamond === "number") message.diamond = object.diamond;else if (typeof object.diamond === "object") message.diamond = new $util.LongBits(object.diamond.low >>> 0, object.diamond.high >>> 0).toNumber(true);

    if (object.workbenchItems) {
      if (!Array.isArray(object.workbenchItems)) throw TypeError(".LoginReply.workbenchItems: array expected");
      message.workbenchItems = [];

      for (var i = 0; i < object.workbenchItems.length; ++i) {
        if (typeof object.workbenchItems[i] !== "object") throw TypeError(".LoginReply.workbenchItems: object expected");
        message.workbenchItems[i] = $root.WorkBenchItemInfo.fromObject(object.workbenchItems[i]);
      }
    }

    return message;
  };
  /**
   * Creates a plain object from a LoginReply message. Also converts values to other types if specified.
   * @function toObject
   * @memberof LoginReply
   * @static
   * @param {LoginReply} message LoginReply
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  LoginReply.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};
    if (options.arrays || options.defaults) object.workbenchItems = [];

    if (options.defaults) {
      object.userInfo = null;
      object.serverTime = 0;

      if ($util.Long) {
        var _long = new $util.Long(0, 0, true);

        object.coin = options.longs === String ? _long.toString() : options.longs === Number ? _long.toNumber() : _long;
      } else object.coin = options.longs === String ? "0" : 0;

      if ($util.Long) {
        var _long = new $util.Long(0, 0, true);

        object.diamond = options.longs === String ? _long.toString() : options.longs === Number ? _long.toNumber() : _long;
      } else object.diamond = options.longs === String ? "0" : 0;
    }

    if (message.userInfo != null && message.hasOwnProperty("userInfo")) object.userInfo = $root.UserInfo.toObject(message.userInfo, options);
    if (message.serverTime != null && message.hasOwnProperty("serverTime")) object.serverTime = message.serverTime;
    if (message.coin != null && message.hasOwnProperty("coin")) if (typeof message.coin === "number") object.coin = options.longs === String ? String(message.coin) : message.coin;else object.coin = options.longs === String ? $util.Long.prototype.toString.call(message.coin) : options.longs === Number ? new $util.LongBits(message.coin.low >>> 0, message.coin.high >>> 0).toNumber(true) : message.coin;
    if (message.diamond != null && message.hasOwnProperty("diamond")) if (typeof message.diamond === "number") object.diamond = options.longs === String ? String(message.diamond) : message.diamond;else object.diamond = options.longs === String ? $util.Long.prototype.toString.call(message.diamond) : options.longs === Number ? new $util.LongBits(message.diamond.low >>> 0, message.diamond.high >>> 0).toNumber(true) : message.diamond;

    if (message.workbenchItems && message.workbenchItems.length) {
      object.workbenchItems = [];

      for (var j = 0; j < message.workbenchItems.length; ++j) {
        object.workbenchItems[j] = $root.WorkBenchItemInfo.toObject(message.workbenchItems[j], options);
      }
    }

    return object;
  };
  /**
   * Converts this LoginReply to JSON.
   * @function toJSON
   * @memberof LoginReply
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  LoginReply.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return LoginReply;
}();

$root.BuyProduceRequest = function () {
  /**
   * Properties of a BuyProduceRequest.
   * @exports IBuyProduceRequest
   * @interface IBuyProduceRequest
   * @property {number|null} [produceLevel] BuyProduceRequest produceLevel
   */

  /**
   * Constructs a new BuyProduceRequest.
   * @exports BuyProduceRequest
   * @classdesc Represents a BuyProduceRequest.
   * @implements IBuyProduceRequest
   * @constructor
   * @param {IBuyProduceRequest=} [properties] Properties to set
   */
  function BuyProduceRequest(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * BuyProduceRequest produceLevel.
   * @member {number} produceLevel
   * @memberof BuyProduceRequest
   * @instance
   */


  BuyProduceRequest.prototype.produceLevel = 0;
  /**
   * Creates a new BuyProduceRequest instance using the specified properties.
   * @function create
   * @memberof BuyProduceRequest
   * @static
   * @param {IBuyProduceRequest=} [properties] Properties to set
   * @returns {BuyProduceRequest} BuyProduceRequest instance
   */

  BuyProduceRequest.create = function create(properties) {
    return new BuyProduceRequest(properties);
  };
  /**
   * Encodes the specified BuyProduceRequest message. Does not implicitly {@link BuyProduceRequest.verify|verify} messages.
   * @function encode
   * @memberof BuyProduceRequest
   * @static
   * @param {IBuyProduceRequest} message BuyProduceRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  BuyProduceRequest.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.produceLevel != null && Object.hasOwnProperty.call(message, "produceLevel")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.produceLevel);
    return writer;
  };
  /**
   * Encodes the specified BuyProduceRequest message, length delimited. Does not implicitly {@link BuyProduceRequest.verify|verify} messages.
   * @function encodeDelimited
   * @memberof BuyProduceRequest
   * @static
   * @param {IBuyProduceRequest} message BuyProduceRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  BuyProduceRequest.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a BuyProduceRequest message from the specified reader or buffer.
   * @function decode
   * @memberof BuyProduceRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {BuyProduceRequest} BuyProduceRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  BuyProduceRequest.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.BuyProduceRequest();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.produceLevel = reader.int32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a BuyProduceRequest message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof BuyProduceRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {BuyProduceRequest} BuyProduceRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  BuyProduceRequest.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a BuyProduceRequest message.
   * @function verify
   * @memberof BuyProduceRequest
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  BuyProduceRequest.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) if (!$util.isInteger(message.produceLevel)) return "produceLevel: integer expected";
    return null;
  };
  /**
   * Creates a BuyProduceRequest message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof BuyProduceRequest
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {BuyProduceRequest} BuyProduceRequest
   */


  BuyProduceRequest.fromObject = function fromObject(object) {
    if (object instanceof $root.BuyProduceRequest) return object;
    var message = new $root.BuyProduceRequest();
    if (object.produceLevel != null) message.produceLevel = object.produceLevel | 0;
    return message;
  };
  /**
   * Creates a plain object from a BuyProduceRequest message. Also converts values to other types if specified.
   * @function toObject
   * @memberof BuyProduceRequest
   * @static
   * @param {BuyProduceRequest} message BuyProduceRequest
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  BuyProduceRequest.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};
    if (options.defaults) object.produceLevel = 0;
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) object.produceLevel = message.produceLevel;
    return object;
  };
  /**
   * Converts this BuyProduceRequest to JSON.
   * @function toJSON
   * @memberof BuyProduceRequest
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  BuyProduceRequest.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return BuyProduceRequest;
}();

$root.BuyProduceReply = function () {
  /**
   * Properties of a BuyProduceReply.
   * @exports IBuyProduceReply
   * @interface IBuyProduceReply
   * @property {number|null} [errcode] BuyProduceReply errcode
   * @property {number|null} [produceLevel] BuyProduceReply produceLevel
   * @property {number|null} [position] BuyProduceReply position
   */

  /**
   * Constructs a new BuyProduceReply.
   * @exports BuyProduceReply
   * @classdesc Represents a BuyProduceReply.
   * @implements IBuyProduceReply
   * @constructor
   * @param {IBuyProduceReply=} [properties] Properties to set
   */
  function BuyProduceReply(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * BuyProduceReply errcode.
   * @member {number} errcode
   * @memberof BuyProduceReply
   * @instance
   */


  BuyProduceReply.prototype.errcode = 0;
  /**
   * BuyProduceReply produceLevel.
   * @member {number} produceLevel
   * @memberof BuyProduceReply
   * @instance
   */

  BuyProduceReply.prototype.produceLevel = 0;
  /**
   * BuyProduceReply position.
   * @member {number} position
   * @memberof BuyProduceReply
   * @instance
   */

  BuyProduceReply.prototype.position = 0;
  /**
   * Creates a new BuyProduceReply instance using the specified properties.
   * @function create
   * @memberof BuyProduceReply
   * @static
   * @param {IBuyProduceReply=} [properties] Properties to set
   * @returns {BuyProduceReply} BuyProduceReply instance
   */

  BuyProduceReply.create = function create(properties) {
    return new BuyProduceReply(properties);
  };
  /**
   * Encodes the specified BuyProduceReply message. Does not implicitly {@link BuyProduceReply.verify|verify} messages.
   * @function encode
   * @memberof BuyProduceReply
   * @static
   * @param {IBuyProduceReply} message BuyProduceReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  BuyProduceReply.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.errcode != null && Object.hasOwnProperty.call(message, "errcode")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.errcode);
    if (message.produceLevel != null && Object.hasOwnProperty.call(message, "produceLevel")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int32(message.produceLevel);
    if (message.position != null && Object.hasOwnProperty.call(message, "position")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).int32(message.position);
    return writer;
  };
  /**
   * Encodes the specified BuyProduceReply message, length delimited. Does not implicitly {@link BuyProduceReply.verify|verify} messages.
   * @function encodeDelimited
   * @memberof BuyProduceReply
   * @static
   * @param {IBuyProduceReply} message BuyProduceReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  BuyProduceReply.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a BuyProduceReply message from the specified reader or buffer.
   * @function decode
   * @memberof BuyProduceReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {BuyProduceReply} BuyProduceReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  BuyProduceReply.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.BuyProduceReply();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.errcode = reader.int32();
          break;

        case 2:
          message.produceLevel = reader.int32();
          break;

        case 3:
          message.position = reader.int32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a BuyProduceReply message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof BuyProduceReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {BuyProduceReply} BuyProduceReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  BuyProduceReply.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a BuyProduceReply message.
   * @function verify
   * @memberof BuyProduceReply
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  BuyProduceReply.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.errcode != null && message.hasOwnProperty("errcode")) if (!$util.isInteger(message.errcode)) return "errcode: integer expected";
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) if (!$util.isInteger(message.produceLevel)) return "produceLevel: integer expected";
    if (message.position != null && message.hasOwnProperty("position")) if (!$util.isInteger(message.position)) return "position: integer expected";
    return null;
  };
  /**
   * Creates a BuyProduceReply message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof BuyProduceReply
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {BuyProduceReply} BuyProduceReply
   */


  BuyProduceReply.fromObject = function fromObject(object) {
    if (object instanceof $root.BuyProduceReply) return object;
    var message = new $root.BuyProduceReply();
    if (object.errcode != null) message.errcode = object.errcode | 0;
    if (object.produceLevel != null) message.produceLevel = object.produceLevel | 0;
    if (object.position != null) message.position = object.position | 0;
    return message;
  };
  /**
   * Creates a plain object from a BuyProduceReply message. Also converts values to other types if specified.
   * @function toObject
   * @memberof BuyProduceReply
   * @static
   * @param {BuyProduceReply} message BuyProduceReply
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  BuyProduceReply.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.errcode = 0;
      object.produceLevel = 0;
      object.position = 0;
    }

    if (message.errcode != null && message.hasOwnProperty("errcode")) object.errcode = message.errcode;
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) object.produceLevel = message.produceLevel;
    if (message.position != null && message.hasOwnProperty("position")) object.position = message.position;
    return object;
  };
  /**
   * Converts this BuyProduceReply to JSON.
   * @function toJSON
   * @memberof BuyProduceReply
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  BuyProduceReply.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return BuyProduceReply;
}();

$root.CombineProduceRequest = function () {
  /**
   * Properties of a CombineProduceRequest.
   * @exports ICombineProduceRequest
   * @interface ICombineProduceRequest
   * @property {number|null} [positionBase] CombineProduceRequest positionBase
   * @property {number|null} [positionTarget] CombineProduceRequest positionTarget
   */

  /**
   * Constructs a new CombineProduceRequest.
   * @exports CombineProduceRequest
   * @classdesc Represents a CombineProduceRequest.
   * @implements ICombineProduceRequest
   * @constructor
   * @param {ICombineProduceRequest=} [properties] Properties to set
   */
  function CombineProduceRequest(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * CombineProduceRequest positionBase.
   * @member {number} positionBase
   * @memberof CombineProduceRequest
   * @instance
   */


  CombineProduceRequest.prototype.positionBase = 0;
  /**
   * CombineProduceRequest positionTarget.
   * @member {number} positionTarget
   * @memberof CombineProduceRequest
   * @instance
   */

  CombineProduceRequest.prototype.positionTarget = 0;
  /**
   * Creates a new CombineProduceRequest instance using the specified properties.
   * @function create
   * @memberof CombineProduceRequest
   * @static
   * @param {ICombineProduceRequest=} [properties] Properties to set
   * @returns {CombineProduceRequest} CombineProduceRequest instance
   */

  CombineProduceRequest.create = function create(properties) {
    return new CombineProduceRequest(properties);
  };
  /**
   * Encodes the specified CombineProduceRequest message. Does not implicitly {@link CombineProduceRequest.verify|verify} messages.
   * @function encode
   * @memberof CombineProduceRequest
   * @static
   * @param {ICombineProduceRequest} message CombineProduceRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CombineProduceRequest.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.positionBase != null && Object.hasOwnProperty.call(message, "positionBase")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.positionBase);
    if (message.positionTarget != null && Object.hasOwnProperty.call(message, "positionTarget")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int32(message.positionTarget);
    return writer;
  };
  /**
   * Encodes the specified CombineProduceRequest message, length delimited. Does not implicitly {@link CombineProduceRequest.verify|verify} messages.
   * @function encodeDelimited
   * @memberof CombineProduceRequest
   * @static
   * @param {ICombineProduceRequest} message CombineProduceRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CombineProduceRequest.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a CombineProduceRequest message from the specified reader or buffer.
   * @function decode
   * @memberof CombineProduceRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {CombineProduceRequest} CombineProduceRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CombineProduceRequest.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.CombineProduceRequest();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.positionBase = reader.int32();
          break;

        case 2:
          message.positionTarget = reader.int32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a CombineProduceRequest message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof CombineProduceRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {CombineProduceRequest} CombineProduceRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CombineProduceRequest.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a CombineProduceRequest message.
   * @function verify
   * @memberof CombineProduceRequest
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  CombineProduceRequest.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.positionBase != null && message.hasOwnProperty("positionBase")) if (!$util.isInteger(message.positionBase)) return "positionBase: integer expected";
    if (message.positionTarget != null && message.hasOwnProperty("positionTarget")) if (!$util.isInteger(message.positionTarget)) return "positionTarget: integer expected";
    return null;
  };
  /**
   * Creates a CombineProduceRequest message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof CombineProduceRequest
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {CombineProduceRequest} CombineProduceRequest
   */


  CombineProduceRequest.fromObject = function fromObject(object) {
    if (object instanceof $root.CombineProduceRequest) return object;
    var message = new $root.CombineProduceRequest();
    if (object.positionBase != null) message.positionBase = object.positionBase | 0;
    if (object.positionTarget != null) message.positionTarget = object.positionTarget | 0;
    return message;
  };
  /**
   * Creates a plain object from a CombineProduceRequest message. Also converts values to other types if specified.
   * @function toObject
   * @memberof CombineProduceRequest
   * @static
   * @param {CombineProduceRequest} message CombineProduceRequest
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  CombineProduceRequest.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.positionBase = 0;
      object.positionTarget = 0;
    }

    if (message.positionBase != null && message.hasOwnProperty("positionBase")) object.positionBase = message.positionBase;
    if (message.positionTarget != null && message.hasOwnProperty("positionTarget")) object.positionTarget = message.positionTarget;
    return object;
  };
  /**
   * Converts this CombineProduceRequest to JSON.
   * @function toJSON
   * @memberof CombineProduceRequest
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  CombineProduceRequest.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return CombineProduceRequest;
}();

$root.CombineProduceReply = function () {
  /**
   * Properties of a CombineProduceReply.
   * @exports ICombineProduceReply
   * @interface ICombineProduceReply
   * @property {number|null} [errcode] CombineProduceReply errcode
   * @property {number|null} [produceLevel] CombineProduceReply produceLevel
   * @property {number|null} [positionNew] CombineProduceReply positionNew
   */

  /**
   * Constructs a new CombineProduceReply.
   * @exports CombineProduceReply
   * @classdesc Represents a CombineProduceReply.
   * @implements ICombineProduceReply
   * @constructor
   * @param {ICombineProduceReply=} [properties] Properties to set
   */
  function CombineProduceReply(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * CombineProduceReply errcode.
   * @member {number} errcode
   * @memberof CombineProduceReply
   * @instance
   */


  CombineProduceReply.prototype.errcode = 0;
  /**
   * CombineProduceReply produceLevel.
   * @member {number} produceLevel
   * @memberof CombineProduceReply
   * @instance
   */

  CombineProduceReply.prototype.produceLevel = 0;
  /**
   * CombineProduceReply positionNew.
   * @member {number} positionNew
   * @memberof CombineProduceReply
   * @instance
   */

  CombineProduceReply.prototype.positionNew = 0;
  /**
   * Creates a new CombineProduceReply instance using the specified properties.
   * @function create
   * @memberof CombineProduceReply
   * @static
   * @param {ICombineProduceReply=} [properties] Properties to set
   * @returns {CombineProduceReply} CombineProduceReply instance
   */

  CombineProduceReply.create = function create(properties) {
    return new CombineProduceReply(properties);
  };
  /**
   * Encodes the specified CombineProduceReply message. Does not implicitly {@link CombineProduceReply.verify|verify} messages.
   * @function encode
   * @memberof CombineProduceReply
   * @static
   * @param {ICombineProduceReply} message CombineProduceReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CombineProduceReply.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.errcode != null && Object.hasOwnProperty.call(message, "errcode")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.errcode);
    if (message.produceLevel != null && Object.hasOwnProperty.call(message, "produceLevel")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int32(message.produceLevel);
    if (message.positionNew != null && Object.hasOwnProperty.call(message, "positionNew")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).int32(message.positionNew);
    return writer;
  };
  /**
   * Encodes the specified CombineProduceReply message, length delimited. Does not implicitly {@link CombineProduceReply.verify|verify} messages.
   * @function encodeDelimited
   * @memberof CombineProduceReply
   * @static
   * @param {ICombineProduceReply} message CombineProduceReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CombineProduceReply.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a CombineProduceReply message from the specified reader or buffer.
   * @function decode
   * @memberof CombineProduceReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {CombineProduceReply} CombineProduceReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CombineProduceReply.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.CombineProduceReply();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.errcode = reader.int32();
          break;

        case 2:
          message.produceLevel = reader.int32();
          break;

        case 3:
          message.positionNew = reader.int32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a CombineProduceReply message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof CombineProduceReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {CombineProduceReply} CombineProduceReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CombineProduceReply.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a CombineProduceReply message.
   * @function verify
   * @memberof CombineProduceReply
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  CombineProduceReply.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.errcode != null && message.hasOwnProperty("errcode")) if (!$util.isInteger(message.errcode)) return "errcode: integer expected";
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) if (!$util.isInteger(message.produceLevel)) return "produceLevel: integer expected";
    if (message.positionNew != null && message.hasOwnProperty("positionNew")) if (!$util.isInteger(message.positionNew)) return "positionNew: integer expected";
    return null;
  };
  /**
   * Creates a CombineProduceReply message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof CombineProduceReply
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {CombineProduceReply} CombineProduceReply
   */


  CombineProduceReply.fromObject = function fromObject(object) {
    if (object instanceof $root.CombineProduceReply) return object;
    var message = new $root.CombineProduceReply();
    if (object.errcode != null) message.errcode = object.errcode | 0;
    if (object.produceLevel != null) message.produceLevel = object.produceLevel | 0;
    if (object.positionNew != null) message.positionNew = object.positionNew | 0;
    return message;
  };
  /**
   * Creates a plain object from a CombineProduceReply message. Also converts values to other types if specified.
   * @function toObject
   * @memberof CombineProduceReply
   * @static
   * @param {CombineProduceReply} message CombineProduceReply
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  CombineProduceReply.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.errcode = 0;
      object.produceLevel = 0;
      object.positionNew = 0;
    }

    if (message.errcode != null && message.hasOwnProperty("errcode")) object.errcode = message.errcode;
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) object.produceLevel = message.produceLevel;
    if (message.positionNew != null && message.hasOwnProperty("positionNew")) object.positionNew = message.positionNew;
    return object;
  };
  /**
   * Converts this CombineProduceReply to JSON.
   * @function toJSON
   * @memberof CombineProduceReply
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  CombineProduceReply.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return CombineProduceReply;
}();

$root.RefreshWorkBenchRequest = function () {
  /**
   * Properties of a RefreshWorkBenchRequest.
   * @exports IRefreshWorkBenchRequest
   * @interface IRefreshWorkBenchRequest
   */

  /**
   * Constructs a new RefreshWorkBenchRequest.
   * @exports RefreshWorkBenchRequest
   * @classdesc Represents a RefreshWorkBenchRequest.
   * @implements IRefreshWorkBenchRequest
   * @constructor
   * @param {IRefreshWorkBenchRequest=} [properties] Properties to set
   */
  function RefreshWorkBenchRequest(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * Creates a new RefreshWorkBenchRequest instance using the specified properties.
   * @function create
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {IRefreshWorkBenchRequest=} [properties] Properties to set
   * @returns {RefreshWorkBenchRequest} RefreshWorkBenchRequest instance
   */


  RefreshWorkBenchRequest.create = function create(properties) {
    return new RefreshWorkBenchRequest(properties);
  };
  /**
   * Encodes the specified RefreshWorkBenchRequest message. Does not implicitly {@link RefreshWorkBenchRequest.verify|verify} messages.
   * @function encode
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {IRefreshWorkBenchRequest} message RefreshWorkBenchRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  RefreshWorkBenchRequest.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    return writer;
  };
  /**
   * Encodes the specified RefreshWorkBenchRequest message, length delimited. Does not implicitly {@link RefreshWorkBenchRequest.verify|verify} messages.
   * @function encodeDelimited
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {IRefreshWorkBenchRequest} message RefreshWorkBenchRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  RefreshWorkBenchRequest.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a RefreshWorkBenchRequest message from the specified reader or buffer.
   * @function decode
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {RefreshWorkBenchRequest} RefreshWorkBenchRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  RefreshWorkBenchRequest.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.RefreshWorkBenchRequest();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a RefreshWorkBenchRequest message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {RefreshWorkBenchRequest} RefreshWorkBenchRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  RefreshWorkBenchRequest.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a RefreshWorkBenchRequest message.
   * @function verify
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  RefreshWorkBenchRequest.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    return null;
  };
  /**
   * Creates a RefreshWorkBenchRequest message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {RefreshWorkBenchRequest} RefreshWorkBenchRequest
   */


  RefreshWorkBenchRequest.fromObject = function fromObject(object) {
    if (object instanceof $root.RefreshWorkBenchRequest) return object;
    return new $root.RefreshWorkBenchRequest();
  };
  /**
   * Creates a plain object from a RefreshWorkBenchRequest message. Also converts values to other types if specified.
   * @function toObject
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {RefreshWorkBenchRequest} message RefreshWorkBenchRequest
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  RefreshWorkBenchRequest.toObject = function toObject() {
    return {};
  };
  /**
   * Converts this RefreshWorkBenchRequest to JSON.
   * @function toJSON
   * @memberof RefreshWorkBenchRequest
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  RefreshWorkBenchRequest.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return RefreshWorkBenchRequest;
}();

$root.RefreshWorkBenchReply = function () {
  /**
   * Properties of a RefreshWorkBenchReply.
   * @exports IRefreshWorkBenchReply
   * @interface IRefreshWorkBenchReply
   * @property {Array.<number>|null} [workbenchInfo] RefreshWorkBenchReply workbenchInfo
   */

  /**
   * Constructs a new RefreshWorkBenchReply.
   * @exports RefreshWorkBenchReply
   * @classdesc Represents a RefreshWorkBenchReply.
   * @implements IRefreshWorkBenchReply
   * @constructor
   * @param {IRefreshWorkBenchReply=} [properties] Properties to set
   */
  function RefreshWorkBenchReply(properties) {
    this.workbenchInfo = [];
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * RefreshWorkBenchReply workbenchInfo.
   * @member {Array.<number>} workbenchInfo
   * @memberof RefreshWorkBenchReply
   * @instance
   */


  RefreshWorkBenchReply.prototype.workbenchInfo = $util.emptyArray;
  /**
   * Creates a new RefreshWorkBenchReply instance using the specified properties.
   * @function create
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {IRefreshWorkBenchReply=} [properties] Properties to set
   * @returns {RefreshWorkBenchReply} RefreshWorkBenchReply instance
   */

  RefreshWorkBenchReply.create = function create(properties) {
    return new RefreshWorkBenchReply(properties);
  };
  /**
   * Encodes the specified RefreshWorkBenchReply message. Does not implicitly {@link RefreshWorkBenchReply.verify|verify} messages.
   * @function encode
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {IRefreshWorkBenchReply} message RefreshWorkBenchReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  RefreshWorkBenchReply.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();

    if (message.workbenchInfo != null && message.workbenchInfo.length) {
      writer.uint32(
      /* id 1, wireType 2 =*/
      10).fork();

      for (var i = 0; i < message.workbenchInfo.length; ++i) {
        writer.int32(message.workbenchInfo[i]);
      }

      writer.ldelim();
    }

    return writer;
  };
  /**
   * Encodes the specified RefreshWorkBenchReply message, length delimited. Does not implicitly {@link RefreshWorkBenchReply.verify|verify} messages.
   * @function encodeDelimited
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {IRefreshWorkBenchReply} message RefreshWorkBenchReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  RefreshWorkBenchReply.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a RefreshWorkBenchReply message from the specified reader or buffer.
   * @function decode
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {RefreshWorkBenchReply} RefreshWorkBenchReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  RefreshWorkBenchReply.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.RefreshWorkBenchReply();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          if (!(message.workbenchInfo && message.workbenchInfo.length)) message.workbenchInfo = [];

          if ((tag & 7) === 2) {
            var end2 = reader.uint32() + reader.pos;

            while (reader.pos < end2) {
              message.workbenchInfo.push(reader.int32());
            }
          } else message.workbenchInfo.push(reader.int32());

          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a RefreshWorkBenchReply message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {RefreshWorkBenchReply} RefreshWorkBenchReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  RefreshWorkBenchReply.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a RefreshWorkBenchReply message.
   * @function verify
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  RefreshWorkBenchReply.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";

    if (message.workbenchInfo != null && message.hasOwnProperty("workbenchInfo")) {
      if (!Array.isArray(message.workbenchInfo)) return "workbenchInfo: array expected";

      for (var i = 0; i < message.workbenchInfo.length; ++i) {
        if (!$util.isInteger(message.workbenchInfo[i])) return "workbenchInfo: integer[] expected";
      }
    }

    return null;
  };
  /**
   * Creates a RefreshWorkBenchReply message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {RefreshWorkBenchReply} RefreshWorkBenchReply
   */


  RefreshWorkBenchReply.fromObject = function fromObject(object) {
    if (object instanceof $root.RefreshWorkBenchReply) return object;
    var message = new $root.RefreshWorkBenchReply();

    if (object.workbenchInfo) {
      if (!Array.isArray(object.workbenchInfo)) throw TypeError(".RefreshWorkBenchReply.workbenchInfo: array expected");
      message.workbenchInfo = [];

      for (var i = 0; i < object.workbenchInfo.length; ++i) {
        message.workbenchInfo[i] = object.workbenchInfo[i] | 0;
      }
    }

    return message;
  };
  /**
   * Creates a plain object from a RefreshWorkBenchReply message. Also converts values to other types if specified.
   * @function toObject
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {RefreshWorkBenchReply} message RefreshWorkBenchReply
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  RefreshWorkBenchReply.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};
    if (options.arrays || options.defaults) object.workbenchInfo = [];

    if (message.workbenchInfo && message.workbenchInfo.length) {
      object.workbenchInfo = [];

      for (var j = 0; j < message.workbenchInfo.length; ++j) {
        object.workbenchInfo[j] = message.workbenchInfo[j];
      }
    }

    return object;
  };
  /**
   * Converts this RefreshWorkBenchReply to JSON.
   * @function toJSON
   * @memberof RefreshWorkBenchReply
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  RefreshWorkBenchReply.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return RefreshWorkBenchReply;
}();

$root.DecimalValue = function () {
  /**
   * Properties of a DecimalValue.
   * @exports IDecimalValue
   * @interface IDecimalValue
   * @property {number|Long|null} [uints] DecimalValue uints
   * @property {number|null} [nanos] DecimalValue nanos
   */

  /**
   * Constructs a new DecimalValue.
   * @exports DecimalValue
   * @classdesc Represents a DecimalValue.
   * @implements IDecimalValue
   * @constructor
   * @param {IDecimalValue=} [properties] Properties to set
   */
  function DecimalValue(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * DecimalValue uints.
   * @member {number|Long} uints
   * @memberof DecimalValue
   * @instance
   */


  DecimalValue.prototype.uints = $util.Long ? $util.Long.fromBits(0, 0, false) : 0;
  /**
   * DecimalValue nanos.
   * @member {number} nanos
   * @memberof DecimalValue
   * @instance
   */

  DecimalValue.prototype.nanos = 0;
  /**
   * Creates a new DecimalValue instance using the specified properties.
   * @function create
   * @memberof DecimalValue
   * @static
   * @param {IDecimalValue=} [properties] Properties to set
   * @returns {DecimalValue} DecimalValue instance
   */

  DecimalValue.create = function create(properties) {
    return new DecimalValue(properties);
  };
  /**
   * Encodes the specified DecimalValue message. Does not implicitly {@link DecimalValue.verify|verify} messages.
   * @function encode
   * @memberof DecimalValue
   * @static
   * @param {IDecimalValue} message DecimalValue message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  DecimalValue.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.uints != null && Object.hasOwnProperty.call(message, "uints")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int64(message.uints);
    if (message.nanos != null && Object.hasOwnProperty.call(message, "nanos")) writer.uint32(
    /* id 2, wireType 5 =*/
    21).sfixed32(message.nanos);
    return writer;
  };
  /**
   * Encodes the specified DecimalValue message, length delimited. Does not implicitly {@link DecimalValue.verify|verify} messages.
   * @function encodeDelimited
   * @memberof DecimalValue
   * @static
   * @param {IDecimalValue} message DecimalValue message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  DecimalValue.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a DecimalValue message from the specified reader or buffer.
   * @function decode
   * @memberof DecimalValue
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {DecimalValue} DecimalValue
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  DecimalValue.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.DecimalValue();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.uints = reader.int64();
          break;

        case 2:
          message.nanos = reader.sfixed32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a DecimalValue message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof DecimalValue
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {DecimalValue} DecimalValue
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  DecimalValue.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a DecimalValue message.
   * @function verify
   * @memberof DecimalValue
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  DecimalValue.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.uints != null && message.hasOwnProperty("uints")) if (!$util.isInteger(message.uints) && !(message.uints && $util.isInteger(message.uints.low) && $util.isInteger(message.uints.high))) return "uints: integer|Long expected";
    if (message.nanos != null && message.hasOwnProperty("nanos")) if (!$util.isInteger(message.nanos)) return "nanos: integer expected";
    return null;
  };
  /**
   * Creates a DecimalValue message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof DecimalValue
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {DecimalValue} DecimalValue
   */


  DecimalValue.fromObject = function fromObject(object) {
    if (object instanceof $root.DecimalValue) return object;
    var message = new $root.DecimalValue();
    if (object.uints != null) if ($util.Long) (message.uints = $util.Long.fromValue(object.uints)).unsigned = false;else if (typeof object.uints === "string") message.uints = parseInt(object.uints, 10);else if (typeof object.uints === "number") message.uints = object.uints;else if (typeof object.uints === "object") message.uints = new $util.LongBits(object.uints.low >>> 0, object.uints.high >>> 0).toNumber();
    if (object.nanos != null) message.nanos = object.nanos | 0;
    return message;
  };
  /**
   * Creates a plain object from a DecimalValue message. Also converts values to other types if specified.
   * @function toObject
   * @memberof DecimalValue
   * @static
   * @param {DecimalValue} message DecimalValue
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  DecimalValue.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      if ($util.Long) {
        var _long2 = new $util.Long(0, 0, false);

        object.uints = options.longs === String ? _long2.toString() : options.longs === Number ? _long2.toNumber() : _long2;
      } else object.uints = options.longs === String ? "0" : 0;

      object.nanos = 0;
    }

    if (message.uints != null && message.hasOwnProperty("uints")) if (typeof message.uints === "number") object.uints = options.longs === String ? String(message.uints) : message.uints;else object.uints = options.longs === String ? $util.Long.prototype.toString.call(message.uints) : options.longs === Number ? new $util.LongBits(message.uints.low >>> 0, message.uints.high >>> 0).toNumber() : message.uints;
    if (message.nanos != null && message.hasOwnProperty("nanos")) object.nanos = message.nanos;
    return object;
  };
  /**
   * Converts this DecimalValue to JSON.
   * @function toJSON
   * @memberof DecimalValue
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  DecimalValue.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return DecimalValue;
}();

$root.UserInfo = function () {
  /**
   * Properties of a UserInfo.
   * @exports IUserInfo
   * @interface IUserInfo
   * @property {string|null} [account] UserInfo account
   * @property {string|null} [nickname] UserInfo nickname
   * @property {string|null} [avatarUrl] UserInfo avatarUrl
   * @property {IDecimalValue|null} [money] UserInfo money
   */

  /**
   * Constructs a new UserInfo.
   * @exports UserInfo
   * @classdesc Represents a UserInfo.
   * @implements IUserInfo
   * @constructor
   * @param {IUserInfo=} [properties] Properties to set
   */
  function UserInfo(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * UserInfo account.
   * @member {string} account
   * @memberof UserInfo
   * @instance
   */


  UserInfo.prototype.account = "";
  /**
   * UserInfo nickname.
   * @member {string} nickname
   * @memberof UserInfo
   * @instance
   */

  UserInfo.prototype.nickname = "";
  /**
   * UserInfo avatarUrl.
   * @member {string} avatarUrl
   * @memberof UserInfo
   * @instance
   */

  UserInfo.prototype.avatarUrl = "";
  /**
   * UserInfo money.
   * @member {IDecimalValue|null|undefined} money
   * @memberof UserInfo
   * @instance
   */

  UserInfo.prototype.money = null;
  /**
   * Creates a new UserInfo instance using the specified properties.
   * @function create
   * @memberof UserInfo
   * @static
   * @param {IUserInfo=} [properties] Properties to set
   * @returns {UserInfo} UserInfo instance
   */

  UserInfo.create = function create(properties) {
    return new UserInfo(properties);
  };
  /**
   * Encodes the specified UserInfo message. Does not implicitly {@link UserInfo.verify|verify} messages.
   * @function encode
   * @memberof UserInfo
   * @static
   * @param {IUserInfo} message UserInfo message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  UserInfo.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.account != null && Object.hasOwnProperty.call(message, "account")) writer.uint32(
    /* id 1, wireType 2 =*/
    10).string(message.account);
    if (message.nickname != null && Object.hasOwnProperty.call(message, "nickname")) writer.uint32(
    /* id 2, wireType 2 =*/
    18).string(message.nickname);
    if (message.avatarUrl != null && Object.hasOwnProperty.call(message, "avatarUrl")) writer.uint32(
    /* id 3, wireType 2 =*/
    26).string(message.avatarUrl);
    if (message.money != null && Object.hasOwnProperty.call(message, "money")) $root.DecimalValue.encode(message.money, writer.uint32(
    /* id 4, wireType 2 =*/
    34).fork()).ldelim();
    return writer;
  };
  /**
   * Encodes the specified UserInfo message, length delimited. Does not implicitly {@link UserInfo.verify|verify} messages.
   * @function encodeDelimited
   * @memberof UserInfo
   * @static
   * @param {IUserInfo} message UserInfo message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  UserInfo.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a UserInfo message from the specified reader or buffer.
   * @function decode
   * @memberof UserInfo
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {UserInfo} UserInfo
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  UserInfo.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.UserInfo();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.account = reader.string();
          break;

        case 2:
          message.nickname = reader.string();
          break;

        case 3:
          message.avatarUrl = reader.string();
          break;

        case 4:
          message.money = $root.DecimalValue.decode(reader, reader.uint32());
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a UserInfo message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof UserInfo
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {UserInfo} UserInfo
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  UserInfo.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a UserInfo message.
   * @function verify
   * @memberof UserInfo
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  UserInfo.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.account != null && message.hasOwnProperty("account")) if (!$util.isString(message.account)) return "account: string expected";
    if (message.nickname != null && message.hasOwnProperty("nickname")) if (!$util.isString(message.nickname)) return "nickname: string expected";
    if (message.avatarUrl != null && message.hasOwnProperty("avatarUrl")) if (!$util.isString(message.avatarUrl)) return "avatarUrl: string expected";

    if (message.money != null && message.hasOwnProperty("money")) {
      var error = $root.DecimalValue.verify(message.money);
      if (error) return "money." + error;
    }

    return null;
  };
  /**
   * Creates a UserInfo message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof UserInfo
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {UserInfo} UserInfo
   */


  UserInfo.fromObject = function fromObject(object) {
    if (object instanceof $root.UserInfo) return object;
    var message = new $root.UserInfo();
    if (object.account != null) message.account = String(object.account);
    if (object.nickname != null) message.nickname = String(object.nickname);
    if (object.avatarUrl != null) message.avatarUrl = String(object.avatarUrl);

    if (object.money != null) {
      if (typeof object.money !== "object") throw TypeError(".UserInfo.money: object expected");
      message.money = $root.DecimalValue.fromObject(object.money);
    }

    return message;
  };
  /**
   * Creates a plain object from a UserInfo message. Also converts values to other types if specified.
   * @function toObject
   * @memberof UserInfo
   * @static
   * @param {UserInfo} message UserInfo
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  UserInfo.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.account = "";
      object.nickname = "";
      object.avatarUrl = "";
      object.money = null;
    }

    if (message.account != null && message.hasOwnProperty("account")) object.account = message.account;
    if (message.nickname != null && message.hasOwnProperty("nickname")) object.nickname = message.nickname;
    if (message.avatarUrl != null && message.hasOwnProperty("avatarUrl")) object.avatarUrl = message.avatarUrl;
    if (message.money != null && message.hasOwnProperty("money")) object.money = $root.DecimalValue.toObject(message.money, options);
    return object;
  };
  /**
   * Converts this UserInfo to JSON.
   * @function toJSON
   * @memberof UserInfo
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  UserInfo.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return UserInfo;
}();

$root.UserMoney = function () {
  /**
   * Properties of a UserMoney.
   * @exports IUserMoney
   * @interface IUserMoney
   * @property {number|null} [coin] UserMoney coin
   * @property {number|null} [diamond] UserMoney diamond
   * @property {number|null} [money] UserMoney money
   */

  /**
   * Constructs a new UserMoney.
   * @exports UserMoney
   * @classdesc Represents a UserMoney.
   * @implements IUserMoney
   * @constructor
   * @param {IUserMoney=} [properties] Properties to set
   */
  function UserMoney(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * UserMoney coin.
   * @member {number} coin
   * @memberof UserMoney
   * @instance
   */


  UserMoney.prototype.coin = 0;
  /**
   * UserMoney diamond.
   * @member {number} diamond
   * @memberof UserMoney
   * @instance
   */

  UserMoney.prototype.diamond = 0;
  /**
   * UserMoney money.
   * @member {number} money
   * @memberof UserMoney
   * @instance
   */

  UserMoney.prototype.money = 0;
  /**
   * Creates a new UserMoney instance using the specified properties.
   * @function create
   * @memberof UserMoney
   * @static
   * @param {IUserMoney=} [properties] Properties to set
   * @returns {UserMoney} UserMoney instance
   */

  UserMoney.create = function create(properties) {
    return new UserMoney(properties);
  };
  /**
   * Encodes the specified UserMoney message. Does not implicitly {@link UserMoney.verify|verify} messages.
   * @function encode
   * @memberof UserMoney
   * @static
   * @param {IUserMoney} message UserMoney message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  UserMoney.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.coin != null && Object.hasOwnProperty.call(message, "coin")) writer.uint32(
    /* id 1, wireType 1 =*/
    9)["double"](message.coin);
    if (message.diamond != null && Object.hasOwnProperty.call(message, "diamond")) writer.uint32(
    /* id 2, wireType 1 =*/
    17)["double"](message.diamond);
    if (message.money != null && Object.hasOwnProperty.call(message, "money")) writer.uint32(
    /* id 3, wireType 5 =*/
    29)["float"](message.money);
    return writer;
  };
  /**
   * Encodes the specified UserMoney message, length delimited. Does not implicitly {@link UserMoney.verify|verify} messages.
   * @function encodeDelimited
   * @memberof UserMoney
   * @static
   * @param {IUserMoney} message UserMoney message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  UserMoney.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a UserMoney message from the specified reader or buffer.
   * @function decode
   * @memberof UserMoney
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {UserMoney} UserMoney
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  UserMoney.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.UserMoney();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.coin = reader["double"]();
          break;

        case 2:
          message.diamond = reader["double"]();
          break;

        case 3:
          message.money = reader["float"]();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a UserMoney message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof UserMoney
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {UserMoney} UserMoney
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  UserMoney.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a UserMoney message.
   * @function verify
   * @memberof UserMoney
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  UserMoney.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.coin != null && message.hasOwnProperty("coin")) if (typeof message.coin !== "number") return "coin: number expected";
    if (message.diamond != null && message.hasOwnProperty("diamond")) if (typeof message.diamond !== "number") return "diamond: number expected";
    if (message.money != null && message.hasOwnProperty("money")) if (typeof message.money !== "number") return "money: number expected";
    return null;
  };
  /**
   * Creates a UserMoney message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof UserMoney
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {UserMoney} UserMoney
   */


  UserMoney.fromObject = function fromObject(object) {
    if (object instanceof $root.UserMoney) return object;
    var message = new $root.UserMoney();
    if (object.coin != null) message.coin = Number(object.coin);
    if (object.diamond != null) message.diamond = Number(object.diamond);
    if (object.money != null) message.money = Number(object.money);
    return message;
  };
  /**
   * Creates a plain object from a UserMoney message. Also converts values to other types if specified.
   * @function toObject
   * @memberof UserMoney
   * @static
   * @param {UserMoney} message UserMoney
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  UserMoney.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.coin = 0;
      object.diamond = 0;
      object.money = 0;
    }

    if (message.coin != null && message.hasOwnProperty("coin")) object.coin = options.json && !isFinite(message.coin) ? String(message.coin) : message.coin;
    if (message.diamond != null && message.hasOwnProperty("diamond")) object.diamond = options.json && !isFinite(message.diamond) ? String(message.diamond) : message.diamond;
    if (message.money != null && message.hasOwnProperty("money")) object.money = options.json && !isFinite(message.money) ? String(message.money) : message.money;
    return object;
  };
  /**
   * Converts this UserMoney to JSON.
   * @function toJSON
   * @memberof UserMoney
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  UserMoney.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return UserMoney;
}();

$root.WorkBenchItemInfo = function () {
  /**
   * Properties of a WorkBenchItemInfo.
   * @exports IWorkBenchItemInfo
   * @interface IWorkBenchItemInfo
   * @property {number|null} [level] WorkBenchItemInfo level
   * @property {number|Long|null} [coinCost] WorkBenchItemInfo coinCost
   * @property {number|Long|null} [coinIncome] WorkBenchItemInfo coinIncome
   */

  /**
   * Constructs a new WorkBenchItemInfo.
   * @exports WorkBenchItemInfo
   * @classdesc Represents a WorkBenchItemInfo.
   * @implements IWorkBenchItemInfo
   * @constructor
   * @param {IWorkBenchItemInfo=} [properties] Properties to set
   */
  function WorkBenchItemInfo(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * WorkBenchItemInfo level.
   * @member {number} level
   * @memberof WorkBenchItemInfo
   * @instance
   */


  WorkBenchItemInfo.prototype.level = 0;
  /**
   * WorkBenchItemInfo coinCost.
   * @member {number|Long} coinCost
   * @memberof WorkBenchItemInfo
   * @instance
   */

  WorkBenchItemInfo.prototype.coinCost = $util.Long ? $util.Long.fromBits(0, 0, false) : 0;
  /**
   * WorkBenchItemInfo coinIncome.
   * @member {number|Long} coinIncome
   * @memberof WorkBenchItemInfo
   * @instance
   */

  WorkBenchItemInfo.prototype.coinIncome = $util.Long ? $util.Long.fromBits(0, 0, false) : 0;
  /**
   * Creates a new WorkBenchItemInfo instance using the specified properties.
   * @function create
   * @memberof WorkBenchItemInfo
   * @static
   * @param {IWorkBenchItemInfo=} [properties] Properties to set
   * @returns {WorkBenchItemInfo} WorkBenchItemInfo instance
   */

  WorkBenchItemInfo.create = function create(properties) {
    return new WorkBenchItemInfo(properties);
  };
  /**
   * Encodes the specified WorkBenchItemInfo message. Does not implicitly {@link WorkBenchItemInfo.verify|verify} messages.
   * @function encode
   * @memberof WorkBenchItemInfo
   * @static
   * @param {IWorkBenchItemInfo} message WorkBenchItemInfo message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  WorkBenchItemInfo.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.level != null && Object.hasOwnProperty.call(message, "level")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.level);
    if (message.coinCost != null && Object.hasOwnProperty.call(message, "coinCost")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int64(message.coinCost);
    if (message.coinIncome != null && Object.hasOwnProperty.call(message, "coinIncome")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).int64(message.coinIncome);
    return writer;
  };
  /**
   * Encodes the specified WorkBenchItemInfo message, length delimited. Does not implicitly {@link WorkBenchItemInfo.verify|verify} messages.
   * @function encodeDelimited
   * @memberof WorkBenchItemInfo
   * @static
   * @param {IWorkBenchItemInfo} message WorkBenchItemInfo message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  WorkBenchItemInfo.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a WorkBenchItemInfo message from the specified reader or buffer.
   * @function decode
   * @memberof WorkBenchItemInfo
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {WorkBenchItemInfo} WorkBenchItemInfo
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  WorkBenchItemInfo.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.WorkBenchItemInfo();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.level = reader.int32();
          break;

        case 2:
          message.coinCost = reader.int64();
          break;

        case 3:
          message.coinIncome = reader.int64();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a WorkBenchItemInfo message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof WorkBenchItemInfo
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {WorkBenchItemInfo} WorkBenchItemInfo
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  WorkBenchItemInfo.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a WorkBenchItemInfo message.
   * @function verify
   * @memberof WorkBenchItemInfo
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  WorkBenchItemInfo.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.level != null && message.hasOwnProperty("level")) if (!$util.isInteger(message.level)) return "level: integer expected";
    if (message.coinCost != null && message.hasOwnProperty("coinCost")) if (!$util.isInteger(message.coinCost) && !(message.coinCost && $util.isInteger(message.coinCost.low) && $util.isInteger(message.coinCost.high))) return "coinCost: integer|Long expected";
    if (message.coinIncome != null && message.hasOwnProperty("coinIncome")) if (!$util.isInteger(message.coinIncome) && !(message.coinIncome && $util.isInteger(message.coinIncome.low) && $util.isInteger(message.coinIncome.high))) return "coinIncome: integer|Long expected";
    return null;
  };
  /**
   * Creates a WorkBenchItemInfo message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof WorkBenchItemInfo
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {WorkBenchItemInfo} WorkBenchItemInfo
   */


  WorkBenchItemInfo.fromObject = function fromObject(object) {
    if (object instanceof $root.WorkBenchItemInfo) return object;
    var message = new $root.WorkBenchItemInfo();
    if (object.level != null) message.level = object.level | 0;
    if (object.coinCost != null) if ($util.Long) (message.coinCost = $util.Long.fromValue(object.coinCost)).unsigned = false;else if (typeof object.coinCost === "string") message.coinCost = parseInt(object.coinCost, 10);else if (typeof object.coinCost === "number") message.coinCost = object.coinCost;else if (typeof object.coinCost === "object") message.coinCost = new $util.LongBits(object.coinCost.low >>> 0, object.coinCost.high >>> 0).toNumber();
    if (object.coinIncome != null) if ($util.Long) (message.coinIncome = $util.Long.fromValue(object.coinIncome)).unsigned = false;else if (typeof object.coinIncome === "string") message.coinIncome = parseInt(object.coinIncome, 10);else if (typeof object.coinIncome === "number") message.coinIncome = object.coinIncome;else if (typeof object.coinIncome === "object") message.coinIncome = new $util.LongBits(object.coinIncome.low >>> 0, object.coinIncome.high >>> 0).toNumber();
    return message;
  };
  /**
   * Creates a plain object from a WorkBenchItemInfo message. Also converts values to other types if specified.
   * @function toObject
   * @memberof WorkBenchItemInfo
   * @static
   * @param {WorkBenchItemInfo} message WorkBenchItemInfo
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  WorkBenchItemInfo.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.level = 0;

      if ($util.Long) {
        var _long3 = new $util.Long(0, 0, false);

        object.coinCost = options.longs === String ? _long3.toString() : options.longs === Number ? _long3.toNumber() : _long3;
      } else object.coinCost = options.longs === String ? "0" : 0;

      if ($util.Long) {
        var _long3 = new $util.Long(0, 0, false);

        object.coinIncome = options.longs === String ? _long3.toString() : options.longs === Number ? _long3.toNumber() : _long3;
      } else object.coinIncome = options.longs === String ? "0" : 0;
    }

    if (message.level != null && message.hasOwnProperty("level")) object.level = message.level;
    if (message.coinCost != null && message.hasOwnProperty("coinCost")) if (typeof message.coinCost === "number") object.coinCost = options.longs === String ? String(message.coinCost) : message.coinCost;else object.coinCost = options.longs === String ? $util.Long.prototype.toString.call(message.coinCost) : options.longs === Number ? new $util.LongBits(message.coinCost.low >>> 0, message.coinCost.high >>> 0).toNumber() : message.coinCost;
    if (message.coinIncome != null && message.hasOwnProperty("coinIncome")) if (typeof message.coinIncome === "number") object.coinIncome = options.longs === String ? String(message.coinIncome) : message.coinIncome;else object.coinIncome = options.longs === String ? $util.Long.prototype.toString.call(message.coinIncome) : options.longs === Number ? new $util.LongBits(message.coinIncome.low >>> 0, message.coinIncome.high >>> 0).toNumber() : message.coinIncome;
    return object;
  };
  /**
   * Converts this WorkBenchItemInfo to JSON.
   * @function toJSON
   * @memberof WorkBenchItemInfo
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  WorkBenchItemInfo.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return WorkBenchItemInfo;
}();

module.exports = $root;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9Qcm90b2NvbC9idW5kbGUuanMiXSwibmFtZXMiOlsiJHByb3RvYnVmIiwicmVxdWlyZSIsIiRSZWFkZXIiLCJSZWFkZXIiLCIkV3JpdGVyIiwiV3JpdGVyIiwiJHV0aWwiLCJ1dGlsIiwiJHJvb3QiLCJyb290cyIsIkludm9rZUJlZ2luIiwicHJvcGVydGllcyIsImtleXMiLCJPYmplY3QiLCJpIiwibGVuZ3RoIiwicHJvdG90eXBlIiwiaW52b2tlSWQiLCJtZXRob2QiLCJjcmVhdGUiLCJlbmNvZGUiLCJtZXNzYWdlIiwid3JpdGVyIiwiaGFzT3duUHJvcGVydHkiLCJjYWxsIiwidWludDMyIiwiaW50MzIiLCJzdHJpbmciLCJlbmNvZGVEZWxpbWl0ZWQiLCJsZGVsaW0iLCJkZWNvZGUiLCJyZWFkZXIiLCJlbmQiLCJ1bmRlZmluZWQiLCJsZW4iLCJwb3MiLCJ0YWciLCJza2lwVHlwZSIsImRlY29kZURlbGltaXRlZCIsInZlcmlmeSIsImlzSW50ZWdlciIsImlzU3RyaW5nIiwiZnJvbU9iamVjdCIsIm9iamVjdCIsIlN0cmluZyIsInRvT2JqZWN0Iiwib3B0aW9ucyIsImRlZmF1bHRzIiwidG9KU09OIiwiY29uc3RydWN0b3IiLCJ0b0pTT05PcHRpb25zIiwiSW52b2tlRW5kIiwic3RhdHVzIiwiZXJyb3IiLCJDbG9zZUNvbm5lY3Rpb24iLCJjb2RlIiwicmVhc29uIiwiYWxsb3dSZWNvbm5lY3QiLCJib29sIiwiQm9vbGVhbiIsIkVudmVsb3BlIiwiaGVhZGVyIiwicGF5bG9hZCIsIm5ld0J1ZmZlciIsIkhlYWRlciIsImZvcmsiLCJieXRlcyIsIlR5cGVFcnJvciIsImJhc2U2NCIsIkFycmF5Iiwic2xpY2UiLCJpbnZva2VCZWdpbiIsImludm9rZUVuZCIsImNsb3NlQ29ubmVjdGlvbiIsImV2ZW50IiwiJG9uZU9mRmllbGRzIiwiZGVmaW5lUHJvcGVydHkiLCJnZXQiLCJvbmVPZkdldHRlciIsInNldCIsIm9uZU9mU2V0dGVyIiwia2luZCIsIm9uZW9mcyIsIkdhbWVTZXJ2ZXIiLCJycGNJbXBsIiwicmVxdWVzdERlbGltaXRlZCIsInJlc3BvbnNlRGVsaW1pdGVkIiwicnBjIiwiU2VydmljZSIsImxvZ2luIiwicmVxdWVzdCIsImNhbGxiYWNrIiwicnBjQ2FsbCIsIkxvZ2luUmVxdWVzdCIsIkxvZ2luUmVwbHkiLCJ2YWx1ZSIsImJ1eVByb2R1Y2UiLCJCdXlQcm9kdWNlUmVxdWVzdCIsIkJ1eVByb2R1Y2VSZXBseSIsImNvbWJpbmVQcm9kdWNlIiwiQ29tYmluZVByb2R1Y2VSZXF1ZXN0IiwiQ29tYmluZVByb2R1Y2VSZXBseSIsInJlZnJlc2hXb3JrQmVuY2giLCJSZWZyZXNoV29ya0JlbmNoUmVxdWVzdCIsIlJlZnJlc2hXb3JrQmVuY2hSZXBseSIsImFjY2Vzc1Rva2VuIiwid29ya2JlbmNoSXRlbXMiLCJ1c2VySW5mbyIsInNlcnZlclRpbWUiLCJjb2luIiwiTG9uZyIsImZyb21CaXRzIiwiZGlhbW9uZCIsImVtcHR5QXJyYXkiLCJVc2VySW5mbyIsInVpbnQ2NCIsIldvcmtCZW5jaEl0ZW1JbmZvIiwicHVzaCIsImxvdyIsImhpZ2giLCJpc0FycmF5IiwiZnJvbVZhbHVlIiwidW5zaWduZWQiLCJwYXJzZUludCIsIkxvbmdCaXRzIiwidG9OdW1iZXIiLCJhcnJheXMiLCJsb25nIiwibG9uZ3MiLCJ0b1N0cmluZyIsIk51bWJlciIsImoiLCJwcm9kdWNlTGV2ZWwiLCJlcnJjb2RlIiwicG9zaXRpb24iLCJwb3NpdGlvbkJhc2UiLCJwb3NpdGlvblRhcmdldCIsInBvc2l0aW9uTmV3Iiwid29ya2JlbmNoSW5mbyIsImVuZDIiLCJEZWNpbWFsVmFsdWUiLCJ1aW50cyIsIm5hbm9zIiwiaW50NjQiLCJzZml4ZWQzMiIsImFjY291bnQiLCJuaWNrbmFtZSIsImF2YXRhclVybCIsIm1vbmV5IiwiVXNlck1vbmV5IiwianNvbiIsImlzRmluaXRlIiwibGV2ZWwiLCJjb2luQ29zdCIsImNvaW5JbmNvbWUiLCJtb2R1bGUiLCJleHBvcnRzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBOztBQUVBLElBQUlBLFNBQVMsR0FBR0MsT0FBTyxDQUFDLG9CQUFELENBQXZCLEVBRUE7OztBQUNBLElBQUlDLE9BQU8sR0FBR0YsU0FBUyxDQUFDRyxNQUF4QjtBQUFBLElBQWdDQyxPQUFPLEdBQUdKLFNBQVMsQ0FBQ0ssTUFBcEQ7QUFBQSxJQUE0REMsS0FBSyxHQUFHTixTQUFTLENBQUNPLElBQTlFLEVBRUE7O0FBQ0EsSUFBSUMsS0FBSyxHQUFHUixTQUFTLENBQUNTLEtBQVYsQ0FBZ0IsU0FBaEIsTUFBK0JULFNBQVMsQ0FBQ1MsS0FBVixDQUFnQixTQUFoQixJQUE2QixFQUE1RCxDQUFaOztBQUVBRCxLQUFLLENBQUNFLFdBQU4sR0FBcUIsWUFBVztBQUU1QjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ksV0FBU0EsV0FBVCxDQUFxQkMsVUFBckIsRUFBaUM7QUFDN0IsUUFBSUEsVUFBSixFQUNJLEtBQUssSUFBSUMsSUFBSSxHQUFHQyxNQUFNLENBQUNELElBQVAsQ0FBWUQsVUFBWixDQUFYLEVBQW9DRyxDQUFDLEdBQUcsQ0FBN0MsRUFBZ0RBLENBQUMsR0FBR0YsSUFBSSxDQUFDRyxNQUF6RCxFQUFpRSxFQUFFRCxDQUFuRTtBQUNJLFVBQUlILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBVixJQUF1QixJQUEzQixFQUNJLEtBQUtGLElBQUksQ0FBQ0UsQ0FBRCxDQUFULElBQWdCSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQTFCO0FBRlI7QUFHUDtBQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lKLEVBQUFBLFdBQVcsQ0FBQ00sU0FBWixDQUFzQkMsUUFBdEIsR0FBaUMsQ0FBakM7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lQLEVBQUFBLFdBQVcsQ0FBQ00sU0FBWixDQUFzQkUsTUFBdEIsR0FBK0IsRUFBL0I7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJUixFQUFBQSxXQUFXLENBQUNTLE1BQVosR0FBcUIsU0FBU0EsTUFBVCxDQUFnQlIsVUFBaEIsRUFBNEI7QUFDN0MsV0FBTyxJQUFJRCxXQUFKLENBQWdCQyxVQUFoQixDQUFQO0FBQ0gsR0FGRDtBQUlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lELEVBQUFBLFdBQVcsQ0FBQ1UsTUFBWixHQUFxQixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDbEQsUUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUO0FBQ0osUUFBSUUsT0FBTyxDQUFDSixRQUFSLElBQW9CLElBQXBCLElBQTRCSixNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxVQUFwQyxDQUFoQyxFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixLQUFyQyxFQUF3Q0MsS0FBeEMsQ0FBOENMLE9BQU8sQ0FBQ0osUUFBdEQ7QUFDSixRQUFJSSxPQUFPLENBQUNILE1BQVIsSUFBa0IsSUFBbEIsSUFBMEJMLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFFBQXBDLENBQTlCLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDRSxNQUF6QyxDQUFnRE4sT0FBTyxDQUFDSCxNQUF4RDtBQUNKLFdBQU9JLE1BQVA7QUFDSCxHQVJEO0FBVUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSVosRUFBQUEsV0FBVyxDQUFDa0IsZUFBWixHQUE4QixTQUFTQSxlQUFULENBQXlCUCxPQUF6QixFQUFrQ0MsTUFBbEMsRUFBMEM7QUFDcEUsV0FBTyxLQUFLRixNQUFMLENBQVlDLE9BQVosRUFBcUJDLE1BQXJCLEVBQTZCTyxNQUE3QixFQUFQO0FBQ0gsR0FGRDtBQUlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJbkIsRUFBQUEsV0FBVyxDQUFDb0IsTUFBWixHQUFxQixTQUFTQSxNQUFULENBQWdCQyxNQUFoQixFQUF3QmhCLE1BQXhCLEVBQWdDO0FBQ2pELFFBQUksRUFBRWdCLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRzdCLE9BQU8sQ0FBQ2lCLE1BQVIsQ0FBZVksTUFBZixDQUFUO0FBQ0osUUFBSUMsR0FBRyxHQUFHakIsTUFBTSxLQUFLa0IsU0FBWCxHQUF1QkYsTUFBTSxDQUFDRyxHQUE5QixHQUFvQ0gsTUFBTSxDQUFDSSxHQUFQLEdBQWFwQixNQUEzRDtBQUFBLFFBQW1FTSxPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDRSxXQUFWLEVBQTdFOztBQUNBLFdBQU9xQixNQUFNLENBQUNJLEdBQVAsR0FBYUgsR0FBcEIsRUFBeUI7QUFDckIsVUFBSUksR0FBRyxHQUFHTCxNQUFNLENBQUNOLE1BQVAsRUFBVjs7QUFDQSxjQUFRVyxHQUFHLEtBQUssQ0FBaEI7QUFDQSxhQUFLLENBQUw7QUFDSWYsVUFBQUEsT0FBTyxDQUFDSixRQUFSLEdBQW1CYyxNQUFNLENBQUNMLEtBQVAsRUFBbkI7QUFDQTs7QUFDSixhQUFLLENBQUw7QUFDSUwsVUFBQUEsT0FBTyxDQUFDSCxNQUFSLEdBQWlCYSxNQUFNLENBQUNKLE1BQVAsRUFBakI7QUFDQTs7QUFDSjtBQUNJSSxVQUFBQSxNQUFNLENBQUNNLFFBQVAsQ0FBZ0JELEdBQUcsR0FBRyxDQUF0QjtBQUNBO0FBVEo7QUFXSDs7QUFDRCxXQUFPZixPQUFQO0FBQ0gsR0FuQkQ7QUFxQkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJWCxFQUFBQSxXQUFXLENBQUM0QixlQUFaLEdBQThCLFNBQVNBLGVBQVQsQ0FBeUJQLE1BQXpCLEVBQWlDO0FBQzNELFFBQUksRUFBRUEsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHLElBQUk3QixPQUFKLENBQVk2QixNQUFaLENBQVQ7QUFDSixXQUFPLEtBQUtELE1BQUwsQ0FBWUMsTUFBWixFQUFvQkEsTUFBTSxDQUFDTixNQUFQLEVBQXBCLENBQVA7QUFDSCxHQUpEO0FBTUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lmLEVBQUFBLFdBQVcsQ0FBQzZCLE1BQVosR0FBcUIsU0FBU0EsTUFBVCxDQUFnQmxCLE9BQWhCLEVBQXlCO0FBQzFDLFFBQUksT0FBT0EsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBTyxLQUFLLElBQS9DLEVBQ0ksT0FBTyxpQkFBUDtBQUNKLFFBQUlBLE9BQU8sQ0FBQ0osUUFBUixJQUFvQixJQUFwQixJQUE0QkksT0FBTyxDQUFDRSxjQUFSLENBQXVCLFVBQXZCLENBQWhDLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQ0osUUFBeEIsQ0FBTCxFQUNJLE9BQU8sNEJBQVA7QUFDUixRQUFJSSxPQUFPLENBQUNILE1BQVIsSUFBa0IsSUFBbEIsSUFBMEJHLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixRQUF2QixDQUE5QixFQUNJLElBQUksQ0FBQ2pCLEtBQUssQ0FBQ21DLFFBQU4sQ0FBZXBCLE9BQU8sQ0FBQ0gsTUFBdkIsQ0FBTCxFQUNJLE9BQU8seUJBQVA7QUFDUixXQUFPLElBQVA7QUFDSCxHQVZEO0FBWUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lSLEVBQUFBLFdBQVcsQ0FBQ2dDLFVBQVosR0FBeUIsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDakQsUUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDRSxXQUE1QixFQUNJLE9BQU9pQyxNQUFQO0FBQ0osUUFBSXRCLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUNFLFdBQVYsRUFBZDtBQUNBLFFBQUlpQyxNQUFNLENBQUMxQixRQUFQLElBQW1CLElBQXZCLEVBQ0lJLE9BQU8sQ0FBQ0osUUFBUixHQUFtQjBCLE1BQU0sQ0FBQzFCLFFBQVAsR0FBa0IsQ0FBckM7QUFDSixRQUFJMEIsTUFBTSxDQUFDekIsTUFBUCxJQUFpQixJQUFyQixFQUNJRyxPQUFPLENBQUNILE1BQVIsR0FBaUIwQixNQUFNLENBQUNELE1BQU0sQ0FBQ3pCLE1BQVIsQ0FBdkI7QUFDSixXQUFPRyxPQUFQO0FBQ0gsR0FURDtBQVdBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lYLEVBQUFBLFdBQVcsQ0FBQ21DLFFBQVosR0FBdUIsU0FBU0EsUUFBVCxDQUFrQnhCLE9BQWxCLEVBQTJCeUIsT0FBM0IsRUFBb0M7QUFDdkQsUUFBSSxDQUFDQSxPQUFMLEVBQ0lBLE9BQU8sR0FBRyxFQUFWO0FBQ0osUUFBSUgsTUFBTSxHQUFHLEVBQWI7O0FBQ0EsUUFBSUcsT0FBTyxDQUFDQyxRQUFaLEVBQXNCO0FBQ2xCSixNQUFBQSxNQUFNLENBQUMxQixRQUFQLEdBQWtCLENBQWxCO0FBQ0EwQixNQUFBQSxNQUFNLENBQUN6QixNQUFQLEdBQWdCLEVBQWhCO0FBQ0g7O0FBQ0QsUUFBSUcsT0FBTyxDQUFDSixRQUFSLElBQW9CLElBQXBCLElBQTRCSSxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsVUFBdkIsQ0FBaEMsRUFDSW9CLE1BQU0sQ0FBQzFCLFFBQVAsR0FBa0JJLE9BQU8sQ0FBQ0osUUFBMUI7QUFDSixRQUFJSSxPQUFPLENBQUNILE1BQVIsSUFBa0IsSUFBbEIsSUFBMEJHLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixRQUF2QixDQUE5QixFQUNJb0IsTUFBTSxDQUFDekIsTUFBUCxHQUFnQkcsT0FBTyxDQUFDSCxNQUF4QjtBQUNKLFdBQU95QixNQUFQO0FBQ0gsR0FiRDtBQWVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSWpDLEVBQUFBLFdBQVcsQ0FBQ00sU0FBWixDQUFzQmdDLE1BQXRCLEdBQStCLFNBQVNBLE1BQVQsR0FBa0I7QUFDN0MsV0FBTyxLQUFLQyxXQUFMLENBQWlCSixRQUFqQixDQUEwQixJQUExQixFQUFnQzdDLFNBQVMsQ0FBQ08sSUFBVixDQUFlMkMsYUFBL0MsQ0FBUDtBQUNILEdBRkQ7O0FBSUEsU0FBT3hDLFdBQVA7QUFDSCxDQWhObUIsRUFBcEI7O0FBa05BRixLQUFLLENBQUMyQyxTQUFOLEdBQW1CLFlBQVc7QUFFMUI7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ksV0FBU0EsU0FBVCxDQUFtQnhDLFVBQW5CLEVBQStCO0FBQzNCLFFBQUlBLFVBQUosRUFDSSxLQUFLLElBQUlDLElBQUksR0FBR0MsTUFBTSxDQUFDRCxJQUFQLENBQVlELFVBQVosQ0FBWCxFQUFvQ0csQ0FBQyxHQUFHLENBQTdDLEVBQWdEQSxDQUFDLEdBQUdGLElBQUksQ0FBQ0csTUFBekQsRUFBaUUsRUFBRUQsQ0FBbkU7QUFDSSxVQUFJSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQVYsSUFBdUIsSUFBM0IsRUFDSSxLQUFLRixJQUFJLENBQUNFLENBQUQsQ0FBVCxJQUFnQkgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUExQjtBQUZSO0FBR1A7QUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJcUMsRUFBQUEsU0FBUyxDQUFDbkMsU0FBVixDQUFvQkMsUUFBcEIsR0FBK0IsQ0FBL0I7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lrQyxFQUFBQSxTQUFTLENBQUNuQyxTQUFWLENBQW9Cb0MsTUFBcEIsR0FBNkIsQ0FBN0I7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lELEVBQUFBLFNBQVMsQ0FBQ25DLFNBQVYsQ0FBb0JxQyxLQUFwQixHQUE0QixFQUE1QjtBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lGLEVBQUFBLFNBQVMsQ0FBQ2hDLE1BQVYsR0FBbUIsU0FBU0EsTUFBVCxDQUFnQlIsVUFBaEIsRUFBNEI7QUFDM0MsV0FBTyxJQUFJd0MsU0FBSixDQUFjeEMsVUFBZCxDQUFQO0FBQ0gsR0FGRDtBQUlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l3QyxFQUFBQSxTQUFTLENBQUMvQixNQUFWLEdBQW1CLFNBQVNBLE1BQVQsQ0FBZ0JDLE9BQWhCLEVBQXlCQyxNQUF6QixFQUFpQztBQUNoRCxRQUFJLENBQUNBLE1BQUwsRUFDSUEsTUFBTSxHQUFHbEIsT0FBTyxDQUFDZSxNQUFSLEVBQVQ7QUFDSixRQUFJRSxPQUFPLENBQUNKLFFBQVIsSUFBb0IsSUFBcEIsSUFBNEJKLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFVBQXBDLENBQWhDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLEtBQXJDLEVBQXdDQyxLQUF4QyxDQUE4Q0wsT0FBTyxDQUFDSixRQUF0RDtBQUNKLFFBQUlJLE9BQU8sQ0FBQytCLE1BQVIsSUFBa0IsSUFBbEIsSUFBMEJ2QyxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxRQUFwQyxDQUE5QixFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixNQUFyQyxFQUF5Q0MsS0FBekMsQ0FBK0NMLE9BQU8sQ0FBQytCLE1BQXZEO0FBQ0osUUFBSS9CLE9BQU8sQ0FBQ2dDLEtBQVIsSUFBaUIsSUFBakIsSUFBeUJ4QyxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxPQUFwQyxDQUE3QixFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixNQUFyQyxFQUF5Q0UsTUFBekMsQ0FBZ0ROLE9BQU8sQ0FBQ2dDLEtBQXhEO0FBQ0osV0FBTy9CLE1BQVA7QUFDSCxHQVZEO0FBWUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTZCLEVBQUFBLFNBQVMsQ0FBQ3ZCLGVBQVYsR0FBNEIsU0FBU0EsZUFBVCxDQUF5QlAsT0FBekIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ2xFLFdBQU8sS0FBS0YsTUFBTCxDQUFZQyxPQUFaLEVBQXFCQyxNQUFyQixFQUE2Qk8sTUFBN0IsRUFBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXNCLEVBQUFBLFNBQVMsQ0FBQ3JCLE1BQVYsR0FBbUIsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0JoQixNQUF4QixFQUFnQztBQUMvQyxRQUFJLEVBQUVnQixNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUc3QixPQUFPLENBQUNpQixNQUFSLENBQWVZLE1BQWYsQ0FBVDtBQUNKLFFBQUlDLEdBQUcsR0FBR2pCLE1BQU0sS0FBS2tCLFNBQVgsR0FBdUJGLE1BQU0sQ0FBQ0csR0FBOUIsR0FBb0NILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhcEIsTUFBM0Q7QUFBQSxRQUFtRU0sT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQzJDLFNBQVYsRUFBN0U7O0FBQ0EsV0FBT3BCLE1BQU0sQ0FBQ0ksR0FBUCxHQUFhSCxHQUFwQixFQUF5QjtBQUNyQixVQUFJSSxHQUFHLEdBQUdMLE1BQU0sQ0FBQ04sTUFBUCxFQUFWOztBQUNBLGNBQVFXLEdBQUcsS0FBSyxDQUFoQjtBQUNBLGFBQUssQ0FBTDtBQUNJZixVQUFBQSxPQUFPLENBQUNKLFFBQVIsR0FBbUJjLE1BQU0sQ0FBQ0wsS0FBUCxFQUFuQjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJTCxVQUFBQSxPQUFPLENBQUMrQixNQUFSLEdBQWlCckIsTUFBTSxDQUFDTCxLQUFQLEVBQWpCO0FBQ0E7O0FBQ0osYUFBSyxDQUFMO0FBQ0lMLFVBQUFBLE9BQU8sQ0FBQ2dDLEtBQVIsR0FBZ0J0QixNQUFNLENBQUNKLE1BQVAsRUFBaEI7QUFDQTs7QUFDSjtBQUNJSSxVQUFBQSxNQUFNLENBQUNNLFFBQVAsQ0FBZ0JELEdBQUcsR0FBRyxDQUF0QjtBQUNBO0FBWko7QUFjSDs7QUFDRCxXQUFPZixPQUFQO0FBQ0gsR0F0QkQ7QUF3QkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJOEIsRUFBQUEsU0FBUyxDQUFDYixlQUFWLEdBQTRCLFNBQVNBLGVBQVQsQ0FBeUJQLE1BQXpCLEVBQWlDO0FBQ3pELFFBQUksRUFBRUEsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHLElBQUk3QixPQUFKLENBQVk2QixNQUFaLENBQVQ7QUFDSixXQUFPLEtBQUtELE1BQUwsQ0FBWUMsTUFBWixFQUFvQkEsTUFBTSxDQUFDTixNQUFQLEVBQXBCLENBQVA7QUFDSCxHQUpEO0FBTUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0kwQixFQUFBQSxTQUFTLENBQUNaLE1BQVYsR0FBbUIsU0FBU0EsTUFBVCxDQUFnQmxCLE9BQWhCLEVBQXlCO0FBQ3hDLFFBQUksT0FBT0EsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBTyxLQUFLLElBQS9DLEVBQ0ksT0FBTyxpQkFBUDtBQUNKLFFBQUlBLE9BQU8sQ0FBQ0osUUFBUixJQUFvQixJQUFwQixJQUE0QkksT0FBTyxDQUFDRSxjQUFSLENBQXVCLFVBQXZCLENBQWhDLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQ0osUUFBeEIsQ0FBTCxFQUNJLE9BQU8sNEJBQVA7QUFDUixRQUFJSSxPQUFPLENBQUMrQixNQUFSLElBQWtCLElBQWxCLElBQTBCL0IsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFFBQXZCLENBQTlCLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQytCLE1BQXhCLENBQUwsRUFDSSxPQUFPLDBCQUFQO0FBQ1IsUUFBSS9CLE9BQU8sQ0FBQ2dDLEtBQVIsSUFBaUIsSUFBakIsSUFBeUJoQyxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNtQyxRQUFOLENBQWVwQixPQUFPLENBQUNnQyxLQUF2QixDQUFMLEVBQ0ksT0FBTyx3QkFBUDtBQUNSLFdBQU8sSUFBUDtBQUNILEdBYkQ7QUFlQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSUYsRUFBQUEsU0FBUyxDQUFDVCxVQUFWLEdBQXVCLFNBQVNBLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQy9DLFFBQUlBLE1BQU0sWUFBWW5DLEtBQUssQ0FBQzJDLFNBQTVCLEVBQ0ksT0FBT1IsTUFBUDtBQUNKLFFBQUl0QixPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDMkMsU0FBVixFQUFkO0FBQ0EsUUFBSVIsTUFBTSxDQUFDMUIsUUFBUCxJQUFtQixJQUF2QixFQUNJSSxPQUFPLENBQUNKLFFBQVIsR0FBbUIwQixNQUFNLENBQUMxQixRQUFQLEdBQWtCLENBQXJDO0FBQ0osUUFBSTBCLE1BQU0sQ0FBQ1MsTUFBUCxJQUFpQixJQUFyQixFQUNJL0IsT0FBTyxDQUFDK0IsTUFBUixHQUFpQlQsTUFBTSxDQUFDUyxNQUFQLEdBQWdCLENBQWpDO0FBQ0osUUFBSVQsTUFBTSxDQUFDVSxLQUFQLElBQWdCLElBQXBCLEVBQ0loQyxPQUFPLENBQUNnQyxLQUFSLEdBQWdCVCxNQUFNLENBQUNELE1BQU0sQ0FBQ1UsS0FBUixDQUF0QjtBQUNKLFdBQU9oQyxPQUFQO0FBQ0gsR0FYRDtBQWFBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0k4QixFQUFBQSxTQUFTLENBQUNOLFFBQVYsR0FBcUIsU0FBU0EsUUFBVCxDQUFrQnhCLE9BQWxCLEVBQTJCeUIsT0FBM0IsRUFBb0M7QUFDckQsUUFBSSxDQUFDQSxPQUFMLEVBQ0lBLE9BQU8sR0FBRyxFQUFWO0FBQ0osUUFBSUgsTUFBTSxHQUFHLEVBQWI7O0FBQ0EsUUFBSUcsT0FBTyxDQUFDQyxRQUFaLEVBQXNCO0FBQ2xCSixNQUFBQSxNQUFNLENBQUMxQixRQUFQLEdBQWtCLENBQWxCO0FBQ0EwQixNQUFBQSxNQUFNLENBQUNTLE1BQVAsR0FBZ0IsQ0FBaEI7QUFDQVQsTUFBQUEsTUFBTSxDQUFDVSxLQUFQLEdBQWUsRUFBZjtBQUNIOztBQUNELFFBQUloQyxPQUFPLENBQUNKLFFBQVIsSUFBb0IsSUFBcEIsSUFBNEJJLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixVQUF2QixDQUFoQyxFQUNJb0IsTUFBTSxDQUFDMUIsUUFBUCxHQUFrQkksT0FBTyxDQUFDSixRQUExQjtBQUNKLFFBQUlJLE9BQU8sQ0FBQytCLE1BQVIsSUFBa0IsSUFBbEIsSUFBMEIvQixPQUFPLENBQUNFLGNBQVIsQ0FBdUIsUUFBdkIsQ0FBOUIsRUFDSW9CLE1BQU0sQ0FBQ1MsTUFBUCxHQUFnQi9CLE9BQU8sQ0FBQytCLE1BQXhCO0FBQ0osUUFBSS9CLE9BQU8sQ0FBQ2dDLEtBQVIsSUFBaUIsSUFBakIsSUFBeUJoQyxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFDSW9CLE1BQU0sQ0FBQ1UsS0FBUCxHQUFlaEMsT0FBTyxDQUFDZ0MsS0FBdkI7QUFDSixXQUFPVixNQUFQO0FBQ0gsR0FoQkQ7QUFrQkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJUSxFQUFBQSxTQUFTLENBQUNuQyxTQUFWLENBQW9CZ0MsTUFBcEIsR0FBNkIsU0FBU0EsTUFBVCxHQUFrQjtBQUMzQyxXQUFPLEtBQUtDLFdBQUwsQ0FBaUJKLFFBQWpCLENBQTBCLElBQTFCLEVBQWdDN0MsU0FBUyxDQUFDTyxJQUFWLENBQWUyQyxhQUEvQyxDQUFQO0FBQ0gsR0FGRDs7QUFJQSxTQUFPQyxTQUFQO0FBQ0gsQ0F0T2lCLEVBQWxCOztBQXdPQTNDLEtBQUssQ0FBQzhDLGVBQU4sR0FBeUIsWUFBVztBQUVoQztBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSSxXQUFTQSxlQUFULENBQXlCM0MsVUFBekIsRUFBcUM7QUFDakMsUUFBSUEsVUFBSixFQUNJLEtBQUssSUFBSUMsSUFBSSxHQUFHQyxNQUFNLENBQUNELElBQVAsQ0FBWUQsVUFBWixDQUFYLEVBQW9DRyxDQUFDLEdBQUcsQ0FBN0MsRUFBZ0RBLENBQUMsR0FBR0YsSUFBSSxDQUFDRyxNQUF6RCxFQUFpRSxFQUFFRCxDQUFuRTtBQUNJLFVBQUlILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBVixJQUF1QixJQUEzQixFQUNJLEtBQUtGLElBQUksQ0FBQ0UsQ0FBRCxDQUFULElBQWdCSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQTFCO0FBRlI7QUFHUDtBQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l3QyxFQUFBQSxlQUFlLENBQUN0QyxTQUFoQixDQUEwQnVDLElBQTFCLEdBQWlDLENBQWpDO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJRCxFQUFBQSxlQUFlLENBQUN0QyxTQUFoQixDQUEwQndDLE1BQTFCLEdBQW1DLEVBQW5DO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJRixFQUFBQSxlQUFlLENBQUN0QyxTQUFoQixDQUEwQnlDLGNBQTFCLEdBQTJDLEtBQTNDO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSUgsRUFBQUEsZUFBZSxDQUFDbkMsTUFBaEIsR0FBeUIsU0FBU0EsTUFBVCxDQUFnQlIsVUFBaEIsRUFBNEI7QUFDakQsV0FBTyxJQUFJMkMsZUFBSixDQUFvQjNDLFVBQXBCLENBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTJDLEVBQUFBLGVBQWUsQ0FBQ2xDLE1BQWhCLEdBQXlCLFNBQVNBLE1BQVQsQ0FBZ0JDLE9BQWhCLEVBQXlCQyxNQUF6QixFQUFpQztBQUN0RCxRQUFJLENBQUNBLE1BQUwsRUFDSUEsTUFBTSxHQUFHbEIsT0FBTyxDQUFDZSxNQUFSLEVBQVQ7QUFDSixRQUFJRSxPQUFPLENBQUNrQyxJQUFSLElBQWdCLElBQWhCLElBQXdCMUMsTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsTUFBcEMsQ0FBNUIsRUFDSUMsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsS0FBckMsRUFBd0NDLEtBQXhDLENBQThDTCxPQUFPLENBQUNrQyxJQUF0RDtBQUNKLFFBQUlsQyxPQUFPLENBQUNtQyxNQUFSLElBQWtCLElBQWxCLElBQTBCM0MsTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsUUFBcEMsQ0FBOUIsRUFDSUMsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsTUFBckMsRUFBeUNFLE1BQXpDLENBQWdETixPQUFPLENBQUNtQyxNQUF4RDtBQUNKLFFBQUluQyxPQUFPLENBQUNvQyxjQUFSLElBQTBCLElBQTFCLElBQWtDNUMsTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsZ0JBQXBDLENBQXRDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDaUMsSUFBekMsQ0FBOENyQyxPQUFPLENBQUNvQyxjQUF0RDtBQUNKLFdBQU9uQyxNQUFQO0FBQ0gsR0FWRDtBQVlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lnQyxFQUFBQSxlQUFlLENBQUMxQixlQUFoQixHQUFrQyxTQUFTQSxlQUFULENBQXlCUCxPQUF6QixFQUFrQ0MsTUFBbEMsRUFBMEM7QUFDeEUsV0FBTyxLQUFLRixNQUFMLENBQVlDLE9BQVosRUFBcUJDLE1BQXJCLEVBQTZCTyxNQUE3QixFQUFQO0FBQ0gsR0FGRDtBQUlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJeUIsRUFBQUEsZUFBZSxDQUFDeEIsTUFBaEIsR0FBeUIsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0JoQixNQUF4QixFQUFnQztBQUNyRCxRQUFJLEVBQUVnQixNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUc3QixPQUFPLENBQUNpQixNQUFSLENBQWVZLE1BQWYsQ0FBVDtBQUNKLFFBQUlDLEdBQUcsR0FBR2pCLE1BQU0sS0FBS2tCLFNBQVgsR0FBdUJGLE1BQU0sQ0FBQ0csR0FBOUIsR0FBb0NILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhcEIsTUFBM0Q7QUFBQSxRQUFtRU0sT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQzhDLGVBQVYsRUFBN0U7O0FBQ0EsV0FBT3ZCLE1BQU0sQ0FBQ0ksR0FBUCxHQUFhSCxHQUFwQixFQUF5QjtBQUNyQixVQUFJSSxHQUFHLEdBQUdMLE1BQU0sQ0FBQ04sTUFBUCxFQUFWOztBQUNBLGNBQVFXLEdBQUcsS0FBSyxDQUFoQjtBQUNBLGFBQUssQ0FBTDtBQUNJZixVQUFBQSxPQUFPLENBQUNrQyxJQUFSLEdBQWV4QixNQUFNLENBQUNMLEtBQVAsRUFBZjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJTCxVQUFBQSxPQUFPLENBQUNtQyxNQUFSLEdBQWlCekIsTUFBTSxDQUFDSixNQUFQLEVBQWpCO0FBQ0E7O0FBQ0osYUFBSyxDQUFMO0FBQ0lOLFVBQUFBLE9BQU8sQ0FBQ29DLGNBQVIsR0FBeUIxQixNQUFNLENBQUMyQixJQUFQLEVBQXpCO0FBQ0E7O0FBQ0o7QUFDSTNCLFVBQUFBLE1BQU0sQ0FBQ00sUUFBUCxDQUFnQkQsR0FBRyxHQUFHLENBQXRCO0FBQ0E7QUFaSjtBQWNIOztBQUNELFdBQU9mLE9BQVA7QUFDSCxHQXRCRDtBQXdCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lpQyxFQUFBQSxlQUFlLENBQUNoQixlQUFoQixHQUFrQyxTQUFTQSxlQUFULENBQXlCUCxNQUF6QixFQUFpQztBQUMvRCxRQUFJLEVBQUVBLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRyxJQUFJN0IsT0FBSixDQUFZNkIsTUFBWixDQUFUO0FBQ0osV0FBTyxLQUFLRCxNQUFMLENBQVlDLE1BQVosRUFBb0JBLE1BQU0sQ0FBQ04sTUFBUCxFQUFwQixDQUFQO0FBQ0gsR0FKRDtBQU1BO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJNkIsRUFBQUEsZUFBZSxDQUFDZixNQUFoQixHQUF5QixTQUFTQSxNQUFULENBQWdCbEIsT0FBaEIsRUFBeUI7QUFDOUMsUUFBSSxPQUFPQSxPQUFQLEtBQW1CLFFBQW5CLElBQStCQSxPQUFPLEtBQUssSUFBL0MsRUFDSSxPQUFPLGlCQUFQO0FBQ0osUUFBSUEsT0FBTyxDQUFDa0MsSUFBUixJQUFnQixJQUFoQixJQUF3QmxDLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixNQUF2QixDQUE1QixFQUNJLElBQUksQ0FBQ2pCLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUNrQyxJQUF4QixDQUFMLEVBQ0ksT0FBTyx3QkFBUDtBQUNSLFFBQUlsQyxPQUFPLENBQUNtQyxNQUFSLElBQWtCLElBQWxCLElBQTBCbkMsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFFBQXZCLENBQTlCLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDbUMsUUFBTixDQUFlcEIsT0FBTyxDQUFDbUMsTUFBdkIsQ0FBTCxFQUNJLE9BQU8seUJBQVA7QUFDUixRQUFJbkMsT0FBTyxDQUFDb0MsY0FBUixJQUEwQixJQUExQixJQUFrQ3BDLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixnQkFBdkIsQ0FBdEMsRUFDSSxJQUFJLE9BQU9GLE9BQU8sQ0FBQ29DLGNBQWYsS0FBa0MsU0FBdEMsRUFDSSxPQUFPLGtDQUFQO0FBQ1IsV0FBTyxJQUFQO0FBQ0gsR0FiRDtBQWVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJSCxFQUFBQSxlQUFlLENBQUNaLFVBQWhCLEdBQTZCLFNBQVNBLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQ3JELFFBQUlBLE1BQU0sWUFBWW5DLEtBQUssQ0FBQzhDLGVBQTVCLEVBQ0ksT0FBT1gsTUFBUDtBQUNKLFFBQUl0QixPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDOEMsZUFBVixFQUFkO0FBQ0EsUUFBSVgsTUFBTSxDQUFDWSxJQUFQLElBQWUsSUFBbkIsRUFDSWxDLE9BQU8sQ0FBQ2tDLElBQVIsR0FBZVosTUFBTSxDQUFDWSxJQUFQLEdBQWMsQ0FBN0I7QUFDSixRQUFJWixNQUFNLENBQUNhLE1BQVAsSUFBaUIsSUFBckIsRUFDSW5DLE9BQU8sQ0FBQ21DLE1BQVIsR0FBaUJaLE1BQU0sQ0FBQ0QsTUFBTSxDQUFDYSxNQUFSLENBQXZCO0FBQ0osUUFBSWIsTUFBTSxDQUFDYyxjQUFQLElBQXlCLElBQTdCLEVBQ0lwQyxPQUFPLENBQUNvQyxjQUFSLEdBQXlCRSxPQUFPLENBQUNoQixNQUFNLENBQUNjLGNBQVIsQ0FBaEM7QUFDSixXQUFPcEMsT0FBUDtBQUNILEdBWEQ7QUFhQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJaUMsRUFBQUEsZUFBZSxDQUFDVCxRQUFoQixHQUEyQixTQUFTQSxRQUFULENBQWtCeEIsT0FBbEIsRUFBMkJ5QixPQUEzQixFQUFvQztBQUMzRCxRQUFJLENBQUNBLE9BQUwsRUFDSUEsT0FBTyxHQUFHLEVBQVY7QUFDSixRQUFJSCxNQUFNLEdBQUcsRUFBYjs7QUFDQSxRQUFJRyxPQUFPLENBQUNDLFFBQVosRUFBc0I7QUFDbEJKLE1BQUFBLE1BQU0sQ0FBQ1ksSUFBUCxHQUFjLENBQWQ7QUFDQVosTUFBQUEsTUFBTSxDQUFDYSxNQUFQLEdBQWdCLEVBQWhCO0FBQ0FiLE1BQUFBLE1BQU0sQ0FBQ2MsY0FBUCxHQUF3QixLQUF4QjtBQUNIOztBQUNELFFBQUlwQyxPQUFPLENBQUNrQyxJQUFSLElBQWdCLElBQWhCLElBQXdCbEMsT0FBTyxDQUFDRSxjQUFSLENBQXVCLE1BQXZCLENBQTVCLEVBQ0lvQixNQUFNLENBQUNZLElBQVAsR0FBY2xDLE9BQU8sQ0FBQ2tDLElBQXRCO0FBQ0osUUFBSWxDLE9BQU8sQ0FBQ21DLE1BQVIsSUFBa0IsSUFBbEIsSUFBMEJuQyxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsUUFBdkIsQ0FBOUIsRUFDSW9CLE1BQU0sQ0FBQ2EsTUFBUCxHQUFnQm5DLE9BQU8sQ0FBQ21DLE1BQXhCO0FBQ0osUUFBSW5DLE9BQU8sQ0FBQ29DLGNBQVIsSUFBMEIsSUFBMUIsSUFBa0NwQyxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsZ0JBQXZCLENBQXRDLEVBQ0lvQixNQUFNLENBQUNjLGNBQVAsR0FBd0JwQyxPQUFPLENBQUNvQyxjQUFoQztBQUNKLFdBQU9kLE1BQVA7QUFDSCxHQWhCRDtBQWtCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lXLEVBQUFBLGVBQWUsQ0FBQ3RDLFNBQWhCLENBQTBCZ0MsTUFBMUIsR0FBbUMsU0FBU0EsTUFBVCxHQUFrQjtBQUNqRCxXQUFPLEtBQUtDLFdBQUwsQ0FBaUJKLFFBQWpCLENBQTBCLElBQTFCLEVBQWdDN0MsU0FBUyxDQUFDTyxJQUFWLENBQWUyQyxhQUEvQyxDQUFQO0FBQ0gsR0FGRDs7QUFJQSxTQUFPSSxlQUFQO0FBQ0gsQ0F0T3VCLEVBQXhCOztBQXdPQTlDLEtBQUssQ0FBQ29ELFFBQU4sR0FBa0IsWUFBVztBQUV6QjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ksV0FBU0EsUUFBVCxDQUFrQmpELFVBQWxCLEVBQThCO0FBQzFCLFFBQUlBLFVBQUosRUFDSSxLQUFLLElBQUlDLElBQUksR0FBR0MsTUFBTSxDQUFDRCxJQUFQLENBQVlELFVBQVosQ0FBWCxFQUFvQ0csQ0FBQyxHQUFHLENBQTdDLEVBQWdEQSxDQUFDLEdBQUdGLElBQUksQ0FBQ0csTUFBekQsRUFBaUUsRUFBRUQsQ0FBbkU7QUFDSSxVQUFJSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQVYsSUFBdUIsSUFBM0IsRUFDSSxLQUFLRixJQUFJLENBQUNFLENBQUQsQ0FBVCxJQUFnQkgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUExQjtBQUZSO0FBR1A7QUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJOEMsRUFBQUEsUUFBUSxDQUFDNUMsU0FBVCxDQUFtQjZDLE1BQW5CLEdBQTRCLElBQTVCO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJRCxFQUFBQSxRQUFRLENBQUM1QyxTQUFULENBQW1COEMsT0FBbkIsR0FBNkJ4RCxLQUFLLENBQUN5RCxTQUFOLENBQWdCLEVBQWhCLENBQTdCO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSUgsRUFBQUEsUUFBUSxDQUFDekMsTUFBVCxHQUFrQixTQUFTQSxNQUFULENBQWdCUixVQUFoQixFQUE0QjtBQUMxQyxXQUFPLElBQUlpRCxRQUFKLENBQWFqRCxVQUFiLENBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSWlELEVBQUFBLFFBQVEsQ0FBQ3hDLE1BQVQsR0FBa0IsU0FBU0EsTUFBVCxDQUFnQkMsT0FBaEIsRUFBeUJDLE1BQXpCLEVBQWlDO0FBQy9DLFFBQUksQ0FBQ0EsTUFBTCxFQUNJQSxNQUFNLEdBQUdsQixPQUFPLENBQUNlLE1BQVIsRUFBVDtBQUNKLFFBQUlFLE9BQU8sQ0FBQ3dDLE1BQVIsSUFBa0IsSUFBbEIsSUFBMEJoRCxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxRQUFwQyxDQUE5QixFQUNJYixLQUFLLENBQUNvRCxRQUFOLENBQWVJLE1BQWYsQ0FBc0I1QyxNQUF0QixDQUE2QkMsT0FBTyxDQUFDd0MsTUFBckMsRUFBNkN2QyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixNQUFyQyxFQUF5Q3dDLElBQXpDLEVBQTdDLEVBQThGcEMsTUFBOUY7QUFDSixRQUFJUixPQUFPLENBQUN5QyxPQUFSLElBQW1CLElBQW5CLElBQTJCakQsTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsU0FBcEMsQ0FBL0IsRUFDSUMsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsTUFBckMsRUFBeUN5QyxLQUF6QyxDQUErQzdDLE9BQU8sQ0FBQ3lDLE9BQXZEO0FBQ0osV0FBT3hDLE1BQVA7QUFDSCxHQVJEO0FBVUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXNDLEVBQUFBLFFBQVEsQ0FBQ2hDLGVBQVQsR0FBMkIsU0FBU0EsZUFBVCxDQUF5QlAsT0FBekIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ2pFLFdBQU8sS0FBS0YsTUFBTCxDQUFZQyxPQUFaLEVBQXFCQyxNQUFyQixFQUE2Qk8sTUFBN0IsRUFBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSStCLEVBQUFBLFFBQVEsQ0FBQzlCLE1BQVQsR0FBa0IsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0JoQixNQUF4QixFQUFnQztBQUM5QyxRQUFJLEVBQUVnQixNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUc3QixPQUFPLENBQUNpQixNQUFSLENBQWVZLE1BQWYsQ0FBVDtBQUNKLFFBQUlDLEdBQUcsR0FBR2pCLE1BQU0sS0FBS2tCLFNBQVgsR0FBdUJGLE1BQU0sQ0FBQ0csR0FBOUIsR0FBb0NILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhcEIsTUFBM0Q7QUFBQSxRQUFtRU0sT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQ29ELFFBQVYsRUFBN0U7O0FBQ0EsV0FBTzdCLE1BQU0sQ0FBQ0ksR0FBUCxHQUFhSCxHQUFwQixFQUF5QjtBQUNyQixVQUFJSSxHQUFHLEdBQUdMLE1BQU0sQ0FBQ04sTUFBUCxFQUFWOztBQUNBLGNBQVFXLEdBQUcsS0FBSyxDQUFoQjtBQUNBLGFBQUssQ0FBTDtBQUNJZixVQUFBQSxPQUFPLENBQUN3QyxNQUFSLEdBQWlCckQsS0FBSyxDQUFDb0QsUUFBTixDQUFlSSxNQUFmLENBQXNCbEMsTUFBdEIsQ0FBNkJDLE1BQTdCLEVBQXFDQSxNQUFNLENBQUNOLE1BQVAsRUFBckMsQ0FBakI7QUFDQTs7QUFDSixhQUFLLENBQUw7QUFDSUosVUFBQUEsT0FBTyxDQUFDeUMsT0FBUixHQUFrQi9CLE1BQU0sQ0FBQ21DLEtBQVAsRUFBbEI7QUFDQTs7QUFDSjtBQUNJbkMsVUFBQUEsTUFBTSxDQUFDTSxRQUFQLENBQWdCRCxHQUFHLEdBQUcsQ0FBdEI7QUFDQTtBQVRKO0FBV0g7O0FBQ0QsV0FBT2YsT0FBUDtBQUNILEdBbkJEO0FBcUJBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXVDLEVBQUFBLFFBQVEsQ0FBQ3RCLGVBQVQsR0FBMkIsU0FBU0EsZUFBVCxDQUF5QlAsTUFBekIsRUFBaUM7QUFDeEQsUUFBSSxFQUFFQSxNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUcsSUFBSTdCLE9BQUosQ0FBWTZCLE1BQVosQ0FBVDtBQUNKLFdBQU8sS0FBS0QsTUFBTCxDQUFZQyxNQUFaLEVBQW9CQSxNQUFNLENBQUNOLE1BQVAsRUFBcEIsQ0FBUDtBQUNILEdBSkQ7QUFNQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSW1DLEVBQUFBLFFBQVEsQ0FBQ3JCLE1BQVQsR0FBa0IsU0FBU0EsTUFBVCxDQUFnQmxCLE9BQWhCLEVBQXlCO0FBQ3ZDLFFBQUksT0FBT0EsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBTyxLQUFLLElBQS9DLEVBQ0ksT0FBTyxpQkFBUDs7QUFDSixRQUFJQSxPQUFPLENBQUN3QyxNQUFSLElBQWtCLElBQWxCLElBQTBCeEMsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFFBQXZCLENBQTlCLEVBQWdFO0FBQzVELFVBQUk4QixLQUFLLEdBQUc3QyxLQUFLLENBQUNvRCxRQUFOLENBQWVJLE1BQWYsQ0FBc0J6QixNQUF0QixDQUE2QmxCLE9BQU8sQ0FBQ3dDLE1BQXJDLENBQVo7QUFDQSxVQUFJUixLQUFKLEVBQ0ksT0FBTyxZQUFZQSxLQUFuQjtBQUNQOztBQUNELFFBQUloQyxPQUFPLENBQUN5QyxPQUFSLElBQW1CLElBQW5CLElBQTJCekMsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFNBQXZCLENBQS9CLEVBQ0ksSUFBSSxFQUFFRixPQUFPLENBQUN5QyxPQUFSLElBQW1CLE9BQU96QyxPQUFPLENBQUN5QyxPQUFSLENBQWdCL0MsTUFBdkIsS0FBa0MsUUFBckQsSUFBaUVULEtBQUssQ0FBQ21DLFFBQU4sQ0FBZXBCLE9BQU8sQ0FBQ3lDLE9BQXZCLENBQW5FLENBQUosRUFDSSxPQUFPLDBCQUFQO0FBQ1IsV0FBTyxJQUFQO0FBQ0gsR0FaRDtBQWNBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJRixFQUFBQSxRQUFRLENBQUNsQixVQUFULEdBQXNCLFNBQVNBLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQzlDLFFBQUlBLE1BQU0sWUFBWW5DLEtBQUssQ0FBQ29ELFFBQTVCLEVBQ0ksT0FBT2pCLE1BQVA7QUFDSixRQUFJdEIsT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQ29ELFFBQVYsRUFBZDs7QUFDQSxRQUFJakIsTUFBTSxDQUFDa0IsTUFBUCxJQUFpQixJQUFyQixFQUEyQjtBQUN2QixVQUFJLE9BQU9sQixNQUFNLENBQUNrQixNQUFkLEtBQXlCLFFBQTdCLEVBQ0ksTUFBTU0sU0FBUyxDQUFDLG1DQUFELENBQWY7QUFDSjlDLE1BQUFBLE9BQU8sQ0FBQ3dDLE1BQVIsR0FBaUJyRCxLQUFLLENBQUNvRCxRQUFOLENBQWVJLE1BQWYsQ0FBc0J0QixVQUF0QixDQUFpQ0MsTUFBTSxDQUFDa0IsTUFBeEMsQ0FBakI7QUFDSDs7QUFDRCxRQUFJbEIsTUFBTSxDQUFDbUIsT0FBUCxJQUFrQixJQUF0QixFQUNJLElBQUksT0FBT25CLE1BQU0sQ0FBQ21CLE9BQWQsS0FBMEIsUUFBOUIsRUFDSXhELEtBQUssQ0FBQzhELE1BQU4sQ0FBYXRDLE1BQWIsQ0FBb0JhLE1BQU0sQ0FBQ21CLE9BQTNCLEVBQW9DekMsT0FBTyxDQUFDeUMsT0FBUixHQUFrQnhELEtBQUssQ0FBQ3lELFNBQU4sQ0FBZ0J6RCxLQUFLLENBQUM4RCxNQUFOLENBQWFyRCxNQUFiLENBQW9CNEIsTUFBTSxDQUFDbUIsT0FBM0IsQ0FBaEIsQ0FBdEQsRUFBNEcsQ0FBNUcsRUFESixLQUVLLElBQUluQixNQUFNLENBQUNtQixPQUFQLENBQWUvQyxNQUFuQixFQUNETSxPQUFPLENBQUN5QyxPQUFSLEdBQWtCbkIsTUFBTSxDQUFDbUIsT0FBekI7QUFDUixXQUFPekMsT0FBUDtBQUNILEdBZkQ7QUFpQkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXVDLEVBQUFBLFFBQVEsQ0FBQ2YsUUFBVCxHQUFvQixTQUFTQSxRQUFULENBQWtCeEIsT0FBbEIsRUFBMkJ5QixPQUEzQixFQUFvQztBQUNwRCxRQUFJLENBQUNBLE9BQUwsRUFDSUEsT0FBTyxHQUFHLEVBQVY7QUFDSixRQUFJSCxNQUFNLEdBQUcsRUFBYjs7QUFDQSxRQUFJRyxPQUFPLENBQUNDLFFBQVosRUFBc0I7QUFDbEJKLE1BQUFBLE1BQU0sQ0FBQ2tCLE1BQVAsR0FBZ0IsSUFBaEI7QUFDQSxVQUFJZixPQUFPLENBQUNvQixLQUFSLEtBQWtCdEIsTUFBdEIsRUFDSUQsTUFBTSxDQUFDbUIsT0FBUCxHQUFpQixFQUFqQixDQURKLEtBRUs7QUFDRG5CLFFBQUFBLE1BQU0sQ0FBQ21CLE9BQVAsR0FBaUIsRUFBakI7QUFDQSxZQUFJaEIsT0FBTyxDQUFDb0IsS0FBUixLQUFrQkcsS0FBdEIsRUFDSTFCLE1BQU0sQ0FBQ21CLE9BQVAsR0FBaUJ4RCxLQUFLLENBQUN5RCxTQUFOLENBQWdCcEIsTUFBTSxDQUFDbUIsT0FBdkIsQ0FBakI7QUFDUDtBQUNKOztBQUNELFFBQUl6QyxPQUFPLENBQUN3QyxNQUFSLElBQWtCLElBQWxCLElBQTBCeEMsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFFBQXZCLENBQTlCLEVBQ0lvQixNQUFNLENBQUNrQixNQUFQLEdBQWdCckQsS0FBSyxDQUFDb0QsUUFBTixDQUFlSSxNQUFmLENBQXNCbkIsUUFBdEIsQ0FBK0J4QixPQUFPLENBQUN3QyxNQUF2QyxFQUErQ2YsT0FBL0MsQ0FBaEI7QUFDSixRQUFJekIsT0FBTyxDQUFDeUMsT0FBUixJQUFtQixJQUFuQixJQUEyQnpDLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixTQUF2QixDQUEvQixFQUNJb0IsTUFBTSxDQUFDbUIsT0FBUCxHQUFpQmhCLE9BQU8sQ0FBQ29CLEtBQVIsS0FBa0J0QixNQUFsQixHQUEyQnRDLEtBQUssQ0FBQzhELE1BQU4sQ0FBYWhELE1BQWIsQ0FBb0JDLE9BQU8sQ0FBQ3lDLE9BQTVCLEVBQXFDLENBQXJDLEVBQXdDekMsT0FBTyxDQUFDeUMsT0FBUixDQUFnQi9DLE1BQXhELENBQTNCLEdBQTZGK0IsT0FBTyxDQUFDb0IsS0FBUixLQUFrQkcsS0FBbEIsR0FBMEJBLEtBQUssQ0FBQ3JELFNBQU4sQ0FBZ0JzRCxLQUFoQixDQUFzQjlDLElBQXRCLENBQTJCSCxPQUFPLENBQUN5QyxPQUFuQyxDQUExQixHQUF3RXpDLE9BQU8sQ0FBQ3lDLE9BQTlMO0FBQ0osV0FBT25CLE1BQVA7QUFDSCxHQW5CRDtBQXFCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lpQixFQUFBQSxRQUFRLENBQUM1QyxTQUFULENBQW1CZ0MsTUFBbkIsR0FBNEIsU0FBU0EsTUFBVCxHQUFrQjtBQUMxQyxXQUFPLEtBQUtDLFdBQUwsQ0FBaUJKLFFBQWpCLENBQTBCLElBQTFCLEVBQWdDN0MsU0FBUyxDQUFDTyxJQUFWLENBQWUyQyxhQUEvQyxDQUFQO0FBQ0gsR0FGRDs7QUFJQVUsRUFBQUEsUUFBUSxDQUFDSSxNQUFULEdBQW1CLFlBQVc7QUFFMUI7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDUSxhQUFTQSxNQUFULENBQWdCckQsVUFBaEIsRUFBNEI7QUFDeEIsVUFBSUEsVUFBSixFQUNJLEtBQUssSUFBSUMsSUFBSSxHQUFHQyxNQUFNLENBQUNELElBQVAsQ0FBWUQsVUFBWixDQUFYLEVBQW9DRyxDQUFDLEdBQUcsQ0FBN0MsRUFBZ0RBLENBQUMsR0FBR0YsSUFBSSxDQUFDRyxNQUF6RCxFQUFpRSxFQUFFRCxDQUFuRTtBQUNJLFlBQUlILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBVixJQUF1QixJQUEzQixFQUNJLEtBQUtGLElBQUksQ0FBQ0UsQ0FBRCxDQUFULElBQWdCSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQTFCO0FBRlI7QUFHUDtBQUVEO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ1FrRCxJQUFBQSxNQUFNLENBQUNoRCxTQUFQLENBQWlCdUQsV0FBakIsR0FBK0IsSUFBL0I7QUFFQTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ1FQLElBQUFBLE1BQU0sQ0FBQ2hELFNBQVAsQ0FBaUJ3RCxTQUFqQixHQUE2QixJQUE3QjtBQUVBO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDUVIsSUFBQUEsTUFBTSxDQUFDaEQsU0FBUCxDQUFpQnlELGVBQWpCLEdBQW1DLElBQW5DO0FBRUE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNRVCxJQUFBQSxNQUFNLENBQUNoRCxTQUFQLENBQWlCMEQsS0FBakIsR0FBeUIsSUFBekIsQ0F6RDBCLENBMkQxQjs7QUFDQSxRQUFJQyxZQUFKO0FBRUE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNROUQsSUFBQUEsTUFBTSxDQUFDK0QsY0FBUCxDQUFzQlosTUFBTSxDQUFDaEQsU0FBN0IsRUFBd0MsTUFBeEMsRUFBZ0Q7QUFDNUM2RCxNQUFBQSxHQUFHLEVBQUV2RSxLQUFLLENBQUN3RSxXQUFOLENBQWtCSCxZQUFZLEdBQUcsQ0FBQyxhQUFELEVBQWdCLFdBQWhCLEVBQTZCLGlCQUE3QixFQUFnRCxPQUFoRCxDQUFqQyxDQUR1QztBQUU1Q0ksTUFBQUEsR0FBRyxFQUFFekUsS0FBSyxDQUFDMEUsV0FBTixDQUFrQkwsWUFBbEI7QUFGdUMsS0FBaEQ7QUFLQTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNRWCxJQUFBQSxNQUFNLENBQUM3QyxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JSLFVBQWhCLEVBQTRCO0FBQ3hDLGFBQU8sSUFBSXFELE1BQUosQ0FBV3JELFVBQVgsQ0FBUDtBQUNILEtBRkQ7QUFJQTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNRcUQsSUFBQUEsTUFBTSxDQUFDNUMsTUFBUCxHQUFnQixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDN0MsVUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUO0FBQ0osVUFBSUUsT0FBTyxDQUFDa0QsV0FBUixJQUF1QixJQUF2QixJQUErQjFELE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLGFBQXBDLENBQW5DLEVBQ0liLEtBQUssQ0FBQ0UsV0FBTixDQUFrQlUsTUFBbEIsQ0FBeUJDLE9BQU8sQ0FBQ2tELFdBQWpDLEVBQThDakQsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsUUFBckMsRUFBeUN3QyxJQUF6QyxFQUE5QyxFQUErRnBDLE1BQS9GO0FBQ0osVUFBSVIsT0FBTyxDQUFDbUQsU0FBUixJQUFxQixJQUFyQixJQUE2QjNELE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFdBQXBDLENBQWpDLEVBQ0liLEtBQUssQ0FBQzJDLFNBQU4sQ0FBZ0IvQixNQUFoQixDQUF1QkMsT0FBTyxDQUFDbUQsU0FBL0IsRUFBMENsRCxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixRQUFyQyxFQUF5Q3dDLElBQXpDLEVBQTFDLEVBQTJGcEMsTUFBM0Y7QUFDSixVQUFJUixPQUFPLENBQUNvRCxlQUFSLElBQTJCLElBQTNCLElBQW1DNUQsTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsaUJBQXBDLENBQXZDLEVBQ0liLEtBQUssQ0FBQzhDLGVBQU4sQ0FBc0JsQyxNQUF0QixDQUE2QkMsT0FBTyxDQUFDb0QsZUFBckMsRUFBc0RuRCxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixRQUFyQyxFQUF5Q3dDLElBQXpDLEVBQXRELEVBQXVHcEMsTUFBdkc7QUFDSixVQUFJUixPQUFPLENBQUNxRCxLQUFSLElBQWlCLElBQWpCLElBQXlCN0QsTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsT0FBcEMsQ0FBN0IsRUFDSUMsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsUUFBckMsRUFBeUNFLE1BQXpDLENBQWdETixPQUFPLENBQUNxRCxLQUF4RDtBQUNKLGFBQU9wRCxNQUFQO0FBQ0gsS0FaRDtBQWNBO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ1EwQyxJQUFBQSxNQUFNLENBQUNwQyxlQUFQLEdBQXlCLFNBQVNBLGVBQVQsQ0FBeUJQLE9BQXpCLEVBQWtDQyxNQUFsQyxFQUEwQztBQUMvRCxhQUFPLEtBQUtGLE1BQUwsQ0FBWUMsT0FBWixFQUFxQkMsTUFBckIsRUFBNkJPLE1BQTdCLEVBQVA7QUFDSCxLQUZEO0FBSUE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ1FtQyxJQUFBQSxNQUFNLENBQUNsQyxNQUFQLEdBQWdCLFNBQVNBLE1BQVQsQ0FBZ0JDLE1BQWhCLEVBQXdCaEIsTUFBeEIsRUFBZ0M7QUFDNUMsVUFBSSxFQUFFZ0IsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHN0IsT0FBTyxDQUFDaUIsTUFBUixDQUFlWSxNQUFmLENBQVQ7QUFDSixVQUFJQyxHQUFHLEdBQUdqQixNQUFNLEtBQUtrQixTQUFYLEdBQXVCRixNQUFNLENBQUNHLEdBQTlCLEdBQW9DSCxNQUFNLENBQUNJLEdBQVAsR0FBYXBCLE1BQTNEO0FBQUEsVUFBbUVNLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUNvRCxRQUFOLENBQWVJLE1BQW5CLEVBQTdFOztBQUNBLGFBQU9qQyxNQUFNLENBQUNJLEdBQVAsR0FBYUgsR0FBcEIsRUFBeUI7QUFDckIsWUFBSUksR0FBRyxHQUFHTCxNQUFNLENBQUNOLE1BQVAsRUFBVjs7QUFDQSxnQkFBUVcsR0FBRyxLQUFLLENBQWhCO0FBQ0EsZUFBSyxDQUFMO0FBQ0lmLFlBQUFBLE9BQU8sQ0FBQ2tELFdBQVIsR0FBc0IvRCxLQUFLLENBQUNFLFdBQU4sQ0FBa0JvQixNQUFsQixDQUF5QkMsTUFBekIsRUFBaUNBLE1BQU0sQ0FBQ04sTUFBUCxFQUFqQyxDQUF0QjtBQUNBOztBQUNKLGVBQUssQ0FBTDtBQUNJSixZQUFBQSxPQUFPLENBQUNtRCxTQUFSLEdBQW9CaEUsS0FBSyxDQUFDMkMsU0FBTixDQUFnQnJCLE1BQWhCLENBQXVCQyxNQUF2QixFQUErQkEsTUFBTSxDQUFDTixNQUFQLEVBQS9CLENBQXBCO0FBQ0E7O0FBQ0osZUFBSyxDQUFMO0FBQ0lKLFlBQUFBLE9BQU8sQ0FBQ29ELGVBQVIsR0FBMEJqRSxLQUFLLENBQUM4QyxlQUFOLENBQXNCeEIsTUFBdEIsQ0FBNkJDLE1BQTdCLEVBQXFDQSxNQUFNLENBQUNOLE1BQVAsRUFBckMsQ0FBMUI7QUFDQTs7QUFDSixlQUFLLENBQUw7QUFDSUosWUFBQUEsT0FBTyxDQUFDcUQsS0FBUixHQUFnQjNDLE1BQU0sQ0FBQ0osTUFBUCxFQUFoQjtBQUNBOztBQUNKO0FBQ0lJLFlBQUFBLE1BQU0sQ0FBQ00sUUFBUCxDQUFnQkQsR0FBRyxHQUFHLENBQXRCO0FBQ0E7QUFmSjtBQWlCSDs7QUFDRCxhQUFPZixPQUFQO0FBQ0gsS0F6QkQ7QUEyQkE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNRMkMsSUFBQUEsTUFBTSxDQUFDMUIsZUFBUCxHQUF5QixTQUFTQSxlQUFULENBQXlCUCxNQUF6QixFQUFpQztBQUN0RCxVQUFJLEVBQUVBLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRyxJQUFJN0IsT0FBSixDQUFZNkIsTUFBWixDQUFUO0FBQ0osYUFBTyxLQUFLRCxNQUFMLENBQVlDLE1BQVosRUFBb0JBLE1BQU0sQ0FBQ04sTUFBUCxFQUFwQixDQUFQO0FBQ0gsS0FKRDtBQU1BO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNRdUMsSUFBQUEsTUFBTSxDQUFDekIsTUFBUCxHQUFnQixTQUFTQSxNQUFULENBQWdCbEIsT0FBaEIsRUFBeUI7QUFDckMsVUFBSSxPQUFPQSxPQUFQLEtBQW1CLFFBQW5CLElBQStCQSxPQUFPLEtBQUssSUFBL0MsRUFDSSxPQUFPLGlCQUFQO0FBQ0osVUFBSVYsVUFBVSxHQUFHLEVBQWpCOztBQUNBLFVBQUlVLE9BQU8sQ0FBQ2tELFdBQVIsSUFBdUIsSUFBdkIsSUFBK0JsRCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsYUFBdkIsQ0FBbkMsRUFBMEU7QUFDdEVaLFFBQUFBLFVBQVUsQ0FBQ3NFLElBQVgsR0FBa0IsQ0FBbEI7QUFDQTtBQUNJLGNBQUk1QixLQUFLLEdBQUc3QyxLQUFLLENBQUNFLFdBQU4sQ0FBa0I2QixNQUFsQixDQUF5QmxCLE9BQU8sQ0FBQ2tELFdBQWpDLENBQVo7QUFDQSxjQUFJbEIsS0FBSixFQUNJLE9BQU8saUJBQWlCQSxLQUF4QjtBQUNQO0FBQ0o7O0FBQ0QsVUFBSWhDLE9BQU8sQ0FBQ21ELFNBQVIsSUFBcUIsSUFBckIsSUFBNkJuRCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsV0FBdkIsQ0FBakMsRUFBc0U7QUFDbEUsWUFBSVosVUFBVSxDQUFDc0UsSUFBWCxLQUFvQixDQUF4QixFQUNJLE9BQU8sdUJBQVA7QUFDSnRFLFFBQUFBLFVBQVUsQ0FBQ3NFLElBQVgsR0FBa0IsQ0FBbEI7QUFDQTtBQUNJLGNBQUk1QixLQUFLLEdBQUc3QyxLQUFLLENBQUMyQyxTQUFOLENBQWdCWixNQUFoQixDQUF1QmxCLE9BQU8sQ0FBQ21ELFNBQS9CLENBQVo7QUFDQSxjQUFJbkIsS0FBSixFQUNJLE9BQU8sZUFBZUEsS0FBdEI7QUFDUDtBQUNKOztBQUNELFVBQUloQyxPQUFPLENBQUNvRCxlQUFSLElBQTJCLElBQTNCLElBQW1DcEQsT0FBTyxDQUFDRSxjQUFSLENBQXVCLGlCQUF2QixDQUF2QyxFQUFrRjtBQUM5RSxZQUFJWixVQUFVLENBQUNzRSxJQUFYLEtBQW9CLENBQXhCLEVBQ0ksT0FBTyx1QkFBUDtBQUNKdEUsUUFBQUEsVUFBVSxDQUFDc0UsSUFBWCxHQUFrQixDQUFsQjtBQUNBO0FBQ0ksY0FBSTVCLEtBQUssR0FBRzdDLEtBQUssQ0FBQzhDLGVBQU4sQ0FBc0JmLE1BQXRCLENBQTZCbEIsT0FBTyxDQUFDb0QsZUFBckMsQ0FBWjtBQUNBLGNBQUlwQixLQUFKLEVBQ0ksT0FBTyxxQkFBcUJBLEtBQTVCO0FBQ1A7QUFDSjs7QUFDRCxVQUFJaEMsT0FBTyxDQUFDcUQsS0FBUixJQUFpQixJQUFqQixJQUF5QnJELE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixPQUF2QixDQUE3QixFQUE4RDtBQUMxRCxZQUFJWixVQUFVLENBQUNzRSxJQUFYLEtBQW9CLENBQXhCLEVBQ0ksT0FBTyx1QkFBUDtBQUNKdEUsUUFBQUEsVUFBVSxDQUFDc0UsSUFBWCxHQUFrQixDQUFsQjtBQUNBLFlBQUksQ0FBQzNFLEtBQUssQ0FBQ21DLFFBQU4sQ0FBZXBCLE9BQU8sQ0FBQ3FELEtBQXZCLENBQUwsRUFDSSxPQUFPLHdCQUFQO0FBQ1A7O0FBQ0QsYUFBTyxJQUFQO0FBQ0gsS0F4Q0Q7QUEwQ0E7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ1FWLElBQUFBLE1BQU0sQ0FBQ3RCLFVBQVAsR0FBb0IsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDNUMsVUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDb0QsUUFBTixDQUFlSSxNQUFyQyxFQUNJLE9BQU9yQixNQUFQO0FBQ0osVUFBSXRCLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUNvRCxRQUFOLENBQWVJLE1BQW5CLEVBQWQ7O0FBQ0EsVUFBSXJCLE1BQU0sQ0FBQzRCLFdBQVAsSUFBc0IsSUFBMUIsRUFBZ0M7QUFDNUIsWUFBSSxPQUFPNUIsTUFBTSxDQUFDNEIsV0FBZCxLQUE4QixRQUFsQyxFQUNJLE1BQU1KLFNBQVMsQ0FBQywrQ0FBRCxDQUFmO0FBQ0o5QyxRQUFBQSxPQUFPLENBQUNrRCxXQUFSLEdBQXNCL0QsS0FBSyxDQUFDRSxXQUFOLENBQWtCZ0MsVUFBbEIsQ0FBNkJDLE1BQU0sQ0FBQzRCLFdBQXBDLENBQXRCO0FBQ0g7O0FBQ0QsVUFBSTVCLE1BQU0sQ0FBQzZCLFNBQVAsSUFBb0IsSUFBeEIsRUFBOEI7QUFDMUIsWUFBSSxPQUFPN0IsTUFBTSxDQUFDNkIsU0FBZCxLQUE0QixRQUFoQyxFQUNJLE1BQU1MLFNBQVMsQ0FBQyw2Q0FBRCxDQUFmO0FBQ0o5QyxRQUFBQSxPQUFPLENBQUNtRCxTQUFSLEdBQW9CaEUsS0FBSyxDQUFDMkMsU0FBTixDQUFnQlQsVUFBaEIsQ0FBMkJDLE1BQU0sQ0FBQzZCLFNBQWxDLENBQXBCO0FBQ0g7O0FBQ0QsVUFBSTdCLE1BQU0sQ0FBQzhCLGVBQVAsSUFBMEIsSUFBOUIsRUFBb0M7QUFDaEMsWUFBSSxPQUFPOUIsTUFBTSxDQUFDOEIsZUFBZCxLQUFrQyxRQUF0QyxFQUNJLE1BQU1OLFNBQVMsQ0FBQyxtREFBRCxDQUFmO0FBQ0o5QyxRQUFBQSxPQUFPLENBQUNvRCxlQUFSLEdBQTBCakUsS0FBSyxDQUFDOEMsZUFBTixDQUFzQlosVUFBdEIsQ0FBaUNDLE1BQU0sQ0FBQzhCLGVBQXhDLENBQTFCO0FBQ0g7O0FBQ0QsVUFBSTlCLE1BQU0sQ0FBQytCLEtBQVAsSUFBZ0IsSUFBcEIsRUFDSXJELE9BQU8sQ0FBQ3FELEtBQVIsR0FBZ0I5QixNQUFNLENBQUNELE1BQU0sQ0FBQytCLEtBQVIsQ0FBdEI7QUFDSixhQUFPckQsT0FBUDtBQUNILEtBdEJEO0FBd0JBO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ1EyQyxJQUFBQSxNQUFNLENBQUNuQixRQUFQLEdBQWtCLFNBQVNBLFFBQVQsQ0FBa0J4QixPQUFsQixFQUEyQnlCLE9BQTNCLEVBQW9DO0FBQ2xELFVBQUksQ0FBQ0EsT0FBTCxFQUNJQSxPQUFPLEdBQUcsRUFBVjtBQUNKLFVBQUlILE1BQU0sR0FBRyxFQUFiOztBQUNBLFVBQUl0QixPQUFPLENBQUNrRCxXQUFSLElBQXVCLElBQXZCLElBQStCbEQsT0FBTyxDQUFDRSxjQUFSLENBQXVCLGFBQXZCLENBQW5DLEVBQTBFO0FBQ3RFb0IsUUFBQUEsTUFBTSxDQUFDNEIsV0FBUCxHQUFxQi9ELEtBQUssQ0FBQ0UsV0FBTixDQUFrQm1DLFFBQWxCLENBQTJCeEIsT0FBTyxDQUFDa0QsV0FBbkMsRUFBZ0R6QixPQUFoRCxDQUFyQjtBQUNBLFlBQUlBLE9BQU8sQ0FBQ29DLE1BQVosRUFDSXZDLE1BQU0sQ0FBQ3NDLElBQVAsR0FBYyxhQUFkO0FBQ1A7O0FBQ0QsVUFBSTVELE9BQU8sQ0FBQ21ELFNBQVIsSUFBcUIsSUFBckIsSUFBNkJuRCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsV0FBdkIsQ0FBakMsRUFBc0U7QUFDbEVvQixRQUFBQSxNQUFNLENBQUM2QixTQUFQLEdBQW1CaEUsS0FBSyxDQUFDMkMsU0FBTixDQUFnQk4sUUFBaEIsQ0FBeUJ4QixPQUFPLENBQUNtRCxTQUFqQyxFQUE0QzFCLE9BQTVDLENBQW5CO0FBQ0EsWUFBSUEsT0FBTyxDQUFDb0MsTUFBWixFQUNJdkMsTUFBTSxDQUFDc0MsSUFBUCxHQUFjLFdBQWQ7QUFDUDs7QUFDRCxVQUFJNUQsT0FBTyxDQUFDb0QsZUFBUixJQUEyQixJQUEzQixJQUFtQ3BELE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixpQkFBdkIsQ0FBdkMsRUFBa0Y7QUFDOUVvQixRQUFBQSxNQUFNLENBQUM4QixlQUFQLEdBQXlCakUsS0FBSyxDQUFDOEMsZUFBTixDQUFzQlQsUUFBdEIsQ0FBK0J4QixPQUFPLENBQUNvRCxlQUF2QyxFQUF3RDNCLE9BQXhELENBQXpCO0FBQ0EsWUFBSUEsT0FBTyxDQUFDb0MsTUFBWixFQUNJdkMsTUFBTSxDQUFDc0MsSUFBUCxHQUFjLGlCQUFkO0FBQ1A7O0FBQ0QsVUFBSTVELE9BQU8sQ0FBQ3FELEtBQVIsSUFBaUIsSUFBakIsSUFBeUJyRCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFBOEQ7QUFDMURvQixRQUFBQSxNQUFNLENBQUMrQixLQUFQLEdBQWVyRCxPQUFPLENBQUNxRCxLQUF2QjtBQUNBLFlBQUk1QixPQUFPLENBQUNvQyxNQUFaLEVBQ0l2QyxNQUFNLENBQUNzQyxJQUFQLEdBQWMsT0FBZDtBQUNQOztBQUNELGFBQU90QyxNQUFQO0FBQ0gsS0F6QkQ7QUEyQkE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNRcUIsSUFBQUEsTUFBTSxDQUFDaEQsU0FBUCxDQUFpQmdDLE1BQWpCLEdBQTBCLFNBQVNBLE1BQVQsR0FBa0I7QUFDeEMsYUFBTyxLQUFLQyxXQUFMLENBQWlCSixRQUFqQixDQUEwQixJQUExQixFQUFnQzdDLFNBQVMsQ0FBQ08sSUFBVixDQUFlMkMsYUFBL0MsQ0FBUDtBQUNILEtBRkQ7O0FBSUEsV0FBT2MsTUFBUDtBQUNILEdBalRpQixFQUFsQjs7QUFtVEEsU0FBT0osUUFBUDtBQUNILENBamhCZ0IsRUFBakI7O0FBbWhCQXBELEtBQUssQ0FBQzJFLFVBQU4sR0FBb0IsWUFBVztBQUUzQjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNJLFdBQVNBLFVBQVQsQ0FBb0JDLE9BQXBCLEVBQTZCQyxnQkFBN0IsRUFBK0NDLGlCQUEvQyxFQUFrRTtBQUM5RHRGLElBQUFBLFNBQVMsQ0FBQ3VGLEdBQVYsQ0FBY0MsT0FBZCxDQUFzQmhFLElBQXRCLENBQTJCLElBQTNCLEVBQWlDNEQsT0FBakMsRUFBMENDLGdCQUExQyxFQUE0REMsaUJBQTVEO0FBQ0g7O0FBRUQsR0FBQ0gsVUFBVSxDQUFDbkUsU0FBWCxHQUF1QkgsTUFBTSxDQUFDTSxNQUFQLENBQWNuQixTQUFTLENBQUN1RixHQUFWLENBQWNDLE9BQWQsQ0FBc0J4RSxTQUFwQyxDQUF4QixFQUF3RWlDLFdBQXhFLEdBQXNGa0MsVUFBdEY7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSUEsRUFBQUEsVUFBVSxDQUFDaEUsTUFBWCxHQUFvQixTQUFTQSxNQUFULENBQWdCaUUsT0FBaEIsRUFBeUJDLGdCQUF6QixFQUEyQ0MsaUJBQTNDLEVBQThEO0FBQzlFLFdBQU8sSUFBSSxJQUFKLENBQVNGLE9BQVQsRUFBa0JDLGdCQUFsQixFQUFvQ0MsaUJBQXBDLENBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l6RSxFQUFBQSxNQUFNLENBQUMrRCxjQUFQLENBQXNCTyxVQUFVLENBQUNuRSxTQUFYLENBQXFCeUUsS0FBckIsR0FBNkIsU0FBU0EsS0FBVCxDQUFlQyxPQUFmLEVBQXdCQyxRQUF4QixFQUFrQztBQUNqRixXQUFPLEtBQUtDLE9BQUwsQ0FBYUgsS0FBYixFQUFvQmpGLEtBQUssQ0FBQ3FGLFlBQTFCLEVBQXdDckYsS0FBSyxDQUFDc0YsVUFBOUMsRUFBMERKLE9BQTFELEVBQW1FQyxRQUFuRSxDQUFQO0FBQ0gsR0FGRCxFQUVHLE1BRkgsRUFFVztBQUFFSSxJQUFBQSxLQUFLLEVBQUU7QUFBVCxHQUZYO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lsRixFQUFBQSxNQUFNLENBQUMrRCxjQUFQLENBQXNCTyxVQUFVLENBQUNuRSxTQUFYLENBQXFCZ0YsVUFBckIsR0FBa0MsU0FBU0EsVUFBVCxDQUFvQk4sT0FBcEIsRUFBNkJDLFFBQTdCLEVBQXVDO0FBQzNGLFdBQU8sS0FBS0MsT0FBTCxDQUFhSSxVQUFiLEVBQXlCeEYsS0FBSyxDQUFDeUYsaUJBQS9CLEVBQWtEekYsS0FBSyxDQUFDMEYsZUFBeEQsRUFBeUVSLE9BQXpFLEVBQWtGQyxRQUFsRixDQUFQO0FBQ0gsR0FGRCxFQUVHLE1BRkgsRUFFVztBQUFFSSxJQUFBQSxLQUFLLEVBQUU7QUFBVCxHQUZYO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lsRixFQUFBQSxNQUFNLENBQUMrRCxjQUFQLENBQXNCTyxVQUFVLENBQUNuRSxTQUFYLENBQXFCbUYsY0FBckIsR0FBc0MsU0FBU0EsY0FBVCxDQUF3QlQsT0FBeEIsRUFBaUNDLFFBQWpDLEVBQTJDO0FBQ25HLFdBQU8sS0FBS0MsT0FBTCxDQUFhTyxjQUFiLEVBQTZCM0YsS0FBSyxDQUFDNEYscUJBQW5DLEVBQTBENUYsS0FBSyxDQUFDNkYsbUJBQWhFLEVBQXFGWCxPQUFyRixFQUE4RkMsUUFBOUYsQ0FBUDtBQUNILEdBRkQsRUFFRyxNQUZILEVBRVc7QUFBRUksSUFBQUEsS0FBSyxFQUFFO0FBQVQsR0FGWDtBQUlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJbEYsRUFBQUEsTUFBTSxDQUFDK0QsY0FBUCxDQUFzQk8sVUFBVSxDQUFDbkUsU0FBWCxDQUFxQnNGLGdCQUFyQixHQUF3QyxTQUFTQSxnQkFBVCxDQUEwQlosT0FBMUIsRUFBbUNDLFFBQW5DLEVBQTZDO0FBQ3ZHLFdBQU8sS0FBS0MsT0FBTCxDQUFhVSxnQkFBYixFQUErQjlGLEtBQUssQ0FBQytGLHVCQUFyQyxFQUE4RC9GLEtBQUssQ0FBQ2dHLHFCQUFwRSxFQUEyRmQsT0FBM0YsRUFBb0dDLFFBQXBHLENBQVA7QUFDSCxHQUZELEVBRUcsTUFGSCxFQUVXO0FBQUVJLElBQUFBLEtBQUssRUFBRTtBQUFULEdBRlg7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUksU0FBT1osVUFBUDtBQUNILENBcktrQixFQUFuQjs7QUF1S0EzRSxLQUFLLENBQUNxRixZQUFOLEdBQXNCLFlBQVc7QUFFN0I7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSSxXQUFTQSxZQUFULENBQXNCbEYsVUFBdEIsRUFBa0M7QUFDOUIsUUFBSUEsVUFBSixFQUNJLEtBQUssSUFBSUMsSUFBSSxHQUFHQyxNQUFNLENBQUNELElBQVAsQ0FBWUQsVUFBWixDQUFYLEVBQW9DRyxDQUFDLEdBQUcsQ0FBN0MsRUFBZ0RBLENBQUMsR0FBR0YsSUFBSSxDQUFDRyxNQUF6RCxFQUFpRSxFQUFFRCxDQUFuRTtBQUNJLFVBQUlILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBVixJQUF1QixJQUEzQixFQUNJLEtBQUtGLElBQUksQ0FBQ0UsQ0FBRCxDQUFULElBQWdCSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQTFCO0FBRlI7QUFHUDtBQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0krRSxFQUFBQSxZQUFZLENBQUM3RSxTQUFiLENBQXVCeUYsV0FBdkIsR0FBcUMsRUFBckM7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJWixFQUFBQSxZQUFZLENBQUMxRSxNQUFiLEdBQXNCLFNBQVNBLE1BQVQsQ0FBZ0JSLFVBQWhCLEVBQTRCO0FBQzlDLFdBQU8sSUFBSWtGLFlBQUosQ0FBaUJsRixVQUFqQixDQUFQO0FBQ0gsR0FGRDtBQUlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lrRixFQUFBQSxZQUFZLENBQUN6RSxNQUFiLEdBQXNCLFNBQVNBLE1BQVQsQ0FBZ0JDLE9BQWhCLEVBQXlCQyxNQUF6QixFQUFpQztBQUNuRCxRQUFJLENBQUNBLE1BQUwsRUFDSUEsTUFBTSxHQUFHbEIsT0FBTyxDQUFDZSxNQUFSLEVBQVQ7QUFDSixRQUFJRSxPQUFPLENBQUNvRixXQUFSLElBQXVCLElBQXZCLElBQStCNUYsTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsYUFBcEMsQ0FBbkMsRUFDSUMsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsTUFBckMsRUFBeUNFLE1BQXpDLENBQWdETixPQUFPLENBQUNvRixXQUF4RDtBQUNKLFdBQU9uRixNQUFQO0FBQ0gsR0FORDtBQVFBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l1RSxFQUFBQSxZQUFZLENBQUNqRSxlQUFiLEdBQStCLFNBQVNBLGVBQVQsQ0FBeUJQLE9BQXpCLEVBQWtDQyxNQUFsQyxFQUEwQztBQUNyRSxXQUFPLEtBQUtGLE1BQUwsQ0FBWUMsT0FBWixFQUFxQkMsTUFBckIsRUFBNkJPLE1BQTdCLEVBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lnRSxFQUFBQSxZQUFZLENBQUMvRCxNQUFiLEdBQXNCLFNBQVNBLE1BQVQsQ0FBZ0JDLE1BQWhCLEVBQXdCaEIsTUFBeEIsRUFBZ0M7QUFDbEQsUUFBSSxFQUFFZ0IsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHN0IsT0FBTyxDQUFDaUIsTUFBUixDQUFlWSxNQUFmLENBQVQ7QUFDSixRQUFJQyxHQUFHLEdBQUdqQixNQUFNLEtBQUtrQixTQUFYLEdBQXVCRixNQUFNLENBQUNHLEdBQTlCLEdBQW9DSCxNQUFNLENBQUNJLEdBQVAsR0FBYXBCLE1BQTNEO0FBQUEsUUFBbUVNLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUNxRixZQUFWLEVBQTdFOztBQUNBLFdBQU85RCxNQUFNLENBQUNJLEdBQVAsR0FBYUgsR0FBcEIsRUFBeUI7QUFDckIsVUFBSUksR0FBRyxHQUFHTCxNQUFNLENBQUNOLE1BQVAsRUFBVjs7QUFDQSxjQUFRVyxHQUFHLEtBQUssQ0FBaEI7QUFDQSxhQUFLLENBQUw7QUFDSWYsVUFBQUEsT0FBTyxDQUFDb0YsV0FBUixHQUFzQjFFLE1BQU0sQ0FBQ0osTUFBUCxFQUF0QjtBQUNBOztBQUNKO0FBQ0lJLFVBQUFBLE1BQU0sQ0FBQ00sUUFBUCxDQUFnQkQsR0FBRyxHQUFHLENBQXRCO0FBQ0E7QUFOSjtBQVFIOztBQUNELFdBQU9mLE9BQVA7QUFDSCxHQWhCRDtBQWtCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l3RSxFQUFBQSxZQUFZLENBQUN2RCxlQUFiLEdBQStCLFNBQVNBLGVBQVQsQ0FBeUJQLE1BQXpCLEVBQWlDO0FBQzVELFFBQUksRUFBRUEsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHLElBQUk3QixPQUFKLENBQVk2QixNQUFaLENBQVQ7QUFDSixXQUFPLEtBQUtELE1BQUwsQ0FBWUMsTUFBWixFQUFvQkEsTUFBTSxDQUFDTixNQUFQLEVBQXBCLENBQVA7QUFDSCxHQUpEO0FBTUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lvRSxFQUFBQSxZQUFZLENBQUN0RCxNQUFiLEdBQXNCLFNBQVNBLE1BQVQsQ0FBZ0JsQixPQUFoQixFQUF5QjtBQUMzQyxRQUFJLE9BQU9BLE9BQVAsS0FBbUIsUUFBbkIsSUFBK0JBLE9BQU8sS0FBSyxJQUEvQyxFQUNJLE9BQU8saUJBQVA7QUFDSixRQUFJQSxPQUFPLENBQUNvRixXQUFSLElBQXVCLElBQXZCLElBQStCcEYsT0FBTyxDQUFDRSxjQUFSLENBQXVCLGFBQXZCLENBQW5DLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDbUMsUUFBTixDQUFlcEIsT0FBTyxDQUFDb0YsV0FBdkIsQ0FBTCxFQUNJLE9BQU8sOEJBQVA7QUFDUixXQUFPLElBQVA7QUFDSCxHQVBEO0FBU0E7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0laLEVBQUFBLFlBQVksQ0FBQ25ELFVBQWIsR0FBMEIsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDbEQsUUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDcUYsWUFBNUIsRUFDSSxPQUFPbEQsTUFBUDtBQUNKLFFBQUl0QixPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDcUYsWUFBVixFQUFkO0FBQ0EsUUFBSWxELE1BQU0sQ0FBQzhELFdBQVAsSUFBc0IsSUFBMUIsRUFDSXBGLE9BQU8sQ0FBQ29GLFdBQVIsR0FBc0I3RCxNQUFNLENBQUNELE1BQU0sQ0FBQzhELFdBQVIsQ0FBNUI7QUFDSixXQUFPcEYsT0FBUDtBQUNILEdBUEQ7QUFTQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJd0UsRUFBQUEsWUFBWSxDQUFDaEQsUUFBYixHQUF3QixTQUFTQSxRQUFULENBQWtCeEIsT0FBbEIsRUFBMkJ5QixPQUEzQixFQUFvQztBQUN4RCxRQUFJLENBQUNBLE9BQUwsRUFDSUEsT0FBTyxHQUFHLEVBQVY7QUFDSixRQUFJSCxNQUFNLEdBQUcsRUFBYjtBQUNBLFFBQUlHLE9BQU8sQ0FBQ0MsUUFBWixFQUNJSixNQUFNLENBQUM4RCxXQUFQLEdBQXFCLEVBQXJCO0FBQ0osUUFBSXBGLE9BQU8sQ0FBQ29GLFdBQVIsSUFBdUIsSUFBdkIsSUFBK0JwRixPQUFPLENBQUNFLGNBQVIsQ0FBdUIsYUFBdkIsQ0FBbkMsRUFDSW9CLE1BQU0sQ0FBQzhELFdBQVAsR0FBcUJwRixPQUFPLENBQUNvRixXQUE3QjtBQUNKLFdBQU85RCxNQUFQO0FBQ0gsR0FURDtBQVdBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSWtELEVBQUFBLFlBQVksQ0FBQzdFLFNBQWIsQ0FBdUJnQyxNQUF2QixHQUFnQyxTQUFTQSxNQUFULEdBQWtCO0FBQzlDLFdBQU8sS0FBS0MsV0FBTCxDQUFpQkosUUFBakIsQ0FBMEIsSUFBMUIsRUFBZ0M3QyxTQUFTLENBQUNPLElBQVYsQ0FBZTJDLGFBQS9DLENBQVA7QUFDSCxHQUZEOztBQUlBLFNBQU8yQyxZQUFQO0FBQ0gsQ0F6TG9CLEVBQXJCOztBQTJMQXJGLEtBQUssQ0FBQ3NGLFVBQU4sR0FBb0IsWUFBVztBQUUzQjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ksV0FBU0EsVUFBVCxDQUFvQm5GLFVBQXBCLEVBQWdDO0FBQzVCLFNBQUsrRixjQUFMLEdBQXNCLEVBQXRCO0FBQ0EsUUFBSS9GLFVBQUosRUFDSSxLQUFLLElBQUlDLElBQUksR0FBR0MsTUFBTSxDQUFDRCxJQUFQLENBQVlELFVBQVosQ0FBWCxFQUFvQ0csQ0FBQyxHQUFHLENBQTdDLEVBQWdEQSxDQUFDLEdBQUdGLElBQUksQ0FBQ0csTUFBekQsRUFBaUUsRUFBRUQsQ0FBbkU7QUFDSSxVQUFJSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQVYsSUFBdUIsSUFBM0IsRUFDSSxLQUFLRixJQUFJLENBQUNFLENBQUQsQ0FBVCxJQUFnQkgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUExQjtBQUZSO0FBR1A7QUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJZ0YsRUFBQUEsVUFBVSxDQUFDOUUsU0FBWCxDQUFxQjJGLFFBQXJCLEdBQWdDLElBQWhDO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJYixFQUFBQSxVQUFVLENBQUM5RSxTQUFYLENBQXFCNEYsVUFBckIsR0FBa0MsQ0FBbEM7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lkLEVBQUFBLFVBQVUsQ0FBQzlFLFNBQVgsQ0FBcUI2RixJQUFyQixHQUE0QnZHLEtBQUssQ0FBQ3dHLElBQU4sR0FBYXhHLEtBQUssQ0FBQ3dHLElBQU4sQ0FBV0MsUUFBWCxDQUFvQixDQUFwQixFQUFzQixDQUF0QixFQUF3QixJQUF4QixDQUFiLEdBQTZDLENBQXpFO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJakIsRUFBQUEsVUFBVSxDQUFDOUUsU0FBWCxDQUFxQmdHLE9BQXJCLEdBQStCMUcsS0FBSyxDQUFDd0csSUFBTixHQUFheEcsS0FBSyxDQUFDd0csSUFBTixDQUFXQyxRQUFYLENBQW9CLENBQXBCLEVBQXNCLENBQXRCLEVBQXdCLElBQXhCLENBQWIsR0FBNkMsQ0FBNUU7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lqQixFQUFBQSxVQUFVLENBQUM5RSxTQUFYLENBQXFCMEYsY0FBckIsR0FBc0NwRyxLQUFLLENBQUMyRyxVQUE1QztBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0luQixFQUFBQSxVQUFVLENBQUMzRSxNQUFYLEdBQW9CLFNBQVNBLE1BQVQsQ0FBZ0JSLFVBQWhCLEVBQTRCO0FBQzVDLFdBQU8sSUFBSW1GLFVBQUosQ0FBZW5GLFVBQWYsQ0FBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJbUYsRUFBQUEsVUFBVSxDQUFDMUUsTUFBWCxHQUFvQixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDakQsUUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUO0FBQ0osUUFBSUUsT0FBTyxDQUFDc0YsUUFBUixJQUFvQixJQUFwQixJQUE0QjlGLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFVBQXBDLENBQWhDLEVBQ0liLEtBQUssQ0FBQzBHLFFBQU4sQ0FBZTlGLE1BQWYsQ0FBc0JDLE9BQU8sQ0FBQ3NGLFFBQTlCLEVBQXdDckYsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsTUFBckMsRUFBeUN3QyxJQUF6QyxFQUF4QyxFQUF5RnBDLE1BQXpGO0FBQ0osUUFBSVIsT0FBTyxDQUFDdUYsVUFBUixJQUFzQixJQUF0QixJQUE4Qi9GLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFlBQXBDLENBQWxDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDQSxNQUF6QyxDQUFnREosT0FBTyxDQUFDdUYsVUFBeEQ7QUFDSixRQUFJdkYsT0FBTyxDQUFDd0YsSUFBUixJQUFnQixJQUFoQixJQUF3QmhHLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLE1BQXBDLENBQTVCLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDMEYsTUFBekMsQ0FBZ0Q5RixPQUFPLENBQUN3RixJQUF4RDtBQUNKLFFBQUl4RixPQUFPLENBQUMyRixPQUFSLElBQW1CLElBQW5CLElBQTJCbkcsTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsU0FBcEMsQ0FBL0IsRUFDSUMsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsTUFBckMsRUFBeUMwRixNQUF6QyxDQUFnRDlGLE9BQU8sQ0FBQzJGLE9BQXhEO0FBQ0osUUFBSTNGLE9BQU8sQ0FBQ3FGLGNBQVIsSUFBMEIsSUFBMUIsSUFBa0NyRixPQUFPLENBQUNxRixjQUFSLENBQXVCM0YsTUFBN0QsRUFDSSxLQUFLLElBQUlELENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdPLE9BQU8sQ0FBQ3FGLGNBQVIsQ0FBdUIzRixNQUEzQyxFQUFtRCxFQUFFRCxDQUFyRDtBQUNJTixNQUFBQSxLQUFLLENBQUM0RyxpQkFBTixDQUF3QmhHLE1BQXhCLENBQStCQyxPQUFPLENBQUNxRixjQUFSLENBQXVCNUYsQ0FBdkIsQ0FBL0IsRUFBMERRLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLFFBQXJDLEVBQXlDd0MsSUFBekMsRUFBMUQsRUFBMkdwQyxNQUEzRztBQURKO0FBRUosV0FBT1AsTUFBUDtBQUNILEdBZkQ7QUFpQkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXdFLEVBQUFBLFVBQVUsQ0FBQ2xFLGVBQVgsR0FBNkIsU0FBU0EsZUFBVCxDQUF5QlAsT0FBekIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ25FLFdBQU8sS0FBS0YsTUFBTCxDQUFZQyxPQUFaLEVBQXFCQyxNQUFyQixFQUE2Qk8sTUFBN0IsRUFBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSWlFLEVBQUFBLFVBQVUsQ0FBQ2hFLE1BQVgsR0FBb0IsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0JoQixNQUF4QixFQUFnQztBQUNoRCxRQUFJLEVBQUVnQixNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUc3QixPQUFPLENBQUNpQixNQUFSLENBQWVZLE1BQWYsQ0FBVDtBQUNKLFFBQUlDLEdBQUcsR0FBR2pCLE1BQU0sS0FBS2tCLFNBQVgsR0FBdUJGLE1BQU0sQ0FBQ0csR0FBOUIsR0FBb0NILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhcEIsTUFBM0Q7QUFBQSxRQUFtRU0sT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQ3NGLFVBQVYsRUFBN0U7O0FBQ0EsV0FBTy9ELE1BQU0sQ0FBQ0ksR0FBUCxHQUFhSCxHQUFwQixFQUF5QjtBQUNyQixVQUFJSSxHQUFHLEdBQUdMLE1BQU0sQ0FBQ04sTUFBUCxFQUFWOztBQUNBLGNBQVFXLEdBQUcsS0FBSyxDQUFoQjtBQUNBLGFBQUssQ0FBTDtBQUNJZixVQUFBQSxPQUFPLENBQUNzRixRQUFSLEdBQW1CbkcsS0FBSyxDQUFDMEcsUUFBTixDQUFlcEYsTUFBZixDQUFzQkMsTUFBdEIsRUFBOEJBLE1BQU0sQ0FBQ04sTUFBUCxFQUE5QixDQUFuQjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJSixVQUFBQSxPQUFPLENBQUN1RixVQUFSLEdBQXFCN0UsTUFBTSxDQUFDTixNQUFQLEVBQXJCO0FBQ0E7O0FBQ0osYUFBSyxDQUFMO0FBQ0lKLFVBQUFBLE9BQU8sQ0FBQ3dGLElBQVIsR0FBZTlFLE1BQU0sQ0FBQ29GLE1BQVAsRUFBZjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJOUYsVUFBQUEsT0FBTyxDQUFDMkYsT0FBUixHQUFrQmpGLE1BQU0sQ0FBQ29GLE1BQVAsRUFBbEI7QUFDQTs7QUFDSixhQUFLLENBQUw7QUFDSSxjQUFJLEVBQUU5RixPQUFPLENBQUNxRixjQUFSLElBQTBCckYsT0FBTyxDQUFDcUYsY0FBUixDQUF1QjNGLE1BQW5ELENBQUosRUFDSU0sT0FBTyxDQUFDcUYsY0FBUixHQUF5QixFQUF6QjtBQUNKckYsVUFBQUEsT0FBTyxDQUFDcUYsY0FBUixDQUF1QlcsSUFBdkIsQ0FBNEI3RyxLQUFLLENBQUM0RyxpQkFBTixDQUF3QnRGLE1BQXhCLENBQStCQyxNQUEvQixFQUF1Q0EsTUFBTSxDQUFDTixNQUFQLEVBQXZDLENBQTVCO0FBQ0E7O0FBQ0o7QUFDSU0sVUFBQUEsTUFBTSxDQUFDTSxRQUFQLENBQWdCRCxHQUFHLEdBQUcsQ0FBdEI7QUFDQTtBQXBCSjtBQXNCSDs7QUFDRCxXQUFPZixPQUFQO0FBQ0gsR0E5QkQ7QUFnQ0E7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJeUUsRUFBQUEsVUFBVSxDQUFDeEQsZUFBWCxHQUE2QixTQUFTQSxlQUFULENBQXlCUCxNQUF6QixFQUFpQztBQUMxRCxRQUFJLEVBQUVBLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRyxJQUFJN0IsT0FBSixDQUFZNkIsTUFBWixDQUFUO0FBQ0osV0FBTyxLQUFLRCxNQUFMLENBQVlDLE1BQVosRUFBb0JBLE1BQU0sQ0FBQ04sTUFBUCxFQUFwQixDQUFQO0FBQ0gsR0FKRDtBQU1BO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJcUUsRUFBQUEsVUFBVSxDQUFDdkQsTUFBWCxHQUFvQixTQUFTQSxNQUFULENBQWdCbEIsT0FBaEIsRUFBeUI7QUFDekMsUUFBSSxPQUFPQSxPQUFQLEtBQW1CLFFBQW5CLElBQStCQSxPQUFPLEtBQUssSUFBL0MsRUFDSSxPQUFPLGlCQUFQOztBQUNKLFFBQUlBLE9BQU8sQ0FBQ3NGLFFBQVIsSUFBb0IsSUFBcEIsSUFBNEJ0RixPQUFPLENBQUNFLGNBQVIsQ0FBdUIsVUFBdkIsQ0FBaEMsRUFBb0U7QUFDaEUsVUFBSThCLEtBQUssR0FBRzdDLEtBQUssQ0FBQzBHLFFBQU4sQ0FBZTNFLE1BQWYsQ0FBc0JsQixPQUFPLENBQUNzRixRQUE5QixDQUFaO0FBQ0EsVUFBSXRELEtBQUosRUFDSSxPQUFPLGNBQWNBLEtBQXJCO0FBQ1A7O0FBQ0QsUUFBSWhDLE9BQU8sQ0FBQ3VGLFVBQVIsSUFBc0IsSUFBdEIsSUFBOEJ2RixPQUFPLENBQUNFLGNBQVIsQ0FBdUIsWUFBdkIsQ0FBbEMsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDdUYsVUFBeEIsQ0FBTCxFQUNJLE9BQU8sOEJBQVA7QUFDUixRQUFJdkYsT0FBTyxDQUFDd0YsSUFBUixJQUFnQixJQUFoQixJQUF3QnhGLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixNQUF2QixDQUE1QixFQUNJLElBQUksQ0FBQ2pCLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUN3RixJQUF4QixDQUFELElBQWtDLEVBQUV4RixPQUFPLENBQUN3RixJQUFSLElBQWdCdkcsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQ3dGLElBQVIsQ0FBYVMsR0FBN0IsQ0FBaEIsSUFBcURoSCxLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDd0YsSUFBUixDQUFhVSxJQUE3QixDQUF2RCxDQUF0QyxFQUNJLE9BQU8sNkJBQVA7QUFDUixRQUFJbEcsT0FBTyxDQUFDMkYsT0FBUixJQUFtQixJQUFuQixJQUEyQjNGLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixTQUF2QixDQUEvQixFQUNJLElBQUksQ0FBQ2pCLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUMyRixPQUF4QixDQUFELElBQXFDLEVBQUUzRixPQUFPLENBQUMyRixPQUFSLElBQW1CMUcsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQzJGLE9BQVIsQ0FBZ0JNLEdBQWhDLENBQW5CLElBQTJEaEgsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQzJGLE9BQVIsQ0FBZ0JPLElBQWhDLENBQTdELENBQXpDLEVBQ0ksT0FBTyxnQ0FBUDs7QUFDUixRQUFJbEcsT0FBTyxDQUFDcUYsY0FBUixJQUEwQixJQUExQixJQUFrQ3JGLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixnQkFBdkIsQ0FBdEMsRUFBZ0Y7QUFDNUUsVUFBSSxDQUFDOEMsS0FBSyxDQUFDbUQsT0FBTixDQUFjbkcsT0FBTyxDQUFDcUYsY0FBdEIsQ0FBTCxFQUNJLE9BQU8sZ0NBQVA7O0FBQ0osV0FBSyxJQUFJNUYsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR08sT0FBTyxDQUFDcUYsY0FBUixDQUF1QjNGLE1BQTNDLEVBQW1ELEVBQUVELENBQXJELEVBQXdEO0FBQ3BELFlBQUl1QyxLQUFLLEdBQUc3QyxLQUFLLENBQUM0RyxpQkFBTixDQUF3QjdFLE1BQXhCLENBQStCbEIsT0FBTyxDQUFDcUYsY0FBUixDQUF1QjVGLENBQXZCLENBQS9CLENBQVo7QUFDQSxZQUFJdUMsS0FBSixFQUNJLE9BQU8sb0JBQW9CQSxLQUEzQjtBQUNQO0FBQ0o7O0FBQ0QsV0FBTyxJQUFQO0FBQ0gsR0EzQkQ7QUE2QkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l5QyxFQUFBQSxVQUFVLENBQUNwRCxVQUFYLEdBQXdCLFNBQVNBLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQ2hELFFBQUlBLE1BQU0sWUFBWW5DLEtBQUssQ0FBQ3NGLFVBQTVCLEVBQ0ksT0FBT25ELE1BQVA7QUFDSixRQUFJdEIsT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQ3NGLFVBQVYsRUFBZDs7QUFDQSxRQUFJbkQsTUFBTSxDQUFDZ0UsUUFBUCxJQUFtQixJQUF2QixFQUE2QjtBQUN6QixVQUFJLE9BQU9oRSxNQUFNLENBQUNnRSxRQUFkLEtBQTJCLFFBQS9CLEVBQ0ksTUFBTXhDLFNBQVMsQ0FBQyx1Q0FBRCxDQUFmO0FBQ0o5QyxNQUFBQSxPQUFPLENBQUNzRixRQUFSLEdBQW1CbkcsS0FBSyxDQUFDMEcsUUFBTixDQUFleEUsVUFBZixDQUEwQkMsTUFBTSxDQUFDZ0UsUUFBakMsQ0FBbkI7QUFDSDs7QUFDRCxRQUFJaEUsTUFBTSxDQUFDaUUsVUFBUCxJQUFxQixJQUF6QixFQUNJdkYsT0FBTyxDQUFDdUYsVUFBUixHQUFxQmpFLE1BQU0sQ0FBQ2lFLFVBQVAsS0FBc0IsQ0FBM0M7QUFDSixRQUFJakUsTUFBTSxDQUFDa0UsSUFBUCxJQUFlLElBQW5CLEVBQ0ksSUFBSXZHLEtBQUssQ0FBQ3dHLElBQVYsRUFDSSxDQUFDekYsT0FBTyxDQUFDd0YsSUFBUixHQUFldkcsS0FBSyxDQUFDd0csSUFBTixDQUFXVyxTQUFYLENBQXFCOUUsTUFBTSxDQUFDa0UsSUFBNUIsQ0FBaEIsRUFBbURhLFFBQW5ELEdBQThELElBQTlELENBREosS0FFSyxJQUFJLE9BQU8vRSxNQUFNLENBQUNrRSxJQUFkLEtBQXVCLFFBQTNCLEVBQ0R4RixPQUFPLENBQUN3RixJQUFSLEdBQWVjLFFBQVEsQ0FBQ2hGLE1BQU0sQ0FBQ2tFLElBQVIsRUFBYyxFQUFkLENBQXZCLENBREMsS0FFQSxJQUFJLE9BQU9sRSxNQUFNLENBQUNrRSxJQUFkLEtBQXVCLFFBQTNCLEVBQ0R4RixPQUFPLENBQUN3RixJQUFSLEdBQWVsRSxNQUFNLENBQUNrRSxJQUF0QixDQURDLEtBRUEsSUFBSSxPQUFPbEUsTUFBTSxDQUFDa0UsSUFBZCxLQUF1QixRQUEzQixFQUNEeEYsT0FBTyxDQUFDd0YsSUFBUixHQUFlLElBQUl2RyxLQUFLLENBQUNzSCxRQUFWLENBQW1CakYsTUFBTSxDQUFDa0UsSUFBUCxDQUFZUyxHQUFaLEtBQW9CLENBQXZDLEVBQTBDM0UsTUFBTSxDQUFDa0UsSUFBUCxDQUFZVSxJQUFaLEtBQXFCLENBQS9ELEVBQWtFTSxRQUFsRSxDQUEyRSxJQUEzRSxDQUFmO0FBQ1IsUUFBSWxGLE1BQU0sQ0FBQ3FFLE9BQVAsSUFBa0IsSUFBdEIsRUFDSSxJQUFJMUcsS0FBSyxDQUFDd0csSUFBVixFQUNJLENBQUN6RixPQUFPLENBQUMyRixPQUFSLEdBQWtCMUcsS0FBSyxDQUFDd0csSUFBTixDQUFXVyxTQUFYLENBQXFCOUUsTUFBTSxDQUFDcUUsT0FBNUIsQ0FBbkIsRUFBeURVLFFBQXpELEdBQW9FLElBQXBFLENBREosS0FFSyxJQUFJLE9BQU8vRSxNQUFNLENBQUNxRSxPQUFkLEtBQTBCLFFBQTlCLEVBQ0QzRixPQUFPLENBQUMyRixPQUFSLEdBQWtCVyxRQUFRLENBQUNoRixNQUFNLENBQUNxRSxPQUFSLEVBQWlCLEVBQWpCLENBQTFCLENBREMsS0FFQSxJQUFJLE9BQU9yRSxNQUFNLENBQUNxRSxPQUFkLEtBQTBCLFFBQTlCLEVBQ0QzRixPQUFPLENBQUMyRixPQUFSLEdBQWtCckUsTUFBTSxDQUFDcUUsT0FBekIsQ0FEQyxLQUVBLElBQUksT0FBT3JFLE1BQU0sQ0FBQ3FFLE9BQWQsS0FBMEIsUUFBOUIsRUFDRDNGLE9BQU8sQ0FBQzJGLE9BQVIsR0FBa0IsSUFBSTFHLEtBQUssQ0FBQ3NILFFBQVYsQ0FBbUJqRixNQUFNLENBQUNxRSxPQUFQLENBQWVNLEdBQWYsS0FBdUIsQ0FBMUMsRUFBNkMzRSxNQUFNLENBQUNxRSxPQUFQLENBQWVPLElBQWYsS0FBd0IsQ0FBckUsRUFBd0VNLFFBQXhFLENBQWlGLElBQWpGLENBQWxCOztBQUNSLFFBQUlsRixNQUFNLENBQUMrRCxjQUFYLEVBQTJCO0FBQ3ZCLFVBQUksQ0FBQ3JDLEtBQUssQ0FBQ21ELE9BQU4sQ0FBYzdFLE1BQU0sQ0FBQytELGNBQXJCLENBQUwsRUFDSSxNQUFNdkMsU0FBUyxDQUFDLDRDQUFELENBQWY7QUFDSjlDLE1BQUFBLE9BQU8sQ0FBQ3FGLGNBQVIsR0FBeUIsRUFBekI7O0FBQ0EsV0FBSyxJQUFJNUYsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRzZCLE1BQU0sQ0FBQytELGNBQVAsQ0FBc0IzRixNQUExQyxFQUFrRCxFQUFFRCxDQUFwRCxFQUF1RDtBQUNuRCxZQUFJLE9BQU82QixNQUFNLENBQUMrRCxjQUFQLENBQXNCNUYsQ0FBdEIsQ0FBUCxLQUFvQyxRQUF4QyxFQUNJLE1BQU1xRCxTQUFTLENBQUMsNkNBQUQsQ0FBZjtBQUNKOUMsUUFBQUEsT0FBTyxDQUFDcUYsY0FBUixDQUF1QjVGLENBQXZCLElBQTRCTixLQUFLLENBQUM0RyxpQkFBTixDQUF3QjFFLFVBQXhCLENBQW1DQyxNQUFNLENBQUMrRCxjQUFQLENBQXNCNUYsQ0FBdEIsQ0FBbkMsQ0FBNUI7QUFDSDtBQUNKOztBQUNELFdBQU9PLE9BQVA7QUFDSCxHQXhDRDtBQTBDQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJeUUsRUFBQUEsVUFBVSxDQUFDakQsUUFBWCxHQUFzQixTQUFTQSxRQUFULENBQWtCeEIsT0FBbEIsRUFBMkJ5QixPQUEzQixFQUFvQztBQUN0RCxRQUFJLENBQUNBLE9BQUwsRUFDSUEsT0FBTyxHQUFHLEVBQVY7QUFDSixRQUFJSCxNQUFNLEdBQUcsRUFBYjtBQUNBLFFBQUlHLE9BQU8sQ0FBQ2dGLE1BQVIsSUFBa0JoRixPQUFPLENBQUNDLFFBQTlCLEVBQ0lKLE1BQU0sQ0FBQytELGNBQVAsR0FBd0IsRUFBeEI7O0FBQ0osUUFBSTVELE9BQU8sQ0FBQ0MsUUFBWixFQUFzQjtBQUNsQkosTUFBQUEsTUFBTSxDQUFDZ0UsUUFBUCxHQUFrQixJQUFsQjtBQUNBaEUsTUFBQUEsTUFBTSxDQUFDaUUsVUFBUCxHQUFvQixDQUFwQjs7QUFDQSxVQUFJdEcsS0FBSyxDQUFDd0csSUFBVixFQUFnQjtBQUNaLFlBQUlpQixLQUFJLEdBQUcsSUFBSXpILEtBQUssQ0FBQ3dHLElBQVYsQ0FBZSxDQUFmLEVBQWtCLENBQWxCLEVBQXFCLElBQXJCLENBQVg7O0FBQ0FuRSxRQUFBQSxNQUFNLENBQUNrRSxJQUFQLEdBQWMvRCxPQUFPLENBQUNrRixLQUFSLEtBQWtCcEYsTUFBbEIsR0FBMkJtRixLQUFJLENBQUNFLFFBQUwsRUFBM0IsR0FBNkNuRixPQUFPLENBQUNrRixLQUFSLEtBQWtCRSxNQUFsQixHQUEyQkgsS0FBSSxDQUFDRixRQUFMLEVBQTNCLEdBQTZDRSxLQUF4RztBQUNILE9BSEQsTUFJSXBGLE1BQU0sQ0FBQ2tFLElBQVAsR0FBYy9ELE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JwRixNQUFsQixHQUEyQixHQUEzQixHQUFpQyxDQUEvQzs7QUFDSixVQUFJdEMsS0FBSyxDQUFDd0csSUFBVixFQUFnQjtBQUNaLFlBQUlpQixLQUFJLEdBQUcsSUFBSXpILEtBQUssQ0FBQ3dHLElBQVYsQ0FBZSxDQUFmLEVBQWtCLENBQWxCLEVBQXFCLElBQXJCLENBQVg7O0FBQ0FuRSxRQUFBQSxNQUFNLENBQUNxRSxPQUFQLEdBQWlCbEUsT0FBTyxDQUFDa0YsS0FBUixLQUFrQnBGLE1BQWxCLEdBQTJCbUYsS0FBSSxDQUFDRSxRQUFMLEVBQTNCLEdBQTZDbkYsT0FBTyxDQUFDa0YsS0FBUixLQUFrQkUsTUFBbEIsR0FBMkJILEtBQUksQ0FBQ0YsUUFBTCxFQUEzQixHQUE2Q0UsS0FBM0c7QUFDSCxPQUhELE1BSUlwRixNQUFNLENBQUNxRSxPQUFQLEdBQWlCbEUsT0FBTyxDQUFDa0YsS0FBUixLQUFrQnBGLE1BQWxCLEdBQTJCLEdBQTNCLEdBQWlDLENBQWxEO0FBQ1A7O0FBQ0QsUUFBSXZCLE9BQU8sQ0FBQ3NGLFFBQVIsSUFBb0IsSUFBcEIsSUFBNEJ0RixPQUFPLENBQUNFLGNBQVIsQ0FBdUIsVUFBdkIsQ0FBaEMsRUFDSW9CLE1BQU0sQ0FBQ2dFLFFBQVAsR0FBa0JuRyxLQUFLLENBQUMwRyxRQUFOLENBQWVyRSxRQUFmLENBQXdCeEIsT0FBTyxDQUFDc0YsUUFBaEMsRUFBMEM3RCxPQUExQyxDQUFsQjtBQUNKLFFBQUl6QixPQUFPLENBQUN1RixVQUFSLElBQXNCLElBQXRCLElBQThCdkYsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFlBQXZCLENBQWxDLEVBQ0lvQixNQUFNLENBQUNpRSxVQUFQLEdBQW9CdkYsT0FBTyxDQUFDdUYsVUFBNUI7QUFDSixRQUFJdkYsT0FBTyxDQUFDd0YsSUFBUixJQUFnQixJQUFoQixJQUF3QnhGLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixNQUF2QixDQUE1QixFQUNJLElBQUksT0FBT0YsT0FBTyxDQUFDd0YsSUFBZixLQUF3QixRQUE1QixFQUNJbEUsTUFBTSxDQUFDa0UsSUFBUCxHQUFjL0QsT0FBTyxDQUFDa0YsS0FBUixLQUFrQnBGLE1BQWxCLEdBQTJCQSxNQUFNLENBQUN2QixPQUFPLENBQUN3RixJQUFULENBQWpDLEdBQWtEeEYsT0FBTyxDQUFDd0YsSUFBeEUsQ0FESixLQUdJbEUsTUFBTSxDQUFDa0UsSUFBUCxHQUFjL0QsT0FBTyxDQUFDa0YsS0FBUixLQUFrQnBGLE1BQWxCLEdBQTJCdEMsS0FBSyxDQUFDd0csSUFBTixDQUFXOUYsU0FBWCxDQUFxQmlILFFBQXJCLENBQThCekcsSUFBOUIsQ0FBbUNILE9BQU8sQ0FBQ3dGLElBQTNDLENBQTNCLEdBQThFL0QsT0FBTyxDQUFDa0YsS0FBUixLQUFrQkUsTUFBbEIsR0FBMkIsSUFBSTVILEtBQUssQ0FBQ3NILFFBQVYsQ0FBbUJ2RyxPQUFPLENBQUN3RixJQUFSLENBQWFTLEdBQWIsS0FBcUIsQ0FBeEMsRUFBMkNqRyxPQUFPLENBQUN3RixJQUFSLENBQWFVLElBQWIsS0FBc0IsQ0FBakUsRUFBb0VNLFFBQXBFLENBQTZFLElBQTdFLENBQTNCLEdBQWdIeEcsT0FBTyxDQUFDd0YsSUFBcE47QUFDUixRQUFJeEYsT0FBTyxDQUFDMkYsT0FBUixJQUFtQixJQUFuQixJQUEyQjNGLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixTQUF2QixDQUEvQixFQUNJLElBQUksT0FBT0YsT0FBTyxDQUFDMkYsT0FBZixLQUEyQixRQUEvQixFQUNJckUsTUFBTSxDQUFDcUUsT0FBUCxHQUFpQmxFLE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JwRixNQUFsQixHQUEyQkEsTUFBTSxDQUFDdkIsT0FBTyxDQUFDMkYsT0FBVCxDQUFqQyxHQUFxRDNGLE9BQU8sQ0FBQzJGLE9BQTlFLENBREosS0FHSXJFLE1BQU0sQ0FBQ3FFLE9BQVAsR0FBaUJsRSxPQUFPLENBQUNrRixLQUFSLEtBQWtCcEYsTUFBbEIsR0FBMkJ0QyxLQUFLLENBQUN3RyxJQUFOLENBQVc5RixTQUFYLENBQXFCaUgsUUFBckIsQ0FBOEJ6RyxJQUE5QixDQUFtQ0gsT0FBTyxDQUFDMkYsT0FBM0MsQ0FBM0IsR0FBaUZsRSxPQUFPLENBQUNrRixLQUFSLEtBQWtCRSxNQUFsQixHQUEyQixJQUFJNUgsS0FBSyxDQUFDc0gsUUFBVixDQUFtQnZHLE9BQU8sQ0FBQzJGLE9BQVIsQ0FBZ0JNLEdBQWhCLEtBQXdCLENBQTNDLEVBQThDakcsT0FBTyxDQUFDMkYsT0FBUixDQUFnQk8sSUFBaEIsS0FBeUIsQ0FBdkUsRUFBMEVNLFFBQTFFLENBQW1GLElBQW5GLENBQTNCLEdBQXNIeEcsT0FBTyxDQUFDMkYsT0FBaE87O0FBQ1IsUUFBSTNGLE9BQU8sQ0FBQ3FGLGNBQVIsSUFBMEJyRixPQUFPLENBQUNxRixjQUFSLENBQXVCM0YsTUFBckQsRUFBNkQ7QUFDekQ0QixNQUFBQSxNQUFNLENBQUMrRCxjQUFQLEdBQXdCLEVBQXhCOztBQUNBLFdBQUssSUFBSXlCLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUc5RyxPQUFPLENBQUNxRixjQUFSLENBQXVCM0YsTUFBM0MsRUFBbUQsRUFBRW9ILENBQXJEO0FBQ0l4RixRQUFBQSxNQUFNLENBQUMrRCxjQUFQLENBQXNCeUIsQ0FBdEIsSUFBMkIzSCxLQUFLLENBQUM0RyxpQkFBTixDQUF3QnZFLFFBQXhCLENBQWlDeEIsT0FBTyxDQUFDcUYsY0FBUixDQUF1QnlCLENBQXZCLENBQWpDLEVBQTREckYsT0FBNUQsQ0FBM0I7QUFESjtBQUVIOztBQUNELFdBQU9ILE1BQVA7QUFDSCxHQXhDRDtBQTBDQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0ltRCxFQUFBQSxVQUFVLENBQUM5RSxTQUFYLENBQXFCZ0MsTUFBckIsR0FBOEIsU0FBU0EsTUFBVCxHQUFrQjtBQUM1QyxXQUFPLEtBQUtDLFdBQUwsQ0FBaUJKLFFBQWpCLENBQTBCLElBQTFCLEVBQWdDN0MsU0FBUyxDQUFDTyxJQUFWLENBQWUyQyxhQUEvQyxDQUFQO0FBQ0gsR0FGRDs7QUFJQSxTQUFPNEMsVUFBUDtBQUNILENBelVrQixFQUFuQjs7QUEyVUF0RixLQUFLLENBQUN5RixpQkFBTixHQUEyQixZQUFXO0FBRWxDO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ksV0FBU0EsaUJBQVQsQ0FBMkJ0RixVQUEzQixFQUF1QztBQUNuQyxRQUFJQSxVQUFKLEVBQ0ksS0FBSyxJQUFJQyxJQUFJLEdBQUdDLE1BQU0sQ0FBQ0QsSUFBUCxDQUFZRCxVQUFaLENBQVgsRUFBb0NHLENBQUMsR0FBRyxDQUE3QyxFQUFnREEsQ0FBQyxHQUFHRixJQUFJLENBQUNHLE1BQXpELEVBQWlFLEVBQUVELENBQW5FO0FBQ0ksVUFBSUgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUFWLElBQXVCLElBQTNCLEVBQ0ksS0FBS0YsSUFBSSxDQUFDRSxDQUFELENBQVQsSUFBZ0JILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBMUI7QUFGUjtBQUdQO0FBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSW1GLEVBQUFBLGlCQUFpQixDQUFDakYsU0FBbEIsQ0FBNEJvSCxZQUE1QixHQUEyQyxDQUEzQztBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0luQyxFQUFBQSxpQkFBaUIsQ0FBQzlFLE1BQWxCLEdBQTJCLFNBQVNBLE1BQVQsQ0FBZ0JSLFVBQWhCLEVBQTRCO0FBQ25ELFdBQU8sSUFBSXNGLGlCQUFKLENBQXNCdEYsVUFBdEIsQ0FBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJc0YsRUFBQUEsaUJBQWlCLENBQUM3RSxNQUFsQixHQUEyQixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDeEQsUUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUO0FBQ0osUUFBSUUsT0FBTyxDQUFDK0csWUFBUixJQUF3QixJQUF4QixJQUFnQ3ZILE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLGNBQXBDLENBQXBDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLEtBQXJDLEVBQXdDQyxLQUF4QyxDQUE4Q0wsT0FBTyxDQUFDK0csWUFBdEQ7QUFDSixXQUFPOUcsTUFBUDtBQUNILEdBTkQ7QUFRQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJMkUsRUFBQUEsaUJBQWlCLENBQUNyRSxlQUFsQixHQUFvQyxTQUFTQSxlQUFULENBQXlCUCxPQUF6QixFQUFrQ0MsTUFBbEMsRUFBMEM7QUFDMUUsV0FBTyxLQUFLRixNQUFMLENBQVlDLE9BQVosRUFBcUJDLE1BQXJCLEVBQTZCTyxNQUE3QixFQUFQO0FBQ0gsR0FGRDtBQUlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJb0UsRUFBQUEsaUJBQWlCLENBQUNuRSxNQUFsQixHQUEyQixTQUFTQSxNQUFULENBQWdCQyxNQUFoQixFQUF3QmhCLE1BQXhCLEVBQWdDO0FBQ3ZELFFBQUksRUFBRWdCLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRzdCLE9BQU8sQ0FBQ2lCLE1BQVIsQ0FBZVksTUFBZixDQUFUO0FBQ0osUUFBSUMsR0FBRyxHQUFHakIsTUFBTSxLQUFLa0IsU0FBWCxHQUF1QkYsTUFBTSxDQUFDRyxHQUE5QixHQUFvQ0gsTUFBTSxDQUFDSSxHQUFQLEdBQWFwQixNQUEzRDtBQUFBLFFBQW1FTSxPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDeUYsaUJBQVYsRUFBN0U7O0FBQ0EsV0FBT2xFLE1BQU0sQ0FBQ0ksR0FBUCxHQUFhSCxHQUFwQixFQUF5QjtBQUNyQixVQUFJSSxHQUFHLEdBQUdMLE1BQU0sQ0FBQ04sTUFBUCxFQUFWOztBQUNBLGNBQVFXLEdBQUcsS0FBSyxDQUFoQjtBQUNBLGFBQUssQ0FBTDtBQUNJZixVQUFBQSxPQUFPLENBQUMrRyxZQUFSLEdBQXVCckcsTUFBTSxDQUFDTCxLQUFQLEVBQXZCO0FBQ0E7O0FBQ0o7QUFDSUssVUFBQUEsTUFBTSxDQUFDTSxRQUFQLENBQWdCRCxHQUFHLEdBQUcsQ0FBdEI7QUFDQTtBQU5KO0FBUUg7O0FBQ0QsV0FBT2YsT0FBUDtBQUNILEdBaEJEO0FBa0JBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTRFLEVBQUFBLGlCQUFpQixDQUFDM0QsZUFBbEIsR0FBb0MsU0FBU0EsZUFBVCxDQUF5QlAsTUFBekIsRUFBaUM7QUFDakUsUUFBSSxFQUFFQSxNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUcsSUFBSTdCLE9BQUosQ0FBWTZCLE1BQVosQ0FBVDtBQUNKLFdBQU8sS0FBS0QsTUFBTCxDQUFZQyxNQUFaLEVBQW9CQSxNQUFNLENBQUNOLE1BQVAsRUFBcEIsQ0FBUDtBQUNILEdBSkQ7QUFNQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXdFLEVBQUFBLGlCQUFpQixDQUFDMUQsTUFBbEIsR0FBMkIsU0FBU0EsTUFBVCxDQUFnQmxCLE9BQWhCLEVBQXlCO0FBQ2hELFFBQUksT0FBT0EsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBTyxLQUFLLElBQS9DLEVBQ0ksT0FBTyxpQkFBUDtBQUNKLFFBQUlBLE9BQU8sQ0FBQytHLFlBQVIsSUFBd0IsSUFBeEIsSUFBZ0MvRyxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsY0FBdkIsQ0FBcEMsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDK0csWUFBeEIsQ0FBTCxFQUNJLE9BQU8sZ0NBQVA7QUFDUixXQUFPLElBQVA7QUFDSCxHQVBEO0FBU0E7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0luQyxFQUFBQSxpQkFBaUIsQ0FBQ3ZELFVBQWxCLEdBQStCLFNBQVNBLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQ3ZELFFBQUlBLE1BQU0sWUFBWW5DLEtBQUssQ0FBQ3lGLGlCQUE1QixFQUNJLE9BQU90RCxNQUFQO0FBQ0osUUFBSXRCLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUN5RixpQkFBVixFQUFkO0FBQ0EsUUFBSXRELE1BQU0sQ0FBQ3lGLFlBQVAsSUFBdUIsSUFBM0IsRUFDSS9HLE9BQU8sQ0FBQytHLFlBQVIsR0FBdUJ6RixNQUFNLENBQUN5RixZQUFQLEdBQXNCLENBQTdDO0FBQ0osV0FBTy9HLE9BQVA7QUFDSCxHQVBEO0FBU0E7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTRFLEVBQUFBLGlCQUFpQixDQUFDcEQsUUFBbEIsR0FBNkIsU0FBU0EsUUFBVCxDQUFrQnhCLE9BQWxCLEVBQTJCeUIsT0FBM0IsRUFBb0M7QUFDN0QsUUFBSSxDQUFDQSxPQUFMLEVBQ0lBLE9BQU8sR0FBRyxFQUFWO0FBQ0osUUFBSUgsTUFBTSxHQUFHLEVBQWI7QUFDQSxRQUFJRyxPQUFPLENBQUNDLFFBQVosRUFDSUosTUFBTSxDQUFDeUYsWUFBUCxHQUFzQixDQUF0QjtBQUNKLFFBQUkvRyxPQUFPLENBQUMrRyxZQUFSLElBQXdCLElBQXhCLElBQWdDL0csT0FBTyxDQUFDRSxjQUFSLENBQXVCLGNBQXZCLENBQXBDLEVBQ0lvQixNQUFNLENBQUN5RixZQUFQLEdBQXNCL0csT0FBTyxDQUFDK0csWUFBOUI7QUFDSixXQUFPekYsTUFBUDtBQUNILEdBVEQ7QUFXQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lzRCxFQUFBQSxpQkFBaUIsQ0FBQ2pGLFNBQWxCLENBQTRCZ0MsTUFBNUIsR0FBcUMsU0FBU0EsTUFBVCxHQUFrQjtBQUNuRCxXQUFPLEtBQUtDLFdBQUwsQ0FBaUJKLFFBQWpCLENBQTBCLElBQTFCLEVBQWdDN0MsU0FBUyxDQUFDTyxJQUFWLENBQWUyQyxhQUEvQyxDQUFQO0FBQ0gsR0FGRDs7QUFJQSxTQUFPK0MsaUJBQVA7QUFDSCxDQXpMeUIsRUFBMUI7O0FBMkxBekYsS0FBSyxDQUFDMEYsZUFBTixHQUF5QixZQUFXO0FBRWhDO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNJLFdBQVNBLGVBQVQsQ0FBeUJ2RixVQUF6QixFQUFxQztBQUNqQyxRQUFJQSxVQUFKLEVBQ0ksS0FBSyxJQUFJQyxJQUFJLEdBQUdDLE1BQU0sQ0FBQ0QsSUFBUCxDQUFZRCxVQUFaLENBQVgsRUFBb0NHLENBQUMsR0FBRyxDQUE3QyxFQUFnREEsQ0FBQyxHQUFHRixJQUFJLENBQUNHLE1BQXpELEVBQWlFLEVBQUVELENBQW5FO0FBQ0ksVUFBSUgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUFWLElBQXVCLElBQTNCLEVBQ0ksS0FBS0YsSUFBSSxDQUFDRSxDQUFELENBQVQsSUFBZ0JILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBMUI7QUFGUjtBQUdQO0FBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSW9GLEVBQUFBLGVBQWUsQ0FBQ2xGLFNBQWhCLENBQTBCcUgsT0FBMUIsR0FBb0MsQ0FBcEM7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0luQyxFQUFBQSxlQUFlLENBQUNsRixTQUFoQixDQUEwQm9ILFlBQTFCLEdBQXlDLENBQXpDO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJbEMsRUFBQUEsZUFBZSxDQUFDbEYsU0FBaEIsQ0FBMEJzSCxRQUExQixHQUFxQyxDQUFyQztBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lwQyxFQUFBQSxlQUFlLENBQUMvRSxNQUFoQixHQUF5QixTQUFTQSxNQUFULENBQWdCUixVQUFoQixFQUE0QjtBQUNqRCxXQUFPLElBQUl1RixlQUFKLENBQW9CdkYsVUFBcEIsQ0FBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJdUYsRUFBQUEsZUFBZSxDQUFDOUUsTUFBaEIsR0FBeUIsU0FBU0EsTUFBVCxDQUFnQkMsT0FBaEIsRUFBeUJDLE1BQXpCLEVBQWlDO0FBQ3RELFFBQUksQ0FBQ0EsTUFBTCxFQUNJQSxNQUFNLEdBQUdsQixPQUFPLENBQUNlLE1BQVIsRUFBVDtBQUNKLFFBQUlFLE9BQU8sQ0FBQ2dILE9BQVIsSUFBbUIsSUFBbkIsSUFBMkJ4SCxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxTQUFwQyxDQUEvQixFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixLQUFyQyxFQUF3Q0MsS0FBeEMsQ0FBOENMLE9BQU8sQ0FBQ2dILE9BQXREO0FBQ0osUUFBSWhILE9BQU8sQ0FBQytHLFlBQVIsSUFBd0IsSUFBeEIsSUFBZ0N2SCxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxjQUFwQyxDQUFwQyxFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixNQUFyQyxFQUF5Q0MsS0FBekMsQ0FBK0NMLE9BQU8sQ0FBQytHLFlBQXZEO0FBQ0osUUFBSS9HLE9BQU8sQ0FBQ2lILFFBQVIsSUFBb0IsSUFBcEIsSUFBNEJ6SCxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxVQUFwQyxDQUFoQyxFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixNQUFyQyxFQUF5Q0MsS0FBekMsQ0FBK0NMLE9BQU8sQ0FBQ2lILFFBQXZEO0FBQ0osV0FBT2hILE1BQVA7QUFDSCxHQVZEO0FBWUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTRFLEVBQUFBLGVBQWUsQ0FBQ3RFLGVBQWhCLEdBQWtDLFNBQVNBLGVBQVQsQ0FBeUJQLE9BQXpCLEVBQWtDQyxNQUFsQyxFQUEwQztBQUN4RSxXQUFPLEtBQUtGLE1BQUwsQ0FBWUMsT0FBWixFQUFxQkMsTUFBckIsRUFBNkJPLE1BQTdCLEVBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lxRSxFQUFBQSxlQUFlLENBQUNwRSxNQUFoQixHQUF5QixTQUFTQSxNQUFULENBQWdCQyxNQUFoQixFQUF3QmhCLE1BQXhCLEVBQWdDO0FBQ3JELFFBQUksRUFBRWdCLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRzdCLE9BQU8sQ0FBQ2lCLE1BQVIsQ0FBZVksTUFBZixDQUFUO0FBQ0osUUFBSUMsR0FBRyxHQUFHakIsTUFBTSxLQUFLa0IsU0FBWCxHQUF1QkYsTUFBTSxDQUFDRyxHQUE5QixHQUFvQ0gsTUFBTSxDQUFDSSxHQUFQLEdBQWFwQixNQUEzRDtBQUFBLFFBQW1FTSxPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDMEYsZUFBVixFQUE3RTs7QUFDQSxXQUFPbkUsTUFBTSxDQUFDSSxHQUFQLEdBQWFILEdBQXBCLEVBQXlCO0FBQ3JCLFVBQUlJLEdBQUcsR0FBR0wsTUFBTSxDQUFDTixNQUFQLEVBQVY7O0FBQ0EsY0FBUVcsR0FBRyxLQUFLLENBQWhCO0FBQ0EsYUFBSyxDQUFMO0FBQ0lmLFVBQUFBLE9BQU8sQ0FBQ2dILE9BQVIsR0FBa0J0RyxNQUFNLENBQUNMLEtBQVAsRUFBbEI7QUFDQTs7QUFDSixhQUFLLENBQUw7QUFDSUwsVUFBQUEsT0FBTyxDQUFDK0csWUFBUixHQUF1QnJHLE1BQU0sQ0FBQ0wsS0FBUCxFQUF2QjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJTCxVQUFBQSxPQUFPLENBQUNpSCxRQUFSLEdBQW1CdkcsTUFBTSxDQUFDTCxLQUFQLEVBQW5CO0FBQ0E7O0FBQ0o7QUFDSUssVUFBQUEsTUFBTSxDQUFDTSxRQUFQLENBQWdCRCxHQUFHLEdBQUcsQ0FBdEI7QUFDQTtBQVpKO0FBY0g7O0FBQ0QsV0FBT2YsT0FBUDtBQUNILEdBdEJEO0FBd0JBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTZFLEVBQUFBLGVBQWUsQ0FBQzVELGVBQWhCLEdBQWtDLFNBQVNBLGVBQVQsQ0FBeUJQLE1BQXpCLEVBQWlDO0FBQy9ELFFBQUksRUFBRUEsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHLElBQUk3QixPQUFKLENBQVk2QixNQUFaLENBQVQ7QUFDSixXQUFPLEtBQUtELE1BQUwsQ0FBWUMsTUFBWixFQUFvQkEsTUFBTSxDQUFDTixNQUFQLEVBQXBCLENBQVA7QUFDSCxHQUpEO0FBTUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l5RSxFQUFBQSxlQUFlLENBQUMzRCxNQUFoQixHQUF5QixTQUFTQSxNQUFULENBQWdCbEIsT0FBaEIsRUFBeUI7QUFDOUMsUUFBSSxPQUFPQSxPQUFQLEtBQW1CLFFBQW5CLElBQStCQSxPQUFPLEtBQUssSUFBL0MsRUFDSSxPQUFPLGlCQUFQO0FBQ0osUUFBSUEsT0FBTyxDQUFDZ0gsT0FBUixJQUFtQixJQUFuQixJQUEyQmhILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixTQUF2QixDQUEvQixFQUNJLElBQUksQ0FBQ2pCLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUNnSCxPQUF4QixDQUFMLEVBQ0ksT0FBTywyQkFBUDtBQUNSLFFBQUloSCxPQUFPLENBQUMrRyxZQUFSLElBQXdCLElBQXhCLElBQWdDL0csT0FBTyxDQUFDRSxjQUFSLENBQXVCLGNBQXZCLENBQXBDLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQytHLFlBQXhCLENBQUwsRUFDSSxPQUFPLGdDQUFQO0FBQ1IsUUFBSS9HLE9BQU8sQ0FBQ2lILFFBQVIsSUFBb0IsSUFBcEIsSUFBNEJqSCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsVUFBdkIsQ0FBaEMsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDaUgsUUFBeEIsQ0FBTCxFQUNJLE9BQU8sNEJBQVA7QUFDUixXQUFPLElBQVA7QUFDSCxHQWJEO0FBZUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lwQyxFQUFBQSxlQUFlLENBQUN4RCxVQUFoQixHQUE2QixTQUFTQSxVQUFULENBQW9CQyxNQUFwQixFQUE0QjtBQUNyRCxRQUFJQSxNQUFNLFlBQVluQyxLQUFLLENBQUMwRixlQUE1QixFQUNJLE9BQU92RCxNQUFQO0FBQ0osUUFBSXRCLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUMwRixlQUFWLEVBQWQ7QUFDQSxRQUFJdkQsTUFBTSxDQUFDMEYsT0FBUCxJQUFrQixJQUF0QixFQUNJaEgsT0FBTyxDQUFDZ0gsT0FBUixHQUFrQjFGLE1BQU0sQ0FBQzBGLE9BQVAsR0FBaUIsQ0FBbkM7QUFDSixRQUFJMUYsTUFBTSxDQUFDeUYsWUFBUCxJQUF1QixJQUEzQixFQUNJL0csT0FBTyxDQUFDK0csWUFBUixHQUF1QnpGLE1BQU0sQ0FBQ3lGLFlBQVAsR0FBc0IsQ0FBN0M7QUFDSixRQUFJekYsTUFBTSxDQUFDMkYsUUFBUCxJQUFtQixJQUF2QixFQUNJakgsT0FBTyxDQUFDaUgsUUFBUixHQUFtQjNGLE1BQU0sQ0FBQzJGLFFBQVAsR0FBa0IsQ0FBckM7QUFDSixXQUFPakgsT0FBUDtBQUNILEdBWEQ7QUFhQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJNkUsRUFBQUEsZUFBZSxDQUFDckQsUUFBaEIsR0FBMkIsU0FBU0EsUUFBVCxDQUFrQnhCLE9BQWxCLEVBQTJCeUIsT0FBM0IsRUFBb0M7QUFDM0QsUUFBSSxDQUFDQSxPQUFMLEVBQ0lBLE9BQU8sR0FBRyxFQUFWO0FBQ0osUUFBSUgsTUFBTSxHQUFHLEVBQWI7O0FBQ0EsUUFBSUcsT0FBTyxDQUFDQyxRQUFaLEVBQXNCO0FBQ2xCSixNQUFBQSxNQUFNLENBQUMwRixPQUFQLEdBQWlCLENBQWpCO0FBQ0ExRixNQUFBQSxNQUFNLENBQUN5RixZQUFQLEdBQXNCLENBQXRCO0FBQ0F6RixNQUFBQSxNQUFNLENBQUMyRixRQUFQLEdBQWtCLENBQWxCO0FBQ0g7O0FBQ0QsUUFBSWpILE9BQU8sQ0FBQ2dILE9BQVIsSUFBbUIsSUFBbkIsSUFBMkJoSCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsU0FBdkIsQ0FBL0IsRUFDSW9CLE1BQU0sQ0FBQzBGLE9BQVAsR0FBaUJoSCxPQUFPLENBQUNnSCxPQUF6QjtBQUNKLFFBQUloSCxPQUFPLENBQUMrRyxZQUFSLElBQXdCLElBQXhCLElBQWdDL0csT0FBTyxDQUFDRSxjQUFSLENBQXVCLGNBQXZCLENBQXBDLEVBQ0lvQixNQUFNLENBQUN5RixZQUFQLEdBQXNCL0csT0FBTyxDQUFDK0csWUFBOUI7QUFDSixRQUFJL0csT0FBTyxDQUFDaUgsUUFBUixJQUFvQixJQUFwQixJQUE0QmpILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixVQUF2QixDQUFoQyxFQUNJb0IsTUFBTSxDQUFDMkYsUUFBUCxHQUFrQmpILE9BQU8sQ0FBQ2lILFFBQTFCO0FBQ0osV0FBTzNGLE1BQVA7QUFDSCxHQWhCRDtBQWtCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l1RCxFQUFBQSxlQUFlLENBQUNsRixTQUFoQixDQUEwQmdDLE1BQTFCLEdBQW1DLFNBQVNBLE1BQVQsR0FBa0I7QUFDakQsV0FBTyxLQUFLQyxXQUFMLENBQWlCSixRQUFqQixDQUEwQixJQUExQixFQUFnQzdDLFNBQVMsQ0FBQ08sSUFBVixDQUFlMkMsYUFBL0MsQ0FBUDtBQUNILEdBRkQ7O0FBSUEsU0FBT2dELGVBQVA7QUFDSCxDQXRPdUIsRUFBeEI7O0FBd09BMUYsS0FBSyxDQUFDNEYscUJBQU4sR0FBK0IsWUFBVztBQUV0QztBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ksV0FBU0EscUJBQVQsQ0FBK0J6RixVQUEvQixFQUEyQztBQUN2QyxRQUFJQSxVQUFKLEVBQ0ksS0FBSyxJQUFJQyxJQUFJLEdBQUdDLE1BQU0sQ0FBQ0QsSUFBUCxDQUFZRCxVQUFaLENBQVgsRUFBb0NHLENBQUMsR0FBRyxDQUE3QyxFQUFnREEsQ0FBQyxHQUFHRixJQUFJLENBQUNHLE1BQXpELEVBQWlFLEVBQUVELENBQW5FO0FBQ0ksVUFBSUgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUFWLElBQXVCLElBQTNCLEVBQ0ksS0FBS0YsSUFBSSxDQUFDRSxDQUFELENBQVQsSUFBZ0JILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBMUI7QUFGUjtBQUdQO0FBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXNGLEVBQUFBLHFCQUFxQixDQUFDcEYsU0FBdEIsQ0FBZ0N1SCxZQUFoQyxHQUErQyxDQUEvQztBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSW5DLEVBQUFBLHFCQUFxQixDQUFDcEYsU0FBdEIsQ0FBZ0N3SCxjQUFoQyxHQUFpRCxDQUFqRDtBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lwQyxFQUFBQSxxQkFBcUIsQ0FBQ2pGLE1BQXRCLEdBQStCLFNBQVNBLE1BQVQsQ0FBZ0JSLFVBQWhCLEVBQTRCO0FBQ3ZELFdBQU8sSUFBSXlGLHFCQUFKLENBQTBCekYsVUFBMUIsQ0FBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJeUYsRUFBQUEscUJBQXFCLENBQUNoRixNQUF0QixHQUErQixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDNUQsUUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUO0FBQ0osUUFBSUUsT0FBTyxDQUFDa0gsWUFBUixJQUF3QixJQUF4QixJQUFnQzFILE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLGNBQXBDLENBQXBDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLEtBQXJDLEVBQXdDQyxLQUF4QyxDQUE4Q0wsT0FBTyxDQUFDa0gsWUFBdEQ7QUFDSixRQUFJbEgsT0FBTyxDQUFDbUgsY0FBUixJQUEwQixJQUExQixJQUFrQzNILE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLGdCQUFwQyxDQUF0QyxFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixNQUFyQyxFQUF5Q0MsS0FBekMsQ0FBK0NMLE9BQU8sQ0FBQ21ILGNBQXZEO0FBQ0osV0FBT2xILE1BQVA7QUFDSCxHQVJEO0FBVUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSThFLEVBQUFBLHFCQUFxQixDQUFDeEUsZUFBdEIsR0FBd0MsU0FBU0EsZUFBVCxDQUF5QlAsT0FBekIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQzlFLFdBQU8sS0FBS0YsTUFBTCxDQUFZQyxPQUFaLEVBQXFCQyxNQUFyQixFQUE2Qk8sTUFBN0IsRUFBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXVFLEVBQUFBLHFCQUFxQixDQUFDdEUsTUFBdEIsR0FBK0IsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0JoQixNQUF4QixFQUFnQztBQUMzRCxRQUFJLEVBQUVnQixNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUc3QixPQUFPLENBQUNpQixNQUFSLENBQWVZLE1BQWYsQ0FBVDtBQUNKLFFBQUlDLEdBQUcsR0FBR2pCLE1BQU0sS0FBS2tCLFNBQVgsR0FBdUJGLE1BQU0sQ0FBQ0csR0FBOUIsR0FBb0NILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhcEIsTUFBM0Q7QUFBQSxRQUFtRU0sT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQzRGLHFCQUFWLEVBQTdFOztBQUNBLFdBQU9yRSxNQUFNLENBQUNJLEdBQVAsR0FBYUgsR0FBcEIsRUFBeUI7QUFDckIsVUFBSUksR0FBRyxHQUFHTCxNQUFNLENBQUNOLE1BQVAsRUFBVjs7QUFDQSxjQUFRVyxHQUFHLEtBQUssQ0FBaEI7QUFDQSxhQUFLLENBQUw7QUFDSWYsVUFBQUEsT0FBTyxDQUFDa0gsWUFBUixHQUF1QnhHLE1BQU0sQ0FBQ0wsS0FBUCxFQUF2QjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJTCxVQUFBQSxPQUFPLENBQUNtSCxjQUFSLEdBQXlCekcsTUFBTSxDQUFDTCxLQUFQLEVBQXpCO0FBQ0E7O0FBQ0o7QUFDSUssVUFBQUEsTUFBTSxDQUFDTSxRQUFQLENBQWdCRCxHQUFHLEdBQUcsQ0FBdEI7QUFDQTtBQVRKO0FBV0g7O0FBQ0QsV0FBT2YsT0FBUDtBQUNILEdBbkJEO0FBcUJBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSStFLEVBQUFBLHFCQUFxQixDQUFDOUQsZUFBdEIsR0FBd0MsU0FBU0EsZUFBVCxDQUF5QlAsTUFBekIsRUFBaUM7QUFDckUsUUFBSSxFQUFFQSxNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUcsSUFBSTdCLE9BQUosQ0FBWTZCLE1BQVosQ0FBVDtBQUNKLFdBQU8sS0FBS0QsTUFBTCxDQUFZQyxNQUFaLEVBQW9CQSxNQUFNLENBQUNOLE1BQVAsRUFBcEIsQ0FBUDtBQUNILEdBSkQ7QUFNQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTJFLEVBQUFBLHFCQUFxQixDQUFDN0QsTUFBdEIsR0FBK0IsU0FBU0EsTUFBVCxDQUFnQmxCLE9BQWhCLEVBQXlCO0FBQ3BELFFBQUksT0FBT0EsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBTyxLQUFLLElBQS9DLEVBQ0ksT0FBTyxpQkFBUDtBQUNKLFFBQUlBLE9BQU8sQ0FBQ2tILFlBQVIsSUFBd0IsSUFBeEIsSUFBZ0NsSCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsY0FBdkIsQ0FBcEMsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDa0gsWUFBeEIsQ0FBTCxFQUNJLE9BQU8sZ0NBQVA7QUFDUixRQUFJbEgsT0FBTyxDQUFDbUgsY0FBUixJQUEwQixJQUExQixJQUFrQ25ILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixnQkFBdkIsQ0FBdEMsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDbUgsY0FBeEIsQ0FBTCxFQUNJLE9BQU8sa0NBQVA7QUFDUixXQUFPLElBQVA7QUFDSCxHQVZEO0FBWUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lwQyxFQUFBQSxxQkFBcUIsQ0FBQzFELFVBQXRCLEdBQW1DLFNBQVNBLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQzNELFFBQUlBLE1BQU0sWUFBWW5DLEtBQUssQ0FBQzRGLHFCQUE1QixFQUNJLE9BQU96RCxNQUFQO0FBQ0osUUFBSXRCLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUM0RixxQkFBVixFQUFkO0FBQ0EsUUFBSXpELE1BQU0sQ0FBQzRGLFlBQVAsSUFBdUIsSUFBM0IsRUFDSWxILE9BQU8sQ0FBQ2tILFlBQVIsR0FBdUI1RixNQUFNLENBQUM0RixZQUFQLEdBQXNCLENBQTdDO0FBQ0osUUFBSTVGLE1BQU0sQ0FBQzZGLGNBQVAsSUFBeUIsSUFBN0IsRUFDSW5ILE9BQU8sQ0FBQ21ILGNBQVIsR0FBeUI3RixNQUFNLENBQUM2RixjQUFQLEdBQXdCLENBQWpEO0FBQ0osV0FBT25ILE9BQVA7QUFDSCxHQVREO0FBV0E7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSStFLEVBQUFBLHFCQUFxQixDQUFDdkQsUUFBdEIsR0FBaUMsU0FBU0EsUUFBVCxDQUFrQnhCLE9BQWxCLEVBQTJCeUIsT0FBM0IsRUFBb0M7QUFDakUsUUFBSSxDQUFDQSxPQUFMLEVBQ0lBLE9BQU8sR0FBRyxFQUFWO0FBQ0osUUFBSUgsTUFBTSxHQUFHLEVBQWI7O0FBQ0EsUUFBSUcsT0FBTyxDQUFDQyxRQUFaLEVBQXNCO0FBQ2xCSixNQUFBQSxNQUFNLENBQUM0RixZQUFQLEdBQXNCLENBQXRCO0FBQ0E1RixNQUFBQSxNQUFNLENBQUM2RixjQUFQLEdBQXdCLENBQXhCO0FBQ0g7O0FBQ0QsUUFBSW5ILE9BQU8sQ0FBQ2tILFlBQVIsSUFBd0IsSUFBeEIsSUFBZ0NsSCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsY0FBdkIsQ0FBcEMsRUFDSW9CLE1BQU0sQ0FBQzRGLFlBQVAsR0FBc0JsSCxPQUFPLENBQUNrSCxZQUE5QjtBQUNKLFFBQUlsSCxPQUFPLENBQUNtSCxjQUFSLElBQTBCLElBQTFCLElBQWtDbkgsT0FBTyxDQUFDRSxjQUFSLENBQXVCLGdCQUF2QixDQUF0QyxFQUNJb0IsTUFBTSxDQUFDNkYsY0FBUCxHQUF3Qm5ILE9BQU8sQ0FBQ21ILGNBQWhDO0FBQ0osV0FBTzdGLE1BQVA7QUFDSCxHQWJEO0FBZUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJeUQsRUFBQUEscUJBQXFCLENBQUNwRixTQUF0QixDQUFnQ2dDLE1BQWhDLEdBQXlDLFNBQVNBLE1BQVQsR0FBa0I7QUFDdkQsV0FBTyxLQUFLQyxXQUFMLENBQWlCSixRQUFqQixDQUEwQixJQUExQixFQUFnQzdDLFNBQVMsQ0FBQ08sSUFBVixDQUFlMkMsYUFBL0MsQ0FBUDtBQUNILEdBRkQ7O0FBSUEsU0FBT2tELHFCQUFQO0FBQ0gsQ0FoTjZCLEVBQTlCOztBQWtOQTVGLEtBQUssQ0FBQzZGLG1CQUFOLEdBQTZCLFlBQVc7QUFFcEM7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ksV0FBU0EsbUJBQVQsQ0FBNkIxRixVQUE3QixFQUF5QztBQUNyQyxRQUFJQSxVQUFKLEVBQ0ksS0FBSyxJQUFJQyxJQUFJLEdBQUdDLE1BQU0sQ0FBQ0QsSUFBUCxDQUFZRCxVQUFaLENBQVgsRUFBb0NHLENBQUMsR0FBRyxDQUE3QyxFQUFnREEsQ0FBQyxHQUFHRixJQUFJLENBQUNHLE1BQXpELEVBQWlFLEVBQUVELENBQW5FO0FBQ0ksVUFBSUgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUFWLElBQXVCLElBQTNCLEVBQ0ksS0FBS0YsSUFBSSxDQUFDRSxDQUFELENBQVQsSUFBZ0JILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBMUI7QUFGUjtBQUdQO0FBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXVGLEVBQUFBLG1CQUFtQixDQUFDckYsU0FBcEIsQ0FBOEJxSCxPQUE5QixHQUF3QyxDQUF4QztBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSWhDLEVBQUFBLG1CQUFtQixDQUFDckYsU0FBcEIsQ0FBOEJvSCxZQUE5QixHQUE2QyxDQUE3QztBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSS9CLEVBQUFBLG1CQUFtQixDQUFDckYsU0FBcEIsQ0FBOEJ5SCxXQUE5QixHQUE0QyxDQUE1QztBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lwQyxFQUFBQSxtQkFBbUIsQ0FBQ2xGLE1BQXBCLEdBQTZCLFNBQVNBLE1BQVQsQ0FBZ0JSLFVBQWhCLEVBQTRCO0FBQ3JELFdBQU8sSUFBSTBGLG1CQUFKLENBQXdCMUYsVUFBeEIsQ0FBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJMEYsRUFBQUEsbUJBQW1CLENBQUNqRixNQUFwQixHQUE2QixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDMUQsUUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUO0FBQ0osUUFBSUUsT0FBTyxDQUFDZ0gsT0FBUixJQUFtQixJQUFuQixJQUEyQnhILE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFNBQXBDLENBQS9CLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLEtBQXJDLEVBQXdDQyxLQUF4QyxDQUE4Q0wsT0FBTyxDQUFDZ0gsT0FBdEQ7QUFDSixRQUFJaEgsT0FBTyxDQUFDK0csWUFBUixJQUF3QixJQUF4QixJQUFnQ3ZILE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLGNBQXBDLENBQXBDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDQyxLQUF6QyxDQUErQ0wsT0FBTyxDQUFDK0csWUFBdkQ7QUFDSixRQUFJL0csT0FBTyxDQUFDb0gsV0FBUixJQUF1QixJQUF2QixJQUErQjVILE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLGFBQXBDLENBQW5DLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDQyxLQUF6QyxDQUErQ0wsT0FBTyxDQUFDb0gsV0FBdkQ7QUFDSixXQUFPbkgsTUFBUDtBQUNILEdBVkQ7QUFZQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJK0UsRUFBQUEsbUJBQW1CLENBQUN6RSxlQUFwQixHQUFzQyxTQUFTQSxlQUFULENBQXlCUCxPQUF6QixFQUFrQ0MsTUFBbEMsRUFBMEM7QUFDNUUsV0FBTyxLQUFLRixNQUFMLENBQVlDLE9BQVosRUFBcUJDLE1BQXJCLEVBQTZCTyxNQUE3QixFQUFQO0FBQ0gsR0FGRDtBQUlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJd0UsRUFBQUEsbUJBQW1CLENBQUN2RSxNQUFwQixHQUE2QixTQUFTQSxNQUFULENBQWdCQyxNQUFoQixFQUF3QmhCLE1BQXhCLEVBQWdDO0FBQ3pELFFBQUksRUFBRWdCLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRzdCLE9BQU8sQ0FBQ2lCLE1BQVIsQ0FBZVksTUFBZixDQUFUO0FBQ0osUUFBSUMsR0FBRyxHQUFHakIsTUFBTSxLQUFLa0IsU0FBWCxHQUF1QkYsTUFBTSxDQUFDRyxHQUE5QixHQUFvQ0gsTUFBTSxDQUFDSSxHQUFQLEdBQWFwQixNQUEzRDtBQUFBLFFBQW1FTSxPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDNkYsbUJBQVYsRUFBN0U7O0FBQ0EsV0FBT3RFLE1BQU0sQ0FBQ0ksR0FBUCxHQUFhSCxHQUFwQixFQUF5QjtBQUNyQixVQUFJSSxHQUFHLEdBQUdMLE1BQU0sQ0FBQ04sTUFBUCxFQUFWOztBQUNBLGNBQVFXLEdBQUcsS0FBSyxDQUFoQjtBQUNBLGFBQUssQ0FBTDtBQUNJZixVQUFBQSxPQUFPLENBQUNnSCxPQUFSLEdBQWtCdEcsTUFBTSxDQUFDTCxLQUFQLEVBQWxCO0FBQ0E7O0FBQ0osYUFBSyxDQUFMO0FBQ0lMLFVBQUFBLE9BQU8sQ0FBQytHLFlBQVIsR0FBdUJyRyxNQUFNLENBQUNMLEtBQVAsRUFBdkI7QUFDQTs7QUFDSixhQUFLLENBQUw7QUFDSUwsVUFBQUEsT0FBTyxDQUFDb0gsV0FBUixHQUFzQjFHLE1BQU0sQ0FBQ0wsS0FBUCxFQUF0QjtBQUNBOztBQUNKO0FBQ0lLLFVBQUFBLE1BQU0sQ0FBQ00sUUFBUCxDQUFnQkQsR0FBRyxHQUFHLENBQXRCO0FBQ0E7QUFaSjtBQWNIOztBQUNELFdBQU9mLE9BQVA7QUFDSCxHQXRCRDtBQXdCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lnRixFQUFBQSxtQkFBbUIsQ0FBQy9ELGVBQXBCLEdBQXNDLFNBQVNBLGVBQVQsQ0FBeUJQLE1BQXpCLEVBQWlDO0FBQ25FLFFBQUksRUFBRUEsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHLElBQUk3QixPQUFKLENBQVk2QixNQUFaLENBQVQ7QUFDSixXQUFPLEtBQUtELE1BQUwsQ0FBWUMsTUFBWixFQUFvQkEsTUFBTSxDQUFDTixNQUFQLEVBQXBCLENBQVA7QUFDSCxHQUpEO0FBTUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0k0RSxFQUFBQSxtQkFBbUIsQ0FBQzlELE1BQXBCLEdBQTZCLFNBQVNBLE1BQVQsQ0FBZ0JsQixPQUFoQixFQUF5QjtBQUNsRCxRQUFJLE9BQU9BLE9BQVAsS0FBbUIsUUFBbkIsSUFBK0JBLE9BQU8sS0FBSyxJQUEvQyxFQUNJLE9BQU8saUJBQVA7QUFDSixRQUFJQSxPQUFPLENBQUNnSCxPQUFSLElBQW1CLElBQW5CLElBQTJCaEgsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFNBQXZCLENBQS9CLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQ2dILE9BQXhCLENBQUwsRUFDSSxPQUFPLDJCQUFQO0FBQ1IsUUFBSWhILE9BQU8sQ0FBQytHLFlBQVIsSUFBd0IsSUFBeEIsSUFBZ0MvRyxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsY0FBdkIsQ0FBcEMsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDK0csWUFBeEIsQ0FBTCxFQUNJLE9BQU8sZ0NBQVA7QUFDUixRQUFJL0csT0FBTyxDQUFDb0gsV0FBUixJQUF1QixJQUF2QixJQUErQnBILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixhQUF2QixDQUFuQyxFQUNJLElBQUksQ0FBQ2pCLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUNvSCxXQUF4QixDQUFMLEVBQ0ksT0FBTywrQkFBUDtBQUNSLFdBQU8sSUFBUDtBQUNILEdBYkQ7QUFlQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXBDLEVBQUFBLG1CQUFtQixDQUFDM0QsVUFBcEIsR0FBaUMsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDekQsUUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDNkYsbUJBQTVCLEVBQ0ksT0FBTzFELE1BQVA7QUFDSixRQUFJdEIsT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQzZGLG1CQUFWLEVBQWQ7QUFDQSxRQUFJMUQsTUFBTSxDQUFDMEYsT0FBUCxJQUFrQixJQUF0QixFQUNJaEgsT0FBTyxDQUFDZ0gsT0FBUixHQUFrQjFGLE1BQU0sQ0FBQzBGLE9BQVAsR0FBaUIsQ0FBbkM7QUFDSixRQUFJMUYsTUFBTSxDQUFDeUYsWUFBUCxJQUF1QixJQUEzQixFQUNJL0csT0FBTyxDQUFDK0csWUFBUixHQUF1QnpGLE1BQU0sQ0FBQ3lGLFlBQVAsR0FBc0IsQ0FBN0M7QUFDSixRQUFJekYsTUFBTSxDQUFDOEYsV0FBUCxJQUFzQixJQUExQixFQUNJcEgsT0FBTyxDQUFDb0gsV0FBUixHQUFzQjlGLE1BQU0sQ0FBQzhGLFdBQVAsR0FBcUIsQ0FBM0M7QUFDSixXQUFPcEgsT0FBUDtBQUNILEdBWEQ7QUFhQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJZ0YsRUFBQUEsbUJBQW1CLENBQUN4RCxRQUFwQixHQUErQixTQUFTQSxRQUFULENBQWtCeEIsT0FBbEIsRUFBMkJ5QixPQUEzQixFQUFvQztBQUMvRCxRQUFJLENBQUNBLE9BQUwsRUFDSUEsT0FBTyxHQUFHLEVBQVY7QUFDSixRQUFJSCxNQUFNLEdBQUcsRUFBYjs7QUFDQSxRQUFJRyxPQUFPLENBQUNDLFFBQVosRUFBc0I7QUFDbEJKLE1BQUFBLE1BQU0sQ0FBQzBGLE9BQVAsR0FBaUIsQ0FBakI7QUFDQTFGLE1BQUFBLE1BQU0sQ0FBQ3lGLFlBQVAsR0FBc0IsQ0FBdEI7QUFDQXpGLE1BQUFBLE1BQU0sQ0FBQzhGLFdBQVAsR0FBcUIsQ0FBckI7QUFDSDs7QUFDRCxRQUFJcEgsT0FBTyxDQUFDZ0gsT0FBUixJQUFtQixJQUFuQixJQUEyQmhILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixTQUF2QixDQUEvQixFQUNJb0IsTUFBTSxDQUFDMEYsT0FBUCxHQUFpQmhILE9BQU8sQ0FBQ2dILE9BQXpCO0FBQ0osUUFBSWhILE9BQU8sQ0FBQytHLFlBQVIsSUFBd0IsSUFBeEIsSUFBZ0MvRyxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsY0FBdkIsQ0FBcEMsRUFDSW9CLE1BQU0sQ0FBQ3lGLFlBQVAsR0FBc0IvRyxPQUFPLENBQUMrRyxZQUE5QjtBQUNKLFFBQUkvRyxPQUFPLENBQUNvSCxXQUFSLElBQXVCLElBQXZCLElBQStCcEgsT0FBTyxDQUFDRSxjQUFSLENBQXVCLGFBQXZCLENBQW5DLEVBQ0lvQixNQUFNLENBQUM4RixXQUFQLEdBQXFCcEgsT0FBTyxDQUFDb0gsV0FBN0I7QUFDSixXQUFPOUYsTUFBUDtBQUNILEdBaEJEO0FBa0JBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTBELEVBQUFBLG1CQUFtQixDQUFDckYsU0FBcEIsQ0FBOEJnQyxNQUE5QixHQUF1QyxTQUFTQSxNQUFULEdBQWtCO0FBQ3JELFdBQU8sS0FBS0MsV0FBTCxDQUFpQkosUUFBakIsQ0FBMEIsSUFBMUIsRUFBZ0M3QyxTQUFTLENBQUNPLElBQVYsQ0FBZTJDLGFBQS9DLENBQVA7QUFDSCxHQUZEOztBQUlBLFNBQU9tRCxtQkFBUDtBQUNILENBdE8yQixFQUE1Qjs7QUF3T0E3RixLQUFLLENBQUMrRix1QkFBTixHQUFpQyxZQUFXO0FBRXhDO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7O0FBRUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNJLFdBQVNBLHVCQUFULENBQWlDNUYsVUFBakMsRUFBNkM7QUFDekMsUUFBSUEsVUFBSixFQUNJLEtBQUssSUFBSUMsSUFBSSxHQUFHQyxNQUFNLENBQUNELElBQVAsQ0FBWUQsVUFBWixDQUFYLEVBQW9DRyxDQUFDLEdBQUcsQ0FBN0MsRUFBZ0RBLENBQUMsR0FBR0YsSUFBSSxDQUFDRyxNQUF6RCxFQUFpRSxFQUFFRCxDQUFuRTtBQUNJLFVBQUlILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBVixJQUF1QixJQUEzQixFQUNJLEtBQUtGLElBQUksQ0FBQ0UsQ0FBRCxDQUFULElBQWdCSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQTFCO0FBRlI7QUFHUDtBQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJeUYsRUFBQUEsdUJBQXVCLENBQUNwRixNQUF4QixHQUFpQyxTQUFTQSxNQUFULENBQWdCUixVQUFoQixFQUE0QjtBQUN6RCxXQUFPLElBQUk0Rix1QkFBSixDQUE0QjVGLFVBQTVCLENBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTRGLEVBQUFBLHVCQUF1QixDQUFDbkYsTUFBeEIsR0FBaUMsU0FBU0EsTUFBVCxDQUFnQkMsT0FBaEIsRUFBeUJDLE1BQXpCLEVBQWlDO0FBQzlELFFBQUksQ0FBQ0EsTUFBTCxFQUNJQSxNQUFNLEdBQUdsQixPQUFPLENBQUNlLE1BQVIsRUFBVDtBQUNKLFdBQU9HLE1BQVA7QUFDSCxHQUpEO0FBTUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSWlGLEVBQUFBLHVCQUF1QixDQUFDM0UsZUFBeEIsR0FBMEMsU0FBU0EsZUFBVCxDQUF5QlAsT0FBekIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ2hGLFdBQU8sS0FBS0YsTUFBTCxDQUFZQyxPQUFaLEVBQXFCQyxNQUFyQixFQUE2Qk8sTUFBN0IsRUFBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTBFLEVBQUFBLHVCQUF1QixDQUFDekUsTUFBeEIsR0FBaUMsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0JoQixNQUF4QixFQUFnQztBQUM3RCxRQUFJLEVBQUVnQixNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUc3QixPQUFPLENBQUNpQixNQUFSLENBQWVZLE1BQWYsQ0FBVDtBQUNKLFFBQUlDLEdBQUcsR0FBR2pCLE1BQU0sS0FBS2tCLFNBQVgsR0FBdUJGLE1BQU0sQ0FBQ0csR0FBOUIsR0FBb0NILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhcEIsTUFBM0Q7QUFBQSxRQUFtRU0sT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQytGLHVCQUFWLEVBQTdFOztBQUNBLFdBQU94RSxNQUFNLENBQUNJLEdBQVAsR0FBYUgsR0FBcEIsRUFBeUI7QUFDckIsVUFBSUksR0FBRyxHQUFHTCxNQUFNLENBQUNOLE1BQVAsRUFBVjs7QUFDQSxjQUFRVyxHQUFHLEtBQUssQ0FBaEI7QUFDQTtBQUNJTCxVQUFBQSxNQUFNLENBQUNNLFFBQVAsQ0FBZ0JELEdBQUcsR0FBRyxDQUF0QjtBQUNBO0FBSEo7QUFLSDs7QUFDRCxXQUFPZixPQUFQO0FBQ0gsR0FiRDtBQWVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSWtGLEVBQUFBLHVCQUF1QixDQUFDakUsZUFBeEIsR0FBMEMsU0FBU0EsZUFBVCxDQUF5QlAsTUFBekIsRUFBaUM7QUFDdkUsUUFBSSxFQUFFQSxNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUcsSUFBSTdCLE9BQUosQ0FBWTZCLE1BQVosQ0FBVDtBQUNKLFdBQU8sS0FBS0QsTUFBTCxDQUFZQyxNQUFaLEVBQW9CQSxNQUFNLENBQUNOLE1BQVAsRUFBcEIsQ0FBUDtBQUNILEdBSkQ7QUFNQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSThFLEVBQUFBLHVCQUF1QixDQUFDaEUsTUFBeEIsR0FBaUMsU0FBU0EsTUFBVCxDQUFnQmxCLE9BQWhCLEVBQXlCO0FBQ3RELFFBQUksT0FBT0EsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBTyxLQUFLLElBQS9DLEVBQ0ksT0FBTyxpQkFBUDtBQUNKLFdBQU8sSUFBUDtBQUNILEdBSkQ7QUFNQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSWtGLEVBQUFBLHVCQUF1QixDQUFDN0QsVUFBeEIsR0FBcUMsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDN0QsUUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDK0YsdUJBQTVCLEVBQ0ksT0FBTzVELE1BQVA7QUFDSixXQUFPLElBQUluQyxLQUFLLENBQUMrRix1QkFBVixFQUFQO0FBQ0gsR0FKRDtBQU1BO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lBLEVBQUFBLHVCQUF1QixDQUFDMUQsUUFBeEIsR0FBbUMsU0FBU0EsUUFBVCxHQUFvQjtBQUNuRCxXQUFPLEVBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJMEQsRUFBQUEsdUJBQXVCLENBQUN2RixTQUF4QixDQUFrQ2dDLE1BQWxDLEdBQTJDLFNBQVNBLE1BQVQsR0FBa0I7QUFDekQsV0FBTyxLQUFLQyxXQUFMLENBQWlCSixRQUFqQixDQUEwQixJQUExQixFQUFnQzdDLFNBQVMsQ0FBQ08sSUFBVixDQUFlMkMsYUFBL0MsQ0FBUDtBQUNILEdBRkQ7O0FBSUEsU0FBT3FELHVCQUFQO0FBQ0gsQ0E5SitCLEVBQWhDOztBQWdLQS9GLEtBQUssQ0FBQ2dHLHFCQUFOLEdBQStCLFlBQVc7QUFFdEM7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSSxXQUFTQSxxQkFBVCxDQUErQjdGLFVBQS9CLEVBQTJDO0FBQ3ZDLFNBQUsrSCxhQUFMLEdBQXFCLEVBQXJCO0FBQ0EsUUFBSS9ILFVBQUosRUFDSSxLQUFLLElBQUlDLElBQUksR0FBR0MsTUFBTSxDQUFDRCxJQUFQLENBQVlELFVBQVosQ0FBWCxFQUFvQ0csQ0FBQyxHQUFHLENBQTdDLEVBQWdEQSxDQUFDLEdBQUdGLElBQUksQ0FBQ0csTUFBekQsRUFBaUUsRUFBRUQsQ0FBbkU7QUFDSSxVQUFJSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQVYsSUFBdUIsSUFBM0IsRUFDSSxLQUFLRixJQUFJLENBQUNFLENBQUQsQ0FBVCxJQUFnQkgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUExQjtBQUZSO0FBR1A7QUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJMEYsRUFBQUEscUJBQXFCLENBQUN4RixTQUF0QixDQUFnQzBILGFBQWhDLEdBQWdEcEksS0FBSyxDQUFDMkcsVUFBdEQ7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJVCxFQUFBQSxxQkFBcUIsQ0FBQ3JGLE1BQXRCLEdBQStCLFNBQVNBLE1BQVQsQ0FBZ0JSLFVBQWhCLEVBQTRCO0FBQ3ZELFdBQU8sSUFBSTZGLHFCQUFKLENBQTBCN0YsVUFBMUIsQ0FBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJNkYsRUFBQUEscUJBQXFCLENBQUNwRixNQUF0QixHQUErQixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDNUQsUUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUOztBQUNKLFFBQUlFLE9BQU8sQ0FBQ3FILGFBQVIsSUFBeUIsSUFBekIsSUFBaUNySCxPQUFPLENBQUNxSCxhQUFSLENBQXNCM0gsTUFBM0QsRUFBbUU7QUFDL0RPLE1BQUFBLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLFFBQXJDLEVBQXlDd0MsSUFBekM7O0FBQ0EsV0FBSyxJQUFJbkQsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR08sT0FBTyxDQUFDcUgsYUFBUixDQUFzQjNILE1BQTFDLEVBQWtELEVBQUVELENBQXBEO0FBQ0lRLFFBQUFBLE1BQU0sQ0FBQ0ksS0FBUCxDQUFhTCxPQUFPLENBQUNxSCxhQUFSLENBQXNCNUgsQ0FBdEIsQ0FBYjtBQURKOztBQUVBUSxNQUFBQSxNQUFNLENBQUNPLE1BQVA7QUFDSDs7QUFDRCxXQUFPUCxNQUFQO0FBQ0gsR0FWRDtBQVlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lrRixFQUFBQSxxQkFBcUIsQ0FBQzVFLGVBQXRCLEdBQXdDLFNBQVNBLGVBQVQsQ0FBeUJQLE9BQXpCLEVBQWtDQyxNQUFsQyxFQUEwQztBQUM5RSxXQUFPLEtBQUtGLE1BQUwsQ0FBWUMsT0FBWixFQUFxQkMsTUFBckIsRUFBNkJPLE1BQTdCLEVBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0kyRSxFQUFBQSxxQkFBcUIsQ0FBQzFFLE1BQXRCLEdBQStCLFNBQVNBLE1BQVQsQ0FBZ0JDLE1BQWhCLEVBQXdCaEIsTUFBeEIsRUFBZ0M7QUFDM0QsUUFBSSxFQUFFZ0IsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHN0IsT0FBTyxDQUFDaUIsTUFBUixDQUFlWSxNQUFmLENBQVQ7QUFDSixRQUFJQyxHQUFHLEdBQUdqQixNQUFNLEtBQUtrQixTQUFYLEdBQXVCRixNQUFNLENBQUNHLEdBQTlCLEdBQW9DSCxNQUFNLENBQUNJLEdBQVAsR0FBYXBCLE1BQTNEO0FBQUEsUUFBbUVNLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUNnRyxxQkFBVixFQUE3RTs7QUFDQSxXQUFPekUsTUFBTSxDQUFDSSxHQUFQLEdBQWFILEdBQXBCLEVBQXlCO0FBQ3JCLFVBQUlJLEdBQUcsR0FBR0wsTUFBTSxDQUFDTixNQUFQLEVBQVY7O0FBQ0EsY0FBUVcsR0FBRyxLQUFLLENBQWhCO0FBQ0EsYUFBSyxDQUFMO0FBQ0ksY0FBSSxFQUFFZixPQUFPLENBQUNxSCxhQUFSLElBQXlCckgsT0FBTyxDQUFDcUgsYUFBUixDQUFzQjNILE1BQWpELENBQUosRUFDSU0sT0FBTyxDQUFDcUgsYUFBUixHQUF3QixFQUF4Qjs7QUFDSixjQUFJLENBQUN0RyxHQUFHLEdBQUcsQ0FBUCxNQUFjLENBQWxCLEVBQXFCO0FBQ2pCLGdCQUFJdUcsSUFBSSxHQUFHNUcsTUFBTSxDQUFDTixNQUFQLEtBQWtCTSxNQUFNLENBQUNJLEdBQXBDOztBQUNBLG1CQUFPSixNQUFNLENBQUNJLEdBQVAsR0FBYXdHLElBQXBCO0FBQ0l0SCxjQUFBQSxPQUFPLENBQUNxSCxhQUFSLENBQXNCckIsSUFBdEIsQ0FBMkJ0RixNQUFNLENBQUNMLEtBQVAsRUFBM0I7QUFESjtBQUVILFdBSkQsTUFLSUwsT0FBTyxDQUFDcUgsYUFBUixDQUFzQnJCLElBQXRCLENBQTJCdEYsTUFBTSxDQUFDTCxLQUFQLEVBQTNCOztBQUNKOztBQUNKO0FBQ0lLLFVBQUFBLE1BQU0sQ0FBQ00sUUFBUCxDQUFnQkQsR0FBRyxHQUFHLENBQXRCO0FBQ0E7QUFiSjtBQWVIOztBQUNELFdBQU9mLE9BQVA7QUFDSCxHQXZCRDtBQXlCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0ltRixFQUFBQSxxQkFBcUIsQ0FBQ2xFLGVBQXRCLEdBQXdDLFNBQVNBLGVBQVQsQ0FBeUJQLE1BQXpCLEVBQWlDO0FBQ3JFLFFBQUksRUFBRUEsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHLElBQUk3QixPQUFKLENBQVk2QixNQUFaLENBQVQ7QUFDSixXQUFPLEtBQUtELE1BQUwsQ0FBWUMsTUFBWixFQUFvQkEsTUFBTSxDQUFDTixNQUFQLEVBQXBCLENBQVA7QUFDSCxHQUpEO0FBTUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0krRSxFQUFBQSxxQkFBcUIsQ0FBQ2pFLE1BQXRCLEdBQStCLFNBQVNBLE1BQVQsQ0FBZ0JsQixPQUFoQixFQUF5QjtBQUNwRCxRQUFJLE9BQU9BLE9BQVAsS0FBbUIsUUFBbkIsSUFBK0JBLE9BQU8sS0FBSyxJQUEvQyxFQUNJLE9BQU8saUJBQVA7O0FBQ0osUUFBSUEsT0FBTyxDQUFDcUgsYUFBUixJQUF5QixJQUF6QixJQUFpQ3JILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixlQUF2QixDQUFyQyxFQUE4RTtBQUMxRSxVQUFJLENBQUM4QyxLQUFLLENBQUNtRCxPQUFOLENBQWNuRyxPQUFPLENBQUNxSCxhQUF0QixDQUFMLEVBQ0ksT0FBTywrQkFBUDs7QUFDSixXQUFLLElBQUk1SCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHTyxPQUFPLENBQUNxSCxhQUFSLENBQXNCM0gsTUFBMUMsRUFBa0QsRUFBRUQsQ0FBcEQ7QUFDSSxZQUFJLENBQUNSLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUNxSCxhQUFSLENBQXNCNUgsQ0FBdEIsQ0FBaEIsQ0FBTCxFQUNJLE9BQU8sbUNBQVA7QUFGUjtBQUdIOztBQUNELFdBQU8sSUFBUDtBQUNILEdBWEQ7QUFhQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTBGLEVBQUFBLHFCQUFxQixDQUFDOUQsVUFBdEIsR0FBbUMsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDM0QsUUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDZ0cscUJBQTVCLEVBQ0ksT0FBTzdELE1BQVA7QUFDSixRQUFJdEIsT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQ2dHLHFCQUFWLEVBQWQ7O0FBQ0EsUUFBSTdELE1BQU0sQ0FBQytGLGFBQVgsRUFBMEI7QUFDdEIsVUFBSSxDQUFDckUsS0FBSyxDQUFDbUQsT0FBTixDQUFjN0UsTUFBTSxDQUFDK0YsYUFBckIsQ0FBTCxFQUNJLE1BQU12RSxTQUFTLENBQUMsc0RBQUQsQ0FBZjtBQUNKOUMsTUFBQUEsT0FBTyxDQUFDcUgsYUFBUixHQUF3QixFQUF4Qjs7QUFDQSxXQUFLLElBQUk1SCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHNkIsTUFBTSxDQUFDK0YsYUFBUCxDQUFxQjNILE1BQXpDLEVBQWlELEVBQUVELENBQW5EO0FBQ0lPLFFBQUFBLE9BQU8sQ0FBQ3FILGFBQVIsQ0FBc0I1SCxDQUF0QixJQUEyQjZCLE1BQU0sQ0FBQytGLGFBQVAsQ0FBcUI1SCxDQUFyQixJQUEwQixDQUFyRDtBQURKO0FBRUg7O0FBQ0QsV0FBT08sT0FBUDtBQUNILEdBWkQ7QUFjQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJbUYsRUFBQUEscUJBQXFCLENBQUMzRCxRQUF0QixHQUFpQyxTQUFTQSxRQUFULENBQWtCeEIsT0FBbEIsRUFBMkJ5QixPQUEzQixFQUFvQztBQUNqRSxRQUFJLENBQUNBLE9BQUwsRUFDSUEsT0FBTyxHQUFHLEVBQVY7QUFDSixRQUFJSCxNQUFNLEdBQUcsRUFBYjtBQUNBLFFBQUlHLE9BQU8sQ0FBQ2dGLE1BQVIsSUFBa0JoRixPQUFPLENBQUNDLFFBQTlCLEVBQ0lKLE1BQU0sQ0FBQytGLGFBQVAsR0FBdUIsRUFBdkI7O0FBQ0osUUFBSXJILE9BQU8sQ0FBQ3FILGFBQVIsSUFBeUJySCxPQUFPLENBQUNxSCxhQUFSLENBQXNCM0gsTUFBbkQsRUFBMkQ7QUFDdkQ0QixNQUFBQSxNQUFNLENBQUMrRixhQUFQLEdBQXVCLEVBQXZCOztBQUNBLFdBQUssSUFBSVAsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRzlHLE9BQU8sQ0FBQ3FILGFBQVIsQ0FBc0IzSCxNQUExQyxFQUFrRCxFQUFFb0gsQ0FBcEQ7QUFDSXhGLFFBQUFBLE1BQU0sQ0FBQytGLGFBQVAsQ0FBcUJQLENBQXJCLElBQTBCOUcsT0FBTyxDQUFDcUgsYUFBUixDQUFzQlAsQ0FBdEIsQ0FBMUI7QUFESjtBQUVIOztBQUNELFdBQU94RixNQUFQO0FBQ0gsR0FaRDtBQWNBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTZELEVBQUFBLHFCQUFxQixDQUFDeEYsU0FBdEIsQ0FBZ0NnQyxNQUFoQyxHQUF5QyxTQUFTQSxNQUFULEdBQWtCO0FBQ3ZELFdBQU8sS0FBS0MsV0FBTCxDQUFpQkosUUFBakIsQ0FBMEIsSUFBMUIsRUFBZ0M3QyxTQUFTLENBQUNPLElBQVYsQ0FBZTJDLGFBQS9DLENBQVA7QUFDSCxHQUZEOztBQUlBLFNBQU9zRCxxQkFBUDtBQUNILENBak42QixFQUE5Qjs7QUFtTkFoRyxLQUFLLENBQUNvSSxZQUFOLEdBQXNCLFlBQVc7QUFFN0I7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNJLFdBQVNBLFlBQVQsQ0FBc0JqSSxVQUF0QixFQUFrQztBQUM5QixRQUFJQSxVQUFKLEVBQ0ksS0FBSyxJQUFJQyxJQUFJLEdBQUdDLE1BQU0sQ0FBQ0QsSUFBUCxDQUFZRCxVQUFaLENBQVgsRUFBb0NHLENBQUMsR0FBRyxDQUE3QyxFQUFnREEsQ0FBQyxHQUFHRixJQUFJLENBQUNHLE1BQXpELEVBQWlFLEVBQUVELENBQW5FO0FBQ0ksVUFBSUgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUFWLElBQXVCLElBQTNCLEVBQ0ksS0FBS0YsSUFBSSxDQUFDRSxDQUFELENBQVQsSUFBZ0JILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBMUI7QUFGUjtBQUdQO0FBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSThILEVBQUFBLFlBQVksQ0FBQzVILFNBQWIsQ0FBdUI2SCxLQUF2QixHQUErQnZJLEtBQUssQ0FBQ3dHLElBQU4sR0FBYXhHLEtBQUssQ0FBQ3dHLElBQU4sQ0FBV0MsUUFBWCxDQUFvQixDQUFwQixFQUFzQixDQUF0QixFQUF3QixLQUF4QixDQUFiLEdBQThDLENBQTdFO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJNkIsRUFBQUEsWUFBWSxDQUFDNUgsU0FBYixDQUF1QjhILEtBQXZCLEdBQStCLENBQS9CO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSUYsRUFBQUEsWUFBWSxDQUFDekgsTUFBYixHQUFzQixTQUFTQSxNQUFULENBQWdCUixVQUFoQixFQUE0QjtBQUM5QyxXQUFPLElBQUlpSSxZQUFKLENBQWlCakksVUFBakIsQ0FBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJaUksRUFBQUEsWUFBWSxDQUFDeEgsTUFBYixHQUFzQixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDbkQsUUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUO0FBQ0osUUFBSUUsT0FBTyxDQUFDd0gsS0FBUixJQUFpQixJQUFqQixJQUF5QmhJLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLE9BQXBDLENBQTdCLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLEtBQXJDLEVBQXdDc0gsS0FBeEMsQ0FBOEMxSCxPQUFPLENBQUN3SCxLQUF0RDtBQUNKLFFBQUl4SCxPQUFPLENBQUN5SCxLQUFSLElBQWlCLElBQWpCLElBQXlCakksTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsT0FBcEMsQ0FBN0IsRUFDSUMsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsTUFBckMsRUFBeUN1SCxRQUF6QyxDQUFrRDNILE9BQU8sQ0FBQ3lILEtBQTFEO0FBQ0osV0FBT3hILE1BQVA7QUFDSCxHQVJEO0FBVUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXNILEVBQUFBLFlBQVksQ0FBQ2hILGVBQWIsR0FBK0IsU0FBU0EsZUFBVCxDQUF5QlAsT0FBekIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ3JFLFdBQU8sS0FBS0YsTUFBTCxDQUFZQyxPQUFaLEVBQXFCQyxNQUFyQixFQUE2Qk8sTUFBN0IsRUFBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSStHLEVBQUFBLFlBQVksQ0FBQzlHLE1BQWIsR0FBc0IsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0JoQixNQUF4QixFQUFnQztBQUNsRCxRQUFJLEVBQUVnQixNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUc3QixPQUFPLENBQUNpQixNQUFSLENBQWVZLE1BQWYsQ0FBVDtBQUNKLFFBQUlDLEdBQUcsR0FBR2pCLE1BQU0sS0FBS2tCLFNBQVgsR0FBdUJGLE1BQU0sQ0FBQ0csR0FBOUIsR0FBb0NILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhcEIsTUFBM0Q7QUFBQSxRQUFtRU0sT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQ29JLFlBQVYsRUFBN0U7O0FBQ0EsV0FBTzdHLE1BQU0sQ0FBQ0ksR0FBUCxHQUFhSCxHQUFwQixFQUF5QjtBQUNyQixVQUFJSSxHQUFHLEdBQUdMLE1BQU0sQ0FBQ04sTUFBUCxFQUFWOztBQUNBLGNBQVFXLEdBQUcsS0FBSyxDQUFoQjtBQUNBLGFBQUssQ0FBTDtBQUNJZixVQUFBQSxPQUFPLENBQUN3SCxLQUFSLEdBQWdCOUcsTUFBTSxDQUFDZ0gsS0FBUCxFQUFoQjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJMUgsVUFBQUEsT0FBTyxDQUFDeUgsS0FBUixHQUFnQi9HLE1BQU0sQ0FBQ2lILFFBQVAsRUFBaEI7QUFDQTs7QUFDSjtBQUNJakgsVUFBQUEsTUFBTSxDQUFDTSxRQUFQLENBQWdCRCxHQUFHLEdBQUcsQ0FBdEI7QUFDQTtBQVRKO0FBV0g7O0FBQ0QsV0FBT2YsT0FBUDtBQUNILEdBbkJEO0FBcUJBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXVILEVBQUFBLFlBQVksQ0FBQ3RHLGVBQWIsR0FBK0IsU0FBU0EsZUFBVCxDQUF5QlAsTUFBekIsRUFBaUM7QUFDNUQsUUFBSSxFQUFFQSxNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUcsSUFBSTdCLE9BQUosQ0FBWTZCLE1BQVosQ0FBVDtBQUNKLFdBQU8sS0FBS0QsTUFBTCxDQUFZQyxNQUFaLEVBQW9CQSxNQUFNLENBQUNOLE1BQVAsRUFBcEIsQ0FBUDtBQUNILEdBSkQ7QUFNQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSW1ILEVBQUFBLFlBQVksQ0FBQ3JHLE1BQWIsR0FBc0IsU0FBU0EsTUFBVCxDQUFnQmxCLE9BQWhCLEVBQXlCO0FBQzNDLFFBQUksT0FBT0EsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBTyxLQUFLLElBQS9DLEVBQ0ksT0FBTyxpQkFBUDtBQUNKLFFBQUlBLE9BQU8sQ0FBQ3dILEtBQVIsSUFBaUIsSUFBakIsSUFBeUJ4SCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDd0gsS0FBeEIsQ0FBRCxJQUFtQyxFQUFFeEgsT0FBTyxDQUFDd0gsS0FBUixJQUFpQnZJLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUN3SCxLQUFSLENBQWN2QixHQUE5QixDQUFqQixJQUF1RGhILEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUN3SCxLQUFSLENBQWN0QixJQUE5QixDQUF6RCxDQUF2QyxFQUNJLE9BQU8sOEJBQVA7QUFDUixRQUFJbEcsT0FBTyxDQUFDeUgsS0FBUixJQUFpQixJQUFqQixJQUF5QnpILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixPQUF2QixDQUE3QixFQUNJLElBQUksQ0FBQ2pCLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUN5SCxLQUF4QixDQUFMLEVBQ0ksT0FBTyx5QkFBUDtBQUNSLFdBQU8sSUFBUDtBQUNILEdBVkQ7QUFZQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSUYsRUFBQUEsWUFBWSxDQUFDbEcsVUFBYixHQUEwQixTQUFTQSxVQUFULENBQW9CQyxNQUFwQixFQUE0QjtBQUNsRCxRQUFJQSxNQUFNLFlBQVluQyxLQUFLLENBQUNvSSxZQUE1QixFQUNJLE9BQU9qRyxNQUFQO0FBQ0osUUFBSXRCLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUNvSSxZQUFWLEVBQWQ7QUFDQSxRQUFJakcsTUFBTSxDQUFDa0csS0FBUCxJQUFnQixJQUFwQixFQUNJLElBQUl2SSxLQUFLLENBQUN3RyxJQUFWLEVBQ0ksQ0FBQ3pGLE9BQU8sQ0FBQ3dILEtBQVIsR0FBZ0J2SSxLQUFLLENBQUN3RyxJQUFOLENBQVdXLFNBQVgsQ0FBcUI5RSxNQUFNLENBQUNrRyxLQUE1QixDQUFqQixFQUFxRG5CLFFBQXJELEdBQWdFLEtBQWhFLENBREosS0FFSyxJQUFJLE9BQU8vRSxNQUFNLENBQUNrRyxLQUFkLEtBQXdCLFFBQTVCLEVBQ0R4SCxPQUFPLENBQUN3SCxLQUFSLEdBQWdCbEIsUUFBUSxDQUFDaEYsTUFBTSxDQUFDa0csS0FBUixFQUFlLEVBQWYsQ0FBeEIsQ0FEQyxLQUVBLElBQUksT0FBT2xHLE1BQU0sQ0FBQ2tHLEtBQWQsS0FBd0IsUUFBNUIsRUFDRHhILE9BQU8sQ0FBQ3dILEtBQVIsR0FBZ0JsRyxNQUFNLENBQUNrRyxLQUF2QixDQURDLEtBRUEsSUFBSSxPQUFPbEcsTUFBTSxDQUFDa0csS0FBZCxLQUF3QixRQUE1QixFQUNEeEgsT0FBTyxDQUFDd0gsS0FBUixHQUFnQixJQUFJdkksS0FBSyxDQUFDc0gsUUFBVixDQUFtQmpGLE1BQU0sQ0FBQ2tHLEtBQVAsQ0FBYXZCLEdBQWIsS0FBcUIsQ0FBeEMsRUFBMkMzRSxNQUFNLENBQUNrRyxLQUFQLENBQWF0QixJQUFiLEtBQXNCLENBQWpFLEVBQW9FTSxRQUFwRSxFQUFoQjtBQUNSLFFBQUlsRixNQUFNLENBQUNtRyxLQUFQLElBQWdCLElBQXBCLEVBQ0l6SCxPQUFPLENBQUN5SCxLQUFSLEdBQWdCbkcsTUFBTSxDQUFDbUcsS0FBUCxHQUFlLENBQS9CO0FBQ0osV0FBT3pILE9BQVA7QUFDSCxHQWhCRDtBQWtCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJdUgsRUFBQUEsWUFBWSxDQUFDL0YsUUFBYixHQUF3QixTQUFTQSxRQUFULENBQWtCeEIsT0FBbEIsRUFBMkJ5QixPQUEzQixFQUFvQztBQUN4RCxRQUFJLENBQUNBLE9BQUwsRUFDSUEsT0FBTyxHQUFHLEVBQVY7QUFDSixRQUFJSCxNQUFNLEdBQUcsRUFBYjs7QUFDQSxRQUFJRyxPQUFPLENBQUNDLFFBQVosRUFBc0I7QUFDbEIsVUFBSXpDLEtBQUssQ0FBQ3dHLElBQVYsRUFBZ0I7QUFDWixZQUFJaUIsTUFBSSxHQUFHLElBQUl6SCxLQUFLLENBQUN3RyxJQUFWLENBQWUsQ0FBZixFQUFrQixDQUFsQixFQUFxQixLQUFyQixDQUFYOztBQUNBbkUsUUFBQUEsTUFBTSxDQUFDa0csS0FBUCxHQUFlL0YsT0FBTyxDQUFDa0YsS0FBUixLQUFrQnBGLE1BQWxCLEdBQTJCbUYsTUFBSSxDQUFDRSxRQUFMLEVBQTNCLEdBQTZDbkYsT0FBTyxDQUFDa0YsS0FBUixLQUFrQkUsTUFBbEIsR0FBMkJILE1BQUksQ0FBQ0YsUUFBTCxFQUEzQixHQUE2Q0UsTUFBekc7QUFDSCxPQUhELE1BSUlwRixNQUFNLENBQUNrRyxLQUFQLEdBQWUvRixPQUFPLENBQUNrRixLQUFSLEtBQWtCcEYsTUFBbEIsR0FBMkIsR0FBM0IsR0FBaUMsQ0FBaEQ7O0FBQ0pELE1BQUFBLE1BQU0sQ0FBQ21HLEtBQVAsR0FBZSxDQUFmO0FBQ0g7O0FBQ0QsUUFBSXpILE9BQU8sQ0FBQ3dILEtBQVIsSUFBaUIsSUFBakIsSUFBeUJ4SCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFDSSxJQUFJLE9BQU9GLE9BQU8sQ0FBQ3dILEtBQWYsS0FBeUIsUUFBN0IsRUFDSWxHLE1BQU0sQ0FBQ2tHLEtBQVAsR0FBZS9GLE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JwRixNQUFsQixHQUEyQkEsTUFBTSxDQUFDdkIsT0FBTyxDQUFDd0gsS0FBVCxDQUFqQyxHQUFtRHhILE9BQU8sQ0FBQ3dILEtBQTFFLENBREosS0FHSWxHLE1BQU0sQ0FBQ2tHLEtBQVAsR0FBZS9GLE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JwRixNQUFsQixHQUEyQnRDLEtBQUssQ0FBQ3dHLElBQU4sQ0FBVzlGLFNBQVgsQ0FBcUJpSCxRQUFyQixDQUE4QnpHLElBQTlCLENBQW1DSCxPQUFPLENBQUN3SCxLQUEzQyxDQUEzQixHQUErRS9GLE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JFLE1BQWxCLEdBQTJCLElBQUk1SCxLQUFLLENBQUNzSCxRQUFWLENBQW1CdkcsT0FBTyxDQUFDd0gsS0FBUixDQUFjdkIsR0FBZCxLQUFzQixDQUF6QyxFQUE0Q2pHLE9BQU8sQ0FBQ3dILEtBQVIsQ0FBY3RCLElBQWQsS0FBdUIsQ0FBbkUsRUFBc0VNLFFBQXRFLEVBQTNCLEdBQThHeEcsT0FBTyxDQUFDd0gsS0FBcE47QUFDUixRQUFJeEgsT0FBTyxDQUFDeUgsS0FBUixJQUFpQixJQUFqQixJQUF5QnpILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixPQUF2QixDQUE3QixFQUNJb0IsTUFBTSxDQUFDbUcsS0FBUCxHQUFlekgsT0FBTyxDQUFDeUgsS0FBdkI7QUFDSixXQUFPbkcsTUFBUDtBQUNILEdBcEJEO0FBc0JBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSWlHLEVBQUFBLFlBQVksQ0FBQzVILFNBQWIsQ0FBdUJnQyxNQUF2QixHQUFnQyxTQUFTQSxNQUFULEdBQWtCO0FBQzlDLFdBQU8sS0FBS0MsV0FBTCxDQUFpQkosUUFBakIsQ0FBMEIsSUFBMUIsRUFBZ0M3QyxTQUFTLENBQUNPLElBQVYsQ0FBZTJDLGFBQS9DLENBQVA7QUFDSCxHQUZEOztBQUlBLFNBQU8wRixZQUFQO0FBQ0gsQ0E5Tm9CLEVBQXJCOztBQWdPQXBJLEtBQUssQ0FBQzBHLFFBQU4sR0FBa0IsWUFBVztBQUV6QjtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNJLFdBQVNBLFFBQVQsQ0FBa0J2RyxVQUFsQixFQUE4QjtBQUMxQixRQUFJQSxVQUFKLEVBQ0ksS0FBSyxJQUFJQyxJQUFJLEdBQUdDLE1BQU0sQ0FBQ0QsSUFBUCxDQUFZRCxVQUFaLENBQVgsRUFBb0NHLENBQUMsR0FBRyxDQUE3QyxFQUFnREEsQ0FBQyxHQUFHRixJQUFJLENBQUNHLE1BQXpELEVBQWlFLEVBQUVELENBQW5FO0FBQ0ksVUFBSUgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUFWLElBQXVCLElBQTNCLEVBQ0ksS0FBS0YsSUFBSSxDQUFDRSxDQUFELENBQVQsSUFBZ0JILFVBQVUsQ0FBQ0MsSUFBSSxDQUFDRSxDQUFELENBQUwsQ0FBMUI7QUFGUjtBQUdQO0FBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSW9HLEVBQUFBLFFBQVEsQ0FBQ2xHLFNBQVQsQ0FBbUJpSSxPQUFuQixHQUE2QixFQUE3QjtBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSS9CLEVBQUFBLFFBQVEsQ0FBQ2xHLFNBQVQsQ0FBbUJrSSxRQUFuQixHQUE4QixFQUE5QjtBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSWhDLEVBQUFBLFFBQVEsQ0FBQ2xHLFNBQVQsQ0FBbUJtSSxTQUFuQixHQUErQixFQUEvQjtBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSWpDLEVBQUFBLFFBQVEsQ0FBQ2xHLFNBQVQsQ0FBbUJvSSxLQUFuQixHQUEyQixJQUEzQjtBQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lsQyxFQUFBQSxRQUFRLENBQUMvRixNQUFULEdBQWtCLFNBQVNBLE1BQVQsQ0FBZ0JSLFVBQWhCLEVBQTRCO0FBQzFDLFdBQU8sSUFBSXVHLFFBQUosQ0FBYXZHLFVBQWIsQ0FBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJdUcsRUFBQUEsUUFBUSxDQUFDOUYsTUFBVCxHQUFrQixTQUFTQSxNQUFULENBQWdCQyxPQUFoQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDL0MsUUFBSSxDQUFDQSxNQUFMLEVBQ0lBLE1BQU0sR0FBR2xCLE9BQU8sQ0FBQ2UsTUFBUixFQUFUO0FBQ0osUUFBSUUsT0FBTyxDQUFDNEgsT0FBUixJQUFtQixJQUFuQixJQUEyQnBJLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFNBQXBDLENBQS9CLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDRSxNQUF6QyxDQUFnRE4sT0FBTyxDQUFDNEgsT0FBeEQ7QUFDSixRQUFJNUgsT0FBTyxDQUFDNkgsUUFBUixJQUFvQixJQUFwQixJQUE0QnJJLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFVBQXBDLENBQWhDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDRSxNQUF6QyxDQUFnRE4sT0FBTyxDQUFDNkgsUUFBeEQ7QUFDSixRQUFJN0gsT0FBTyxDQUFDOEgsU0FBUixJQUFxQixJQUFyQixJQUE2QnRJLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFdBQXBDLENBQWpDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDRSxNQUF6QyxDQUFnRE4sT0FBTyxDQUFDOEgsU0FBeEQ7QUFDSixRQUFJOUgsT0FBTyxDQUFDK0gsS0FBUixJQUFpQixJQUFqQixJQUF5QnZJLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLE9BQXBDLENBQTdCLEVBQ0liLEtBQUssQ0FBQ29JLFlBQU4sQ0FBbUJ4SCxNQUFuQixDQUEwQkMsT0FBTyxDQUFDK0gsS0FBbEMsRUFBeUM5SCxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixNQUFyQyxFQUF5Q3dDLElBQXpDLEVBQXpDLEVBQTBGcEMsTUFBMUY7QUFDSixXQUFPUCxNQUFQO0FBQ0gsR0FaRDtBQWNBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0k0RixFQUFBQSxRQUFRLENBQUN0RixlQUFULEdBQTJCLFNBQVNBLGVBQVQsQ0FBeUJQLE9BQXpCLEVBQWtDQyxNQUFsQyxFQUEwQztBQUNqRSxXQUFPLEtBQUtGLE1BQUwsQ0FBWUMsT0FBWixFQUFxQkMsTUFBckIsRUFBNkJPLE1BQTdCLEVBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lxRixFQUFBQSxRQUFRLENBQUNwRixNQUFULEdBQWtCLFNBQVNBLE1BQVQsQ0FBZ0JDLE1BQWhCLEVBQXdCaEIsTUFBeEIsRUFBZ0M7QUFDOUMsUUFBSSxFQUFFZ0IsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHN0IsT0FBTyxDQUFDaUIsTUFBUixDQUFlWSxNQUFmLENBQVQ7QUFDSixRQUFJQyxHQUFHLEdBQUdqQixNQUFNLEtBQUtrQixTQUFYLEdBQXVCRixNQUFNLENBQUNHLEdBQTlCLEdBQW9DSCxNQUFNLENBQUNJLEdBQVAsR0FBYXBCLE1BQTNEO0FBQUEsUUFBbUVNLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUMwRyxRQUFWLEVBQTdFOztBQUNBLFdBQU9uRixNQUFNLENBQUNJLEdBQVAsR0FBYUgsR0FBcEIsRUFBeUI7QUFDckIsVUFBSUksR0FBRyxHQUFHTCxNQUFNLENBQUNOLE1BQVAsRUFBVjs7QUFDQSxjQUFRVyxHQUFHLEtBQUssQ0FBaEI7QUFDQSxhQUFLLENBQUw7QUFDSWYsVUFBQUEsT0FBTyxDQUFDNEgsT0FBUixHQUFrQmxILE1BQU0sQ0FBQ0osTUFBUCxFQUFsQjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJTixVQUFBQSxPQUFPLENBQUM2SCxRQUFSLEdBQW1CbkgsTUFBTSxDQUFDSixNQUFQLEVBQW5CO0FBQ0E7O0FBQ0osYUFBSyxDQUFMO0FBQ0lOLFVBQUFBLE9BQU8sQ0FBQzhILFNBQVIsR0FBb0JwSCxNQUFNLENBQUNKLE1BQVAsRUFBcEI7QUFDQTs7QUFDSixhQUFLLENBQUw7QUFDSU4sVUFBQUEsT0FBTyxDQUFDK0gsS0FBUixHQUFnQjVJLEtBQUssQ0FBQ29JLFlBQU4sQ0FBbUI5RyxNQUFuQixDQUEwQkMsTUFBMUIsRUFBa0NBLE1BQU0sQ0FBQ04sTUFBUCxFQUFsQyxDQUFoQjtBQUNBOztBQUNKO0FBQ0lNLFVBQUFBLE1BQU0sQ0FBQ00sUUFBUCxDQUFnQkQsR0FBRyxHQUFHLENBQXRCO0FBQ0E7QUFmSjtBQWlCSDs7QUFDRCxXQUFPZixPQUFQO0FBQ0gsR0F6QkQ7QUEyQkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJNkYsRUFBQUEsUUFBUSxDQUFDNUUsZUFBVCxHQUEyQixTQUFTQSxlQUFULENBQXlCUCxNQUF6QixFQUFpQztBQUN4RCxRQUFJLEVBQUVBLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRyxJQUFJN0IsT0FBSixDQUFZNkIsTUFBWixDQUFUO0FBQ0osV0FBTyxLQUFLRCxNQUFMLENBQVlDLE1BQVosRUFBb0JBLE1BQU0sQ0FBQ04sTUFBUCxFQUFwQixDQUFQO0FBQ0gsR0FKRDtBQU1BO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJeUYsRUFBQUEsUUFBUSxDQUFDM0UsTUFBVCxHQUFrQixTQUFTQSxNQUFULENBQWdCbEIsT0FBaEIsRUFBeUI7QUFDdkMsUUFBSSxPQUFPQSxPQUFQLEtBQW1CLFFBQW5CLElBQStCQSxPQUFPLEtBQUssSUFBL0MsRUFDSSxPQUFPLGlCQUFQO0FBQ0osUUFBSUEsT0FBTyxDQUFDNEgsT0FBUixJQUFtQixJQUFuQixJQUEyQjVILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixTQUF2QixDQUEvQixFQUNJLElBQUksQ0FBQ2pCLEtBQUssQ0FBQ21DLFFBQU4sQ0FBZXBCLE9BQU8sQ0FBQzRILE9BQXZCLENBQUwsRUFDSSxPQUFPLDBCQUFQO0FBQ1IsUUFBSTVILE9BQU8sQ0FBQzZILFFBQVIsSUFBb0IsSUFBcEIsSUFBNEI3SCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsVUFBdkIsQ0FBaEMsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNtQyxRQUFOLENBQWVwQixPQUFPLENBQUM2SCxRQUF2QixDQUFMLEVBQ0ksT0FBTywyQkFBUDtBQUNSLFFBQUk3SCxPQUFPLENBQUM4SCxTQUFSLElBQXFCLElBQXJCLElBQTZCOUgsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFdBQXZCLENBQWpDLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDbUMsUUFBTixDQUFlcEIsT0FBTyxDQUFDOEgsU0FBdkIsQ0FBTCxFQUNJLE9BQU8sNEJBQVA7O0FBQ1IsUUFBSTlILE9BQU8sQ0FBQytILEtBQVIsSUFBaUIsSUFBakIsSUFBeUIvSCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFBOEQ7QUFDMUQsVUFBSThCLEtBQUssR0FBRzdDLEtBQUssQ0FBQ29JLFlBQU4sQ0FBbUJyRyxNQUFuQixDQUEwQmxCLE9BQU8sQ0FBQytILEtBQWxDLENBQVo7QUFDQSxVQUFJL0YsS0FBSixFQUNJLE9BQU8sV0FBV0EsS0FBbEI7QUFDUDs7QUFDRCxXQUFPLElBQVA7QUFDSCxHQWxCRDtBQW9CQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTZELEVBQUFBLFFBQVEsQ0FBQ3hFLFVBQVQsR0FBc0IsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDOUMsUUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDMEcsUUFBNUIsRUFDSSxPQUFPdkUsTUFBUDtBQUNKLFFBQUl0QixPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDMEcsUUFBVixFQUFkO0FBQ0EsUUFBSXZFLE1BQU0sQ0FBQ3NHLE9BQVAsSUFBa0IsSUFBdEIsRUFDSTVILE9BQU8sQ0FBQzRILE9BQVIsR0FBa0JyRyxNQUFNLENBQUNELE1BQU0sQ0FBQ3NHLE9BQVIsQ0FBeEI7QUFDSixRQUFJdEcsTUFBTSxDQUFDdUcsUUFBUCxJQUFtQixJQUF2QixFQUNJN0gsT0FBTyxDQUFDNkgsUUFBUixHQUFtQnRHLE1BQU0sQ0FBQ0QsTUFBTSxDQUFDdUcsUUFBUixDQUF6QjtBQUNKLFFBQUl2RyxNQUFNLENBQUN3RyxTQUFQLElBQW9CLElBQXhCLEVBQ0k5SCxPQUFPLENBQUM4SCxTQUFSLEdBQW9CdkcsTUFBTSxDQUFDRCxNQUFNLENBQUN3RyxTQUFSLENBQTFCOztBQUNKLFFBQUl4RyxNQUFNLENBQUN5RyxLQUFQLElBQWdCLElBQXBCLEVBQTBCO0FBQ3RCLFVBQUksT0FBT3pHLE1BQU0sQ0FBQ3lHLEtBQWQsS0FBd0IsUUFBNUIsRUFDSSxNQUFNakYsU0FBUyxDQUFDLGtDQUFELENBQWY7QUFDSjlDLE1BQUFBLE9BQU8sQ0FBQytILEtBQVIsR0FBZ0I1SSxLQUFLLENBQUNvSSxZQUFOLENBQW1CbEcsVUFBbkIsQ0FBOEJDLE1BQU0sQ0FBQ3lHLEtBQXJDLENBQWhCO0FBQ0g7O0FBQ0QsV0FBTy9ILE9BQVA7QUFDSCxHQWhCRDtBQWtCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJNkYsRUFBQUEsUUFBUSxDQUFDckUsUUFBVCxHQUFvQixTQUFTQSxRQUFULENBQWtCeEIsT0FBbEIsRUFBMkJ5QixPQUEzQixFQUFvQztBQUNwRCxRQUFJLENBQUNBLE9BQUwsRUFDSUEsT0FBTyxHQUFHLEVBQVY7QUFDSixRQUFJSCxNQUFNLEdBQUcsRUFBYjs7QUFDQSxRQUFJRyxPQUFPLENBQUNDLFFBQVosRUFBc0I7QUFDbEJKLE1BQUFBLE1BQU0sQ0FBQ3NHLE9BQVAsR0FBaUIsRUFBakI7QUFDQXRHLE1BQUFBLE1BQU0sQ0FBQ3VHLFFBQVAsR0FBa0IsRUFBbEI7QUFDQXZHLE1BQUFBLE1BQU0sQ0FBQ3dHLFNBQVAsR0FBbUIsRUFBbkI7QUFDQXhHLE1BQUFBLE1BQU0sQ0FBQ3lHLEtBQVAsR0FBZSxJQUFmO0FBQ0g7O0FBQ0QsUUFBSS9ILE9BQU8sQ0FBQzRILE9BQVIsSUFBbUIsSUFBbkIsSUFBMkI1SCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsU0FBdkIsQ0FBL0IsRUFDSW9CLE1BQU0sQ0FBQ3NHLE9BQVAsR0FBaUI1SCxPQUFPLENBQUM0SCxPQUF6QjtBQUNKLFFBQUk1SCxPQUFPLENBQUM2SCxRQUFSLElBQW9CLElBQXBCLElBQTRCN0gsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFVBQXZCLENBQWhDLEVBQ0lvQixNQUFNLENBQUN1RyxRQUFQLEdBQWtCN0gsT0FBTyxDQUFDNkgsUUFBMUI7QUFDSixRQUFJN0gsT0FBTyxDQUFDOEgsU0FBUixJQUFxQixJQUFyQixJQUE2QjlILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixXQUF2QixDQUFqQyxFQUNJb0IsTUFBTSxDQUFDd0csU0FBUCxHQUFtQjlILE9BQU8sQ0FBQzhILFNBQTNCO0FBQ0osUUFBSTlILE9BQU8sQ0FBQytILEtBQVIsSUFBaUIsSUFBakIsSUFBeUIvSCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFDSW9CLE1BQU0sQ0FBQ3lHLEtBQVAsR0FBZTVJLEtBQUssQ0FBQ29JLFlBQU4sQ0FBbUIvRixRQUFuQixDQUE0QnhCLE9BQU8sQ0FBQytILEtBQXBDLEVBQTJDdEcsT0FBM0MsQ0FBZjtBQUNKLFdBQU9ILE1BQVA7QUFDSCxHQW5CRDtBQXFCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l1RSxFQUFBQSxRQUFRLENBQUNsRyxTQUFULENBQW1CZ0MsTUFBbkIsR0FBNEIsU0FBU0EsTUFBVCxHQUFrQjtBQUMxQyxXQUFPLEtBQUtDLFdBQUwsQ0FBaUJKLFFBQWpCLENBQTBCLElBQTFCLEVBQWdDN0MsU0FBUyxDQUFDTyxJQUFWLENBQWUyQyxhQUEvQyxDQUFQO0FBQ0gsR0FGRDs7QUFJQSxTQUFPZ0UsUUFBUDtBQUNILENBalFnQixFQUFqQjs7QUFtUUExRyxLQUFLLENBQUM2SSxTQUFOLEdBQW1CLFlBQVc7QUFFMUI7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0ksV0FBU0EsU0FBVCxDQUFtQjFJLFVBQW5CLEVBQStCO0FBQzNCLFFBQUlBLFVBQUosRUFDSSxLQUFLLElBQUlDLElBQUksR0FBR0MsTUFBTSxDQUFDRCxJQUFQLENBQVlELFVBQVosQ0FBWCxFQUFvQ0csQ0FBQyxHQUFHLENBQTdDLEVBQWdEQSxDQUFDLEdBQUdGLElBQUksQ0FBQ0csTUFBekQsRUFBaUUsRUFBRUQsQ0FBbkU7QUFDSSxVQUFJSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQVYsSUFBdUIsSUFBM0IsRUFDSSxLQUFLRixJQUFJLENBQUNFLENBQUQsQ0FBVCxJQUFnQkgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUExQjtBQUZSO0FBR1A7QUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJdUksRUFBQUEsU0FBUyxDQUFDckksU0FBVixDQUFvQjZGLElBQXBCLEdBQTJCLENBQTNCO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJd0MsRUFBQUEsU0FBUyxDQUFDckksU0FBVixDQUFvQmdHLE9BQXBCLEdBQThCLENBQTlCO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJcUMsRUFBQUEsU0FBUyxDQUFDckksU0FBVixDQUFvQm9JLEtBQXBCLEdBQTRCLENBQTVCO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSUMsRUFBQUEsU0FBUyxDQUFDbEksTUFBVixHQUFtQixTQUFTQSxNQUFULENBQWdCUixVQUFoQixFQUE0QjtBQUMzQyxXQUFPLElBQUkwSSxTQUFKLENBQWMxSSxVQUFkLENBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSTBJLEVBQUFBLFNBQVMsQ0FBQ2pJLE1BQVYsR0FBbUIsU0FBU0EsTUFBVCxDQUFnQkMsT0FBaEIsRUFBeUJDLE1BQXpCLEVBQWlDO0FBQ2hELFFBQUksQ0FBQ0EsTUFBTCxFQUNJQSxNQUFNLEdBQUdsQixPQUFPLENBQUNlLE1BQVIsRUFBVDtBQUNKLFFBQUlFLE9BQU8sQ0FBQ3dGLElBQVIsSUFBZ0IsSUFBaEIsSUFBd0JoRyxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxNQUFwQyxDQUE1QixFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixLQUFyQyxZQUErQ0osT0FBTyxDQUFDd0YsSUFBdkQ7QUFDSixRQUFJeEYsT0FBTyxDQUFDMkYsT0FBUixJQUFtQixJQUFuQixJQUEyQm5HLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFNBQXBDLENBQS9CLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLFlBQWdESixPQUFPLENBQUMyRixPQUF4RDtBQUNKLFFBQUkzRixPQUFPLENBQUMrSCxLQUFSLElBQWlCLElBQWpCLElBQXlCdkksTUFBTSxDQUFDVSxjQUFQLENBQXNCQyxJQUF0QixDQUEyQkgsT0FBM0IsRUFBb0MsT0FBcEMsQ0FBN0IsRUFDSUMsTUFBTSxDQUFDRyxNQUFQO0FBQWM7QUFBdUIsTUFBckMsV0FBK0NKLE9BQU8sQ0FBQytILEtBQXZEO0FBQ0osV0FBTzlILE1BQVA7QUFDSCxHQVZEO0FBWUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSStILEVBQUFBLFNBQVMsQ0FBQ3pILGVBQVYsR0FBNEIsU0FBU0EsZUFBVCxDQUF5QlAsT0FBekIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ2xFLFdBQU8sS0FBS0YsTUFBTCxDQUFZQyxPQUFaLEVBQXFCQyxNQUFyQixFQUE2Qk8sTUFBN0IsRUFBUDtBQUNILEdBRkQ7QUFJQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXdILEVBQUFBLFNBQVMsQ0FBQ3ZILE1BQVYsR0FBbUIsU0FBU0EsTUFBVCxDQUFnQkMsTUFBaEIsRUFBd0JoQixNQUF4QixFQUFnQztBQUMvQyxRQUFJLEVBQUVnQixNQUFNLFlBQVk3QixPQUFwQixDQUFKLEVBQ0k2QixNQUFNLEdBQUc3QixPQUFPLENBQUNpQixNQUFSLENBQWVZLE1BQWYsQ0FBVDtBQUNKLFFBQUlDLEdBQUcsR0FBR2pCLE1BQU0sS0FBS2tCLFNBQVgsR0FBdUJGLE1BQU0sQ0FBQ0csR0FBOUIsR0FBb0NILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhcEIsTUFBM0Q7QUFBQSxRQUFtRU0sT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQzZJLFNBQVYsRUFBN0U7O0FBQ0EsV0FBT3RILE1BQU0sQ0FBQ0ksR0FBUCxHQUFhSCxHQUFwQixFQUF5QjtBQUNyQixVQUFJSSxHQUFHLEdBQUdMLE1BQU0sQ0FBQ04sTUFBUCxFQUFWOztBQUNBLGNBQVFXLEdBQUcsS0FBSyxDQUFoQjtBQUNBLGFBQUssQ0FBTDtBQUNJZixVQUFBQSxPQUFPLENBQUN3RixJQUFSLEdBQWU5RSxNQUFNLFVBQU4sRUFBZjtBQUNBOztBQUNKLGFBQUssQ0FBTDtBQUNJVixVQUFBQSxPQUFPLENBQUMyRixPQUFSLEdBQWtCakYsTUFBTSxVQUFOLEVBQWxCO0FBQ0E7O0FBQ0osYUFBSyxDQUFMO0FBQ0lWLFVBQUFBLE9BQU8sQ0FBQytILEtBQVIsR0FBZ0JySCxNQUFNLFNBQU4sRUFBaEI7QUFDQTs7QUFDSjtBQUNJQSxVQUFBQSxNQUFNLENBQUNNLFFBQVAsQ0FBZ0JELEdBQUcsR0FBRyxDQUF0QjtBQUNBO0FBWko7QUFjSDs7QUFDRCxXQUFPZixPQUFQO0FBQ0gsR0F0QkQ7QUF3QkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJZ0ksRUFBQUEsU0FBUyxDQUFDL0csZUFBVixHQUE0QixTQUFTQSxlQUFULENBQXlCUCxNQUF6QixFQUFpQztBQUN6RCxRQUFJLEVBQUVBLE1BQU0sWUFBWTdCLE9BQXBCLENBQUosRUFDSTZCLE1BQU0sR0FBRyxJQUFJN0IsT0FBSixDQUFZNkIsTUFBWixDQUFUO0FBQ0osV0FBTyxLQUFLRCxNQUFMLENBQVlDLE1BQVosRUFBb0JBLE1BQU0sQ0FBQ04sTUFBUCxFQUFwQixDQUFQO0FBQ0gsR0FKRDtBQU1BO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJNEgsRUFBQUEsU0FBUyxDQUFDOUcsTUFBVixHQUFtQixTQUFTQSxNQUFULENBQWdCbEIsT0FBaEIsRUFBeUI7QUFDeEMsUUFBSSxPQUFPQSxPQUFQLEtBQW1CLFFBQW5CLElBQStCQSxPQUFPLEtBQUssSUFBL0MsRUFDSSxPQUFPLGlCQUFQO0FBQ0osUUFBSUEsT0FBTyxDQUFDd0YsSUFBUixJQUFnQixJQUFoQixJQUF3QnhGLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixNQUF2QixDQUE1QixFQUNJLElBQUksT0FBT0YsT0FBTyxDQUFDd0YsSUFBZixLQUF3QixRQUE1QixFQUNJLE9BQU8sdUJBQVA7QUFDUixRQUFJeEYsT0FBTyxDQUFDMkYsT0FBUixJQUFtQixJQUFuQixJQUEyQjNGLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixTQUF2QixDQUEvQixFQUNJLElBQUksT0FBT0YsT0FBTyxDQUFDMkYsT0FBZixLQUEyQixRQUEvQixFQUNJLE9BQU8sMEJBQVA7QUFDUixRQUFJM0YsT0FBTyxDQUFDK0gsS0FBUixJQUFpQixJQUFqQixJQUF5Qi9ILE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixPQUF2QixDQUE3QixFQUNJLElBQUksT0FBT0YsT0FBTyxDQUFDK0gsS0FBZixLQUF5QixRQUE3QixFQUNJLE9BQU8sd0JBQVA7QUFDUixXQUFPLElBQVA7QUFDSCxHQWJEO0FBZUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lDLEVBQUFBLFNBQVMsQ0FBQzNHLFVBQVYsR0FBdUIsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDL0MsUUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDNkksU0FBNUIsRUFDSSxPQUFPMUcsTUFBUDtBQUNKLFFBQUl0QixPQUFPLEdBQUcsSUFBSWIsS0FBSyxDQUFDNkksU0FBVixFQUFkO0FBQ0EsUUFBSTFHLE1BQU0sQ0FBQ2tFLElBQVAsSUFBZSxJQUFuQixFQUNJeEYsT0FBTyxDQUFDd0YsSUFBUixHQUFlcUIsTUFBTSxDQUFDdkYsTUFBTSxDQUFDa0UsSUFBUixDQUFyQjtBQUNKLFFBQUlsRSxNQUFNLENBQUNxRSxPQUFQLElBQWtCLElBQXRCLEVBQ0kzRixPQUFPLENBQUMyRixPQUFSLEdBQWtCa0IsTUFBTSxDQUFDdkYsTUFBTSxDQUFDcUUsT0FBUixDQUF4QjtBQUNKLFFBQUlyRSxNQUFNLENBQUN5RyxLQUFQLElBQWdCLElBQXBCLEVBQ0kvSCxPQUFPLENBQUMrSCxLQUFSLEdBQWdCbEIsTUFBTSxDQUFDdkYsTUFBTSxDQUFDeUcsS0FBUixDQUF0QjtBQUNKLFdBQU8vSCxPQUFQO0FBQ0gsR0FYRDtBQWFBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lnSSxFQUFBQSxTQUFTLENBQUN4RyxRQUFWLEdBQXFCLFNBQVNBLFFBQVQsQ0FBa0J4QixPQUFsQixFQUEyQnlCLE9BQTNCLEVBQW9DO0FBQ3JELFFBQUksQ0FBQ0EsT0FBTCxFQUNJQSxPQUFPLEdBQUcsRUFBVjtBQUNKLFFBQUlILE1BQU0sR0FBRyxFQUFiOztBQUNBLFFBQUlHLE9BQU8sQ0FBQ0MsUUFBWixFQUFzQjtBQUNsQkosTUFBQUEsTUFBTSxDQUFDa0UsSUFBUCxHQUFjLENBQWQ7QUFDQWxFLE1BQUFBLE1BQU0sQ0FBQ3FFLE9BQVAsR0FBaUIsQ0FBakI7QUFDQXJFLE1BQUFBLE1BQU0sQ0FBQ3lHLEtBQVAsR0FBZSxDQUFmO0FBQ0g7O0FBQ0QsUUFBSS9ILE9BQU8sQ0FBQ3dGLElBQVIsSUFBZ0IsSUFBaEIsSUFBd0J4RixPQUFPLENBQUNFLGNBQVIsQ0FBdUIsTUFBdkIsQ0FBNUIsRUFDSW9CLE1BQU0sQ0FBQ2tFLElBQVAsR0FBYy9ELE9BQU8sQ0FBQ3dHLElBQVIsSUFBZ0IsQ0FBQ0MsUUFBUSxDQUFDbEksT0FBTyxDQUFDd0YsSUFBVCxDQUF6QixHQUEwQ2pFLE1BQU0sQ0FBQ3ZCLE9BQU8sQ0FBQ3dGLElBQVQsQ0FBaEQsR0FBaUV4RixPQUFPLENBQUN3RixJQUF2RjtBQUNKLFFBQUl4RixPQUFPLENBQUMyRixPQUFSLElBQW1CLElBQW5CLElBQTJCM0YsT0FBTyxDQUFDRSxjQUFSLENBQXVCLFNBQXZCLENBQS9CLEVBQ0lvQixNQUFNLENBQUNxRSxPQUFQLEdBQWlCbEUsT0FBTyxDQUFDd0csSUFBUixJQUFnQixDQUFDQyxRQUFRLENBQUNsSSxPQUFPLENBQUMyRixPQUFULENBQXpCLEdBQTZDcEUsTUFBTSxDQUFDdkIsT0FBTyxDQUFDMkYsT0FBVCxDQUFuRCxHQUF1RTNGLE9BQU8sQ0FBQzJGLE9BQWhHO0FBQ0osUUFBSTNGLE9BQU8sQ0FBQytILEtBQVIsSUFBaUIsSUFBakIsSUFBeUIvSCxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFDSW9CLE1BQU0sQ0FBQ3lHLEtBQVAsR0FBZXRHLE9BQU8sQ0FBQ3dHLElBQVIsSUFBZ0IsQ0FBQ0MsUUFBUSxDQUFDbEksT0FBTyxDQUFDK0gsS0FBVCxDQUF6QixHQUEyQ3hHLE1BQU0sQ0FBQ3ZCLE9BQU8sQ0FBQytILEtBQVQsQ0FBakQsR0FBbUUvSCxPQUFPLENBQUMrSCxLQUExRjtBQUNKLFdBQU96RyxNQUFQO0FBQ0gsR0FoQkQ7QUFrQkE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJMEcsRUFBQUEsU0FBUyxDQUFDckksU0FBVixDQUFvQmdDLE1BQXBCLEdBQTZCLFNBQVNBLE1BQVQsR0FBa0I7QUFDM0MsV0FBTyxLQUFLQyxXQUFMLENBQWlCSixRQUFqQixDQUEwQixJQUExQixFQUFnQzdDLFNBQVMsQ0FBQ08sSUFBVixDQUFlMkMsYUFBL0MsQ0FBUDtBQUNILEdBRkQ7O0FBSUEsU0FBT21HLFNBQVA7QUFDSCxDQXRPaUIsRUFBbEI7O0FBd09BN0ksS0FBSyxDQUFDNEcsaUJBQU4sR0FBMkIsWUFBVztBQUVsQztBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSSxXQUFTQSxpQkFBVCxDQUEyQnpHLFVBQTNCLEVBQXVDO0FBQ25DLFFBQUlBLFVBQUosRUFDSSxLQUFLLElBQUlDLElBQUksR0FBR0MsTUFBTSxDQUFDRCxJQUFQLENBQVlELFVBQVosQ0FBWCxFQUFvQ0csQ0FBQyxHQUFHLENBQTdDLEVBQWdEQSxDQUFDLEdBQUdGLElBQUksQ0FBQ0csTUFBekQsRUFBaUUsRUFBRUQsQ0FBbkU7QUFDSSxVQUFJSCxVQUFVLENBQUNDLElBQUksQ0FBQ0UsQ0FBRCxDQUFMLENBQVYsSUFBdUIsSUFBM0IsRUFDSSxLQUFLRixJQUFJLENBQUNFLENBQUQsQ0FBVCxJQUFnQkgsVUFBVSxDQUFDQyxJQUFJLENBQUNFLENBQUQsQ0FBTCxDQUExQjtBQUZSO0FBR1A7QUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJc0csRUFBQUEsaUJBQWlCLENBQUNwRyxTQUFsQixDQUE0QndJLEtBQTVCLEdBQW9DLENBQXBDO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNJcEMsRUFBQUEsaUJBQWlCLENBQUNwRyxTQUFsQixDQUE0QnlJLFFBQTVCLEdBQXVDbkosS0FBSyxDQUFDd0csSUFBTixHQUFheEcsS0FBSyxDQUFDd0csSUFBTixDQUFXQyxRQUFYLENBQW9CLENBQXBCLEVBQXNCLENBQXRCLEVBQXdCLEtBQXhCLENBQWIsR0FBOEMsQ0FBckY7QUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0lLLEVBQUFBLGlCQUFpQixDQUFDcEcsU0FBbEIsQ0FBNEIwSSxVQUE1QixHQUF5Q3BKLEtBQUssQ0FBQ3dHLElBQU4sR0FBYXhHLEtBQUssQ0FBQ3dHLElBQU4sQ0FBV0MsUUFBWCxDQUFvQixDQUFwQixFQUFzQixDQUF0QixFQUF3QixLQUF4QixDQUFiLEdBQThDLENBQXZGO0FBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDSUssRUFBQUEsaUJBQWlCLENBQUNqRyxNQUFsQixHQUEyQixTQUFTQSxNQUFULENBQWdCUixVQUFoQixFQUE0QjtBQUNuRCxXQUFPLElBQUl5RyxpQkFBSixDQUFzQnpHLFVBQXRCLENBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDSXlHLEVBQUFBLGlCQUFpQixDQUFDaEcsTUFBbEIsR0FBMkIsU0FBU0EsTUFBVCxDQUFnQkMsT0FBaEIsRUFBeUJDLE1BQXpCLEVBQWlDO0FBQ3hELFFBQUksQ0FBQ0EsTUFBTCxFQUNJQSxNQUFNLEdBQUdsQixPQUFPLENBQUNlLE1BQVIsRUFBVDtBQUNKLFFBQUlFLE9BQU8sQ0FBQ21JLEtBQVIsSUFBaUIsSUFBakIsSUFBeUIzSSxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxPQUFwQyxDQUE3QixFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixLQUFyQyxFQUF3Q0MsS0FBeEMsQ0FBOENMLE9BQU8sQ0FBQ21JLEtBQXREO0FBQ0osUUFBSW5JLE9BQU8sQ0FBQ29JLFFBQVIsSUFBb0IsSUFBcEIsSUFBNEI1SSxNQUFNLENBQUNVLGNBQVAsQ0FBc0JDLElBQXRCLENBQTJCSCxPQUEzQixFQUFvQyxVQUFwQyxDQUFoQyxFQUNJQyxNQUFNLENBQUNHLE1BQVA7QUFBYztBQUF1QixNQUFyQyxFQUF5Q3NILEtBQXpDLENBQStDMUgsT0FBTyxDQUFDb0ksUUFBdkQ7QUFDSixRQUFJcEksT0FBTyxDQUFDcUksVUFBUixJQUFzQixJQUF0QixJQUE4QjdJLE1BQU0sQ0FBQ1UsY0FBUCxDQUFzQkMsSUFBdEIsQ0FBMkJILE9BQTNCLEVBQW9DLFlBQXBDLENBQWxDLEVBQ0lDLE1BQU0sQ0FBQ0csTUFBUDtBQUFjO0FBQXVCLE1BQXJDLEVBQXlDc0gsS0FBekMsQ0FBK0MxSCxPQUFPLENBQUNxSSxVQUF2RDtBQUNKLFdBQU9wSSxNQUFQO0FBQ0gsR0FWRDtBQVlBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0k4RixFQUFBQSxpQkFBaUIsQ0FBQ3hGLGVBQWxCLEdBQW9DLFNBQVNBLGVBQVQsQ0FBeUJQLE9BQXpCLEVBQWtDQyxNQUFsQyxFQUEwQztBQUMxRSxXQUFPLEtBQUtGLE1BQUwsQ0FBWUMsT0FBWixFQUFxQkMsTUFBckIsRUFBNkJPLE1BQTdCLEVBQVA7QUFDSCxHQUZEO0FBSUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0l1RixFQUFBQSxpQkFBaUIsQ0FBQ3RGLE1BQWxCLEdBQTJCLFNBQVNBLE1BQVQsQ0FBZ0JDLE1BQWhCLEVBQXdCaEIsTUFBeEIsRUFBZ0M7QUFDdkQsUUFBSSxFQUFFZ0IsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHN0IsT0FBTyxDQUFDaUIsTUFBUixDQUFlWSxNQUFmLENBQVQ7QUFDSixRQUFJQyxHQUFHLEdBQUdqQixNQUFNLEtBQUtrQixTQUFYLEdBQXVCRixNQUFNLENBQUNHLEdBQTlCLEdBQW9DSCxNQUFNLENBQUNJLEdBQVAsR0FBYXBCLE1BQTNEO0FBQUEsUUFBbUVNLE9BQU8sR0FBRyxJQUFJYixLQUFLLENBQUM0RyxpQkFBVixFQUE3RTs7QUFDQSxXQUFPckYsTUFBTSxDQUFDSSxHQUFQLEdBQWFILEdBQXBCLEVBQXlCO0FBQ3JCLFVBQUlJLEdBQUcsR0FBR0wsTUFBTSxDQUFDTixNQUFQLEVBQVY7O0FBQ0EsY0FBUVcsR0FBRyxLQUFLLENBQWhCO0FBQ0EsYUFBSyxDQUFMO0FBQ0lmLFVBQUFBLE9BQU8sQ0FBQ21JLEtBQVIsR0FBZ0J6SCxNQUFNLENBQUNMLEtBQVAsRUFBaEI7QUFDQTs7QUFDSixhQUFLLENBQUw7QUFDSUwsVUFBQUEsT0FBTyxDQUFDb0ksUUFBUixHQUFtQjFILE1BQU0sQ0FBQ2dILEtBQVAsRUFBbkI7QUFDQTs7QUFDSixhQUFLLENBQUw7QUFDSTFILFVBQUFBLE9BQU8sQ0FBQ3FJLFVBQVIsR0FBcUIzSCxNQUFNLENBQUNnSCxLQUFQLEVBQXJCO0FBQ0E7O0FBQ0o7QUFDSWhILFVBQUFBLE1BQU0sQ0FBQ00sUUFBUCxDQUFnQkQsR0FBRyxHQUFHLENBQXRCO0FBQ0E7QUFaSjtBQWNIOztBQUNELFdBQU9mLE9BQVA7QUFDSCxHQXRCRDtBQXdCQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0krRixFQUFBQSxpQkFBaUIsQ0FBQzlFLGVBQWxCLEdBQW9DLFNBQVNBLGVBQVQsQ0FBeUJQLE1BQXpCLEVBQWlDO0FBQ2pFLFFBQUksRUFBRUEsTUFBTSxZQUFZN0IsT0FBcEIsQ0FBSixFQUNJNkIsTUFBTSxHQUFHLElBQUk3QixPQUFKLENBQVk2QixNQUFaLENBQVQ7QUFDSixXQUFPLEtBQUtELE1BQUwsQ0FBWUMsTUFBWixFQUFvQkEsTUFBTSxDQUFDTixNQUFQLEVBQXBCLENBQVA7QUFDSCxHQUpEO0FBTUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0kyRixFQUFBQSxpQkFBaUIsQ0FBQzdFLE1BQWxCLEdBQTJCLFNBQVNBLE1BQVQsQ0FBZ0JsQixPQUFoQixFQUF5QjtBQUNoRCxRQUFJLE9BQU9BLE9BQVAsS0FBbUIsUUFBbkIsSUFBK0JBLE9BQU8sS0FBSyxJQUEvQyxFQUNJLE9BQU8saUJBQVA7QUFDSixRQUFJQSxPQUFPLENBQUNtSSxLQUFSLElBQWlCLElBQWpCLElBQXlCbkksT0FBTyxDQUFDRSxjQUFSLENBQXVCLE9BQXZCLENBQTdCLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQ21JLEtBQXhCLENBQUwsRUFDSSxPQUFPLHlCQUFQO0FBQ1IsUUFBSW5JLE9BQU8sQ0FBQ29JLFFBQVIsSUFBb0IsSUFBcEIsSUFBNEJwSSxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsVUFBdkIsQ0FBaEMsRUFDSSxJQUFJLENBQUNqQixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDb0ksUUFBeEIsQ0FBRCxJQUFzQyxFQUFFcEksT0FBTyxDQUFDb0ksUUFBUixJQUFvQm5KLEtBQUssQ0FBQ2tDLFNBQU4sQ0FBZ0JuQixPQUFPLENBQUNvSSxRQUFSLENBQWlCbkMsR0FBakMsQ0FBcEIsSUFBNkRoSCxLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDb0ksUUFBUixDQUFpQmxDLElBQWpDLENBQS9ELENBQTFDLEVBQ0ksT0FBTyxpQ0FBUDtBQUNSLFFBQUlsRyxPQUFPLENBQUNxSSxVQUFSLElBQXNCLElBQXRCLElBQThCckksT0FBTyxDQUFDRSxjQUFSLENBQXVCLFlBQXZCLENBQWxDLEVBQ0ksSUFBSSxDQUFDakIsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQ3FJLFVBQXhCLENBQUQsSUFBd0MsRUFBRXJJLE9BQU8sQ0FBQ3FJLFVBQVIsSUFBc0JwSixLQUFLLENBQUNrQyxTQUFOLENBQWdCbkIsT0FBTyxDQUFDcUksVUFBUixDQUFtQnBDLEdBQW5DLENBQXRCLElBQWlFaEgsS0FBSyxDQUFDa0MsU0FBTixDQUFnQm5CLE9BQU8sQ0FBQ3FJLFVBQVIsQ0FBbUJuQyxJQUFuQyxDQUFuRSxDQUE1QyxFQUNJLE9BQU8sbUNBQVA7QUFDUixXQUFPLElBQVA7QUFDSCxHQWJEO0FBZUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0lILEVBQUFBLGlCQUFpQixDQUFDMUUsVUFBbEIsR0FBK0IsU0FBU0EsVUFBVCxDQUFvQkMsTUFBcEIsRUFBNEI7QUFDdkQsUUFBSUEsTUFBTSxZQUFZbkMsS0FBSyxDQUFDNEcsaUJBQTVCLEVBQ0ksT0FBT3pFLE1BQVA7QUFDSixRQUFJdEIsT0FBTyxHQUFHLElBQUliLEtBQUssQ0FBQzRHLGlCQUFWLEVBQWQ7QUFDQSxRQUFJekUsTUFBTSxDQUFDNkcsS0FBUCxJQUFnQixJQUFwQixFQUNJbkksT0FBTyxDQUFDbUksS0FBUixHQUFnQjdHLE1BQU0sQ0FBQzZHLEtBQVAsR0FBZSxDQUEvQjtBQUNKLFFBQUk3RyxNQUFNLENBQUM4RyxRQUFQLElBQW1CLElBQXZCLEVBQ0ksSUFBSW5KLEtBQUssQ0FBQ3dHLElBQVYsRUFDSSxDQUFDekYsT0FBTyxDQUFDb0ksUUFBUixHQUFtQm5KLEtBQUssQ0FBQ3dHLElBQU4sQ0FBV1csU0FBWCxDQUFxQjlFLE1BQU0sQ0FBQzhHLFFBQTVCLENBQXBCLEVBQTJEL0IsUUFBM0QsR0FBc0UsS0FBdEUsQ0FESixLQUVLLElBQUksT0FBTy9FLE1BQU0sQ0FBQzhHLFFBQWQsS0FBMkIsUUFBL0IsRUFDRHBJLE9BQU8sQ0FBQ29JLFFBQVIsR0FBbUI5QixRQUFRLENBQUNoRixNQUFNLENBQUM4RyxRQUFSLEVBQWtCLEVBQWxCLENBQTNCLENBREMsS0FFQSxJQUFJLE9BQU85RyxNQUFNLENBQUM4RyxRQUFkLEtBQTJCLFFBQS9CLEVBQ0RwSSxPQUFPLENBQUNvSSxRQUFSLEdBQW1COUcsTUFBTSxDQUFDOEcsUUFBMUIsQ0FEQyxLQUVBLElBQUksT0FBTzlHLE1BQU0sQ0FBQzhHLFFBQWQsS0FBMkIsUUFBL0IsRUFDRHBJLE9BQU8sQ0FBQ29JLFFBQVIsR0FBbUIsSUFBSW5KLEtBQUssQ0FBQ3NILFFBQVYsQ0FBbUJqRixNQUFNLENBQUM4RyxRQUFQLENBQWdCbkMsR0FBaEIsS0FBd0IsQ0FBM0MsRUFBOEMzRSxNQUFNLENBQUM4RyxRQUFQLENBQWdCbEMsSUFBaEIsS0FBeUIsQ0FBdkUsRUFBMEVNLFFBQTFFLEVBQW5CO0FBQ1IsUUFBSWxGLE1BQU0sQ0FBQytHLFVBQVAsSUFBcUIsSUFBekIsRUFDSSxJQUFJcEosS0FBSyxDQUFDd0csSUFBVixFQUNJLENBQUN6RixPQUFPLENBQUNxSSxVQUFSLEdBQXFCcEosS0FBSyxDQUFDd0csSUFBTixDQUFXVyxTQUFYLENBQXFCOUUsTUFBTSxDQUFDK0csVUFBNUIsQ0FBdEIsRUFBK0RoQyxRQUEvRCxHQUEwRSxLQUExRSxDQURKLEtBRUssSUFBSSxPQUFPL0UsTUFBTSxDQUFDK0csVUFBZCxLQUE2QixRQUFqQyxFQUNEckksT0FBTyxDQUFDcUksVUFBUixHQUFxQi9CLFFBQVEsQ0FBQ2hGLE1BQU0sQ0FBQytHLFVBQVIsRUFBb0IsRUFBcEIsQ0FBN0IsQ0FEQyxLQUVBLElBQUksT0FBTy9HLE1BQU0sQ0FBQytHLFVBQWQsS0FBNkIsUUFBakMsRUFDRHJJLE9BQU8sQ0FBQ3FJLFVBQVIsR0FBcUIvRyxNQUFNLENBQUMrRyxVQUE1QixDQURDLEtBRUEsSUFBSSxPQUFPL0csTUFBTSxDQUFDK0csVUFBZCxLQUE2QixRQUFqQyxFQUNEckksT0FBTyxDQUFDcUksVUFBUixHQUFxQixJQUFJcEosS0FBSyxDQUFDc0gsUUFBVixDQUFtQmpGLE1BQU0sQ0FBQytHLFVBQVAsQ0FBa0JwQyxHQUFsQixLQUEwQixDQUE3QyxFQUFnRDNFLE1BQU0sQ0FBQytHLFVBQVAsQ0FBa0JuQyxJQUFsQixLQUEyQixDQUEzRSxFQUE4RU0sUUFBOUUsRUFBckI7QUFDUixXQUFPeEcsT0FBUDtBQUNILEdBekJEO0FBMkJBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0krRixFQUFBQSxpQkFBaUIsQ0FBQ3ZFLFFBQWxCLEdBQTZCLFNBQVNBLFFBQVQsQ0FBa0J4QixPQUFsQixFQUEyQnlCLE9BQTNCLEVBQW9DO0FBQzdELFFBQUksQ0FBQ0EsT0FBTCxFQUNJQSxPQUFPLEdBQUcsRUFBVjtBQUNKLFFBQUlILE1BQU0sR0FBRyxFQUFiOztBQUNBLFFBQUlHLE9BQU8sQ0FBQ0MsUUFBWixFQUFzQjtBQUNsQkosTUFBQUEsTUFBTSxDQUFDNkcsS0FBUCxHQUFlLENBQWY7O0FBQ0EsVUFBSWxKLEtBQUssQ0FBQ3dHLElBQVYsRUFBZ0I7QUFDWixZQUFJaUIsTUFBSSxHQUFHLElBQUl6SCxLQUFLLENBQUN3RyxJQUFWLENBQWUsQ0FBZixFQUFrQixDQUFsQixFQUFxQixLQUFyQixDQUFYOztBQUNBbkUsUUFBQUEsTUFBTSxDQUFDOEcsUUFBUCxHQUFrQjNHLE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JwRixNQUFsQixHQUEyQm1GLE1BQUksQ0FBQ0UsUUFBTCxFQUEzQixHQUE2Q25GLE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JFLE1BQWxCLEdBQTJCSCxNQUFJLENBQUNGLFFBQUwsRUFBM0IsR0FBNkNFLE1BQTVHO0FBQ0gsT0FIRCxNQUlJcEYsTUFBTSxDQUFDOEcsUUFBUCxHQUFrQjNHLE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JwRixNQUFsQixHQUEyQixHQUEzQixHQUFpQyxDQUFuRDs7QUFDSixVQUFJdEMsS0FBSyxDQUFDd0csSUFBVixFQUFnQjtBQUNaLFlBQUlpQixNQUFJLEdBQUcsSUFBSXpILEtBQUssQ0FBQ3dHLElBQVYsQ0FBZSxDQUFmLEVBQWtCLENBQWxCLEVBQXFCLEtBQXJCLENBQVg7O0FBQ0FuRSxRQUFBQSxNQUFNLENBQUMrRyxVQUFQLEdBQW9CNUcsT0FBTyxDQUFDa0YsS0FBUixLQUFrQnBGLE1BQWxCLEdBQTJCbUYsTUFBSSxDQUFDRSxRQUFMLEVBQTNCLEdBQTZDbkYsT0FBTyxDQUFDa0YsS0FBUixLQUFrQkUsTUFBbEIsR0FBMkJILE1BQUksQ0FBQ0YsUUFBTCxFQUEzQixHQUE2Q0UsTUFBOUc7QUFDSCxPQUhELE1BSUlwRixNQUFNLENBQUMrRyxVQUFQLEdBQW9CNUcsT0FBTyxDQUFDa0YsS0FBUixLQUFrQnBGLE1BQWxCLEdBQTJCLEdBQTNCLEdBQWlDLENBQXJEO0FBQ1A7O0FBQ0QsUUFBSXZCLE9BQU8sQ0FBQ21JLEtBQVIsSUFBaUIsSUFBakIsSUFBeUJuSSxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsT0FBdkIsQ0FBN0IsRUFDSW9CLE1BQU0sQ0FBQzZHLEtBQVAsR0FBZW5JLE9BQU8sQ0FBQ21JLEtBQXZCO0FBQ0osUUFBSW5JLE9BQU8sQ0FBQ29JLFFBQVIsSUFBb0IsSUFBcEIsSUFBNEJwSSxPQUFPLENBQUNFLGNBQVIsQ0FBdUIsVUFBdkIsQ0FBaEMsRUFDSSxJQUFJLE9BQU9GLE9BQU8sQ0FBQ29JLFFBQWYsS0FBNEIsUUFBaEMsRUFDSTlHLE1BQU0sQ0FBQzhHLFFBQVAsR0FBa0IzRyxPQUFPLENBQUNrRixLQUFSLEtBQWtCcEYsTUFBbEIsR0FBMkJBLE1BQU0sQ0FBQ3ZCLE9BQU8sQ0FBQ29JLFFBQVQsQ0FBakMsR0FBc0RwSSxPQUFPLENBQUNvSSxRQUFoRixDQURKLEtBR0k5RyxNQUFNLENBQUM4RyxRQUFQLEdBQWtCM0csT0FBTyxDQUFDa0YsS0FBUixLQUFrQnBGLE1BQWxCLEdBQTJCdEMsS0FBSyxDQUFDd0csSUFBTixDQUFXOUYsU0FBWCxDQUFxQmlILFFBQXJCLENBQThCekcsSUFBOUIsQ0FBbUNILE9BQU8sQ0FBQ29JLFFBQTNDLENBQTNCLEdBQWtGM0csT0FBTyxDQUFDa0YsS0FBUixLQUFrQkUsTUFBbEIsR0FBMkIsSUFBSTVILEtBQUssQ0FBQ3NILFFBQVYsQ0FBbUJ2RyxPQUFPLENBQUNvSSxRQUFSLENBQWlCbkMsR0FBakIsS0FBeUIsQ0FBNUMsRUFBK0NqRyxPQUFPLENBQUNvSSxRQUFSLENBQWlCbEMsSUFBakIsS0FBMEIsQ0FBekUsRUFBNEVNLFFBQTVFLEVBQTNCLEdBQW9IeEcsT0FBTyxDQUFDb0ksUUFBaE87QUFDUixRQUFJcEksT0FBTyxDQUFDcUksVUFBUixJQUFzQixJQUF0QixJQUE4QnJJLE9BQU8sQ0FBQ0UsY0FBUixDQUF1QixZQUF2QixDQUFsQyxFQUNJLElBQUksT0FBT0YsT0FBTyxDQUFDcUksVUFBZixLQUE4QixRQUFsQyxFQUNJL0csTUFBTSxDQUFDK0csVUFBUCxHQUFvQjVHLE9BQU8sQ0FBQ2tGLEtBQVIsS0FBa0JwRixNQUFsQixHQUEyQkEsTUFBTSxDQUFDdkIsT0FBTyxDQUFDcUksVUFBVCxDQUFqQyxHQUF3RHJJLE9BQU8sQ0FBQ3FJLFVBQXBGLENBREosS0FHSS9HLE1BQU0sQ0FBQytHLFVBQVAsR0FBb0I1RyxPQUFPLENBQUNrRixLQUFSLEtBQWtCcEYsTUFBbEIsR0FBMkJ0QyxLQUFLLENBQUN3RyxJQUFOLENBQVc5RixTQUFYLENBQXFCaUgsUUFBckIsQ0FBOEJ6RyxJQUE5QixDQUFtQ0gsT0FBTyxDQUFDcUksVUFBM0MsQ0FBM0IsR0FBb0Y1RyxPQUFPLENBQUNrRixLQUFSLEtBQWtCRSxNQUFsQixHQUEyQixJQUFJNUgsS0FBSyxDQUFDc0gsUUFBVixDQUFtQnZHLE9BQU8sQ0FBQ3FJLFVBQVIsQ0FBbUJwQyxHQUFuQixLQUEyQixDQUE5QyxFQUFpRGpHLE9BQU8sQ0FBQ3FJLFVBQVIsQ0FBbUJuQyxJQUFuQixLQUE0QixDQUE3RSxFQUFnRk0sUUFBaEYsRUFBM0IsR0FBd0h4RyxPQUFPLENBQUNxSSxVQUF4TztBQUNSLFdBQU8vRyxNQUFQO0FBQ0gsR0E5QkQ7QUFnQ0E7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNJeUUsRUFBQUEsaUJBQWlCLENBQUNwRyxTQUFsQixDQUE0QmdDLE1BQTVCLEdBQXFDLFNBQVNBLE1BQVQsR0FBa0I7QUFDbkQsV0FBTyxLQUFLQyxXQUFMLENBQWlCSixRQUFqQixDQUEwQixJQUExQixFQUFnQzdDLFNBQVMsQ0FBQ08sSUFBVixDQUFlMkMsYUFBL0MsQ0FBUDtBQUNILEdBRkQ7O0FBSUEsU0FBT2tFLGlCQUFQO0FBQ0gsQ0FsUXlCLEVBQTFCOztBQW9RQXVDLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQnBKLEtBQWpCIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvKmVzbGludC1kaXNhYmxlIGJsb2NrLXNjb3BlZC12YXIsIGlkLWxlbmd0aCwgbm8tY29udHJvbC1yZWdleCwgbm8tbWFnaWMtbnVtYmVycywgbm8tcHJvdG90eXBlLWJ1aWx0aW5zLCBuby1yZWRlY2xhcmUsIG5vLXNoYWRvdywgbm8tdmFyLCBzb3J0LXZhcnMqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciAkcHJvdG9idWYgPSByZXF1aXJlKFwicHJvdG9idWZqcy9taW5pbWFsXCIpO1xuXG4vLyBDb21tb24gYWxpYXNlc1xudmFyICRSZWFkZXIgPSAkcHJvdG9idWYuUmVhZGVyLCAkV3JpdGVyID0gJHByb3RvYnVmLldyaXRlciwgJHV0aWwgPSAkcHJvdG9idWYudXRpbDtcblxuLy8gRXhwb3J0ZWQgcm9vdCBuYW1lc3BhY2VcbnZhciAkcm9vdCA9ICRwcm90b2J1Zi5yb290c1tcImRlZmF1bHRcIl0gfHwgKCRwcm90b2J1Zi5yb290c1tcImRlZmF1bHRcIl0gPSB7fSk7XG5cbiRyb290Lkludm9rZUJlZ2luID0gKGZ1bmN0aW9uKCkge1xuXG4gICAgLyoqXG4gICAgICogUHJvcGVydGllcyBvZiBhbiBJbnZva2VCZWdpbi5cbiAgICAgKiBAZXhwb3J0cyBJSW52b2tlQmVnaW5cbiAgICAgKiBAaW50ZXJmYWNlIElJbnZva2VCZWdpblxuICAgICAqIEBwcm9wZXJ0eSB7bnVtYmVyfG51bGx9IFtpbnZva2VJZF0gSW52b2tlQmVnaW4gaW52b2tlSWRcbiAgICAgKiBAcHJvcGVydHkge3N0cmluZ3xudWxsfSBbbWV0aG9kXSBJbnZva2VCZWdpbiBtZXRob2RcbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIENvbnN0cnVjdHMgYSBuZXcgSW52b2tlQmVnaW4uXG4gICAgICogQGV4cG9ydHMgSW52b2tlQmVnaW5cbiAgICAgKiBAY2xhc3NkZXNjIFJlcHJlc2VudHMgYW4gSW52b2tlQmVnaW4uXG4gICAgICogQGltcGxlbWVudHMgSUludm9rZUJlZ2luXG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICogQHBhcmFtIHtJSW52b2tlQmVnaW49fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBJbnZva2VCZWdpbihwcm9wZXJ0aWVzKSB7XG4gICAgICAgIGlmIChwcm9wZXJ0aWVzKVxuICAgICAgICAgICAgZm9yICh2YXIga2V5cyA9IE9iamVjdC5rZXlzKHByb3BlcnRpZXMpLCBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpXG4gICAgICAgICAgICAgICAgaWYgKHByb3BlcnRpZXNba2V5c1tpXV0gIT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgdGhpc1trZXlzW2ldXSA9IHByb3BlcnRpZXNba2V5c1tpXV07XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW52b2tlQmVnaW4gaW52b2tlSWQuXG4gICAgICogQG1lbWJlciB7bnVtYmVyfSBpbnZva2VJZFxuICAgICAqIEBtZW1iZXJvZiBJbnZva2VCZWdpblxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqL1xuICAgIEludm9rZUJlZ2luLnByb3RvdHlwZS5pbnZva2VJZCA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBJbnZva2VCZWdpbiBtZXRob2QuXG4gICAgICogQG1lbWJlciB7c3RyaW5nfSBtZXRob2RcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlQmVnaW5cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBJbnZva2VCZWdpbi5wcm90b3R5cGUubWV0aG9kID0gXCJcIjtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBuZXcgSW52b2tlQmVnaW4gaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAqIEBmdW5jdGlvbiBjcmVhdGVcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlQmVnaW5cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJSW52b2tlQmVnaW49fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKiBAcmV0dXJucyB7SW52b2tlQmVnaW59IEludm9rZUJlZ2luIGluc3RhbmNlXG4gICAgICovXG4gICAgSW52b2tlQmVnaW4uY3JlYXRlID0gZnVuY3Rpb24gY3JlYXRlKHByb3BlcnRpZXMpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBJbnZva2VCZWdpbihwcm9wZXJ0aWVzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIEludm9rZUJlZ2luIG1lc3NhZ2UuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIEludm9rZUJlZ2luLnZlcmlmeXx2ZXJpZnl9IG1lc3NhZ2VzLlxuICAgICAqIEBmdW5jdGlvbiBlbmNvZGVcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlQmVnaW5cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJSW52b2tlQmVnaW59IG1lc3NhZ2UgSW52b2tlQmVnaW4gbWVzc2FnZSBvciBwbGFpbiBvYmplY3QgdG8gZW5jb2RlXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuV3JpdGVyfSBbd3JpdGVyXSBXcml0ZXIgdG8gZW5jb2RlIHRvXG4gICAgICogQHJldHVybnMgeyRwcm90b2J1Zi5Xcml0ZXJ9IFdyaXRlclxuICAgICAqL1xuICAgIEludm9rZUJlZ2luLmVuY29kZSA9IGZ1bmN0aW9uIGVuY29kZShtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgaWYgKCF3cml0ZXIpXG4gICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICBpZiAobWVzc2FnZS5pbnZva2VJZCAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwiaW52b2tlSWRcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDEsIHdpcmVUeXBlIDAgPSovOCkuaW50MzIobWVzc2FnZS5pbnZva2VJZCk7XG4gICAgICAgIGlmIChtZXNzYWdlLm1ldGhvZCAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwibWV0aG9kXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAyLCB3aXJlVHlwZSAyID0qLzE4KS5zdHJpbmcobWVzc2FnZS5tZXRob2QpO1xuICAgICAgICByZXR1cm4gd3JpdGVyO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgSW52b2tlQmVnaW4gbWVzc2FnZSwgbGVuZ3RoIGRlbGltaXRlZC4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgSW52b2tlQmVnaW4udmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBJbnZva2VCZWdpblxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lJbnZva2VCZWdpbn0gbWVzc2FnZSBJbnZva2VCZWdpbiBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgSW52b2tlQmVnaW4uZW5jb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5lbmNvZGUobWVzc2FnZSwgd3JpdGVyKS5sZGVsaW0oKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhbiBJbnZva2VCZWdpbiBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlQmVnaW5cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUmVhZGVyfFVpbnQ4QXJyYXl9IHJlYWRlciBSZWFkZXIgb3IgYnVmZmVyIHRvIGRlY29kZSBmcm9tXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFtsZW5ndGhdIE1lc3NhZ2UgbGVuZ3RoIGlmIGtub3duIGJlZm9yZWhhbmRcbiAgICAgKiBAcmV0dXJucyB7SW52b2tlQmVnaW59IEludm9rZUJlZ2luXG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBJbnZva2VCZWdpbi5kZWNvZGUgPSBmdW5jdGlvbiBkZWNvZGUocmVhZGVyLCBsZW5ndGgpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSAkUmVhZGVyLmNyZWF0ZShyZWFkZXIpO1xuICAgICAgICB2YXIgZW5kID0gbGVuZ3RoID09PSB1bmRlZmluZWQgPyByZWFkZXIubGVuIDogcmVhZGVyLnBvcyArIGxlbmd0aCwgbWVzc2FnZSA9IG5ldyAkcm9vdC5JbnZva2VCZWdpbigpO1xuICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZCkge1xuICAgICAgICAgICAgdmFyIHRhZyA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5pbnZva2VJZCA9IHJlYWRlci5pbnQzMigpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UubWV0aG9kID0gcmVhZGVyLnN0cmluZygpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZWFkZXIuc2tpcFR5cGUodGFnICYgNyk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYW4gSW52b2tlQmVnaW4gbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlciwgbGVuZ3RoIGRlbGltaXRlZC5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIEludm9rZUJlZ2luXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtJbnZva2VCZWdpbn0gSW52b2tlQmVnaW5cbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIEludm9rZUJlZ2luLmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhbiBJbnZva2VCZWdpbiBtZXNzYWdlLlxuICAgICAqIEBmdW5jdGlvbiB2ZXJpZnlcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlQmVnaW5cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBJbnZva2VCZWdpbi52ZXJpZnkgPSBmdW5jdGlvbiB2ZXJpZnkobWVzc2FnZSkge1xuICAgICAgICBpZiAodHlwZW9mIG1lc3NhZ2UgIT09IFwib2JqZWN0XCIgfHwgbWVzc2FnZSA9PT0gbnVsbClcbiAgICAgICAgICAgIHJldHVybiBcIm9iamVjdCBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5pbnZva2VJZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJpbnZva2VJZFwiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UuaW52b2tlSWQpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcImludm9rZUlkOiBpbnRlZ2VyIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLm1ldGhvZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJtZXRob2RcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzU3RyaW5nKG1lc3NhZ2UubWV0aG9kKSlcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJtZXRob2Q6IHN0cmluZyBleHBlY3RlZFwiO1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhbiBJbnZva2VCZWdpbiBtZXNzYWdlIGZyb20gYSBwbGFpbiBvYmplY3QuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIHRoZWlyIHJlc3BlY3RpdmUgaW50ZXJuYWwgdHlwZXMuXG4gICAgICogQGZ1bmN0aW9uIGZyb21PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlQmVnaW5cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gb2JqZWN0IFBsYWluIG9iamVjdFxuICAgICAqIEByZXR1cm5zIHtJbnZva2VCZWdpbn0gSW52b2tlQmVnaW5cbiAgICAgKi9cbiAgICBJbnZva2VCZWdpbi5mcm9tT2JqZWN0ID0gZnVuY3Rpb24gZnJvbU9iamVjdChvYmplY3QpIHtcbiAgICAgICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mICRyb290Lkludm9rZUJlZ2luKVxuICAgICAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICAgICAgdmFyIG1lc3NhZ2UgPSBuZXcgJHJvb3QuSW52b2tlQmVnaW4oKTtcbiAgICAgICAgaWYgKG9iamVjdC5pbnZva2VJZCAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5pbnZva2VJZCA9IG9iamVjdC5pbnZva2VJZCB8IDA7XG4gICAgICAgIGlmIChvYmplY3QubWV0aG9kICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLm1ldGhvZCA9IFN0cmluZyhvYmplY3QubWV0aG9kKTtcbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBwbGFpbiBvYmplY3QgZnJvbSBhbiBJbnZva2VCZWdpbiBtZXNzYWdlLiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byBvdGhlciB0eXBlcyBpZiBzcGVjaWZpZWQuXG4gICAgICogQGZ1bmN0aW9uIHRvT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIEludm9rZUJlZ2luXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SW52b2tlQmVnaW59IG1lc3NhZ2UgSW52b2tlQmVnaW5cbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5JQ29udmVyc2lvbk9wdGlvbnN9IFtvcHRpb25zXSBDb252ZXJzaW9uIG9wdGlvbnNcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IFBsYWluIG9iamVjdFxuICAgICAqL1xuICAgIEludm9rZUJlZ2luLnRvT2JqZWN0ID0gZnVuY3Rpb24gdG9PYmplY3QobWVzc2FnZSwgb3B0aW9ucykge1xuICAgICAgICBpZiAoIW9wdGlvbnMpXG4gICAgICAgICAgICBvcHRpb25zID0ge307XG4gICAgICAgIHZhciBvYmplY3QgPSB7fTtcbiAgICAgICAgaWYgKG9wdGlvbnMuZGVmYXVsdHMpIHtcbiAgICAgICAgICAgIG9iamVjdC5pbnZva2VJZCA9IDA7XG4gICAgICAgICAgICBvYmplY3QubWV0aG9kID0gXCJcIjtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWVzc2FnZS5pbnZva2VJZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJpbnZva2VJZFwiKSlcbiAgICAgICAgICAgIG9iamVjdC5pbnZva2VJZCA9IG1lc3NhZ2UuaW52b2tlSWQ7XG4gICAgICAgIGlmIChtZXNzYWdlLm1ldGhvZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJtZXRob2RcIikpXG4gICAgICAgICAgICBvYmplY3QubWV0aG9kID0gbWVzc2FnZS5tZXRob2Q7XG4gICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENvbnZlcnRzIHRoaXMgSW52b2tlQmVnaW4gdG8gSlNPTi5cbiAgICAgKiBAZnVuY3Rpb24gdG9KU09OXG4gICAgICogQG1lbWJlcm9mIEludm9rZUJlZ2luXG4gICAgICogQGluc3RhbmNlXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBKU09OIG9iamVjdFxuICAgICAqL1xuICAgIEludm9rZUJlZ2luLnByb3RvdHlwZS50b0pTT04gPSBmdW5jdGlvbiB0b0pTT04oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbnN0cnVjdG9yLnRvT2JqZWN0KHRoaXMsICRwcm90b2J1Zi51dGlsLnRvSlNPTk9wdGlvbnMpO1xuICAgIH07XG5cbiAgICByZXR1cm4gSW52b2tlQmVnaW47XG59KSgpO1xuXG4kcm9vdC5JbnZva2VFbmQgPSAoZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBQcm9wZXJ0aWVzIG9mIGFuIEludm9rZUVuZC5cbiAgICAgKiBAZXhwb3J0cyBJSW52b2tlRW5kXG4gICAgICogQGludGVyZmFjZSBJSW52b2tlRW5kXG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8bnVsbH0gW2ludm9rZUlkXSBJbnZva2VFbmQgaW52b2tlSWRcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbc3RhdHVzXSBJbnZva2VFbmQgc3RhdHVzXG4gICAgICogQHByb3BlcnR5IHtzdHJpbmd8bnVsbH0gW2Vycm9yXSBJbnZva2VFbmQgZXJyb3JcbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIENvbnN0cnVjdHMgYSBuZXcgSW52b2tlRW5kLlxuICAgICAqIEBleHBvcnRzIEludm9rZUVuZFxuICAgICAqIEBjbGFzc2Rlc2MgUmVwcmVzZW50cyBhbiBJbnZva2VFbmQuXG4gICAgICogQGltcGxlbWVudHMgSUludm9rZUVuZFxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7SUludm9rZUVuZD19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqL1xuICAgIGZ1bmN0aW9uIEludm9rZUVuZChwcm9wZXJ0aWVzKSB7XG4gICAgICAgIGlmIChwcm9wZXJ0aWVzKVxuICAgICAgICAgICAgZm9yICh2YXIga2V5cyA9IE9iamVjdC5rZXlzKHByb3BlcnRpZXMpLCBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpXG4gICAgICAgICAgICAgICAgaWYgKHByb3BlcnRpZXNba2V5c1tpXV0gIT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgdGhpc1trZXlzW2ldXSA9IHByb3BlcnRpZXNba2V5c1tpXV07XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW52b2tlRW5kIGludm9rZUlkLlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gaW52b2tlSWRcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlRW5kXG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgSW52b2tlRW5kLnByb3RvdHlwZS5pbnZva2VJZCA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBJbnZva2VFbmQgc3RhdHVzLlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gc3RhdHVzXG4gICAgICogQG1lbWJlcm9mIEludm9rZUVuZFxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqL1xuICAgIEludm9rZUVuZC5wcm90b3R5cGUuc3RhdHVzID0gMDtcblxuICAgIC8qKlxuICAgICAqIEludm9rZUVuZCBlcnJvci5cbiAgICAgKiBAbWVtYmVyIHtzdHJpbmd9IGVycm9yXG4gICAgICogQG1lbWJlcm9mIEludm9rZUVuZFxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqL1xuICAgIEludm9rZUVuZC5wcm90b3R5cGUuZXJyb3IgPSBcIlwiO1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBJbnZva2VFbmQgaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAqIEBmdW5jdGlvbiBjcmVhdGVcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlRW5kXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUludm9rZUVuZD19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqIEByZXR1cm5zIHtJbnZva2VFbmR9IEludm9rZUVuZCBpbnN0YW5jZVxuICAgICAqL1xuICAgIEludm9rZUVuZC5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUocHJvcGVydGllcykge1xuICAgICAgICByZXR1cm4gbmV3IEludm9rZUVuZChwcm9wZXJ0aWVzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIEludm9rZUVuZCBtZXNzYWdlLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBJbnZva2VFbmQudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZVxuICAgICAqIEBtZW1iZXJvZiBJbnZva2VFbmRcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJSW52b2tlRW5kfSBtZXNzYWdlIEludm9rZUVuZCBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgSW52b2tlRW5kLmVuY29kZSA9IGZ1bmN0aW9uIGVuY29kZShtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgaWYgKCF3cml0ZXIpXG4gICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICBpZiAobWVzc2FnZS5pbnZva2VJZCAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwiaW52b2tlSWRcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDEsIHdpcmVUeXBlIDAgPSovOCkuaW50MzIobWVzc2FnZS5pbnZva2VJZCk7XG4gICAgICAgIGlmIChtZXNzYWdlLnN0YXR1cyAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwic3RhdHVzXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAyLCB3aXJlVHlwZSAwID0qLzE2KS5pbnQzMihtZXNzYWdlLnN0YXR1cyk7XG4gICAgICAgIGlmIChtZXNzYWdlLmVycm9yICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJlcnJvclwiKSlcbiAgICAgICAgICAgIHdyaXRlci51aW50MzIoLyogaWQgMywgd2lyZVR5cGUgMiA9Ki8yNikuc3RyaW5nKG1lc3NhZ2UuZXJyb3IpO1xuICAgICAgICByZXR1cm4gd3JpdGVyO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgSW52b2tlRW5kIG1lc3NhZ2UsIGxlbmd0aCBkZWxpbWl0ZWQuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIEludm9rZUVuZC52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIEludm9rZUVuZFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lJbnZva2VFbmR9IG1lc3NhZ2UgSW52b2tlRW5kIG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBJbnZva2VFbmQuZW5jb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5lbmNvZGUobWVzc2FnZSwgd3JpdGVyKS5sZGVsaW0oKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhbiBJbnZva2VFbmQgbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlci5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlXG4gICAgICogQG1lbWJlcm9mIEludm9rZUVuZFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtJbnZva2VFbmR9IEludm9rZUVuZFxuICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICogQHRocm93cyB7JHByb3RvYnVmLnV0aWwuUHJvdG9jb2xFcnJvcn0gSWYgcmVxdWlyZWQgZmllbGRzIGFyZSBtaXNzaW5nXG4gICAgICovXG4gICAgSW52b2tlRW5kLmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290Lkludm9rZUVuZCgpO1xuICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZCkge1xuICAgICAgICAgICAgdmFyIHRhZyA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5pbnZva2VJZCA9IHJlYWRlci5pbnQzMigpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2Uuc3RhdHVzID0gcmVhZGVyLmludDMyKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5lcnJvciA9IHJlYWRlci5zdHJpbmcoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGFuIEludm9rZUVuZCBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLCBsZW5ndGggZGVsaW1pdGVkLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVEZWxpbWl0ZWRcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlRW5kXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtJbnZva2VFbmR9IEludm9rZUVuZFxuICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICogQHRocm93cyB7JHByb3RvYnVmLnV0aWwuUHJvdG9jb2xFcnJvcn0gSWYgcmVxdWlyZWQgZmllbGRzIGFyZSBtaXNzaW5nXG4gICAgICovXG4gICAgSW52b2tlRW5kLmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhbiBJbnZva2VFbmQgbWVzc2FnZS5cbiAgICAgKiBAZnVuY3Rpb24gdmVyaWZ5XG4gICAgICogQG1lbWJlcm9mIEludm9rZUVuZFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBtZXNzYWdlIFBsYWluIG9iamVjdCB0byB2ZXJpZnlcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfG51bGx9IGBudWxsYCBpZiB2YWxpZCwgb3RoZXJ3aXNlIHRoZSByZWFzb24gd2h5IGl0IGlzIG5vdFxuICAgICAqL1xuICAgIEludm9rZUVuZC52ZXJpZnkgPSBmdW5jdGlvbiB2ZXJpZnkobWVzc2FnZSkge1xuICAgICAgICBpZiAodHlwZW9mIG1lc3NhZ2UgIT09IFwib2JqZWN0XCIgfHwgbWVzc2FnZSA9PT0gbnVsbClcbiAgICAgICAgICAgIHJldHVybiBcIm9iamVjdCBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5pbnZva2VJZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJpbnZva2VJZFwiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UuaW52b2tlSWQpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcImludm9rZUlkOiBpbnRlZ2VyIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLnN0YXR1cyAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJzdGF0dXNcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLnN0YXR1cykpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwic3RhdHVzOiBpbnRlZ2VyIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLmVycm9yICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImVycm9yXCIpKVxuICAgICAgICAgICAgaWYgKCEkdXRpbC5pc1N0cmluZyhtZXNzYWdlLmVycm9yKSlcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJlcnJvcjogc3RyaW5nIGV4cGVjdGVkXCI7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGFuIEludm9rZUVuZCBtZXNzYWdlIGZyb20gYSBwbGFpbiBvYmplY3QuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIHRoZWlyIHJlc3BlY3RpdmUgaW50ZXJuYWwgdHlwZXMuXG4gICAgICogQGZ1bmN0aW9uIGZyb21PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlRW5kXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7T2JqZWN0LjxzdHJpbmcsKj59IG9iamVjdCBQbGFpbiBvYmplY3RcbiAgICAgKiBAcmV0dXJucyB7SW52b2tlRW5kfSBJbnZva2VFbmRcbiAgICAgKi9cbiAgICBJbnZva2VFbmQuZnJvbU9iamVjdCA9IGZ1bmN0aW9uIGZyb21PYmplY3Qob2JqZWN0KSB7XG4gICAgICAgIGlmIChvYmplY3QgaW5zdGFuY2VvZiAkcm9vdC5JbnZva2VFbmQpXG4gICAgICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyAkcm9vdC5JbnZva2VFbmQoKTtcbiAgICAgICAgaWYgKG9iamVjdC5pbnZva2VJZCAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5pbnZva2VJZCA9IG9iamVjdC5pbnZva2VJZCB8IDA7XG4gICAgICAgIGlmIChvYmplY3Quc3RhdHVzICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLnN0YXR1cyA9IG9iamVjdC5zdGF0dXMgfCAwO1xuICAgICAgICBpZiAob2JqZWN0LmVycm9yICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLmVycm9yID0gU3RyaW5nKG9iamVjdC5lcnJvcik7XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgcGxhaW4gb2JqZWN0IGZyb20gYW4gSW52b2tlRW5kIG1lc3NhZ2UuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIG90aGVyIHR5cGVzIGlmIHNwZWNpZmllZC5cbiAgICAgKiBAZnVuY3Rpb24gdG9PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgSW52b2tlRW5kXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SW52b2tlRW5kfSBtZXNzYWdlIEludm9rZUVuZFxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLklDb252ZXJzaW9uT3B0aW9uc30gW29wdGlvbnNdIENvbnZlcnNpb24gb3B0aW9uc1xuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gUGxhaW4gb2JqZWN0XG4gICAgICovXG4gICAgSW52b2tlRW5kLnRvT2JqZWN0ID0gZnVuY3Rpb24gdG9PYmplY3QobWVzc2FnZSwgb3B0aW9ucykge1xuICAgICAgICBpZiAoIW9wdGlvbnMpXG4gICAgICAgICAgICBvcHRpb25zID0ge307XG4gICAgICAgIHZhciBvYmplY3QgPSB7fTtcbiAgICAgICAgaWYgKG9wdGlvbnMuZGVmYXVsdHMpIHtcbiAgICAgICAgICAgIG9iamVjdC5pbnZva2VJZCA9IDA7XG4gICAgICAgICAgICBvYmplY3Quc3RhdHVzID0gMDtcbiAgICAgICAgICAgIG9iamVjdC5lcnJvciA9IFwiXCI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1lc3NhZ2UuaW52b2tlSWQgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiaW52b2tlSWRcIikpXG4gICAgICAgICAgICBvYmplY3QuaW52b2tlSWQgPSBtZXNzYWdlLmludm9rZUlkO1xuICAgICAgICBpZiAobWVzc2FnZS5zdGF0dXMgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwic3RhdHVzXCIpKVxuICAgICAgICAgICAgb2JqZWN0LnN0YXR1cyA9IG1lc3NhZ2Uuc3RhdHVzO1xuICAgICAgICBpZiAobWVzc2FnZS5lcnJvciAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJlcnJvclwiKSlcbiAgICAgICAgICAgIG9iamVjdC5lcnJvciA9IG1lc3NhZ2UuZXJyb3I7XG4gICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENvbnZlcnRzIHRoaXMgSW52b2tlRW5kIHRvIEpTT04uXG4gICAgICogQGZ1bmN0aW9uIHRvSlNPTlxuICAgICAqIEBtZW1iZXJvZiBJbnZva2VFbmRcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IEpTT04gb2JqZWN0XG4gICAgICovXG4gICAgSW52b2tlRW5kLnByb3RvdHlwZS50b0pTT04gPSBmdW5jdGlvbiB0b0pTT04oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbnN0cnVjdG9yLnRvT2JqZWN0KHRoaXMsICRwcm90b2J1Zi51dGlsLnRvSlNPTk9wdGlvbnMpO1xuICAgIH07XG5cbiAgICByZXR1cm4gSW52b2tlRW5kO1xufSkoKTtcblxuJHJvb3QuQ2xvc2VDb25uZWN0aW9uID0gKGZ1bmN0aW9uKCkge1xuXG4gICAgLyoqXG4gICAgICogUHJvcGVydGllcyBvZiBhIENsb3NlQ29ubmVjdGlvbi5cbiAgICAgKiBAZXhwb3J0cyBJQ2xvc2VDb25uZWN0aW9uXG4gICAgICogQGludGVyZmFjZSBJQ2xvc2VDb25uZWN0aW9uXG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8bnVsbH0gW2NvZGVdIENsb3NlQ29ubmVjdGlvbiBjb2RlXG4gICAgICogQHByb3BlcnR5IHtzdHJpbmd8bnVsbH0gW3JlYXNvbl0gQ2xvc2VDb25uZWN0aW9uIHJlYXNvblxuICAgICAqIEBwcm9wZXJ0eSB7Ym9vbGVhbnxudWxsfSBbYWxsb3dSZWNvbm5lY3RdIENsb3NlQ29ubmVjdGlvbiBhbGxvd1JlY29ubmVjdFxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ29uc3RydWN0cyBhIG5ldyBDbG9zZUNvbm5lY3Rpb24uXG4gICAgICogQGV4cG9ydHMgQ2xvc2VDb25uZWN0aW9uXG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgQ2xvc2VDb25uZWN0aW9uLlxuICAgICAqIEBpbXBsZW1lbnRzIElDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0ge0lDbG9zZUNvbm5lY3Rpb249fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBDbG9zZUNvbm5lY3Rpb24ocHJvcGVydGllcykge1xuICAgICAgICBpZiAocHJvcGVydGllcylcbiAgICAgICAgICAgIGZvciAodmFyIGtleXMgPSBPYmplY3Qua2V5cyhwcm9wZXJ0aWVzKSwgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgIGlmIChwcm9wZXJ0aWVzW2tleXNbaV1dICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXNba2V5c1tpXV0gPSBwcm9wZXJ0aWVzW2tleXNbaV1dO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENsb3NlQ29ubmVjdGlvbiBjb2RlLlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gY29kZVxuICAgICAqIEBtZW1iZXJvZiBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBDbG9zZUNvbm5lY3Rpb24ucHJvdG90eXBlLmNvZGUgPSAwO1xuXG4gICAgLyoqXG4gICAgICogQ2xvc2VDb25uZWN0aW9uIHJlYXNvbi5cbiAgICAgKiBAbWVtYmVyIHtzdHJpbmd9IHJlYXNvblxuICAgICAqIEBtZW1iZXJvZiBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBDbG9zZUNvbm5lY3Rpb24ucHJvdG90eXBlLnJlYXNvbiA9IFwiXCI7XG5cbiAgICAvKipcbiAgICAgKiBDbG9zZUNvbm5lY3Rpb24gYWxsb3dSZWNvbm5lY3QuXG4gICAgICogQG1lbWJlciB7Ym9vbGVhbn0gYWxsb3dSZWNvbm5lY3RcbiAgICAgKiBAbWVtYmVyb2YgQ2xvc2VDb25uZWN0aW9uXG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgQ2xvc2VDb25uZWN0aW9uLnByb3RvdHlwZS5hbGxvd1JlY29ubmVjdCA9IGZhbHNlO1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBDbG9zZUNvbm5lY3Rpb24gaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAqIEBmdW5jdGlvbiBjcmVhdGVcbiAgICAgKiBAbWVtYmVyb2YgQ2xvc2VDb25uZWN0aW9uXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUNsb3NlQ29ubmVjdGlvbj19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqIEByZXR1cm5zIHtDbG9zZUNvbm5lY3Rpb259IENsb3NlQ29ubmVjdGlvbiBpbnN0YW5jZVxuICAgICAqL1xuICAgIENsb3NlQ29ubmVjdGlvbi5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUocHJvcGVydGllcykge1xuICAgICAgICByZXR1cm4gbmV3IENsb3NlQ29ubmVjdGlvbihwcm9wZXJ0aWVzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIENsb3NlQ29ubmVjdGlvbiBtZXNzYWdlLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBDbG9zZUNvbm5lY3Rpb24udmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZVxuICAgICAqIEBtZW1iZXJvZiBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJQ2xvc2VDb25uZWN0aW9ufSBtZXNzYWdlIENsb3NlQ29ubmVjdGlvbiBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgQ2xvc2VDb25uZWN0aW9uLmVuY29kZSA9IGZ1bmN0aW9uIGVuY29kZShtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgaWYgKCF3cml0ZXIpXG4gICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICBpZiAobWVzc2FnZS5jb2RlICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJjb2RlXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAxLCB3aXJlVHlwZSAwID0qLzgpLmludDMyKG1lc3NhZ2UuY29kZSk7XG4gICAgICAgIGlmIChtZXNzYWdlLnJlYXNvbiAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwicmVhc29uXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAyLCB3aXJlVHlwZSAyID0qLzE4KS5zdHJpbmcobWVzc2FnZS5yZWFzb24pO1xuICAgICAgICBpZiAobWVzc2FnZS5hbGxvd1JlY29ubmVjdCAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwiYWxsb3dSZWNvbm5lY3RcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDMsIHdpcmVUeXBlIDAgPSovMjQpLmJvb2wobWVzc2FnZS5hbGxvd1JlY29ubmVjdCk7XG4gICAgICAgIHJldHVybiB3cml0ZXI7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEVuY29kZXMgdGhlIHNwZWNpZmllZCBDbG9zZUNvbm5lY3Rpb24gbWVzc2FnZSwgbGVuZ3RoIGRlbGltaXRlZC4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgQ2xvc2VDb25uZWN0aW9uLnZlcmlmeXx2ZXJpZnl9IG1lc3NhZ2VzLlxuICAgICAqIEBmdW5jdGlvbiBlbmNvZGVEZWxpbWl0ZWRcbiAgICAgKiBAbWVtYmVyb2YgQ2xvc2VDb25uZWN0aW9uXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUNsb3NlQ29ubmVjdGlvbn0gbWVzc2FnZSBDbG9zZUNvbm5lY3Rpb24gbWVzc2FnZSBvciBwbGFpbiBvYmplY3QgdG8gZW5jb2RlXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuV3JpdGVyfSBbd3JpdGVyXSBXcml0ZXIgdG8gZW5jb2RlIHRvXG4gICAgICogQHJldHVybnMgeyRwcm90b2J1Zi5Xcml0ZXJ9IFdyaXRlclxuICAgICAqL1xuICAgIENsb3NlQ29ubmVjdGlvbi5lbmNvZGVEZWxpbWl0ZWQgPSBmdW5jdGlvbiBlbmNvZGVEZWxpbWl0ZWQobWVzc2FnZSwgd3JpdGVyKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmVuY29kZShtZXNzYWdlLCB3cml0ZXIpLmxkZWxpbSgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgQ2xvc2VDb25uZWN0aW9uIG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZVxuICAgICAqIEBtZW1iZXJvZiBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUmVhZGVyfFVpbnQ4QXJyYXl9IHJlYWRlciBSZWFkZXIgb3IgYnVmZmVyIHRvIGRlY29kZSBmcm9tXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFtsZW5ndGhdIE1lc3NhZ2UgbGVuZ3RoIGlmIGtub3duIGJlZm9yZWhhbmRcbiAgICAgKiBAcmV0dXJucyB7Q2xvc2VDb25uZWN0aW9ufSBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIENsb3NlQ29ubmVjdGlvbi5kZWNvZGUgPSBmdW5jdGlvbiBkZWNvZGUocmVhZGVyLCBsZW5ndGgpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSAkUmVhZGVyLmNyZWF0ZShyZWFkZXIpO1xuICAgICAgICB2YXIgZW5kID0gbGVuZ3RoID09PSB1bmRlZmluZWQgPyByZWFkZXIubGVuIDogcmVhZGVyLnBvcyArIGxlbmd0aCwgbWVzc2FnZSA9IG5ldyAkcm9vdC5DbG9zZUNvbm5lY3Rpb24oKTtcbiAgICAgICAgd2hpbGUgKHJlYWRlci5wb3MgPCBlbmQpIHtcbiAgICAgICAgICAgIHZhciB0YWcgPSByZWFkZXIudWludDMyKCk7XG4gICAgICAgICAgICBzd2l0Y2ggKHRhZyA+Pj4gMykge1xuICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UuY29kZSA9IHJlYWRlci5pbnQzMigpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UucmVhc29uID0gcmVhZGVyLnN0cmluZygpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAzOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UuYWxsb3dSZWNvbm5lY3QgPSByZWFkZXIuYm9vbCgpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZWFkZXIuc2tpcFR5cGUodGFnICYgNyk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBDbG9zZUNvbm5lY3Rpb24gbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlciwgbGVuZ3RoIGRlbGltaXRlZC5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIENsb3NlQ29ubmVjdGlvblxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcmV0dXJucyB7Q2xvc2VDb25uZWN0aW9ufSBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIENsb3NlQ29ubmVjdGlvbi5kZWNvZGVEZWxpbWl0ZWQgPSBmdW5jdGlvbiBkZWNvZGVEZWxpbWl0ZWQocmVhZGVyKSB7XG4gICAgICAgIGlmICghKHJlYWRlciBpbnN0YW5jZW9mICRSZWFkZXIpKVxuICAgICAgICAgICAgcmVhZGVyID0gbmV3ICRSZWFkZXIocmVhZGVyKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVjb2RlKHJlYWRlciwgcmVhZGVyLnVpbnQzMigpKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVmVyaWZpZXMgYSBDbG9zZUNvbm5lY3Rpb24gbWVzc2FnZS5cbiAgICAgKiBAZnVuY3Rpb24gdmVyaWZ5XG4gICAgICogQG1lbWJlcm9mIENsb3NlQ29ubmVjdGlvblxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBtZXNzYWdlIFBsYWluIG9iamVjdCB0byB2ZXJpZnlcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfG51bGx9IGBudWxsYCBpZiB2YWxpZCwgb3RoZXJ3aXNlIHRoZSByZWFzb24gd2h5IGl0IGlzIG5vdFxuICAgICAqL1xuICAgIENsb3NlQ29ubmVjdGlvbi52ZXJpZnkgPSBmdW5jdGlvbiB2ZXJpZnkobWVzc2FnZSkge1xuICAgICAgICBpZiAodHlwZW9mIG1lc3NhZ2UgIT09IFwib2JqZWN0XCIgfHwgbWVzc2FnZSA9PT0gbnVsbClcbiAgICAgICAgICAgIHJldHVybiBcIm9iamVjdCBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5jb2RlICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImNvZGVcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLmNvZGUpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcImNvZGU6IGludGVnZXIgZXhwZWN0ZWRcIjtcbiAgICAgICAgaWYgKG1lc3NhZ2UucmVhc29uICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInJlYXNvblwiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNTdHJpbmcobWVzc2FnZS5yZWFzb24pKVxuICAgICAgICAgICAgICAgIHJldHVybiBcInJlYXNvbjogc3RyaW5nIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLmFsbG93UmVjb25uZWN0ICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImFsbG93UmVjb25uZWN0XCIpKVxuICAgICAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlLmFsbG93UmVjb25uZWN0ICE9PSBcImJvb2xlYW5cIilcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJhbGxvd1JlY29ubmVjdDogYm9vbGVhbiBleHBlY3RlZFwiO1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIENsb3NlQ29ubmVjdGlvbiBtZXNzYWdlIGZyb20gYSBwbGFpbiBvYmplY3QuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIHRoZWlyIHJlc3BlY3RpdmUgaW50ZXJuYWwgdHlwZXMuXG4gICAgICogQGZ1bmN0aW9uIGZyb21PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgQ2xvc2VDb25uZWN0aW9uXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7T2JqZWN0LjxzdHJpbmcsKj59IG9iamVjdCBQbGFpbiBvYmplY3RcbiAgICAgKiBAcmV0dXJucyB7Q2xvc2VDb25uZWN0aW9ufSBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKi9cbiAgICBDbG9zZUNvbm5lY3Rpb24uZnJvbU9iamVjdCA9IGZ1bmN0aW9uIGZyb21PYmplY3Qob2JqZWN0KSB7XG4gICAgICAgIGlmIChvYmplY3QgaW5zdGFuY2VvZiAkcm9vdC5DbG9zZUNvbm5lY3Rpb24pXG4gICAgICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyAkcm9vdC5DbG9zZUNvbm5lY3Rpb24oKTtcbiAgICAgICAgaWYgKG9iamVjdC5jb2RlICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLmNvZGUgPSBvYmplY3QuY29kZSB8IDA7XG4gICAgICAgIGlmIChvYmplY3QucmVhc29uICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLnJlYXNvbiA9IFN0cmluZyhvYmplY3QucmVhc29uKTtcbiAgICAgICAgaWYgKG9iamVjdC5hbGxvd1JlY29ubmVjdCAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5hbGxvd1JlY29ubmVjdCA9IEJvb2xlYW4ob2JqZWN0LmFsbG93UmVjb25uZWN0KTtcbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBwbGFpbiBvYmplY3QgZnJvbSBhIENsb3NlQ29ubmVjdGlvbiBtZXNzYWdlLiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byBvdGhlciB0eXBlcyBpZiBzcGVjaWZpZWQuXG4gICAgICogQGZ1bmN0aW9uIHRvT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIENsb3NlQ29ubmVjdGlvblxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0Nsb3NlQ29ubmVjdGlvbn0gbWVzc2FnZSBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5JQ29udmVyc2lvbk9wdGlvbnN9IFtvcHRpb25zXSBDb252ZXJzaW9uIG9wdGlvbnNcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IFBsYWluIG9iamVjdFxuICAgICAqL1xuICAgIENsb3NlQ29ubmVjdGlvbi50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG1lc3NhZ2UsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgICB2YXIgb2JqZWN0ID0ge307XG4gICAgICAgIGlmIChvcHRpb25zLmRlZmF1bHRzKSB7XG4gICAgICAgICAgICBvYmplY3QuY29kZSA9IDA7XG4gICAgICAgICAgICBvYmplY3QucmVhc29uID0gXCJcIjtcbiAgICAgICAgICAgIG9iamVjdC5hbGxvd1JlY29ubmVjdCA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChtZXNzYWdlLmNvZGUgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiY29kZVwiKSlcbiAgICAgICAgICAgIG9iamVjdC5jb2RlID0gbWVzc2FnZS5jb2RlO1xuICAgICAgICBpZiAobWVzc2FnZS5yZWFzb24gIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwicmVhc29uXCIpKVxuICAgICAgICAgICAgb2JqZWN0LnJlYXNvbiA9IG1lc3NhZ2UucmVhc29uO1xuICAgICAgICBpZiAobWVzc2FnZS5hbGxvd1JlY29ubmVjdCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJhbGxvd1JlY29ubmVjdFwiKSlcbiAgICAgICAgICAgIG9iamVjdC5hbGxvd1JlY29ubmVjdCA9IG1lc3NhZ2UuYWxsb3dSZWNvbm5lY3Q7XG4gICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENvbnZlcnRzIHRoaXMgQ2xvc2VDb25uZWN0aW9uIHRvIEpTT04uXG4gICAgICogQGZ1bmN0aW9uIHRvSlNPTlxuICAgICAqIEBtZW1iZXJvZiBDbG9zZUNvbm5lY3Rpb25cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IEpTT04gb2JqZWN0XG4gICAgICovXG4gICAgQ2xvc2VDb25uZWN0aW9uLnByb3RvdHlwZS50b0pTT04gPSBmdW5jdGlvbiB0b0pTT04oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbnN0cnVjdG9yLnRvT2JqZWN0KHRoaXMsICRwcm90b2J1Zi51dGlsLnRvSlNPTk9wdGlvbnMpO1xuICAgIH07XG5cbiAgICByZXR1cm4gQ2xvc2VDb25uZWN0aW9uO1xufSkoKTtcblxuJHJvb3QuRW52ZWxvcGUgPSAoZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBQcm9wZXJ0aWVzIG9mIGFuIEVudmVsb3BlLlxuICAgICAqIEBleHBvcnRzIElFbnZlbG9wZVxuICAgICAqIEBpbnRlcmZhY2UgSUVudmVsb3BlXG4gICAgICogQHByb3BlcnR5IHtFbnZlbG9wZS5JSGVhZGVyfG51bGx9IFtoZWFkZXJdIEVudmVsb3BlIGhlYWRlclxuICAgICAqIEBwcm9wZXJ0eSB7VWludDhBcnJheXxudWxsfSBbcGF5bG9hZF0gRW52ZWxvcGUgcGF5bG9hZFxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ29uc3RydWN0cyBhIG5ldyBFbnZlbG9wZS5cbiAgICAgKiBAZXhwb3J0cyBFbnZlbG9wZVxuICAgICAqIEBjbGFzc2Rlc2MgUmVwcmVzZW50cyBhbiBFbnZlbG9wZS5cbiAgICAgKiBAaW1wbGVtZW50cyBJRW52ZWxvcGVcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0ge0lFbnZlbG9wZT19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqL1xuICAgIGZ1bmN0aW9uIEVudmVsb3BlKHByb3BlcnRpZXMpIHtcbiAgICAgICAgaWYgKHByb3BlcnRpZXMpXG4gICAgICAgICAgICBmb3IgKHZhciBrZXlzID0gT2JqZWN0LmtleXMocHJvcGVydGllcyksIGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7ICsraSlcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydGllc1trZXlzW2ldXSAhPSBudWxsKVxuICAgICAgICAgICAgICAgICAgICB0aGlzW2tleXNbaV1dID0gcHJvcGVydGllc1trZXlzW2ldXTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBFbnZlbG9wZSBoZWFkZXIuXG4gICAgICogQG1lbWJlciB7RW52ZWxvcGUuSUhlYWRlcnxudWxsfHVuZGVmaW5lZH0gaGVhZGVyXG4gICAgICogQG1lbWJlcm9mIEVudmVsb3BlXG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgRW52ZWxvcGUucHJvdG90eXBlLmhlYWRlciA9IG51bGw7XG5cbiAgICAvKipcbiAgICAgKiBFbnZlbG9wZSBwYXlsb2FkLlxuICAgICAqIEBtZW1iZXIge1VpbnQ4QXJyYXl9IHBheWxvYWRcbiAgICAgKiBAbWVtYmVyb2YgRW52ZWxvcGVcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBFbnZlbG9wZS5wcm90b3R5cGUucGF5bG9hZCA9ICR1dGlsLm5ld0J1ZmZlcihbXSk7XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IEVudmVsb3BlIGluc3RhbmNlIHVzaW5nIHRoZSBzcGVjaWZpZWQgcHJvcGVydGllcy5cbiAgICAgKiBAZnVuY3Rpb24gY3JlYXRlXG4gICAgICogQG1lbWJlcm9mIEVudmVsb3BlXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUVudmVsb3BlPX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICogQHJldHVybnMge0VudmVsb3BlfSBFbnZlbG9wZSBpbnN0YW5jZVxuICAgICAqL1xuICAgIEVudmVsb3BlLmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShwcm9wZXJ0aWVzKSB7XG4gICAgICAgIHJldHVybiBuZXcgRW52ZWxvcGUocHJvcGVydGllcyk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEVuY29kZXMgdGhlIHNwZWNpZmllZCBFbnZlbG9wZSBtZXNzYWdlLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBFbnZlbG9wZS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlXG4gICAgICogQG1lbWJlcm9mIEVudmVsb3BlXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUVudmVsb3BlfSBtZXNzYWdlIEVudmVsb3BlIG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBFbnZlbG9wZS5lbmNvZGUgPSBmdW5jdGlvbiBlbmNvZGUobWVzc2FnZSwgd3JpdGVyKSB7XG4gICAgICAgIGlmICghd3JpdGVyKVxuICAgICAgICAgICAgd3JpdGVyID0gJFdyaXRlci5jcmVhdGUoKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UuaGVhZGVyICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJoZWFkZXJcIikpXG4gICAgICAgICAgICAkcm9vdC5FbnZlbG9wZS5IZWFkZXIuZW5jb2RlKG1lc3NhZ2UuaGVhZGVyLCB3cml0ZXIudWludDMyKC8qIGlkIDEsIHdpcmVUeXBlIDIgPSovMTApLmZvcmsoKSkubGRlbGltKCk7XG4gICAgICAgIGlmIChtZXNzYWdlLnBheWxvYWQgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcInBheWxvYWRcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDIsIHdpcmVUeXBlIDIgPSovMTgpLmJ5dGVzKG1lc3NhZ2UucGF5bG9hZCk7XG4gICAgICAgIHJldHVybiB3cml0ZXI7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEVuY29kZXMgdGhlIHNwZWNpZmllZCBFbnZlbG9wZSBtZXNzYWdlLCBsZW5ndGggZGVsaW1pdGVkLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBFbnZlbG9wZS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIEVudmVsb3BlXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUVudmVsb3BlfSBtZXNzYWdlIEVudmVsb3BlIG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBFbnZlbG9wZS5lbmNvZGVEZWxpbWl0ZWQgPSBmdW5jdGlvbiBlbmNvZGVEZWxpbWl0ZWQobWVzc2FnZSwgd3JpdGVyKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmVuY29kZShtZXNzYWdlLCB3cml0ZXIpLmxkZWxpbSgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGFuIEVudmVsb3BlIG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZVxuICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtFbnZlbG9wZX0gRW52ZWxvcGVcbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIEVudmVsb3BlLmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LkVudmVsb3BlKCk7XG4gICAgICAgIHdoaWxlIChyZWFkZXIucG9zIDwgZW5kKSB7XG4gICAgICAgICAgICB2YXIgdGFnID0gcmVhZGVyLnVpbnQzMigpO1xuICAgICAgICAgICAgc3dpdGNoICh0YWcgPj4+IDMpIHtcbiAgICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmhlYWRlciA9ICRyb290LkVudmVsb3BlLkhlYWRlci5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UucGF5bG9hZCA9IHJlYWRlci5ieXRlcygpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZWFkZXIuc2tpcFR5cGUodGFnICYgNyk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYW4gRW52ZWxvcGUgbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlciwgbGVuZ3RoIGRlbGltaXRlZC5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIEVudmVsb3BlXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtFbnZlbG9wZX0gRW52ZWxvcGVcbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIEVudmVsb3BlLmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhbiBFbnZlbG9wZSBtZXNzYWdlLlxuICAgICAqIEBmdW5jdGlvbiB2ZXJpZnlcbiAgICAgKiBAbWVtYmVyb2YgRW52ZWxvcGVcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBFbnZlbG9wZS52ZXJpZnkgPSBmdW5jdGlvbiB2ZXJpZnkobWVzc2FnZSkge1xuICAgICAgICBpZiAodHlwZW9mIG1lc3NhZ2UgIT09IFwib2JqZWN0XCIgfHwgbWVzc2FnZSA9PT0gbnVsbClcbiAgICAgICAgICAgIHJldHVybiBcIm9iamVjdCBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5oZWFkZXIgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiaGVhZGVyXCIpKSB7XG4gICAgICAgICAgICB2YXIgZXJyb3IgPSAkcm9vdC5FbnZlbG9wZS5IZWFkZXIudmVyaWZ5KG1lc3NhZ2UuaGVhZGVyKTtcbiAgICAgICAgICAgIGlmIChlcnJvcilcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJoZWFkZXIuXCIgKyBlcnJvcjtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWVzc2FnZS5wYXlsb2FkICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInBheWxvYWRcIikpXG4gICAgICAgICAgICBpZiAoIShtZXNzYWdlLnBheWxvYWQgJiYgdHlwZW9mIG1lc3NhZ2UucGF5bG9hZC5sZW5ndGggPT09IFwibnVtYmVyXCIgfHwgJHV0aWwuaXNTdHJpbmcobWVzc2FnZS5wYXlsb2FkKSkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwicGF5bG9hZDogYnVmZmVyIGV4cGVjdGVkXCI7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGFuIEVudmVsb3BlIG1lc3NhZ2UgZnJvbSBhIHBsYWluIG9iamVjdC4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gdGhlaXIgcmVzcGVjdGl2ZSBpbnRlcm5hbCB0eXBlcy5cbiAgICAgKiBAZnVuY3Rpb24gZnJvbU9iamVjdFxuICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBvYmplY3QgUGxhaW4gb2JqZWN0XG4gICAgICogQHJldHVybnMge0VudmVsb3BlfSBFbnZlbG9wZVxuICAgICAqL1xuICAgIEVudmVsb3BlLmZyb21PYmplY3QgPSBmdW5jdGlvbiBmcm9tT2JqZWN0KG9iamVjdCkge1xuICAgICAgICBpZiAob2JqZWN0IGluc3RhbmNlb2YgJHJvb3QuRW52ZWxvcGUpXG4gICAgICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyAkcm9vdC5FbnZlbG9wZSgpO1xuICAgICAgICBpZiAob2JqZWN0LmhlYWRlciAhPSBudWxsKSB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIG9iamVjdC5oZWFkZXIgIT09IFwib2JqZWN0XCIpXG4gICAgICAgICAgICAgICAgdGhyb3cgVHlwZUVycm9yKFwiLkVudmVsb3BlLmhlYWRlcjogb2JqZWN0IGV4cGVjdGVkXCIpO1xuICAgICAgICAgICAgbWVzc2FnZS5oZWFkZXIgPSAkcm9vdC5FbnZlbG9wZS5IZWFkZXIuZnJvbU9iamVjdChvYmplY3QuaGVhZGVyKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAob2JqZWN0LnBheWxvYWQgIT0gbnVsbClcbiAgICAgICAgICAgIGlmICh0eXBlb2Ygb2JqZWN0LnBheWxvYWQgPT09IFwic3RyaW5nXCIpXG4gICAgICAgICAgICAgICAgJHV0aWwuYmFzZTY0LmRlY29kZShvYmplY3QucGF5bG9hZCwgbWVzc2FnZS5wYXlsb2FkID0gJHV0aWwubmV3QnVmZmVyKCR1dGlsLmJhc2U2NC5sZW5ndGgob2JqZWN0LnBheWxvYWQpKSwgMCk7XG4gICAgICAgICAgICBlbHNlIGlmIChvYmplY3QucGF5bG9hZC5sZW5ndGgpXG4gICAgICAgICAgICAgICAgbWVzc2FnZS5wYXlsb2FkID0gb2JqZWN0LnBheWxvYWQ7XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgcGxhaW4gb2JqZWN0IGZyb20gYW4gRW52ZWxvcGUgbWVzc2FnZS4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gb3RoZXIgdHlwZXMgaWYgc3BlY2lmaWVkLlxuICAgICAqIEBmdW5jdGlvbiB0b09iamVjdFxuICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0VudmVsb3BlfSBtZXNzYWdlIEVudmVsb3BlXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuSUNvbnZlcnNpb25PcHRpb25zfSBbb3B0aW9uc10gQ29udmVyc2lvbiBvcHRpb25zXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBQbGFpbiBvYmplY3RcbiAgICAgKi9cbiAgICBFbnZlbG9wZS50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG1lc3NhZ2UsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgICB2YXIgb2JqZWN0ID0ge307XG4gICAgICAgIGlmIChvcHRpb25zLmRlZmF1bHRzKSB7XG4gICAgICAgICAgICBvYmplY3QuaGVhZGVyID0gbnVsbDtcbiAgICAgICAgICAgIGlmIChvcHRpb25zLmJ5dGVzID09PSBTdHJpbmcpXG4gICAgICAgICAgICAgICAgb2JqZWN0LnBheWxvYWQgPSBcIlwiO1xuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0LnBheWxvYWQgPSBbXTtcbiAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5ieXRlcyAhPT0gQXJyYXkpXG4gICAgICAgICAgICAgICAgICAgIG9iamVjdC5wYXlsb2FkID0gJHV0aWwubmV3QnVmZmVyKG9iamVjdC5wYXlsb2FkKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpZiAobWVzc2FnZS5oZWFkZXIgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiaGVhZGVyXCIpKVxuICAgICAgICAgICAgb2JqZWN0LmhlYWRlciA9ICRyb290LkVudmVsb3BlLkhlYWRlci50b09iamVjdChtZXNzYWdlLmhlYWRlciwgb3B0aW9ucyk7XG4gICAgICAgIGlmIChtZXNzYWdlLnBheWxvYWQgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwicGF5bG9hZFwiKSlcbiAgICAgICAgICAgIG9iamVjdC5wYXlsb2FkID0gb3B0aW9ucy5ieXRlcyA9PT0gU3RyaW5nID8gJHV0aWwuYmFzZTY0LmVuY29kZShtZXNzYWdlLnBheWxvYWQsIDAsIG1lc3NhZ2UucGF5bG9hZC5sZW5ndGgpIDogb3B0aW9ucy5ieXRlcyA9PT0gQXJyYXkgPyBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChtZXNzYWdlLnBheWxvYWQpIDogbWVzc2FnZS5wYXlsb2FkO1xuICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGlzIEVudmVsb3BlIHRvIEpTT04uXG4gICAgICogQGZ1bmN0aW9uIHRvSlNPTlxuICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZVxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gSlNPTiBvYmplY3RcbiAgICAgKi9cbiAgICBFbnZlbG9wZS5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci50b09iamVjdCh0aGlzLCAkcHJvdG9idWYudXRpbC50b0pTT05PcHRpb25zKTtcbiAgICB9O1xuXG4gICAgRW52ZWxvcGUuSGVhZGVyID0gKGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBQcm9wZXJ0aWVzIG9mIGEgSGVhZGVyLlxuICAgICAgICAgKiBAbWVtYmVyb2YgRW52ZWxvcGVcbiAgICAgICAgICogQGludGVyZmFjZSBJSGVhZGVyXG4gICAgICAgICAqIEBwcm9wZXJ0eSB7SUludm9rZUJlZ2lufG51bGx9IFtpbnZva2VCZWdpbl0gSGVhZGVyIGludm9rZUJlZ2luXG4gICAgICAgICAqIEBwcm9wZXJ0eSB7SUludm9rZUVuZHxudWxsfSBbaW52b2tlRW5kXSBIZWFkZXIgaW52b2tlRW5kXG4gICAgICAgICAqIEBwcm9wZXJ0eSB7SUNsb3NlQ29ubmVjdGlvbnxudWxsfSBbY2xvc2VDb25uZWN0aW9uXSBIZWFkZXIgY2xvc2VDb25uZWN0aW9uXG4gICAgICAgICAqIEBwcm9wZXJ0eSB7c3RyaW5nfG51bGx9IFtldmVudF0gSGVhZGVyIGV2ZW50XG4gICAgICAgICAqL1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBDb25zdHJ1Y3RzIGEgbmV3IEhlYWRlci5cbiAgICAgICAgICogQG1lbWJlcm9mIEVudmVsb3BlXG4gICAgICAgICAqIEBjbGFzc2Rlc2MgUmVwcmVzZW50cyBhIEhlYWRlci5cbiAgICAgICAgICogQGltcGxlbWVudHMgSUhlYWRlclxuICAgICAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgICAgICogQHBhcmFtIHtFbnZlbG9wZS5JSGVhZGVyPX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiBIZWFkZXIocHJvcGVydGllcykge1xuICAgICAgICAgICAgaWYgKHByb3BlcnRpZXMpXG4gICAgICAgICAgICAgICAgZm9yICh2YXIga2V5cyA9IE9iamVjdC5rZXlzKHByb3BlcnRpZXMpLCBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpXG4gICAgICAgICAgICAgICAgICAgIGlmIChwcm9wZXJ0aWVzW2tleXNbaV1dICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzW2tleXNbaV1dID0gcHJvcGVydGllc1trZXlzW2ldXTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBIZWFkZXIgaW52b2tlQmVnaW4uXG4gICAgICAgICAqIEBtZW1iZXIge0lJbnZva2VCZWdpbnxudWxsfHVuZGVmaW5lZH0gaW52b2tlQmVnaW5cbiAgICAgICAgICogQG1lbWJlcm9mIEVudmVsb3BlLkhlYWRlclxuICAgICAgICAgKiBAaW5zdGFuY2VcbiAgICAgICAgICovXG4gICAgICAgIEhlYWRlci5wcm90b3R5cGUuaW52b2tlQmVnaW4gPSBudWxsO1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBIZWFkZXIgaW52b2tlRW5kLlxuICAgICAgICAgKiBAbWVtYmVyIHtJSW52b2tlRW5kfG51bGx8dW5kZWZpbmVkfSBpbnZva2VFbmRcbiAgICAgICAgICogQG1lbWJlcm9mIEVudmVsb3BlLkhlYWRlclxuICAgICAgICAgKiBAaW5zdGFuY2VcbiAgICAgICAgICovXG4gICAgICAgIEhlYWRlci5wcm90b3R5cGUuaW52b2tlRW5kID0gbnVsbDtcblxuICAgICAgICAvKipcbiAgICAgICAgICogSGVhZGVyIGNsb3NlQ29ubmVjdGlvbi5cbiAgICAgICAgICogQG1lbWJlciB7SUNsb3NlQ29ubmVjdGlvbnxudWxsfHVuZGVmaW5lZH0gY2xvc2VDb25uZWN0aW9uXG4gICAgICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZS5IZWFkZXJcbiAgICAgICAgICogQGluc3RhbmNlXG4gICAgICAgICAqL1xuICAgICAgICBIZWFkZXIucHJvdG90eXBlLmNsb3NlQ29ubmVjdGlvbiA9IG51bGw7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEhlYWRlciBldmVudC5cbiAgICAgICAgICogQG1lbWJlciB7c3RyaW5nfG51bGx8dW5kZWZpbmVkfSBldmVudFxuICAgICAgICAgKiBAbWVtYmVyb2YgRW52ZWxvcGUuSGVhZGVyXG4gICAgICAgICAqIEBpbnN0YW5jZVxuICAgICAgICAgKi9cbiAgICAgICAgSGVhZGVyLnByb3RvdHlwZS5ldmVudCA9IG51bGw7XG5cbiAgICAgICAgLy8gT25lT2YgZmllbGQgbmFtZXMgYm91bmQgdG8gdmlydHVhbCBnZXR0ZXJzIGFuZCBzZXR0ZXJzXG4gICAgICAgIHZhciAkb25lT2ZGaWVsZHM7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEhlYWRlciBraW5kLlxuICAgICAgICAgKiBAbWVtYmVyIHtcImludm9rZUJlZ2luXCJ8XCJpbnZva2VFbmRcInxcImNsb3NlQ29ubmVjdGlvblwifFwiZXZlbnRcInx1bmRlZmluZWR9IGtpbmRcbiAgICAgICAgICogQG1lbWJlcm9mIEVudmVsb3BlLkhlYWRlclxuICAgICAgICAgKiBAaW5zdGFuY2VcbiAgICAgICAgICovXG4gICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShIZWFkZXIucHJvdG90eXBlLCBcImtpbmRcIiwge1xuICAgICAgICAgICAgZ2V0OiAkdXRpbC5vbmVPZkdldHRlcigkb25lT2ZGaWVsZHMgPSBbXCJpbnZva2VCZWdpblwiLCBcImludm9rZUVuZFwiLCBcImNsb3NlQ29ubmVjdGlvblwiLCBcImV2ZW50XCJdKSxcbiAgICAgICAgICAgIHNldDogJHV0aWwub25lT2ZTZXR0ZXIoJG9uZU9mRmllbGRzKVxuICAgICAgICB9KTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ3JlYXRlcyBhIG5ldyBIZWFkZXIgaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAgICAgKiBAZnVuY3Rpb24gY3JlYXRlXG4gICAgICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZS5IZWFkZXJcbiAgICAgICAgICogQHN0YXRpY1xuICAgICAgICAgKiBAcGFyYW0ge0VudmVsb3BlLklIZWFkZXI9fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgICAgICogQHJldHVybnMge0VudmVsb3BlLkhlYWRlcn0gSGVhZGVyIGluc3RhbmNlXG4gICAgICAgICAqL1xuICAgICAgICBIZWFkZXIuY3JlYXRlID0gZnVuY3Rpb24gY3JlYXRlKHByb3BlcnRpZXMpIHtcbiAgICAgICAgICAgIHJldHVybiBuZXcgSGVhZGVyKHByb3BlcnRpZXMpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgSGVhZGVyIG1lc3NhZ2UuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIEVudmVsb3BlLkhlYWRlci52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgICAgICogQGZ1bmN0aW9uIGVuY29kZVxuICAgICAgICAgKiBAbWVtYmVyb2YgRW52ZWxvcGUuSGVhZGVyXG4gICAgICAgICAqIEBzdGF0aWNcbiAgICAgICAgICogQHBhcmFtIHtFbnZlbG9wZS5JSGVhZGVyfSBtZXNzYWdlIEhlYWRlciBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgICAgICogQHBhcmFtIHskcHJvdG9idWYuV3JpdGVyfSBbd3JpdGVyXSBXcml0ZXIgdG8gZW5jb2RlIHRvXG4gICAgICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgICAgICovXG4gICAgICAgIEhlYWRlci5lbmNvZGUgPSBmdW5jdGlvbiBlbmNvZGUobWVzc2FnZSwgd3JpdGVyKSB7XG4gICAgICAgICAgICBpZiAoIXdyaXRlcilcbiAgICAgICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICAgICAgaWYgKG1lc3NhZ2UuaW52b2tlQmVnaW4gIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImludm9rZUJlZ2luXCIpKVxuICAgICAgICAgICAgICAgICRyb290Lkludm9rZUJlZ2luLmVuY29kZShtZXNzYWdlLmludm9rZUJlZ2luLCB3cml0ZXIudWludDMyKC8qIGlkIDEsIHdpcmVUeXBlIDIgPSovMTApLmZvcmsoKSkubGRlbGltKCk7XG4gICAgICAgICAgICBpZiAobWVzc2FnZS5pbnZva2VFbmQgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImludm9rZUVuZFwiKSlcbiAgICAgICAgICAgICAgICAkcm9vdC5JbnZva2VFbmQuZW5jb2RlKG1lc3NhZ2UuaW52b2tlRW5kLCB3cml0ZXIudWludDMyKC8qIGlkIDIsIHdpcmVUeXBlIDIgPSovMTgpLmZvcmsoKSkubGRlbGltKCk7XG4gICAgICAgICAgICBpZiAobWVzc2FnZS5jbG9zZUNvbm5lY3Rpb24gIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImNsb3NlQ29ubmVjdGlvblwiKSlcbiAgICAgICAgICAgICAgICAkcm9vdC5DbG9zZUNvbm5lY3Rpb24uZW5jb2RlKG1lc3NhZ2UuY2xvc2VDb25uZWN0aW9uLCB3cml0ZXIudWludDMyKC8qIGlkIDMsIHdpcmVUeXBlIDIgPSovMjYpLmZvcmsoKSkubGRlbGltKCk7XG4gICAgICAgICAgICBpZiAobWVzc2FnZS5ldmVudCAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwiZXZlbnRcIikpXG4gICAgICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCA0LCB3aXJlVHlwZSAyID0qLzM0KS5zdHJpbmcobWVzc2FnZS5ldmVudCk7XG4gICAgICAgICAgICByZXR1cm4gd3JpdGVyO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgSGVhZGVyIG1lc3NhZ2UsIGxlbmd0aCBkZWxpbWl0ZWQuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIEVudmVsb3BlLkhlYWRlci52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAgICAgKiBAbWVtYmVyb2YgRW52ZWxvcGUuSGVhZGVyXG4gICAgICAgICAqIEBzdGF0aWNcbiAgICAgICAgICogQHBhcmFtIHtFbnZlbG9wZS5JSGVhZGVyfSBtZXNzYWdlIEhlYWRlciBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgICAgICogQHBhcmFtIHskcHJvdG9idWYuV3JpdGVyfSBbd3JpdGVyXSBXcml0ZXIgdG8gZW5jb2RlIHRvXG4gICAgICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgICAgICovXG4gICAgICAgIEhlYWRlci5lbmNvZGVEZWxpbWl0ZWQgPSBmdW5jdGlvbiBlbmNvZGVEZWxpbWl0ZWQobWVzc2FnZSwgd3JpdGVyKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5lbmNvZGUobWVzc2FnZSwgd3JpdGVyKS5sZGVsaW0oKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogRGVjb2RlcyBhIEhlYWRlciBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLlxuICAgICAgICAgKiBAZnVuY3Rpb24gZGVjb2RlXG4gICAgICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZS5IZWFkZXJcbiAgICAgICAgICogQHN0YXRpY1xuICAgICAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgICAgICogQHBhcmFtIHtudW1iZXJ9IFtsZW5ndGhdIE1lc3NhZ2UgbGVuZ3RoIGlmIGtub3duIGJlZm9yZWhhbmRcbiAgICAgICAgICogQHJldHVybnMge0VudmVsb3BlLkhlYWRlcn0gSGVhZGVyXG4gICAgICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAgICAgKi9cbiAgICAgICAgSGVhZGVyLmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICAgICAgcmVhZGVyID0gJFJlYWRlci5jcmVhdGUocmVhZGVyKTtcbiAgICAgICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LkVudmVsb3BlLkhlYWRlcigpO1xuICAgICAgICAgICAgd2hpbGUgKHJlYWRlci5wb3MgPCBlbmQpIHtcbiAgICAgICAgICAgICAgICB2YXIgdGFnID0gcmVhZGVyLnVpbnQzMigpO1xuICAgICAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlLmludm9rZUJlZ2luID0gJHJvb3QuSW52b2tlQmVnaW4uZGVjb2RlKHJlYWRlciwgcmVhZGVyLnVpbnQzMigpKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlLmludm9rZUVuZCA9ICRyb290Lkludm9rZUVuZC5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2UuY2xvc2VDb25uZWN0aW9uID0gJHJvb3QuQ2xvc2VDb25uZWN0aW9uLmRlY29kZShyZWFkZXIsIHJlYWRlci51aW50MzIoKSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgNDpcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZS5ldmVudCA9IHJlYWRlci5zdHJpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogRGVjb2RlcyBhIEhlYWRlciBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLCBsZW5ndGggZGVsaW1pdGVkLlxuICAgICAgICAgKiBAZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkXG4gICAgICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZS5IZWFkZXJcbiAgICAgICAgICogQHN0YXRpY1xuICAgICAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgICAgICogQHJldHVybnMge0VudmVsb3BlLkhlYWRlcn0gSGVhZGVyXG4gICAgICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAgICAgKi9cbiAgICAgICAgSGVhZGVyLmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgICAgIGlmICghKHJlYWRlciBpbnN0YW5jZW9mICRSZWFkZXIpKVxuICAgICAgICAgICAgICAgIHJlYWRlciA9IG5ldyAkUmVhZGVyKHJlYWRlcik7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBWZXJpZmllcyBhIEhlYWRlciBtZXNzYWdlLlxuICAgICAgICAgKiBAZnVuY3Rpb24gdmVyaWZ5XG4gICAgICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZS5IZWFkZXJcbiAgICAgICAgICogQHN0YXRpY1xuICAgICAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBtZXNzYWdlIFBsYWluIG9iamVjdCB0byB2ZXJpZnlcbiAgICAgICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgICAgICovXG4gICAgICAgIEhlYWRlci52ZXJpZnkgPSBmdW5jdGlvbiB2ZXJpZnkobWVzc2FnZSkge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlICE9PSBcIm9iamVjdFwiIHx8IG1lc3NhZ2UgPT09IG51bGwpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwib2JqZWN0IGV4cGVjdGVkXCI7XG4gICAgICAgICAgICB2YXIgcHJvcGVydGllcyA9IHt9O1xuICAgICAgICAgICAgaWYgKG1lc3NhZ2UuaW52b2tlQmVnaW4gIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiaW52b2tlQmVnaW5cIikpIHtcbiAgICAgICAgICAgICAgICBwcm9wZXJ0aWVzLmtpbmQgPSAxO1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9yID0gJHJvb3QuSW52b2tlQmVnaW4udmVyaWZ5KG1lc3NhZ2UuaW52b2tlQmVnaW4pO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3IpXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJpbnZva2VCZWdpbi5cIiArIGVycm9yO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChtZXNzYWdlLmludm9rZUVuZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJpbnZva2VFbmRcIikpIHtcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydGllcy5raW5kID09PSAxKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJraW5kOiBtdWx0aXBsZSB2YWx1ZXNcIjtcbiAgICAgICAgICAgICAgICBwcm9wZXJ0aWVzLmtpbmQgPSAxO1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9yID0gJHJvb3QuSW52b2tlRW5kLnZlcmlmeShtZXNzYWdlLmludm9rZUVuZCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnJvcilcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBcImludm9rZUVuZC5cIiArIGVycm9yO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChtZXNzYWdlLmNsb3NlQ29ubmVjdGlvbiAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJjbG9zZUNvbm5lY3Rpb25cIikpIHtcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydGllcy5raW5kID09PSAxKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJraW5kOiBtdWx0aXBsZSB2YWx1ZXNcIjtcbiAgICAgICAgICAgICAgICBwcm9wZXJ0aWVzLmtpbmQgPSAxO1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9yID0gJHJvb3QuQ2xvc2VDb25uZWN0aW9uLnZlcmlmeShtZXNzYWdlLmNsb3NlQ29ubmVjdGlvbik7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnJvcilcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBcImNsb3NlQ29ubmVjdGlvbi5cIiArIGVycm9yO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChtZXNzYWdlLmV2ZW50ICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImV2ZW50XCIpKSB7XG4gICAgICAgICAgICAgICAgaWYgKHByb3BlcnRpZXMua2luZCA9PT0gMSlcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwia2luZDogbXVsdGlwbGUgdmFsdWVzXCI7XG4gICAgICAgICAgICAgICAgcHJvcGVydGllcy5raW5kID0gMTtcbiAgICAgICAgICAgICAgICBpZiAoISR1dGlsLmlzU3RyaW5nKG1lc3NhZ2UuZXZlbnQpKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJldmVudDogc3RyaW5nIGV4cGVjdGVkXCI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ3JlYXRlcyBhIEhlYWRlciBtZXNzYWdlIGZyb20gYSBwbGFpbiBvYmplY3QuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIHRoZWlyIHJlc3BlY3RpdmUgaW50ZXJuYWwgdHlwZXMuXG4gICAgICAgICAqIEBmdW5jdGlvbiBmcm9tT2JqZWN0XG4gICAgICAgICAqIEBtZW1iZXJvZiBFbnZlbG9wZS5IZWFkZXJcbiAgICAgICAgICogQHN0YXRpY1xuICAgICAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBvYmplY3QgUGxhaW4gb2JqZWN0XG4gICAgICAgICAqIEByZXR1cm5zIHtFbnZlbG9wZS5IZWFkZXJ9IEhlYWRlclxuICAgICAgICAgKi9cbiAgICAgICAgSGVhZGVyLmZyb21PYmplY3QgPSBmdW5jdGlvbiBmcm9tT2JqZWN0KG9iamVjdCkge1xuICAgICAgICAgICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mICRyb290LkVudmVsb3BlLkhlYWRlcilcbiAgICAgICAgICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgICAgICAgICAgdmFyIG1lc3NhZ2UgPSBuZXcgJHJvb3QuRW52ZWxvcGUuSGVhZGVyKCk7XG4gICAgICAgICAgICBpZiAob2JqZWN0Lmludm9rZUJlZ2luICE9IG51bGwpIHtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG9iamVjdC5pbnZva2VCZWdpbiAhPT0gXCJvYmplY3RcIilcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgVHlwZUVycm9yKFwiLkVudmVsb3BlLkhlYWRlci5pbnZva2VCZWdpbjogb2JqZWN0IGV4cGVjdGVkXCIpO1xuICAgICAgICAgICAgICAgIG1lc3NhZ2UuaW52b2tlQmVnaW4gPSAkcm9vdC5JbnZva2VCZWdpbi5mcm9tT2JqZWN0KG9iamVjdC5pbnZva2VCZWdpbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAob2JqZWN0Lmludm9rZUVuZCAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBvYmplY3QuaW52b2tlRW5kICE9PSBcIm9iamVjdFwiKVxuICAgICAgICAgICAgICAgICAgICB0aHJvdyBUeXBlRXJyb3IoXCIuRW52ZWxvcGUuSGVhZGVyLmludm9rZUVuZDogb2JqZWN0IGV4cGVjdGVkXCIpO1xuICAgICAgICAgICAgICAgIG1lc3NhZ2UuaW52b2tlRW5kID0gJHJvb3QuSW52b2tlRW5kLmZyb21PYmplY3Qob2JqZWN0Lmludm9rZUVuZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAob2JqZWN0LmNsb3NlQ29ubmVjdGlvbiAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBvYmplY3QuY2xvc2VDb25uZWN0aW9uICE9PSBcIm9iamVjdFwiKVxuICAgICAgICAgICAgICAgICAgICB0aHJvdyBUeXBlRXJyb3IoXCIuRW52ZWxvcGUuSGVhZGVyLmNsb3NlQ29ubmVjdGlvbjogb2JqZWN0IGV4cGVjdGVkXCIpO1xuICAgICAgICAgICAgICAgIG1lc3NhZ2UuY2xvc2VDb25uZWN0aW9uID0gJHJvb3QuQ2xvc2VDb25uZWN0aW9uLmZyb21PYmplY3Qob2JqZWN0LmNsb3NlQ29ubmVjdGlvbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAob2JqZWN0LmV2ZW50ICE9IG51bGwpXG4gICAgICAgICAgICAgICAgbWVzc2FnZS5ldmVudCA9IFN0cmluZyhvYmplY3QuZXZlbnQpO1xuICAgICAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENyZWF0ZXMgYSBwbGFpbiBvYmplY3QgZnJvbSBhIEhlYWRlciBtZXNzYWdlLiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byBvdGhlciB0eXBlcyBpZiBzcGVjaWZpZWQuXG4gICAgICAgICAqIEBmdW5jdGlvbiB0b09iamVjdFxuICAgICAgICAgKiBAbWVtYmVyb2YgRW52ZWxvcGUuSGVhZGVyXG4gICAgICAgICAqIEBzdGF0aWNcbiAgICAgICAgICogQHBhcmFtIHtFbnZlbG9wZS5IZWFkZXJ9IG1lc3NhZ2UgSGVhZGVyXG4gICAgICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLklDb252ZXJzaW9uT3B0aW9uc30gW29wdGlvbnNdIENvbnZlcnNpb24gb3B0aW9uc1xuICAgICAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IFBsYWluIG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgSGVhZGVyLnRvT2JqZWN0ID0gZnVuY3Rpb24gdG9PYmplY3QobWVzc2FnZSwgb3B0aW9ucykge1xuICAgICAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgICAgIG9wdGlvbnMgPSB7fTtcbiAgICAgICAgICAgIHZhciBvYmplY3QgPSB7fTtcbiAgICAgICAgICAgIGlmIChtZXNzYWdlLmludm9rZUJlZ2luICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImludm9rZUJlZ2luXCIpKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0Lmludm9rZUJlZ2luID0gJHJvb3QuSW52b2tlQmVnaW4udG9PYmplY3QobWVzc2FnZS5pbnZva2VCZWdpbiwgb3B0aW9ucyk7XG4gICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMub25lb2ZzKVxuICAgICAgICAgICAgICAgICAgICBvYmplY3Qua2luZCA9IFwiaW52b2tlQmVnaW5cIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChtZXNzYWdlLmludm9rZUVuZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJpbnZva2VFbmRcIikpIHtcbiAgICAgICAgICAgICAgICBvYmplY3QuaW52b2tlRW5kID0gJHJvb3QuSW52b2tlRW5kLnRvT2JqZWN0KG1lc3NhZ2UuaW52b2tlRW5kLCBvcHRpb25zKTtcbiAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5vbmVvZnMpXG4gICAgICAgICAgICAgICAgICAgIG9iamVjdC5raW5kID0gXCJpbnZva2VFbmRcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChtZXNzYWdlLmNsb3NlQ29ubmVjdGlvbiAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJjbG9zZUNvbm5lY3Rpb25cIikpIHtcbiAgICAgICAgICAgICAgICBvYmplY3QuY2xvc2VDb25uZWN0aW9uID0gJHJvb3QuQ2xvc2VDb25uZWN0aW9uLnRvT2JqZWN0KG1lc3NhZ2UuY2xvc2VDb25uZWN0aW9uLCBvcHRpb25zKTtcbiAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5vbmVvZnMpXG4gICAgICAgICAgICAgICAgICAgIG9iamVjdC5raW5kID0gXCJjbG9zZUNvbm5lY3Rpb25cIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChtZXNzYWdlLmV2ZW50ICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImV2ZW50XCIpKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0LmV2ZW50ID0gbWVzc2FnZS5ldmVudDtcbiAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5vbmVvZnMpXG4gICAgICAgICAgICAgICAgICAgIG9iamVjdC5raW5kID0gXCJldmVudFwiO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ29udmVydHMgdGhpcyBIZWFkZXIgdG8gSlNPTi5cbiAgICAgICAgICogQGZ1bmN0aW9uIHRvSlNPTlxuICAgICAgICAgKiBAbWVtYmVyb2YgRW52ZWxvcGUuSGVhZGVyXG4gICAgICAgICAqIEBpbnN0YW5jZVxuICAgICAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IEpTT04gb2JqZWN0XG4gICAgICAgICAqL1xuICAgICAgICBIZWFkZXIucHJvdG90eXBlLnRvSlNPTiA9IGZ1bmN0aW9uIHRvSlNPTigpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnN0cnVjdG9yLnRvT2JqZWN0KHRoaXMsICRwcm90b2J1Zi51dGlsLnRvSlNPTk9wdGlvbnMpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBIZWFkZXI7XG4gICAgfSkoKTtcblxuICAgIHJldHVybiBFbnZlbG9wZTtcbn0pKCk7XG5cbiRyb290LkdhbWVTZXJ2ZXIgPSAoZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBDb25zdHJ1Y3RzIGEgbmV3IEdhbWVTZXJ2ZXIgc2VydmljZS5cbiAgICAgKiBAZXhwb3J0cyBHYW1lU2VydmVyXG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgR2FtZVNlcnZlclxuICAgICAqIEBleHRlbmRzICRwcm90b2J1Zi5ycGMuU2VydmljZVxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJQQ0ltcGx9IHJwY0ltcGwgUlBDIGltcGxlbWVudGF0aW9uXG4gICAgICogQHBhcmFtIHtib29sZWFufSBbcmVxdWVzdERlbGltaXRlZD1mYWxzZV0gV2hldGhlciByZXF1ZXN0cyBhcmUgbGVuZ3RoLWRlbGltaXRlZFxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gW3Jlc3BvbnNlRGVsaW1pdGVkPWZhbHNlXSBXaGV0aGVyIHJlc3BvbnNlcyBhcmUgbGVuZ3RoLWRlbGltaXRlZFxuICAgICAqL1xuICAgIGZ1bmN0aW9uIEdhbWVTZXJ2ZXIocnBjSW1wbCwgcmVxdWVzdERlbGltaXRlZCwgcmVzcG9uc2VEZWxpbWl0ZWQpIHtcbiAgICAgICAgJHByb3RvYnVmLnJwYy5TZXJ2aWNlLmNhbGwodGhpcywgcnBjSW1wbCwgcmVxdWVzdERlbGltaXRlZCwgcmVzcG9uc2VEZWxpbWl0ZWQpO1xuICAgIH1cblxuICAgIChHYW1lU2VydmVyLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoJHByb3RvYnVmLnJwYy5TZXJ2aWNlLnByb3RvdHlwZSkpLmNvbnN0cnVjdG9yID0gR2FtZVNlcnZlcjtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgbmV3IEdhbWVTZXJ2ZXIgc2VydmljZSB1c2luZyB0aGUgc3BlY2lmaWVkIHJwYyBpbXBsZW1lbnRhdGlvbi5cbiAgICAgKiBAZnVuY3Rpb24gY3JlYXRlXG4gICAgICogQG1lbWJlcm9mIEdhbWVTZXJ2ZXJcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUlBDSW1wbH0gcnBjSW1wbCBSUEMgaW1wbGVtZW50YXRpb25cbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtyZXF1ZXN0RGVsaW1pdGVkPWZhbHNlXSBXaGV0aGVyIHJlcXVlc3RzIGFyZSBsZW5ndGgtZGVsaW1pdGVkXG4gICAgICogQHBhcmFtIHtib29sZWFufSBbcmVzcG9uc2VEZWxpbWl0ZWQ9ZmFsc2VdIFdoZXRoZXIgcmVzcG9uc2VzIGFyZSBsZW5ndGgtZGVsaW1pdGVkXG4gICAgICogQHJldHVybnMge0dhbWVTZXJ2ZXJ9IFJQQyBzZXJ2aWNlLiBVc2VmdWwgd2hlcmUgcmVxdWVzdHMgYW5kL29yIHJlc3BvbnNlcyBhcmUgc3RyZWFtZWQuXG4gICAgICovXG4gICAgR2FtZVNlcnZlci5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUocnBjSW1wbCwgcmVxdWVzdERlbGltaXRlZCwgcmVzcG9uc2VEZWxpbWl0ZWQpIHtcbiAgICAgICAgcmV0dXJuIG5ldyB0aGlzKHJwY0ltcGwsIHJlcXVlc3REZWxpbWl0ZWQsIHJlc3BvbnNlRGVsaW1pdGVkKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ2FsbGJhY2sgYXMgdXNlZCBieSB7QGxpbmsgR2FtZVNlcnZlciNsb2dpbn0uXG4gICAgICogQG1lbWJlcm9mIEdhbWVTZXJ2ZXJcbiAgICAgKiBAdHlwZWRlZiBMb2dpbkNhbGxiYWNrXG4gICAgICogQHR5cGUge2Z1bmN0aW9ufVxuICAgICAqIEBwYXJhbSB7RXJyb3J8bnVsbH0gZXJyb3IgRXJyb3IsIGlmIGFueVxuICAgICAqIEBwYXJhbSB7TG9naW5SZXBseX0gW3Jlc3BvbnNlXSBMb2dpblJlcGx5XG4gICAgICovXG5cbiAgICAvKipcbiAgICAgKiBDYWxscyBMb2dpbi5cbiAgICAgKiBAZnVuY3Rpb24gbG9naW5cbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7SUxvZ2luUmVxdWVzdH0gcmVxdWVzdCBMb2dpblJlcXVlc3QgbWVzc2FnZSBvciBwbGFpbiBvYmplY3RcbiAgICAgKiBAcGFyYW0ge0dhbWVTZXJ2ZXIuTG9naW5DYWxsYmFja30gY2FsbGJhY2sgTm9kZS1zdHlsZSBjYWxsYmFjayBjYWxsZWQgd2l0aCB0aGUgZXJyb3IsIGlmIGFueSwgYW5kIExvZ2luUmVwbHlcbiAgICAgKiBAcmV0dXJucyB7dW5kZWZpbmVkfVxuICAgICAqIEB2YXJpYXRpb24gMVxuICAgICAqL1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShHYW1lU2VydmVyLnByb3RvdHlwZS5sb2dpbiA9IGZ1bmN0aW9uIGxvZ2luKHJlcXVlc3QsIGNhbGxiYWNrKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJwY0NhbGwobG9naW4sICRyb290LkxvZ2luUmVxdWVzdCwgJHJvb3QuTG9naW5SZXBseSwgcmVxdWVzdCwgY2FsbGJhY2spO1xuICAgIH0sIFwibmFtZVwiLCB7IHZhbHVlOiBcIkxvZ2luXCIgfSk7XG5cbiAgICAvKipcbiAgICAgKiBDYWxscyBMb2dpbi5cbiAgICAgKiBAZnVuY3Rpb24gbG9naW5cbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7SUxvZ2luUmVxdWVzdH0gcmVxdWVzdCBMb2dpblJlcXVlc3QgbWVzc2FnZSBvciBwbGFpbiBvYmplY3RcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxMb2dpblJlcGx5Pn0gUHJvbWlzZVxuICAgICAqIEB2YXJpYXRpb24gMlxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ2FsbGJhY2sgYXMgdXNlZCBieSB7QGxpbmsgR2FtZVNlcnZlciNidXlQcm9kdWNlfS5cbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEB0eXBlZGVmIEJ1eVByb2R1Y2VDYWxsYmFja1xuICAgICAqIEB0eXBlIHtmdW5jdGlvbn1cbiAgICAgKiBAcGFyYW0ge0Vycm9yfG51bGx9IGVycm9yIEVycm9yLCBpZiBhbnlcbiAgICAgKiBAcGFyYW0ge0J1eVByb2R1Y2VSZXBseX0gW3Jlc3BvbnNlXSBCdXlQcm9kdWNlUmVwbHlcbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIENhbGxzIEJ1eVByb2R1Y2UuXG4gICAgICogQGZ1bmN0aW9uIGJ1eVByb2R1Y2VcbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7SUJ1eVByb2R1Y2VSZXF1ZXN0fSByZXF1ZXN0IEJ1eVByb2R1Y2VSZXF1ZXN0IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0XG4gICAgICogQHBhcmFtIHtHYW1lU2VydmVyLkJ1eVByb2R1Y2VDYWxsYmFja30gY2FsbGJhY2sgTm9kZS1zdHlsZSBjYWxsYmFjayBjYWxsZWQgd2l0aCB0aGUgZXJyb3IsIGlmIGFueSwgYW5kIEJ1eVByb2R1Y2VSZXBseVxuICAgICAqIEByZXR1cm5zIHt1bmRlZmluZWR9XG4gICAgICogQHZhcmlhdGlvbiAxXG4gICAgICovXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KEdhbWVTZXJ2ZXIucHJvdG90eXBlLmJ1eVByb2R1Y2UgPSBmdW5jdGlvbiBidXlQcm9kdWNlKHJlcXVlc3QsIGNhbGxiYWNrKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJwY0NhbGwoYnV5UHJvZHVjZSwgJHJvb3QuQnV5UHJvZHVjZVJlcXVlc3QsICRyb290LkJ1eVByb2R1Y2VSZXBseSwgcmVxdWVzdCwgY2FsbGJhY2spO1xuICAgIH0sIFwibmFtZVwiLCB7IHZhbHVlOiBcIkJ1eVByb2R1Y2VcIiB9KTtcblxuICAgIC8qKlxuICAgICAqIENhbGxzIEJ1eVByb2R1Y2UuXG4gICAgICogQGZ1bmN0aW9uIGJ1eVByb2R1Y2VcbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7SUJ1eVByb2R1Y2VSZXF1ZXN0fSByZXF1ZXN0IEJ1eVByb2R1Y2VSZXF1ZXN0IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0XG4gICAgICogQHJldHVybnMge1Byb21pc2U8QnV5UHJvZHVjZVJlcGx5Pn0gUHJvbWlzZVxuICAgICAqIEB2YXJpYXRpb24gMlxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ2FsbGJhY2sgYXMgdXNlZCBieSB7QGxpbmsgR2FtZVNlcnZlciNjb21iaW5lUHJvZHVjZX0uXG4gICAgICogQG1lbWJlcm9mIEdhbWVTZXJ2ZXJcbiAgICAgKiBAdHlwZWRlZiBDb21iaW5lUHJvZHVjZUNhbGxiYWNrXG4gICAgICogQHR5cGUge2Z1bmN0aW9ufVxuICAgICAqIEBwYXJhbSB7RXJyb3J8bnVsbH0gZXJyb3IgRXJyb3IsIGlmIGFueVxuICAgICAqIEBwYXJhbSB7Q29tYmluZVByb2R1Y2VSZXBseX0gW3Jlc3BvbnNlXSBDb21iaW5lUHJvZHVjZVJlcGx5XG4gICAgICovXG5cbiAgICAvKipcbiAgICAgKiBDYWxscyBDb21iaW5lUHJvZHVjZS5cbiAgICAgKiBAZnVuY3Rpb24gY29tYmluZVByb2R1Y2VcbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7SUNvbWJpbmVQcm9kdWNlUmVxdWVzdH0gcmVxdWVzdCBDb21iaW5lUHJvZHVjZVJlcXVlc3QgbWVzc2FnZSBvciBwbGFpbiBvYmplY3RcbiAgICAgKiBAcGFyYW0ge0dhbWVTZXJ2ZXIuQ29tYmluZVByb2R1Y2VDYWxsYmFja30gY2FsbGJhY2sgTm9kZS1zdHlsZSBjYWxsYmFjayBjYWxsZWQgd2l0aCB0aGUgZXJyb3IsIGlmIGFueSwgYW5kIENvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKiBAcmV0dXJucyB7dW5kZWZpbmVkfVxuICAgICAqIEB2YXJpYXRpb24gMVxuICAgICAqL1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShHYW1lU2VydmVyLnByb3RvdHlwZS5jb21iaW5lUHJvZHVjZSA9IGZ1bmN0aW9uIGNvbWJpbmVQcm9kdWNlKHJlcXVlc3QsIGNhbGxiYWNrKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJwY0NhbGwoY29tYmluZVByb2R1Y2UsICRyb290LkNvbWJpbmVQcm9kdWNlUmVxdWVzdCwgJHJvb3QuQ29tYmluZVByb2R1Y2VSZXBseSwgcmVxdWVzdCwgY2FsbGJhY2spO1xuICAgIH0sIFwibmFtZVwiLCB7IHZhbHVlOiBcIkNvbWJpbmVQcm9kdWNlXCIgfSk7XG5cbiAgICAvKipcbiAgICAgKiBDYWxscyBDb21iaW5lUHJvZHVjZS5cbiAgICAgKiBAZnVuY3Rpb24gY29tYmluZVByb2R1Y2VcbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7SUNvbWJpbmVQcm9kdWNlUmVxdWVzdH0gcmVxdWVzdCBDb21iaW5lUHJvZHVjZVJlcXVlc3QgbWVzc2FnZSBvciBwbGFpbiBvYmplY3RcbiAgICAgKiBAcmV0dXJucyB7UHJvbWlzZTxDb21iaW5lUHJvZHVjZVJlcGx5Pn0gUHJvbWlzZVxuICAgICAqIEB2YXJpYXRpb24gMlxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ2FsbGJhY2sgYXMgdXNlZCBieSB7QGxpbmsgR2FtZVNlcnZlciNyZWZyZXNoV29ya0JlbmNofS5cbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEB0eXBlZGVmIFJlZnJlc2hXb3JrQmVuY2hDYWxsYmFja1xuICAgICAqIEB0eXBlIHtmdW5jdGlvbn1cbiAgICAgKiBAcGFyYW0ge0Vycm9yfG51bGx9IGVycm9yIEVycm9yLCBpZiBhbnlcbiAgICAgKiBAcGFyYW0ge1JlZnJlc2hXb3JrQmVuY2hSZXBseX0gW3Jlc3BvbnNlXSBSZWZyZXNoV29ya0JlbmNoUmVwbHlcbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIENhbGxzIFJlZnJlc2hXb3JrQmVuY2guXG4gICAgICogQGZ1bmN0aW9uIHJlZnJlc2hXb3JrQmVuY2hcbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7SVJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0fSByZXF1ZXN0IFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0XG4gICAgICogQHBhcmFtIHtHYW1lU2VydmVyLlJlZnJlc2hXb3JrQmVuY2hDYWxsYmFja30gY2FsbGJhY2sgTm9kZS1zdHlsZSBjYWxsYmFjayBjYWxsZWQgd2l0aCB0aGUgZXJyb3IsIGlmIGFueSwgYW5kIFJlZnJlc2hXb3JrQmVuY2hSZXBseVxuICAgICAqIEByZXR1cm5zIHt1bmRlZmluZWR9XG4gICAgICogQHZhcmlhdGlvbiAxXG4gICAgICovXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KEdhbWVTZXJ2ZXIucHJvdG90eXBlLnJlZnJlc2hXb3JrQmVuY2ggPSBmdW5jdGlvbiByZWZyZXNoV29ya0JlbmNoKHJlcXVlc3QsIGNhbGxiYWNrKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJwY0NhbGwocmVmcmVzaFdvcmtCZW5jaCwgJHJvb3QuUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QsICRyb290LlJlZnJlc2hXb3JrQmVuY2hSZXBseSwgcmVxdWVzdCwgY2FsbGJhY2spO1xuICAgIH0sIFwibmFtZVwiLCB7IHZhbHVlOiBcIlJlZnJlc2hXb3JrQmVuY2hcIiB9KTtcblxuICAgIC8qKlxuICAgICAqIENhbGxzIFJlZnJlc2hXb3JrQmVuY2guXG4gICAgICogQGZ1bmN0aW9uIHJlZnJlc2hXb3JrQmVuY2hcbiAgICAgKiBAbWVtYmVyb2YgR2FtZVNlcnZlclxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEBwYXJhbSB7SVJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0fSByZXF1ZXN0IFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0XG4gICAgICogQHJldHVybnMge1Byb21pc2U8UmVmcmVzaFdvcmtCZW5jaFJlcGx5Pn0gUHJvbWlzZVxuICAgICAqIEB2YXJpYXRpb24gMlxuICAgICAqL1xuXG4gICAgcmV0dXJuIEdhbWVTZXJ2ZXI7XG59KSgpO1xuXG4kcm9vdC5Mb2dpblJlcXVlc3QgPSAoZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBQcm9wZXJ0aWVzIG9mIGEgTG9naW5SZXF1ZXN0LlxuICAgICAqIEBleHBvcnRzIElMb2dpblJlcXVlc3RcbiAgICAgKiBAaW50ZXJmYWNlIElMb2dpblJlcXVlc3RcbiAgICAgKiBAcHJvcGVydHkge3N0cmluZ3xudWxsfSBbYWNjZXNzVG9rZW5dIExvZ2luUmVxdWVzdCBhY2Nlc3NUb2tlblxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ29uc3RydWN0cyBhIG5ldyBMb2dpblJlcXVlc3QuXG4gICAgICogQGV4cG9ydHMgTG9naW5SZXF1ZXN0XG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgTG9naW5SZXF1ZXN0LlxuICAgICAqIEBpbXBsZW1lbnRzIElMb2dpblJlcXVlc3RcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0ge0lMb2dpblJlcXVlc3Q9fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBMb2dpblJlcXVlc3QocHJvcGVydGllcykge1xuICAgICAgICBpZiAocHJvcGVydGllcylcbiAgICAgICAgICAgIGZvciAodmFyIGtleXMgPSBPYmplY3Qua2V5cyhwcm9wZXJ0aWVzKSwgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgIGlmIChwcm9wZXJ0aWVzW2tleXNbaV1dICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXNba2V5c1tpXV0gPSBwcm9wZXJ0aWVzW2tleXNbaV1dO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIExvZ2luUmVxdWVzdCBhY2Nlc3NUb2tlbi5cbiAgICAgKiBAbWVtYmVyIHtzdHJpbmd9IGFjY2Vzc1Rva2VuXG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVxdWVzdFxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqL1xuICAgIExvZ2luUmVxdWVzdC5wcm90b3R5cGUuYWNjZXNzVG9rZW4gPSBcIlwiO1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBMb2dpblJlcXVlc3QgaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAqIEBmdW5jdGlvbiBjcmVhdGVcbiAgICAgKiBAbWVtYmVyb2YgTG9naW5SZXF1ZXN0XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUxvZ2luUmVxdWVzdD19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqIEByZXR1cm5zIHtMb2dpblJlcXVlc3R9IExvZ2luUmVxdWVzdCBpbnN0YW5jZVxuICAgICAqL1xuICAgIExvZ2luUmVxdWVzdC5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUocHJvcGVydGllcykge1xuICAgICAgICByZXR1cm4gbmV3IExvZ2luUmVxdWVzdChwcm9wZXJ0aWVzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIExvZ2luUmVxdWVzdCBtZXNzYWdlLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBMb2dpblJlcXVlc3QudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZVxuICAgICAqIEBtZW1iZXJvZiBMb2dpblJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJTG9naW5SZXF1ZXN0fSBtZXNzYWdlIExvZ2luUmVxdWVzdCBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgTG9naW5SZXF1ZXN0LmVuY29kZSA9IGZ1bmN0aW9uIGVuY29kZShtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgaWYgKCF3cml0ZXIpXG4gICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICBpZiAobWVzc2FnZS5hY2Nlc3NUb2tlbiAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwiYWNjZXNzVG9rZW5cIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDEsIHdpcmVUeXBlIDIgPSovMTApLnN0cmluZyhtZXNzYWdlLmFjY2Vzc1Rva2VuKTtcbiAgICAgICAgcmV0dXJuIHdyaXRlcjtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIExvZ2luUmVxdWVzdCBtZXNzYWdlLCBsZW5ndGggZGVsaW1pdGVkLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBMb2dpblJlcXVlc3QudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBMb2dpblJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJTG9naW5SZXF1ZXN0fSBtZXNzYWdlIExvZ2luUmVxdWVzdCBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgTG9naW5SZXF1ZXN0LmVuY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZChtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikubGRlbGltKCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBMb2dpblJlcXVlc3QgbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlci5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlXG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtMb2dpblJlcXVlc3R9IExvZ2luUmVxdWVzdFxuICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICogQHRocm93cyB7JHByb3RvYnVmLnV0aWwuUHJvdG9jb2xFcnJvcn0gSWYgcmVxdWlyZWQgZmllbGRzIGFyZSBtaXNzaW5nXG4gICAgICovXG4gICAgTG9naW5SZXF1ZXN0LmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LkxvZ2luUmVxdWVzdCgpO1xuICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZCkge1xuICAgICAgICAgICAgdmFyIHRhZyA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5hY2Nlc3NUb2tlbiA9IHJlYWRlci5zdHJpbmcoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgTG9naW5SZXF1ZXN0IG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIsIGxlbmd0aCBkZWxpbWl0ZWQuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBMb2dpblJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUmVhZGVyfFVpbnQ4QXJyYXl9IHJlYWRlciBSZWFkZXIgb3IgYnVmZmVyIHRvIGRlY29kZSBmcm9tXG4gICAgICogQHJldHVybnMge0xvZ2luUmVxdWVzdH0gTG9naW5SZXF1ZXN0XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBMb2dpblJlcXVlc3QuZGVjb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkKHJlYWRlcikge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9IG5ldyAkUmVhZGVyKHJlYWRlcik7XG4gICAgICAgIHJldHVybiB0aGlzLmRlY29kZShyZWFkZXIsIHJlYWRlci51aW50MzIoKSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFZlcmlmaWVzIGEgTG9naW5SZXF1ZXN0IG1lc3NhZ2UuXG4gICAgICogQGZ1bmN0aW9uIHZlcmlmeVxuICAgICAqIEBtZW1iZXJvZiBMb2dpblJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBMb2dpblJlcXVlc3QudmVyaWZ5ID0gZnVuY3Rpb24gdmVyaWZ5KG1lc3NhZ2UpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlICE9PSBcIm9iamVjdFwiIHx8IG1lc3NhZ2UgPT09IG51bGwpXG4gICAgICAgICAgICByZXR1cm4gXCJvYmplY3QgZXhwZWN0ZWRcIjtcbiAgICAgICAgaWYgKG1lc3NhZ2UuYWNjZXNzVG9rZW4gIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiYWNjZXNzVG9rZW5cIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzU3RyaW5nKG1lc3NhZ2UuYWNjZXNzVG9rZW4pKVxuICAgICAgICAgICAgICAgIHJldHVybiBcImFjY2Vzc1Rva2VuOiBzdHJpbmcgZXhwZWN0ZWRcIjtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBMb2dpblJlcXVlc3QgbWVzc2FnZSBmcm9tIGEgcGxhaW4gb2JqZWN0LiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byB0aGVpciByZXNwZWN0aXZlIGludGVybmFsIHR5cGVzLlxuICAgICAqIEBmdW5jdGlvbiBmcm9tT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBvYmplY3QgUGxhaW4gb2JqZWN0XG4gICAgICogQHJldHVybnMge0xvZ2luUmVxdWVzdH0gTG9naW5SZXF1ZXN0XG4gICAgICovXG4gICAgTG9naW5SZXF1ZXN0LmZyb21PYmplY3QgPSBmdW5jdGlvbiBmcm9tT2JqZWN0KG9iamVjdCkge1xuICAgICAgICBpZiAob2JqZWN0IGluc3RhbmNlb2YgJHJvb3QuTG9naW5SZXF1ZXN0KVxuICAgICAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICAgICAgdmFyIG1lc3NhZ2UgPSBuZXcgJHJvb3QuTG9naW5SZXF1ZXN0KCk7XG4gICAgICAgIGlmIChvYmplY3QuYWNjZXNzVG9rZW4gIT0gbnVsbClcbiAgICAgICAgICAgIG1lc3NhZ2UuYWNjZXNzVG9rZW4gPSBTdHJpbmcob2JqZWN0LmFjY2Vzc1Rva2VuKTtcbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBwbGFpbiBvYmplY3QgZnJvbSBhIExvZ2luUmVxdWVzdCBtZXNzYWdlLiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byBvdGhlciB0eXBlcyBpZiBzcGVjaWZpZWQuXG4gICAgICogQGZ1bmN0aW9uIHRvT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0xvZ2luUmVxdWVzdH0gbWVzc2FnZSBMb2dpblJlcXVlc3RcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5JQ29udmVyc2lvbk9wdGlvbnN9IFtvcHRpb25zXSBDb252ZXJzaW9uIG9wdGlvbnNcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IFBsYWluIG9iamVjdFxuICAgICAqL1xuICAgIExvZ2luUmVxdWVzdC50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG1lc3NhZ2UsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgICB2YXIgb2JqZWN0ID0ge307XG4gICAgICAgIGlmIChvcHRpb25zLmRlZmF1bHRzKVxuICAgICAgICAgICAgb2JqZWN0LmFjY2Vzc1Rva2VuID0gXCJcIjtcbiAgICAgICAgaWYgKG1lc3NhZ2UuYWNjZXNzVG9rZW4gIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiYWNjZXNzVG9rZW5cIikpXG4gICAgICAgICAgICBvYmplY3QuYWNjZXNzVG9rZW4gPSBtZXNzYWdlLmFjY2Vzc1Rva2VuO1xuICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGlzIExvZ2luUmVxdWVzdCB0byBKU09OLlxuICAgICAqIEBmdW5jdGlvbiB0b0pTT05cbiAgICAgKiBAbWVtYmVyb2YgTG9naW5SZXF1ZXN0XG4gICAgICogQGluc3RhbmNlXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBKU09OIG9iamVjdFxuICAgICAqL1xuICAgIExvZ2luUmVxdWVzdC5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci50b09iamVjdCh0aGlzLCAkcHJvdG9idWYudXRpbC50b0pTT05PcHRpb25zKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIExvZ2luUmVxdWVzdDtcbn0pKCk7XG5cbiRyb290LkxvZ2luUmVwbHkgPSAoZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBQcm9wZXJ0aWVzIG9mIGEgTG9naW5SZXBseS5cbiAgICAgKiBAZXhwb3J0cyBJTG9naW5SZXBseVxuICAgICAqIEBpbnRlcmZhY2UgSUxvZ2luUmVwbHlcbiAgICAgKiBAcHJvcGVydHkge0lVc2VySW5mb3xudWxsfSBbdXNlckluZm9dIExvZ2luUmVwbHkgdXNlckluZm9cbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbc2VydmVyVGltZV0gTG9naW5SZXBseSBzZXJ2ZXJUaW1lXG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8TG9uZ3xudWxsfSBbY29pbl0gTG9naW5SZXBseSBjb2luXG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8TG9uZ3xudWxsfSBbZGlhbW9uZF0gTG9naW5SZXBseSBkaWFtb25kXG4gICAgICogQHByb3BlcnR5IHtBcnJheS48SVdvcmtCZW5jaEl0ZW1JbmZvPnxudWxsfSBbd29ya2JlbmNoSXRlbXNdIExvZ2luUmVwbHkgd29ya2JlbmNoSXRlbXNcbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIENvbnN0cnVjdHMgYSBuZXcgTG9naW5SZXBseS5cbiAgICAgKiBAZXhwb3J0cyBMb2dpblJlcGx5XG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgTG9naW5SZXBseS5cbiAgICAgKiBAaW1wbGVtZW50cyBJTG9naW5SZXBseVxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7SUxvZ2luUmVwbHk9fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBMb2dpblJlcGx5KHByb3BlcnRpZXMpIHtcbiAgICAgICAgdGhpcy53b3JrYmVuY2hJdGVtcyA9IFtdO1xuICAgICAgICBpZiAocHJvcGVydGllcylcbiAgICAgICAgICAgIGZvciAodmFyIGtleXMgPSBPYmplY3Qua2V5cyhwcm9wZXJ0aWVzKSwgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgIGlmIChwcm9wZXJ0aWVzW2tleXNbaV1dICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXNba2V5c1tpXV0gPSBwcm9wZXJ0aWVzW2tleXNbaV1dO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIExvZ2luUmVwbHkgdXNlckluZm8uXG4gICAgICogQG1lbWJlciB7SVVzZXJJbmZvfG51bGx8dW5kZWZpbmVkfSB1c2VySW5mb1xuICAgICAqIEBtZW1iZXJvZiBMb2dpblJlcGx5XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgTG9naW5SZXBseS5wcm90b3R5cGUudXNlckluZm8gPSBudWxsO1xuXG4gICAgLyoqXG4gICAgICogTG9naW5SZXBseSBzZXJ2ZXJUaW1lLlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gc2VydmVyVGltZVxuICAgICAqIEBtZW1iZXJvZiBMb2dpblJlcGx5XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgTG9naW5SZXBseS5wcm90b3R5cGUuc2VydmVyVGltZSA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBMb2dpblJlcGx5IGNvaW4uXG4gICAgICogQG1lbWJlciB7bnVtYmVyfExvbmd9IGNvaW5cbiAgICAgKiBAbWVtYmVyb2YgTG9naW5SZXBseVxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqL1xuICAgIExvZ2luUmVwbHkucHJvdG90eXBlLmNvaW4gPSAkdXRpbC5Mb25nID8gJHV0aWwuTG9uZy5mcm9tQml0cygwLDAsdHJ1ZSkgOiAwO1xuXG4gICAgLyoqXG4gICAgICogTG9naW5SZXBseSBkaWFtb25kLlxuICAgICAqIEBtZW1iZXIge251bWJlcnxMb25nfSBkaWFtb25kXG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVwbHlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBMb2dpblJlcGx5LnByb3RvdHlwZS5kaWFtb25kID0gJHV0aWwuTG9uZyA/ICR1dGlsLkxvbmcuZnJvbUJpdHMoMCwwLHRydWUpIDogMDtcblxuICAgIC8qKlxuICAgICAqIExvZ2luUmVwbHkgd29ya2JlbmNoSXRlbXMuXG4gICAgICogQG1lbWJlciB7QXJyYXkuPElXb3JrQmVuY2hJdGVtSW5mbz59IHdvcmtiZW5jaEl0ZW1zXG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVwbHlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBMb2dpblJlcGx5LnByb3RvdHlwZS53b3JrYmVuY2hJdGVtcyA9ICR1dGlsLmVtcHR5QXJyYXk7XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IExvZ2luUmVwbHkgaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAqIEBmdW5jdGlvbiBjcmVhdGVcbiAgICAgKiBAbWVtYmVyb2YgTG9naW5SZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lMb2dpblJlcGx5PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICogQHJldHVybnMge0xvZ2luUmVwbHl9IExvZ2luUmVwbHkgaW5zdGFuY2VcbiAgICAgKi9cbiAgICBMb2dpblJlcGx5LmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShwcm9wZXJ0aWVzKSB7XG4gICAgICAgIHJldHVybiBuZXcgTG9naW5SZXBseShwcm9wZXJ0aWVzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIExvZ2luUmVwbHkgbWVzc2FnZS4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgTG9naW5SZXBseS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlXG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJTG9naW5SZXBseX0gbWVzc2FnZSBMb2dpblJlcGx5IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBMb2dpblJlcGx5LmVuY29kZSA9IGZ1bmN0aW9uIGVuY29kZShtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgaWYgKCF3cml0ZXIpXG4gICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICBpZiAobWVzc2FnZS51c2VySW5mbyAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwidXNlckluZm9cIikpXG4gICAgICAgICAgICAkcm9vdC5Vc2VySW5mby5lbmNvZGUobWVzc2FnZS51c2VySW5mbywgd3JpdGVyLnVpbnQzMigvKiBpZCAxLCB3aXJlVHlwZSAyID0qLzEwKS5mb3JrKCkpLmxkZWxpbSgpO1xuICAgICAgICBpZiAobWVzc2FnZS5zZXJ2ZXJUaW1lICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJzZXJ2ZXJUaW1lXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAyLCB3aXJlVHlwZSAwID0qLzE2KS51aW50MzIobWVzc2FnZS5zZXJ2ZXJUaW1lKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UuY29pbiAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwiY29pblwiKSlcbiAgICAgICAgICAgIHdyaXRlci51aW50MzIoLyogaWQgMywgd2lyZVR5cGUgMCA9Ki8yNCkudWludDY0KG1lc3NhZ2UuY29pbik7XG4gICAgICAgIGlmIChtZXNzYWdlLmRpYW1vbmQgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImRpYW1vbmRcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDQsIHdpcmVUeXBlIDAgPSovMzIpLnVpbnQ2NChtZXNzYWdlLmRpYW1vbmQpO1xuICAgICAgICBpZiAobWVzc2FnZS53b3JrYmVuY2hJdGVtcyAhPSBudWxsICYmIG1lc3NhZ2Uud29ya2JlbmNoSXRlbXMubGVuZ3RoKVxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtZXNzYWdlLndvcmtiZW5jaEl0ZW1zLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgICRyb290LldvcmtCZW5jaEl0ZW1JbmZvLmVuY29kZShtZXNzYWdlLndvcmtiZW5jaEl0ZW1zW2ldLCB3cml0ZXIudWludDMyKC8qIGlkIDUsIHdpcmVUeXBlIDIgPSovNDIpLmZvcmsoKSkubGRlbGltKCk7XG4gICAgICAgIHJldHVybiB3cml0ZXI7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEVuY29kZXMgdGhlIHNwZWNpZmllZCBMb2dpblJlcGx5IG1lc3NhZ2UsIGxlbmd0aCBkZWxpbWl0ZWQuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIExvZ2luUmVwbHkudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBMb2dpblJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUxvZ2luUmVwbHl9IG1lc3NhZ2UgTG9naW5SZXBseSBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgTG9naW5SZXBseS5lbmNvZGVEZWxpbWl0ZWQgPSBmdW5jdGlvbiBlbmNvZGVEZWxpbWl0ZWQobWVzc2FnZSwgd3JpdGVyKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmVuY29kZShtZXNzYWdlLCB3cml0ZXIpLmxkZWxpbSgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgTG9naW5SZXBseSBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVcbiAgICAgKiBAbWVtYmVyb2YgTG9naW5SZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtMb2dpblJlcGx5fSBMb2dpblJlcGx5XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBMb2dpblJlcGx5LmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LkxvZ2luUmVwbHkoKTtcbiAgICAgICAgd2hpbGUgKHJlYWRlci5wb3MgPCBlbmQpIHtcbiAgICAgICAgICAgIHZhciB0YWcgPSByZWFkZXIudWludDMyKCk7XG4gICAgICAgICAgICBzd2l0Y2ggKHRhZyA+Pj4gMykge1xuICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UudXNlckluZm8gPSAkcm9vdC5Vc2VySW5mby5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2Uuc2VydmVyVGltZSA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgMzpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW4gPSByZWFkZXIudWludDY0KCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIDQ6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5kaWFtb25kID0gcmVhZGVyLnVpbnQ2NCgpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSA1OlxuICAgICAgICAgICAgICAgIGlmICghKG1lc3NhZ2Uud29ya2JlbmNoSXRlbXMgJiYgbWVzc2FnZS53b3JrYmVuY2hJdGVtcy5sZW5ndGgpKVxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlLndvcmtiZW5jaEl0ZW1zID0gW107XG4gICAgICAgICAgICAgICAgbWVzc2FnZS53b3JrYmVuY2hJdGVtcy5wdXNoKCRyb290LldvcmtCZW5jaEl0ZW1JbmZvLmRlY29kZShyZWFkZXIsIHJlYWRlci51aW50MzIoKSkpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZWFkZXIuc2tpcFR5cGUodGFnICYgNyk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBMb2dpblJlcGx5IG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIsIGxlbmd0aCBkZWxpbWl0ZWQuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBMb2dpblJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtMb2dpblJlcGx5fSBMb2dpblJlcGx5XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBMb2dpblJlcGx5LmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhIExvZ2luUmVwbHkgbWVzc2FnZS5cbiAgICAgKiBAZnVuY3Rpb24gdmVyaWZ5XG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBMb2dpblJlcGx5LnZlcmlmeSA9IGZ1bmN0aW9uIHZlcmlmeShtZXNzYWdlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZSAhPT0gXCJvYmplY3RcIiB8fCBtZXNzYWdlID09PSBudWxsKVxuICAgICAgICAgICAgcmV0dXJuIFwib2JqZWN0IGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLnVzZXJJbmZvICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInVzZXJJbmZvXCIpKSB7XG4gICAgICAgICAgICB2YXIgZXJyb3IgPSAkcm9vdC5Vc2VySW5mby52ZXJpZnkobWVzc2FnZS51c2VySW5mbyk7XG4gICAgICAgICAgICBpZiAoZXJyb3IpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwidXNlckluZm8uXCIgKyBlcnJvcjtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWVzc2FnZS5zZXJ2ZXJUaW1lICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInNlcnZlclRpbWVcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLnNlcnZlclRpbWUpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcInNlcnZlclRpbWU6IGludGVnZXIgZXhwZWN0ZWRcIjtcbiAgICAgICAgaWYgKG1lc3NhZ2UuY29pbiAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJjb2luXCIpKVxuICAgICAgICAgICAgaWYgKCEkdXRpbC5pc0ludGVnZXIobWVzc2FnZS5jb2luKSAmJiAhKG1lc3NhZ2UuY29pbiAmJiAkdXRpbC5pc0ludGVnZXIobWVzc2FnZS5jb2luLmxvdykgJiYgJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UuY29pbi5oaWdoKSkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiY29pbjogaW50ZWdlcnxMb25nIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLmRpYW1vbmQgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiZGlhbW9uZFwiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UuZGlhbW9uZCkgJiYgIShtZXNzYWdlLmRpYW1vbmQgJiYgJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UuZGlhbW9uZC5sb3cpICYmICR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLmRpYW1vbmQuaGlnaCkpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcImRpYW1vbmQ6IGludGVnZXJ8TG9uZyBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS53b3JrYmVuY2hJdGVtcyAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJ3b3JrYmVuY2hJdGVtc1wiKSkge1xuICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KG1lc3NhZ2Uud29ya2JlbmNoSXRlbXMpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcIndvcmtiZW5jaEl0ZW1zOiBhcnJheSBleHBlY3RlZFwiO1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtZXNzYWdlLndvcmtiZW5jaEl0ZW1zLmxlbmd0aDsgKytpKSB7XG4gICAgICAgICAgICAgICAgdmFyIGVycm9yID0gJHJvb3QuV29ya0JlbmNoSXRlbUluZm8udmVyaWZ5KG1lc3NhZ2Uud29ya2JlbmNoSXRlbXNbaV0pO1xuICAgICAgICAgICAgICAgIGlmIChlcnJvcilcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwid29ya2JlbmNoSXRlbXMuXCIgKyBlcnJvcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIExvZ2luUmVwbHkgbWVzc2FnZSBmcm9tIGEgcGxhaW4gb2JqZWN0LiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byB0aGVpciByZXNwZWN0aXZlIGludGVybmFsIHR5cGVzLlxuICAgICAqIEBmdW5jdGlvbiBmcm9tT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gb2JqZWN0IFBsYWluIG9iamVjdFxuICAgICAqIEByZXR1cm5zIHtMb2dpblJlcGx5fSBMb2dpblJlcGx5XG4gICAgICovXG4gICAgTG9naW5SZXBseS5mcm9tT2JqZWN0ID0gZnVuY3Rpb24gZnJvbU9iamVjdChvYmplY3QpIHtcbiAgICAgICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mICRyb290LkxvZ2luUmVwbHkpXG4gICAgICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyAkcm9vdC5Mb2dpblJlcGx5KCk7XG4gICAgICAgIGlmIChvYmplY3QudXNlckluZm8gIT0gbnVsbCkge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBvYmplY3QudXNlckluZm8gIT09IFwib2JqZWN0XCIpXG4gICAgICAgICAgICAgICAgdGhyb3cgVHlwZUVycm9yKFwiLkxvZ2luUmVwbHkudXNlckluZm86IG9iamVjdCBleHBlY3RlZFwiKTtcbiAgICAgICAgICAgIG1lc3NhZ2UudXNlckluZm8gPSAkcm9vdC5Vc2VySW5mby5mcm9tT2JqZWN0KG9iamVjdC51c2VySW5mbyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG9iamVjdC5zZXJ2ZXJUaW1lICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLnNlcnZlclRpbWUgPSBvYmplY3Quc2VydmVyVGltZSA+Pj4gMDtcbiAgICAgICAgaWYgKG9iamVjdC5jb2luICE9IG51bGwpXG4gICAgICAgICAgICBpZiAoJHV0aWwuTG9uZylcbiAgICAgICAgICAgICAgICAobWVzc2FnZS5jb2luID0gJHV0aWwuTG9uZy5mcm9tVmFsdWUob2JqZWN0LmNvaW4pKS51bnNpZ25lZCA9IHRydWU7XG4gICAgICAgICAgICBlbHNlIGlmICh0eXBlb2Ygb2JqZWN0LmNvaW4gPT09IFwic3RyaW5nXCIpXG4gICAgICAgICAgICAgICAgbWVzc2FnZS5jb2luID0gcGFyc2VJbnQob2JqZWN0LmNvaW4sIDEwKTtcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QuY29pbiA9PT0gXCJudW1iZXJcIilcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW4gPSBvYmplY3QuY29pbjtcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QuY29pbiA9PT0gXCJvYmplY3RcIilcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW4gPSBuZXcgJHV0aWwuTG9uZ0JpdHMob2JqZWN0LmNvaW4ubG93ID4+PiAwLCBvYmplY3QuY29pbi5oaWdoID4+PiAwKS50b051bWJlcih0cnVlKTtcbiAgICAgICAgaWYgKG9iamVjdC5kaWFtb25kICE9IG51bGwpXG4gICAgICAgICAgICBpZiAoJHV0aWwuTG9uZylcbiAgICAgICAgICAgICAgICAobWVzc2FnZS5kaWFtb25kID0gJHV0aWwuTG9uZy5mcm9tVmFsdWUob2JqZWN0LmRpYW1vbmQpKS51bnNpZ25lZCA9IHRydWU7XG4gICAgICAgICAgICBlbHNlIGlmICh0eXBlb2Ygb2JqZWN0LmRpYW1vbmQgPT09IFwic3RyaW5nXCIpXG4gICAgICAgICAgICAgICAgbWVzc2FnZS5kaWFtb25kID0gcGFyc2VJbnQob2JqZWN0LmRpYW1vbmQsIDEwKTtcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QuZGlhbW9uZCA9PT0gXCJudW1iZXJcIilcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmRpYW1vbmQgPSBvYmplY3QuZGlhbW9uZDtcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QuZGlhbW9uZCA9PT0gXCJvYmplY3RcIilcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmRpYW1vbmQgPSBuZXcgJHV0aWwuTG9uZ0JpdHMob2JqZWN0LmRpYW1vbmQubG93ID4+PiAwLCBvYmplY3QuZGlhbW9uZC5oaWdoID4+PiAwKS50b051bWJlcih0cnVlKTtcbiAgICAgICAgaWYgKG9iamVjdC53b3JrYmVuY2hJdGVtcykge1xuICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KG9iamVjdC53b3JrYmVuY2hJdGVtcykpXG4gICAgICAgICAgICAgICAgdGhyb3cgVHlwZUVycm9yKFwiLkxvZ2luUmVwbHkud29ya2JlbmNoSXRlbXM6IGFycmF5IGV4cGVjdGVkXCIpO1xuICAgICAgICAgICAgbWVzc2FnZS53b3JrYmVuY2hJdGVtcyA9IFtdO1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBvYmplY3Qud29ya2JlbmNoSXRlbXMubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG9iamVjdC53b3JrYmVuY2hJdGVtc1tpXSAhPT0gXCJvYmplY3RcIilcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgVHlwZUVycm9yKFwiLkxvZ2luUmVwbHkud29ya2JlbmNoSXRlbXM6IG9iamVjdCBleHBlY3RlZFwiKTtcbiAgICAgICAgICAgICAgICBtZXNzYWdlLndvcmtiZW5jaEl0ZW1zW2ldID0gJHJvb3QuV29ya0JlbmNoSXRlbUluZm8uZnJvbU9iamVjdChvYmplY3Qud29ya2JlbmNoSXRlbXNbaV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgcGxhaW4gb2JqZWN0IGZyb20gYSBMb2dpblJlcGx5IG1lc3NhZ2UuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIG90aGVyIHR5cGVzIGlmIHNwZWNpZmllZC5cbiAgICAgKiBAZnVuY3Rpb24gdG9PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgTG9naW5SZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0xvZ2luUmVwbHl9IG1lc3NhZ2UgTG9naW5SZXBseVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLklDb252ZXJzaW9uT3B0aW9uc30gW29wdGlvbnNdIENvbnZlcnNpb24gb3B0aW9uc1xuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gUGxhaW4gb2JqZWN0XG4gICAgICovXG4gICAgTG9naW5SZXBseS50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG1lc3NhZ2UsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgICB2YXIgb2JqZWN0ID0ge307XG4gICAgICAgIGlmIChvcHRpb25zLmFycmF5cyB8fCBvcHRpb25zLmRlZmF1bHRzKVxuICAgICAgICAgICAgb2JqZWN0LndvcmtiZW5jaEl0ZW1zID0gW107XG4gICAgICAgIGlmIChvcHRpb25zLmRlZmF1bHRzKSB7XG4gICAgICAgICAgICBvYmplY3QudXNlckluZm8gPSBudWxsO1xuICAgICAgICAgICAgb2JqZWN0LnNlcnZlclRpbWUgPSAwO1xuICAgICAgICAgICAgaWYgKCR1dGlsLkxvbmcpIHtcbiAgICAgICAgICAgICAgICB2YXIgbG9uZyA9IG5ldyAkdXRpbC5Mb25nKDAsIDAsIHRydWUpO1xuICAgICAgICAgICAgICAgIG9iamVjdC5jb2luID0gb3B0aW9ucy5sb25ncyA9PT0gU3RyaW5nID8gbG9uZy50b1N0cmluZygpIDogb3B0aW9ucy5sb25ncyA9PT0gTnVtYmVyID8gbG9uZy50b051bWJlcigpIDogbG9uZztcbiAgICAgICAgICAgIH0gZWxzZVxuICAgICAgICAgICAgICAgIG9iamVjdC5jb2luID0gb3B0aW9ucy5sb25ncyA9PT0gU3RyaW5nID8gXCIwXCIgOiAwO1xuICAgICAgICAgICAgaWYgKCR1dGlsLkxvbmcpIHtcbiAgICAgICAgICAgICAgICB2YXIgbG9uZyA9IG5ldyAkdXRpbC5Mb25nKDAsIDAsIHRydWUpO1xuICAgICAgICAgICAgICAgIG9iamVjdC5kaWFtb25kID0gb3B0aW9ucy5sb25ncyA9PT0gU3RyaW5nID8gbG9uZy50b1N0cmluZygpIDogb3B0aW9ucy5sb25ncyA9PT0gTnVtYmVyID8gbG9uZy50b051bWJlcigpIDogbG9uZztcbiAgICAgICAgICAgIH0gZWxzZVxuICAgICAgICAgICAgICAgIG9iamVjdC5kaWFtb25kID0gb3B0aW9ucy5sb25ncyA9PT0gU3RyaW5nID8gXCIwXCIgOiAwO1xuICAgICAgICB9XG4gICAgICAgIGlmIChtZXNzYWdlLnVzZXJJbmZvICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInVzZXJJbmZvXCIpKVxuICAgICAgICAgICAgb2JqZWN0LnVzZXJJbmZvID0gJHJvb3QuVXNlckluZm8udG9PYmplY3QobWVzc2FnZS51c2VySW5mbywgb3B0aW9ucyk7XG4gICAgICAgIGlmIChtZXNzYWdlLnNlcnZlclRpbWUgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwic2VydmVyVGltZVwiKSlcbiAgICAgICAgICAgIG9iamVjdC5zZXJ2ZXJUaW1lID0gbWVzc2FnZS5zZXJ2ZXJUaW1lO1xuICAgICAgICBpZiAobWVzc2FnZS5jb2luICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImNvaW5cIikpXG4gICAgICAgICAgICBpZiAodHlwZW9mIG1lc3NhZ2UuY29pbiA9PT0gXCJudW1iZXJcIilcbiAgICAgICAgICAgICAgICBvYmplY3QuY29pbiA9IG9wdGlvbnMubG9uZ3MgPT09IFN0cmluZyA/IFN0cmluZyhtZXNzYWdlLmNvaW4pIDogbWVzc2FnZS5jb2luO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIG9iamVjdC5jb2luID0gb3B0aW9ucy5sb25ncyA9PT0gU3RyaW5nID8gJHV0aWwuTG9uZy5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChtZXNzYWdlLmNvaW4pIDogb3B0aW9ucy5sb25ncyA9PT0gTnVtYmVyID8gbmV3ICR1dGlsLkxvbmdCaXRzKG1lc3NhZ2UuY29pbi5sb3cgPj4+IDAsIG1lc3NhZ2UuY29pbi5oaWdoID4+PiAwKS50b051bWJlcih0cnVlKSA6IG1lc3NhZ2UuY29pbjtcbiAgICAgICAgaWYgKG1lc3NhZ2UuZGlhbW9uZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJkaWFtb25kXCIpKVxuICAgICAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlLmRpYW1vbmQgPT09IFwibnVtYmVyXCIpXG4gICAgICAgICAgICAgICAgb2JqZWN0LmRpYW1vbmQgPSBvcHRpb25zLmxvbmdzID09PSBTdHJpbmcgPyBTdHJpbmcobWVzc2FnZS5kaWFtb25kKSA6IG1lc3NhZ2UuZGlhbW9uZDtcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICBvYmplY3QuZGlhbW9uZCA9IG9wdGlvbnMubG9uZ3MgPT09IFN0cmluZyA/ICR1dGlsLkxvbmcucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobWVzc2FnZS5kaWFtb25kKSA6IG9wdGlvbnMubG9uZ3MgPT09IE51bWJlciA/IG5ldyAkdXRpbC5Mb25nQml0cyhtZXNzYWdlLmRpYW1vbmQubG93ID4+PiAwLCBtZXNzYWdlLmRpYW1vbmQuaGlnaCA+Pj4gMCkudG9OdW1iZXIodHJ1ZSkgOiBtZXNzYWdlLmRpYW1vbmQ7XG4gICAgICAgIGlmIChtZXNzYWdlLndvcmtiZW5jaEl0ZW1zICYmIG1lc3NhZ2Uud29ya2JlbmNoSXRlbXMubGVuZ3RoKSB7XG4gICAgICAgICAgICBvYmplY3Qud29ya2JlbmNoSXRlbXMgPSBbXTtcbiAgICAgICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgbWVzc2FnZS53b3JrYmVuY2hJdGVtcy5sZW5ndGg7ICsrailcbiAgICAgICAgICAgICAgICBvYmplY3Qud29ya2JlbmNoSXRlbXNbal0gPSAkcm9vdC5Xb3JrQmVuY2hJdGVtSW5mby50b09iamVjdChtZXNzYWdlLndvcmtiZW5jaEl0ZW1zW2pdLCBvcHRpb25zKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGlzIExvZ2luUmVwbHkgdG8gSlNPTi5cbiAgICAgKiBAZnVuY3Rpb24gdG9KU09OXG4gICAgICogQG1lbWJlcm9mIExvZ2luUmVwbHlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IEpTT04gb2JqZWN0XG4gICAgICovXG4gICAgTG9naW5SZXBseS5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci50b09iamVjdCh0aGlzLCAkcHJvdG9idWYudXRpbC50b0pTT05PcHRpb25zKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIExvZ2luUmVwbHk7XG59KSgpO1xuXG4kcm9vdC5CdXlQcm9kdWNlUmVxdWVzdCA9IChmdW5jdGlvbigpIHtcblxuICAgIC8qKlxuICAgICAqIFByb3BlcnRpZXMgb2YgYSBCdXlQcm9kdWNlUmVxdWVzdC5cbiAgICAgKiBAZXhwb3J0cyBJQnV5UHJvZHVjZVJlcXVlc3RcbiAgICAgKiBAaW50ZXJmYWNlIElCdXlQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBwcm9wZXJ0eSB7bnVtYmVyfG51bGx9IFtwcm9kdWNlTGV2ZWxdIEJ1eVByb2R1Y2VSZXF1ZXN0IHByb2R1Y2VMZXZlbFxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ29uc3RydWN0cyBhIG5ldyBCdXlQcm9kdWNlUmVxdWVzdC5cbiAgICAgKiBAZXhwb3J0cyBCdXlQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBjbGFzc2Rlc2MgUmVwcmVzZW50cyBhIEJ1eVByb2R1Y2VSZXF1ZXN0LlxuICAgICAqIEBpbXBsZW1lbnRzIElCdXlQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7SUJ1eVByb2R1Y2VSZXF1ZXN0PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICovXG4gICAgZnVuY3Rpb24gQnV5UHJvZHVjZVJlcXVlc3QocHJvcGVydGllcykge1xuICAgICAgICBpZiAocHJvcGVydGllcylcbiAgICAgICAgICAgIGZvciAodmFyIGtleXMgPSBPYmplY3Qua2V5cyhwcm9wZXJ0aWVzKSwgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgIGlmIChwcm9wZXJ0aWVzW2tleXNbaV1dICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXNba2V5c1tpXV0gPSBwcm9wZXJ0aWVzW2tleXNbaV1dO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEJ1eVByb2R1Y2VSZXF1ZXN0IHByb2R1Y2VMZXZlbC5cbiAgICAgKiBAbWVtYmVyIHtudW1iZXJ9IHByb2R1Y2VMZXZlbFxuICAgICAqIEBtZW1iZXJvZiBCdXlQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqL1xuICAgIEJ1eVByb2R1Y2VSZXF1ZXN0LnByb3RvdHlwZS5wcm9kdWNlTGV2ZWwgPSAwO1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBCdXlQcm9kdWNlUmVxdWVzdCBpbnN0YW5jZSB1c2luZyB0aGUgc3BlY2lmaWVkIHByb3BlcnRpZXMuXG4gICAgICogQGZ1bmN0aW9uIGNyZWF0ZVxuICAgICAqIEBtZW1iZXJvZiBCdXlQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lCdXlQcm9kdWNlUmVxdWVzdD19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqIEByZXR1cm5zIHtCdXlQcm9kdWNlUmVxdWVzdH0gQnV5UHJvZHVjZVJlcXVlc3QgaW5zdGFuY2VcbiAgICAgKi9cbiAgICBCdXlQcm9kdWNlUmVxdWVzdC5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUocHJvcGVydGllcykge1xuICAgICAgICByZXR1cm4gbmV3IEJ1eVByb2R1Y2VSZXF1ZXN0KHByb3BlcnRpZXMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgQnV5UHJvZHVjZVJlcXVlc3QgbWVzc2FnZS4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgQnV5UHJvZHVjZVJlcXVlc3QudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZVxuICAgICAqIEBtZW1iZXJvZiBCdXlQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lCdXlQcm9kdWNlUmVxdWVzdH0gbWVzc2FnZSBCdXlQcm9kdWNlUmVxdWVzdCBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcXVlc3QuZW5jb2RlID0gZnVuY3Rpb24gZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICBpZiAoIXdyaXRlcilcbiAgICAgICAgICAgIHdyaXRlciA9ICRXcml0ZXIuY3JlYXRlKCk7XG4gICAgICAgIGlmIChtZXNzYWdlLnByb2R1Y2VMZXZlbCAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwicHJvZHVjZUxldmVsXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAxLCB3aXJlVHlwZSAwID0qLzgpLmludDMyKG1lc3NhZ2UucHJvZHVjZUxldmVsKTtcbiAgICAgICAgcmV0dXJuIHdyaXRlcjtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIEJ1eVByb2R1Y2VSZXF1ZXN0IG1lc3NhZ2UsIGxlbmd0aCBkZWxpbWl0ZWQuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIEJ1eVByb2R1Y2VSZXF1ZXN0LnZlcmlmeXx2ZXJpZnl9IG1lc3NhZ2VzLlxuICAgICAqIEBmdW5jdGlvbiBlbmNvZGVEZWxpbWl0ZWRcbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJQnV5UHJvZHVjZVJlcXVlc3R9IG1lc3NhZ2UgQnV5UHJvZHVjZVJlcXVlc3QgbWVzc2FnZSBvciBwbGFpbiBvYmplY3QgdG8gZW5jb2RlXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuV3JpdGVyfSBbd3JpdGVyXSBXcml0ZXIgdG8gZW5jb2RlIHRvXG4gICAgICogQHJldHVybnMgeyRwcm90b2J1Zi5Xcml0ZXJ9IFdyaXRlclxuICAgICAqL1xuICAgIEJ1eVByb2R1Y2VSZXF1ZXN0LmVuY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZChtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikubGRlbGltKCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBCdXlQcm9kdWNlUmVxdWVzdCBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVcbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUmVhZGVyfFVpbnQ4QXJyYXl9IHJlYWRlciBSZWFkZXIgb3IgYnVmZmVyIHRvIGRlY29kZSBmcm9tXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFtsZW5ndGhdIE1lc3NhZ2UgbGVuZ3RoIGlmIGtub3duIGJlZm9yZWhhbmRcbiAgICAgKiBAcmV0dXJucyB7QnV5UHJvZHVjZVJlcXVlc3R9IEJ1eVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBCdXlQcm9kdWNlUmVxdWVzdC5kZWNvZGUgPSBmdW5jdGlvbiBkZWNvZGUocmVhZGVyLCBsZW5ndGgpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSAkUmVhZGVyLmNyZWF0ZShyZWFkZXIpO1xuICAgICAgICB2YXIgZW5kID0gbGVuZ3RoID09PSB1bmRlZmluZWQgPyByZWFkZXIubGVuIDogcmVhZGVyLnBvcyArIGxlbmd0aCwgbWVzc2FnZSA9IG5ldyAkcm9vdC5CdXlQcm9kdWNlUmVxdWVzdCgpO1xuICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZCkge1xuICAgICAgICAgICAgdmFyIHRhZyA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5wcm9kdWNlTGV2ZWwgPSByZWFkZXIuaW50MzIoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgQnV5UHJvZHVjZVJlcXVlc3QgbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlciwgbGVuZ3RoIGRlbGltaXRlZC5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIEJ1eVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtCdXlQcm9kdWNlUmVxdWVzdH0gQnV5UHJvZHVjZVJlcXVlc3RcbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIEJ1eVByb2R1Y2VSZXF1ZXN0LmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhIEJ1eVByb2R1Y2VSZXF1ZXN0IG1lc3NhZ2UuXG4gICAgICogQGZ1bmN0aW9uIHZlcmlmeVxuICAgICAqIEBtZW1iZXJvZiBCdXlQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBtZXNzYWdlIFBsYWluIG9iamVjdCB0byB2ZXJpZnlcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfG51bGx9IGBudWxsYCBpZiB2YWxpZCwgb3RoZXJ3aXNlIHRoZSByZWFzb24gd2h5IGl0IGlzIG5vdFxuICAgICAqL1xuICAgIEJ1eVByb2R1Y2VSZXF1ZXN0LnZlcmlmeSA9IGZ1bmN0aW9uIHZlcmlmeShtZXNzYWdlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZSAhPT0gXCJvYmplY3RcIiB8fCBtZXNzYWdlID09PSBudWxsKVxuICAgICAgICAgICAgcmV0dXJuIFwib2JqZWN0IGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLnByb2R1Y2VMZXZlbCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJwcm9kdWNlTGV2ZWxcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLnByb2R1Y2VMZXZlbCkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwicHJvZHVjZUxldmVsOiBpbnRlZ2VyIGV4cGVjdGVkXCI7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgQnV5UHJvZHVjZVJlcXVlc3QgbWVzc2FnZSBmcm9tIGEgcGxhaW4gb2JqZWN0LiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byB0aGVpciByZXNwZWN0aXZlIGludGVybmFsIHR5cGVzLlxuICAgICAqIEBmdW5jdGlvbiBmcm9tT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIEJ1eVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7T2JqZWN0LjxzdHJpbmcsKj59IG9iamVjdCBQbGFpbiBvYmplY3RcbiAgICAgKiBAcmV0dXJucyB7QnV5UHJvZHVjZVJlcXVlc3R9IEJ1eVByb2R1Y2VSZXF1ZXN0XG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcXVlc3QuZnJvbU9iamVjdCA9IGZ1bmN0aW9uIGZyb21PYmplY3Qob2JqZWN0KSB7XG4gICAgICAgIGlmIChvYmplY3QgaW5zdGFuY2VvZiAkcm9vdC5CdXlQcm9kdWNlUmVxdWVzdClcbiAgICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgICAgIHZhciBtZXNzYWdlID0gbmV3ICRyb290LkJ1eVByb2R1Y2VSZXF1ZXN0KCk7XG4gICAgICAgIGlmIChvYmplY3QucHJvZHVjZUxldmVsICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLnByb2R1Y2VMZXZlbCA9IG9iamVjdC5wcm9kdWNlTGV2ZWwgfCAwO1xuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIHBsYWluIG9iamVjdCBmcm9tIGEgQnV5UHJvZHVjZVJlcXVlc3QgbWVzc2FnZS4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gb3RoZXIgdHlwZXMgaWYgc3BlY2lmaWVkLlxuICAgICAqIEBmdW5jdGlvbiB0b09iamVjdFxuICAgICAqIEBtZW1iZXJvZiBCdXlQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0J1eVByb2R1Y2VSZXF1ZXN0fSBtZXNzYWdlIEJ1eVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuSUNvbnZlcnNpb25PcHRpb25zfSBbb3B0aW9uc10gQ29udmVyc2lvbiBvcHRpb25zXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBQbGFpbiBvYmplY3RcbiAgICAgKi9cbiAgICBCdXlQcm9kdWNlUmVxdWVzdC50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG1lc3NhZ2UsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgICB2YXIgb2JqZWN0ID0ge307XG4gICAgICAgIGlmIChvcHRpb25zLmRlZmF1bHRzKVxuICAgICAgICAgICAgb2JqZWN0LnByb2R1Y2VMZXZlbCA9IDA7XG4gICAgICAgIGlmIChtZXNzYWdlLnByb2R1Y2VMZXZlbCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJwcm9kdWNlTGV2ZWxcIikpXG4gICAgICAgICAgICBvYmplY3QucHJvZHVjZUxldmVsID0gbWVzc2FnZS5wcm9kdWNlTGV2ZWw7XG4gICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENvbnZlcnRzIHRoaXMgQnV5UHJvZHVjZVJlcXVlc3QgdG8gSlNPTi5cbiAgICAgKiBAZnVuY3Rpb24gdG9KU09OXG4gICAgICogQG1lbWJlcm9mIEJ1eVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQGluc3RhbmNlXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBKU09OIG9iamVjdFxuICAgICAqL1xuICAgIEJ1eVByb2R1Y2VSZXF1ZXN0LnByb3RvdHlwZS50b0pTT04gPSBmdW5jdGlvbiB0b0pTT04oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbnN0cnVjdG9yLnRvT2JqZWN0KHRoaXMsICRwcm90b2J1Zi51dGlsLnRvSlNPTk9wdGlvbnMpO1xuICAgIH07XG5cbiAgICByZXR1cm4gQnV5UHJvZHVjZVJlcXVlc3Q7XG59KSgpO1xuXG4kcm9vdC5CdXlQcm9kdWNlUmVwbHkgPSAoZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBQcm9wZXJ0aWVzIG9mIGEgQnV5UHJvZHVjZVJlcGx5LlxuICAgICAqIEBleHBvcnRzIElCdXlQcm9kdWNlUmVwbHlcbiAgICAgKiBAaW50ZXJmYWNlIElCdXlQcm9kdWNlUmVwbHlcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbZXJyY29kZV0gQnV5UHJvZHVjZVJlcGx5IGVycmNvZGVcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbcHJvZHVjZUxldmVsXSBCdXlQcm9kdWNlUmVwbHkgcHJvZHVjZUxldmVsXG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8bnVsbH0gW3Bvc2l0aW9uXSBCdXlQcm9kdWNlUmVwbHkgcG9zaXRpb25cbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIENvbnN0cnVjdHMgYSBuZXcgQnV5UHJvZHVjZVJlcGx5LlxuICAgICAqIEBleHBvcnRzIEJ1eVByb2R1Y2VSZXBseVxuICAgICAqIEBjbGFzc2Rlc2MgUmVwcmVzZW50cyBhIEJ1eVByb2R1Y2VSZXBseS5cbiAgICAgKiBAaW1wbGVtZW50cyBJQnV5UHJvZHVjZVJlcGx5XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICogQHBhcmFtIHtJQnV5UHJvZHVjZVJlcGx5PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICovXG4gICAgZnVuY3Rpb24gQnV5UHJvZHVjZVJlcGx5KHByb3BlcnRpZXMpIHtcbiAgICAgICAgaWYgKHByb3BlcnRpZXMpXG4gICAgICAgICAgICBmb3IgKHZhciBrZXlzID0gT2JqZWN0LmtleXMocHJvcGVydGllcyksIGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7ICsraSlcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydGllc1trZXlzW2ldXSAhPSBudWxsKVxuICAgICAgICAgICAgICAgICAgICB0aGlzW2tleXNbaV1dID0gcHJvcGVydGllc1trZXlzW2ldXTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBCdXlQcm9kdWNlUmVwbHkgZXJyY29kZS5cbiAgICAgKiBAbWVtYmVyIHtudW1iZXJ9IGVycmNvZGVcbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcGx5XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LnByb3RvdHlwZS5lcnJjb2RlID0gMDtcblxuICAgIC8qKlxuICAgICAqIEJ1eVByb2R1Y2VSZXBseSBwcm9kdWNlTGV2ZWwuXG4gICAgICogQG1lbWJlciB7bnVtYmVyfSBwcm9kdWNlTGV2ZWxcbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcGx5XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LnByb3RvdHlwZS5wcm9kdWNlTGV2ZWwgPSAwO1xuXG4gICAgLyoqXG4gICAgICogQnV5UHJvZHVjZVJlcGx5IHBvc2l0aW9uLlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gcG9zaXRpb25cbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcGx5XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LnByb3RvdHlwZS5wb3NpdGlvbiA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IEJ1eVByb2R1Y2VSZXBseSBpbnN0YW5jZSB1c2luZyB0aGUgc3BlY2lmaWVkIHByb3BlcnRpZXMuXG4gICAgICogQGZ1bmN0aW9uIGNyZWF0ZVxuICAgICAqIEBtZW1iZXJvZiBCdXlQcm9kdWNlUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJQnV5UHJvZHVjZVJlcGx5PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICogQHJldHVybnMge0J1eVByb2R1Y2VSZXBseX0gQnV5UHJvZHVjZVJlcGx5IGluc3RhbmNlXG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShwcm9wZXJ0aWVzKSB7XG4gICAgICAgIHJldHVybiBuZXcgQnV5UHJvZHVjZVJlcGx5KHByb3BlcnRpZXMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgQnV5UHJvZHVjZVJlcGx5IG1lc3NhZ2UuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIEJ1eVByb2R1Y2VSZXBseS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlXG4gICAgICogQG1lbWJlcm9mIEJ1eVByb2R1Y2VSZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lCdXlQcm9kdWNlUmVwbHl9IG1lc3NhZ2UgQnV5UHJvZHVjZVJlcGx5IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBCdXlQcm9kdWNlUmVwbHkuZW5jb2RlID0gZnVuY3Rpb24gZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICBpZiAoIXdyaXRlcilcbiAgICAgICAgICAgIHdyaXRlciA9ICRXcml0ZXIuY3JlYXRlKCk7XG4gICAgICAgIGlmIChtZXNzYWdlLmVycmNvZGUgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImVycmNvZGVcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDEsIHdpcmVUeXBlIDAgPSovOCkuaW50MzIobWVzc2FnZS5lcnJjb2RlKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UucHJvZHVjZUxldmVsICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJwcm9kdWNlTGV2ZWxcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDIsIHdpcmVUeXBlIDAgPSovMTYpLmludDMyKG1lc3NhZ2UucHJvZHVjZUxldmVsKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UucG9zaXRpb24gIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcInBvc2l0aW9uXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAzLCB3aXJlVHlwZSAwID0qLzI0KS5pbnQzMihtZXNzYWdlLnBvc2l0aW9uKTtcbiAgICAgICAgcmV0dXJuIHdyaXRlcjtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIEJ1eVByb2R1Y2VSZXBseSBtZXNzYWdlLCBsZW5ndGggZGVsaW1pdGVkLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBCdXlQcm9kdWNlUmVwbHkudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBCdXlQcm9kdWNlUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJQnV5UHJvZHVjZVJlcGx5fSBtZXNzYWdlIEJ1eVByb2R1Y2VSZXBseSBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LmVuY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZChtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikubGRlbGltKCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBCdXlQcm9kdWNlUmVwbHkgbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlci5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlXG4gICAgICogQG1lbWJlcm9mIEJ1eVByb2R1Y2VSZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtCdXlQcm9kdWNlUmVwbHl9IEJ1eVByb2R1Y2VSZXBseVxuICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICogQHRocm93cyB7JHByb3RvYnVmLnV0aWwuUHJvdG9jb2xFcnJvcn0gSWYgcmVxdWlyZWQgZmllbGRzIGFyZSBtaXNzaW5nXG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LkJ1eVByb2R1Y2VSZXBseSgpO1xuICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZCkge1xuICAgICAgICAgICAgdmFyIHRhZyA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5lcnJjb2RlID0gcmVhZGVyLmludDMyKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5wcm9kdWNlTGV2ZWwgPSByZWFkZXIuaW50MzIoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgMzpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLnBvc2l0aW9uID0gcmVhZGVyLmludDMyKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHJlYWRlci5za2lwVHlwZSh0YWcgJiA3KTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhIEJ1eVByb2R1Y2VSZXBseSBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLCBsZW5ndGggZGVsaW1pdGVkLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVEZWxpbWl0ZWRcbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtCdXlQcm9kdWNlUmVwbHl9IEJ1eVByb2R1Y2VSZXBseVxuICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICogQHRocm93cyB7JHByb3RvYnVmLnV0aWwuUHJvdG9jb2xFcnJvcn0gSWYgcmVxdWlyZWQgZmllbGRzIGFyZSBtaXNzaW5nXG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhIEJ1eVByb2R1Y2VSZXBseSBtZXNzYWdlLlxuICAgICAqIEBmdW5jdGlvbiB2ZXJpZnlcbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7T2JqZWN0LjxzdHJpbmcsKj59IG1lc3NhZ2UgUGxhaW4gb2JqZWN0IHRvIHZlcmlmeVxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd8bnVsbH0gYG51bGxgIGlmIHZhbGlkLCBvdGhlcndpc2UgdGhlIHJlYXNvbiB3aHkgaXQgaXMgbm90XG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LnZlcmlmeSA9IGZ1bmN0aW9uIHZlcmlmeShtZXNzYWdlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZSAhPT0gXCJvYmplY3RcIiB8fCBtZXNzYWdlID09PSBudWxsKVxuICAgICAgICAgICAgcmV0dXJuIFwib2JqZWN0IGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLmVycmNvZGUgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiZXJyY29kZVwiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UuZXJyY29kZSkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiZXJyY29kZTogaW50ZWdlciBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5wcm9kdWNlTGV2ZWwgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwicHJvZHVjZUxldmVsXCIpKVxuICAgICAgICAgICAgaWYgKCEkdXRpbC5pc0ludGVnZXIobWVzc2FnZS5wcm9kdWNlTGV2ZWwpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcInByb2R1Y2VMZXZlbDogaW50ZWdlciBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5wb3NpdGlvbiAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJwb3NpdGlvblwiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UucG9zaXRpb24pKVxuICAgICAgICAgICAgICAgIHJldHVybiBcInBvc2l0aW9uOiBpbnRlZ2VyIGV4cGVjdGVkXCI7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgQnV5UHJvZHVjZVJlcGx5IG1lc3NhZ2UgZnJvbSBhIHBsYWluIG9iamVjdC4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gdGhlaXIgcmVzcGVjdGl2ZSBpbnRlcm5hbCB0eXBlcy5cbiAgICAgKiBAZnVuY3Rpb24gZnJvbU9iamVjdFxuICAgICAqIEBtZW1iZXJvZiBCdXlQcm9kdWNlUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gb2JqZWN0IFBsYWluIG9iamVjdFxuICAgICAqIEByZXR1cm5zIHtCdXlQcm9kdWNlUmVwbHl9IEJ1eVByb2R1Y2VSZXBseVxuICAgICAqL1xuICAgIEJ1eVByb2R1Y2VSZXBseS5mcm9tT2JqZWN0ID0gZnVuY3Rpb24gZnJvbU9iamVjdChvYmplY3QpIHtcbiAgICAgICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mICRyb290LkJ1eVByb2R1Y2VSZXBseSlcbiAgICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgICAgIHZhciBtZXNzYWdlID0gbmV3ICRyb290LkJ1eVByb2R1Y2VSZXBseSgpO1xuICAgICAgICBpZiAob2JqZWN0LmVycmNvZGUgIT0gbnVsbClcbiAgICAgICAgICAgIG1lc3NhZ2UuZXJyY29kZSA9IG9iamVjdC5lcnJjb2RlIHwgMDtcbiAgICAgICAgaWYgKG9iamVjdC5wcm9kdWNlTGV2ZWwgIT0gbnVsbClcbiAgICAgICAgICAgIG1lc3NhZ2UucHJvZHVjZUxldmVsID0gb2JqZWN0LnByb2R1Y2VMZXZlbCB8IDA7XG4gICAgICAgIGlmIChvYmplY3QucG9zaXRpb24gIT0gbnVsbClcbiAgICAgICAgICAgIG1lc3NhZ2UucG9zaXRpb24gPSBvYmplY3QucG9zaXRpb24gfCAwO1xuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIHBsYWluIG9iamVjdCBmcm9tIGEgQnV5UHJvZHVjZVJlcGx5IG1lc3NhZ2UuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIG90aGVyIHR5cGVzIGlmIHNwZWNpZmllZC5cbiAgICAgKiBAZnVuY3Rpb24gdG9PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7QnV5UHJvZHVjZVJlcGx5fSBtZXNzYWdlIEJ1eVByb2R1Y2VSZXBseVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLklDb252ZXJzaW9uT3B0aW9uc30gW29wdGlvbnNdIENvbnZlcnNpb24gb3B0aW9uc1xuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gUGxhaW4gb2JqZWN0XG4gICAgICovXG4gICAgQnV5UHJvZHVjZVJlcGx5LnRvT2JqZWN0ID0gZnVuY3Rpb24gdG9PYmplY3QobWVzc2FnZSwgb3B0aW9ucykge1xuICAgICAgICBpZiAoIW9wdGlvbnMpXG4gICAgICAgICAgICBvcHRpb25zID0ge307XG4gICAgICAgIHZhciBvYmplY3QgPSB7fTtcbiAgICAgICAgaWYgKG9wdGlvbnMuZGVmYXVsdHMpIHtcbiAgICAgICAgICAgIG9iamVjdC5lcnJjb2RlID0gMDtcbiAgICAgICAgICAgIG9iamVjdC5wcm9kdWNlTGV2ZWwgPSAwO1xuICAgICAgICAgICAgb2JqZWN0LnBvc2l0aW9uID0gMDtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWVzc2FnZS5lcnJjb2RlICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImVycmNvZGVcIikpXG4gICAgICAgICAgICBvYmplY3QuZXJyY29kZSA9IG1lc3NhZ2UuZXJyY29kZTtcbiAgICAgICAgaWYgKG1lc3NhZ2UucHJvZHVjZUxldmVsICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInByb2R1Y2VMZXZlbFwiKSlcbiAgICAgICAgICAgIG9iamVjdC5wcm9kdWNlTGV2ZWwgPSBtZXNzYWdlLnByb2R1Y2VMZXZlbDtcbiAgICAgICAgaWYgKG1lc3NhZ2UucG9zaXRpb24gIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwicG9zaXRpb25cIikpXG4gICAgICAgICAgICBvYmplY3QucG9zaXRpb24gPSBtZXNzYWdlLnBvc2l0aW9uO1xuICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGlzIEJ1eVByb2R1Y2VSZXBseSB0byBKU09OLlxuICAgICAqIEBmdW5jdGlvbiB0b0pTT05cbiAgICAgKiBAbWVtYmVyb2YgQnV5UHJvZHVjZVJlcGx5XG4gICAgICogQGluc3RhbmNlXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBKU09OIG9iamVjdFxuICAgICAqL1xuICAgIEJ1eVByb2R1Y2VSZXBseS5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci50b09iamVjdCh0aGlzLCAkcHJvdG9idWYudXRpbC50b0pTT05PcHRpb25zKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIEJ1eVByb2R1Y2VSZXBseTtcbn0pKCk7XG5cbiRyb290LkNvbWJpbmVQcm9kdWNlUmVxdWVzdCA9IChmdW5jdGlvbigpIHtcblxuICAgIC8qKlxuICAgICAqIFByb3BlcnRpZXMgb2YgYSBDb21iaW5lUHJvZHVjZVJlcXVlc3QuXG4gICAgICogQGV4cG9ydHMgSUNvbWJpbmVQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBpbnRlcmZhY2UgSUNvbWJpbmVQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBwcm9wZXJ0eSB7bnVtYmVyfG51bGx9IFtwb3NpdGlvbkJhc2VdIENvbWJpbmVQcm9kdWNlUmVxdWVzdCBwb3NpdGlvbkJhc2VcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbcG9zaXRpb25UYXJnZXRdIENvbWJpbmVQcm9kdWNlUmVxdWVzdCBwb3NpdGlvblRhcmdldFxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ29uc3RydWN0cyBhIG5ldyBDb21iaW5lUHJvZHVjZVJlcXVlc3QuXG4gICAgICogQGV4cG9ydHMgQ29tYmluZVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LlxuICAgICAqIEBpbXBsZW1lbnRzIElDb21iaW5lUHJvZHVjZVJlcXVlc3RcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0ge0lDb21iaW5lUHJvZHVjZVJlcXVlc3Q9fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBDb21iaW5lUHJvZHVjZVJlcXVlc3QocHJvcGVydGllcykge1xuICAgICAgICBpZiAocHJvcGVydGllcylcbiAgICAgICAgICAgIGZvciAodmFyIGtleXMgPSBPYmplY3Qua2V5cyhwcm9wZXJ0aWVzKSwgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgIGlmIChwcm9wZXJ0aWVzW2tleXNbaV1dICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXNba2V5c1tpXV0gPSBwcm9wZXJ0aWVzW2tleXNbaV1dO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENvbWJpbmVQcm9kdWNlUmVxdWVzdCBwb3NpdGlvbkJhc2UuXG4gICAgICogQG1lbWJlciB7bnVtYmVyfSBwb3NpdGlvbkJhc2VcbiAgICAgKiBAbWVtYmVyb2YgQ29tYmluZVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LnByb3RvdHlwZS5wb3NpdGlvbkJhc2UgPSAwO1xuXG4gICAgLyoqXG4gICAgICogQ29tYmluZVByb2R1Y2VSZXF1ZXN0IHBvc2l0aW9uVGFyZ2V0LlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gcG9zaXRpb25UYXJnZXRcbiAgICAgKiBAbWVtYmVyb2YgQ29tYmluZVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LnByb3RvdHlwZS5wb3NpdGlvblRhcmdldCA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IENvbWJpbmVQcm9kdWNlUmVxdWVzdCBpbnN0YW5jZSB1c2luZyB0aGUgc3BlY2lmaWVkIHByb3BlcnRpZXMuXG4gICAgICogQGZ1bmN0aW9uIGNyZWF0ZVxuICAgICAqIEBtZW1iZXJvZiBDb21iaW5lUHJvZHVjZVJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJQ29tYmluZVByb2R1Y2VSZXF1ZXN0PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICogQHJldHVybnMge0NvbWJpbmVQcm9kdWNlUmVxdWVzdH0gQ29tYmluZVByb2R1Y2VSZXF1ZXN0IGluc3RhbmNlXG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShwcm9wZXJ0aWVzKSB7XG4gICAgICAgIHJldHVybiBuZXcgQ29tYmluZVByb2R1Y2VSZXF1ZXN0KHByb3BlcnRpZXMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgQ29tYmluZVByb2R1Y2VSZXF1ZXN0IG1lc3NhZ2UuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIENvbWJpbmVQcm9kdWNlUmVxdWVzdC52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlXG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lDb21iaW5lUHJvZHVjZVJlcXVlc3R9IG1lc3NhZ2UgQ29tYmluZVByb2R1Y2VSZXF1ZXN0IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcXVlc3QuZW5jb2RlID0gZnVuY3Rpb24gZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICBpZiAoIXdyaXRlcilcbiAgICAgICAgICAgIHdyaXRlciA9ICRXcml0ZXIuY3JlYXRlKCk7XG4gICAgICAgIGlmIChtZXNzYWdlLnBvc2l0aW9uQmFzZSAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwicG9zaXRpb25CYXNlXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAxLCB3aXJlVHlwZSAwID0qLzgpLmludDMyKG1lc3NhZ2UucG9zaXRpb25CYXNlKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UucG9zaXRpb25UYXJnZXQgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcInBvc2l0aW9uVGFyZ2V0XCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAyLCB3aXJlVHlwZSAwID0qLzE2KS5pbnQzMihtZXNzYWdlLnBvc2l0aW9uVGFyZ2V0KTtcbiAgICAgICAgcmV0dXJuIHdyaXRlcjtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIENvbWJpbmVQcm9kdWNlUmVxdWVzdCBtZXNzYWdlLCBsZW5ndGggZGVsaW1pdGVkLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBDb21iaW5lUHJvZHVjZVJlcXVlc3QudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBDb21iaW5lUHJvZHVjZVJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJQ29tYmluZVByb2R1Y2VSZXF1ZXN0fSBtZXNzYWdlIENvbWJpbmVQcm9kdWNlUmVxdWVzdCBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LmVuY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZChtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikubGRlbGltKCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBDb21iaW5lUHJvZHVjZVJlcXVlc3QgbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlci5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlXG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtDb21iaW5lUHJvZHVjZVJlcXVlc3R9IENvbWJpbmVQcm9kdWNlUmVxdWVzdFxuICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICogQHRocm93cyB7JHByb3RvYnVmLnV0aWwuUHJvdG9jb2xFcnJvcn0gSWYgcmVxdWlyZWQgZmllbGRzIGFyZSBtaXNzaW5nXG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LkNvbWJpbmVQcm9kdWNlUmVxdWVzdCgpO1xuICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZCkge1xuICAgICAgICAgICAgdmFyIHRhZyA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5wb3NpdGlvbkJhc2UgPSByZWFkZXIuaW50MzIoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgMjpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLnBvc2l0aW9uVGFyZ2V0ID0gcmVhZGVyLmludDMyKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHJlYWRlci5za2lwVHlwZSh0YWcgJiA3KTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhIENvbWJpbmVQcm9kdWNlUmVxdWVzdCBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLCBsZW5ndGggZGVsaW1pdGVkLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVEZWxpbWl0ZWRcbiAgICAgKiBAbWVtYmVyb2YgQ29tYmluZVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtDb21iaW5lUHJvZHVjZVJlcXVlc3R9IENvbWJpbmVQcm9kdWNlUmVxdWVzdFxuICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICogQHRocm93cyB7JHByb3RvYnVmLnV0aWwuUHJvdG9jb2xFcnJvcn0gSWYgcmVxdWlyZWQgZmllbGRzIGFyZSBtaXNzaW5nXG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhIENvbWJpbmVQcm9kdWNlUmVxdWVzdCBtZXNzYWdlLlxuICAgICAqIEBmdW5jdGlvbiB2ZXJpZnlcbiAgICAgKiBAbWVtYmVyb2YgQ29tYmluZVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7T2JqZWN0LjxzdHJpbmcsKj59IG1lc3NhZ2UgUGxhaW4gb2JqZWN0IHRvIHZlcmlmeVxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd8bnVsbH0gYG51bGxgIGlmIHZhbGlkLCBvdGhlcndpc2UgdGhlIHJlYXNvbiB3aHkgaXQgaXMgbm90XG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LnZlcmlmeSA9IGZ1bmN0aW9uIHZlcmlmeShtZXNzYWdlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZSAhPT0gXCJvYmplY3RcIiB8fCBtZXNzYWdlID09PSBudWxsKVxuICAgICAgICAgICAgcmV0dXJuIFwib2JqZWN0IGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLnBvc2l0aW9uQmFzZSAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJwb3NpdGlvbkJhc2VcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLnBvc2l0aW9uQmFzZSkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwicG9zaXRpb25CYXNlOiBpbnRlZ2VyIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLnBvc2l0aW9uVGFyZ2V0ICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInBvc2l0aW9uVGFyZ2V0XCIpKVxuICAgICAgICAgICAgaWYgKCEkdXRpbC5pc0ludGVnZXIobWVzc2FnZS5wb3NpdGlvblRhcmdldCkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwicG9zaXRpb25UYXJnZXQ6IGludGVnZXIgZXhwZWN0ZWRcIjtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBDb21iaW5lUHJvZHVjZVJlcXVlc3QgbWVzc2FnZSBmcm9tIGEgcGxhaW4gb2JqZWN0LiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byB0aGVpciByZXNwZWN0aXZlIGludGVybmFsIHR5cGVzLlxuICAgICAqIEBmdW5jdGlvbiBmcm9tT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBvYmplY3QgUGxhaW4gb2JqZWN0XG4gICAgICogQHJldHVybnMge0NvbWJpbmVQcm9kdWNlUmVxdWVzdH0gQ29tYmluZVByb2R1Y2VSZXF1ZXN0XG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXF1ZXN0LmZyb21PYmplY3QgPSBmdW5jdGlvbiBmcm9tT2JqZWN0KG9iamVjdCkge1xuICAgICAgICBpZiAob2JqZWN0IGluc3RhbmNlb2YgJHJvb3QuQ29tYmluZVByb2R1Y2VSZXF1ZXN0KVxuICAgICAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICAgICAgdmFyIG1lc3NhZ2UgPSBuZXcgJHJvb3QuQ29tYmluZVByb2R1Y2VSZXF1ZXN0KCk7XG4gICAgICAgIGlmIChvYmplY3QucG9zaXRpb25CYXNlICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLnBvc2l0aW9uQmFzZSA9IG9iamVjdC5wb3NpdGlvbkJhc2UgfCAwO1xuICAgICAgICBpZiAob2JqZWN0LnBvc2l0aW9uVGFyZ2V0ICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLnBvc2l0aW9uVGFyZ2V0ID0gb2JqZWN0LnBvc2l0aW9uVGFyZ2V0IHwgMDtcbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBwbGFpbiBvYmplY3QgZnJvbSBhIENvbWJpbmVQcm9kdWNlUmVxdWVzdCBtZXNzYWdlLiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byBvdGhlciB0eXBlcyBpZiBzcGVjaWZpZWQuXG4gICAgICogQGZ1bmN0aW9uIHRvT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0NvbWJpbmVQcm9kdWNlUmVxdWVzdH0gbWVzc2FnZSBDb21iaW5lUHJvZHVjZVJlcXVlc3RcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5JQ29udmVyc2lvbk9wdGlvbnN9IFtvcHRpb25zXSBDb252ZXJzaW9uIG9wdGlvbnNcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IFBsYWluIG9iamVjdFxuICAgICAqL1xuICAgIENvbWJpbmVQcm9kdWNlUmVxdWVzdC50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG1lc3NhZ2UsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgICB2YXIgb2JqZWN0ID0ge307XG4gICAgICAgIGlmIChvcHRpb25zLmRlZmF1bHRzKSB7XG4gICAgICAgICAgICBvYmplY3QucG9zaXRpb25CYXNlID0gMDtcbiAgICAgICAgICAgIG9iamVjdC5wb3NpdGlvblRhcmdldCA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1lc3NhZ2UucG9zaXRpb25CYXNlICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInBvc2l0aW9uQmFzZVwiKSlcbiAgICAgICAgICAgIG9iamVjdC5wb3NpdGlvbkJhc2UgPSBtZXNzYWdlLnBvc2l0aW9uQmFzZTtcbiAgICAgICAgaWYgKG1lc3NhZ2UucG9zaXRpb25UYXJnZXQgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwicG9zaXRpb25UYXJnZXRcIikpXG4gICAgICAgICAgICBvYmplY3QucG9zaXRpb25UYXJnZXQgPSBtZXNzYWdlLnBvc2l0aW9uVGFyZ2V0O1xuICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGlzIENvbWJpbmVQcm9kdWNlUmVxdWVzdCB0byBKU09OLlxuICAgICAqIEBmdW5jdGlvbiB0b0pTT05cbiAgICAgKiBAbWVtYmVyb2YgQ29tYmluZVByb2R1Y2VSZXF1ZXN0XG4gICAgICogQGluc3RhbmNlXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBKU09OIG9iamVjdFxuICAgICAqL1xuICAgIENvbWJpbmVQcm9kdWNlUmVxdWVzdC5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci50b09iamVjdCh0aGlzLCAkcHJvdG9idWYudXRpbC50b0pTT05PcHRpb25zKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIENvbWJpbmVQcm9kdWNlUmVxdWVzdDtcbn0pKCk7XG5cbiRyb290LkNvbWJpbmVQcm9kdWNlUmVwbHkgPSAoZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBQcm9wZXJ0aWVzIG9mIGEgQ29tYmluZVByb2R1Y2VSZXBseS5cbiAgICAgKiBAZXhwb3J0cyBJQ29tYmluZVByb2R1Y2VSZXBseVxuICAgICAqIEBpbnRlcmZhY2UgSUNvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbZXJyY29kZV0gQ29tYmluZVByb2R1Y2VSZXBseSBlcnJjb2RlXG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8bnVsbH0gW3Byb2R1Y2VMZXZlbF0gQ29tYmluZVByb2R1Y2VSZXBseSBwcm9kdWNlTGV2ZWxcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbcG9zaXRpb25OZXddIENvbWJpbmVQcm9kdWNlUmVwbHkgcG9zaXRpb25OZXdcbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIENvbnN0cnVjdHMgYSBuZXcgQ29tYmluZVByb2R1Y2VSZXBseS5cbiAgICAgKiBAZXhwb3J0cyBDb21iaW5lUHJvZHVjZVJlcGx5XG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgQ29tYmluZVByb2R1Y2VSZXBseS5cbiAgICAgKiBAaW1wbGVtZW50cyBJQ29tYmluZVByb2R1Y2VSZXBseVxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7SUNvbWJpbmVQcm9kdWNlUmVwbHk9fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBDb21iaW5lUHJvZHVjZVJlcGx5KHByb3BlcnRpZXMpIHtcbiAgICAgICAgaWYgKHByb3BlcnRpZXMpXG4gICAgICAgICAgICBmb3IgKHZhciBrZXlzID0gT2JqZWN0LmtleXMocHJvcGVydGllcyksIGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7ICsraSlcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydGllc1trZXlzW2ldXSAhPSBudWxsKVxuICAgICAgICAgICAgICAgICAgICB0aGlzW2tleXNbaV1dID0gcHJvcGVydGllc1trZXlzW2ldXTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDb21iaW5lUHJvZHVjZVJlcGx5IGVycmNvZGUuXG4gICAgICogQG1lbWJlciB7bnVtYmVyfSBlcnJjb2RlXG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LnByb3RvdHlwZS5lcnJjb2RlID0gMDtcblxuICAgIC8qKlxuICAgICAqIENvbWJpbmVQcm9kdWNlUmVwbHkgcHJvZHVjZUxldmVsLlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gcHJvZHVjZUxldmVsXG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LnByb3RvdHlwZS5wcm9kdWNlTGV2ZWwgPSAwO1xuXG4gICAgLyoqXG4gICAgICogQ29tYmluZVByb2R1Y2VSZXBseSBwb3NpdGlvbk5ldy5cbiAgICAgKiBAbWVtYmVyIHtudW1iZXJ9IHBvc2l0aW9uTmV3XG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LnByb3RvdHlwZS5wb3NpdGlvbk5ldyA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IENvbWJpbmVQcm9kdWNlUmVwbHkgaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAqIEBmdW5jdGlvbiBjcmVhdGVcbiAgICAgKiBAbWVtYmVyb2YgQ29tYmluZVByb2R1Y2VSZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lDb21iaW5lUHJvZHVjZVJlcGx5PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICogQHJldHVybnMge0NvbWJpbmVQcm9kdWNlUmVwbHl9IENvbWJpbmVQcm9kdWNlUmVwbHkgaW5zdGFuY2VcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShwcm9wZXJ0aWVzKSB7XG4gICAgICAgIHJldHVybiBuZXcgQ29tYmluZVByb2R1Y2VSZXBseShwcm9wZXJ0aWVzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIENvbWJpbmVQcm9kdWNlUmVwbHkgbWVzc2FnZS4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgQ29tYmluZVByb2R1Y2VSZXBseS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlXG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJQ29tYmluZVByb2R1Y2VSZXBseX0gbWVzc2FnZSBDb21iaW5lUHJvZHVjZVJlcGx5IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LmVuY29kZSA9IGZ1bmN0aW9uIGVuY29kZShtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgaWYgKCF3cml0ZXIpXG4gICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICBpZiAobWVzc2FnZS5lcnJjb2RlICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJlcnJjb2RlXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAxLCB3aXJlVHlwZSAwID0qLzgpLmludDMyKG1lc3NhZ2UuZXJyY29kZSk7XG4gICAgICAgIGlmIChtZXNzYWdlLnByb2R1Y2VMZXZlbCAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwicHJvZHVjZUxldmVsXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAyLCB3aXJlVHlwZSAwID0qLzE2KS5pbnQzMihtZXNzYWdlLnByb2R1Y2VMZXZlbCk7XG4gICAgICAgIGlmIChtZXNzYWdlLnBvc2l0aW9uTmV3ICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJwb3NpdGlvbk5ld1wiKSlcbiAgICAgICAgICAgIHdyaXRlci51aW50MzIoLyogaWQgMywgd2lyZVR5cGUgMCA9Ki8yNCkuaW50MzIobWVzc2FnZS5wb3NpdGlvbk5ldyk7XG4gICAgICAgIHJldHVybiB3cml0ZXI7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEVuY29kZXMgdGhlIHNwZWNpZmllZCBDb21iaW5lUHJvZHVjZVJlcGx5IG1lc3NhZ2UsIGxlbmd0aCBkZWxpbWl0ZWQuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIENvbWJpbmVQcm9kdWNlUmVwbHkudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBDb21iaW5lUHJvZHVjZVJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SUNvbWJpbmVQcm9kdWNlUmVwbHl9IG1lc3NhZ2UgQ29tYmluZVByb2R1Y2VSZXBseSBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXBseS5lbmNvZGVEZWxpbWl0ZWQgPSBmdW5jdGlvbiBlbmNvZGVEZWxpbWl0ZWQobWVzc2FnZSwgd3JpdGVyKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmVuY29kZShtZXNzYWdlLCB3cml0ZXIpLmxkZWxpbSgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgQ29tYmluZVByb2R1Y2VSZXBseSBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVcbiAgICAgKiBAbWVtYmVyb2YgQ29tYmluZVByb2R1Y2VSZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtDb21iaW5lUHJvZHVjZVJlcGx5fSBDb21iaW5lUHJvZHVjZVJlcGx5XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LkNvbWJpbmVQcm9kdWNlUmVwbHkoKTtcbiAgICAgICAgd2hpbGUgKHJlYWRlci5wb3MgPCBlbmQpIHtcbiAgICAgICAgICAgIHZhciB0YWcgPSByZWFkZXIudWludDMyKCk7XG4gICAgICAgICAgICBzd2l0Y2ggKHRhZyA+Pj4gMykge1xuICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UuZXJyY29kZSA9IHJlYWRlci5pbnQzMigpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UucHJvZHVjZUxldmVsID0gcmVhZGVyLmludDMyKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5wb3NpdGlvbk5ldyA9IHJlYWRlci5pbnQzMigpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZWFkZXIuc2tpcFR5cGUodGFnICYgNyk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBDb21iaW5lUHJvZHVjZVJlcGx5IG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIsIGxlbmd0aCBkZWxpbWl0ZWQuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBDb21iaW5lUHJvZHVjZVJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtDb21iaW5lUHJvZHVjZVJlcGx5fSBDb21iaW5lUHJvZHVjZVJlcGx5XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhIENvbWJpbmVQcm9kdWNlUmVwbHkgbWVzc2FnZS5cbiAgICAgKiBAZnVuY3Rpb24gdmVyaWZ5XG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LnZlcmlmeSA9IGZ1bmN0aW9uIHZlcmlmeShtZXNzYWdlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZSAhPT0gXCJvYmplY3RcIiB8fCBtZXNzYWdlID09PSBudWxsKVxuICAgICAgICAgICAgcmV0dXJuIFwib2JqZWN0IGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLmVycmNvZGUgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiZXJyY29kZVwiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UuZXJyY29kZSkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiZXJyY29kZTogaW50ZWdlciBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5wcm9kdWNlTGV2ZWwgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwicHJvZHVjZUxldmVsXCIpKVxuICAgICAgICAgICAgaWYgKCEkdXRpbC5pc0ludGVnZXIobWVzc2FnZS5wcm9kdWNlTGV2ZWwpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcInByb2R1Y2VMZXZlbDogaW50ZWdlciBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5wb3NpdGlvbk5ldyAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJwb3NpdGlvbk5ld1wiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UucG9zaXRpb25OZXcpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcInBvc2l0aW9uTmV3OiBpbnRlZ2VyIGV4cGVjdGVkXCI7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgQ29tYmluZVByb2R1Y2VSZXBseSBtZXNzYWdlIGZyb20gYSBwbGFpbiBvYmplY3QuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIHRoZWlyIHJlc3BlY3RpdmUgaW50ZXJuYWwgdHlwZXMuXG4gICAgICogQGZ1bmN0aW9uIGZyb21PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgQ29tYmluZVByb2R1Y2VSZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBvYmplY3QgUGxhaW4gb2JqZWN0XG4gICAgICogQHJldHVybnMge0NvbWJpbmVQcm9kdWNlUmVwbHl9IENvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LmZyb21PYmplY3QgPSBmdW5jdGlvbiBmcm9tT2JqZWN0KG9iamVjdCkge1xuICAgICAgICBpZiAob2JqZWN0IGluc3RhbmNlb2YgJHJvb3QuQ29tYmluZVByb2R1Y2VSZXBseSlcbiAgICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgICAgIHZhciBtZXNzYWdlID0gbmV3ICRyb290LkNvbWJpbmVQcm9kdWNlUmVwbHkoKTtcbiAgICAgICAgaWYgKG9iamVjdC5lcnJjb2RlICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLmVycmNvZGUgPSBvYmplY3QuZXJyY29kZSB8IDA7XG4gICAgICAgIGlmIChvYmplY3QucHJvZHVjZUxldmVsICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLnByb2R1Y2VMZXZlbCA9IG9iamVjdC5wcm9kdWNlTGV2ZWwgfCAwO1xuICAgICAgICBpZiAob2JqZWN0LnBvc2l0aW9uTmV3ICE9IG51bGwpXG4gICAgICAgICAgICBtZXNzYWdlLnBvc2l0aW9uTmV3ID0gb2JqZWN0LnBvc2l0aW9uTmV3IHwgMDtcbiAgICAgICAgcmV0dXJuIG1lc3NhZ2U7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBwbGFpbiBvYmplY3QgZnJvbSBhIENvbWJpbmVQcm9kdWNlUmVwbHkgbWVzc2FnZS4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gb3RoZXIgdHlwZXMgaWYgc3BlY2lmaWVkLlxuICAgICAqIEBmdW5jdGlvbiB0b09iamVjdFxuICAgICAqIEBtZW1iZXJvZiBDb21iaW5lUHJvZHVjZVJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7Q29tYmluZVByb2R1Y2VSZXBseX0gbWVzc2FnZSBDb21iaW5lUHJvZHVjZVJlcGx5XG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuSUNvbnZlcnNpb25PcHRpb25zfSBbb3B0aW9uc10gQ29udmVyc2lvbiBvcHRpb25zXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBQbGFpbiBvYmplY3RcbiAgICAgKi9cbiAgICBDb21iaW5lUHJvZHVjZVJlcGx5LnRvT2JqZWN0ID0gZnVuY3Rpb24gdG9PYmplY3QobWVzc2FnZSwgb3B0aW9ucykge1xuICAgICAgICBpZiAoIW9wdGlvbnMpXG4gICAgICAgICAgICBvcHRpb25zID0ge307XG4gICAgICAgIHZhciBvYmplY3QgPSB7fTtcbiAgICAgICAgaWYgKG9wdGlvbnMuZGVmYXVsdHMpIHtcbiAgICAgICAgICAgIG9iamVjdC5lcnJjb2RlID0gMDtcbiAgICAgICAgICAgIG9iamVjdC5wcm9kdWNlTGV2ZWwgPSAwO1xuICAgICAgICAgICAgb2JqZWN0LnBvc2l0aW9uTmV3ID0gMDtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWVzc2FnZS5lcnJjb2RlICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImVycmNvZGVcIikpXG4gICAgICAgICAgICBvYmplY3QuZXJyY29kZSA9IG1lc3NhZ2UuZXJyY29kZTtcbiAgICAgICAgaWYgKG1lc3NhZ2UucHJvZHVjZUxldmVsICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcInByb2R1Y2VMZXZlbFwiKSlcbiAgICAgICAgICAgIG9iamVjdC5wcm9kdWNlTGV2ZWwgPSBtZXNzYWdlLnByb2R1Y2VMZXZlbDtcbiAgICAgICAgaWYgKG1lc3NhZ2UucG9zaXRpb25OZXcgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwicG9zaXRpb25OZXdcIikpXG4gICAgICAgICAgICBvYmplY3QucG9zaXRpb25OZXcgPSBtZXNzYWdlLnBvc2l0aW9uTmV3O1xuICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGlzIENvbWJpbmVQcm9kdWNlUmVwbHkgdG8gSlNPTi5cbiAgICAgKiBAZnVuY3Rpb24gdG9KU09OXG4gICAgICogQG1lbWJlcm9mIENvbWJpbmVQcm9kdWNlUmVwbHlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IEpTT04gb2JqZWN0XG4gICAgICovXG4gICAgQ29tYmluZVByb2R1Y2VSZXBseS5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci50b09iamVjdCh0aGlzLCAkcHJvdG9idWYudXRpbC50b0pTT05PcHRpb25zKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIENvbWJpbmVQcm9kdWNlUmVwbHk7XG59KSgpO1xuXG4kcm9vdC5SZWZyZXNoV29ya0JlbmNoUmVxdWVzdCA9IChmdW5jdGlvbigpIHtcblxuICAgIC8qKlxuICAgICAqIFByb3BlcnRpZXMgb2YgYSBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdC5cbiAgICAgKiBAZXhwb3J0cyBJUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3RcbiAgICAgKiBAaW50ZXJmYWNlIElSZWZyZXNoV29ya0JlbmNoUmVxdWVzdFxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ29uc3RydWN0cyBhIG5ldyBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdC5cbiAgICAgKiBAZXhwb3J0cyBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdFxuICAgICAqIEBjbGFzc2Rlc2MgUmVwcmVzZW50cyBhIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0LlxuICAgICAqIEBpbXBsZW1lbnRzIElSZWZyZXNoV29ya0JlbmNoUmVxdWVzdFxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7SVJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICovXG4gICAgZnVuY3Rpb24gUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QocHJvcGVydGllcykge1xuICAgICAgICBpZiAocHJvcGVydGllcylcbiAgICAgICAgICAgIGZvciAodmFyIGtleXMgPSBPYmplY3Qua2V5cyhwcm9wZXJ0aWVzKSwgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgIGlmIChwcm9wZXJ0aWVzW2tleXNbaV1dICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXNba2V5c1tpXV0gPSBwcm9wZXJ0aWVzW2tleXNbaV1dO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBuZXcgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QgaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAqIEBmdW5jdGlvbiBjcmVhdGVcbiAgICAgKiBAbWVtYmVyb2YgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3Q9fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKiBAcmV0dXJucyB7UmVmcmVzaFdvcmtCZW5jaFJlcXVlc3R9IFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0IGluc3RhbmNlXG4gICAgICovXG4gICAgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QuY3JlYXRlID0gZnVuY3Rpb24gY3JlYXRlKHByb3BlcnRpZXMpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdChwcm9wZXJ0aWVzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0IG1lc3NhZ2UuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0LnZlcmlmeXx2ZXJpZnl9IG1lc3NhZ2VzLlxuICAgICAqIEBmdW5jdGlvbiBlbmNvZGVcbiAgICAgKiBAbWVtYmVyb2YgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3RcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3R9IG1lc3NhZ2UgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QgbWVzc2FnZSBvciBwbGFpbiBvYmplY3QgdG8gZW5jb2RlXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuV3JpdGVyfSBbd3JpdGVyXSBXcml0ZXIgdG8gZW5jb2RlIHRvXG4gICAgICogQHJldHVybnMgeyRwcm90b2J1Zi5Xcml0ZXJ9IFdyaXRlclxuICAgICAqL1xuICAgIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0LmVuY29kZSA9IGZ1bmN0aW9uIGVuY29kZShtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgaWYgKCF3cml0ZXIpXG4gICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICByZXR1cm4gd3JpdGVyO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QgbWVzc2FnZSwgbGVuZ3RoIGRlbGltaXRlZC4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lSZWZyZXNoV29ya0JlbmNoUmVxdWVzdH0gbWVzc2FnZSBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdCBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QuZW5jb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5lbmNvZGUobWVzc2FnZSwgd3JpdGVyKS5sZGVsaW0oKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0IG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZVxuICAgICAqIEBtZW1iZXJvZiBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtSZWZyZXNoV29ya0JlbmNoUmVxdWVzdH0gUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3RcbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0LmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LlJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0KCk7XG4gICAgICAgIHdoaWxlIChyZWFkZXIucG9zIDwgZW5kKSB7XG4gICAgICAgICAgICB2YXIgdGFnID0gcmVhZGVyLnVpbnQzMigpO1xuICAgICAgICAgICAgc3dpdGNoICh0YWcgPj4+IDMpIHtcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QgbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlciwgbGVuZ3RoIGRlbGltaXRlZC5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtSZWZyZXNoV29ya0JlbmNoUmVxdWVzdH0gUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3RcbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0LmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0IG1lc3NhZ2UuXG4gICAgICogQGZ1bmN0aW9uIHZlcmlmeVxuICAgICAqIEBtZW1iZXJvZiBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBtZXNzYWdlIFBsYWluIG9iamVjdCB0byB2ZXJpZnlcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfG51bGx9IGBudWxsYCBpZiB2YWxpZCwgb3RoZXJ3aXNlIHRoZSByZWFzb24gd2h5IGl0IGlzIG5vdFxuICAgICAqL1xuICAgIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0LnZlcmlmeSA9IGZ1bmN0aW9uIHZlcmlmeShtZXNzYWdlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZSAhPT0gXCJvYmplY3RcIiB8fCBtZXNzYWdlID09PSBudWxsKVxuICAgICAgICAgICAgcmV0dXJuIFwib2JqZWN0IGV4cGVjdGVkXCI7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QgbWVzc2FnZSBmcm9tIGEgcGxhaW4gb2JqZWN0LiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byB0aGVpciByZXNwZWN0aXZlIGludGVybmFsIHR5cGVzLlxuICAgICAqIEBmdW5jdGlvbiBmcm9tT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7T2JqZWN0LjxzdHJpbmcsKj59IG9iamVjdCBQbGFpbiBvYmplY3RcbiAgICAgKiBAcmV0dXJucyB7UmVmcmVzaFdvcmtCZW5jaFJlcXVlc3R9IFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0XG4gICAgICovXG4gICAgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QuZnJvbU9iamVjdCA9IGZ1bmN0aW9uIGZyb21PYmplY3Qob2JqZWN0KSB7XG4gICAgICAgIGlmIChvYmplY3QgaW5zdGFuY2VvZiAkcm9vdC5SZWZyZXNoV29ya0JlbmNoUmVxdWVzdClcbiAgICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgICAgIHJldHVybiBuZXcgJHJvb3QuUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QoKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIHBsYWluIG9iamVjdCBmcm9tIGEgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QgbWVzc2FnZS4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gb3RoZXIgdHlwZXMgaWYgc3BlY2lmaWVkLlxuICAgICAqIEBmdW5jdGlvbiB0b09iamVjdFxuICAgICAqIEBtZW1iZXJvZiBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdFxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge1JlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0fSBtZXNzYWdlIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0XG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuSUNvbnZlcnNpb25PcHRpb25zfSBbb3B0aW9uc10gQ29udmVyc2lvbiBvcHRpb25zXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBQbGFpbiBvYmplY3RcbiAgICAgKi9cbiAgICBSZWZyZXNoV29ya0JlbmNoUmVxdWVzdC50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KCkge1xuICAgICAgICByZXR1cm4ge307XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENvbnZlcnRzIHRoaXMgUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3QgdG8gSlNPTi5cbiAgICAgKiBAZnVuY3Rpb24gdG9KU09OXG4gICAgICogQG1lbWJlcm9mIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0XG4gICAgICogQGluc3RhbmNlXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBKU09OIG9iamVjdFxuICAgICAqL1xuICAgIFJlZnJlc2hXb3JrQmVuY2hSZXF1ZXN0LnByb3RvdHlwZS50b0pTT04gPSBmdW5jdGlvbiB0b0pTT04oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbnN0cnVjdG9yLnRvT2JqZWN0KHRoaXMsICRwcm90b2J1Zi51dGlsLnRvSlNPTk9wdGlvbnMpO1xuICAgIH07XG5cbiAgICByZXR1cm4gUmVmcmVzaFdvcmtCZW5jaFJlcXVlc3Q7XG59KSgpO1xuXG4kcm9vdC5SZWZyZXNoV29ya0JlbmNoUmVwbHkgPSAoZnVuY3Rpb24oKSB7XG5cbiAgICAvKipcbiAgICAgKiBQcm9wZXJ0aWVzIG9mIGEgUmVmcmVzaFdvcmtCZW5jaFJlcGx5LlxuICAgICAqIEBleHBvcnRzIElSZWZyZXNoV29ya0JlbmNoUmVwbHlcbiAgICAgKiBAaW50ZXJmYWNlIElSZWZyZXNoV29ya0JlbmNoUmVwbHlcbiAgICAgKiBAcHJvcGVydHkge0FycmF5LjxudW1iZXI+fG51bGx9IFt3b3JrYmVuY2hJbmZvXSBSZWZyZXNoV29ya0JlbmNoUmVwbHkgd29ya2JlbmNoSW5mb1xuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ29uc3RydWN0cyBhIG5ldyBSZWZyZXNoV29ya0JlbmNoUmVwbHkuXG4gICAgICogQGV4cG9ydHMgUmVmcmVzaFdvcmtCZW5jaFJlcGx5XG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgUmVmcmVzaFdvcmtCZW5jaFJlcGx5LlxuICAgICAqIEBpbXBsZW1lbnRzIElSZWZyZXNoV29ya0JlbmNoUmVwbHlcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0ge0lSZWZyZXNoV29ya0JlbmNoUmVwbHk9fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBSZWZyZXNoV29ya0JlbmNoUmVwbHkocHJvcGVydGllcykge1xuICAgICAgICB0aGlzLndvcmtiZW5jaEluZm8gPSBbXTtcbiAgICAgICAgaWYgKHByb3BlcnRpZXMpXG4gICAgICAgICAgICBmb3IgKHZhciBrZXlzID0gT2JqZWN0LmtleXMocHJvcGVydGllcyksIGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7ICsraSlcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydGllc1trZXlzW2ldXSAhPSBudWxsKVxuICAgICAgICAgICAgICAgICAgICB0aGlzW2tleXNbaV1dID0gcHJvcGVydGllc1trZXlzW2ldXTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZWZyZXNoV29ya0JlbmNoUmVwbHkgd29ya2JlbmNoSW5mby5cbiAgICAgKiBAbWVtYmVyIHtBcnJheS48bnVtYmVyPn0gd29ya2JlbmNoSW5mb1xuICAgICAqIEBtZW1iZXJvZiBSZWZyZXNoV29ya0JlbmNoUmVwbHlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBSZWZyZXNoV29ya0JlbmNoUmVwbHkucHJvdG90eXBlLndvcmtiZW5jaEluZm8gPSAkdXRpbC5lbXB0eUFycmF5O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBSZWZyZXNoV29ya0JlbmNoUmVwbHkgaW5zdGFuY2UgdXNpbmcgdGhlIHNwZWNpZmllZCBwcm9wZXJ0aWVzLlxuICAgICAqIEBmdW5jdGlvbiBjcmVhdGVcbiAgICAgKiBAbWVtYmVyb2YgUmVmcmVzaFdvcmtCZW5jaFJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7SVJlZnJlc2hXb3JrQmVuY2hSZXBseT19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqIEByZXR1cm5zIHtSZWZyZXNoV29ya0JlbmNoUmVwbHl9IFJlZnJlc2hXb3JrQmVuY2hSZXBseSBpbnN0YW5jZVxuICAgICAqL1xuICAgIFJlZnJlc2hXb3JrQmVuY2hSZXBseS5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUocHJvcGVydGllcykge1xuICAgICAgICByZXR1cm4gbmV3IFJlZnJlc2hXb3JrQmVuY2hSZXBseShwcm9wZXJ0aWVzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIFJlZnJlc2hXb3JrQmVuY2hSZXBseSBtZXNzYWdlLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBSZWZyZXNoV29ya0JlbmNoUmVwbHkudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZVxuICAgICAqIEBtZW1iZXJvZiBSZWZyZXNoV29ya0JlbmNoUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJUmVmcmVzaFdvcmtCZW5jaFJlcGx5fSBtZXNzYWdlIFJlZnJlc2hXb3JrQmVuY2hSZXBseSBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgUmVmcmVzaFdvcmtCZW5jaFJlcGx5LmVuY29kZSA9IGZ1bmN0aW9uIGVuY29kZShtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgaWYgKCF3cml0ZXIpXG4gICAgICAgICAgICB3cml0ZXIgPSAkV3JpdGVyLmNyZWF0ZSgpO1xuICAgICAgICBpZiAobWVzc2FnZS53b3JrYmVuY2hJbmZvICE9IG51bGwgJiYgbWVzc2FnZS53b3JrYmVuY2hJbmZvLmxlbmd0aCkge1xuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAxLCB3aXJlVHlwZSAyID0qLzEwKS5mb3JrKCk7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG1lc3NhZ2Uud29ya2JlbmNoSW5mby5sZW5ndGg7ICsraSlcbiAgICAgICAgICAgICAgICB3cml0ZXIuaW50MzIobWVzc2FnZS53b3JrYmVuY2hJbmZvW2ldKTtcbiAgICAgICAgICAgIHdyaXRlci5sZGVsaW0oKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gd3JpdGVyO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgUmVmcmVzaFdvcmtCZW5jaFJlcGx5IG1lc3NhZ2UsIGxlbmd0aCBkZWxpbWl0ZWQuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIFJlZnJlc2hXb3JrQmVuY2hSZXBseS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIFJlZnJlc2hXb3JrQmVuY2hSZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lSZWZyZXNoV29ya0JlbmNoUmVwbHl9IG1lc3NhZ2UgUmVmcmVzaFdvcmtCZW5jaFJlcGx5IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBSZWZyZXNoV29ya0JlbmNoUmVwbHkuZW5jb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5lbmNvZGUobWVzc2FnZSwgd3JpdGVyKS5sZGVsaW0oKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhIFJlZnJlc2hXb3JrQmVuY2hSZXBseSBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVcbiAgICAgKiBAbWVtYmVyb2YgUmVmcmVzaFdvcmtCZW5jaFJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbbGVuZ3RoXSBNZXNzYWdlIGxlbmd0aCBpZiBrbm93biBiZWZvcmVoYW5kXG4gICAgICogQHJldHVybnMge1JlZnJlc2hXb3JrQmVuY2hSZXBseX0gUmVmcmVzaFdvcmtCZW5jaFJlcGx5XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBSZWZyZXNoV29ya0JlbmNoUmVwbHkuZGVjb2RlID0gZnVuY3Rpb24gZGVjb2RlKHJlYWRlciwgbGVuZ3RoKSB7XG4gICAgICAgIGlmICghKHJlYWRlciBpbnN0YW5jZW9mICRSZWFkZXIpKVxuICAgICAgICAgICAgcmVhZGVyID0gJFJlYWRlci5jcmVhdGUocmVhZGVyKTtcbiAgICAgICAgdmFyIGVuZCA9IGxlbmd0aCA9PT0gdW5kZWZpbmVkID8gcmVhZGVyLmxlbiA6IHJlYWRlci5wb3MgKyBsZW5ndGgsIG1lc3NhZ2UgPSBuZXcgJHJvb3QuUmVmcmVzaFdvcmtCZW5jaFJlcGx5KCk7XG4gICAgICAgIHdoaWxlIChyZWFkZXIucG9zIDwgZW5kKSB7XG4gICAgICAgICAgICB2YXIgdGFnID0gcmVhZGVyLnVpbnQzMigpO1xuICAgICAgICAgICAgc3dpdGNoICh0YWcgPj4+IDMpIHtcbiAgICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgICAgICBpZiAoIShtZXNzYWdlLndvcmtiZW5jaEluZm8gJiYgbWVzc2FnZS53b3JrYmVuY2hJbmZvLmxlbmd0aCkpXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2Uud29ya2JlbmNoSW5mbyA9IFtdO1xuICAgICAgICAgICAgICAgIGlmICgodGFnICYgNykgPT09IDIpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVuZDIgPSByZWFkZXIudWludDMyKCkgKyByZWFkZXIucG9zO1xuICAgICAgICAgICAgICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZDIpXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlLndvcmtiZW5jaEluZm8ucHVzaChyZWFkZXIuaW50MzIoKSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2Uud29ya2JlbmNoSW5mby5wdXNoKHJlYWRlci5pbnQzMigpKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgUmVmcmVzaFdvcmtCZW5jaFJlcGx5IG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIsIGxlbmd0aCBkZWxpbWl0ZWQuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBSZWZyZXNoV29ya0JlbmNoUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUmVhZGVyfFVpbnQ4QXJyYXl9IHJlYWRlciBSZWFkZXIgb3IgYnVmZmVyIHRvIGRlY29kZSBmcm9tXG4gICAgICogQHJldHVybnMge1JlZnJlc2hXb3JrQmVuY2hSZXBseX0gUmVmcmVzaFdvcmtCZW5jaFJlcGx5XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBSZWZyZXNoV29ya0JlbmNoUmVwbHkuZGVjb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkKHJlYWRlcikge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9IG5ldyAkUmVhZGVyKHJlYWRlcik7XG4gICAgICAgIHJldHVybiB0aGlzLmRlY29kZShyZWFkZXIsIHJlYWRlci51aW50MzIoKSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFZlcmlmaWVzIGEgUmVmcmVzaFdvcmtCZW5jaFJlcGx5IG1lc3NhZ2UuXG4gICAgICogQGZ1bmN0aW9uIHZlcmlmeVxuICAgICAqIEBtZW1iZXJvZiBSZWZyZXNoV29ya0JlbmNoUmVwbHlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBSZWZyZXNoV29ya0JlbmNoUmVwbHkudmVyaWZ5ID0gZnVuY3Rpb24gdmVyaWZ5KG1lc3NhZ2UpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlICE9PSBcIm9iamVjdFwiIHx8IG1lc3NhZ2UgPT09IG51bGwpXG4gICAgICAgICAgICByZXR1cm4gXCJvYmplY3QgZXhwZWN0ZWRcIjtcbiAgICAgICAgaWYgKG1lc3NhZ2Uud29ya2JlbmNoSW5mbyAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJ3b3JrYmVuY2hJbmZvXCIpKSB7XG4gICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkobWVzc2FnZS53b3JrYmVuY2hJbmZvKSlcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJ3b3JrYmVuY2hJbmZvOiBhcnJheSBleHBlY3RlZFwiO1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtZXNzYWdlLndvcmtiZW5jaEluZm8ubGVuZ3RoOyArK2kpXG4gICAgICAgICAgICAgICAgaWYgKCEkdXRpbC5pc0ludGVnZXIobWVzc2FnZS53b3JrYmVuY2hJbmZvW2ldKSlcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwid29ya2JlbmNoSW5mbzogaW50ZWdlcltdIGV4cGVjdGVkXCI7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBSZWZyZXNoV29ya0JlbmNoUmVwbHkgbWVzc2FnZSBmcm9tIGEgcGxhaW4gb2JqZWN0LiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byB0aGVpciByZXNwZWN0aXZlIGludGVybmFsIHR5cGVzLlxuICAgICAqIEBmdW5jdGlvbiBmcm9tT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIFJlZnJlc2hXb3JrQmVuY2hSZXBseVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBvYmplY3QgUGxhaW4gb2JqZWN0XG4gICAgICogQHJldHVybnMge1JlZnJlc2hXb3JrQmVuY2hSZXBseX0gUmVmcmVzaFdvcmtCZW5jaFJlcGx5XG4gICAgICovXG4gICAgUmVmcmVzaFdvcmtCZW5jaFJlcGx5LmZyb21PYmplY3QgPSBmdW5jdGlvbiBmcm9tT2JqZWN0KG9iamVjdCkge1xuICAgICAgICBpZiAob2JqZWN0IGluc3RhbmNlb2YgJHJvb3QuUmVmcmVzaFdvcmtCZW5jaFJlcGx5KVxuICAgICAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICAgICAgdmFyIG1lc3NhZ2UgPSBuZXcgJHJvb3QuUmVmcmVzaFdvcmtCZW5jaFJlcGx5KCk7XG4gICAgICAgIGlmIChvYmplY3Qud29ya2JlbmNoSW5mbykge1xuICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KG9iamVjdC53b3JrYmVuY2hJbmZvKSlcbiAgICAgICAgICAgICAgICB0aHJvdyBUeXBlRXJyb3IoXCIuUmVmcmVzaFdvcmtCZW5jaFJlcGx5LndvcmtiZW5jaEluZm86IGFycmF5IGV4cGVjdGVkXCIpO1xuICAgICAgICAgICAgbWVzc2FnZS53b3JrYmVuY2hJbmZvID0gW107XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG9iamVjdC53b3JrYmVuY2hJbmZvLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgIG1lc3NhZ2Uud29ya2JlbmNoSW5mb1tpXSA9IG9iamVjdC53b3JrYmVuY2hJbmZvW2ldIHwgMDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIHBsYWluIG9iamVjdCBmcm9tIGEgUmVmcmVzaFdvcmtCZW5jaFJlcGx5IG1lc3NhZ2UuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIG90aGVyIHR5cGVzIGlmIHNwZWNpZmllZC5cbiAgICAgKiBAZnVuY3Rpb24gdG9PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgUmVmcmVzaFdvcmtCZW5jaFJlcGx5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7UmVmcmVzaFdvcmtCZW5jaFJlcGx5fSBtZXNzYWdlIFJlZnJlc2hXb3JrQmVuY2hSZXBseVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLklDb252ZXJzaW9uT3B0aW9uc30gW29wdGlvbnNdIENvbnZlcnNpb24gb3B0aW9uc1xuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gUGxhaW4gb2JqZWN0XG4gICAgICovXG4gICAgUmVmcmVzaFdvcmtCZW5jaFJlcGx5LnRvT2JqZWN0ID0gZnVuY3Rpb24gdG9PYmplY3QobWVzc2FnZSwgb3B0aW9ucykge1xuICAgICAgICBpZiAoIW9wdGlvbnMpXG4gICAgICAgICAgICBvcHRpb25zID0ge307XG4gICAgICAgIHZhciBvYmplY3QgPSB7fTtcbiAgICAgICAgaWYgKG9wdGlvbnMuYXJyYXlzIHx8IG9wdGlvbnMuZGVmYXVsdHMpXG4gICAgICAgICAgICBvYmplY3Qud29ya2JlbmNoSW5mbyA9IFtdO1xuICAgICAgICBpZiAobWVzc2FnZS53b3JrYmVuY2hJbmZvICYmIG1lc3NhZ2Uud29ya2JlbmNoSW5mby5sZW5ndGgpIHtcbiAgICAgICAgICAgIG9iamVjdC53b3JrYmVuY2hJbmZvID0gW107XG4gICAgICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IG1lc3NhZ2Uud29ya2JlbmNoSW5mby5sZW5ndGg7ICsrailcbiAgICAgICAgICAgICAgICBvYmplY3Qud29ya2JlbmNoSW5mb1tqXSA9IG1lc3NhZ2Uud29ya2JlbmNoSW5mb1tqXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGlzIFJlZnJlc2hXb3JrQmVuY2hSZXBseSB0byBKU09OLlxuICAgICAqIEBmdW5jdGlvbiB0b0pTT05cbiAgICAgKiBAbWVtYmVyb2YgUmVmcmVzaFdvcmtCZW5jaFJlcGx5XG4gICAgICogQGluc3RhbmNlXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBKU09OIG9iamVjdFxuICAgICAqL1xuICAgIFJlZnJlc2hXb3JrQmVuY2hSZXBseS5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci50b09iamVjdCh0aGlzLCAkcHJvdG9idWYudXRpbC50b0pTT05PcHRpb25zKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIFJlZnJlc2hXb3JrQmVuY2hSZXBseTtcbn0pKCk7XG5cbiRyb290LkRlY2ltYWxWYWx1ZSA9IChmdW5jdGlvbigpIHtcblxuICAgIC8qKlxuICAgICAqIFByb3BlcnRpZXMgb2YgYSBEZWNpbWFsVmFsdWUuXG4gICAgICogQGV4cG9ydHMgSURlY2ltYWxWYWx1ZVxuICAgICAqIEBpbnRlcmZhY2UgSURlY2ltYWxWYWx1ZVxuICAgICAqIEBwcm9wZXJ0eSB7bnVtYmVyfExvbmd8bnVsbH0gW3VpbnRzXSBEZWNpbWFsVmFsdWUgdWludHNcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxudWxsfSBbbmFub3NdIERlY2ltYWxWYWx1ZSBuYW5vc1xuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQ29uc3RydWN0cyBhIG5ldyBEZWNpbWFsVmFsdWUuXG4gICAgICogQGV4cG9ydHMgRGVjaW1hbFZhbHVlXG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgRGVjaW1hbFZhbHVlLlxuICAgICAqIEBpbXBsZW1lbnRzIElEZWNpbWFsVmFsdWVcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0ge0lEZWNpbWFsVmFsdWU9fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBEZWNpbWFsVmFsdWUocHJvcGVydGllcykge1xuICAgICAgICBpZiAocHJvcGVydGllcylcbiAgICAgICAgICAgIGZvciAodmFyIGtleXMgPSBPYmplY3Qua2V5cyhwcm9wZXJ0aWVzKSwgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKVxuICAgICAgICAgICAgICAgIGlmIChwcm9wZXJ0aWVzW2tleXNbaV1dICE9IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXNba2V5c1tpXV0gPSBwcm9wZXJ0aWVzW2tleXNbaV1dO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIERlY2ltYWxWYWx1ZSB1aW50cy5cbiAgICAgKiBAbWVtYmVyIHtudW1iZXJ8TG9uZ30gdWludHNcbiAgICAgKiBAbWVtYmVyb2YgRGVjaW1hbFZhbHVlXG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgRGVjaW1hbFZhbHVlLnByb3RvdHlwZS51aW50cyA9ICR1dGlsLkxvbmcgPyAkdXRpbC5Mb25nLmZyb21CaXRzKDAsMCxmYWxzZSkgOiAwO1xuXG4gICAgLyoqXG4gICAgICogRGVjaW1hbFZhbHVlIG5hbm9zLlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gbmFub3NcbiAgICAgKiBAbWVtYmVyb2YgRGVjaW1hbFZhbHVlXG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgRGVjaW1hbFZhbHVlLnByb3RvdHlwZS5uYW5vcyA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IERlY2ltYWxWYWx1ZSBpbnN0YW5jZSB1c2luZyB0aGUgc3BlY2lmaWVkIHByb3BlcnRpZXMuXG4gICAgICogQGZ1bmN0aW9uIGNyZWF0ZVxuICAgICAqIEBtZW1iZXJvZiBEZWNpbWFsVmFsdWVcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJRGVjaW1hbFZhbHVlPX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICogQHJldHVybnMge0RlY2ltYWxWYWx1ZX0gRGVjaW1hbFZhbHVlIGluc3RhbmNlXG4gICAgICovXG4gICAgRGVjaW1hbFZhbHVlLmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShwcm9wZXJ0aWVzKSB7XG4gICAgICAgIHJldHVybiBuZXcgRGVjaW1hbFZhbHVlKHByb3BlcnRpZXMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgRGVjaW1hbFZhbHVlIG1lc3NhZ2UuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIERlY2ltYWxWYWx1ZS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlXG4gICAgICogQG1lbWJlcm9mIERlY2ltYWxWYWx1ZVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lEZWNpbWFsVmFsdWV9IG1lc3NhZ2UgRGVjaW1hbFZhbHVlIG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBEZWNpbWFsVmFsdWUuZW5jb2RlID0gZnVuY3Rpb24gZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICBpZiAoIXdyaXRlcilcbiAgICAgICAgICAgIHdyaXRlciA9ICRXcml0ZXIuY3JlYXRlKCk7XG4gICAgICAgIGlmIChtZXNzYWdlLnVpbnRzICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJ1aW50c1wiKSlcbiAgICAgICAgICAgIHdyaXRlci51aW50MzIoLyogaWQgMSwgd2lyZVR5cGUgMCA9Ki84KS5pbnQ2NChtZXNzYWdlLnVpbnRzKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UubmFub3MgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcIm5hbm9zXCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAyLCB3aXJlVHlwZSA1ID0qLzIxKS5zZml4ZWQzMihtZXNzYWdlLm5hbm9zKTtcbiAgICAgICAgcmV0dXJuIHdyaXRlcjtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIERlY2ltYWxWYWx1ZSBtZXNzYWdlLCBsZW5ndGggZGVsaW1pdGVkLiBEb2VzIG5vdCBpbXBsaWNpdGx5IHtAbGluayBEZWNpbWFsVmFsdWUudmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBEZWNpbWFsVmFsdWVcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJRGVjaW1hbFZhbHVlfSBtZXNzYWdlIERlY2ltYWxWYWx1ZSBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgRGVjaW1hbFZhbHVlLmVuY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZChtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikubGRlbGltKCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBEZWNpbWFsVmFsdWUgbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlci5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlXG4gICAgICogQG1lbWJlcm9mIERlY2ltYWxWYWx1ZVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtEZWNpbWFsVmFsdWV9IERlY2ltYWxWYWx1ZVxuICAgICAqIEB0aHJvd3Mge0Vycm9yfSBJZiB0aGUgcGF5bG9hZCBpcyBub3QgYSByZWFkZXIgb3IgdmFsaWQgYnVmZmVyXG4gICAgICogQHRocm93cyB7JHByb3RvYnVmLnV0aWwuUHJvdG9jb2xFcnJvcn0gSWYgcmVxdWlyZWQgZmllbGRzIGFyZSBtaXNzaW5nXG4gICAgICovXG4gICAgRGVjaW1hbFZhbHVlLmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LkRlY2ltYWxWYWx1ZSgpO1xuICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZCkge1xuICAgICAgICAgICAgdmFyIHRhZyA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS51aW50cyA9IHJlYWRlci5pbnQ2NCgpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UubmFub3MgPSByZWFkZXIuc2ZpeGVkMzIoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgRGVjaW1hbFZhbHVlIG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIsIGxlbmd0aCBkZWxpbWl0ZWQuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBEZWNpbWFsVmFsdWVcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUmVhZGVyfFVpbnQ4QXJyYXl9IHJlYWRlciBSZWFkZXIgb3IgYnVmZmVyIHRvIGRlY29kZSBmcm9tXG4gICAgICogQHJldHVybnMge0RlY2ltYWxWYWx1ZX0gRGVjaW1hbFZhbHVlXG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBEZWNpbWFsVmFsdWUuZGVjb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkKHJlYWRlcikge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9IG5ldyAkUmVhZGVyKHJlYWRlcik7XG4gICAgICAgIHJldHVybiB0aGlzLmRlY29kZShyZWFkZXIsIHJlYWRlci51aW50MzIoKSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFZlcmlmaWVzIGEgRGVjaW1hbFZhbHVlIG1lc3NhZ2UuXG4gICAgICogQGZ1bmN0aW9uIHZlcmlmeVxuICAgICAqIEBtZW1iZXJvZiBEZWNpbWFsVmFsdWVcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBEZWNpbWFsVmFsdWUudmVyaWZ5ID0gZnVuY3Rpb24gdmVyaWZ5KG1lc3NhZ2UpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlICE9PSBcIm9iamVjdFwiIHx8IG1lc3NhZ2UgPT09IG51bGwpXG4gICAgICAgICAgICByZXR1cm4gXCJvYmplY3QgZXhwZWN0ZWRcIjtcbiAgICAgICAgaWYgKG1lc3NhZ2UudWludHMgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwidWludHNcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLnVpbnRzKSAmJiAhKG1lc3NhZ2UudWludHMgJiYgJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UudWludHMubG93KSAmJiAkdXRpbC5pc0ludGVnZXIobWVzc2FnZS51aW50cy5oaWdoKSkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwidWludHM6IGludGVnZXJ8TG9uZyBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5uYW5vcyAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJuYW5vc1wiKSlcbiAgICAgICAgICAgIGlmICghJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UubmFub3MpKVxuICAgICAgICAgICAgICAgIHJldHVybiBcIm5hbm9zOiBpbnRlZ2VyIGV4cGVjdGVkXCI7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgRGVjaW1hbFZhbHVlIG1lc3NhZ2UgZnJvbSBhIHBsYWluIG9iamVjdC4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gdGhlaXIgcmVzcGVjdGl2ZSBpbnRlcm5hbCB0eXBlcy5cbiAgICAgKiBAZnVuY3Rpb24gZnJvbU9iamVjdFxuICAgICAqIEBtZW1iZXJvZiBEZWNpbWFsVmFsdWVcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gb2JqZWN0IFBsYWluIG9iamVjdFxuICAgICAqIEByZXR1cm5zIHtEZWNpbWFsVmFsdWV9IERlY2ltYWxWYWx1ZVxuICAgICAqL1xuICAgIERlY2ltYWxWYWx1ZS5mcm9tT2JqZWN0ID0gZnVuY3Rpb24gZnJvbU9iamVjdChvYmplY3QpIHtcbiAgICAgICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mICRyb290LkRlY2ltYWxWYWx1ZSlcbiAgICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgICAgIHZhciBtZXNzYWdlID0gbmV3ICRyb290LkRlY2ltYWxWYWx1ZSgpO1xuICAgICAgICBpZiAob2JqZWN0LnVpbnRzICE9IG51bGwpXG4gICAgICAgICAgICBpZiAoJHV0aWwuTG9uZylcbiAgICAgICAgICAgICAgICAobWVzc2FnZS51aW50cyA9ICR1dGlsLkxvbmcuZnJvbVZhbHVlKG9iamVjdC51aW50cykpLnVuc2lnbmVkID0gZmFsc2U7XG4gICAgICAgICAgICBlbHNlIGlmICh0eXBlb2Ygb2JqZWN0LnVpbnRzID09PSBcInN0cmluZ1wiKVxuICAgICAgICAgICAgICAgIG1lc3NhZ2UudWludHMgPSBwYXJzZUludChvYmplY3QudWludHMsIDEwKTtcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QudWludHMgPT09IFwibnVtYmVyXCIpXG4gICAgICAgICAgICAgICAgbWVzc2FnZS51aW50cyA9IG9iamVjdC51aW50cztcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QudWludHMgPT09IFwib2JqZWN0XCIpXG4gICAgICAgICAgICAgICAgbWVzc2FnZS51aW50cyA9IG5ldyAkdXRpbC5Mb25nQml0cyhvYmplY3QudWludHMubG93ID4+PiAwLCBvYmplY3QudWludHMuaGlnaCA+Pj4gMCkudG9OdW1iZXIoKTtcbiAgICAgICAgaWYgKG9iamVjdC5uYW5vcyAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5uYW5vcyA9IG9iamVjdC5uYW5vcyB8IDA7XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgcGxhaW4gb2JqZWN0IGZyb20gYSBEZWNpbWFsVmFsdWUgbWVzc2FnZS4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gb3RoZXIgdHlwZXMgaWYgc3BlY2lmaWVkLlxuICAgICAqIEBmdW5jdGlvbiB0b09iamVjdFxuICAgICAqIEBtZW1iZXJvZiBEZWNpbWFsVmFsdWVcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtEZWNpbWFsVmFsdWV9IG1lc3NhZ2UgRGVjaW1hbFZhbHVlXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuSUNvbnZlcnNpb25PcHRpb25zfSBbb3B0aW9uc10gQ29udmVyc2lvbiBvcHRpb25zXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBQbGFpbiBvYmplY3RcbiAgICAgKi9cbiAgICBEZWNpbWFsVmFsdWUudG9PYmplY3QgPSBmdW5jdGlvbiB0b09iamVjdChtZXNzYWdlLCBvcHRpb25zKSB7XG4gICAgICAgIGlmICghb3B0aW9ucylcbiAgICAgICAgICAgIG9wdGlvbnMgPSB7fTtcbiAgICAgICAgdmFyIG9iamVjdCA9IHt9O1xuICAgICAgICBpZiAob3B0aW9ucy5kZWZhdWx0cykge1xuICAgICAgICAgICAgaWYgKCR1dGlsLkxvbmcpIHtcbiAgICAgICAgICAgICAgICB2YXIgbG9uZyA9IG5ldyAkdXRpbC5Mb25nKDAsIDAsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICBvYmplY3QudWludHMgPSBvcHRpb25zLmxvbmdzID09PSBTdHJpbmcgPyBsb25nLnRvU3RyaW5nKCkgOiBvcHRpb25zLmxvbmdzID09PSBOdW1iZXIgPyBsb25nLnRvTnVtYmVyKCkgOiBsb25nO1xuICAgICAgICAgICAgfSBlbHNlXG4gICAgICAgICAgICAgICAgb2JqZWN0LnVpbnRzID0gb3B0aW9ucy5sb25ncyA9PT0gU3RyaW5nID8gXCIwXCIgOiAwO1xuICAgICAgICAgICAgb2JqZWN0Lm5hbm9zID0gMDtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWVzc2FnZS51aW50cyAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJ1aW50c1wiKSlcbiAgICAgICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZS51aW50cyA9PT0gXCJudW1iZXJcIilcbiAgICAgICAgICAgICAgICBvYmplY3QudWludHMgPSBvcHRpb25zLmxvbmdzID09PSBTdHJpbmcgPyBTdHJpbmcobWVzc2FnZS51aW50cykgOiBtZXNzYWdlLnVpbnRzO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIG9iamVjdC51aW50cyA9IG9wdGlvbnMubG9uZ3MgPT09IFN0cmluZyA/ICR1dGlsLkxvbmcucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobWVzc2FnZS51aW50cykgOiBvcHRpb25zLmxvbmdzID09PSBOdW1iZXIgPyBuZXcgJHV0aWwuTG9uZ0JpdHMobWVzc2FnZS51aW50cy5sb3cgPj4+IDAsIG1lc3NhZ2UudWludHMuaGlnaCA+Pj4gMCkudG9OdW1iZXIoKSA6IG1lc3NhZ2UudWludHM7XG4gICAgICAgIGlmIChtZXNzYWdlLm5hbm9zICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcIm5hbm9zXCIpKVxuICAgICAgICAgICAgb2JqZWN0Lm5hbm9zID0gbWVzc2FnZS5uYW5vcztcbiAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ29udmVydHMgdGhpcyBEZWNpbWFsVmFsdWUgdG8gSlNPTi5cbiAgICAgKiBAZnVuY3Rpb24gdG9KU09OXG4gICAgICogQG1lbWJlcm9mIERlY2ltYWxWYWx1ZVxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gSlNPTiBvYmplY3RcbiAgICAgKi9cbiAgICBEZWNpbWFsVmFsdWUucHJvdG90eXBlLnRvSlNPTiA9IGZ1bmN0aW9uIHRvSlNPTigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29uc3RydWN0b3IudG9PYmplY3QodGhpcywgJHByb3RvYnVmLnV0aWwudG9KU09OT3B0aW9ucyk7XG4gICAgfTtcblxuICAgIHJldHVybiBEZWNpbWFsVmFsdWU7XG59KSgpO1xuXG4kcm9vdC5Vc2VySW5mbyA9IChmdW5jdGlvbigpIHtcblxuICAgIC8qKlxuICAgICAqIFByb3BlcnRpZXMgb2YgYSBVc2VySW5mby5cbiAgICAgKiBAZXhwb3J0cyBJVXNlckluZm9cbiAgICAgKiBAaW50ZXJmYWNlIElVc2VySW5mb1xuICAgICAqIEBwcm9wZXJ0eSB7c3RyaW5nfG51bGx9IFthY2NvdW50XSBVc2VySW5mbyBhY2NvdW50XG4gICAgICogQHByb3BlcnR5IHtzdHJpbmd8bnVsbH0gW25pY2tuYW1lXSBVc2VySW5mbyBuaWNrbmFtZVxuICAgICAqIEBwcm9wZXJ0eSB7c3RyaW5nfG51bGx9IFthdmF0YXJVcmxdIFVzZXJJbmZvIGF2YXRhclVybFxuICAgICAqIEBwcm9wZXJ0eSB7SURlY2ltYWxWYWx1ZXxudWxsfSBbbW9uZXldIFVzZXJJbmZvIG1vbmV5XG4gICAgICovXG5cbiAgICAvKipcbiAgICAgKiBDb25zdHJ1Y3RzIGEgbmV3IFVzZXJJbmZvLlxuICAgICAqIEBleHBvcnRzIFVzZXJJbmZvXG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgVXNlckluZm8uXG4gICAgICogQGltcGxlbWVudHMgSVVzZXJJbmZvXG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICogQHBhcmFtIHtJVXNlckluZm89fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBVc2VySW5mbyhwcm9wZXJ0aWVzKSB7XG4gICAgICAgIGlmIChwcm9wZXJ0aWVzKVxuICAgICAgICAgICAgZm9yICh2YXIga2V5cyA9IE9iamVjdC5rZXlzKHByb3BlcnRpZXMpLCBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpXG4gICAgICAgICAgICAgICAgaWYgKHByb3BlcnRpZXNba2V5c1tpXV0gIT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgdGhpc1trZXlzW2ldXSA9IHByb3BlcnRpZXNba2V5c1tpXV07XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVXNlckluZm8gYWNjb3VudC5cbiAgICAgKiBAbWVtYmVyIHtzdHJpbmd9IGFjY291bnRcbiAgICAgKiBAbWVtYmVyb2YgVXNlckluZm9cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBVc2VySW5mby5wcm90b3R5cGUuYWNjb3VudCA9IFwiXCI7XG5cbiAgICAvKipcbiAgICAgKiBVc2VySW5mbyBuaWNrbmFtZS5cbiAgICAgKiBAbWVtYmVyIHtzdHJpbmd9IG5pY2tuYW1lXG4gICAgICogQG1lbWJlcm9mIFVzZXJJbmZvXG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgVXNlckluZm8ucHJvdG90eXBlLm5pY2tuYW1lID0gXCJcIjtcblxuICAgIC8qKlxuICAgICAqIFVzZXJJbmZvIGF2YXRhclVybC5cbiAgICAgKiBAbWVtYmVyIHtzdHJpbmd9IGF2YXRhclVybFxuICAgICAqIEBtZW1iZXJvZiBVc2VySW5mb1xuICAgICAqIEBpbnN0YW5jZVxuICAgICAqL1xuICAgIFVzZXJJbmZvLnByb3RvdHlwZS5hdmF0YXJVcmwgPSBcIlwiO1xuXG4gICAgLyoqXG4gICAgICogVXNlckluZm8gbW9uZXkuXG4gICAgICogQG1lbWJlciB7SURlY2ltYWxWYWx1ZXxudWxsfHVuZGVmaW5lZH0gbW9uZXlcbiAgICAgKiBAbWVtYmVyb2YgVXNlckluZm9cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBVc2VySW5mby5wcm90b3R5cGUubW9uZXkgPSBudWxsO1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBVc2VySW5mbyBpbnN0YW5jZSB1c2luZyB0aGUgc3BlY2lmaWVkIHByb3BlcnRpZXMuXG4gICAgICogQGZ1bmN0aW9uIGNyZWF0ZVxuICAgICAqIEBtZW1iZXJvZiBVc2VySW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lVc2VySW5mbz19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqIEByZXR1cm5zIHtVc2VySW5mb30gVXNlckluZm8gaW5zdGFuY2VcbiAgICAgKi9cbiAgICBVc2VySW5mby5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUocHJvcGVydGllcykge1xuICAgICAgICByZXR1cm4gbmV3IFVzZXJJbmZvKHByb3BlcnRpZXMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgVXNlckluZm8gbWVzc2FnZS4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgVXNlckluZm8udmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZVxuICAgICAqIEBtZW1iZXJvZiBVc2VySW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lVc2VySW5mb30gbWVzc2FnZSBVc2VySW5mbyBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgVXNlckluZm8uZW5jb2RlID0gZnVuY3Rpb24gZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICBpZiAoIXdyaXRlcilcbiAgICAgICAgICAgIHdyaXRlciA9ICRXcml0ZXIuY3JlYXRlKCk7XG4gICAgICAgIGlmIChtZXNzYWdlLmFjY291bnQgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImFjY291bnRcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDEsIHdpcmVUeXBlIDIgPSovMTApLnN0cmluZyhtZXNzYWdlLmFjY291bnQpO1xuICAgICAgICBpZiAobWVzc2FnZS5uaWNrbmFtZSAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwibmlja25hbWVcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDIsIHdpcmVUeXBlIDIgPSovMTgpLnN0cmluZyhtZXNzYWdlLm5pY2tuYW1lKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UuYXZhdGFyVXJsICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJhdmF0YXJVcmxcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDMsIHdpcmVUeXBlIDIgPSovMjYpLnN0cmluZyhtZXNzYWdlLmF2YXRhclVybCk7XG4gICAgICAgIGlmIChtZXNzYWdlLm1vbmV5ICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJtb25leVwiKSlcbiAgICAgICAgICAgICRyb290LkRlY2ltYWxWYWx1ZS5lbmNvZGUobWVzc2FnZS5tb25leSwgd3JpdGVyLnVpbnQzMigvKiBpZCA0LCB3aXJlVHlwZSAyID0qLzM0KS5mb3JrKCkpLmxkZWxpbSgpO1xuICAgICAgICByZXR1cm4gd3JpdGVyO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgVXNlckluZm8gbWVzc2FnZSwgbGVuZ3RoIGRlbGltaXRlZC4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgVXNlckluZm8udmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBVc2VySW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lVc2VySW5mb30gbWVzc2FnZSBVc2VySW5mbyBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgVXNlckluZm8uZW5jb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5lbmNvZGUobWVzc2FnZSwgd3JpdGVyKS5sZGVsaW0oKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhIFVzZXJJbmZvIG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZVxuICAgICAqIEBtZW1iZXJvZiBVc2VySW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gW2xlbmd0aF0gTWVzc2FnZSBsZW5ndGggaWYga25vd24gYmVmb3JlaGFuZFxuICAgICAqIEByZXR1cm5zIHtVc2VySW5mb30gVXNlckluZm9cbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIFVzZXJJbmZvLmRlY29kZSA9IGZ1bmN0aW9uIGRlY29kZShyZWFkZXIsIGxlbmd0aCkge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9ICRSZWFkZXIuY3JlYXRlKHJlYWRlcik7XG4gICAgICAgIHZhciBlbmQgPSBsZW5ndGggPT09IHVuZGVmaW5lZCA/IHJlYWRlci5sZW4gOiByZWFkZXIucG9zICsgbGVuZ3RoLCBtZXNzYWdlID0gbmV3ICRyb290LlVzZXJJbmZvKCk7XG4gICAgICAgIHdoaWxlIChyZWFkZXIucG9zIDwgZW5kKSB7XG4gICAgICAgICAgICB2YXIgdGFnID0gcmVhZGVyLnVpbnQzMigpO1xuICAgICAgICAgICAgc3dpdGNoICh0YWcgPj4+IDMpIHtcbiAgICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmFjY291bnQgPSByZWFkZXIuc3RyaW5nKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5uaWNrbmFtZSA9IHJlYWRlci5zdHJpbmcoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgMzpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmF2YXRhclVybCA9IHJlYWRlci5zdHJpbmcoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgNDpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLm1vbmV5ID0gJHJvb3QuRGVjaW1hbFZhbHVlLmRlY29kZShyZWFkZXIsIHJlYWRlci51aW50MzIoKSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHJlYWRlci5za2lwVHlwZSh0YWcgJiA3KTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhIFVzZXJJbmZvIG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIsIGxlbmd0aCBkZWxpbWl0ZWQuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBVc2VySW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5SZWFkZXJ8VWludDhBcnJheX0gcmVhZGVyIFJlYWRlciBvciBidWZmZXIgdG8gZGVjb2RlIGZyb21cbiAgICAgKiBAcmV0dXJucyB7VXNlckluZm99IFVzZXJJbmZvXG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBVc2VySW5mby5kZWNvZGVEZWxpbWl0ZWQgPSBmdW5jdGlvbiBkZWNvZGVEZWxpbWl0ZWQocmVhZGVyKSB7XG4gICAgICAgIGlmICghKHJlYWRlciBpbnN0YW5jZW9mICRSZWFkZXIpKVxuICAgICAgICAgICAgcmVhZGVyID0gbmV3ICRSZWFkZXIocmVhZGVyKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVjb2RlKHJlYWRlciwgcmVhZGVyLnVpbnQzMigpKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVmVyaWZpZXMgYSBVc2VySW5mbyBtZXNzYWdlLlxuICAgICAqIEBmdW5jdGlvbiB2ZXJpZnlcbiAgICAgKiBAbWVtYmVyb2YgVXNlckluZm9cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBVc2VySW5mby52ZXJpZnkgPSBmdW5jdGlvbiB2ZXJpZnkobWVzc2FnZSkge1xuICAgICAgICBpZiAodHlwZW9mIG1lc3NhZ2UgIT09IFwib2JqZWN0XCIgfHwgbWVzc2FnZSA9PT0gbnVsbClcbiAgICAgICAgICAgIHJldHVybiBcIm9iamVjdCBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5hY2NvdW50ICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImFjY291bnRcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzU3RyaW5nKG1lc3NhZ2UuYWNjb3VudCkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiYWNjb3VudDogc3RyaW5nIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLm5pY2tuYW1lICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcIm5pY2tuYW1lXCIpKVxuICAgICAgICAgICAgaWYgKCEkdXRpbC5pc1N0cmluZyhtZXNzYWdlLm5pY2tuYW1lKSlcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJuaWNrbmFtZTogc3RyaW5nIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLmF2YXRhclVybCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJhdmF0YXJVcmxcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzU3RyaW5nKG1lc3NhZ2UuYXZhdGFyVXJsKSlcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJhdmF0YXJVcmw6IHN0cmluZyBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5tb25leSAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJtb25leVwiKSkge1xuICAgICAgICAgICAgdmFyIGVycm9yID0gJHJvb3QuRGVjaW1hbFZhbHVlLnZlcmlmeShtZXNzYWdlLm1vbmV5KTtcbiAgICAgICAgICAgIGlmIChlcnJvcilcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJtb25leS5cIiArIGVycm9yO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgVXNlckluZm8gbWVzc2FnZSBmcm9tIGEgcGxhaW4gb2JqZWN0LiBBbHNvIGNvbnZlcnRzIHZhbHVlcyB0byB0aGVpciByZXNwZWN0aXZlIGludGVybmFsIHR5cGVzLlxuICAgICAqIEBmdW5jdGlvbiBmcm9tT2JqZWN0XG4gICAgICogQG1lbWJlcm9mIFVzZXJJbmZvXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7T2JqZWN0LjxzdHJpbmcsKj59IG9iamVjdCBQbGFpbiBvYmplY3RcbiAgICAgKiBAcmV0dXJucyB7VXNlckluZm99IFVzZXJJbmZvXG4gICAgICovXG4gICAgVXNlckluZm8uZnJvbU9iamVjdCA9IGZ1bmN0aW9uIGZyb21PYmplY3Qob2JqZWN0KSB7XG4gICAgICAgIGlmIChvYmplY3QgaW5zdGFuY2VvZiAkcm9vdC5Vc2VySW5mbylcbiAgICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgICAgIHZhciBtZXNzYWdlID0gbmV3ICRyb290LlVzZXJJbmZvKCk7XG4gICAgICAgIGlmIChvYmplY3QuYWNjb3VudCAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5hY2NvdW50ID0gU3RyaW5nKG9iamVjdC5hY2NvdW50KTtcbiAgICAgICAgaWYgKG9iamVjdC5uaWNrbmFtZSAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5uaWNrbmFtZSA9IFN0cmluZyhvYmplY3Qubmlja25hbWUpO1xuICAgICAgICBpZiAob2JqZWN0LmF2YXRhclVybCAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5hdmF0YXJVcmwgPSBTdHJpbmcob2JqZWN0LmF2YXRhclVybCk7XG4gICAgICAgIGlmIChvYmplY3QubW9uZXkgIT0gbnVsbCkge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBvYmplY3QubW9uZXkgIT09IFwib2JqZWN0XCIpXG4gICAgICAgICAgICAgICAgdGhyb3cgVHlwZUVycm9yKFwiLlVzZXJJbmZvLm1vbmV5OiBvYmplY3QgZXhwZWN0ZWRcIik7XG4gICAgICAgICAgICBtZXNzYWdlLm1vbmV5ID0gJHJvb3QuRGVjaW1hbFZhbHVlLmZyb21PYmplY3Qob2JqZWN0Lm1vbmV5KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIHBsYWluIG9iamVjdCBmcm9tIGEgVXNlckluZm8gbWVzc2FnZS4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gb3RoZXIgdHlwZXMgaWYgc3BlY2lmaWVkLlxuICAgICAqIEBmdW5jdGlvbiB0b09iamVjdFxuICAgICAqIEBtZW1iZXJvZiBVc2VySW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge1VzZXJJbmZvfSBtZXNzYWdlIFVzZXJJbmZvXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuSUNvbnZlcnNpb25PcHRpb25zfSBbb3B0aW9uc10gQ29udmVyc2lvbiBvcHRpb25zXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBQbGFpbiBvYmplY3RcbiAgICAgKi9cbiAgICBVc2VySW5mby50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG1lc3NhZ2UsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgICB2YXIgb2JqZWN0ID0ge307XG4gICAgICAgIGlmIChvcHRpb25zLmRlZmF1bHRzKSB7XG4gICAgICAgICAgICBvYmplY3QuYWNjb3VudCA9IFwiXCI7XG4gICAgICAgICAgICBvYmplY3Qubmlja25hbWUgPSBcIlwiO1xuICAgICAgICAgICAgb2JqZWN0LmF2YXRhclVybCA9IFwiXCI7XG4gICAgICAgICAgICBvYmplY3QubW9uZXkgPSBudWxsO1xuICAgICAgICB9XG4gICAgICAgIGlmIChtZXNzYWdlLmFjY291bnQgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiYWNjb3VudFwiKSlcbiAgICAgICAgICAgIG9iamVjdC5hY2NvdW50ID0gbWVzc2FnZS5hY2NvdW50O1xuICAgICAgICBpZiAobWVzc2FnZS5uaWNrbmFtZSAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJuaWNrbmFtZVwiKSlcbiAgICAgICAgICAgIG9iamVjdC5uaWNrbmFtZSA9IG1lc3NhZ2Uubmlja25hbWU7XG4gICAgICAgIGlmIChtZXNzYWdlLmF2YXRhclVybCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJhdmF0YXJVcmxcIikpXG4gICAgICAgICAgICBvYmplY3QuYXZhdGFyVXJsID0gbWVzc2FnZS5hdmF0YXJVcmw7XG4gICAgICAgIGlmIChtZXNzYWdlLm1vbmV5ICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcIm1vbmV5XCIpKVxuICAgICAgICAgICAgb2JqZWN0Lm1vbmV5ID0gJHJvb3QuRGVjaW1hbFZhbHVlLnRvT2JqZWN0KG1lc3NhZ2UubW9uZXksIG9wdGlvbnMpO1xuICAgICAgICByZXR1cm4gb2JqZWN0O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGlzIFVzZXJJbmZvIHRvIEpTT04uXG4gICAgICogQGZ1bmN0aW9uIHRvSlNPTlxuICAgICAqIEBtZW1iZXJvZiBVc2VySW5mb1xuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gSlNPTiBvYmplY3RcbiAgICAgKi9cbiAgICBVc2VySW5mby5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci50b09iamVjdCh0aGlzLCAkcHJvdG9idWYudXRpbC50b0pTT05PcHRpb25zKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIFVzZXJJbmZvO1xufSkoKTtcblxuJHJvb3QuVXNlck1vbmV5ID0gKGZ1bmN0aW9uKCkge1xuXG4gICAgLyoqXG4gICAgICogUHJvcGVydGllcyBvZiBhIFVzZXJNb25leS5cbiAgICAgKiBAZXhwb3J0cyBJVXNlck1vbmV5XG4gICAgICogQGludGVyZmFjZSBJVXNlck1vbmV5XG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8bnVsbH0gW2NvaW5dIFVzZXJNb25leSBjb2luXG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8bnVsbH0gW2RpYW1vbmRdIFVzZXJNb25leSBkaWFtb25kXG4gICAgICogQHByb3BlcnR5IHtudW1iZXJ8bnVsbH0gW21vbmV5XSBVc2VyTW9uZXkgbW9uZXlcbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIENvbnN0cnVjdHMgYSBuZXcgVXNlck1vbmV5LlxuICAgICAqIEBleHBvcnRzIFVzZXJNb25leVxuICAgICAqIEBjbGFzc2Rlc2MgUmVwcmVzZW50cyBhIFVzZXJNb25leS5cbiAgICAgKiBAaW1wbGVtZW50cyBJVXNlck1vbmV5XG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICogQHBhcmFtIHtJVXNlck1vbmV5PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICovXG4gICAgZnVuY3Rpb24gVXNlck1vbmV5KHByb3BlcnRpZXMpIHtcbiAgICAgICAgaWYgKHByb3BlcnRpZXMpXG4gICAgICAgICAgICBmb3IgKHZhciBrZXlzID0gT2JqZWN0LmtleXMocHJvcGVydGllcyksIGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7ICsraSlcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydGllc1trZXlzW2ldXSAhPSBudWxsKVxuICAgICAgICAgICAgICAgICAgICB0aGlzW2tleXNbaV1dID0gcHJvcGVydGllc1trZXlzW2ldXTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBVc2VyTW9uZXkgY29pbi5cbiAgICAgKiBAbWVtYmVyIHtudW1iZXJ9IGNvaW5cbiAgICAgKiBAbWVtYmVyb2YgVXNlck1vbmV5XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgVXNlck1vbmV5LnByb3RvdHlwZS5jb2luID0gMDtcblxuICAgIC8qKlxuICAgICAqIFVzZXJNb25leSBkaWFtb25kLlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gZGlhbW9uZFxuICAgICAqIEBtZW1iZXJvZiBVc2VyTW9uZXlcbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBVc2VyTW9uZXkucHJvdG90eXBlLmRpYW1vbmQgPSAwO1xuXG4gICAgLyoqXG4gICAgICogVXNlck1vbmV5IG1vbmV5LlxuICAgICAqIEBtZW1iZXIge251bWJlcn0gbW9uZXlcbiAgICAgKiBAbWVtYmVyb2YgVXNlck1vbmV5XG4gICAgICogQGluc3RhbmNlXG4gICAgICovXG4gICAgVXNlck1vbmV5LnByb3RvdHlwZS5tb25leSA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IFVzZXJNb25leSBpbnN0YW5jZSB1c2luZyB0aGUgc3BlY2lmaWVkIHByb3BlcnRpZXMuXG4gICAgICogQGZ1bmN0aW9uIGNyZWF0ZVxuICAgICAqIEBtZW1iZXJvZiBVc2VyTW9uZXlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJVXNlck1vbmV5PX0gW3Byb3BlcnRpZXNdIFByb3BlcnRpZXMgdG8gc2V0XG4gICAgICogQHJldHVybnMge1VzZXJNb25leX0gVXNlck1vbmV5IGluc3RhbmNlXG4gICAgICovXG4gICAgVXNlck1vbmV5LmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShwcm9wZXJ0aWVzKSB7XG4gICAgICAgIHJldHVybiBuZXcgVXNlck1vbmV5KHByb3BlcnRpZXMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgVXNlck1vbmV5IG1lc3NhZ2UuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIFVzZXJNb25leS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlXG4gICAgICogQG1lbWJlcm9mIFVzZXJNb25leVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lVc2VyTW9uZXl9IG1lc3NhZ2UgVXNlck1vbmV5IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBVc2VyTW9uZXkuZW5jb2RlID0gZnVuY3Rpb24gZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICBpZiAoIXdyaXRlcilcbiAgICAgICAgICAgIHdyaXRlciA9ICRXcml0ZXIuY3JlYXRlKCk7XG4gICAgICAgIGlmIChtZXNzYWdlLmNvaW4gIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImNvaW5cIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDEsIHdpcmVUeXBlIDEgPSovOSkuZG91YmxlKG1lc3NhZ2UuY29pbik7XG4gICAgICAgIGlmIChtZXNzYWdlLmRpYW1vbmQgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImRpYW1vbmRcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDIsIHdpcmVUeXBlIDEgPSovMTcpLmRvdWJsZShtZXNzYWdlLmRpYW1vbmQpO1xuICAgICAgICBpZiAobWVzc2FnZS5tb25leSAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwibW9uZXlcIikpXG4gICAgICAgICAgICB3cml0ZXIudWludDMyKC8qIGlkIDMsIHdpcmVUeXBlIDUgPSovMjkpLmZsb2F0KG1lc3NhZ2UubW9uZXkpO1xuICAgICAgICByZXR1cm4gd3JpdGVyO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgVXNlck1vbmV5IG1lc3NhZ2UsIGxlbmd0aCBkZWxpbWl0ZWQuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIFVzZXJNb25leS52ZXJpZnl8dmVyaWZ5fSBtZXNzYWdlcy5cbiAgICAgKiBAZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIFVzZXJNb25leVxuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lVc2VyTW9uZXl9IG1lc3NhZ2UgVXNlck1vbmV5IG1lc3NhZ2Ugb3IgcGxhaW4gb2JqZWN0IHRvIGVuY29kZVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLldyaXRlcn0gW3dyaXRlcl0gV3JpdGVyIHRvIGVuY29kZSB0b1xuICAgICAqIEByZXR1cm5zIHskcHJvdG9idWYuV3JpdGVyfSBXcml0ZXJcbiAgICAgKi9cbiAgICBVc2VyTW9uZXkuZW5jb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZW5jb2RlRGVsaW1pdGVkKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5lbmNvZGUobWVzc2FnZSwgd3JpdGVyKS5sZGVsaW0oKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRGVjb2RlcyBhIFVzZXJNb25leSBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVcbiAgICAgKiBAbWVtYmVyb2YgVXNlck1vbmV5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBbbGVuZ3RoXSBNZXNzYWdlIGxlbmd0aCBpZiBrbm93biBiZWZvcmVoYW5kXG4gICAgICogQHJldHVybnMge1VzZXJNb25leX0gVXNlck1vbmV5XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBVc2VyTW9uZXkuZGVjb2RlID0gZnVuY3Rpb24gZGVjb2RlKHJlYWRlciwgbGVuZ3RoKSB7XG4gICAgICAgIGlmICghKHJlYWRlciBpbnN0YW5jZW9mICRSZWFkZXIpKVxuICAgICAgICAgICAgcmVhZGVyID0gJFJlYWRlci5jcmVhdGUocmVhZGVyKTtcbiAgICAgICAgdmFyIGVuZCA9IGxlbmd0aCA9PT0gdW5kZWZpbmVkID8gcmVhZGVyLmxlbiA6IHJlYWRlci5wb3MgKyBsZW5ndGgsIG1lc3NhZ2UgPSBuZXcgJHJvb3QuVXNlck1vbmV5KCk7XG4gICAgICAgIHdoaWxlIChyZWFkZXIucG9zIDwgZW5kKSB7XG4gICAgICAgICAgICB2YXIgdGFnID0gcmVhZGVyLnVpbnQzMigpO1xuICAgICAgICAgICAgc3dpdGNoICh0YWcgPj4+IDMpIHtcbiAgICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW4gPSByZWFkZXIuZG91YmxlKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5kaWFtb25kID0gcmVhZGVyLmRvdWJsZSgpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAzOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UubW9uZXkgPSByZWFkZXIuZmxvYXQoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgVXNlck1vbmV5IG1lc3NhZ2UgZnJvbSB0aGUgc3BlY2lmaWVkIHJlYWRlciBvciBidWZmZXIsIGxlbmd0aCBkZWxpbWl0ZWQuXG4gICAgICogQGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZFxuICAgICAqIEBtZW1iZXJvZiBVc2VyTW9uZXlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUmVhZGVyfFVpbnQ4QXJyYXl9IHJlYWRlciBSZWFkZXIgb3IgYnVmZmVyIHRvIGRlY29kZSBmcm9tXG4gICAgICogQHJldHVybnMge1VzZXJNb25leX0gVXNlck1vbmV5XG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBVc2VyTW9uZXkuZGVjb2RlRGVsaW1pdGVkID0gZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkKHJlYWRlcikge1xuICAgICAgICBpZiAoIShyZWFkZXIgaW5zdGFuY2VvZiAkUmVhZGVyKSlcbiAgICAgICAgICAgIHJlYWRlciA9IG5ldyAkUmVhZGVyKHJlYWRlcik7XG4gICAgICAgIHJldHVybiB0aGlzLmRlY29kZShyZWFkZXIsIHJlYWRlci51aW50MzIoKSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFZlcmlmaWVzIGEgVXNlck1vbmV5IG1lc3NhZ2UuXG4gICAgICogQGZ1bmN0aW9uIHZlcmlmeVxuICAgICAqIEBtZW1iZXJvZiBVc2VyTW9uZXlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gbWVzc2FnZSBQbGFpbiBvYmplY3QgdG8gdmVyaWZ5XG4gICAgICogQHJldHVybnMge3N0cmluZ3xudWxsfSBgbnVsbGAgaWYgdmFsaWQsIG90aGVyd2lzZSB0aGUgcmVhc29uIHdoeSBpdCBpcyBub3RcbiAgICAgKi9cbiAgICBVc2VyTW9uZXkudmVyaWZ5ID0gZnVuY3Rpb24gdmVyaWZ5KG1lc3NhZ2UpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlICE9PSBcIm9iamVjdFwiIHx8IG1lc3NhZ2UgPT09IG51bGwpXG4gICAgICAgICAgICByZXR1cm4gXCJvYmplY3QgZXhwZWN0ZWRcIjtcbiAgICAgICAgaWYgKG1lc3NhZ2UuY29pbiAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJjb2luXCIpKVxuICAgICAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlLmNvaW4gIT09IFwibnVtYmVyXCIpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiY29pbjogbnVtYmVyIGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLmRpYW1vbmQgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiZGlhbW9uZFwiKSlcbiAgICAgICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZS5kaWFtb25kICE9PSBcIm51bWJlclwiKVxuICAgICAgICAgICAgICAgIHJldHVybiBcImRpYW1vbmQ6IG51bWJlciBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5tb25leSAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJtb25leVwiKSlcbiAgICAgICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZS5tb25leSAhPT0gXCJudW1iZXJcIilcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJtb25leTogbnVtYmVyIGV4cGVjdGVkXCI7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgVXNlck1vbmV5IG1lc3NhZ2UgZnJvbSBhIHBsYWluIG9iamVjdC4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gdGhlaXIgcmVzcGVjdGl2ZSBpbnRlcm5hbCB0eXBlcy5cbiAgICAgKiBAZnVuY3Rpb24gZnJvbU9iamVjdFxuICAgICAqIEBtZW1iZXJvZiBVc2VyTW9uZXlcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gb2JqZWN0IFBsYWluIG9iamVjdFxuICAgICAqIEByZXR1cm5zIHtVc2VyTW9uZXl9IFVzZXJNb25leVxuICAgICAqL1xuICAgIFVzZXJNb25leS5mcm9tT2JqZWN0ID0gZnVuY3Rpb24gZnJvbU9iamVjdChvYmplY3QpIHtcbiAgICAgICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mICRyb290LlVzZXJNb25leSlcbiAgICAgICAgICAgIHJldHVybiBvYmplY3Q7XG4gICAgICAgIHZhciBtZXNzYWdlID0gbmV3ICRyb290LlVzZXJNb25leSgpO1xuICAgICAgICBpZiAob2JqZWN0LmNvaW4gIT0gbnVsbClcbiAgICAgICAgICAgIG1lc3NhZ2UuY29pbiA9IE51bWJlcihvYmplY3QuY29pbik7XG4gICAgICAgIGlmIChvYmplY3QuZGlhbW9uZCAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5kaWFtb25kID0gTnVtYmVyKG9iamVjdC5kaWFtb25kKTtcbiAgICAgICAgaWYgKG9iamVjdC5tb25leSAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5tb25leSA9IE51bWJlcihvYmplY3QubW9uZXkpO1xuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIHBsYWluIG9iamVjdCBmcm9tIGEgVXNlck1vbmV5IG1lc3NhZ2UuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIG90aGVyIHR5cGVzIGlmIHNwZWNpZmllZC5cbiAgICAgKiBAZnVuY3Rpb24gdG9PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgVXNlck1vbmV5XG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7VXNlck1vbmV5fSBtZXNzYWdlIFVzZXJNb25leVxuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLklDb252ZXJzaW9uT3B0aW9uc30gW29wdGlvbnNdIENvbnZlcnNpb24gb3B0aW9uc1xuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gUGxhaW4gb2JqZWN0XG4gICAgICovXG4gICAgVXNlck1vbmV5LnRvT2JqZWN0ID0gZnVuY3Rpb24gdG9PYmplY3QobWVzc2FnZSwgb3B0aW9ucykge1xuICAgICAgICBpZiAoIW9wdGlvbnMpXG4gICAgICAgICAgICBvcHRpb25zID0ge307XG4gICAgICAgIHZhciBvYmplY3QgPSB7fTtcbiAgICAgICAgaWYgKG9wdGlvbnMuZGVmYXVsdHMpIHtcbiAgICAgICAgICAgIG9iamVjdC5jb2luID0gMDtcbiAgICAgICAgICAgIG9iamVjdC5kaWFtb25kID0gMDtcbiAgICAgICAgICAgIG9iamVjdC5tb25leSA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1lc3NhZ2UuY29pbiAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJjb2luXCIpKVxuICAgICAgICAgICAgb2JqZWN0LmNvaW4gPSBvcHRpb25zLmpzb24gJiYgIWlzRmluaXRlKG1lc3NhZ2UuY29pbikgPyBTdHJpbmcobWVzc2FnZS5jb2luKSA6IG1lc3NhZ2UuY29pbjtcbiAgICAgICAgaWYgKG1lc3NhZ2UuZGlhbW9uZCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJkaWFtb25kXCIpKVxuICAgICAgICAgICAgb2JqZWN0LmRpYW1vbmQgPSBvcHRpb25zLmpzb24gJiYgIWlzRmluaXRlKG1lc3NhZ2UuZGlhbW9uZCkgPyBTdHJpbmcobWVzc2FnZS5kaWFtb25kKSA6IG1lc3NhZ2UuZGlhbW9uZDtcbiAgICAgICAgaWYgKG1lc3NhZ2UubW9uZXkgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwibW9uZXlcIikpXG4gICAgICAgICAgICBvYmplY3QubW9uZXkgPSBvcHRpb25zLmpzb24gJiYgIWlzRmluaXRlKG1lc3NhZ2UubW9uZXkpID8gU3RyaW5nKG1lc3NhZ2UubW9uZXkpIDogbWVzc2FnZS5tb25leTtcbiAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ29udmVydHMgdGhpcyBVc2VyTW9uZXkgdG8gSlNPTi5cbiAgICAgKiBAZnVuY3Rpb24gdG9KU09OXG4gICAgICogQG1lbWJlcm9mIFVzZXJNb25leVxuICAgICAqIEBpbnN0YW5jZVxuICAgICAqIEByZXR1cm5zIHtPYmplY3QuPHN0cmluZywqPn0gSlNPTiBvYmplY3RcbiAgICAgKi9cbiAgICBVc2VyTW9uZXkucHJvdG90eXBlLnRvSlNPTiA9IGZ1bmN0aW9uIHRvSlNPTigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29uc3RydWN0b3IudG9PYmplY3QodGhpcywgJHByb3RvYnVmLnV0aWwudG9KU09OT3B0aW9ucyk7XG4gICAgfTtcblxuICAgIHJldHVybiBVc2VyTW9uZXk7XG59KSgpO1xuXG4kcm9vdC5Xb3JrQmVuY2hJdGVtSW5mbyA9IChmdW5jdGlvbigpIHtcblxuICAgIC8qKlxuICAgICAqIFByb3BlcnRpZXMgb2YgYSBXb3JrQmVuY2hJdGVtSW5mby5cbiAgICAgKiBAZXhwb3J0cyBJV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKiBAaW50ZXJmYWNlIElXb3JrQmVuY2hJdGVtSW5mb1xuICAgICAqIEBwcm9wZXJ0eSB7bnVtYmVyfG51bGx9IFtsZXZlbF0gV29ya0JlbmNoSXRlbUluZm8gbGV2ZWxcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxMb25nfG51bGx9IFtjb2luQ29zdF0gV29ya0JlbmNoSXRlbUluZm8gY29pbkNvc3RcbiAgICAgKiBAcHJvcGVydHkge251bWJlcnxMb25nfG51bGx9IFtjb2luSW5jb21lXSBXb3JrQmVuY2hJdGVtSW5mbyBjb2luSW5jb21lXG4gICAgICovXG5cbiAgICAvKipcbiAgICAgKiBDb25zdHJ1Y3RzIGEgbmV3IFdvcmtCZW5jaEl0ZW1JbmZvLlxuICAgICAqIEBleHBvcnRzIFdvcmtCZW5jaEl0ZW1JbmZvXG4gICAgICogQGNsYXNzZGVzYyBSZXByZXNlbnRzIGEgV29ya0JlbmNoSXRlbUluZm8uXG4gICAgICogQGltcGxlbWVudHMgSVdvcmtCZW5jaEl0ZW1JbmZvXG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICogQHBhcmFtIHtJV29ya0JlbmNoSXRlbUluZm89fSBbcHJvcGVydGllc10gUHJvcGVydGllcyB0byBzZXRcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBXb3JrQmVuY2hJdGVtSW5mbyhwcm9wZXJ0aWVzKSB7XG4gICAgICAgIGlmIChwcm9wZXJ0aWVzKVxuICAgICAgICAgICAgZm9yICh2YXIga2V5cyA9IE9iamVjdC5rZXlzKHByb3BlcnRpZXMpLCBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpXG4gICAgICAgICAgICAgICAgaWYgKHByb3BlcnRpZXNba2V5c1tpXV0gIT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgdGhpc1trZXlzW2ldXSA9IHByb3BlcnRpZXNba2V5c1tpXV07XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogV29ya0JlbmNoSXRlbUluZm8gbGV2ZWwuXG4gICAgICogQG1lbWJlciB7bnVtYmVyfSBsZXZlbFxuICAgICAqIEBtZW1iZXJvZiBXb3JrQmVuY2hJdGVtSW5mb1xuICAgICAqIEBpbnN0YW5jZVxuICAgICAqL1xuICAgIFdvcmtCZW5jaEl0ZW1JbmZvLnByb3RvdHlwZS5sZXZlbCA9IDA7XG5cbiAgICAvKipcbiAgICAgKiBXb3JrQmVuY2hJdGVtSW5mbyBjb2luQ29zdC5cbiAgICAgKiBAbWVtYmVyIHtudW1iZXJ8TG9uZ30gY29pbkNvc3RcbiAgICAgKiBAbWVtYmVyb2YgV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBXb3JrQmVuY2hJdGVtSW5mby5wcm90b3R5cGUuY29pbkNvc3QgPSAkdXRpbC5Mb25nID8gJHV0aWwuTG9uZy5mcm9tQml0cygwLDAsZmFsc2UpIDogMDtcblxuICAgIC8qKlxuICAgICAqIFdvcmtCZW5jaEl0ZW1JbmZvIGNvaW5JbmNvbWUuXG4gICAgICogQG1lbWJlciB7bnVtYmVyfExvbmd9IGNvaW5JbmNvbWVcbiAgICAgKiBAbWVtYmVyb2YgV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKi9cbiAgICBXb3JrQmVuY2hJdGVtSW5mby5wcm90b3R5cGUuY29pbkluY29tZSA9ICR1dGlsLkxvbmcgPyAkdXRpbC5Mb25nLmZyb21CaXRzKDAsMCxmYWxzZSkgOiAwO1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBXb3JrQmVuY2hJdGVtSW5mbyBpbnN0YW5jZSB1c2luZyB0aGUgc3BlY2lmaWVkIHByb3BlcnRpZXMuXG4gICAgICogQGZ1bmN0aW9uIGNyZWF0ZVxuICAgICAqIEBtZW1iZXJvZiBXb3JrQmVuY2hJdGVtSW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lXb3JrQmVuY2hJdGVtSW5mbz19IFtwcm9wZXJ0aWVzXSBQcm9wZXJ0aWVzIHRvIHNldFxuICAgICAqIEByZXR1cm5zIHtXb3JrQmVuY2hJdGVtSW5mb30gV29ya0JlbmNoSXRlbUluZm8gaW5zdGFuY2VcbiAgICAgKi9cbiAgICBXb3JrQmVuY2hJdGVtSW5mby5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUocHJvcGVydGllcykge1xuICAgICAgICByZXR1cm4gbmV3IFdvcmtCZW5jaEl0ZW1JbmZvKHByb3BlcnRpZXMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFbmNvZGVzIHRoZSBzcGVjaWZpZWQgV29ya0JlbmNoSXRlbUluZm8gbWVzc2FnZS4gRG9lcyBub3QgaW1wbGljaXRseSB7QGxpbmsgV29ya0JlbmNoSXRlbUluZm8udmVyaWZ5fHZlcmlmeX0gbWVzc2FnZXMuXG4gICAgICogQGZ1bmN0aW9uIGVuY29kZVxuICAgICAqIEBtZW1iZXJvZiBXb3JrQmVuY2hJdGVtSW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge0lXb3JrQmVuY2hJdGVtSW5mb30gbWVzc2FnZSBXb3JrQmVuY2hJdGVtSW5mbyBtZXNzYWdlIG9yIHBsYWluIG9iamVjdCB0byBlbmNvZGVcbiAgICAgKiBAcGFyYW0geyRwcm90b2J1Zi5Xcml0ZXJ9IFt3cml0ZXJdIFdyaXRlciB0byBlbmNvZGUgdG9cbiAgICAgKiBAcmV0dXJucyB7JHByb3RvYnVmLldyaXRlcn0gV3JpdGVyXG4gICAgICovXG4gICAgV29ya0JlbmNoSXRlbUluZm8uZW5jb2RlID0gZnVuY3Rpb24gZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikge1xuICAgICAgICBpZiAoIXdyaXRlcilcbiAgICAgICAgICAgIHdyaXRlciA9ICRXcml0ZXIuY3JlYXRlKCk7XG4gICAgICAgIGlmIChtZXNzYWdlLmxldmVsICE9IG51bGwgJiYgT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobWVzc2FnZSwgXCJsZXZlbFwiKSlcbiAgICAgICAgICAgIHdyaXRlci51aW50MzIoLyogaWQgMSwgd2lyZVR5cGUgMCA9Ki84KS5pbnQzMihtZXNzYWdlLmxldmVsKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UuY29pbkNvc3QgIT0gbnVsbCAmJiBPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtZXNzYWdlLCBcImNvaW5Db3N0XCIpKVxuICAgICAgICAgICAgd3JpdGVyLnVpbnQzMigvKiBpZCAyLCB3aXJlVHlwZSAwID0qLzE2KS5pbnQ2NChtZXNzYWdlLmNvaW5Db3N0KTtcbiAgICAgICAgaWYgKG1lc3NhZ2UuY29pbkluY29tZSAhPSBudWxsICYmIE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1lc3NhZ2UsIFwiY29pbkluY29tZVwiKSlcbiAgICAgICAgICAgIHdyaXRlci51aW50MzIoLyogaWQgMywgd2lyZVR5cGUgMCA9Ki8yNCkuaW50NjQobWVzc2FnZS5jb2luSW5jb21lKTtcbiAgICAgICAgcmV0dXJuIHdyaXRlcjtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRW5jb2RlcyB0aGUgc3BlY2lmaWVkIFdvcmtCZW5jaEl0ZW1JbmZvIG1lc3NhZ2UsIGxlbmd0aCBkZWxpbWl0ZWQuIERvZXMgbm90IGltcGxpY2l0bHkge0BsaW5rIFdvcmtCZW5jaEl0ZW1JbmZvLnZlcmlmeXx2ZXJpZnl9IG1lc3NhZ2VzLlxuICAgICAqIEBmdW5jdGlvbiBlbmNvZGVEZWxpbWl0ZWRcbiAgICAgKiBAbWVtYmVyb2YgV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtJV29ya0JlbmNoSXRlbUluZm99IG1lc3NhZ2UgV29ya0JlbmNoSXRlbUluZm8gbWVzc2FnZSBvciBwbGFpbiBvYmplY3QgdG8gZW5jb2RlXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuV3JpdGVyfSBbd3JpdGVyXSBXcml0ZXIgdG8gZW5jb2RlIHRvXG4gICAgICogQHJldHVybnMgeyRwcm90b2J1Zi5Xcml0ZXJ9IFdyaXRlclxuICAgICAqL1xuICAgIFdvcmtCZW5jaEl0ZW1JbmZvLmVuY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGVuY29kZURlbGltaXRlZChtZXNzYWdlLCB3cml0ZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW5jb2RlKG1lc3NhZ2UsIHdyaXRlcikubGRlbGltKCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIERlY29kZXMgYSBXb3JrQmVuY2hJdGVtSW5mbyBtZXNzYWdlIGZyb20gdGhlIHNwZWNpZmllZCByZWFkZXIgb3IgYnVmZmVyLlxuICAgICAqIEBmdW5jdGlvbiBkZWNvZGVcbiAgICAgKiBAbWVtYmVyb2YgV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuUmVhZGVyfFVpbnQ4QXJyYXl9IHJlYWRlciBSZWFkZXIgb3IgYnVmZmVyIHRvIGRlY29kZSBmcm9tXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IFtsZW5ndGhdIE1lc3NhZ2UgbGVuZ3RoIGlmIGtub3duIGJlZm9yZWhhbmRcbiAgICAgKiBAcmV0dXJucyB7V29ya0JlbmNoSXRlbUluZm99IFdvcmtCZW5jaEl0ZW1JbmZvXG4gICAgICogQHRocm93cyB7RXJyb3J9IElmIHRoZSBwYXlsb2FkIGlzIG5vdCBhIHJlYWRlciBvciB2YWxpZCBidWZmZXJcbiAgICAgKiBAdGhyb3dzIHskcHJvdG9idWYudXRpbC5Qcm90b2NvbEVycm9yfSBJZiByZXF1aXJlZCBmaWVsZHMgYXJlIG1pc3NpbmdcbiAgICAgKi9cbiAgICBXb3JrQmVuY2hJdGVtSW5mby5kZWNvZGUgPSBmdW5jdGlvbiBkZWNvZGUocmVhZGVyLCBsZW5ndGgpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSAkUmVhZGVyLmNyZWF0ZShyZWFkZXIpO1xuICAgICAgICB2YXIgZW5kID0gbGVuZ3RoID09PSB1bmRlZmluZWQgPyByZWFkZXIubGVuIDogcmVhZGVyLnBvcyArIGxlbmd0aCwgbWVzc2FnZSA9IG5ldyAkcm9vdC5Xb3JrQmVuY2hJdGVtSW5mbygpO1xuICAgICAgICB3aGlsZSAocmVhZGVyLnBvcyA8IGVuZCkge1xuICAgICAgICAgICAgdmFyIHRhZyA9IHJlYWRlci51aW50MzIoKTtcbiAgICAgICAgICAgIHN3aXRjaCAodGFnID4+PiAzKSB7XG4gICAgICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICAgICAgbWVzc2FnZS5sZXZlbCA9IHJlYWRlci5pbnQzMigpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UuY29pbkNvc3QgPSByZWFkZXIuaW50NjQoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgMzpcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW5JbmNvbWUgPSByZWFkZXIuaW50NjQoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmVhZGVyLnNraXBUeXBlKHRhZyAmIDcpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZWNvZGVzIGEgV29ya0JlbmNoSXRlbUluZm8gbWVzc2FnZSBmcm9tIHRoZSBzcGVjaWZpZWQgcmVhZGVyIG9yIGJ1ZmZlciwgbGVuZ3RoIGRlbGltaXRlZC5cbiAgICAgKiBAZnVuY3Rpb24gZGVjb2RlRGVsaW1pdGVkXG4gICAgICogQG1lbWJlcm9mIFdvcmtCZW5jaEl0ZW1JbmZvXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBwYXJhbSB7JHByb3RvYnVmLlJlYWRlcnxVaW50OEFycmF5fSByZWFkZXIgUmVhZGVyIG9yIGJ1ZmZlciB0byBkZWNvZGUgZnJvbVxuICAgICAqIEByZXR1cm5zIHtXb3JrQmVuY2hJdGVtSW5mb30gV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKiBAdGhyb3dzIHtFcnJvcn0gSWYgdGhlIHBheWxvYWQgaXMgbm90IGEgcmVhZGVyIG9yIHZhbGlkIGJ1ZmZlclxuICAgICAqIEB0aHJvd3MgeyRwcm90b2J1Zi51dGlsLlByb3RvY29sRXJyb3J9IElmIHJlcXVpcmVkIGZpZWxkcyBhcmUgbWlzc2luZ1xuICAgICAqL1xuICAgIFdvcmtCZW5jaEl0ZW1JbmZvLmRlY29kZURlbGltaXRlZCA9IGZ1bmN0aW9uIGRlY29kZURlbGltaXRlZChyZWFkZXIpIHtcbiAgICAgICAgaWYgKCEocmVhZGVyIGluc3RhbmNlb2YgJFJlYWRlcikpXG4gICAgICAgICAgICByZWFkZXIgPSBuZXcgJFJlYWRlcihyZWFkZXIpO1xuICAgICAgICByZXR1cm4gdGhpcy5kZWNvZGUocmVhZGVyLCByZWFkZXIudWludDMyKCkpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBWZXJpZmllcyBhIFdvcmtCZW5jaEl0ZW1JbmZvIG1lc3NhZ2UuXG4gICAgICogQGZ1bmN0aW9uIHZlcmlmeVxuICAgICAqIEBtZW1iZXJvZiBXb3JrQmVuY2hJdGVtSW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge09iamVjdC48c3RyaW5nLCo+fSBtZXNzYWdlIFBsYWluIG9iamVjdCB0byB2ZXJpZnlcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfG51bGx9IGBudWxsYCBpZiB2YWxpZCwgb3RoZXJ3aXNlIHRoZSByZWFzb24gd2h5IGl0IGlzIG5vdFxuICAgICAqL1xuICAgIFdvcmtCZW5jaEl0ZW1JbmZvLnZlcmlmeSA9IGZ1bmN0aW9uIHZlcmlmeShtZXNzYWdlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZSAhPT0gXCJvYmplY3RcIiB8fCBtZXNzYWdlID09PSBudWxsKVxuICAgICAgICAgICAgcmV0dXJuIFwib2JqZWN0IGV4cGVjdGVkXCI7XG4gICAgICAgIGlmIChtZXNzYWdlLmxldmVsICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImxldmVsXCIpKVxuICAgICAgICAgICAgaWYgKCEkdXRpbC5pc0ludGVnZXIobWVzc2FnZS5sZXZlbCkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwibGV2ZWw6IGludGVnZXIgZXhwZWN0ZWRcIjtcbiAgICAgICAgaWYgKG1lc3NhZ2UuY29pbkNvc3QgIT0gbnVsbCAmJiBtZXNzYWdlLmhhc093blByb3BlcnR5KFwiY29pbkNvc3RcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLmNvaW5Db3N0KSAmJiAhKG1lc3NhZ2UuY29pbkNvc3QgJiYgJHV0aWwuaXNJbnRlZ2VyKG1lc3NhZ2UuY29pbkNvc3QubG93KSAmJiAkdXRpbC5pc0ludGVnZXIobWVzc2FnZS5jb2luQ29zdC5oaWdoKSkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiY29pbkNvc3Q6IGludGVnZXJ8TG9uZyBleHBlY3RlZFwiO1xuICAgICAgICBpZiAobWVzc2FnZS5jb2luSW5jb21lICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImNvaW5JbmNvbWVcIikpXG4gICAgICAgICAgICBpZiAoISR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLmNvaW5JbmNvbWUpICYmICEobWVzc2FnZS5jb2luSW5jb21lICYmICR1dGlsLmlzSW50ZWdlcihtZXNzYWdlLmNvaW5JbmNvbWUubG93KSAmJiAkdXRpbC5pc0ludGVnZXIobWVzc2FnZS5jb2luSW5jb21lLmhpZ2gpKSlcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJjb2luSW5jb21lOiBpbnRlZ2VyfExvbmcgZXhwZWN0ZWRcIjtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBXb3JrQmVuY2hJdGVtSW5mbyBtZXNzYWdlIGZyb20gYSBwbGFpbiBvYmplY3QuIEFsc28gY29udmVydHMgdmFsdWVzIHRvIHRoZWlyIHJlc3BlY3RpdmUgaW50ZXJuYWwgdHlwZXMuXG4gICAgICogQGZ1bmN0aW9uIGZyb21PYmplY3RcbiAgICAgKiBAbWVtYmVyb2YgV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKiBAc3RhdGljXG4gICAgICogQHBhcmFtIHtPYmplY3QuPHN0cmluZywqPn0gb2JqZWN0IFBsYWluIG9iamVjdFxuICAgICAqIEByZXR1cm5zIHtXb3JrQmVuY2hJdGVtSW5mb30gV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKi9cbiAgICBXb3JrQmVuY2hJdGVtSW5mby5mcm9tT2JqZWN0ID0gZnVuY3Rpb24gZnJvbU9iamVjdChvYmplY3QpIHtcbiAgICAgICAgaWYgKG9iamVjdCBpbnN0YW5jZW9mICRyb290LldvcmtCZW5jaEl0ZW1JbmZvKVxuICAgICAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICAgICAgdmFyIG1lc3NhZ2UgPSBuZXcgJHJvb3QuV29ya0JlbmNoSXRlbUluZm8oKTtcbiAgICAgICAgaWYgKG9iamVjdC5sZXZlbCAhPSBudWxsKVxuICAgICAgICAgICAgbWVzc2FnZS5sZXZlbCA9IG9iamVjdC5sZXZlbCB8IDA7XG4gICAgICAgIGlmIChvYmplY3QuY29pbkNvc3QgIT0gbnVsbClcbiAgICAgICAgICAgIGlmICgkdXRpbC5Mb25nKVxuICAgICAgICAgICAgICAgIChtZXNzYWdlLmNvaW5Db3N0ID0gJHV0aWwuTG9uZy5mcm9tVmFsdWUob2JqZWN0LmNvaW5Db3N0KSkudW5zaWduZWQgPSBmYWxzZTtcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QuY29pbkNvc3QgPT09IFwic3RyaW5nXCIpXG4gICAgICAgICAgICAgICAgbWVzc2FnZS5jb2luQ29zdCA9IHBhcnNlSW50KG9iamVjdC5jb2luQ29zdCwgMTApO1xuICAgICAgICAgICAgZWxzZSBpZiAodHlwZW9mIG9iamVjdC5jb2luQ29zdCA9PT0gXCJudW1iZXJcIilcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW5Db3N0ID0gb2JqZWN0LmNvaW5Db3N0O1xuICAgICAgICAgICAgZWxzZSBpZiAodHlwZW9mIG9iamVjdC5jb2luQ29zdCA9PT0gXCJvYmplY3RcIilcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW5Db3N0ID0gbmV3ICR1dGlsLkxvbmdCaXRzKG9iamVjdC5jb2luQ29zdC5sb3cgPj4+IDAsIG9iamVjdC5jb2luQ29zdC5oaWdoID4+PiAwKS50b051bWJlcigpO1xuICAgICAgICBpZiAob2JqZWN0LmNvaW5JbmNvbWUgIT0gbnVsbClcbiAgICAgICAgICAgIGlmICgkdXRpbC5Mb25nKVxuICAgICAgICAgICAgICAgIChtZXNzYWdlLmNvaW5JbmNvbWUgPSAkdXRpbC5Mb25nLmZyb21WYWx1ZShvYmplY3QuY29pbkluY29tZSkpLnVuc2lnbmVkID0gZmFsc2U7XG4gICAgICAgICAgICBlbHNlIGlmICh0eXBlb2Ygb2JqZWN0LmNvaW5JbmNvbWUgPT09IFwic3RyaW5nXCIpXG4gICAgICAgICAgICAgICAgbWVzc2FnZS5jb2luSW5jb21lID0gcGFyc2VJbnQob2JqZWN0LmNvaW5JbmNvbWUsIDEwKTtcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QuY29pbkluY29tZSA9PT0gXCJudW1iZXJcIilcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW5JbmNvbWUgPSBvYmplY3QuY29pbkluY29tZTtcbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvYmplY3QuY29pbkluY29tZSA9PT0gXCJvYmplY3RcIilcbiAgICAgICAgICAgICAgICBtZXNzYWdlLmNvaW5JbmNvbWUgPSBuZXcgJHV0aWwuTG9uZ0JpdHMob2JqZWN0LmNvaW5JbmNvbWUubG93ID4+PiAwLCBvYmplY3QuY29pbkluY29tZS5oaWdoID4+PiAwKS50b051bWJlcigpO1xuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIHBsYWluIG9iamVjdCBmcm9tIGEgV29ya0JlbmNoSXRlbUluZm8gbWVzc2FnZS4gQWxzbyBjb252ZXJ0cyB2YWx1ZXMgdG8gb3RoZXIgdHlwZXMgaWYgc3BlY2lmaWVkLlxuICAgICAqIEBmdW5jdGlvbiB0b09iamVjdFxuICAgICAqIEBtZW1iZXJvZiBXb3JrQmVuY2hJdGVtSW5mb1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcGFyYW0ge1dvcmtCZW5jaEl0ZW1JbmZvfSBtZXNzYWdlIFdvcmtCZW5jaEl0ZW1JbmZvXG4gICAgICogQHBhcmFtIHskcHJvdG9idWYuSUNvbnZlcnNpb25PcHRpb25zfSBbb3B0aW9uc10gQ29udmVyc2lvbiBvcHRpb25zXG4gICAgICogQHJldHVybnMge09iamVjdC48c3RyaW5nLCo+fSBQbGFpbiBvYmplY3RcbiAgICAgKi9cbiAgICBXb3JrQmVuY2hJdGVtSW5mby50b09iamVjdCA9IGZ1bmN0aW9uIHRvT2JqZWN0KG1lc3NhZ2UsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKVxuICAgICAgICAgICAgb3B0aW9ucyA9IHt9O1xuICAgICAgICB2YXIgb2JqZWN0ID0ge307XG4gICAgICAgIGlmIChvcHRpb25zLmRlZmF1bHRzKSB7XG4gICAgICAgICAgICBvYmplY3QubGV2ZWwgPSAwO1xuICAgICAgICAgICAgaWYgKCR1dGlsLkxvbmcpIHtcbiAgICAgICAgICAgICAgICB2YXIgbG9uZyA9IG5ldyAkdXRpbC5Mb25nKDAsIDAsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICBvYmplY3QuY29pbkNvc3QgPSBvcHRpb25zLmxvbmdzID09PSBTdHJpbmcgPyBsb25nLnRvU3RyaW5nKCkgOiBvcHRpb25zLmxvbmdzID09PSBOdW1iZXIgPyBsb25nLnRvTnVtYmVyKCkgOiBsb25nO1xuICAgICAgICAgICAgfSBlbHNlXG4gICAgICAgICAgICAgICAgb2JqZWN0LmNvaW5Db3N0ID0gb3B0aW9ucy5sb25ncyA9PT0gU3RyaW5nID8gXCIwXCIgOiAwO1xuICAgICAgICAgICAgaWYgKCR1dGlsLkxvbmcpIHtcbiAgICAgICAgICAgICAgICB2YXIgbG9uZyA9IG5ldyAkdXRpbC5Mb25nKDAsIDAsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICBvYmplY3QuY29pbkluY29tZSA9IG9wdGlvbnMubG9uZ3MgPT09IFN0cmluZyA/IGxvbmcudG9TdHJpbmcoKSA6IG9wdGlvbnMubG9uZ3MgPT09IE51bWJlciA/IGxvbmcudG9OdW1iZXIoKSA6IGxvbmc7XG4gICAgICAgICAgICB9IGVsc2VcbiAgICAgICAgICAgICAgICBvYmplY3QuY29pbkluY29tZSA9IG9wdGlvbnMubG9uZ3MgPT09IFN0cmluZyA/IFwiMFwiIDogMDtcbiAgICAgICAgfVxuICAgICAgICBpZiAobWVzc2FnZS5sZXZlbCAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJsZXZlbFwiKSlcbiAgICAgICAgICAgIG9iamVjdC5sZXZlbCA9IG1lc3NhZ2UubGV2ZWw7XG4gICAgICAgIGlmIChtZXNzYWdlLmNvaW5Db3N0ICE9IG51bGwgJiYgbWVzc2FnZS5oYXNPd25Qcm9wZXJ0eShcImNvaW5Db3N0XCIpKVxuICAgICAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlLmNvaW5Db3N0ID09PSBcIm51bWJlclwiKVxuICAgICAgICAgICAgICAgIG9iamVjdC5jb2luQ29zdCA9IG9wdGlvbnMubG9uZ3MgPT09IFN0cmluZyA/IFN0cmluZyhtZXNzYWdlLmNvaW5Db3N0KSA6IG1lc3NhZ2UuY29pbkNvc3Q7XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgb2JqZWN0LmNvaW5Db3N0ID0gb3B0aW9ucy5sb25ncyA9PT0gU3RyaW5nID8gJHV0aWwuTG9uZy5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChtZXNzYWdlLmNvaW5Db3N0KSA6IG9wdGlvbnMubG9uZ3MgPT09IE51bWJlciA/IG5ldyAkdXRpbC5Mb25nQml0cyhtZXNzYWdlLmNvaW5Db3N0LmxvdyA+Pj4gMCwgbWVzc2FnZS5jb2luQ29zdC5oaWdoID4+PiAwKS50b051bWJlcigpIDogbWVzc2FnZS5jb2luQ29zdDtcbiAgICAgICAgaWYgKG1lc3NhZ2UuY29pbkluY29tZSAhPSBudWxsICYmIG1lc3NhZ2UuaGFzT3duUHJvcGVydHkoXCJjb2luSW5jb21lXCIpKVxuICAgICAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlLmNvaW5JbmNvbWUgPT09IFwibnVtYmVyXCIpXG4gICAgICAgICAgICAgICAgb2JqZWN0LmNvaW5JbmNvbWUgPSBvcHRpb25zLmxvbmdzID09PSBTdHJpbmcgPyBTdHJpbmcobWVzc2FnZS5jb2luSW5jb21lKSA6IG1lc3NhZ2UuY29pbkluY29tZTtcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICBvYmplY3QuY29pbkluY29tZSA9IG9wdGlvbnMubG9uZ3MgPT09IFN0cmluZyA/ICR1dGlsLkxvbmcucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobWVzc2FnZS5jb2luSW5jb21lKSA6IG9wdGlvbnMubG9uZ3MgPT09IE51bWJlciA/IG5ldyAkdXRpbC5Mb25nQml0cyhtZXNzYWdlLmNvaW5JbmNvbWUubG93ID4+PiAwLCBtZXNzYWdlLmNvaW5JbmNvbWUuaGlnaCA+Pj4gMCkudG9OdW1iZXIoKSA6IG1lc3NhZ2UuY29pbkluY29tZTtcbiAgICAgICAgcmV0dXJuIG9iamVjdDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ29udmVydHMgdGhpcyBXb3JrQmVuY2hJdGVtSW5mbyB0byBKU09OLlxuICAgICAqIEBmdW5jdGlvbiB0b0pTT05cbiAgICAgKiBAbWVtYmVyb2YgV29ya0JlbmNoSXRlbUluZm9cbiAgICAgKiBAaW5zdGFuY2VcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0LjxzdHJpbmcsKj59IEpTT04gb2JqZWN0XG4gICAgICovXG4gICAgV29ya0JlbmNoSXRlbUluZm8ucHJvdG90eXBlLnRvSlNPTiA9IGZ1bmN0aW9uIHRvSlNPTigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29uc3RydWN0b3IudG9PYmplY3QodGhpcywgJHByb3RvYnVmLnV0aWwudG9KU09OT3B0aW9ucyk7XG4gICAgfTtcblxuICAgIHJldHVybiBXb3JrQmVuY2hJdGVtSW5mbztcbn0pKCk7XG5cbm1vZHVsZS5leHBvcnRzID0gJHJvb3Q7XG4iXX0=