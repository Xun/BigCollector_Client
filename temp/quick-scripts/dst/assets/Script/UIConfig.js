
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIConfig.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '64643IPUkZDGb5aHw/lReOt', 'UIConfig');
// Script/UIConfig.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UIConfig = /** @class */ (function () {
    function UIConfig() {
    }
    UIConfig.UIHome = {
        prefabUrl: "Forms/Screen/UIHome",
        type: "UIScreen"
    };
    UIConfig.UILogin = {
        prefabUrl: "Forms/Screen/UILogin",
        type: "UIScreen"
    };
    UIConfig.UIStart = {
        prefabUrl: "Forms/Screen/UIStart",
        type: "UIScreen"
    };
    UIConfig.UILoading = {
        prefabUrl: "Forms/Tips/UILoading",
        type: "UITips"
    };
    UIConfig.UIBag = {
        prefabUrl: "Forms/Windows/UIBag",
        type: "UIWindow"
    };
    UIConfig.UIBagAdd = {
        prefabUrl: "Forms/Windows/UIBagAdd",
        type: "UIWindow"
    };
    UIConfig.UIBuyTip = {
        prefabUrl: "Forms/Windows/UIBuyTip",
        type: "UIWindow"
    };
    UIConfig.UIColleCtionConversion = {
        prefabUrl: "Forms/Windows/UIColleCtionConversion",
        type: "UIWindow"
    };
    UIConfig.UICollection = {
        prefabUrl: "Forms/Windows/UICollection",
        type: "UIWindow"
    };
    UIConfig.UIDeal = {
        prefabUrl: "Forms/Windows/UIDeal",
        type: "UIWindow"
    };
    UIConfig.UIDebris = {
        prefabUrl: "Forms/Windows/UIDebris",
        type: "UIWindow"
    };
    UIConfig.UIDialog = {
        prefabUrl: "Forms/Windows/UIDialog",
        type: "UIWindow"
    };
    UIConfig.UIExtarCommitSure = {
        prefabUrl: "Forms/Windows/UIExtarCommitSure",
        type: "UIWindow"
    };
    UIConfig.UIFriend = {
        prefabUrl: "Forms/Windows/UIFriend",
        type: "UIWindow"
    };
    UIConfig.UIFriendGive = {
        prefabUrl: "Forms/Windows/UIFriendGive",
        type: "UIWindow"
    };
    UIConfig.UIFriendGiveDetail = {
        prefabUrl: "Forms/Windows/UIFriendGiveDetail",
        type: "UIWindow"
    };
    UIConfig.UIFriendMonerDetail = {
        prefabUrl: "Forms/Windows/UIFriendMonerDetail",
        type: "UIWindow"
    };
    UIConfig.UIFriendSee = {
        prefabUrl: "Forms/Windows/UIFriendSee",
        type: "UIWindow"
    };
    UIConfig.UIFriendSeeDetail = {
        prefabUrl: "Forms/Windows/UIFriendSeeDetail",
        type: "UIWindow"
    };
    UIConfig.UIFriendSerch = {
        prefabUrl: "Forms/Windows/UIFriendSerch",
        type: "UIWindow"
    };
    UIConfig.UIGetMoneyLog = {
        prefabUrl: "Forms/Windows/UIGetMoneyLog",
        type: "UIWindow"
    };
    UIConfig.UIGetMoneyRule = {
        prefabUrl: "Forms/Windows/UIGetMoneyRule",
        type: "UIWindow"
    };
    UIConfig.UIInviteGetInfo = {
        prefabUrl: "Forms/Windows/UIInviteGetInfo",
        type: "UIWindow"
    };
    UIConfig.UILottery = {
        prefabUrl: "Forms/Windows/UILottery",
        type: "UIWindow"
    };
    UIConfig.UILotteryGetTip = {
        prefabUrl: "Forms/Windows/UILotteryGetTip",
        type: "UIWindow"
    };
    UIConfig.UIMy = {
        prefabUrl: "Forms/Windows/UIMy",
        type: "UIWindow"
    };
    UIConfig.UIMyExtar = {
        prefabUrl: "Forms/Windows/UIMyExtar",
        type: "UIWindow"
    };
    UIConfig.UIMyFriend = {
        prefabUrl: "Forms/Windows/UIMyFriend",
        type: "UIWindow"
    };
    UIConfig.UIMyWallet = {
        prefabUrl: "Forms/Windows/UIMyWallet",
        type: "UIWindow"
    };
    UIConfig.UIOline = {
        prefabUrl: "Forms/Windows/UIOline",
        type: "UIWindow"
    };
    UIConfig.UIRank = {
        prefabUrl: "Forms/Windows/UIRank",
        type: "UIWindow"
    };
    UIConfig.UIRecoverTip = {
        prefabUrl: "Forms/Windows/UIRecoverTip",
        type: "UIWindow"
    };
    UIConfig.UIRotateAndFlip = {
        prefabUrl: "Forms/Windows/UIRotateAndFlip",
        type: "UIWindow"
    };
    UIConfig.UIScoketReconnent = {
        prefabUrl: "Forms/Windows/UIScoketReconnent",
        type: "UIWindow"
    };
    UIConfig.UISetSocial = {
        prefabUrl: "Forms/Windows/UISetSocial",
        type: "UIWindow"
    };
    UIConfig.UISetting = {
        prefabUrl: "Forms/Windows/UISetting",
        type: "UIWindow"
    };
    UIConfig.UIShare = {
        prefabUrl: "Forms/Windows/UIShare",
        type: "UIWindow"
    };
    UIConfig.UIShiMing = {
        prefabUrl: "Forms/Windows/UIShiMing",
        type: "UIWindow"
    };
    UIConfig.UIShop = {
        prefabUrl: "Forms/Windows/UIShop",
        type: "UIWindow"
    };
    UIConfig.UISignIn = {
        prefabUrl: "Forms/Windows/UISignIn",
        type: "UIWindow"
    };
    UIConfig.UITask = {
        prefabUrl: "Forms/Windows/UITask",
        type: "UIWindow"
    };
    UIConfig.UIUnlockNewLevel = {
        prefabUrl: "Forms/Windows/UIUnlockNewLevel",
        type: "UIWindow"
    };
    UIConfig.UIDealDetail = {
        prefabUrl: "Forms/Windows/UIDealDetail",
        type: "UIWindow"
    };
    UIConfig.UIDealMyGet = {
        prefabUrl: "Forms/Windows/UIDealMyGet",
        type: "UIWindow"
    };
    UIConfig.UIDealAdd = {
        prefabUrl: "Forms/Windows/UIDealAdd",
        type: "UIWindow"
    };
    UIConfig.UIDealMySell = {
        prefabUrl: "Forms/Windows/UIDealMySell",
        type: "UIWindow"
    };
    UIConfig.UIDealSell = {
        prefabUrl: "Forms/Windows/UIDealSell",
        type: "UIWindow"
    };
    UIConfig.UIDealTip = {
        prefabUrl: "Forms/Windows/UIDealTip",
        type: "UIWindow"
    };
    return UIConfig;
}());
exports.default = UIConfig;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlDb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtJQUFBO0lBa01JLENBQUM7SUFqTU0sZUFBTSxHQUFHO1FBQ1osU0FBUyxFQUFFLHFCQUFxQjtRQUNoQyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sZ0JBQU8sR0FBRztRQUNiLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGdCQUFPLEdBQUc7UUFDYixTQUFTLEVBQUUsc0JBQXNCO1FBQ2pDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxrQkFBUyxHQUFHO1FBQ2YsU0FBUyxFQUFFLHNCQUFzQjtRQUNqQyxJQUFJLEVBQUUsUUFBUTtLQUNqQixDQUFBO0lBQ00sY0FBSyxHQUFHO1FBQ1gsU0FBUyxFQUFFLHFCQUFxQjtRQUNoQyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00saUJBQVEsR0FBRztRQUNkLFNBQVMsRUFBRSx3QkFBd0I7UUFDbkMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGlCQUFRLEdBQUc7UUFDZCxTQUFTLEVBQUUsd0JBQXdCO1FBQ25DLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSwrQkFBc0IsR0FBRztRQUM1QixTQUFTLEVBQUUsc0NBQXNDO1FBQ2pELElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxxQkFBWSxHQUFHO1FBQ2xCLFNBQVMsRUFBRSw0QkFBNEI7UUFDdkMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGVBQU0sR0FBRztRQUNaLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGlCQUFRLEdBQUc7UUFDZCxTQUFTLEVBQUUsd0JBQXdCO1FBQ25DLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxpQkFBUSxHQUFHO1FBQ2QsU0FBUyxFQUFFLHdCQUF3QjtRQUNuQyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sMEJBQWlCLEdBQUc7UUFDdkIsU0FBUyxFQUFFLGlDQUFpQztRQUM1QyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00saUJBQVEsR0FBRztRQUNkLFNBQVMsRUFBRSx3QkFBd0I7UUFDbkMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLHFCQUFZLEdBQUc7UUFDbEIsU0FBUyxFQUFFLDRCQUE0QjtRQUN2QyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sMkJBQWtCLEdBQUc7UUFDeEIsU0FBUyxFQUFFLGtDQUFrQztRQUM3QyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sNEJBQW1CLEdBQUc7UUFDekIsU0FBUyxFQUFFLG1DQUFtQztRQUM5QyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sb0JBQVcsR0FBRztRQUNqQixTQUFTLEVBQUUsMkJBQTJCO1FBQ3RDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSwwQkFBaUIsR0FBRztRQUN2QixTQUFTLEVBQUUsaUNBQWlDO1FBQzVDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxzQkFBYSxHQUFHO1FBQ25CLFNBQVMsRUFBRSw2QkFBNkI7UUFDeEMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLHNCQUFhLEdBQUc7UUFDbkIsU0FBUyxFQUFFLDZCQUE2QjtRQUN4QyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sdUJBQWMsR0FBRztRQUNwQixTQUFTLEVBQUUsOEJBQThCO1FBQ3pDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSx3QkFBZSxHQUFHO1FBQ3JCLFNBQVMsRUFBRSwrQkFBK0I7UUFDMUMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGtCQUFTLEdBQUc7UUFDZixTQUFTLEVBQUUseUJBQXlCO1FBQ3BDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSx3QkFBZSxHQUFHO1FBQ3JCLFNBQVMsRUFBRSwrQkFBK0I7UUFDMUMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGFBQUksR0FBRztRQUNWLFNBQVMsRUFBRSxvQkFBb0I7UUFDL0IsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGtCQUFTLEdBQUc7UUFDZixTQUFTLEVBQUUseUJBQXlCO1FBQ3BDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxtQkFBVSxHQUFHO1FBQ2hCLFNBQVMsRUFBRSwwQkFBMEI7UUFDckMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLG1CQUFVLEdBQUc7UUFDaEIsU0FBUyxFQUFFLDBCQUEwQjtRQUNyQyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sZ0JBQU8sR0FBRztRQUNiLFNBQVMsRUFBRSx1QkFBdUI7UUFDbEMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGVBQU0sR0FBRztRQUNaLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLHFCQUFZLEdBQUc7UUFDbEIsU0FBUyxFQUFFLDRCQUE0QjtRQUN2QyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sd0JBQWUsR0FBRztRQUNyQixTQUFTLEVBQUUsK0JBQStCO1FBQzFDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSwwQkFBaUIsR0FBRztRQUN2QixTQUFTLEVBQUUsaUNBQWlDO1FBQzVDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxvQkFBVyxHQUFHO1FBQ2pCLFNBQVMsRUFBRSwyQkFBMkI7UUFDdEMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGtCQUFTLEdBQUc7UUFDZixTQUFTLEVBQUUseUJBQXlCO1FBQ3BDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxnQkFBTyxHQUFHO1FBQ2IsU0FBUyxFQUFFLHVCQUF1QjtRQUNsQyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sa0JBQVMsR0FBRztRQUNmLFNBQVMsRUFBRSx5QkFBeUI7UUFDcEMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGVBQU0sR0FBRztRQUNaLFNBQVMsRUFBRSxzQkFBc0I7UUFDakMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLGlCQUFRLEdBQUc7UUFDZCxTQUFTLEVBQUUsd0JBQXdCO1FBQ25DLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxlQUFNLEdBQUc7UUFDWixTQUFTLEVBQUUsc0JBQXNCO1FBQ2pDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSx5QkFBZ0IsR0FBRztRQUN0QixTQUFTLEVBQUUsZ0NBQWdDO1FBQzNDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxxQkFBWSxHQUFHO1FBQ2xCLFNBQVMsRUFBRSw0QkFBNEI7UUFDdkMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLG9CQUFXLEdBQUc7UUFDakIsU0FBUyxFQUFFLDJCQUEyQjtRQUN0QyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sa0JBQVMsR0FBRztRQUNmLFNBQVMsRUFBRSx5QkFBeUI7UUFDcEMsSUFBSSxFQUFFLFVBQVU7S0FDbkIsQ0FBQTtJQUNNLHFCQUFZLEdBQUc7UUFDbEIsU0FBUyxFQUFFLDRCQUE0QjtRQUN2QyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBQ00sbUJBQVUsR0FBRztRQUNoQixTQUFTLEVBQUUsMEJBQTBCO1FBQ3JDLElBQUksRUFBRSxVQUFVO0tBQ25CLENBQUE7SUFDTSxrQkFBUyxHQUFHO1FBQ2YsU0FBUyxFQUFFLHlCQUF5QjtRQUNwQyxJQUFJLEVBQUUsVUFBVTtLQUNuQixDQUFBO0lBRUQsZUFBQztDQWxNTCxBQWtNSyxJQUFBO2tCQWxNZ0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJQ29uZmlnIHtcbiAgICBzdGF0aWMgVUlIb21lID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvU2NyZWVuL1VJSG9tZVwiLFxuICAgICAgICB0eXBlOiBcIlVJU2NyZWVuXCJcbiAgICB9XG4gICAgc3RhdGljIFVJTG9naW4gPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9TY3JlZW4vVUlMb2dpblwiLFxuICAgICAgICB0eXBlOiBcIlVJU2NyZWVuXCJcbiAgICB9XG4gICAgc3RhdGljIFVJU3RhcnQgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9TY3JlZW4vVUlTdGFydFwiLFxuICAgICAgICB0eXBlOiBcIlVJU2NyZWVuXCJcbiAgICB9XG4gICAgc3RhdGljIFVJTG9hZGluZyA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1RpcHMvVUlMb2FkaW5nXCIsXG4gICAgICAgIHR5cGU6IFwiVUlUaXBzXCJcbiAgICB9XG4gICAgc3RhdGljIFVJQmFnID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSUJhZ1wiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJQmFnQWRkID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSUJhZ0FkZFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJQnV5VGlwID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSUJ1eVRpcFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJQ29sbGVDdGlvbkNvbnZlcnNpb24gPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJQ29sbGVDdGlvbkNvbnZlcnNpb25cIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSUNvbGxlY3Rpb24gPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJQ29sbGVjdGlvblwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJRGVhbCA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlEZWFsXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlEZWJyaXMgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJRGVicmlzXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlEaWFsb2cgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJRGlhbG9nXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlFeHRhckNvbW1pdFN1cmUgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJRXh0YXJDb21taXRTdXJlXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlGcmllbmQgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJRnJpZW5kXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlGcmllbmRHaXZlID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSUZyaWVuZEdpdmVcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSUZyaWVuZEdpdmVEZXRhaWwgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJRnJpZW5kR2l2ZURldGFpbFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJRnJpZW5kTW9uZXJEZXRhaWwgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJRnJpZW5kTW9uZXJEZXRhaWxcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSUZyaWVuZFNlZSA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlGcmllbmRTZWVcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSUZyaWVuZFNlZURldGFpbCA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlGcmllbmRTZWVEZXRhaWxcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSUZyaWVuZFNlcmNoID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSUZyaWVuZFNlcmNoXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlHZXRNb25leUxvZyA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlHZXRNb25leUxvZ1wiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJR2V0TW9uZXlSdWxlID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSUdldE1vbmV5UnVsZVwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJSW52aXRlR2V0SW5mbyA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlJbnZpdGVHZXRJbmZvXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlMb3R0ZXJ5ID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSUxvdHRlcnlcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSUxvdHRlcnlHZXRUaXAgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJTG90dGVyeUdldFRpcFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJTXkgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJTXlcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSU15RXh0YXIgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJTXlFeHRhclwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJTXlGcmllbmQgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJTXlGcmllbmRcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSU15V2FsbGV0ID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSU15V2FsbGV0XCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlPbGluZSA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlPbGluZVwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJUmFuayA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlSYW5rXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlSZWNvdmVyVGlwID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSVJlY292ZXJUaXBcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSVJvdGF0ZUFuZEZsaXAgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJUm90YXRlQW5kRmxpcFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJU2Nva2V0UmVjb25uZW50ID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSVNjb2tldFJlY29ubmVudFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJU2V0U29jaWFsID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSVNldFNvY2lhbFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJU2V0dGluZyA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlTZXR0aW5nXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlTaGFyZSA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlTaGFyZVwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJU2hpTWluZyA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlTaGlNaW5nXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlTaG9wID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSVNob3BcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSVNpZ25JbiA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlTaWduSW5cIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSVRhc2sgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJVGFza1wiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJVW5sb2NrTmV3TGV2ZWwgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJVW5sb2NrTmV3TGV2ZWxcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSURlYWxEZXRhaWwgPSB7XG4gICAgICAgIHByZWZhYlVybDogXCJGb3Jtcy9XaW5kb3dzL1VJRGVhbERldGFpbFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJRGVhbE15R2V0ID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSURlYWxNeUdldFwiLFxuICAgICAgICB0eXBlOiBcIlVJV2luZG93XCJcbiAgICB9XG4gICAgc3RhdGljIFVJRGVhbEFkZCA9IHtcbiAgICAgICAgcHJlZmFiVXJsOiBcIkZvcm1zL1dpbmRvd3MvVUlEZWFsQWRkXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlEZWFsTXlTZWxsID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSURlYWxNeVNlbGxcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIHN0YXRpYyBVSURlYWxTZWxsID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSURlYWxTZWxsXCIsXG4gICAgICAgIHR5cGU6IFwiVUlXaW5kb3dcIlxuICAgIH1cbiAgICBzdGF0aWMgVUlEZWFsVGlwID0ge1xuICAgICAgICBwcmVmYWJVcmw6IFwiRm9ybXMvV2luZG93cy9VSURlYWxUaXBcIixcbiAgICAgICAgdHlwZTogXCJVSVdpbmRvd1wiXG4gICAgfVxuICAgIFxuICAgIH0iXX0=