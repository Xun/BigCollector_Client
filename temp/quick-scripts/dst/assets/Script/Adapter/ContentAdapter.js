
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Adapter/ContentAdapter.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7463e2/Hf1Hjq2+JgTMTs7t', 'ContentAdapter');
// Script/Adapter/ContentAdapter.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *                  ___====-_  _-====___
 *            _--^^^#####//      \\#####^^^--_
 *         _-^##########// (    ) \\##########^-_
 *        -############//  |\^^/|  \\############-
 *      _/############//   (@::@)   \\############\_
 *     /#############((     \\//     ))#############\
 *    -###############\\    (oo)    //###############-
 *   -#################\\  / VV \  //#################-
 *  -###################\\/      \//###################-
 * _#/|##########/\######(   /\   )######/\##########|\#_
 * |/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
 * `  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
 *    `   `  `      `   / | |  | | \   '      '  '   '
 *                     (  | |  | |  )
 *                    __\ | |  | | /__
 *                   (vvv(VVV)(VVV)vvv)
 *                        神兽保佑
 *                       代码无BUG!
 */
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
/**
 * @classdesc  游戏主内容节点自适应所有分辨率的脚本
 * @author caizhitao
 * @version 0.1.0
 * @since 2018-11-30
 * @description
 *
 * 用法：
 *      1. 将本组件挂载在节点上即可
 *
 * 适配原理：
 *      1. 将游戏主内容节点的宽高调整为画布的大小，以进行Size适配
 *
 * 注意：
 *      1. 挂载这个脚本的节点不能加入Widget组件，不然这个适配是没有效果的
 *      2. 目前只支持 SHOW_ALL 模式下的背景缩放适配，不支持其他模式的背景缩放
 *
 *  @example
    ```
    // e.g.
    // 代码中设置 SHOW_ALL 模式的参考代码
    cc.view.setDesignResolutionSize(720, 1280, cc.ResolutionPolicy.SHOW_ALL);

    // 或者 Canvas 组件中，同时勾选 Fit Width 和 Fit Height
    ```
 */
var ContentAdapter = /** @class */ (function (_super) {
    __extends(ContentAdapter, _super);
    function ContentAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ContentAdapter.prototype.onLoad = function () {
        // console.log("调整前");
        // console.log(`屏幕分辨率: ${cc.view.getCanvasSize().width} x ${cc.view.getCanvasSize().height}`);
        // console.log(`视图窗口可见区域分辨率: ${cc.view.getVisibleSize().width} x ${cc.view.getVisibleSize().height}`);
        // console.log(`视图中边框尺寸: ${cc.view.getFrameSize().width} x ${cc.view.getFrameSize().height}`);
        // console.log(`设备或浏览器像素比例: ${cc.view.getDevicePixelRatio()}`);
        // console.log(`节点宽高: ${this.node.width} x ${this.node.height}`);
        // 1. 先找到 SHOW_ALL 模式适配之后，本节点的实际宽高以及初始缩放值
        var srcScaleForShowAll = Math.min(cc.view.getCanvasSize().width / this.node.width, cc.view.getCanvasSize().height / this.node.height);
        var realWidth = this.node.width * srcScaleForShowAll;
        var realHeight = this.node.height * srcScaleForShowAll;
        // 2. 基于第一步的数据，再做节点宽高适配
        this.node.width = this.node.width * (cc.view.getCanvasSize().width / realWidth);
        this.node.height = this.node.height * (cc.view.getCanvasSize().height / realHeight);
        // console.log("调整后");
        // console.log(`屏幕分辨率: ${cc.view.getCanvasSize().width} x ${cc.view.getCanvasSize().height}`);
        // console.log(`视图窗口可见区域分辨率: ${cc.view.getVisibleSize().width} x ${cc.view.getVisibleSize().height}`);
        // console.log(`视图中边框尺寸: ${cc.view.getFrameSize().width} x ${cc.view.getFrameSize().height}`);
        // console.log(`设备或浏览器像素比例: ${cc.view.getDevicePixelRatio()}`);
        // console.log(`节点宽高: ${this.node.width} x ${this.node.height}`);
        // console.log(`安全区: ${JSON.stringify(cc.sys.getSafeAreaRect())} `);
    };
    ContentAdapter = __decorate([
        ccclass
    ], ContentAdapter);
    return ContentAdapter;
}(cc.Component));
exports.default = ContentAdapter;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQWRhcHRlci9Db250ZW50QWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQW1CRztBQUNHLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRTVDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBeUJHO0FBRUg7SUFBNEMsa0NBQVk7SUFBeEQ7O0lBNEJBLENBQUM7SUEzQkcsK0JBQU0sR0FBTjtRQUNJLHNCQUFzQjtRQUN0Qiw4RkFBOEY7UUFDOUYsc0dBQXNHO1FBQ3RHLDhGQUE4RjtRQUM5RiwrREFBK0Q7UUFDL0QsaUVBQWlFO1FBRWpFLHlDQUF5QztRQUN6QyxJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0SSxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxrQkFBa0IsQ0FBQztRQUNyRCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQztRQUV2RCx1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsQ0FBQztRQUNoRixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxDQUFDO1FBRXBGLHNCQUFzQjtRQUN0Qiw4RkFBOEY7UUFDOUYsc0dBQXNHO1FBQ3RHLDhGQUE4RjtRQUM5RiwrREFBK0Q7UUFDL0QsaUVBQWlFO1FBRWpFLG9FQUFvRTtJQUN4RSxDQUFDO0lBMUJnQixjQUFjO1FBRGxDLE9BQU87T0FDYSxjQUFjLENBNEJsQztJQUFELHFCQUFDO0NBNUJELEFBNEJDLENBNUIyQyxFQUFFLENBQUMsU0FBUyxHQTRCdkQ7a0JBNUJvQixjQUFjIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiAgICAgICAgICAgICAgICAgIF9fXz09PT0tXyAgXy09PT09X19fXG4gKiAgICAgICAgICAgIF8tLV5eXiMjIyMjLy8gICAgICBcXFxcIyMjIyNeXl4tLV9cbiAqICAgICAgICAgXy1eIyMjIyMjIyMjIy8vICggICAgKSBcXFxcIyMjIyMjIyMjI14tX1xuICogICAgICAgIC0jIyMjIyMjIyMjIyMvLyAgfFxcXl4vfCAgXFxcXCMjIyMjIyMjIyMjIy1cbiAqICAgICAgXy8jIyMjIyMjIyMjIyMvLyAgIChAOjpAKSAgIFxcXFwjIyMjIyMjIyMjIyNcXF9cbiAqICAgICAvIyMjIyMjIyMjIyMjIygoICAgICBcXFxcLy8gICAgICkpIyMjIyMjIyMjIyMjI1xcXG4gKiAgICAtIyMjIyMjIyMjIyMjIyMjXFxcXCAgICAob28pICAgIC8vIyMjIyMjIyMjIyMjIyMjLVxuICogICAtIyMjIyMjIyMjIyMjIyMjIyNcXFxcICAvIFZWIFxcICAvLyMjIyMjIyMjIyMjIyMjIyMjLVxuICogIC0jIyMjIyMjIyMjIyMjIyMjIyMjXFxcXC8gICAgICBcXC8vIyMjIyMjIyMjIyMjIyMjIyMjIy1cbiAqIF8jL3wjIyMjIyMjIyMjL1xcIyMjIyMjKCAgIC9cXCAgICkjIyMjIyMvXFwjIyMjIyMjIyMjfFxcI19cbiAqIHwvIHwjL1xcIy9cXCMvXFwvICBcXCMvXFwjI1xcICB8ICB8ICAvIyMvXFwjLyAgXFwvXFwjL1xcIy9cXCN8IFxcfFxuICogYCAgfC8gIFYgIFYgIGAgICBWICBcXCNcXHwgfCAgfCB8LyMvICBWICAgJyAgViAgViAgXFx8ICAnXG4gKiAgICBgICAgYCAgYCAgICAgIGAgICAvIHwgfCAgfCB8IFxcICAgJyAgICAgICcgICcgICAnXG4gKiAgICAgICAgICAgICAgICAgICAgICggIHwgfCAgfCB8ICApXG4gKiAgICAgICAgICAgICAgICAgICAgX19cXCB8IHwgIHwgfCAvX19cbiAqICAgICAgICAgICAgICAgICAgICh2dnYoVlZWKShWVlYpdnZ2KVxuICogICAgICAgICAgICAgICAgICAgICAgICDnpZ7lhb3kv53kvZFcbiAqICAgICAgICAgICAgICAgICAgICAgICDku6PnoIHml6BCVUchXG4gKi9cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbi8qKlxuICogQGNsYXNzZGVzYyAg5ri45oiP5Li75YaF5a656IqC54K56Ieq6YCC5bqU5omA5pyJ5YiG6L6o546H55qE6ISa5pysXG4gKiBAYXV0aG9yIGNhaXpoaXRhb1xuICogQHZlcnNpb24gMC4xLjBcbiAqIEBzaW5jZSAyMDE4LTExLTMwXG4gKiBAZGVzY3JpcHRpb25cbiAqXG4gKiDnlKjms5XvvJpcbiAqICAgICAgMS4g5bCG5pys57uE5Lu25oyC6L295Zyo6IqC54K55LiK5Y2z5Y+vXG4gKlxuICog6YCC6YWN5Y6f55CG77yaXG4gKiAgICAgIDEuIOWwhua4uOaIj+S4u+WGheWuueiKgueCueeahOWuvemrmOiwg+aVtOS4uueUu+W4g+eahOWkp+Wwj++8jOS7pei/m+ihjFNpemXpgILphY1cbiAqXG4gKiDms6jmhI/vvJpcbiAqICAgICAgMS4g5oyC6L296L+Z5Liq6ISa5pys55qE6IqC54K55LiN6IO95Yqg5YWlV2lkZ2V057uE5Lu277yM5LiN54S26L+Z5Liq6YCC6YWN5piv5rKh5pyJ5pWI5p6c55qEXG4gKiAgICAgIDIuIOebruWJjeWPquaUr+aMgSBTSE9XX0FMTCDmqKHlvI/kuIvnmoTog4zmma/nvKnmlL7pgILphY3vvIzkuI3mlK/mjIHlhbbku5bmqKHlvI/nmoTog4zmma/nvKnmlL5cbiAqXG4gKiAgQGV4YW1wbGVcbiAgICBgYGBcbiAgICAvLyBlLmcuXG4gICAgLy8g5Luj56CB5Lit6K6+572uIFNIT1dfQUxMIOaooeW8j+eahOWPguiAg+S7o+eggVxuICAgIGNjLnZpZXcuc2V0RGVzaWduUmVzb2x1dGlvblNpemUoNzIwLCAxMjgwLCBjYy5SZXNvbHV0aW9uUG9saWN5LlNIT1dfQUxMKTtcblxuICAgIC8vIOaIluiAhSBDYW52YXMg57uE5Lu25Lit77yM5ZCM5pe25Yu+6YCJIEZpdCBXaWR0aCDlkowgRml0IEhlaWdodCBcbiAgICBgYGBcbiAqL1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENvbnRlbnRBZGFwdGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICBvbkxvYWQoKSB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwi6LCD5pW05YmNXCIpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhg5bGP5bmV5YiG6L6o546HOiAke2NjLnZpZXcuZ2V0Q2FudmFzU2l6ZSgpLndpZHRofSB4ICR7Y2Mudmlldy5nZXRDYW52YXNTaXplKCkuaGVpZ2h0fWApO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhg6KeG5Zu+56qX5Y+j5Y+v6KeB5Yy65Z+f5YiG6L6o546HOiAke2NjLnZpZXcuZ2V0VmlzaWJsZVNpemUoKS53aWR0aH0geCAke2NjLnZpZXcuZ2V0VmlzaWJsZVNpemUoKS5oZWlnaHR9YCk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGDop4blm77kuK3ovrnmoYblsLrlr7g6ICR7Y2Mudmlldy5nZXRGcmFtZVNpemUoKS53aWR0aH0geCAke2NjLnZpZXcuZ2V0RnJhbWVTaXplKCkuaGVpZ2h0fWApO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhg6K6+5aSH5oiW5rWP6KeI5Zmo5YOP57Sg5q+U5L6LOiAke2NjLnZpZXcuZ2V0RGV2aWNlUGl4ZWxSYXRpbygpfWApO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhg6IqC54K55a696auYOiAke3RoaXMubm9kZS53aWR0aH0geCAke3RoaXMubm9kZS5oZWlnaHR9YCk7XG5cbiAgICAgICAgLy8gMS4g5YWI5om+5YiwIFNIT1dfQUxMIOaooeW8j+mAgumFjeS5i+WQju+8jOacrOiKgueCueeahOWunumZheWuvemrmOS7peWPiuWIneWni+e8qeaUvuWAvFxuICAgICAgICBsZXQgc3JjU2NhbGVGb3JTaG93QWxsID0gTWF0aC5taW4oY2Mudmlldy5nZXRDYW52YXNTaXplKCkud2lkdGggLyB0aGlzLm5vZGUud2lkdGgsIGNjLnZpZXcuZ2V0Q2FudmFzU2l6ZSgpLmhlaWdodCAvIHRoaXMubm9kZS5oZWlnaHQpO1xuICAgICAgICBsZXQgcmVhbFdpZHRoID0gdGhpcy5ub2RlLndpZHRoICogc3JjU2NhbGVGb3JTaG93QWxsO1xuICAgICAgICBsZXQgcmVhbEhlaWdodCA9IHRoaXMubm9kZS5oZWlnaHQgKiBzcmNTY2FsZUZvclNob3dBbGw7XG5cbiAgICAgICAgLy8gMi4g5Z+65LqO56ys5LiA5q2l55qE5pWw5o2u77yM5YaN5YGa6IqC54K55a696auY6YCC6YWNXG4gICAgICAgIHRoaXMubm9kZS53aWR0aCA9IHRoaXMubm9kZS53aWR0aCAqIChjYy52aWV3LmdldENhbnZhc1NpemUoKS53aWR0aCAvIHJlYWxXaWR0aCk7XG4gICAgICAgIHRoaXMubm9kZS5oZWlnaHQgPSB0aGlzLm5vZGUuaGVpZ2h0ICogKGNjLnZpZXcuZ2V0Q2FudmFzU2l6ZSgpLmhlaWdodCAvIHJlYWxIZWlnaHQpO1xuXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwi6LCD5pW05ZCOXCIpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhg5bGP5bmV5YiG6L6o546HOiAke2NjLnZpZXcuZ2V0Q2FudmFzU2l6ZSgpLndpZHRofSB4ICR7Y2Mudmlldy5nZXRDYW52YXNTaXplKCkuaGVpZ2h0fWApO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhg6KeG5Zu+56qX5Y+j5Y+v6KeB5Yy65Z+f5YiG6L6o546HOiAke2NjLnZpZXcuZ2V0VmlzaWJsZVNpemUoKS53aWR0aH0geCAke2NjLnZpZXcuZ2V0VmlzaWJsZVNpemUoKS5oZWlnaHR9YCk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGDop4blm77kuK3ovrnmoYblsLrlr7g6ICR7Y2Mudmlldy5nZXRGcmFtZVNpemUoKS53aWR0aH0geCAke2NjLnZpZXcuZ2V0RnJhbWVTaXplKCkuaGVpZ2h0fWApO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhg6K6+5aSH5oiW5rWP6KeI5Zmo5YOP57Sg5q+U5L6LOiAke2NjLnZpZXcuZ2V0RGV2aWNlUGl4ZWxSYXRpbygpfWApO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhg6IqC54K55a696auYOiAke3RoaXMubm9kZS53aWR0aH0geCAke3RoaXMubm9kZS5oZWlnaHR9YCk7XG5cbiAgICAgICAgLy8gY29uc29sZS5sb2coYOWuieWFqOWMujogJHtKU09OLnN0cmluZ2lmeShjYy5zeXMuZ2V0U2FmZUFyZWFSZWN0KCkpfSBgKTtcbiAgICB9XG5cbn1cbiJdfQ==