
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Bag/ItemBag.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2c89dxwm1lJSYlLwMI663N5', 'ItemBag');
// Script/UIScript/Bag/ItemBag.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemBag = /** @class */ (function (_super) {
    __extends(ItemBag, _super);
    function ItemBag() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item_name = null;
        _this.item_sp = null;
        return _this;
    }
    ItemBag.prototype.setData = function (item) {
        if (Number(item.itemID) > 1100) {
            var _chipName = CocosHelper_1.default.getShopNameByID(item.itemID.toString());
            this.item_name.string = _chipName + "+" + item.itemCount;
            CocosHelper_1.default.setShopInfo(_chipName, this.item_sp);
        }
        else {
            var _chipName = CocosHelper_1.default.getChipNameByID(item.itemID.toString());
            this.item_name.string = _chipName + "+" + item.itemCount;
            var _iconName = _chipName.replace("碎片", "");
            CocosHelper_1.default.setDealIcon(_iconName, this.item_sp);
        }
    };
    __decorate([
        property(cc.Label)
    ], ItemBag.prototype, "item_name", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemBag.prototype, "item_sp", void 0);
    ItemBag = __decorate([
        ccclass
    ], ItemBag);
    return ItemBag;
}(cc.Component));
exports.default = ItemBag;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvQmFnL0l0ZW1CYWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHbEYsdURBQWtEO0FBRTVDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXFDLDJCQUFZO0lBQWpEO1FBQUEscUVBdUJDO1FBcEJHLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFHM0IsYUFBTyxHQUFjLElBQUksQ0FBQzs7SUFpQjlCLENBQUM7SUFmRyx5QkFBTyxHQUFQLFVBQVEsSUFBaUI7UUFFckIsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksRUFBRTtZQUM1QixJQUFJLFNBQVMsR0FBRyxxQkFBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7WUFDcEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsU0FBUyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3pELHFCQUFXLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDcEQ7YUFBTTtZQUNILElBQUksU0FBUyxHQUFHLHFCQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNwRSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxTQUFTLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDekQsSUFBSSxTQUFTLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDNUMscUJBQVcsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNwRDtJQUdMLENBQUM7SUFuQkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs4Q0FDUTtJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzRDQUNNO0lBTlQsT0FBTztRQUQzQixPQUFPO09BQ2EsT0FBTyxDQXVCM0I7SUFBRCxjQUFDO0NBdkJELEFBdUJDLENBdkJvQyxFQUFFLENBQUMsU0FBUyxHQXVCaEQ7a0JBdkJvQixPQUFPIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCB7IERhdGFCYWdJdGVtIH0gZnJvbSBcIi4uLy4uL0RhdGFNb2RhbC9EYXRhQmFnXCI7XG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJdGVtQmFnIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBpdGVtX25hbWU6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gICAgaXRlbV9zcDogY2MuU3ByaXRlID0gbnVsbDtcblxuICAgIHNldERhdGEoaXRlbTogRGF0YUJhZ0l0ZW0pIHtcblxuICAgICAgICBpZiAoTnVtYmVyKGl0ZW0uaXRlbUlEKSA+IDExMDApIHtcbiAgICAgICAgICAgIGxldCBfY2hpcE5hbWUgPSBDb2Nvc0hlbHBlci5nZXRTaG9wTmFtZUJ5SUQoaXRlbS5pdGVtSUQudG9TdHJpbmcoKSk7XG4gICAgICAgICAgICB0aGlzLml0ZW1fbmFtZS5zdHJpbmcgPSBfY2hpcE5hbWUgKyBcIitcIiArIGl0ZW0uaXRlbUNvdW50O1xuICAgICAgICAgICAgQ29jb3NIZWxwZXIuc2V0U2hvcEluZm8oX2NoaXBOYW1lLCB0aGlzLml0ZW1fc3ApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbGV0IF9jaGlwTmFtZSA9IENvY29zSGVscGVyLmdldENoaXBOYW1lQnlJRChpdGVtLml0ZW1JRC50b1N0cmluZygpKTtcbiAgICAgICAgICAgIHRoaXMuaXRlbV9uYW1lLnN0cmluZyA9IF9jaGlwTmFtZSArIFwiK1wiICsgaXRlbS5pdGVtQ291bnQ7XG4gICAgICAgICAgICBsZXQgX2ljb25OYW1lID0gX2NoaXBOYW1lLnJlcGxhY2UoXCLnoo7niYdcIiwgXCJcIik7XG4gICAgICAgICAgICBDb2Nvc0hlbHBlci5zZXREZWFsSWNvbihfaWNvbk5hbWUsIHRoaXMuaXRlbV9zcCk7XG4gICAgICAgIH1cblxuXG4gICAgfVxufVxuIl19