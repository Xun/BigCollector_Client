
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Bag/UIBag.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '74ea71pHThDZ6ERDnoqDKMw', 'UIBag');
// Script/UIScript/Bag/UIBag.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var UIToast_1 = require("../UIToast");
var ItemBag_1 = require("./ItemBag");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIBag = /** @class */ (function (_super) {
    __extends(UIBag, _super);
    function UIBag() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listBag = null;
        return _this;
    }
    UIBag.prototype.onLoad = function () {
        // GameMgr.dataModalMgr.BagInfo.list_item = [{ item_sp: "imgs/itemProduce/item1", item_name: "毛笔碎片", item_num: 1 }];
        // this.listBag.numItems = GameMgr.dataModalMgr.BagInfo.list_item.length;
        GameMgr_1.default.dataModalMgr.BagInfo.cur_bag_capacity = GameMgr_1.default.dataModalMgr.BagInfo.list_item.length;
    };
    UIBag.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.AddBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIBagAdd);
        }, this);
        this.view.AddBtn.addClick(function () {
            _this.onAddClick();
        }, this);
        this.listBag.numItems = GameMgr_1.default.dataModalMgr.BagInfo.list_item.length;
        GameMgr_1.default.dataModalMgr.BagInfo.cur_bag_capacity = GameMgr_1.default.dataModalMgr.BagInfo.list_item.length;
        this.view.BagNumLab.string = GameMgr_1.default.dataModalMgr.BagInfo.cur_bag_capacity + "/" + GameMgr_1.default.dataModalMgr.BagInfo.total_bag_capacity;
    };
    UIBag.prototype.onAddClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("BagInfo", { type: 2 })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.BagInfo.list_item = data.res.bagList;
                        GameMgr_1.default.dataModalMgr.BagInfo.total_bag_capacity = data.res.bagVol;
                        return [2 /*return*/];
                }
            });
        });
    };
    //垂直列表渲染器
    UIBag.prototype.onListBagRender = function (item, idx) {
        item.getComponent(ItemBag_1.default).setData(GameMgr_1.default.dataModalMgr.BagInfo.list_item[idx]);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIBag.prototype, "listBag", void 0);
    UIBag = __decorate([
        ccclass
    ], UIBag);
    return UIBag;
}(UIForm_1.UIWindow));
exports.default = UIBag;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvQmFnL1VJQmFnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLDZEQUF3RDtBQUN4RCw4Q0FBK0M7QUFFL0MsaURBQTRDO0FBQzVDLGlEQUE0QztBQUU1QyxpREFBZ0Q7QUFDaEQsMkNBQXNDO0FBRXRDLHNDQUFpQztBQUNqQyxxQ0FBZ0M7QUFFMUIsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBbUMseUJBQVE7SUFBM0M7UUFBQSxxRUE2Q0M7UUF4Q0csYUFBTyxHQUFhLElBQUksQ0FBQzs7SUF3QzdCLENBQUM7SUF0Q0csc0JBQU0sR0FBTjtRQUNJLG9IQUFvSDtRQUNwSCx5RUFBeUU7UUFDekUsaUJBQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO0lBQ2xHLENBQUM7SUFFRCxxQkFBSyxHQUFMO1FBQUEsaUJBZ0JDO1FBZkcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsaUJBQU8sQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1FBQ3RFLGlCQUFPLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztRQUM5RixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixHQUFHLEdBQUcsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUM7SUFDdkksQ0FBQztJQUVLLDBCQUFVLEdBQWhCOzs7Ozs0QkFDZSxxQkFBTSxxQkFBUyxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBQTs7d0JBQXRELElBQUksR0FBRyxTQUErQzt3QkFDMUQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQ2QsaUJBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDaEMsc0JBQU87eUJBQ1Y7d0JBQ0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzt3QkFDMUQsaUJBQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDOzs7OztLQUNyRTtJQUVELFNBQVM7SUFDVCwrQkFBZSxHQUFmLFVBQWdCLElBQWEsRUFBRSxHQUFXO1FBQ3RDLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDcEYsQ0FBQztJQXZDRDtRQURDLFFBQVEsQ0FBQyxrQkFBUSxDQUFDOzBDQUNNO0lBTFIsS0FBSztRQUR6QixPQUFPO09BQ2EsS0FBSyxDQTZDekI7SUFBRCxZQUFDO0NBN0NELEFBNkNDLENBN0NrQyxpQkFBUSxHQTZDMUM7a0JBN0NvQixLQUFLIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVJQmFnX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJQmFnX0F1dG9cIjtcbmltcG9ydCBMaXN0VXRpbCBmcm9tIFwiLi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvTGlzdFV0aWxcIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCB7IERhdGFNb2RhbE1nciB9IGZyb20gXCIuLi8uLi9NYW5hZ2VyL0RhdGFNb2RhbE1nclwiO1xuaW1wb3J0IEZvcm1NZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvRm9ybU1nclwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IHsgRXZlbnRDZW50ZXIgfSBmcm9tIFwiLi4vLi4vTmV0L0V2ZW50Q2VudGVyXCI7XG5pbXBvcnQgeyBhcGlDbGllbnQgfSBmcm9tIFwiLi4vLi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IFVJQ29uZmlnIGZyb20gXCIuLi8uLi9VSUNvbmZpZ1wiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcbmltcG9ydCBJdGVtQmFnIGZyb20gXCIuL0l0ZW1CYWdcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJQmFnIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlCYWdfQXV0bztcblxuICAgIEBwcm9wZXJ0eShMaXN0VXRpbClcbiAgICBsaXN0QmFnOiBMaXN0VXRpbCA9IG51bGw7XG5cbiAgICBvbkxvYWQoKSB7XG4gICAgICAgIC8vIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkJhZ0luZm8ubGlzdF9pdGVtID0gW3sgaXRlbV9zcDogXCJpbWdzL2l0ZW1Qcm9kdWNlL2l0ZW0xXCIsIGl0ZW1fbmFtZTogXCLmr5vnrJTnoo7niYdcIiwgaXRlbV9udW06IDEgfV07XG4gICAgICAgIC8vIHRoaXMubGlzdEJhZy5udW1JdGVtcyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkJhZ0luZm8ubGlzdF9pdGVtLmxlbmd0aDtcbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuQmFnSW5mby5jdXJfYmFnX2NhcGFjaXR5ID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQmFnSW5mby5saXN0X2l0ZW0ubGVuZ3RoO1xuICAgIH1cblxuICAgIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLnZpZXcuQ2xvc2VCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkFkZEJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlCYWdBZGQpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQWRkQnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25BZGRDbGljaygpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLmxpc3RCYWcubnVtSXRlbXMgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5CYWdJbmZvLmxpc3RfaXRlbS5sZW5ndGg7XG4gICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkJhZ0luZm8uY3VyX2JhZ19jYXBhY2l0eSA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkJhZ0luZm8ubGlzdF9pdGVtLmxlbmd0aDtcbiAgICAgICAgdGhpcy52aWV3LkJhZ051bUxhYi5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5CYWdJbmZvLmN1cl9iYWdfY2FwYWNpdHkgKyBcIi9cIiArIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkJhZ0luZm8udG90YWxfYmFnX2NhcGFjaXR5O1xuICAgIH1cblxuICAgIGFzeW5jIG9uQWRkQ2xpY2soKSB7XG4gICAgICAgIGxldCBkYXRhID0gYXdhaXQgYXBpQ2xpZW50LmNhbGxBcGkoXCJCYWdJbmZvXCIsIHsgdHlwZTogMiB9KTtcbiAgICAgICAgaWYgKCFkYXRhLmlzU3VjYykge1xuICAgICAgICAgICAgVUlUb2FzdC5wb3BVcChkYXRhLmVyci5tZXNzYWdlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5CYWdJbmZvLmxpc3RfaXRlbSA9IGRhdGEucmVzLmJhZ0xpc3Q7XG4gICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkJhZ0luZm8udG90YWxfYmFnX2NhcGFjaXR5ID0gZGF0YS5yZXMuYmFnVm9sO1xuICAgIH1cblxuICAgIC8v5Z6C55u05YiX6KGo5riy5p+T5ZmoXG4gICAgb25MaXN0QmFnUmVuZGVyKGl0ZW06IGNjLk5vZGUsIGlkeDogbnVtYmVyKSB7XG4gICAgICAgIGl0ZW0uZ2V0Q29tcG9uZW50KEl0ZW1CYWcpLnNldERhdGEoR2FtZU1nci5kYXRhTW9kYWxNZ3IuQmFnSW5mby5saXN0X2l0ZW1baWR4XSk7XG4gICAgfVxufVxuIl19