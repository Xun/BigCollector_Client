
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Friend/UIFriend.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '12f854yj6NDEIDfuIsgedN/', 'UIFriend');
// Script/UIScript/Friend/UIFriend.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriend = /** @class */ (function (_super) {
    __extends(UIFriend, _super);
    function UIFriend() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // onLoad () {}
    UIFriend.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.view.BtnInvoedFriend.addClick(function () {
                            FormMgr_1.default.open(UIConfig_1.default.UIShare);
                        }, this);
                        this.view.BtnMyFriend.addClick(function () {
                            FormMgr_1.default.open(UIConfig_1.default.UIMyFriend);
                        }, this);
                        this.view.ProbablyDetail.addClick(function () {
                            FormMgr_1.default.open(UIConfig_1.default.UIFriendMonerDetail);
                        }, this);
                        this.view.UpChannel.addClick(function () {
                            FormMgr_1.default.open(UIConfig_1.default.UIExtarCommitSure);
                        }, this);
                        this.view.Call.addClick(function () {
                            FormMgr_1.default.open(UIConfig_1.default.UIInviteGetInfo);
                        }, this);
                        this.view.BtnClose.addClick(function () {
                            _this.closeSelf();
                        }, this);
                        this.view.BtnGive.addClick(function () {
                            FormMgr_1.default.open(UIConfig_1.default.UIFriendGive);
                        }, this);
                        this.view.BtnVisit.addClick(function () {
                            FormMgr_1.default.open(UIConfig_1.default.UIFriendSee);
                        }, this);
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("Earnings", {})];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                        }
                        this.view.TodayGetMoneyLab.string = data.res.todayEranings.toString();
                        this.view.TotalGetMoneyLab.string = data.res.totalEarnings.toString();
                        this.view.Name.string = data.res.myParentInfo.name;
                        CocosHelper_1.default.loadHead(data.res.myParentInfo.url, this.view.HeadSp);
                        this.view.MyFriendNumLab.string = "我的好友：" + GameMgr_1.default.dataModalMgr.FriendInfo.my_friend_num + "人";
                        this.view.ProbablyMoneyLab.string = "预计每个好友每天为你产出" + GameMgr_1.default.dataModalMgr.FriendInfo.probabyly_money + "元";
                        // this.view.TodayGetMoneyLab.string = GameMgr.dataModalMgr.FriendInfo.today_get_money + "元";
                        // this.view.TotalGetMoneyLab.string = GameMgr.dataModalMgr.FriendInfo.total_get_money + "元";
                        this.view.TodayExtarGetMoneyLab.string = GameMgr_1.default.dataModalMgr.FriendInfo.today_extar_get_money + "元";
                        this.view.TodayExtarGetMoneyTipsLab.string = "累计获得8000元后成为渠道，当前已获得" + GameMgr_1.default.dataModalMgr.FriendInfo.cur_get_money + "元";
                        return [2 /*return*/];
                }
            });
        });
    };
    UIFriend = __decorate([
        ccclass
    ], UIFriend);
    return UIFriend;
}(UIForm_1.UIWindow));
exports.default = UIFriend;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvRnJpZW5kL1VJRnJpZW5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLDhDQUErQztBQUMvQyxpREFBNEM7QUFDNUMsaURBQTRDO0FBQzVDLGlEQUFnRDtBQUNoRCwyQ0FBc0M7QUFDdEMsdURBQWtEO0FBQ2xELHNDQUFpQztBQUczQixJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFzQyw0QkFBUTtJQUE5Qzs7SUEyREEsQ0FBQztJQXZERyxlQUFlO0lBRVQsd0JBQUssR0FBWDs7Ozs7Ozt3QkFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUM7NEJBQy9CLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ25DLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUM7NEJBQzNCLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQ3RDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7NEJBQzlCLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQzt3QkFDL0MsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUVULElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQzs0QkFDekIsaUJBQU8sQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO3dCQUM3QyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDOzRCQUNwQixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3dCQUMzQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDOzRCQUN4QixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7d0JBQ3JCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7NEJBQ3ZCLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7d0JBQ3hDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7NEJBQ3hCLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQ3ZDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFFRSxxQkFBTSxxQkFBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLEVBQUE7O3dCQUE5QyxJQUFJLEdBQUcsU0FBdUM7d0JBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNkLGlCQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ25DO3dCQUVELElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUN0RSxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3QkFDdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQzt3QkFDbkQscUJBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBRWxFLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUM7d0JBQ2hHLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLGNBQWMsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQzt3QkFDM0csNkZBQTZGO3dCQUM3Riw2RkFBNkY7d0JBQzdGLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUM7d0JBQ3JHLElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsTUFBTSxHQUFHLHNCQUFzQixHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDOzs7OztLQUc3SDtJQXpEZ0IsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQTJENUI7SUFBRCxlQUFDO0NBM0RELEFBMkRDLENBM0RxQyxpQkFBUSxHQTJEN0M7a0JBM0RvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgVUlGcmllbmRfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlGcmllbmRfQXV0b1wiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IEZvcm1NZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvRm9ybU1nclwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IHsgYXBpQ2xpZW50IH0gZnJvbSBcIi4uLy4uL05ldC9ScGNDb25lbnRcIjtcbmltcG9ydCBVSUNvbmZpZyBmcm9tIFwiLi4vLi4vVUlDb25maWdcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmltcG9ydCBVSVRvYXN0IGZyb20gXCIuLi9VSVRvYXN0XCI7XG5pbXBvcnQgVUlGcmllbmRTZWUgZnJvbSBcIi4vVUlGcmllbmRTZWVcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJRnJpZW5kIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlGcmllbmRfQXV0bztcblxuICAgIC8vIG9uTG9hZCAoKSB7fVxuXG4gICAgYXN5bmMgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5CdG5JbnZvZWRGcmllbmQuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJU2hhcmUpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTXlGcmllbmQuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJTXlGcmllbmQpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuUHJvYmFibHlEZXRhaWwuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJRnJpZW5kTW9uZXJEZXRhaWwpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuVXBDaGFubmVsLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSUV4dGFyQ29tbWl0U3VyZSk7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5DYWxsLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSUludml0ZUdldEluZm8pO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuQ2xvc2UuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkdpdmUuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJRnJpZW5kR2l2ZSk7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5WaXNpdC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlGcmllbmRTZWUpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICBsZXQgZGF0YSA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiRWFybmluZ3NcIiwge30pO1xuICAgICAgICBpZiAoIWRhdGEuaXNTdWNjKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKGRhdGEuZXJyLm1lc3NhZ2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy52aWV3LlRvZGF5R2V0TW9uZXlMYWIuc3RyaW5nID0gZGF0YS5yZXMudG9kYXlFcmFuaW5ncy50b1N0cmluZygpO1xuICAgICAgICB0aGlzLnZpZXcuVG90YWxHZXRNb25leUxhYi5zdHJpbmcgPSBkYXRhLnJlcy50b3RhbEVhcm5pbmdzLnRvU3RyaW5nKCk7XG4gICAgICAgIHRoaXMudmlldy5OYW1lLnN0cmluZyA9IGRhdGEucmVzLm15UGFyZW50SW5mby5uYW1lO1xuICAgICAgICBDb2Nvc0hlbHBlci5sb2FkSGVhZChkYXRhLnJlcy5teVBhcmVudEluZm8udXJsLCB0aGlzLnZpZXcuSGVhZFNwKTtcblxuICAgICAgICB0aGlzLnZpZXcuTXlGcmllbmROdW1MYWIuc3RyaW5nID0gXCLmiJHnmoTlpb3lj4vvvJpcIiArIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkZyaWVuZEluZm8ubXlfZnJpZW5kX251bSArIFwi5Lq6XCI7XG4gICAgICAgIHRoaXMudmlldy5Qcm9iYWJseU1vbmV5TGFiLnN0cmluZyA9IFwi6aKE6K6h5q+P5Liq5aW95Y+L5q+P5aSp5Li65L2g5Lqn5Ye6XCIgKyBHYW1lTWdyLmRhdGFNb2RhbE1nci5GcmllbmRJbmZvLnByb2JhYnlseV9tb25leSArIFwi5YWDXCI7XG4gICAgICAgIC8vIHRoaXMudmlldy5Ub2RheUdldE1vbmV5TGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkZyaWVuZEluZm8udG9kYXlfZ2V0X21vbmV5ICsgXCLlhYNcIjtcbiAgICAgICAgLy8gdGhpcy52aWV3LlRvdGFsR2V0TW9uZXlMYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuRnJpZW5kSW5mby50b3RhbF9nZXRfbW9uZXkgKyBcIuWFg1wiO1xuICAgICAgICB0aGlzLnZpZXcuVG9kYXlFeHRhckdldE1vbmV5TGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkZyaWVuZEluZm8udG9kYXlfZXh0YXJfZ2V0X21vbmV5ICsgXCLlhYNcIjtcbiAgICAgICAgdGhpcy52aWV3LlRvZGF5RXh0YXJHZXRNb25leVRpcHNMYWIuc3RyaW5nID0gXCLntK/orqHojrflvpc4MDAw5YWD5ZCO5oiQ5Li65rig6YGT77yM5b2T5YmN5bey6I635b6XXCIgKyBHYW1lTWdyLmRhdGFNb2RhbE1nci5GcmllbmRJbmZvLmN1cl9nZXRfbW9uZXkgKyBcIuWFg1wiO1xuICAgICAgICAvLyB0aGlzLnZpZXcuTmFtZS5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5GcmllbmRJbmZvLm15X2ludml0ZXJfbmFtZTtcbiAgICAgICAgLy8gQ29jb3NIZWxwZXIubG9hZEhlYWQoR2FtZU1nci5kYXRhTW9kYWxNZ3IuRnJpZW5kSW5mby5teV9pbnZpdGVfaGVhZCwgdGhpcy52aWV3LkhlYWRTcCk7XG4gICAgfVxuXG59XG4iXX0=