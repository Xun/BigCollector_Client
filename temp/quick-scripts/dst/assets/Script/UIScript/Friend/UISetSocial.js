
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Friend/UISetSocial.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9e973g5RGZP6pnznOyQce3A', 'UISetSocial');
// Script/UIScript/Friend/UISetSocial.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var RpcConent_1 = require("../../Net/RpcConent");
var UIToast_1 = require("../UIToast");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UISetSocial = /** @class */ (function (_super) {
    __extends(UISetSocial, _super);
    function UISetSocial() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._wechat = "";
        _this._qq = "";
        return _this;
    }
    // onLoad () {}
    UISetSocial.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.SureBtn.addClick(function () {
            _this.sure();
        }, this);
        this.view.Wechat.node.on('text-changed', this.onWechat, this);
        this.view.QQ.node.on('text-changed', this.onQQ, this);
    };
    UISetSocial.prototype.onWechat = function (event) {
        this._wechat = event.string;
    };
    UISetSocial.prototype.onQQ = function (event) {
        this._qq = event.string;
    };
    UISetSocial.prototype.sure = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("SocialSet", { weChat: this._wechat, QQ: this._qq })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        this.closeSelf();
                        UIToast_1.default.popUp("设置成功");
                        return [2 /*return*/];
                }
            });
        });
    };
    UISetSocial = __decorate([
        ccclass
    ], UISetSocial);
    return UISetSocial;
}(UIForm_1.UIWindow));
exports.default = UISetSocial;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvRnJpZW5kL1VJU2V0U29jaWFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLDhDQUErQztBQUMvQyxpREFBZ0Q7QUFDaEQsc0NBQWlDO0FBRTNCLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXlDLCtCQUFRO0lBQWpEO1FBQUEscUVBb0NDO1FBakNXLGFBQU8sR0FBVyxFQUFFLENBQUM7UUFDckIsU0FBRyxHQUFXLEVBQUUsQ0FBQzs7SUFnQzdCLENBQUM7SUEvQkcsZUFBZTtJQUVmLDJCQUFLLEdBQUw7UUFBQSxpQkFVQztRQVRHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUN4QixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNoQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVELDhCQUFRLEdBQVIsVUFBUyxLQUFLO1FBQ1YsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQ2hDLENBQUM7SUFFRCwwQkFBSSxHQUFKLFVBQUssS0FBSztRQUNOLElBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUM1QixDQUFDO0lBRUssMEJBQUksR0FBVjs7Ozs7NEJBQ2UscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFBOzt3QkFBbkYsSUFBSSxHQUFHLFNBQTRFO3dCQUN2RixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTs0QkFDZCxpQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDOzRCQUNoQyxzQkFBTzt5QkFDVjt3QkFDRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7d0JBQ2pCLGlCQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7OztLQUN6QjtJQW5DZ0IsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQW9DL0I7SUFBRCxrQkFBQztDQXBDRCxBQW9DQyxDQXBDd0MsaUJBQVEsR0FvQ2hEO2tCQXBDb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IFVJU2V0U29jaWFsX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJU2V0U29jaWFsX0F1dG9cIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCB7IGFwaUNsaWVudCB9IGZyb20gXCIuLi8uLi9OZXQvUnBjQ29uZW50XCI7XG5pbXBvcnQgVUlUb2FzdCBmcm9tIFwiLi4vVUlUb2FzdFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlTZXRTb2NpYWwgZXh0ZW5kcyBVSVdpbmRvdyB7XG5cbiAgICB2aWV3OiBVSVNldFNvY2lhbF9BdXRvO1xuICAgIHByaXZhdGUgX3dlY2hhdDogc3RyaW5nID0gXCJcIjtcbiAgICBwcml2YXRlIF9xcTogc3RyaW5nID0gXCJcIjtcbiAgICAvLyBvbkxvYWQgKCkge31cblxuICAgIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLnZpZXcuQ2xvc2VCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LlN1cmVCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zdXJlKCk7XG4gICAgICAgIH0sIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuV2VjaGF0Lm5vZGUub24oJ3RleHQtY2hhbmdlZCcsIHRoaXMub25XZWNoYXQsIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuUVEubm9kZS5vbigndGV4dC1jaGFuZ2VkJywgdGhpcy5vblFRLCB0aGlzKTtcbiAgICB9XG5cbiAgICBvbldlY2hhdChldmVudCkge1xuICAgICAgICB0aGlzLl93ZWNoYXQgPSBldmVudC5zdHJpbmc7XG4gICAgfVxuXG4gICAgb25RUShldmVudCkge1xuICAgICAgICB0aGlzLl9xcSA9IGV2ZW50LnN0cmluZztcbiAgICB9XG5cbiAgICBhc3luYyBzdXJlKCkge1xuICAgICAgICBsZXQgZGF0YSA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiU29jaWFsU2V0XCIsIHsgd2VDaGF0OiB0aGlzLl93ZWNoYXQsIFFROiB0aGlzLl9xcSB9KTtcbiAgICAgICAgaWYgKCFkYXRhLmlzU3VjYykge1xuICAgICAgICAgICAgVUlUb2FzdC5wb3BVcChkYXRhLmVyci5tZXNzYWdlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICBVSVRvYXN0LnBvcFVwKFwi6K6+572u5oiQ5YqfXCIpO1xuICAgIH1cbn1cbiJdfQ==