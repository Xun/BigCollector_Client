
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Friend/ItemFriendExtraDetail.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fb9c3YtKd9Lio7M6uVogfCp', 'ItemFriendExtraDetail');
// Script/UIScript/Friend/ItemFriendExtraDetail.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemFriendExtraDetail = /** @class */ (function (_super) {
    __extends(ItemFriendExtraDetail, _super);
    function ItemFriendExtraDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.data_lab = null;
        _this.friend_one_lab = null;
        _this.friend_two_lab = null;
        _this.total_lab = null;
        return _this;
    }
    ItemFriendExtraDetail.prototype.setData = function (data) {
        this.data_lab.string = CocosHelper_1.default.changeTime(data.time);
        this.friend_one_lab.string = data.oneFriendEranings + "元";
        this.friend_two_lab.string = data.twoFriendEranings + "元";
        this.total_lab.string = data.todayTotalEranings + "元";
    };
    __decorate([
        property(cc.Label)
    ], ItemFriendExtraDetail.prototype, "data_lab", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendExtraDetail.prototype, "friend_one_lab", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendExtraDetail.prototype, "friend_two_lab", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendExtraDetail.prototype, "total_lab", void 0);
    ItemFriendExtraDetail = __decorate([
        ccclass
    ], ItemFriendExtraDetail);
    return ItemFriendExtraDetail;
}(cc.Component));
exports.default = ItemFriendExtraDetail;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvRnJpZW5kL0l0ZW1GcmllbmRFeHRyYURldGFpbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSx1REFBa0Q7QUFFNUMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBbUQseUNBQVk7SUFBL0Q7UUFBQSxxRUFxQkM7UUFsQkcsY0FBUSxHQUFhLElBQUksQ0FBQztRQUcxQixvQkFBYyxHQUFhLElBQUksQ0FBQztRQUdoQyxvQkFBYyxHQUFhLElBQUksQ0FBQztRQUdoQyxlQUFTLEdBQWEsSUFBSSxDQUFDOztJQVMvQixDQUFDO0lBUEcsdUNBQU8sR0FBUCxVQUFRLElBQStCO1FBQ25DLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLHFCQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsR0FBRyxDQUFDO1FBQzFELElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLENBQUM7UUFDMUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEdBQUcsQ0FBQztJQUMxRCxDQUFDO0lBaEJEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7MkRBQ087SUFHMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztpRUFDYTtJQUdoQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2lFQUNhO0lBR2hDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7NERBQ1E7SUFaVixxQkFBcUI7UUFEekMsT0FBTztPQUNhLHFCQUFxQixDQXFCekM7SUFBRCw0QkFBQztDQXJCRCxBQXFCQyxDQXJCa0QsRUFBRSxDQUFDLFNBQVMsR0FxQjlEO2tCQXJCb0IscUJBQXFCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGF0YUl0ZW1GcmllbmRNb25leURldGFpbCB9IGZyb20gXCIuLi8uLi9EYXRhTW9kYWwvRGF0YUZyaWVuZFwiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSXRlbUZyaWVuZEV4dHJhRGV0YWlsIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBkYXRhX2xhYjogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGZyaWVuZF9vbmVfbGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgZnJpZW5kX3R3b19sYWI6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB0b3RhbF9sYWI6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIHNldERhdGEoZGF0YTogRGF0YUl0ZW1GcmllbmRNb25leURldGFpbCkge1xuICAgICAgICB0aGlzLmRhdGFfbGFiLnN0cmluZyA9IENvY29zSGVscGVyLmNoYW5nZVRpbWUoZGF0YS50aW1lKTtcbiAgICAgICAgdGhpcy5mcmllbmRfb25lX2xhYi5zdHJpbmcgPSBkYXRhLm9uZUZyaWVuZEVyYW5pbmdzICsgXCLlhYNcIjtcbiAgICAgICAgdGhpcy5mcmllbmRfdHdvX2xhYi5zdHJpbmcgPSBkYXRhLnR3b0ZyaWVuZEVyYW5pbmdzICsgXCLlhYNcIjtcbiAgICAgICAgdGhpcy50b3RhbF9sYWIuc3RyaW5nID0gZGF0YS50b2RheVRvdGFsRXJhbmluZ3MgKyBcIuWFg1wiO1xuICAgIH1cblxufVxuIl19