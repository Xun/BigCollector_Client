
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Friend/UIFriendGive.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a291c4siCBG+71COhJFJCcd', 'UIFriendGive');
// Script/UIScript/Friend/UIFriendGive.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var RpcConent_1 = require("../../Net/RpcConent");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendGive = /** @class */ (function (_super) {
    __extends(UIFriendGive, _super);
    function UIFriendGive() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._userId = "";
        _this._num = "";
        return _this;
    }
    // onLoad () {}
    UIFriendGive.prototype.start = function () {
        var _this = this;
        this.view.BtnClose.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.BtnSerch.addClick(function () {
            _this.onSerch();
        }, this);
        this.view.BtnGive.addClick(function () {
            _this.onGive();
        }, this);
        this.view.IDBox.node.on('text-changed', this.onInputID, this);
        this.view.GiveBox.node.on('text-changed', this.onInputNum, this);
    };
    UIFriendGive.prototype.onInputID = function (event) {
        this._userId = event.string;
    };
    UIFriendGive.prototype.onInputNum = function (event) {
        this._num = event.string;
    };
    UIFriendGive.prototype.onSerch = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("SerchFriend", { id: this._userId })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        this.view.NameLab.string = data.res.serchInfo.name;
                        CocosHelper_1.default.loadHead(data.res.serchInfo.url, this.view.HeadSP);
                        return [2 /*return*/];
                }
            });
        });
    };
    UIFriendGive.prototype.onGive = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("GiveFriend", { id: this._userId, num: this._num })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        UIToast_1.default.popUp("赠送成功");
                        return [2 /*return*/];
                }
            });
        });
    };
    UIFriendGive = __decorate([
        ccclass
    ], UIFriendGive);
    return UIFriendGive;
}(UIForm_1.UIWindow));
exports.default = UIFriendGive;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvRnJpZW5kL1VJRnJpZW5kR2l2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSw4Q0FBK0M7QUFDL0MsaURBQWdEO0FBQ2hELHVEQUFrRDtBQUNsRCxzQ0FBaUM7QUFFM0IsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBMEMsZ0NBQVE7SUFBbEQ7UUFBQSxxRUErQ0M7UUE1Q1csYUFBTyxHQUFXLEVBQUUsQ0FBQztRQUNyQixVQUFJLEdBQVcsRUFBRSxDQUFDOztJQTJDOUIsQ0FBQztJQXpDRyxlQUFlO0lBRWYsNEJBQUssR0FBTDtRQUFBLGlCQVlDO1FBWEcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDeEIsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ25CLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUN2QixLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFBO0lBQ3BFLENBQUM7SUFFRCxnQ0FBUyxHQUFULFVBQVUsS0FBSztRQUNYLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUNoQyxDQUFDO0lBRUQsaUNBQVUsR0FBVixVQUFXLEtBQUs7UUFDWixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDN0IsQ0FBQztJQUNLLDhCQUFPLEdBQWI7Ozs7OzRCQUNlLHFCQUFNLHFCQUFTLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBQTs7d0JBQW5FLElBQUksR0FBRyxTQUE0RDt3QkFDdkUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQ2QsaUJBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDaEMsc0JBQU87eUJBQ1Y7d0JBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzt3QkFDbkQscUJBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7O0tBQ2xFO0lBRUssNkJBQU0sR0FBWjs7Ozs7NEJBQ2UscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFBOzt3QkFBbEYsSUFBSSxHQUFHLFNBQTJFO3dCQUN0RixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTs0QkFDZCxpQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDOzRCQUNoQyxzQkFBTzt5QkFDVjt3QkFDRCxpQkFBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQzs7Ozs7S0FDekI7SUE5Q2dCLFlBQVk7UUFEaEMsT0FBTztPQUNhLFlBQVksQ0ErQ2hDO0lBQUQsbUJBQUM7Q0EvQ0QsQUErQ0MsQ0EvQ3lDLGlCQUFRLEdBK0NqRDtrQkEvQ29CLFlBQVkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBVSUZyaWVuZEdpdmVfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlGcmllbmRHaXZlX0F1dG9cIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCB7IGFwaUNsaWVudCB9IGZyb20gXCIuLi8uLi9OZXQvUnBjQ29uZW50XCI7XG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5pbXBvcnQgVUlUb2FzdCBmcm9tIFwiLi4vVUlUb2FzdFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlGcmllbmRHaXZlIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlGcmllbmRHaXZlX0F1dG87XG4gICAgcHJpdmF0ZSBfdXNlcklkOiBzdHJpbmcgPSBcIlwiO1xuICAgIHByaXZhdGUgX251bTogc3RyaW5nID0gXCJcIjtcblxuICAgIC8vIG9uTG9hZCAoKSB7fVxuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5CdG5DbG9zZS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LkJ0blNlcmNoLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZXJjaCgpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LkJ0bkdpdmUuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vbkdpdmUoKTtcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgICAgIHRoaXMudmlldy5JREJveC5ub2RlLm9uKCd0ZXh0LWNoYW5nZWQnLCB0aGlzLm9uSW5wdXRJRCwgdGhpcyk7XG4gICAgICAgIHRoaXMudmlldy5HaXZlQm94Lm5vZGUub24oJ3RleHQtY2hhbmdlZCcsIHRoaXMub25JbnB1dE51bSwgdGhpcylcbiAgICB9XG5cbiAgICBvbklucHV0SUQoZXZlbnQpIHtcbiAgICAgICAgdGhpcy5fdXNlcklkID0gZXZlbnQuc3RyaW5nO1xuICAgIH1cblxuICAgIG9uSW5wdXROdW0oZXZlbnQpIHtcbiAgICAgICAgdGhpcy5fbnVtID0gZXZlbnQuc3RyaW5nO1xuICAgIH1cbiAgICBhc3luYyBvblNlcmNoKCkge1xuICAgICAgICBsZXQgZGF0YSA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiU2VyY2hGcmllbmRcIiwgeyBpZDogdGhpcy5fdXNlcklkIH0pO1xuICAgICAgICBpZiAoIWRhdGEuaXNTdWNjKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKGRhdGEuZXJyLm1lc3NhZ2UpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMudmlldy5OYW1lTGFiLnN0cmluZyA9IGRhdGEucmVzLnNlcmNoSW5mby5uYW1lO1xuICAgICAgICBDb2Nvc0hlbHBlci5sb2FkSGVhZChkYXRhLnJlcy5zZXJjaEluZm8udXJsLCB0aGlzLnZpZXcuSGVhZFNQKTtcbiAgICB9XG5cbiAgICBhc3luYyBvbkdpdmUoKSB7XG4gICAgICAgIGxldCBkYXRhID0gYXdhaXQgYXBpQ2xpZW50LmNhbGxBcGkoXCJHaXZlRnJpZW5kXCIsIHsgaWQ6IHRoaXMuX3VzZXJJZCwgbnVtOiB0aGlzLl9udW0gfSk7XG4gICAgICAgIGlmICghZGF0YS5pc1N1Y2MpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoZGF0YS5lcnIubWVzc2FnZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgVUlUb2FzdC5wb3BVcChcIui1oOmAgeaIkOWKn1wiKTtcbiAgICB9XG59XG4iXX0=