
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Friend/UIFriendSee.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '051619VVkpPCZo5puArAdJa', 'UIFriendSee');
// Script/UIScript/Friend/UIFriendSee.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var UIConfig_1 = require("../../UIConfig");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendSee = /** @class */ (function (_super) {
    __extends(UIFriendSee, _super);
    function UIFriendSee() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UIFriendSee.prototype.start = function () {
        var _this = this;
        this.view.BtnClose.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.InviteBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIShare);
        }, this);
        this.view.AddFriendBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIFriendSerch);
        }, this);
        this.view.SeeDetailBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIFriendSeeDetail);
        }, this);
    };
    UIFriendSee = __decorate([
        ccclass
    ], UIFriendSee);
    return UIFriendSee;
}(UIForm_1.UIWindow));
exports.default = UIFriendSee;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvRnJpZW5kL1VJRnJpZW5kU2VlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLDhDQUErQztBQUMvQyxpREFBNEM7QUFDNUMsMkNBQXNDO0FBRWhDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXlDLCtCQUFRO0lBQWpEOztJQXNCQSxDQUFDO0lBbEJHLDJCQUFLLEdBQUw7UUFBQSxpQkFnQkM7UUFmRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDeEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztZQUN6QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ25DLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztZQUM1QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3pDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztZQUM1QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDN0MsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQXBCZ0IsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQXNCL0I7SUFBRCxrQkFBQztDQXRCRCxBQXNCQyxDQXRCd0MsaUJBQVEsR0FzQmhEO2tCQXRCb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IFVJRnJpZW5kU2VlX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJRnJpZW5kU2VlX0F1dG9cIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCBGb3JtTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL0Zvcm1NZ3JcIjtcbmltcG9ydCBVSUNvbmZpZyBmcm9tIFwiLi4vLi4vVUlDb25maWdcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJRnJpZW5kU2VlIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlGcmllbmRTZWVfQXV0bztcblxuICAgIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLnZpZXcuQnRuQ2xvc2UuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3Lkludml0ZUJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlTaGFyZSk7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5BZGRGcmllbmRCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJRnJpZW5kU2VyY2gpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuU2VlRGV0YWlsQnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSUZyaWVuZFNlZURldGFpbCk7XG4gICAgICAgIH0sIHRoaXMpO1xuICAgIH1cblxufVxuIl19