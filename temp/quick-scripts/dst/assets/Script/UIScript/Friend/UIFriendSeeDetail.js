
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Friend/UIFriendSeeDetail.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9f8fcAhCUtAq5OTeNZjyvJQ', 'UIFriendSeeDetail');
// Script/UIScript/Friend/UIFriendSeeDetail.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendSeeDetail = /** @class */ (function (_super) {
    __extends(UIFriendSeeDetail, _super);
    function UIFriendSeeDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.list_see_detail = null;
        return _this;
    }
    UIFriendSeeDetail.prototype.onLoad = function () {
        // this.list_see_detail.numItems
    };
    UIFriendSeeDetail.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
    };
    //垂直列表渲染器
    UIFriendSeeDetail.prototype.onListRankRender = function (item) {
        // item.getComponent(ItemFriendSeeDetail).setData();
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIFriendSeeDetail.prototype, "list_see_detail", void 0);
    UIFriendSeeDetail = __decorate([
        ccclass
    ], UIFriendSeeDetail);
    return UIFriendSeeDetail;
}(UIForm_1.UIWindow));
exports.default = UIFriendSeeDetail;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvRnJpZW5kL1VJRnJpZW5kU2VlRGV0YWlsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLDZEQUF3RDtBQUN4RCw4Q0FBK0M7QUFJekMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBK0MscUNBQVE7SUFBdkQ7UUFBQSxxRUFzQkM7UUFqQkcscUJBQWUsR0FBYSxJQUFJLENBQUM7O0lBaUJyQyxDQUFDO0lBZkcsa0NBQU0sR0FBTjtRQUNJLGdDQUFnQztJQUNwQyxDQUFDO0lBRUQsaUNBQUssR0FBTDtRQUFBLGlCQUlDO1FBSEcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsU0FBUztJQUNULDRDQUFnQixHQUFoQixVQUFpQixJQUFhO1FBQzFCLG9EQUFvRDtJQUN4RCxDQUFDO0lBZkQ7UUFEQyxRQUFRLENBQUMsa0JBQVEsQ0FBQzs4REFDYztJQUxoQixpQkFBaUI7UUFEckMsT0FBTztPQUNhLGlCQUFpQixDQXNCckM7SUFBRCx3QkFBQztDQXRCRCxBQXNCQyxDQXRCOEMsaUJBQVEsR0FzQnREO2tCQXRCb0IsaUJBQWlCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgVUlGcmllbmRTZWVEZXRhaWxfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlGcmllbmRTZWVEZXRhaWxfQXV0b1wiO1xuaW1wb3J0IExpc3RVdGlsIGZyb20gXCIuLi8uLi9Db21tb24vQ29tcG9uZW50cy9MaXN0VXRpbFwiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IEl0ZW1GcmllbmRTZWVEZXRhaWwgZnJvbSBcIi4vSXRlbUZyaWVuZFNlZURldGFpbFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlGcmllbmRTZWVEZXRhaWwgZXh0ZW5kcyBVSVdpbmRvdyB7XG5cbiAgICB2aWV3OiBVSUZyaWVuZFNlZURldGFpbF9BdXRvO1xuXG4gICAgQHByb3BlcnR5KExpc3RVdGlsKVxuICAgIGxpc3Rfc2VlX2RldGFpbDogTGlzdFV0aWwgPSBudWxsO1xuXG4gICAgb25Mb2FkKCkge1xuICAgICAgICAvLyB0aGlzLmxpc3Rfc2VlX2RldGFpbC5udW1JdGVtc1xuICAgIH1cblxuICAgIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLnZpZXcuQ2xvc2VCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgfVxuXG4gICAgLy/lnoLnm7TliJfooajmuLLmn5PlmahcbiAgICBvbkxpc3RSYW5rUmVuZGVyKGl0ZW06IGNjLk5vZGUpIHtcbiAgICAgICAgLy8gaXRlbS5nZXRDb21wb25lbnQoSXRlbUZyaWVuZFNlZURldGFpbCkuc2V0RGF0YSgpO1xuICAgIH1cblxufVxuIl19