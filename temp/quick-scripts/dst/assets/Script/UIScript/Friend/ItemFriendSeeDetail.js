
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Friend/ItemFriendSeeDetail.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2bda8vJmM1CbaeYq3QgXRRS', 'ItemFriendSeeDetail');
// Script/UIScript/Friend/ItemFriendSeeDetail.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("../../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemFriendSeeDetail = /** @class */ (function (_super) {
    __extends(ItemFriendSeeDetail, _super);
    function ItemFriendSeeDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.uname = null;
        _this.uid = null;
        _this.utime = null;
        _this.uhead = null;
        _this.seeBtn = null;
        return _this;
    }
    ItemFriendSeeDetail.prototype.start = function () {
        this.seeBtn.addClick(function () {
            console.log("huifang");
        }, this);
    };
    ItemFriendSeeDetail.prototype.setData = function (data) {
    };
    __decorate([
        property(cc.Label)
    ], ItemFriendSeeDetail.prototype, "uname", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendSeeDetail.prototype, "uid", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendSeeDetail.prototype, "utime", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemFriendSeeDetail.prototype, "uhead", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], ItemFriendSeeDetail.prototype, "seeBtn", void 0);
    ItemFriendSeeDetail = __decorate([
        ccclass
    ], ItemFriendSeeDetail);
    return ItemFriendSeeDetail;
}(cc.Component));
exports.default = ItemFriendSeeDetail;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvRnJpZW5kL0l0ZW1GcmllbmRTZWVEZXRhaWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFbEYsaUVBQTREO0FBR3RELElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQWlELHVDQUFZO0lBQTdEO1FBQUEscUVBMEJDO1FBdkJHLFdBQUssR0FBYSxJQUFJLENBQUM7UUFHdkIsU0FBRyxHQUFhLElBQUksQ0FBQztRQUdyQixXQUFLLEdBQWEsSUFBSSxDQUFDO1FBR3ZCLFdBQUssR0FBYyxJQUFJLENBQUM7UUFHeEIsWUFBTSxHQUFlLElBQUksQ0FBQzs7SUFXOUIsQ0FBQztJQVRHLG1DQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNiLENBQUM7SUFFRCxxQ0FBTyxHQUFQLFVBQVEsSUFBdUI7SUFFL0IsQ0FBQztJQXRCRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3NEQUNJO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7b0RBQ0U7SUFHckI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztzREFDSTtJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3NEQUNJO0lBR3hCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7dURBQ0s7SUFmVCxtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQTBCdkM7SUFBRCwwQkFBQztDQTFCRCxBQTBCQyxDQTFCZ0QsRUFBRSxDQUFDLFNBQVMsR0EwQjVEO2tCQTFCb0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCI7XG5pbXBvcnQgeyBEYXRhSXRlbVNlZURldGFpbCB9IGZyb20gXCIuLi8uLi9EYXRhTW9kYWwvRGF0YUZyaWVuZFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSXRlbUZyaWVuZFNlZURldGFpbCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgdW5hbWU6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB1aWQ6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB1dGltZTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICB1aGVhZDogY2MuU3ByaXRlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuICAgIHNlZUJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cbiAgICBzdGFydCgpIHtcbiAgICAgICAgdGhpcy5zZWVCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJodWlmYW5nXCIpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICB9XG5cbiAgICBzZXREYXRhKGRhdGE6IERhdGFJdGVtU2VlRGV0YWlsKSB7XG5cbiAgICB9XG59XG4iXX0=