
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/UILogin.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '458d9swLH9Pm7SGFvP8V6H5', 'UILogin');
// Script/UIScript/UILogin.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../Common/UIForm");
var FormMgr_1 = require("../Manager/FormMgr");
var GameMgr_1 = require("../Manager/GameMgr");
var RpcConent_1 = require("../Net/RpcConent");
var UIConfig_1 = require("../UIConfig");
var UIToast_1 = require("./UIToast");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UILogin = /** @class */ (function (_super) {
    __extends(UILogin, _super);
    function UILogin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UILogin_1 = UILogin;
    UILogin.prototype.onLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        RpcConent_1.apiClient.flows.postDisconnectFlow.push(function (v) {
                            if (!v.isManual) {
                                setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                                    var res;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4 /*yield*/, RpcConent_1.apiClient.connect()];
                                            case 1:
                                                res = _a.sent();
                                                if (!res.isSucc) {
                                                    console.log(result.errMsg);
                                                    FormMgr_1.default.open(UIConfig_1.default.UIScoketReconnent);
                                                }
                                                return [2 /*return*/];
                                        }
                                    });
                                }); });
                            }
                            return v;
                        });
                        RpcConent_1.apiClient.listenMsg("WorkBench", function (msg) {
                            GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench = msg.userWorkBench;
                            GameMgr_1.default.dataModalMgr.UserInfo.userCurNeedBuyCoinNum = msg.buyNeedCoin;
                        });
                        return [4 /*yield*/, RpcConent_1.apiClient.connect()];
                    case 1:
                        result = _a.sent();
                        // 连接不一定成功（例如网络错误），所以要记得错误处理
                        if (!result.isSucc) {
                            console.log(result.errMsg);
                            FormMgr_1.default.open(UIConfig_1.default.UIScoketReconnent);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    UILogin.wechatCall = function (code) {
        return __awaiter(this, void 0, void 0, function () {
            var datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("code:" + code);
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("Login", {
                                code: code
                            })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            FormMgr_1.default.open(UIConfig_1.default.UIScoketReconnent);
                            return [2 /*return*/];
                        }
                        // console.log()
                        GameMgr_1.default.dataModalMgr.UserInfo.userAccount = datas.res.userInfo.userAccount;
                        GameMgr_1.default.dataModalMgr.UserInfo.userName = datas.res.userInfo.userName;
                        // GameMgr.dataModalMgr.UserInfo.userAvatarUrl = datas.res.userInfo.userAvatarUrl;
                        GameMgr_1.default.dataModalMgr.UserInfo.userAvatarUrl = "https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=false&word=%E5%A4%B4%E5%83%8F&hs=0&pn=4&spn=0&di=7146857200093233153&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&ie=utf-8&oe=utf-8&cl=2&lm=-1&cs=3172226535%2C2587509282&os=1391451534%2C125733970&simid=59149375%2C589686028&adpicid=0&lpn=0&ln=30&fr=ala&fm=&sme=&cg=head&bdtype=0&oriquery=%E5%A4%B4%E5%83%8F&objurl=https%3A%2F%2Fgimg2.baidu.com%2Fimage_search%2Fsrc%3Dhttp%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202107%2F09%2F20210709142454_dc8dc.thumb.1000_0.jpeg%26refer%3Dhttp%3A%2F%2Fc-ssl.duitang.com%26app%3D2002%26size%3Df9999%2C10000%26q%3Da80%26n%3D0%26g%3D0n%26fmt%3Dauto%3Fsec%3D1670321388%26t%3D27faa2c2687ab622529fa60f58795d7f&fromurl=ippr_z2C%24qAzdH3FAzdH3Fooo_z%26e3B17tpwg2_z%26e3Bv54AzdH3Fks52AzdH3F%3Ft1%3D8nc000m8an&gsm=&islist=&querylist=&dyTabStr=MCwzLDEsNSwyLDcsOCw2LDQsOQ%3D%3D";
                        GameMgr_1.default.dataModalMgr.UserInfo.userCoin = datas.res.userInfo.userCoin;
                        GameMgr_1.default.dataModalMgr.UserInfo.userDiamond = datas.res.userInfo.userDiamond;
                        GameMgr_1.default.dataModalMgr.UserInfo.userMoney = datas.res.userInfo.userMoney;
                        FormMgr_1.default.open(UIConfig_1.default.UIHome);
                        return [2 /*return*/];
                }
            });
        });
    };
    UILogin.prototype.start = function () {
        var _this = this;
        this.view.BtnWechat.addClick(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.login();
                return [2 /*return*/];
            });
        }); }, this);
    };
    UILogin.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isAgarn;
            return __generator(this, function (_a) {
                isAgarn = this.view.UserAgarn.getComponent(cc.Toggle).isChecked;
                if (!isAgarn) {
                    UIToast_1.default.popUp("请阅读并同意《用户协议》");
                    return [2 /*return*/];
                }
                // NativeMgr.callNativeClass("AppActivity", "loginWX", UILogin.testRpc);
                // console.log("调用完成")
                UILogin_1.wechatCall(this.view.testAccount.string);
                return [2 /*return*/];
            });
        });
    };
    var UILogin_1;
    UILogin = UILogin_1 = __decorate([
        ccclass
    ], UILogin);
    return UILogin;
}(UIForm_1.UIScreen));
exports.default = UILogin;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvVUlMb2dpbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJQSwyQ0FBNEM7QUFHNUMsOENBQXlDO0FBQ3pDLDhDQUF5QztBQUt6Qyw4Q0FBNkM7QUFDN0Msd0NBQW1DO0FBR25DLHFDQUFnQztBQUUxQixJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFxQywyQkFBUTtJQUE3Qzs7SUF5RUEsQ0FBQztnQkF6RW9CLE9BQU87SUFJbEIsd0JBQU0sR0FBWjs7Ozs7Ozt3QkFFSSxxQkFBUyxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDOzRCQUNyQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtnQ0FDYixVQUFVLENBQUM7Ozs7b0RBQ0cscUJBQU0scUJBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBQTs7Z0RBQS9CLEdBQUcsR0FBRyxTQUF5QjtnREFDbkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUU7b0RBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7b0RBQzNCLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQztpREFDNUM7Ozs7cUNBQ0osQ0FBQyxDQUFBOzZCQUNMOzRCQUNELE9BQU8sQ0FBQyxDQUFDO3dCQUNiLENBQUMsQ0FBQyxDQUFBO3dCQUVGLHFCQUFTLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxVQUFDLEdBQUc7NEJBQ2pDLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDLGFBQWEsQ0FBQzs0QkFDaEUsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsQ0FBQyxXQUFXLENBQUM7d0JBQzFFLENBQUMsQ0FBQyxDQUFBO3dCQUdXLHFCQUFNLHFCQUFTLENBQUMsT0FBTyxFQUFFLEVBQUE7O3dCQUFsQyxNQUFNLEdBQUcsU0FBeUI7d0JBQ3RDLDRCQUE0Qjt3QkFDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7NEJBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzRCQUMzQixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7eUJBRTVDOzs7OztLQUNKO0lBRW1CLGtCQUFVLEdBQTlCLFVBQStCLElBQVk7Ozs7Ozt3QkFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLENBQUM7d0JBQ2hCLHFCQUFNLHFCQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTtnQ0FDekMsSUFBSSxFQUFFLElBQUk7NkJBQ2IsQ0FBQyxFQUFBOzt3QkFGRSxLQUFLLEdBQUcsU0FFVjt3QkFFRixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTs0QkFDZixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUM7NEJBQ3pDLHNCQUFPO3lCQUNWO3dCQUNELGdCQUFnQjt3QkFDaEIsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7d0JBQzNFLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO3dCQUNyRSxrRkFBa0Y7d0JBQ2xGLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsNDFCQUE0MUIsQ0FBQTt3QkFDMTRCLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO3dCQUNyRSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQzt3QkFDM0UsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7d0JBQ3ZFLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7O0tBQ2pDO0lBRUQsdUJBQUssR0FBTDtRQUFBLGlCQUlDO1FBSEcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDOztnQkFDekIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDOzs7YUFDaEIsRUFBRSxJQUFJLENBQUMsQ0FBQTtJQUNaLENBQUM7SUFFYSx1QkFBSyxHQUFuQjs7OztnQkFDUSxPQUFPLEdBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQzdFLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ1YsaUJBQU8sQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQzlCLHNCQUFPO2lCQUNWO2dCQUNELHdFQUF3RTtnQkFDeEUsc0JBQXNCO2dCQUN0QixTQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7O0tBRXBEOztJQXZFZ0IsT0FBTztRQUQzQixPQUFPO09BQ2EsT0FBTyxDQXlFM0I7SUFBRCxjQUFDO0NBekVELEFBeUVDLENBekVvQyxpQkFBUSxHQXlFNUM7a0JBekVvQixPQUFPIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgeyBVc2VySW5mbyB9IGZyb20gXCIuLi8uLi9Qcm90b2NvbC9idW5kbGVcIjtcbmltcG9ydCB7IEdhbWVTZXJ2ZXIsIEludm9rZUJlZ2luLCBMb2dpblJlcGx5LCBMb2dpblJlcXVlc3QgfSBmcm9tIFwiLi4vLi4vUHJvdG9jb2wvYnVuZGxlXCI7XG5pbXBvcnQgVUlMb2dpbl9BdXRvIGZyb20gXCIuLi9BdXRvU2NyaXB0cy9VSUxvZ2luX0F1dG9cIjtcbmltcG9ydCB7IFVJU2NyZWVuIH0gZnJvbSBcIi4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCB7IERhdGFTZWVEZXRhaWwgfSBmcm9tIFwiLi4vRGF0YU1vZGFsL0RhdGFGcmllbmRcIjtcbmltcG9ydCB7IERhdGFNb2RhbE1nciB9IGZyb20gXCIuLi9NYW5hZ2VyL0RhdGFNb2RhbE1nclwiO1xuaW1wb3J0IEZvcm1NZ3IgZnJvbSBcIi4uL01hbmFnZXIvRm9ybU1nclwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IHsgTmF0aXZlTWdyIH0gZnJvbSBcIi4uL01hbmFnZXIvTmF0aXZlTWdyXCI7XG5pbXBvcnQgVGlwc01nciBmcm9tIFwiLi4vTWFuYWdlci9UaXBzTWdyXCI7XG5pbXBvcnQgV2luZG93TWdyIGZyb20gXCIuLi9NYW5hZ2VyL1dpbmRvd01nclwiO1xuaW1wb3J0IEh0dHBIZWxwZXIgZnJvbSBcIi4uL05ldC9IdHRwSGVscGVyXCI7XG5pbXBvcnQgeyBhcGlDbGllbnQgfSBmcm9tIFwiLi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IFVJQ29uZmlnIGZyb20gXCIuLi9VSUNvbmZpZ1wiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuXG5pbXBvcnQgVUlUb2FzdCBmcm9tIFwiLi9VSVRvYXN0XCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSUxvZ2luIGV4dGVuZHMgVUlTY3JlZW4ge1xuXG4gICAgdmlldzogVUlMb2dpbl9BdXRvO1xuXG4gICAgYXN5bmMgb25Mb2FkKCkge1xuXG4gICAgICAgIGFwaUNsaWVudC5mbG93cy5wb3N0RGlzY29ubmVjdEZsb3cucHVzaCh2ID0+IHtcbiAgICAgICAgICAgIGlmICghdi5pc01hbnVhbCkge1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoYXN5bmMgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBsZXQgcmVzID0gYXdhaXQgYXBpQ2xpZW50LmNvbm5lY3QoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFyZXMuaXNTdWNjKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQuZXJyTXNnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSVNjb2tldFJlY29ubmVudCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHY7XG4gICAgICAgIH0pXG5cbiAgICAgICAgYXBpQ2xpZW50Lmxpc3Rlbk1zZyhcIldvcmtCZW5jaFwiLCAobXNnKSA9PiB7XG4gICAgICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoID0gbXNnLnVzZXJXb3JrQmVuY2g7XG4gICAgICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyQ3VyTmVlZEJ1eUNvaW5OdW0gPSBtc2cuYnV5TmVlZENvaW47XG4gICAgICAgIH0pXG5cblxuICAgICAgICBsZXQgcmVzdWx0ID0gYXdhaXQgYXBpQ2xpZW50LmNvbm5lY3QoKTtcbiAgICAgICAgLy8g6L+e5o6l5LiN5LiA5a6a5oiQ5Yqf77yI5L6L5aaC572R57uc6ZSZ6K+v77yJ77yM5omA5Lul6KaB6K6w5b6X6ZSZ6K+v5aSE55CGXG4gICAgICAgIGlmICghcmVzdWx0LmlzU3VjYykge1xuICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0LmVyck1zZyk7XG4gICAgICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlTY29rZXRSZWNvbm5lbnQpO1xuXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIHdlY2hhdENhbGwoY29kZTogc3RyaW5nKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiY29kZTpcIiArIGNvZGUpO1xuICAgICAgICBsZXQgZGF0YXMgPSBhd2FpdCBhcGlDbGllbnQuY2FsbEFwaShcIkxvZ2luXCIsIHtcbiAgICAgICAgICAgIGNvZGU6IGNvZGVcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKCFkYXRhcy5pc1N1Y2MpIHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSVNjb2tldFJlY29ubmVudCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgLy8gY29uc29sZS5sb2coKVxuICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyQWNjb3VudCA9IGRhdGFzLnJlcy51c2VySW5mby51c2VyQWNjb3VudDtcbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlck5hbWUgPSBkYXRhcy5yZXMudXNlckluZm8udXNlck5hbWU7XG4gICAgICAgIC8vIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJBdmF0YXJVcmwgPSBkYXRhcy5yZXMudXNlckluZm8udXNlckF2YXRhclVybDtcbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlckF2YXRhclVybCA9IFwiaHR0cHM6Ly9pbWFnZS5iYWlkdS5jb20vc2VhcmNoL2RldGFpbD9jdD01MDMzMTY0ODAmej0wJmlwbj1mYWxzZSZ3b3JkPSVFNSVBNCVCNCVFNSU4MyU4RiZocz0wJnBuPTQmc3BuPTAmZGk9NzE0Njg1NzIwMDA5MzIzMzE1MyZwaT0wJnJuPTEmdG49YmFpZHVpbWFnZWRldGFpbCZpcz0wJTJDMCZpZT11dGYtOCZvZT11dGYtOCZjbD0yJmxtPS0xJmNzPTMxNzIyMjY1MzUlMkMyNTg3NTA5MjgyJm9zPTEzOTE0NTE1MzQlMkMxMjU3MzM5NzAmc2ltaWQ9NTkxNDkzNzUlMkM1ODk2ODYwMjgmYWRwaWNpZD0wJmxwbj0wJmxuPTMwJmZyPWFsYSZmbT0mc21lPSZjZz1oZWFkJmJkdHlwZT0wJm9yaXF1ZXJ5PSVFNSVBNCVCNCVFNSU4MyU4RiZvYmp1cmw9aHR0cHMlM0ElMkYlMkZnaW1nMi5iYWlkdS5jb20lMkZpbWFnZV9zZWFyY2glMkZzcmMlM0RodHRwJTNBJTJGJTJGYy1zc2wuZHVpdGFuZy5jb20lMkZ1cGxvYWRzJTJGYmxvZyUyRjIwMjEwNyUyRjA5JTJGMjAyMTA3MDkxNDI0NTRfZGM4ZGMudGh1bWIuMTAwMF8wLmpwZWclMjZyZWZlciUzRGh0dHAlM0ElMkYlMkZjLXNzbC5kdWl0YW5nLmNvbSUyNmFwcCUzRDIwMDIlMjZzaXplJTNEZjk5OTklMkMxMDAwMCUyNnElM0RhODAlMjZuJTNEMCUyNmclM0QwbiUyNmZtdCUzRGF1dG8lM0ZzZWMlM0QxNjcwMzIxMzg4JTI2dCUzRDI3ZmFhMmMyNjg3YWI2MjI1MjlmYTYwZjU4Nzk1ZDdmJmZyb211cmw9aXBwcl96MkMlMjRxQXpkSDNGQXpkSDNGb29vX3olMjZlM0IxN3Rwd2cyX3olMjZlM0J2NTRBemRIM0ZrczUyQXpkSDNGJTNGdDElM0Q4bmMwMDBtOGFuJmdzbT0maXNsaXN0PSZxdWVyeWxpc3Q9JmR5VGFiU3RyPU1Dd3pMREVzTlN3eUxEY3NPQ3cyTERRc09RJTNEJTNEXCJcbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlckNvaW4gPSBkYXRhcy5yZXMudXNlckluZm8udXNlckNvaW47XG4gICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJEaWFtb25kID0gZGF0YXMucmVzLnVzZXJJbmZvLnVzZXJEaWFtb25kO1xuICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyTW9uZXkgPSBkYXRhcy5yZXMudXNlckluZm8udXNlck1vbmV5O1xuICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlIb21lKTtcbiAgICB9XG5cbiAgICBzdGFydCgpIHtcbiAgICAgICAgdGhpcy52aWV3LkJ0bldlY2hhdC5hZGRDbGljayhhc3luYyAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmxvZ2luKCk7XG4gICAgICAgIH0sIHRoaXMpXG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBsb2dpbigpIHtcbiAgICAgICAgbGV0IGlzQWdhcm46IGJvb2xlYW4gPSB0aGlzLnZpZXcuVXNlckFnYXJuLmdldENvbXBvbmVudChjYy5Ub2dnbGUpLmlzQ2hlY2tlZDtcbiAgICAgICAgaWYgKCFpc0FnYXJuKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKFwi6K+36ZiF6K+75bm25ZCM5oSP44CK55So5oi35Y2P6K6u44CLXCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIC8vIE5hdGl2ZU1nci5jYWxsTmF0aXZlQ2xhc3MoXCJBcHBBY3Rpdml0eVwiLCBcImxvZ2luV1hcIiwgVUlMb2dpbi50ZXN0UnBjKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCLosIPnlKjlrozmiJBcIilcbiAgICAgICAgVUlMb2dpbi53ZWNoYXRDYWxsKHRoaXMudmlldy50ZXN0QWNjb3VudC5zdHJpbmcpO1xuXG4gICAgfVxuXG59Il19