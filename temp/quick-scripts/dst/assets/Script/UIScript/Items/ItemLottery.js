
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Items/ItemLottery.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '33722j4CsdN26lwVVhV89fi', 'ItemLottery');
// Script/UIScript/Items/ItemLottery.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemLottery = /** @class */ (function (_super) {
    __extends(ItemLottery, _super);
    function ItemLottery() {
        // @property(cc.Label)
        // num: cc.Label = null;
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.icon = null;
        return _this;
    }
    ItemLottery.prototype.setInfo = function (data) {
        // this.num.string = data.num;
        CocosHelper_1.default.setLotteryIcon(data.icon, this.icon);
    };
    __decorate([
        property(cc.Sprite)
    ], ItemLottery.prototype, "icon", void 0);
    ItemLottery = __decorate([
        ccclass
    ], ItemLottery);
    return ItemLottery;
}(cc.Component));
exports.default = ItemLottery;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvSXRlbXMvSXRlbUxvdHRlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsdURBQWtEO0FBRTVDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXlDLCtCQUFZO0lBQXJEO1FBRUksc0JBQXNCO1FBQ3RCLHdCQUF3QjtRQUg1QixxRUFZQztRQU5HLFVBQUksR0FBYyxJQUFJLENBQUM7O0lBTTNCLENBQUM7SUFKRyw2QkFBTyxHQUFQLFVBQVEsSUFBaUI7UUFDckIsOEJBQThCO1FBQzlCLHFCQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFMRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzZDQUNHO0lBTk4sV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQVkvQjtJQUFELGtCQUFDO0NBWkQsQUFZQyxDQVp3QyxFQUFFLENBQUMsU0FBUyxHQVlwRDtrQkFab0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElpZW1Mb3R0ZXJ5IH0gZnJvbSBcIi4uLy4uL01hbmFnZXIvSW50ZXJmYWNlTWdyXCI7XG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJdGVtTG90dGVyeSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICAvLyBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgLy8gbnVtOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICAgIGljb246IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgICBzZXRJbmZvKGRhdGE6IElpZW1Mb3R0ZXJ5KSB7XG4gICAgICAgIC8vIHRoaXMubnVtLnN0cmluZyA9IGRhdGEubnVtO1xuICAgICAgICBDb2Nvc0hlbHBlci5zZXRMb3R0ZXJ5SWNvbihkYXRhLmljb24sIHRoaXMuaWNvbik7XG4gICAgfVxufVxuIl19