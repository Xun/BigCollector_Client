
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Items/ItemProduce.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '833b9VVoiRBu4HdjfWzaMYu', 'ItemProduce');
// Script/UIScript/Items/ItemProduce.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemProduce = /** @class */ (function (_super) {
    __extends(ItemProduce, _super);
    function ItemProduce() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.nodeIcon = null;
        _this.nodeBg = null;
        _this.nodeLevel = null;
        _this.txtLevel = null;
        _this.txtCoin = null;
        _this.isDrag = false;
        _this.isUsed = false;
        _this._itemId = 0;
        _this._index = -1;
        _this._itemType = -1;
        _this._nextLevel = -1;
        _this._itemInfo = null;
        return _this;
    }
    // onLoad () {}
    ItemProduce.prototype.start = function () {
    };
    /**
     * 设置工作台物品信息
     * @param {Number} index
     * @param {Number} itemId
     */
    ItemProduce.prototype.setWorkbenchItemInfo = function (index, itemId) {
        var _this = this;
        //设置信息
        // console.log("index:" + index);
        // console.log("itemId:" + itemId);
        this._index = index;
        this._itemId = itemId;
        this._itemType = 1;
        this.isUsed = false;
        this.txtCoin.node.active = true;
        if (this._itemId) {
            this.nodeIcon.active = false;
            this._itemInfo = CocosHelper_1.default.getItemProduceInfoById(this._itemId);
            CocosHelper_1.default.setItemProduceIcon(this._itemId, this.nodeIcon.getComponent(cc.Sprite), function () {
                _this.nodeIcon.active = true;
                _this.txtLevel.string = _this._itemInfo.id.toString();
                _this.nodeLevel.active = true;
                _this.playProduceAni(true);
            });
        }
        else {
            this.nodeIcon.active = false;
            this.nodeLevel.active = false;
            this.txtCoin.node.active = false;
        }
        if (this.isUsed) {
            this.nodeIcon.opacity = 150;
        }
        else if (!this.isDrag) {
            this.nodeIcon.opacity = 255;
        }
    };
    ItemProduce.prototype.dragStart = function () {
        if (this.isUsed || !this._itemId) { //正在使用中，不可拖拽
            return false;
        }
        this.isDrag = true;
        this.nodeIcon.opacity = 150;
        return true;
    };
    ItemProduce.prototype.dragOver = function () {
        if (this.isDrag) {
            this.isDrag = false;
            this.nodeIcon.opacity = 255;
        }
    };
    ItemProduce.prototype.getInfo = function () {
        return this._itemId;
    };
    ItemProduce.prototype.showVirtual = function () {
        this.nodeBg.active = false;
    };
    ItemProduce.prototype.playProduceAni = function (isShow) {
        var _this = this;
        if (isShow) {
            this.schedule(function () {
                CocosHelper_1.default.runTweenSync(_this.txtCoin.node, cc.tween().to(1, { y: 110 }).to(1, { y: 20 }));
                CocosHelper_1.default.runTweenSync(_this.nodeIcon, cc.tween().to(0.5, { scale: 1.1 }).to(0.2, { scale: 1 }));
            }, 5);
            //播放动画
        }
        else {
            this.unscheduleAllCallbacks();
            //隐藏动画
        }
    };
    __decorate([
        property(cc.Node)
    ], ItemProduce.prototype, "nodeIcon", void 0);
    __decorate([
        property(cc.Node)
    ], ItemProduce.prototype, "nodeBg", void 0);
    __decorate([
        property(cc.Node)
    ], ItemProduce.prototype, "nodeLevel", void 0);
    __decorate([
        property(cc.Label)
    ], ItemProduce.prototype, "txtLevel", void 0);
    __decorate([
        property(cc.Label)
    ], ItemProduce.prototype, "txtCoin", void 0);
    ItemProduce = __decorate([
        ccclass
    ], ItemProduce);
    return ItemProduce;
}(cc.Component));
exports.default = ItemProduce;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvSXRlbXMvSXRlbVByb2R1Y2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsdURBQWtEO0FBRTVDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXlDLCtCQUFZO0lBQXJEO1FBQUEscUVBc0dDO1FBbkdHLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRzFCLGNBQVEsR0FBYSxJQUFJLENBQUM7UUFHMUIsYUFBTyxHQUFhLElBQUksQ0FBQztRQUVsQixZQUFNLEdBQVksS0FBSyxDQUFDO1FBQ3hCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFDeEIsYUFBTyxHQUFXLENBQUMsQ0FBQztRQUNwQixZQUFNLEdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDcEIsZUFBUyxHQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDeEIsZUFBUyxHQUFRLElBQUksQ0FBQzs7SUErRWpDLENBQUM7SUE5RUcsZUFBZTtJQUVmLDJCQUFLLEdBQUw7SUFFQSxDQUFDO0lBQ0Q7Ozs7T0FJRztJQUNILDBDQUFvQixHQUFwQixVQUFxQixLQUFhLEVBQUUsTUFBYztRQUFsRCxpQkE2QkM7UUE1QkcsTUFBTTtRQUNOLGlDQUFpQztRQUNqQyxtQ0FBbUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQkFBVyxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNsRSxxQkFBVyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNoRixLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNwRCxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQzdCLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ3BDO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1NBQy9CO2FBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1NBQy9CO0lBQ0wsQ0FBQztJQUVELCtCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsWUFBWTtZQUM1QyxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUM1QixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsOEJBQVEsR0FBUjtRQUNJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztTQUMvQjtJQUNMLENBQUM7SUFFRCw2QkFBTyxHQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxpQ0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQy9CLENBQUM7SUFFRCxvQ0FBYyxHQUFkLFVBQWUsTUFBTTtRQUFyQixpQkFXQztRQVZHLElBQUksTUFBTSxFQUFFO1lBQ1IsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDVixxQkFBVyxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO2dCQUMxRixxQkFBVyxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7WUFDckcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1lBQ0wsTUFBTTtTQUNUO2FBQU07WUFDSCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUM5QixNQUFNO1NBQ1Q7SUFDTCxDQUFDO0lBbEdEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrQ0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNRO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aURBQ087SUFHMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztnREFDTTtJQWZSLFdBQVc7UUFEL0IsT0FBTztPQUNhLFdBQVcsQ0FzRy9CO0lBQUQsa0JBQUM7Q0F0R0QsQUFzR0MsQ0F0R3dDLEVBQUUsQ0FBQyxTQUFTLEdBc0dwRDtrQkF0R29CLFdBQVciLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJdGVtUHJvZHVjZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBub2RlSWNvbjogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBub2RlQmc6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgbm9kZUxldmVsOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB0eHRMZXZlbDogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHR4dENvaW46IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIHB1YmxpYyBpc0RyYWc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBwdWJsaWMgaXNVc2VkOiBib29sZWFuID0gZmFsc2U7XG4gICAgcHVibGljIF9pdGVtSWQ6IG51bWJlciA9IDA7XG4gICAgcHVibGljIF9pbmRleDogbnVtYmVyID0gLTE7XG4gICAgcHVibGljIF9pdGVtVHlwZTogbnVtYmVyID0gLTE7XG4gICAgcHVibGljIF9uZXh0TGV2ZWw6IG51bWJlciA9IC0xO1xuICAgIHB1YmxpYyBfaXRlbUluZm86IGFueSA9IG51bGw7XG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCgpIHtcblxuICAgIH1cbiAgICAvKipcbiAgICAgKiDorr7nva7lt6XkvZzlj7Dnianlk4Hkv6Hmga9cbiAgICAgKiBAcGFyYW0ge051bWJlcn0gaW5kZXggXG4gICAgICogQHBhcmFtIHtOdW1iZXJ9IGl0ZW1JZCBcbiAgICAgKi9cbiAgICBzZXRXb3JrYmVuY2hJdGVtSW5mbyhpbmRleDogbnVtYmVyLCBpdGVtSWQ6IG51bWJlcikge1xuICAgICAgICAvL+iuvue9ruS/oeaBr1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhcImluZGV4OlwiICsgaW5kZXgpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhcIml0ZW1JZDpcIiArIGl0ZW1JZCk7XG4gICAgICAgIHRoaXMuX2luZGV4ID0gaW5kZXg7XG4gICAgICAgIHRoaXMuX2l0ZW1JZCA9IGl0ZW1JZDtcbiAgICAgICAgdGhpcy5faXRlbVR5cGUgPSAxO1xuICAgICAgICB0aGlzLmlzVXNlZCA9IGZhbHNlO1xuICAgICAgICB0aGlzLnR4dENvaW4ubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICBpZiAodGhpcy5faXRlbUlkKSB7XG4gICAgICAgICAgICB0aGlzLm5vZGVJY29uLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5faXRlbUluZm8gPSBDb2Nvc0hlbHBlci5nZXRJdGVtUHJvZHVjZUluZm9CeUlkKHRoaXMuX2l0ZW1JZCk7XG4gICAgICAgICAgICBDb2Nvc0hlbHBlci5zZXRJdGVtUHJvZHVjZUljb24odGhpcy5faXRlbUlkLCB0aGlzLm5vZGVJY29uLmdldENvbXBvbmVudChjYy5TcHJpdGUpLCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlSWNvbi5hY3RpdmUgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHRoaXMudHh0TGV2ZWwuc3RyaW5nID0gdGhpcy5faXRlbUluZm8uaWQudG9TdHJpbmcoKTtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGVMZXZlbC5hY3RpdmUgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHRoaXMucGxheVByb2R1Y2VBbmkodHJ1ZSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubm9kZUljb24uYWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLm5vZGVMZXZlbC5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMudHh0Q29pbi5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuaXNVc2VkKSB7XG4gICAgICAgICAgICB0aGlzLm5vZGVJY29uLm9wYWNpdHkgPSAxNTA7XG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuaXNEcmFnKSB7XG4gICAgICAgICAgICB0aGlzLm5vZGVJY29uLm9wYWNpdHkgPSAyNTU7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBkcmFnU3RhcnQoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzVXNlZCB8fCAhdGhpcy5faXRlbUlkKSB7IC8v5q2j5Zyo5L2/55So5Lit77yM5LiN5Y+v5ouW5ou9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmlzRHJhZyA9IHRydWU7XG4gICAgICAgIHRoaXMubm9kZUljb24ub3BhY2l0eSA9IDE1MDtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgZHJhZ092ZXIoKSB7XG4gICAgICAgIGlmICh0aGlzLmlzRHJhZykge1xuICAgICAgICAgICAgdGhpcy5pc0RyYWcgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMubm9kZUljb24ub3BhY2l0eSA9IDI1NTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldEluZm8oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pdGVtSWQ7XG4gICAgfVxuXG4gICAgc2hvd1ZpcnR1YWwoKSB7XG4gICAgICAgIHRoaXMubm9kZUJnLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIHBsYXlQcm9kdWNlQW5pKGlzU2hvdykge1xuICAgICAgICBpZiAoaXNTaG93KSB7XG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlKCgpID0+IHtcbiAgICAgICAgICAgICAgICBDb2Nvc0hlbHBlci5ydW5Ud2VlblN5bmModGhpcy50eHRDb2luLm5vZGUsIGNjLnR3ZWVuKCkudG8oMSwgeyB5OiAxMTAgfSkudG8oMSwgeyB5OiAyMCB9KSlcbiAgICAgICAgICAgICAgICBDb2Nvc0hlbHBlci5ydW5Ud2VlblN5bmModGhpcy5ub2RlSWNvbiwgY2MudHdlZW4oKS50bygwLjUsIHsgc2NhbGU6IDEuMSB9KS50bygwLjIsIHsgc2NhbGU6IDEgfSkpXG4gICAgICAgICAgICB9LCA1KVxuICAgICAgICAgICAgLy/mkq3mlL7liqjnlLtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xuICAgICAgICAgICAgLy/pmpDol4/liqjnlLtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==