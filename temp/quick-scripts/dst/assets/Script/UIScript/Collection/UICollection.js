
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Collection/UICollection.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'dda071AclpG/LHrHJaGISlC', 'UICollection');
// Script/UIScript/Collection/UICollection.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UICollection = /** @class */ (function (_super) {
    __extends(UICollection, _super);
    function UICollection() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UICollection.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () { _this.closeSelf(); }, this);
        this.view.WenFangSiBaoProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curWFSBProgress / 4;
        this.view.WenFangSiBaoProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curWFSBProgress + "/4";
        this.view.WenFangSiBaoGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.WFSBGetMoney.toString();
        this.view.SiJunZiProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curSJZProgress / 4;
        this.view.SiJunZiProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curSJZProgress + "/4";
        this.view.SiJunZiGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.SJZGetMoney.toString();
        this.view.MingZhuProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curSDMZProgress / 4;
        this.view.MingZhuProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curSDMZProgress + "/4";
        this.view.MingZhuGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.SDMZGetMoney.toString();
        this.view.ShenShouProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curSDSSProgress / 4;
        this.view.ShenShouProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curSDSSProgress + "/4";
        this.view.ShenShouGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.SDSSGetMoney.toString();
        this.view.BaDaJiaProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curBDJProgress / 8;
        this.view.BaDaJiaProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curBDJProgress + "/8";
        this.view.BaDaJiaGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.BDJGetMoney.toString();
        this.view.JinChanProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curJCProgress / 1;
        this.view.JinChanProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curJCProgress + "/1";
        this.view.JinChanGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.JCDescribe;
        this.view.PiXiuProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curPXProgress / 1;
        this.view.PiXiuProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curPXProgress + "/1";
        this.view.PiXiuGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.JCDescribe;
        this.view.JinLongProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curJLProgress / 1;
        this.view.JinLongProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curJLProgress + "/1";
        this.view.JinLongGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.JLDescribe;
    };
    UICollection = __decorate([
        ccclass
    ], UICollection);
    return UICollection;
}(UIForm_1.UIWindow));
exports.default = UICollection;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvQ29sbGVjdGlvbi9VSUNvbGxlY3Rpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsOENBQStDO0FBRS9DLGlEQUE0QztBQUV0QyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUEwQyxnQ0FBUTtJQUFsRDs7SUF3Q0EsQ0FBQztJQXBDRyw0QkFBSyxHQUFMO1FBQUEsaUJBaUNDO1FBaENHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxjQUFRLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDO1FBQy9ILElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3RHLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFeEcsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7UUFDekgsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDaEcsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUVsRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQztRQUMxSCxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNqRyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRW5HLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUM7UUFDM0gsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDbEcsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUVwRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztRQUN6SCxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUNoRyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRWxHLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ3hILElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQy9GLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7UUFFdEYsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDdEgsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDN0YsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQztRQUVwRixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN4SCxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUMvRixJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDO0lBQzFGLENBQUM7SUFyQ2dCLFlBQVk7UUFEaEMsT0FBTztPQUNhLFlBQVksQ0F3Q2hDO0lBQUQsbUJBQUM7Q0F4Q0QsQUF3Q0MsQ0F4Q3lDLGlCQUFRLEdBd0NqRDtrQkF4Q29CLFlBQVkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVUlDb2xsZWN0aW9uX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJQ29sbGVjdGlvbl9BdXRvXCI7XG5pbXBvcnQgeyBVSVdpbmRvdyB9IGZyb20gXCIuLi8uLi9Db21tb24vVUlGb3JtXCI7XG5pbXBvcnQgeyBEYXRhTW9kYWxNZ3IgfSBmcm9tIFwiLi4vLi4vTWFuYWdlci9EYXRhTW9kYWxNZ3JcIjtcbmltcG9ydCBHYW1lTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL0dhbWVNZ3JcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJQ29sbGVjdGlvbiBleHRlbmRzIFVJV2luZG93IHtcblxuICAgIHZpZXc6IFVJQ29sbGVjdGlvbl9BdXRvXG5cbiAgICBzdGFydCgpIHtcbiAgICAgICAgdGhpcy52aWV3LkNsb3NlQnRuLmFkZENsaWNrKCgpID0+IHsgdGhpcy5jbG9zZVNlbGYoKSB9LCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LldlbkZhbmdTaUJhb1Byb2dyZXNzLmdldENvbXBvbmVudChjYy5Qcm9ncmVzc0JhcikucHJvZ3Jlc3MgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Db2xsZWN0aW9uSW5mby5jdXJXRlNCUHJvZ3Jlc3MgLyA0O1xuICAgICAgICB0aGlzLnZpZXcuV2VuRmFuZ1NpQmFvUHJvZ3Jlc3NMYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ29sbGVjdGlvbkluZm8uY3VyV0ZTQlByb2dyZXNzICsgXCIvNFwiO1xuICAgICAgICB0aGlzLnZpZXcuV2VuRmFuZ1NpQmFvR2V0TnVtVGlwTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNvbGxlY3Rpb25JbmZvLldGU0JHZXRNb25leS50b1N0cmluZygpO1xuXG4gICAgICAgIHRoaXMudmlldy5TaUp1blppUHJvZ3Jlc3MuZ2V0Q29tcG9uZW50KGNjLlByb2dyZXNzQmFyKS5wcm9ncmVzcyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNvbGxlY3Rpb25JbmZvLmN1clNKWlByb2dyZXNzIC8gNDtcbiAgICAgICAgdGhpcy52aWV3LlNpSnVuWmlQcm9ncmVzc0xhYi5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Db2xsZWN0aW9uSW5mby5jdXJTSlpQcm9ncmVzcyArIFwiLzRcIjtcbiAgICAgICAgdGhpcy52aWV3LlNpSnVuWmlHZXROdW1UaXBMYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ29sbGVjdGlvbkluZm8uU0paR2V0TW9uZXkudG9TdHJpbmcoKTtcblxuICAgICAgICB0aGlzLnZpZXcuTWluZ1podVByb2dyZXNzLmdldENvbXBvbmVudChjYy5Qcm9ncmVzc0JhcikucHJvZ3Jlc3MgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Db2xsZWN0aW9uSW5mby5jdXJTRE1aUHJvZ3Jlc3MgLyA0O1xuICAgICAgICB0aGlzLnZpZXcuTWluZ1podVByb2dyZXNzTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNvbGxlY3Rpb25JbmZvLmN1clNETVpQcm9ncmVzcyArIFwiLzRcIjtcbiAgICAgICAgdGhpcy52aWV3Lk1pbmdaaHVHZXROdW1UaXBMYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ29sbGVjdGlvbkluZm8uU0RNWkdldE1vbmV5LnRvU3RyaW5nKCk7XG5cbiAgICAgICAgdGhpcy52aWV3LlNoZW5TaG91UHJvZ3Jlc3MuZ2V0Q29tcG9uZW50KGNjLlByb2dyZXNzQmFyKS5wcm9ncmVzcyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNvbGxlY3Rpb25JbmZvLmN1clNEU1NQcm9ncmVzcyAvIDQ7XG4gICAgICAgIHRoaXMudmlldy5TaGVuU2hvdVByb2dyZXNzTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNvbGxlY3Rpb25JbmZvLmN1clNEU1NQcm9ncmVzcyArIFwiLzRcIjtcbiAgICAgICAgdGhpcy52aWV3LlNoZW5TaG91R2V0TnVtVGlwTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNvbGxlY3Rpb25JbmZvLlNEU1NHZXRNb25leS50b1N0cmluZygpO1xuXG4gICAgICAgIHRoaXMudmlldy5CYURhSmlhUHJvZ3Jlc3MuZ2V0Q29tcG9uZW50KGNjLlByb2dyZXNzQmFyKS5wcm9ncmVzcyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNvbGxlY3Rpb25JbmZvLmN1ckJESlByb2dyZXNzIC8gODtcbiAgICAgICAgdGhpcy52aWV3LkJhRGFKaWFQcm9ncmVzc0xhYi5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Db2xsZWN0aW9uSW5mby5jdXJCREpQcm9ncmVzcyArIFwiLzhcIjtcbiAgICAgICAgdGhpcy52aWV3LkJhRGFKaWFHZXROdW1UaXBMYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ29sbGVjdGlvbkluZm8uQkRKR2V0TW9uZXkudG9TdHJpbmcoKTtcblxuICAgICAgICB0aGlzLnZpZXcuSmluQ2hhblByb2dyZXNzLmdldENvbXBvbmVudChjYy5Qcm9ncmVzc0JhcikucHJvZ3Jlc3MgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Db2xsZWN0aW9uSW5mby5jdXJKQ1Byb2dyZXNzIC8gMTtcbiAgICAgICAgdGhpcy52aWV3LkppbkNoYW5Qcm9ncmVzc0xhYi5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Db2xsZWN0aW9uSW5mby5jdXJKQ1Byb2dyZXNzICsgXCIvMVwiO1xuICAgICAgICB0aGlzLnZpZXcuSmluQ2hhbkdldE51bVRpcExhYi5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Db2xsZWN0aW9uSW5mby5KQ0Rlc2NyaWJlO1xuXG4gICAgICAgIHRoaXMudmlldy5QaVhpdVByb2dyZXNzLmdldENvbXBvbmVudChjYy5Qcm9ncmVzc0JhcikucHJvZ3Jlc3MgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Db2xsZWN0aW9uSW5mby5jdXJQWFByb2dyZXNzIC8gMTtcbiAgICAgICAgdGhpcy52aWV3LlBpWGl1UHJvZ3Jlc3NMYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ29sbGVjdGlvbkluZm8uY3VyUFhQcm9ncmVzcyArIFwiLzFcIjtcbiAgICAgICAgdGhpcy52aWV3LlBpWGl1R2V0TnVtVGlwTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNvbGxlY3Rpb25JbmZvLkpDRGVzY3JpYmU7XG5cbiAgICAgICAgdGhpcy52aWV3LkppbkxvbmdQcm9ncmVzcy5nZXRDb21wb25lbnQoY2MuUHJvZ3Jlc3NCYXIpLnByb2dyZXNzID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ29sbGVjdGlvbkluZm8uY3VySkxQcm9ncmVzcyAvIDE7XG4gICAgICAgIHRoaXMudmlldy5KaW5Mb25nUHJvZ3Jlc3NMYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ29sbGVjdGlvbkluZm8uY3VySkxQcm9ncmVzcyArIFwiLzFcIjtcbiAgICAgICAgdGhpcy52aWV3LkppbkxvbmdHZXROdW1UaXBMYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ29sbGVjdGlvbkluZm8uSkxEZXNjcmliZTtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxufVxuIl19