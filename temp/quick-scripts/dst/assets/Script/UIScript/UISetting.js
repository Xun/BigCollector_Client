
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/UISetting.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6794aDGudpOn7XTM7oDTUsV', 'UISetting');
// Script/UIScript/UISetting.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../Common/UIForm");
var SoundMgr_1 = require("../Manager/SoundMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UISetting = /** @class */ (function (_super) {
    __extends(UISetting, _super);
    function UISetting() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UISetting.prototype.onLoad = function () {
        var volume = SoundMgr_1.default.inst.getVolume();
        if (volume != null && volume.effectVolume == 0 && volume.musicVolume == 0) {
            this.view.offNode.active = true;
            this.view.onNode.active = false;
        }
        else {
            this.view.offNode.active = false;
            this.view.onNode.active = true;
        }
    };
    UISetting.prototype.start = function () {
        var _this = this;
        this.view.BtnClose.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.BtnMusic.addClick(function () {
            _this.view.offNode.active = !_this.view.offNode.active;
            _this.view.onNode.active = !_this.view.onNode.active;
            SoundMgr_1.default.inst.setEffectVolume(_this.view.onNode.active ? 1 : 0);
            SoundMgr_1.default.inst.setMusicVolume(_this.view.onNode.active ? 1 : 0);
        }, this);
    };
    UISetting = __decorate([
        ccclass
    ], UISetting);
    return UISetting;
}(UIForm_1.UIWindow));
exports.default = UISetting;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvVUlTZXR0aW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR2xGLDJDQUE0QztBQUM1QyxnREFBMkM7QUFFckMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBdUMsNkJBQVE7SUFBL0M7O0lBNEJBLENBQUM7SUF4QkcsMEJBQU0sR0FBTjtRQUNJLElBQUksTUFBTSxHQUFHLGtCQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3ZDLElBQUksTUFBTSxJQUFJLElBQUksSUFBSSxNQUFNLENBQUMsWUFBWSxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsV0FBVyxJQUFJLENBQUMsRUFBRTtZQUN2RSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDbkM7YUFBTTtZQUNILElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztTQUNsQztJQUNMLENBQUM7SUFFRCx5QkFBSyxHQUFMO1FBQUEsaUJBV0M7UUFWRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDeEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUN4QixLQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7WUFDckQsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBQ25ELGtCQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0Qsa0JBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNsRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBMUJnQixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBNEI3QjtJQUFELGdCQUFDO0NBNUJELEFBNEJDLENBNUJzQyxpQkFBUSxHQTRCOUM7a0JBNUJvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBVSVNldHRpbmdfQXV0byBmcm9tIFwiLi4vQXV0b1NjcmlwdHMvVUlTZXR0aW5nX0F1dG9cIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCBTb3VuZE1nciBmcm9tIFwiLi4vTWFuYWdlci9Tb3VuZE1nclwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlTZXR0aW5nIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlTZXR0aW5nX0F1dG87XG5cbiAgICBvbkxvYWQoKSB7XG4gICAgICAgIGxldCB2b2x1bWUgPSBTb3VuZE1nci5pbnN0LmdldFZvbHVtZSgpO1xuICAgICAgICBpZiAodm9sdW1lICE9IG51bGwgJiYgdm9sdW1lLmVmZmVjdFZvbHVtZSA9PSAwICYmIHZvbHVtZS5tdXNpY1ZvbHVtZSA9PSAwKSB7XG4gICAgICAgICAgICB0aGlzLnZpZXcub2ZmTm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy52aWV3Lm9uTm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMudmlldy5vZmZOb2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy52aWV3Lm9uTm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5CdG5DbG9zZS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTXVzaWMuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy52aWV3Lm9mZk5vZGUuYWN0aXZlID0gIXRoaXMudmlldy5vZmZOb2RlLmFjdGl2ZTtcbiAgICAgICAgICAgIHRoaXMudmlldy5vbk5vZGUuYWN0aXZlID0gIXRoaXMudmlldy5vbk5vZGUuYWN0aXZlO1xuICAgICAgICAgICAgU291bmRNZ3IuaW5zdC5zZXRFZmZlY3RWb2x1bWUodGhpcy52aWV3Lm9uTm9kZS5hY3RpdmUgPyAxIDogMCk7XG4gICAgICAgICAgICBTb3VuZE1nci5pbnN0LnNldE11c2ljVm9sdW1lKHRoaXMudmlldy5vbk5vZGUuYWN0aXZlID8gMSA6IDApO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICB9XG5cbn1cbiJdfQ==