
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/UIDialog.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fb4a5ZfY11CILd1ph9+IuFV', 'UIDialog');
// Script/UIScript/UIDialog.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Struct_1 = require("../Common/Struct");
var SysDefine_1 = require("../Common/SysDefine");
var UIForm_1 = require("../Common/UIForm");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDialog = /** @class */ (function (_super) {
    __extends(UIDialog, _super);
    function UIDialog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.modalType = new Struct_1.ModalType(SysDefine_1.ModalOpacity.OpacityHalf, true);
        return _this;
    }
    UIDialog.prototype.onLoad = function () {
        var _this = this;
        this.view.Sure.addClick(function () {
            _this.closeSelf();
        }, this);
    };
    UIDialog.prototype.onShow = function (str) {
        this.view.Msg.string = str;
    };
    UIDialog = __decorate([
        ccclass
    ], UIDialog);
    return UIDialog;
}(UIForm_1.UIWindow));
exports.default = UIDialog;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvVUlEaWFsb2cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsMkNBQTZDO0FBQzdDLGlEQUFtRDtBQUNuRCwyQ0FBNEM7QUFHdEMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBc0MsNEJBQVE7SUFBOUM7UUFBQSxxRUFnQkM7UUFkRyxlQUFTLEdBQUcsSUFBSSxrQkFBUyxDQUFDLHdCQUFZLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDOztJQWM5RCxDQUFDO0lBVkcseUJBQU0sR0FBTjtRQUFBLGlCQUlDO1FBSEcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7SUFDWixDQUFDO0lBRUQseUJBQU0sR0FBTixVQUFPLEdBQVc7UUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO0lBQy9CLENBQUM7SUFkZ0IsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQWdCNUI7SUFBRCxlQUFDO0NBaEJELEFBZ0JDLENBaEJxQyxpQkFBUSxHQWdCN0M7a0JBaEJvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgVUlEaWFsb2dfQXV0byBmcm9tIFwiLi4vQXV0b1NjcmlwdHMvVUlEaWFsb2dfQXV0b1wiO1xuaW1wb3J0IHsgTW9kYWxUeXBlIH0gZnJvbSBcIi4uL0NvbW1vbi9TdHJ1Y3RcIjtcbmltcG9ydCB7IE1vZGFsT3BhY2l0eSB9IGZyb20gXCIuLi9Db21tb24vU3lzRGVmaW5lXCI7XG5pbXBvcnQgeyBVSVdpbmRvdyB9IGZyb20gXCIuLi9Db21tb24vVUlGb3JtXCI7XG5cblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJRGlhbG9nIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgbW9kYWxUeXBlID0gbmV3IE1vZGFsVHlwZShNb2RhbE9wYWNpdHkuT3BhY2l0eUhhbGYsIHRydWUpO1xuXG4gICAgdmlldzogVUlEaWFsb2dfQXV0bztcblxuICAgIG9uTG9hZCgpIHtcbiAgICAgICAgdGhpcy52aWV3LlN1cmUuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcylcbiAgICB9XG5cbiAgICBvblNob3coc3RyOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy52aWV3Lk1zZy5zdHJpbmcgPSBzdHI7XG4gICAgfVxuXG59XG4iXX0=