
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Share/UIShare.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fcc2aY0G6VMe58TO/xv8+KX', 'UIShare');
// Script/UIScript/Share/UIShare.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIShare = /** @class */ (function (_super) {
    __extends(UIShare, _super);
    function UIShare() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.camereCapture = null;
        _this.texture2 = null;
        _this._width = 0;
        _this._height = 0;
        return _this;
    }
    UIShare.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.PhotoBtn.addClick(this.onCapture, this);
    };
    UIShare.prototype.onCapture = function () {
        var node = this.screenShot(this.view.contentNode);
        node.x = 0;
        node.y = 0;
        node.parent = cc.director.getScene();
        this.captureAction(node, node.width, node.height);
    };
    /**
     * 截图
     * @param targetNode  截图目标节点，如果为null则表示截全屏
     * @returns 返回截屏图片的node
     */
    UIShare.prototype.screenShot = function (targetNode) {
        if (targetNode === void 0) { targetNode = null; }
        //创建新的texture
        var texture = new cc.RenderTexture();
        texture.initWithSize(cc.winSize.width, cc.winSize.height, cc.game._renderContext.STENCIL_INDEX8);
        //创建新的spriteFrame
        var spriteFrame = new cc.SpriteFrame();
        var nodeX = cc.winSize.width / 2 + targetNode.x - targetNode.width / 2;
        var nodeY = cc.winSize.height / 2 + targetNode.y - targetNode.height / 2;
        var nodeWidth = targetNode.width;
        var nodeHeight = targetNode.height;
        if (targetNode == null) {
            spriteFrame.setTexture(texture);
        }
        else {
            //只显示node部分的图片
            spriteFrame.setTexture(texture, new cc.Rect(nodeX, nodeY, nodeWidth, nodeHeight));
        }
        //创建新的node
        var node = new cc.Node();
        var sprite = node.addComponent(cc.Sprite);
        sprite.spriteFrame = spriteFrame;
        //截图是反的，这里将截图scaleY取反，这样就是正的了
        // sprite.node.scaleY = - Math.abs(sprite.node.scaleY);
        sprite.spriteFrame.setFlipY(true);
        // let buffer = new ArrayBuffer(targetNode.width * targetNode.height * 4);
        // let data = new Uint8Array(buffer);
        // texture.readPixels(data, nodeX, nodeY, nodeWidth, nodeHeight);
        //手动渲染camera
        // camera.cullingMask = 0xffffffff;
        this.camereCapture.targetTexture = texture;
        this.camereCapture.render();
        this.camereCapture.targetTexture = null;
        return node;
    };
    UIShare.prototype.captureAction = function (capture, width, height) {
        var scaleAction = cc.scaleTo(1, 0.5);
        var targetPos = cc.v2(this.node.width / 2 + width / 2 - 50, height + height / 2);
        var moveAction = cc.moveTo(1, targetPos);
        var spawn = cc.spawn(scaleAction, moveAction);
        capture.runAction(spawn);
        var blinkAction = cc.blink(0.1, 1);
        // scene action
        this.node.runAction(blinkAction);
    };
    UIShare.prototype.saveFile = function (picData) {
        if (CC_JSB) {
            // let filePath = jsb.fileUtils.getWritablePath() + 'render_to_sprite_image.png';
            // let success = jsb.saveImageData(picData, this._width, this._height, filePath)
            // if (success) {
            //     cc.log("save image data success, file: " + filePath);
            // }
            // else {
            //     cc.error("save image data failed!");
            // }
        }
    };
    __decorate([
        property(cc.Camera)
    ], UIShare.prototype, "camereCapture", void 0);
    UIShare = __decorate([
        ccclass
    ], UIShare);
    return UIShare;
}(UIForm_1.UIWindow));
exports.default = UIShare;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hhcmUvVUlTaGFyZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdsRiw4Q0FBK0M7QUFHekMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBcUMsMkJBQVE7SUFBN0M7UUFBQSxxRUEyRkM7UUF4RkcsbUJBQWEsR0FBYyxJQUFJLENBQUM7UUFFaEMsY0FBUSxHQUFxQixJQUFJLENBQUM7UUFDbEMsWUFBTSxHQUFXLENBQUMsQ0FBQztRQUNuQixhQUFPLEdBQVcsQ0FBQyxDQUFDOztJQW9GeEIsQ0FBQztJQWhGRyx1QkFBSyxHQUFMO1FBQUEsaUJBS0M7UUFKRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDeEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFDRCwyQkFBUyxHQUFUO1FBQ0ksSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1gsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDWCxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCw0QkFBVSxHQUFWLFVBQVcsVUFBMEI7UUFBMUIsMkJBQUEsRUFBQSxpQkFBMEI7UUFDakMsYUFBYTtRQUNiLElBQUksT0FBTyxHQUFHLElBQUksRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUcsRUFBRSxDQUFDLElBQVksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDMUcsaUJBQWlCO1FBQ2pCLElBQUksV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3pFLElBQUksU0FBUyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUM7UUFDakMsSUFBSSxVQUFVLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQztRQUNuQyxJQUFJLFVBQVUsSUFBSSxJQUFJLEVBQUU7WUFDcEIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNuQzthQUFNO1lBRUgsY0FBYztZQUNkLFdBQVcsQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO1NBQ3JGO1FBQ0QsVUFBVTtRQUNWLElBQUksSUFBSSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQ2pDLDZCQUE2QjtRQUM3Qix1REFBdUQ7UUFDdkQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFbEMsMEVBQTBFO1FBQzFFLHFDQUFxQztRQUNyQyxpRUFBaUU7UUFDakUsWUFBWTtRQUNaLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7UUFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFFeEMsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELCtCQUFhLEdBQWIsVUFBYyxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU07UUFDaEMsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDckMsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLEdBQUcsS0FBSyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsTUFBTSxHQUFHLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNqRixJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN6QyxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUM5QyxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pCLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ25DLGVBQWU7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsMEJBQVEsR0FBUixVQUFTLE9BQU87UUFDWixJQUFJLE1BQU0sRUFBRTtZQUNSLGlGQUFpRjtZQUVqRixnRkFBZ0Y7WUFDaEYsaUJBQWlCO1lBQ2pCLDREQUE0RDtZQUM1RCxJQUFJO1lBQ0osU0FBUztZQUNULDJDQUEyQztZQUMzQyxJQUFJO1NBQ1A7SUFDTCxDQUFDO0lBdkZEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7a0RBQ1k7SUFIZixPQUFPO1FBRDNCLE9BQU87T0FDYSxPQUFPLENBMkYzQjtJQUFELGNBQUM7Q0EzRkQsQUEyRkMsQ0EzRm9DLGlCQUFRLEdBMkY1QztrQkEzRm9CLE9BQU8iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFVJU2hhcmVfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlTaGFyZV9BdXRvXCI7XG5pbXBvcnQgeyBVSVdpbmRvdyB9IGZyb20gXCIuLi8uLi9Db21tb24vVUlGb3JtXCI7XG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSVNoYXJlIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgQHByb3BlcnR5KGNjLkNhbWVyYSlcbiAgICBjYW1lcmVDYXB0dXJlOiBjYy5DYW1lcmEgPSBudWxsO1xuXG4gICAgdGV4dHVyZTI6IGNjLlJlbmRlclRleHR1cmUgPSBudWxsO1xuICAgIF93aWR0aDogbnVtYmVyID0gMDtcbiAgICBfaGVpZ2h0OiBudW1iZXIgPSAwO1xuXG4gICAgdmlldzogVUlTaGFyZV9BdXRvO1xuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5DbG9zZUJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LlBob3RvQnRuLmFkZENsaWNrKHRoaXMub25DYXB0dXJlLCB0aGlzKTtcbiAgICB9XG4gICAgb25DYXB0dXJlKCkge1xuICAgICAgICBsZXQgbm9kZSA9IHRoaXMuc2NyZWVuU2hvdCh0aGlzLnZpZXcuY29udGVudE5vZGUpO1xuICAgICAgICBub2RlLnggPSAwO1xuICAgICAgICBub2RlLnkgPSAwO1xuICAgICAgICBub2RlLnBhcmVudCA9IGNjLmRpcmVjdG9yLmdldFNjZW5lKCk7XG4gICAgICAgIHRoaXMuY2FwdHVyZUFjdGlvbihub2RlLCBub2RlLndpZHRoLCBub2RlLmhlaWdodCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5oiq5Zu+XG4gICAgICogQHBhcmFtIHRhcmdldE5vZGUgIOaIquWbvuebruagh+iKgueCue+8jOWmguaenOS4um51bGzliJnooajnpLrmiKrlhajlsY9cbiAgICAgKiBAcmV0dXJucyDov5Tlm57miKrlsY/lm77niYfnmoRub2RlXG4gICAgICovXG4gICAgc2NyZWVuU2hvdCh0YXJnZXROb2RlOiBjYy5Ob2RlID0gbnVsbCkge1xuICAgICAgICAvL+WIm+W7uuaWsOeahHRleHR1cmVcbiAgICAgICAgbGV0IHRleHR1cmUgPSBuZXcgY2MuUmVuZGVyVGV4dHVyZSgpO1xuICAgICAgICB0ZXh0dXJlLmluaXRXaXRoU2l6ZShjYy53aW5TaXplLndpZHRoLCBjYy53aW5TaXplLmhlaWdodCwgKGNjLmdhbWUgYXMgYW55KS5fcmVuZGVyQ29udGV4dC5TVEVOQ0lMX0lOREVYOCk7XG4gICAgICAgIC8v5Yib5bu65paw55qEc3ByaXRlRnJhbWVcbiAgICAgICAgbGV0IHNwcml0ZUZyYW1lID0gbmV3IGNjLlNwcml0ZUZyYW1lKCk7XG4gICAgICAgIGxldCBub2RlWCA9IGNjLndpblNpemUud2lkdGggLyAyICsgdGFyZ2V0Tm9kZS54IC0gdGFyZ2V0Tm9kZS53aWR0aCAvIDI7XG4gICAgICAgIGxldCBub2RlWSA9IGNjLndpblNpemUuaGVpZ2h0IC8gMiArIHRhcmdldE5vZGUueSAtIHRhcmdldE5vZGUuaGVpZ2h0IC8gMjtcbiAgICAgICAgbGV0IG5vZGVXaWR0aCA9IHRhcmdldE5vZGUud2lkdGg7XG4gICAgICAgIGxldCBub2RlSGVpZ2h0ID0gdGFyZ2V0Tm9kZS5oZWlnaHQ7XG4gICAgICAgIGlmICh0YXJnZXROb2RlID09IG51bGwpIHtcbiAgICAgICAgICAgIHNwcml0ZUZyYW1lLnNldFRleHR1cmUodGV4dHVyZSk7XG4gICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgIC8v5Y+q5pi+56S6bm9kZemDqOWIhueahOWbvueJh1xuICAgICAgICAgICAgc3ByaXRlRnJhbWUuc2V0VGV4dHVyZSh0ZXh0dXJlLCBuZXcgY2MuUmVjdChub2RlWCwgbm9kZVksIG5vZGVXaWR0aCwgbm9kZUhlaWdodCkpO1xuICAgICAgICB9XG4gICAgICAgIC8v5Yib5bu65paw55qEbm9kZVxuICAgICAgICBsZXQgbm9kZSA9IG5ldyBjYy5Ob2RlKCk7XG4gICAgICAgIGxldCBzcHJpdGUgPSBub2RlLmFkZENvbXBvbmVudChjYy5TcHJpdGUpO1xuICAgICAgICBzcHJpdGUuc3ByaXRlRnJhbWUgPSBzcHJpdGVGcmFtZTtcbiAgICAgICAgLy/miKrlm77mmK/lj43nmoTvvIzov5nph4zlsIbmiKrlm75zY2FsZVnlj5blj43vvIzov5nmoLflsLHmmK/mraPnmoTkuoZcbiAgICAgICAgLy8gc3ByaXRlLm5vZGUuc2NhbGVZID0gLSBNYXRoLmFicyhzcHJpdGUubm9kZS5zY2FsZVkpO1xuICAgICAgICBzcHJpdGUuc3ByaXRlRnJhbWUuc2V0RmxpcFkodHJ1ZSk7XG5cbiAgICAgICAgLy8gbGV0IGJ1ZmZlciA9IG5ldyBBcnJheUJ1ZmZlcih0YXJnZXROb2RlLndpZHRoICogdGFyZ2V0Tm9kZS5oZWlnaHQgKiA0KTtcbiAgICAgICAgLy8gbGV0IGRhdGEgPSBuZXcgVWludDhBcnJheShidWZmZXIpO1xuICAgICAgICAvLyB0ZXh0dXJlLnJlYWRQaXhlbHMoZGF0YSwgbm9kZVgsIG5vZGVZLCBub2RlV2lkdGgsIG5vZGVIZWlnaHQpO1xuICAgICAgICAvL+aJi+WKqOa4suafk2NhbWVyYVxuICAgICAgICAvLyBjYW1lcmEuY3VsbGluZ01hc2sgPSAweGZmZmZmZmZmO1xuICAgICAgICB0aGlzLmNhbWVyZUNhcHR1cmUudGFyZ2V0VGV4dHVyZSA9IHRleHR1cmU7XG4gICAgICAgIHRoaXMuY2FtZXJlQ2FwdHVyZS5yZW5kZXIoKTtcbiAgICAgICAgdGhpcy5jYW1lcmVDYXB0dXJlLnRhcmdldFRleHR1cmUgPSBudWxsO1xuXG4gICAgICAgIHJldHVybiBub2RlO1xuICAgIH1cblxuICAgIGNhcHR1cmVBY3Rpb24oY2FwdHVyZSwgd2lkdGgsIGhlaWdodCkge1xuICAgICAgICBsZXQgc2NhbGVBY3Rpb24gPSBjYy5zY2FsZVRvKDEsIDAuNSk7XG4gICAgICAgIGxldCB0YXJnZXRQb3MgPSBjYy52Mih0aGlzLm5vZGUud2lkdGggLyAyICsgd2lkdGggLyAyIC0gNTAsIGhlaWdodCArIGhlaWdodCAvIDIpO1xuICAgICAgICBsZXQgbW92ZUFjdGlvbiA9IGNjLm1vdmVUbygxLCB0YXJnZXRQb3MpO1xuICAgICAgICBsZXQgc3Bhd24gPSBjYy5zcGF3bihzY2FsZUFjdGlvbiwgbW92ZUFjdGlvbik7XG4gICAgICAgIGNhcHR1cmUucnVuQWN0aW9uKHNwYXduKTtcbiAgICAgICAgbGV0IGJsaW5rQWN0aW9uID0gY2MuYmxpbmsoMC4xLCAxKTtcbiAgICAgICAgLy8gc2NlbmUgYWN0aW9uXG4gICAgICAgIHRoaXMubm9kZS5ydW5BY3Rpb24oYmxpbmtBY3Rpb24pO1xuICAgIH1cblxuICAgIHNhdmVGaWxlKHBpY0RhdGEpIHtcbiAgICAgICAgaWYgKENDX0pTQikge1xuICAgICAgICAgICAgLy8gbGV0IGZpbGVQYXRoID0ganNiLmZpbGVVdGlscy5nZXRXcml0YWJsZVBhdGgoKSArICdyZW5kZXJfdG9fc3ByaXRlX2ltYWdlLnBuZyc7XG5cbiAgICAgICAgICAgIC8vIGxldCBzdWNjZXNzID0ganNiLnNhdmVJbWFnZURhdGEocGljRGF0YSwgdGhpcy5fd2lkdGgsIHRoaXMuX2hlaWdodCwgZmlsZVBhdGgpXG4gICAgICAgICAgICAvLyBpZiAoc3VjY2Vzcykge1xuICAgICAgICAgICAgLy8gICAgIGNjLmxvZyhcInNhdmUgaW1hZ2UgZGF0YSBzdWNjZXNzLCBmaWxlOiBcIiArIGZpbGVQYXRoKTtcbiAgICAgICAgICAgIC8vIH1cbiAgICAgICAgICAgIC8vIGVsc2Uge1xuICAgICAgICAgICAgLy8gICAgIGNjLmVycm9yKFwic2F2ZSBpbWFnZSBkYXRhIGZhaWxlZCFcIik7XG4gICAgICAgICAgICAvLyB9XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=