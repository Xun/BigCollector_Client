
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Share/GenerateQR.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2bb095/a31FHo6dokcSVtRb', 'GenerateQR');
// Script/UIScript/Share/GenerateQR.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GenerateQR = /** @class */ (function (_super) {
    __extends(GenerateQR, _super);
    function GenerateQR() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GenerateQR.prototype.onLoad = function () {
        this.init('http://baidu.com');
    };
    GenerateQR.prototype.init = function (url) {
        var ctx = this.node.addComponent(cc.Graphics);
        if (typeof (url) !== 'string') {
            console.log('url is not string', url);
            return;
        }
        this.createQR(ctx, url);
    };
    GenerateQR.prototype.createQR = function (ctx, url) {
        var qrcode = new QRCode(-1, QRErrorCorrectLevel.H);
        qrcode.addData(url);
        qrcode.make();
        ctx.fillColor = cc.Color.BLACK;
        //块宽高
        var tileW = this.node.width / qrcode.getModuleCount();
        var tileH = this.node.height / qrcode.getModuleCount();
        // 用Graphics画二维码
        for (var row = 0; row < qrcode.getModuleCount(); row++) {
            for (var col = 0; col < qrcode.getModuleCount(); col++) {
                if (qrcode.isDark(row, col)) {
                    // ctx.fillColor = cc.Color.BLACK;
                    var w = (Math.ceil((col + 1) * tileW) - Math.floor(col * tileW));
                    var h = (Math.ceil((row + 1) * tileW) - Math.floor(row * tileW));
                    ctx.rect(Math.round(col * tileW) - this.node.width / 2, Math.round(row * tileH) - this.node.height / 2, w, h);
                    ctx.fill();
                }
            }
        }
    };
    GenerateQR = __decorate([
        ccclass
    ], GenerateQR);
    return GenerateQR;
}(cc.Component));
exports.default = GenerateQR;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hhcmUvR2VuZXJhdGVRUi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDTSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUF3Qyw4QkFBWTtJQUFwRDs7SUEwQ0EsQ0FBQztJQXZDQSwyQkFBTSxHQUFOO1FBQ0MsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCx5QkFBSSxHQUFKLFVBQUssR0FBVztRQUVmLElBQUksR0FBRyxHQUFnQixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDM0QsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxFQUFFO1lBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDdEMsT0FBTztTQUNQO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELDZCQUFRLEdBQVIsVUFBUyxHQUFnQixFQUFFLEdBQVc7UUFDckMsSUFBSSxNQUFNLEdBQVcsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0QsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNwQixNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFZCxHQUFHLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBRS9CLEtBQUs7UUFDTCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEQsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXZELGdCQUFnQjtRQUNoQixLQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsTUFBTSxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQ3ZELEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxNQUFNLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUU7Z0JBQ3ZELElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUU7b0JBQzVCLGtDQUFrQztvQkFDbEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ2pFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNqRSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUM5RyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7aUJBQ1g7YUFDRDtTQUNEO0lBQ0YsQ0FBQztJQXpDbUIsVUFBVTtRQUQ5QixPQUFPO09BQ2EsVUFBVSxDQTBDOUI7SUFBRCxpQkFBQztDQTFDRCxBQTBDQyxDQTFDdUMsRUFBRSxDQUFDLFNBQVMsR0EwQ25EO2tCQTFDb0IsVUFBVSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2VuZXJhdGVRUiBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG5cclxuXHRvbkxvYWQoKSB7XHJcblx0XHR0aGlzLmluaXQoJ2h0dHA6Ly9iYWlkdS5jb20nKTtcclxuXHR9XHJcblxyXG5cdGluaXQodXJsOiBzdHJpbmcpIHtcclxuXHJcblx0XHRsZXQgY3R4OiBjYy5HcmFwaGljcyA9IHRoaXMubm9kZS5hZGRDb21wb25lbnQoY2MuR3JhcGhpY3MpO1xyXG5cdFx0aWYgKHR5cGVvZiAodXJsKSAhPT0gJ3N0cmluZycpIHtcclxuXHRcdFx0Y29uc29sZS5sb2coJ3VybCBpcyBub3Qgc3RyaW5nJywgdXJsKTtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cclxuXHRcdHRoaXMuY3JlYXRlUVIoY3R4LCB1cmwpO1xyXG5cdH1cclxuXHJcblx0Y3JlYXRlUVIoY3R4OiBjYy5HcmFwaGljcywgdXJsOiBzdHJpbmcpIHtcclxuXHRcdGxldCBxcmNvZGU6IFFSQ29kZSA9IG5ldyBRUkNvZGUoLTEsIFFSRXJyb3JDb3JyZWN0TGV2ZWwuSCk7XHJcblx0XHRxcmNvZGUuYWRkRGF0YSh1cmwpO1xyXG5cdFx0cXJjb2RlLm1ha2UoKTtcclxuXHJcblx0XHRjdHguZmlsbENvbG9yID0gY2MuQ29sb3IuQkxBQ0s7XHJcblxyXG5cdFx0Ly/lnZflrr3pq5hcclxuXHRcdGxldCB0aWxlVyA9IHRoaXMubm9kZS53aWR0aCAvIHFyY29kZS5nZXRNb2R1bGVDb3VudCgpO1xyXG5cdFx0bGV0IHRpbGVIID0gdGhpcy5ub2RlLmhlaWdodCAvIHFyY29kZS5nZXRNb2R1bGVDb3VudCgpO1xyXG5cclxuXHRcdC8vIOeUqEdyYXBoaWNz55S75LqM57u056CBXHJcblx0XHRmb3IgKGxldCByb3cgPSAwOyByb3cgPCBxcmNvZGUuZ2V0TW9kdWxlQ291bnQoKTsgcm93KyspIHtcclxuXHRcdFx0Zm9yIChsZXQgY29sID0gMDsgY29sIDwgcXJjb2RlLmdldE1vZHVsZUNvdW50KCk7IGNvbCsrKSB7XHJcblx0XHRcdFx0aWYgKHFyY29kZS5pc0Rhcmsocm93LCBjb2wpKSB7XHJcblx0XHRcdFx0XHQvLyBjdHguZmlsbENvbG9yID0gY2MuQ29sb3IuQkxBQ0s7XHJcblx0XHRcdFx0XHRsZXQgdyA9IChNYXRoLmNlaWwoKGNvbCArIDEpICogdGlsZVcpIC0gTWF0aC5mbG9vcihjb2wgKiB0aWxlVykpO1xyXG5cdFx0XHRcdFx0bGV0IGggPSAoTWF0aC5jZWlsKChyb3cgKyAxKSAqIHRpbGVXKSAtIE1hdGguZmxvb3Iocm93ICogdGlsZVcpKTtcclxuXHRcdFx0XHRcdGN0eC5yZWN0KE1hdGgucm91bmQoY29sICogdGlsZVcpIC0gdGhpcy5ub2RlLndpZHRoIC8gMiwgTWF0aC5yb3VuZChyb3cgKiB0aWxlSCkgLSB0aGlzLm5vZGUuaGVpZ2h0IC8gMiwgdywgaCk7XHJcblx0XHRcdFx0XHRjdHguZmlsbCgpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0=