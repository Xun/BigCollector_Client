
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Home/UIUnlockNewLevel.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '49da5xdVQtLO6+ecL6HL4ks', 'UIUnlockNewLevel');
// Script/UIScript/Home/UIUnlockNewLevel.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Struct_1 = require("../../Common/Struct");
var SysDefine_1 = require("../../Common/SysDefine");
var UIForm_1 = require("../../Common/UIForm");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIUnlockNewLevel = /** @class */ (function (_super) {
    __extends(UIUnlockNewLevel, _super);
    function UIUnlockNewLevel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // onLoad () {}
        _this.modalType = new Struct_1.ModalType(SysDefine_1.ModalOpacity.OpacityHigh);
        return _this;
    }
    UIUnlockNewLevel.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
    };
    UIUnlockNewLevel.prototype.onShow = function (params) {
        this.view.NewLevelLab.string = "等级" + params.lv;
        this.view.YinPiaoNumLab.string = "银票" + params.money;
    };
    UIUnlockNewLevel = __decorate([
        ccclass
    ], UIUnlockNewLevel);
    return UIUnlockNewLevel;
}(UIForm_1.UIWindow));
exports.default = UIUnlockNewLevel;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvSG9tZS9VSVVubG9ja05ld0xldmVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLDhDQUFnRDtBQUNoRCxvREFBc0Q7QUFDdEQsOENBQStDO0FBRXpDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQThDLG9DQUFRO0lBQXREO1FBQUEscUVBa0JDO1FBZEcsZUFBZTtRQUNmLGVBQVMsR0FBRyxJQUFJLGtCQUFTLENBQUMsd0JBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQzs7SUFheEQsQ0FBQztJQVpHLGdDQUFLLEdBQUw7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUN4QixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVNLGlDQUFNLEdBQWIsVUFBYyxNQUFXO1FBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBQztRQUNoRCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFFekQsQ0FBQztJQWhCZ0IsZ0JBQWdCO1FBRHBDLE9BQU87T0FDYSxnQkFBZ0IsQ0FrQnBDO0lBQUQsdUJBQUM7Q0FsQkQsQUFrQkMsQ0FsQjZDLGlCQUFRLEdBa0JyRDtrQkFsQm9CLGdCQUFnQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IFVJVW5sb2NrTmV3TGV2ZWxfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlVbmxvY2tOZXdMZXZlbF9BdXRvXCI7XG5pbXBvcnQgeyBNb2RhbFR5cGUgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1N0cnVjdFwiO1xuaW1wb3J0IHsgTW9kYWxPcGFjaXR5IH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9TeXNEZWZpbmVcIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9VSUZvcm1cIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJVW5sb2NrTmV3TGV2ZWwgZXh0ZW5kcyBVSVdpbmRvdyB7XG5cblxuICAgIHZpZXc6IFVJVW5sb2NrTmV3TGV2ZWxfQXV0bztcbiAgICAvLyBvbkxvYWQgKCkge31cbiAgICBtb2RhbFR5cGUgPSBuZXcgTW9kYWxUeXBlKE1vZGFsT3BhY2l0eS5PcGFjaXR5SGlnaCk7XG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5DbG9zZUJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25TaG93KHBhcmFtczogYW55KTogdm9pZCB7XG4gICAgICAgIHRoaXMudmlldy5OZXdMZXZlbExhYi5zdHJpbmcgPSBcIuetiee6p1wiICsgcGFyYW1zLmx2O1xuICAgICAgICB0aGlzLnZpZXcuWWluUGlhb051bUxhYi5zdHJpbmcgPSBcIumTtuelqFwiICsgcGFyYW1zLm1vbmV5O1xuXG4gICAgfVxuXG59XG4iXX0=