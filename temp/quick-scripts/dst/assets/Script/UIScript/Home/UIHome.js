
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Home/UIHome.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ce700EVbt5MTrg0Llyxj9zb', 'UIHome');
// Script/UIScript/Home/UIHome.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var DataUser_1 = require("../../DataModal/DataUser");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var SoundMgr_1 = require("../../Manager/SoundMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var TAG_VIRTUAL_MOVE = 10001; //虚拟道具
var SPEED_VIRUTAL_MOVE = 2000;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIHome = /** @class */ (function (_super) {
    __extends(UIHome, _super);
    function UIHome() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.itemProducePrefab = null;
        //是否自动合成
        _this._isCombining = false;
        //是否可以拖拽，当前是否在使用
        _this._isCanDrag = false;
        //当前拖拽节点
        _this._currentNode = null;
        //拖拽时的虚拟节点
        _this._virtualNode = null;
        return _this;
    }
    UIHome.prototype.load = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                SoundMgr_1.default.inst.playMusic("Sounds/bg");
                return [2 /*return*/, null];
            });
        });
    };
    UIHome.prototype.start = function () {
        var _this = this;
        // if (AdapterMgr.inst.checkIsIphoneX()) {
        //     AdapterMgr.inst.adapteByType(AdapterType.Top, this.view.topNode, 20);
        // }
        RpcConent_1.apiClient.listenMsg("LvUp", function (data) {
            _this.onLvUp(data);
        });
        RpcConent_1.apiClient.listenMsg("UpdateUserInfo", function (data) {
            _this.upDataUserInfo(data);
        });
        this.item_pos_arr = [new cc.Vec2(0, 360), new cc.Vec2(-175, 270), new cc.Vec2(175, 270), new cc.Vec2(0, 180), new cc.Vec2(-175, 90), new cc.Vec2(175, 90), new cc.Vec2(0, 0), new cc.Vec2(-175, -90), new cc.Vec2(175, -90), new cc.Vec2(0, -180), new cc.Vec2(-175, -270), new cc.Vec2(175, -270)];
        this.initEvent();
        this.initData();
        this.initClickEvent();
        this.initWorkbench();
        // GameMgr.client.on("user_info_modified", async (payload) => {
        //     GameMgr.dataModalMgr.UserInfo.userInfos = UserInfo.decode(payload);
        // })
        // let s: string = NativeMgr.getStr("OneClass", "FucTwo");
        // console.log("sssss:" + s);
        // this.adaptiveNoteLayout();
        // this.scheduleOnce(() => {
        //     EventCenter.emit("shouguide", CocosHelper.findChildInNode("_ButtonPlus$SettingBtn", this.node));
        // }, 1)
    };
    UIHome.prototype.upDataUserInfo = function (data) {
        this.view.CoinTxt.string = data.coin.toString();
        this.view.DiamondTxt.string = data.diamond.toString();
    };
    UIHome.prototype.onLvUp = function (datas) {
        if (datas.type === 1) {
            FormMgr_1.default.open(UIConfig_1.default.UIUnlockNewLevel, datas);
        }
    };
    UIHome.prototype.adaptiveNoteLayout = function () {
        var winSize = cc.winSize; //获取当前游戏窗口大小
        cc.log("--当前游戏窗口大小  w:" + winSize.width + "   h:" + winSize.height);
        var viewSize = cc.view.getFrameSize();
        cc.log("--视图边框尺寸：w:" + viewSize.width + "  h:" + viewSize.height);
        var canvasSize = cc.view.getCanvasSize(); //视图中canvas尺寸
        cc.log("--视图中canvas尺寸  w:" + canvasSize.width + "  H:" + canvasSize.height);
        var visibleSize = cc.view.getVisibleSize();
        cc.log("--视图中窗口可见区域的尺寸 w:" + visibleSize.width + "   h:" + visibleSize.height);
        var designSize = cc.view.getDesignResolutionSize();
        cc.log("--设计分辨率：" + designSize.width + "    h: " + designSize.height);
        cc.log("--当前节点的尺寸 w:" + this.node.width + "   h:" + this.node.height);
    };
    //初始化点击事件
    UIHome.prototype.initEvent = function () {
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    };
    //初始化点击事件
    UIHome.prototype.initClickEvent = function () {
        this.view.BuyBtn.addClick(this.onBuyProduceClick, this);
        this.view.CombineAutoBtn.addClick(this.onBtnCombineAutoClick, this);
        this.view.BagBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIBag);
            // NativeMgr.callNativeClass("OneClass", "FucOne", "test01", 11111, true, (str, num, b) => { console.log(str, num, b); })
        }, this);
        // this.view.SettingBtn.addClick(() => {
        // EventCenter.emit("shouguide", CocosHelper.findChildInNode("_ButtonPlus$BagBtn", this.node));
        // }, this);
        this.view.SignInBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UISignIn);
        }, this);
        this.view.DrawBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UILottery);
        }, this);
        this.view.RankBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIRank);
        }, this);
        this.view.CollectBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UICollection);
        }, this);
        this.view.ShopBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIShop);
            // FormMgr.open(UIConfig.UIDeal)
        }, this);
        this.view.MyBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIMy);
        }, this);
        this.view.TaskBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UITask);
        }, this);
        this.view.FriendBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIFriend);
        }, this);
    };
    //初始化显示数据
    UIHome.prototype.initData = function () {
        // CocosHelper.loadHead("https://pic.sogou.com/d?query=%E5%A4%B4%E5%83%8F&forbidqc=&entityid=&preQuery=&rawQuery=&queryList=&st=191&did=5", this.view.HeadSp);
        // GameMgr.dataModalMgr.UserInfo.userWorkbench = [1, 1, 2, 3, 4, 4, 5, 5, 6, 7];
        this.view.BuyCoinNumTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userCurNeedBuyCoinNum.toString();
        this.view.MakeMoneyTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userMakeCoin.toString();
        this.view.CoinTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userCoin.toString();
        this.view.DiamondTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userDiamond.toString();
        this.view.MoneyTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userMoney.toString();
    };
    //初始化工作台信息
    UIHome.prototype.initWorkbench = function () {
        var arrChildren = this.view.WorkContent.children;
        if (arrChildren.length > 0) {
            //已经创建过，不需要再创建
            return;
        }
        for (var idx = 0; idx < GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length; idx++) {
            var item_node = cc.instantiate(this.itemProducePrefab);
            this.view.WorkContent.addChild(item_node);
            item_node.setPosition(this.item_pos_arr[idx]);
        }
        this.view.WorkContent.children.forEach(function (itemNode, index) {
            var itemInfo = itemNode.getComponent('ItemProduce');
            if (index < GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length) {
                itemInfo.setWorkbenchItemInfo(index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[index]);
            }
        }, this);
    };
    UIHome.prototype.onTouchStart = function (touchEvent) {
        this._currentNode = this.getCurrentNodeByTouchPos(touchEvent.getLocation());
        if (this._currentNode) {
            var ItemProduce_1 = this._currentNode.getComponent('ItemProduce');
            this._isCanDrag = ItemProduce_1.dragStart();
            if (this._isCanDrag) {
                var currentWorldPos = this._currentNode.convertToWorldSpaceAR(cc.v2(0, 0));
                this._offsetPos = currentWorldPos.sub(touchEvent.getLocation());
                this.showVirtualItem(ItemProduce_1.getInfo(), touchEvent.getLocation());
                this.showProduce(ItemProduce_1);
            }
        }
    };
    UIHome.prototype.onTouchMove = function (touchEvent) {
        if (!this._isCanDrag) {
            return;
        }
        this.updateVirutalItemPos(touchEvent.getLocation());
    };
    UIHome.prototype.onTouchEnd = function (touchEvent) {
        if (!this._currentNode) {
            return;
        }
        this.hideProduce();
        var cakeItem = this._currentNode.getComponent('ItemProduce');
        cakeItem.dragOver();
        var touchEndNode = this.getCurrentNodeByTouchPos(touchEvent.getLocation());
        if (this._currentNode === touchEndNode) {
            //属于点击自己
            this.hideVirtualItem(true);
            this._currentNode = null;
            return;
        }
        if (!this._isCanDrag) {
            return;
        }
        if (!touchEndNode) {
            //TODO 判断是否拖拽到生产台上，没有则是拖拽到无效位置
            var isDragToRecovery = this.checkIsDragToRecovery(cakeItem._index, touchEvent.getLocation());
            if (!isDragToRecovery) {
                this.hideVirtualItem(true);
            }
            else {
                this.hideVirtualItem(false);
            }
        }
        else {
            //判断是交换位置还是与之合成
            this.combinOrSwap(this._currentNode, touchEndNode);
        }
        this._currentNode = null;
        this._isCanDrag = false;
    };
    UIHome.prototype.onTouchCancel = function (touchEvent) {
        if (!this._currentNode) {
            return;
        }
        var ItemProduce = this._currentNode.getComponent('ItemProduce');
        if (!ItemProduce._itemId) {
            return;
        }
        this.hideProduce();
        ItemProduce.dragOver();
        if (ItemProduce.isUsed) {
            this._currentNode = null;
            return;
        }
        //检查下是否拖拽到回收站了
        var isDragToRecovery = this.checkIsDragToRecovery(ItemProduce._index, touchEvent.getLocation());
        if (!isDragToRecovery) {
            //拖到了无效位置
            this.hideVirtualItem(true);
        }
        else {
            //发起回收的请求,此处已由recovery接收走
            this.hideVirtualItem(false);
        }
        this._currentNode = null;
    };
    UIHome.prototype.getCurrentNodeByTouchPos = function (pos) {
        var arrNode = this.view.WorkContent.children;
        return arrNode.find(function (itemNode, index) {
            var box = itemNode.getBoundingBoxToWorld();
            if (box.contains(pos)) {
                return true;
            }
        }, this);
    };
    /**
     * 拖拽时原物体半透明，然后拖拽的其实是一个虚拟的半透明物体
     */
    UIHome.prototype.showVirtualItem = function (info, pos) {
        if (!this._virtualNode) {
            this._virtualNode = cc.instantiate(this.itemProducePrefab);
            this._virtualNode.parent = this.node.parent;
        }
        else {
            this._virtualNode.active = true;
        }
        this._virtualNode.stopActionByTag(TAG_VIRTUAL_MOVE);
        var cakeItem = this._virtualNode.getComponent('ItemProduce');
        cakeItem.setWorkbenchItemInfo(-1, info);
        cakeItem.showVirtual();
        this.updateVirutalItemPos(pos);
    };
    /**
     * 隐藏拖拽时的半透明图标
     */
    UIHome.prototype.hideVirtualItem = function (isGoBack) {
        var _this = this;
        if (this._virtualNode && this._virtualNode.active) {
            if (isGoBack) {
                // this.currentNode.converttow
                var posWorld = this._currentNode.convertToWorldSpaceAR(cc.v2);
                var posCurrentNode = this.node.parent.convertToNodeSpaceAR(posWorld);
                var duration = posCurrentNode.sub(this._virtualNode.position).mag() / SPEED_VIRUTAL_MOVE;
                CocosHelper_1.default.runTweenSync(this._virtualNode, cc.tween().to(0, duration).call(function () {
                    _this._virtualNode.active = false;
                }));
            }
            else {
                this._virtualNode.active = false;
            }
        }
    };
    /**
     * 更新拖拽时的半透明图标随手指移动位置
     */
    UIHome.prototype.updateVirutalItemPos = function (pos) {
        if (this._virtualNode && this._virtualNode.active) {
            //需要坐标转换
            var posNodeSpace = this.node.parent.convertToNodeSpaceAR(pos);
            this._virtualNode.position = posNodeSpace.add(this._offsetPos);
            this.updateDragPos(pos);
        }
    };
    UIHome.prototype.updateDragPos = function (pos) {
        var box = this.view.RecoveryBtn.node.getBoundingBoxToWorld();
        if (!box.contains(pos)) {
            if (this.view.RecoveryBtn.node.scale === 1.1) {
                this.view.RecoveryBtn.node.scale = 1;
            }
            return false;
        }
        if (this.view.RecoveryBtn.node.scale === 1) {
            this.view.RecoveryBtn.node.scale = 1.1;
        }
    };
    //检查是否拖到回收站了
    UIHome.prototype.checkIsDragToRecovery = function (workbenchIdx, pos) {
        return __awaiter(this, void 0, void 0, function () {
            var box, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        box = this.view.RecoveryBtn.node.getBoundingBoxToWorld();
                        if (!box.contains(pos)) {
                            return [2 /*return*/, false];
                        }
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount, ctrlType: DataUser_1.OperationType.sell, curPos: workbenchIdx })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        else {
                            this.updateWorkbench(workbenchIdx, -1, "Recovery");
                            return [2 /*return*/, true];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    //点击拖动时播放相同等级物品的动画
    UIHome.prototype.showProduce = function (itemProduce) {
        var index = itemProduce._index;
        var itemId = itemProduce._itemId;
        var arrNode = this.view.WorkContent.children;
        for (var idx = 0; idx < arrNode.length; idx++) {
            if (idx === index) {
                continue;
            }
            var produce = arrNode[idx].getComponent('ItemProduce');
            if (!produce.isUsed && produce.itemId === itemId) {
                // produce.playProduceAni(true);
            }
        }
    };
    //隐藏拖拽时的图标
    UIHome.prototype.hideProduce = function () {
        var arrNode = this.view.WorkContent.children;
        for (var idx = 0; idx < arrNode.length; idx++) {
            var produce = arrNode[idx].getComponent('ItemProduce');
            if (!produce.isUsed) {
                // produce.playProduceAni(false);
            }
        }
    };
    /**
     * 判断是交换还是结合
     */
    UIHome.prototype.combinOrSwap = function (originNode, targetNode) {
        return __awaiter(this, void 0, void 0, function () {
            var originScript, targetScript, originItem, targetItem, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        originScript = originNode.getComponent('ItemProduce');
                        targetScript = targetNode.getComponent('ItemProduce');
                        if (originScript.isUsed || targetScript.isUsed) {
                            this.hideVirtualItem(true);
                            return [2 /*return*/];
                        }
                        originItem = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originScript._index];
                        targetItem = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetScript._index];
                        if (originItem == null || targetItem == null) {
                            return [2 /*return*/, false];
                        }
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount, ctrlType: DataUser_1.OperationType.concat, curPos: originScript._index, target: targetScript._index })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp("数据错误");
                            this.updateWorkbench(originScript._index, targetScript._index, "Exchange");
                            return [2 /*return*/];
                        }
                        if (originScript._itemId === targetScript._itemId) {
                            this.combinProduce(originScript._index, targetScript._index, datas.res.userWorkBench[targetScript._index]);
                        }
                        else {
                            //位置交换
                            console.log("jiaohuan");
                            if (this.swapProduce(originScript._index, targetScript._index)) {
                                this.updateWorkbench(originScript._index, targetScript._index, "Exchange");
                            }
                        }
                        this.hideVirtualItem(false);
                        return [2 /*return*/];
                }
            });
        });
    };
    //更新工作台
    UIHome.prototype.updateWorkbench = function (originIndex, targetIndex, type) {
        if (!type) {
            this.initWorkbench();
            return;
        }
        var originInfo;
        var targetInfo;
        if (originIndex >= 0) {
            var originNode = this.view.WorkContent.children[originIndex];
            originInfo = originNode.getComponent('ItemProduce');
        }
        if (targetIndex >= 0) {
            var targetNode = this.view.WorkContent.children[targetIndex];
            targetInfo = targetNode.getComponent('ItemProduce');
        }
        switch (type) {
            case "Exchange":
                console.log("交换");
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                originInfo.setWorkbenchItemInfo(originInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originInfo._index]);
                break;
            case "Combine":
                console.log("组合");
                originInfo.setWorkbenchItemInfo(originInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originInfo._index]);
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                break;
            case "Produce":
                console.log("生产");
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                break;
            case "Recovery":
                console.log("回收");
                originInfo.setWorkbenchItemInfo(originInfo._index, 0);
                GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originInfo._index] = 0;
                break;
        }
    };
    /**
     * 交换生产物位置
     * @param {number} originIndex
     * @param {number} targetIndex
     */
    UIHome.prototype.swapProduce = function (originIndex, targetIndex) {
        if (!GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench) {
            return false;
        }
        if (originIndex >= GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length || targetIndex >= GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length) {
            return false;
        }
        var originItemId = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originIndex];
        var targetItemId = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetIndex];
        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetIndex] = originItemId;
        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originIndex] = targetItemId;
        return true;
    };
    /**
     * 生产物合并
     * @param {Number} originIndex 原位置的index
     * @param {Number} targetIndex 目标位置的index
     * @param {String} itemId 物品id
     */
    UIHome.prototype.combinProduce = function (originIndex, targetIndex, itemId) {
        var originNode = this.view.WorkContent.children[originIndex];
        var targetNode = this.view.WorkContent.children[targetIndex];
        var originScript = originNode.getComponent('ItemProduce');
        var targetScript = targetNode.getComponent('ItemProduce');
        originScript.isUsed = true; //要结合，先将两个生产物置为不可拖拽
        targetScript.isUsed = true;
        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originIndex] = 0;
        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetIndex] = itemId;
        this.updateWorkbench(originScript._index, targetScript._index, "Combine");
    };
    //购买事件
    UIHome.prototype.onBuyProduceClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isEnough, index, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        isEnough = GameMgr_1.default.dataModalMgr.UserInfo.userCoin >= GameMgr_1.default.dataModalMgr.CurBuyProduceInfo.buyPrice;
                        if (!isEnough) {
                            UIToast_1.default.popUp("金币不足！");
                            return [2 /*return*/];
                        }
                        if (!GameMgr_1.default.dataModalMgr.UserInfo.hasPosAtWorkbench()) {
                            UIToast_1.default.popUp("请先出售多余物品！");
                            return [2 /*return*/];
                        }
                        index = GameMgr_1.default.dataModalMgr.UserInfo.checkNullPos();
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount, ctrlType: DataUser_1.OperationType.buy, curPos: index })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[index] = datas.res.userWorkBench[index];
                        this.updateWorkbench(-1, index, "Produce");
                        return [2 /*return*/];
                }
            });
        });
    };
    //自动合成
    UIHome.prototype.onBtnCombineAutoClick = function () {
        if (this._isCombining) {
            UIToast_1.default.popUp("正在自动合成中");
            return;
        }
        var at = GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime;
        if (!at || at <= 0) {
            //停止自动合成
            this.scheduleCombineOver();
            return;
        }
        if (!this._isCombining) {
            this.checkIsCombineAuto();
        }
    };
    //检查是否正在自动合成
    UIHome.prototype.checkIsCombineAuto = function () {
        var spareTime = GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime;
        if (!spareTime || spareTime <= 0) {
            this.scheduleCombineOver();
            return;
        }
        this._isCombining = true;
        this.schedule(this.combineAuto, 1);
        this.view.CombineAutoTxt.string = CocosHelper_1.default.formatTimeForSecond(15);
        this.view.CombineAutoTxt.node.active = true;
    };
    /**
     *
     * 自动合成
     */
    UIHome.prototype.combineAuto = function () {
        GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime -= 1;
        if (GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime <= 0) {
            this.scheduleCombineOver();
        }
        this.view.CombineAutoTxt.string = CocosHelper_1.default.formatTimeForSecond(GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime);
        var arrSort = [];
        for (var idx = 0; idx < GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length; idx++) {
            var cakeNode = this.view.WorkContent.children[idx];
            var cakeItem = cakeNode.getComponent('ItemProduce');
            var isUsed = cakeItem.isUsed;
            if (isUsed) {
                continue;
            }
            var itemId = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[idx];
            if (itemId === 0) {
                continue; //礼盒或者是空位置要屏蔽掉
            }
            arrSort.push({ itemId: itemId, index: idx });
        }
        //按照蛋糕id进行排序
        arrSort.sort(function (a, b) {
            return Number(a.itemId) - Number(b.itemId);
        });
        var originIndex = -1;
        var targetIndex = -1;
        for (var idx = 0; idx < arrSort.length - 1; idx++) {
            if (arrSort[idx].itemId === arrSort[idx + 1].itemId && arrSort[idx].itemId.toString() !== "38") { //排除最高等级，避免最高等级影响后续合成
                //找到等级最低的蛋糕了
                targetIndex = arrSort[idx].index;
                originIndex = arrSort[idx + 1].index;
                break;
            }
        }
        if (originIndex !== -1 && targetIndex !== -1) {
            if (this._currentNode) {
                var cakeItem = this._currentNode.getComponent('ItemProduce');
                if (cakeItem.index === originIndex || cakeItem.index === targetIndex) {
                    cakeItem.dragOver();
                    this.hideProduce();
                    this._currentNode = null;
                    this.hideVirtualItem(false);
                }
            }
            this.combinOrSwap(this.view.WorkContent.children[originIndex], this.view.WorkContent.children[targetIndex]);
        }
    };
    /**
     * 关闭合成倒计时
     */
    UIHome.prototype.scheduleCombineOver = function () {
        this._isCombining = false;
        this.view.CombineAutoTxt.node.active = false;
        this.unschedule(this.combineAuto);
    };
    __decorate([
        property(cc.Prefab)
    ], UIHome.prototype, "itemProducePrefab", void 0);
    UIHome = __decorate([
        ccclass
    ], UIHome);
    return UIHome;
}(UIForm_1.UIScreen));
exports.default = UIHome;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvSG9tZS9VSUhvbWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsOENBQStDO0FBQy9DLHFEQUF5RDtBQUd6RCxpREFBNEM7QUFDNUMsaURBQTRDO0FBRTVDLG1EQUE4QztBQUc5QyxpREFBZ0Q7QUFDaEQsMkNBQXNDO0FBQ3RDLHVEQUFrRDtBQUdsRCxzQ0FBaUM7QUFDakMsSUFBTSxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsQ0FBRSxNQUFNO0FBQ3ZDLElBQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDO0FBQzFCLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQW9DLDBCQUFRO0lBQTVDO1FBQUEscUVBa21CQztRQTdsQkcsdUJBQWlCLEdBQWMsSUFBSSxDQUFDO1FBRXBDLFFBQVE7UUFDQSxrQkFBWSxHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBZ0I7UUFDUixnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUNwQyxRQUFRO1FBQ0Esa0JBQVksR0FBRyxJQUFJLENBQUM7UUFDNUIsVUFBVTtRQUNGLGtCQUFZLEdBQUcsSUFBSSxDQUFDOztJQW9sQmhDLENBQUM7SUFobEJTLHFCQUFJLEdBQVY7OztnQkFDSSxrQkFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3JDLHNCQUFPLElBQUksRUFBQzs7O0tBQ2Y7SUFFRCxzQkFBSyxHQUFMO1FBQUEsaUJBeUJDO1FBeEJHLDBDQUEwQztRQUMxQyw0RUFBNEU7UUFDNUUsSUFBSTtRQUNKLHFCQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxVQUFDLElBQUk7WUFDN0IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FBQztRQUNILHFCQUFTLENBQUMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLFVBQUMsSUFBSTtZQUN2QyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFDblMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXJCLCtEQUErRDtRQUMvRCwwRUFBMEU7UUFDMUUsS0FBSztRQUNMLDBEQUEwRDtRQUMxRCw2QkFBNkI7UUFDN0IsNkJBQTZCO1FBQzdCLDRCQUE0QjtRQUM1Qix1R0FBdUc7UUFDdkcsUUFBUTtJQUNaLENBQUM7SUFFRCwrQkFBYyxHQUFkLFVBQWUsSUFBUztRQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMxRCxDQUFDO0lBRUQsdUJBQU0sR0FBTixVQUFPLEtBQUs7UUFDUixJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxFQUFFO1lBQ2xCLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDbEQ7SUFFTCxDQUFDO0lBRUQsbUNBQWtCLEdBQWxCO1FBQ0ksSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFBLFlBQVk7UUFDckMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsS0FBSyxHQUFHLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFcEUsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN0QyxFQUFFLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsS0FBSyxHQUFHLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFbEUsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFBLGFBQWE7UUFDdEQsRUFBRSxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsR0FBRyxVQUFVLENBQUMsS0FBSyxHQUFHLE1BQU0sR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFNUUsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUMzQyxFQUFFLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLFdBQVcsQ0FBQyxLQUFLLEdBQUcsT0FBTyxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUUvRSxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7UUFDbkQsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDLEtBQUssR0FBRyxTQUFTLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXRFLEVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzFFLENBQUM7SUFDRCxTQUFTO0lBQ1QsMEJBQVMsR0FBVDtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNqRixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN2RixDQUFDO0lBRUQsU0FBUztJQUNULCtCQUFjLEdBQWQ7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0IseUhBQXlIO1FBQzdILENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULHdDQUF3QztRQUN4QywrRkFBK0Y7UUFDL0YsWUFBWTtRQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztZQUN6QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUN2QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUN2QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2xDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUMxQixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUN2QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlCLGdDQUFnQztRQUNwQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsaUJBQU8sQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFDdkIsaUJBQU8sQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7WUFDekIsaUJBQU8sQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUNuQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsU0FBUztJQUNULHlCQUFRLEdBQVI7UUFDSSw4SkFBOEo7UUFDOUosZ0ZBQWdGO1FBQ2hGLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdEYsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDN0UsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbkYsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbkYsQ0FBQztJQUVELFVBQVU7SUFDViw4QkFBYSxHQUFiO1FBQ0ksSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBQ2pELElBQUksV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDeEIsY0FBYztZQUNkLE9BQU87U0FDVjtRQUVELEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUMvRSxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMxQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUNqRDtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFRLEVBQUUsS0FBSztZQUNuRCxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3BELElBQUksS0FBSyxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO2dCQUM1RCxRQUFRLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzthQUM1RjtRQUNMLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNiLENBQUM7SUFFRCw2QkFBWSxHQUFaLFVBQWEsVUFBb0I7UUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDNUUsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ25CLElBQUksYUFBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ2hFLElBQUksQ0FBQyxVQUFVLEdBQUcsYUFBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzFDLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDakIsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzRSxJQUFJLENBQUMsVUFBVSxHQUFHLGVBQWUsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7Z0JBQ2hFLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBVyxDQUFDLE9BQU8sRUFBRSxFQUFFLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO2dCQUN0RSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQVcsQ0FBQyxDQUFDO2FBQ2pDO1NBQ0o7SUFDTCxDQUFDO0lBRUQsNEJBQVcsR0FBWCxVQUFZLFVBQW9CO1FBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2xCLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQsMkJBQVUsR0FBVixVQUFXLFVBQW9CO1FBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3BCLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM3RCxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDcEIsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQzNFLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxZQUFZLEVBQUU7WUFDcEMsUUFBUTtZQUNSLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbEIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNmLDhCQUE4QjtZQUM5QixJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQzdGLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDbkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM5QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQy9CO1NBQ0o7YUFBTTtZQUNILGVBQWU7WUFDZixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUM7U0FDdEQ7UUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRUQsOEJBQWEsR0FBYixVQUFjLFVBQW9CO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3BCLE9BQU87U0FDVjtRQUVELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBRWhFLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFO1lBQ3RCLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUVuQixXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdkIsSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLE9BQU87U0FDVjtRQUVELGNBQWM7UUFDZCxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQ2hHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUNuQixTQUFTO1lBQ1QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QjthQUFNO1lBQ0gseUJBQXlCO1lBQ3pCLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDL0I7UUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRUQseUNBQXdCLEdBQXhCLFVBQXlCLEdBQVk7UUFDakMsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBQzdDLE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVEsRUFBRSxLQUFLO1lBQ2hDLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1lBQzNDLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbkIsT0FBTyxJQUFJLENBQUM7YUFDZjtRQUNMLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNiLENBQUM7SUFFRDs7T0FFRztJQUNILGdDQUFlLEdBQWYsVUFBZ0IsSUFBUyxFQUFFLEdBQVk7UUFDbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1NBQy9DO2FBQU07WUFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7U0FDbkM7UUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzdELFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN4QyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7T0FFRztJQUNILGdDQUFlLEdBQWYsVUFBZ0IsUUFBaUI7UUFBakMsaUJBY0M7UUFiRyxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDL0MsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsOEJBQThCO2dCQUM5QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUQsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3JFLElBQUksUUFBUSxHQUFHLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxrQkFBa0IsQ0FBQztnQkFDekYscUJBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQ3hFLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUNOO2lCQUFNO2dCQUNILElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQzthQUNwQztTQUNKO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gscUNBQW9CLEdBQXBCLFVBQXFCLEdBQVk7UUFDN0IsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO1lBQy9DLFFBQVE7WUFDUixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM5RCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzNCO0lBQ0wsQ0FBQztJQUVELDhCQUFhLEdBQWIsVUFBYyxHQUFZO1FBQ3RCLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQzdELElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3BCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxHQUFHLEVBQUU7Z0JBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO2FBQ3hDO1lBQ0QsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssQ0FBQyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1NBQzFDO0lBQ0wsQ0FBQztJQUVELFlBQVk7SUFDTixzQ0FBcUIsR0FBM0IsVUFBNEIsWUFBb0IsRUFBRSxHQUFZOzs7Ozs7d0JBQ3RELEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQzt3QkFFN0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7NEJBQ3BCLHNCQUFPLEtBQUssRUFBQzt5QkFDaEI7d0JBQ1cscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFLEVBQUUsV0FBVyxFQUFFLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsUUFBUSxFQUFFLHdCQUFhLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFBOzt3QkFBaEssS0FBSyxHQUFHLFNBQXdKO3dCQUNwSyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTs0QkFDZixpQkFBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDOzRCQUNqQyxzQkFBTzt5QkFDVjs2QkFBTTs0QkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQzs0QkFDbkQsc0JBQU8sSUFBSSxFQUFDO3lCQUNmOzs7OztLQUNKO0lBRUQsa0JBQWtCO0lBQ2xCLDRCQUFXLEdBQVgsVUFBWSxXQUF3QjtRQUNoQyxJQUFJLEtBQUssR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1FBQy9CLElBQUksTUFBTSxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUM7UUFDakMsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBRTdDLEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQzNDLElBQUksR0FBRyxLQUFLLEtBQUssRUFBRTtnQkFDZixTQUFTO2FBQ1o7WUFFRCxJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxJQUFJLE9BQU8sQ0FBQyxNQUFNLEtBQUssTUFBTSxFQUFFO2dCQUM5QyxnQ0FBZ0M7YUFDbkM7U0FDSjtJQUNMLENBQUM7SUFFRCxVQUFVO0lBQ1YsNEJBQVcsR0FBWDtRQUNJLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztRQUM3QyxLQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUMzQyxJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUNqQixpQ0FBaUM7YUFDcEM7U0FDSjtJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNHLDZCQUFZLEdBQWxCLFVBQW1CLFVBQW1CLEVBQUUsVUFBbUI7Ozs7Ozt3QkFDbkQsWUFBWSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7d0JBQ3RELFlBQVksR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUMxRCxJQUFJLFlBQVksQ0FBQyxNQUFNLElBQUksWUFBWSxDQUFDLE1BQU0sRUFBRTs0QkFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDM0Isc0JBQU87eUJBQ1Y7d0JBRUcsVUFBVSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUM5RSxVQUFVLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ2xGLElBQUksVUFBVSxJQUFJLElBQUksSUFBSSxVQUFVLElBQUksSUFBSSxFQUFFOzRCQUMxQyxzQkFBTyxLQUFLLEVBQUM7eUJBQ2hCO3dCQUNXLHFCQUFNLHFCQUFTLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxFQUFFLFdBQVcsRUFBRSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSx3QkFBYSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsWUFBWSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUE7O3dCQUF0TSxLQUFLLEdBQUcsU0FBOEw7d0JBQzFNLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFOzRCQUNmLGlCQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzRCQUN0QixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsWUFBWSxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQzs0QkFDM0Usc0JBQU87eUJBQ1Y7d0JBQ0QsSUFBSSxZQUFZLENBQUMsT0FBTyxLQUFLLFlBQVksQ0FBQyxPQUFPLEVBQUU7NEJBQy9DLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO3lCQUM5Rzs2QkFBTTs0QkFDSCxNQUFNOzRCQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUE7NEJBQ3ZCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxNQUFNLENBQUMsRUFBRTtnQ0FDNUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7NkJBQzlFO3lCQUNKO3dCQUNELElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7Ozs7O0tBQy9CO0lBRUQsT0FBTztJQUNQLGdDQUFlLEdBQWYsVUFBZ0IsV0FBbUIsRUFBRSxXQUFtQixFQUFFLElBQVk7UUFDbEUsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNQLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUNyQixPQUFPO1NBQ1Y7UUFFRCxJQUFJLFVBQXVCLENBQUM7UUFDNUIsSUFBSSxVQUF1QixDQUFDO1FBQzVCLElBQUksV0FBVyxJQUFJLENBQUMsRUFBRTtZQUNsQixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDN0QsVUFBVSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDdkQ7UUFDRCxJQUFJLFdBQVcsSUFBSSxDQUFDLEVBQUU7WUFDbEIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzdELFVBQVUsR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ3ZEO1FBRUQsUUFBUSxJQUFJLEVBQUU7WUFDVixLQUFLLFVBQVU7Z0JBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDakIsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDbkgsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDbkgsTUFBTTtZQUNWLEtBQUssU0FBUztnQkFDVixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQixVQUFVLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNuSCxVQUFVLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNuSCxNQUFNO1lBQ1YsS0FBSyxTQUFTO2dCQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ25ILE1BQU07WUFDVixLQUFLLFVBQVU7Z0JBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RELGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDbkUsTUFBTTtTQUNiO0lBRUwsQ0FBQztJQUNEOzs7O09BSUc7SUFDSCw0QkFBVyxHQUFYLFVBQVksV0FBbUIsRUFBRSxXQUFtQjtRQUNoRCxJQUFJLENBQUMsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRTtZQUM5QyxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUVELElBQUksV0FBVyxJQUFJLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLFdBQVcsSUFBSSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUN4SSxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUVELElBQUksWUFBWSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDNUUsSUFBSSxZQUFZLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUU1RSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxHQUFHLFlBQVksQ0FBQztRQUN4RSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxHQUFHLFlBQVksQ0FBQztRQUV4RSxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCw4QkFBYSxHQUFiLFVBQWMsV0FBbUIsRUFBRSxXQUFtQixFQUFFLE1BQWM7UUFDbEUsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzdELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUU3RCxJQUFJLFlBQVksR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzFELElBQUksWUFBWSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFMUQsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsQ0FBQyxtQkFBbUI7UUFDL0MsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFFM0IsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDN0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsR0FBRyxNQUFNLENBQUM7UUFFbEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDOUUsQ0FBQztJQUVELE1BQU07SUFDQSxrQ0FBaUIsR0FBdkI7Ozs7Ozt3QkFDUSxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsSUFBSSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUM7d0JBQ3pHLElBQUksQ0FBQyxRQUFRLEVBQUU7NEJBQ1gsaUJBQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQ3ZCLHNCQUFPO3lCQUNWO3dCQUNELElBQUksQ0FBQyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEVBQUUsRUFBRTs0QkFDcEQsaUJBQU8sQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQzNCLHNCQUFPO3lCQUNWO3dCQUVHLEtBQUssR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFLENBQUM7d0JBQzdDLHFCQUFNLHFCQUFTLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRSxFQUFFLFdBQVcsRUFBRSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSx3QkFBYSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBQTs7d0JBQXhKLEtBQUssR0FBRyxTQUFnSjt3QkFDNUosSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7NEJBQ2YsaUJBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDakMsc0JBQU87eUJBQ1Y7d0JBQ0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDcEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7Ozs7O0tBQzlDO0lBRUQsTUFBTTtJQUNOLHNDQUFxQixHQUFyQjtRQUNJLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNuQixpQkFBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6QixPQUFPO1NBQ1Y7UUFDRCxJQUFJLEVBQUUsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNoQixRQUFRO1lBQ1IsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7WUFDM0IsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDN0I7SUFDTCxDQUFDO0lBRUQsWUFBWTtJQUNaLG1DQUFrQixHQUFsQjtRQUNJLElBQUksU0FBUyxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUM7UUFDOUQsSUFBSSxDQUFDLFNBQVMsSUFBSSxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQzlCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQzNCLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcscUJBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNoRCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsNEJBQVcsR0FBWDtRQUNJLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLElBQUksQ0FBQyxDQUFDO1FBQ25ELElBQUksaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsSUFBSSxDQUFDLEVBQUU7WUFDcEQsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDOUI7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcscUJBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7UUFFakgsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUMvRSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkQsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNwRCxJQUFJLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDO1lBQzdCLElBQUksTUFBTSxFQUFFO2dCQUNSLFNBQVM7YUFDWjtZQUVELElBQUksTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDOUQsSUFBSSxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUNkLFNBQVMsQ0FBQyxjQUFjO2FBQzNCO1lBRUQsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7U0FDaEQ7UUFFRCxZQUFZO1FBQ1osT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO1lBQ2QsT0FBTyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0MsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNyQixJQUFJLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUVyQixLQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUU7WUFDL0MsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxLQUFLLE9BQU8sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssSUFBSSxFQUFFLEVBQUUscUJBQXFCO2dCQUNuSCxZQUFZO2dCQUNaLFdBQVcsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUNqQyxXQUFXLEdBQUcsT0FBTyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3JDLE1BQU07YUFDVDtTQUNKO1FBRUQsSUFBSSxXQUFXLEtBQUssQ0FBQyxDQUFDLElBQUksV0FBVyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQzFDLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDbkIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzdELElBQUksUUFBUSxDQUFDLEtBQUssS0FBSyxXQUFXLElBQUksUUFBUSxDQUFDLEtBQUssS0FBSyxXQUFXLEVBQUU7b0JBQ2xFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDcEIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNuQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztvQkFDekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDL0I7YUFDSjtZQUVELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1NBQy9HO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsb0NBQW1CLEdBQW5CO1FBQ0ksSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQTNsQkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztxREFDZ0I7SUFMbkIsTUFBTTtRQUQxQixPQUFPO09BQ2EsTUFBTSxDQWttQjFCO0lBQUQsYUFBQztDQWxtQkQsQUFrbUJDLENBbG1CbUMsaUJBQVEsR0FrbUIzQztrQkFsbUJvQixNQUFNIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgR2FtZVNlcnZlciwgVXNlckluZm8gfSBmcm9tIFwiLi4vLi4vLi4vUHJvdG9jb2wvYnVuZGxlXCI7XG5pbXBvcnQgVUlIb21lX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJSG9tZV9BdXRvXCI7XG5pbXBvcnQgeyBVSVNjcmVlbiB9IGZyb20gXCIuLi8uLi9Db21tb24vVUlGb3JtXCI7XG5pbXBvcnQgeyBPcGVyYXRpb25UeXBlIH0gZnJvbSBcIi4uLy4uL0RhdGFNb2RhbC9EYXRhVXNlclwiO1xuaW1wb3J0IEFkYXB0ZXJNZ3IsIHsgQWRhcHRlclR5cGUgfSBmcm9tIFwiLi4vLi4vTWFuYWdlci9BZGFwdGVyTWdyXCI7XG5pbXBvcnQgeyBEYXRhTW9kYWxNZ3IgfSBmcm9tIFwiLi4vLi4vTWFuYWdlci9EYXRhTW9kYWxNZ3JcIjtcbmltcG9ydCBGb3JtTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL0Zvcm1NZ3JcIjtcbmltcG9ydCBHYW1lTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL0dhbWVNZ3JcIjtcbmltcG9ydCB7IE5hdGl2ZU1nciB9IGZyb20gXCIuLi8uLi9NYW5hZ2VyL05hdGl2ZU1nclwiO1xuaW1wb3J0IFNvdW5kTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL1NvdW5kTWdyXCI7XG5pbXBvcnQgVGlwc01nciBmcm9tIFwiLi4vLi4vTWFuYWdlci9UaXBzTWdyXCI7XG5pbXBvcnQgeyBFdmVudENlbnRlciB9IGZyb20gXCIuLi8uLi9OZXQvRXZlbnRDZW50ZXJcIjtcbmltcG9ydCB7IGFwaUNsaWVudCB9IGZyb20gXCIuLi8uLi9OZXQvUnBjQ29uZW50XCI7XG5pbXBvcnQgVUlDb25maWcgZnJvbSBcIi4uLy4uL1VJQ29uZmlnXCI7XG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5pbXBvcnQgSXRlbVByb2R1Y2UgZnJvbSBcIi4uL0l0ZW1zL0l0ZW1Qcm9kdWNlXCI7XG5pbXBvcnQgVUlSYW5rIGZyb20gXCIuLi9SYW5rL1VJUmFua1wiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcbmNvbnN0IFRBR19WSVJUVUFMX01PVkUgPSAxMDAwMTsgIC8v6Jma5ouf6YGT5YW3XG5jb25zdCBTUEVFRF9WSVJVVEFMX01PVkUgPSAyMDAwO1xuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJSG9tZSBleHRlbmRzIFVJU2NyZWVuIHtcblxuICAgIHZpZXc6IFVJSG9tZV9BdXRvO1xuXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcbiAgICBpdGVtUHJvZHVjZVByZWZhYjogY2MuUHJlZmFiID0gbnVsbDtcblxuICAgIC8v5piv5ZCm6Ieq5Yqo5ZCI5oiQXG4gICAgcHJpdmF0ZSBfaXNDb21iaW5pbmc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICAvL+aYr+WQpuWPr+S7peaLluaLve+8jOW9k+WJjeaYr+WQpuWcqOS9v+eUqFxuICAgIHByaXZhdGUgX2lzQ2FuRHJhZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIC8v5b2T5YmN5ouW5ou96IqC54K5XG4gICAgcHJpdmF0ZSBfY3VycmVudE5vZGUgPSBudWxsO1xuICAgIC8v5ouW5ou95pe255qE6Jma5ouf6IqC54K5XG4gICAgcHJpdmF0ZSBfdmlydHVhbE5vZGUgPSBudWxsO1xuICAgIC8v5ouW5ou95YGP56e75L2N572uXG4gICAgcHJpdmF0ZSBfb2Zmc2V0UG9zOiBjYy5WZWMyO1xuICAgIHByaXZhdGUgaXRlbV9wb3NfYXJyOiBjYy5WZWMyW107XG4gICAgYXN5bmMgbG9hZCgpIHtcbiAgICAgICAgU291bmRNZ3IuaW5zdC5wbGF5TXVzaWMoXCJTb3VuZHMvYmdcIik7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHN0YXJ0KCkge1xuICAgICAgICAvLyBpZiAoQWRhcHRlck1nci5pbnN0LmNoZWNrSXNJcGhvbmVYKCkpIHtcbiAgICAgICAgLy8gICAgIEFkYXB0ZXJNZ3IuaW5zdC5hZGFwdGVCeVR5cGUoQWRhcHRlclR5cGUuVG9wLCB0aGlzLnZpZXcudG9wTm9kZSwgMjApO1xuICAgICAgICAvLyB9XG4gICAgICAgIGFwaUNsaWVudC5saXN0ZW5Nc2coXCJMdlVwXCIsIChkYXRhKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uTHZVcChkYXRhKTtcbiAgICAgICAgfSk7XG4gICAgICAgIGFwaUNsaWVudC5saXN0ZW5Nc2coXCJVcGRhdGVVc2VySW5mb1wiLCAoZGF0YSkgPT4ge1xuICAgICAgICAgICAgdGhpcy51cERhdGFVc2VySW5mbyhkYXRhKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuaXRlbV9wb3NfYXJyID0gW25ldyBjYy5WZWMyKDAsIDM2MCksIG5ldyBjYy5WZWMyKC0xNzUsIDI3MCksIG5ldyBjYy5WZWMyKDE3NSwgMjcwKSwgbmV3IGNjLlZlYzIoMCwgMTgwKSwgbmV3IGNjLlZlYzIoLTE3NSwgOTApLCBuZXcgY2MuVmVjMigxNzUsIDkwKSwgbmV3IGNjLlZlYzIoMCwgMCksIG5ldyBjYy5WZWMyKC0xNzUsIC05MCksIG5ldyBjYy5WZWMyKDE3NSwgLTkwKSwgbmV3IGNjLlZlYzIoMCwgLTE4MCksIG5ldyBjYy5WZWMyKC0xNzUsIC0yNzApLCBuZXcgY2MuVmVjMigxNzUsIC0yNzApXVxuICAgICAgICB0aGlzLmluaXRFdmVudCgpO1xuICAgICAgICB0aGlzLmluaXREYXRhKCk7XG4gICAgICAgIHRoaXMuaW5pdENsaWNrRXZlbnQoKTtcbiAgICAgICAgdGhpcy5pbml0V29ya2JlbmNoKCk7XG5cbiAgICAgICAgLy8gR2FtZU1nci5jbGllbnQub24oXCJ1c2VyX2luZm9fbW9kaWZpZWRcIiwgYXN5bmMgKHBheWxvYWQpID0+IHtcbiAgICAgICAgLy8gICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJJbmZvcyA9IFVzZXJJbmZvLmRlY29kZShwYXlsb2FkKTtcbiAgICAgICAgLy8gfSlcbiAgICAgICAgLy8gbGV0IHM6IHN0cmluZyA9IE5hdGl2ZU1nci5nZXRTdHIoXCJPbmVDbGFzc1wiLCBcIkZ1Y1R3b1wiKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJzc3NzczpcIiArIHMpO1xuICAgICAgICAvLyB0aGlzLmFkYXB0aXZlTm90ZUxheW91dCgpO1xuICAgICAgICAvLyB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XG4gICAgICAgIC8vICAgICBFdmVudENlbnRlci5lbWl0KFwic2hvdWd1aWRlXCIsIENvY29zSGVscGVyLmZpbmRDaGlsZEluTm9kZShcIl9CdXR0b25QbHVzJFNldHRpbmdCdG5cIiwgdGhpcy5ub2RlKSk7XG4gICAgICAgIC8vIH0sIDEpXG4gICAgfVxuXG4gICAgdXBEYXRhVXNlckluZm8oZGF0YTogYW55KSB7XG4gICAgICAgIHRoaXMudmlldy5Db2luVHh0LnN0cmluZyA9IGRhdGEuY29pbi50b1N0cmluZygpO1xuICAgICAgICB0aGlzLnZpZXcuRGlhbW9uZFR4dC5zdHJpbmcgPSBkYXRhLmRpYW1vbmQudG9TdHJpbmcoKTtcbiAgICB9XG5cbiAgICBvbkx2VXAoZGF0YXMpIHtcbiAgICAgICAgaWYgKGRhdGFzLnR5cGUgPT09IDEpIHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSVVubG9ja05ld0xldmVsLCBkYXRhcyk7XG4gICAgICAgIH1cblxuICAgIH1cblxuICAgIGFkYXB0aXZlTm90ZUxheW91dCgpIHtcbiAgICAgICAgbGV0IHdpblNpemUgPSBjYy53aW5TaXplOy8v6I635Y+W5b2T5YmN5ri45oiP56qX5Y+j5aSn5bCPXG4gICAgICAgIGNjLmxvZyhcIi0t5b2T5YmN5ri45oiP56qX5Y+j5aSn5bCPICB3OlwiICsgd2luU2l6ZS53aWR0aCArIFwiICAgaDpcIiArIHdpblNpemUuaGVpZ2h0KTtcblxuICAgICAgICBsZXQgdmlld1NpemUgPSBjYy52aWV3LmdldEZyYW1lU2l6ZSgpO1xuICAgICAgICBjYy5sb2coXCItLeinhuWbvui+ueahhuWwuuWvuO+8mnc6XCIgKyB2aWV3U2l6ZS53aWR0aCArIFwiICBoOlwiICsgdmlld1NpemUuaGVpZ2h0KTtcblxuICAgICAgICBsZXQgY2FudmFzU2l6ZSA9IGNjLnZpZXcuZ2V0Q2FudmFzU2l6ZSgpOy8v6KeG5Zu+5LitY2FudmFz5bC65a+4XG4gICAgICAgIGNjLmxvZyhcIi0t6KeG5Zu+5LitY2FudmFz5bC65a+4ICB3OlwiICsgY2FudmFzU2l6ZS53aWR0aCArIFwiICBIOlwiICsgY2FudmFzU2l6ZS5oZWlnaHQpO1xuXG4gICAgICAgIGxldCB2aXNpYmxlU2l6ZSA9IGNjLnZpZXcuZ2V0VmlzaWJsZVNpemUoKTtcbiAgICAgICAgY2MubG9nKFwiLS3op4blm77kuK3nqpflj6Plj6/op4HljLrln5/nmoTlsLrlr7ggdzpcIiArIHZpc2libGVTaXplLndpZHRoICsgXCIgICBoOlwiICsgdmlzaWJsZVNpemUuaGVpZ2h0KTtcblxuICAgICAgICBsZXQgZGVzaWduU2l6ZSA9IGNjLnZpZXcuZ2V0RGVzaWduUmVzb2x1dGlvblNpemUoKTtcbiAgICAgICAgY2MubG9nKFwiLS3orr7orqHliIbovqjnjofvvJpcIiArIGRlc2lnblNpemUud2lkdGggKyBcIiAgICBoOiBcIiArIGRlc2lnblNpemUuaGVpZ2h0KTtcblxuICAgICAgICBjYy5sb2coXCItLeW9k+WJjeiKgueCueeahOWwuuWvuCB3OlwiICsgdGhpcy5ub2RlLndpZHRoICsgXCIgICBoOlwiICsgdGhpcy5ub2RlLmhlaWdodCk7XG4gICAgfVxuICAgIC8v5Yid5aeL5YyW54K55Ye75LqL5Lu2XG4gICAgaW5pdEV2ZW50KCkge1xuICAgICAgICB0aGlzLnZpZXcuV29ya0NvbnRlbnQub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsIHRoaXMub25Ub3VjaFN0YXJ0LCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LldvcmtDb250ZW50Lm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX01PVkUsIHRoaXMub25Ub3VjaE1vdmUsIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuV29ya0NvbnRlbnQub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfRU5ELCB0aGlzLm9uVG91Y2hFbmQsIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuV29ya0NvbnRlbnQub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfQ0FOQ0VMLCB0aGlzLm9uVG91Y2hDYW5jZWwsIHRoaXMpO1xuICAgIH1cblxuICAgIC8v5Yid5aeL5YyW54K55Ye75LqL5Lu2XG4gICAgaW5pdENsaWNrRXZlbnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5CdXlCdG4uYWRkQ2xpY2sodGhpcy5vbkJ1eVByb2R1Y2VDbGljaywgdGhpcyk7XG4gICAgICAgIHRoaXMudmlldy5Db21iaW5lQXV0b0J0bi5hZGRDbGljayh0aGlzLm9uQnRuQ29tYmluZUF1dG9DbGljaywgdGhpcyk7XG4gICAgICAgIHRoaXMudmlldy5CYWdCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJQmFnKTtcbiAgICAgICAgICAgIC8vIE5hdGl2ZU1nci5jYWxsTmF0aXZlQ2xhc3MoXCJPbmVDbGFzc1wiLCBcIkZ1Y09uZVwiLCBcInRlc3QwMVwiLCAxMTExMSwgdHJ1ZSwgKHN0ciwgbnVtLCBiKSA9PiB7IGNvbnNvbGUubG9nKHN0ciwgbnVtLCBiKTsgfSlcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgICAgIC8vIHRoaXMudmlldy5TZXR0aW5nQnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgLy8gRXZlbnRDZW50ZXIuZW1pdChcInNob3VndWlkZVwiLCBDb2Nvc0hlbHBlci5maW5kQ2hpbGRJbk5vZGUoXCJfQnV0dG9uUGx1cyRCYWdCdG5cIiwgdGhpcy5ub2RlKSk7XG4gICAgICAgIC8vIH0sIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuU2lnbkluQnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSVNpZ25Jbik7XG4gICAgICAgIH0sIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuRHJhd0J0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlMb3R0ZXJ5KTtcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgICAgIHRoaXMudmlldy5SYW5rQnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSVJhbmspO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LkNvbGxlY3RCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJQ29sbGVjdGlvbik7XG4gICAgICAgIH0sIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuU2hvcEJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlTaG9wKTtcbiAgICAgICAgICAgIC8vIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSURlYWwpXG4gICAgICAgIH0sIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuTXlCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJTXkpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LlRhc2tCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJVGFzayk7XG4gICAgICAgIH0sIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuRnJpZW5kQnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSUZyaWVuZClcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgfVxuXG4gICAgLy/liJ3lp4vljJbmmL7npLrmlbDmja5cbiAgICBpbml0RGF0YSgpIHtcbiAgICAgICAgLy8gQ29jb3NIZWxwZXIubG9hZEhlYWQoXCJodHRwczovL3BpYy5zb2dvdS5jb20vZD9xdWVyeT0lRTUlQTQlQjQlRTUlODMlOEYmZm9yYmlkcWM9JmVudGl0eWlkPSZwcmVRdWVyeT0mcmF3UXVlcnk9JnF1ZXJ5TGlzdD0mc3Q9MTkxJmRpZD01XCIsIHRoaXMudmlldy5IZWFkU3ApO1xuICAgICAgICAvLyBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoID0gWzEsIDEsIDIsIDMsIDQsIDQsIDUsIDUsIDYsIDddO1xuICAgICAgICB0aGlzLnZpZXcuQnV5Q29pbk51bVR4dC5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyQ3VyTmVlZEJ1eUNvaW5OdW0udG9TdHJpbmcoKTtcbiAgICAgICAgdGhpcy52aWV3Lk1ha2VNb25leVR4dC5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyTWFrZUNvaW4udG9TdHJpbmcoKTtcbiAgICAgICAgdGhpcy52aWV3LkNvaW5UeHQuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlckNvaW4udG9TdHJpbmcoKTtcbiAgICAgICAgdGhpcy52aWV3LkRpYW1vbmRUeHQuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlckRpYW1vbmQudG9TdHJpbmcoKTtcbiAgICAgICAgdGhpcy52aWV3Lk1vbmV5VHh0LnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJNb25leS50b1N0cmluZygpO1xuICAgIH1cblxuICAgIC8v5Yid5aeL5YyW5bel5L2c5Y+w5L+h5oGvXG4gICAgaW5pdFdvcmtiZW5jaCgpIHtcbiAgICAgICAgbGV0IGFyckNoaWxkcmVuID0gdGhpcy52aWV3LldvcmtDb250ZW50LmNoaWxkcmVuO1xuICAgICAgICBpZiAoYXJyQ2hpbGRyZW4ubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgLy/lt7Lnu4/liJvlu7rov4fvvIzkuI3pnIDopoHlho3liJvlu7pcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAobGV0IGlkeCA9IDA7IGlkeCA8IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2gubGVuZ3RoOyBpZHgrKykge1xuICAgICAgICAgICAgbGV0IGl0ZW1fbm9kZSA9IGNjLmluc3RhbnRpYXRlKHRoaXMuaXRlbVByb2R1Y2VQcmVmYWIpO1xuICAgICAgICAgICAgdGhpcy52aWV3LldvcmtDb250ZW50LmFkZENoaWxkKGl0ZW1fbm9kZSk7XG4gICAgICAgICAgICBpdGVtX25vZGUuc2V0UG9zaXRpb24odGhpcy5pdGVtX3Bvc19hcnJbaWR4XSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnZpZXcuV29ya0NvbnRlbnQuY2hpbGRyZW4uZm9yRWFjaCgoaXRlbU5vZGUsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICBsZXQgaXRlbUluZm8gPSBpdGVtTm9kZS5nZXRDb21wb25lbnQoJ0l0ZW1Qcm9kdWNlJyk7XG4gICAgICAgICAgICBpZiAoaW5kZXggPCBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgIGl0ZW1JbmZvLnNldFdvcmtiZW5jaEl0ZW1JbmZvKGluZGV4LCBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoW2luZGV4XSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHRoaXMpO1xuICAgIH1cblxuICAgIG9uVG91Y2hTdGFydCh0b3VjaEV2ZW50OiBjYy5Ub3VjaCkge1xuICAgICAgICB0aGlzLl9jdXJyZW50Tm9kZSA9IHRoaXMuZ2V0Q3VycmVudE5vZGVCeVRvdWNoUG9zKHRvdWNoRXZlbnQuZ2V0TG9jYXRpb24oKSk7XG4gICAgICAgIGlmICh0aGlzLl9jdXJyZW50Tm9kZSkge1xuICAgICAgICAgICAgbGV0IEl0ZW1Qcm9kdWNlID0gdGhpcy5fY3VycmVudE5vZGUuZ2V0Q29tcG9uZW50KCdJdGVtUHJvZHVjZScpO1xuICAgICAgICAgICAgdGhpcy5faXNDYW5EcmFnID0gSXRlbVByb2R1Y2UuZHJhZ1N0YXJ0KCk7XG4gICAgICAgICAgICBpZiAodGhpcy5faXNDYW5EcmFnKSB7XG4gICAgICAgICAgICAgICAgbGV0IGN1cnJlbnRXb3JsZFBvcyA9IHRoaXMuX2N1cnJlbnROb2RlLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihjYy52MigwLCAwKSk7XG4gICAgICAgICAgICAgICAgdGhpcy5fb2Zmc2V0UG9zID0gY3VycmVudFdvcmxkUG9zLnN1Yih0b3VjaEV2ZW50LmdldExvY2F0aW9uKCkpO1xuICAgICAgICAgICAgICAgIHRoaXMuc2hvd1ZpcnR1YWxJdGVtKEl0ZW1Qcm9kdWNlLmdldEluZm8oKSwgdG91Y2hFdmVudC5nZXRMb2NhdGlvbigpKTtcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dQcm9kdWNlKEl0ZW1Qcm9kdWNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uVG91Y2hNb3ZlKHRvdWNoRXZlbnQ6IGNjLlRvdWNoKSB7XG4gICAgICAgIGlmICghdGhpcy5faXNDYW5EcmFnKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnVwZGF0ZVZpcnV0YWxJdGVtUG9zKHRvdWNoRXZlbnQuZ2V0TG9jYXRpb24oKSk7XG4gICAgfVxuXG4gICAgb25Ub3VjaEVuZCh0b3VjaEV2ZW50OiBjYy5Ub3VjaCkge1xuICAgICAgICBpZiAoIXRoaXMuX2N1cnJlbnROb2RlKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmhpZGVQcm9kdWNlKCk7XG4gICAgICAgIGxldCBjYWtlSXRlbSA9IHRoaXMuX2N1cnJlbnROb2RlLmdldENvbXBvbmVudCgnSXRlbVByb2R1Y2UnKTtcbiAgICAgICAgY2FrZUl0ZW0uZHJhZ092ZXIoKTtcbiAgICAgICAgbGV0IHRvdWNoRW5kTm9kZSA9IHRoaXMuZ2V0Q3VycmVudE5vZGVCeVRvdWNoUG9zKHRvdWNoRXZlbnQuZ2V0TG9jYXRpb24oKSk7XG4gICAgICAgIGlmICh0aGlzLl9jdXJyZW50Tm9kZSA9PT0gdG91Y2hFbmROb2RlKSB7XG4gICAgICAgICAgICAvL+WxnuS6jueCueWHu+iHquW3sVxuICAgICAgICAgICAgdGhpcy5oaWRlVmlydHVhbEl0ZW0odHJ1ZSk7XG4gICAgICAgICAgICB0aGlzLl9jdXJyZW50Tm9kZSA9IG51bGw7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIXRoaXMuX2lzQ2FuRHJhZykge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCF0b3VjaEVuZE5vZGUpIHtcbiAgICAgICAgICAgIC8vVE9ETyDliKTmlq3mmK/lkKbmi5bmi73liLDnlJ/kuqflj7DkuIrvvIzmsqHmnInliJnmmK/mi5bmi73liLDml6DmlYjkvY3nva5cbiAgICAgICAgICAgIGxldCBpc0RyYWdUb1JlY292ZXJ5ID0gdGhpcy5jaGVja0lzRHJhZ1RvUmVjb3ZlcnkoY2FrZUl0ZW0uX2luZGV4LCB0b3VjaEV2ZW50LmdldExvY2F0aW9uKCkpO1xuICAgICAgICAgICAgaWYgKCFpc0RyYWdUb1JlY292ZXJ5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5oaWRlVmlydHVhbEl0ZW0odHJ1ZSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuaGlkZVZpcnR1YWxJdGVtKGZhbHNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8v5Yik5pat5piv5Lqk5o2i5L2N572u6L+Y5piv5LiO5LmL5ZCI5oiQXG4gICAgICAgICAgICB0aGlzLmNvbWJpbk9yU3dhcCh0aGlzLl9jdXJyZW50Tm9kZSwgdG91Y2hFbmROb2RlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuX2N1cnJlbnROb2RlID0gbnVsbDtcbiAgICAgICAgdGhpcy5faXNDYW5EcmFnID0gZmFsc2U7XG4gICAgfVxuXG4gICAgb25Ub3VjaENhbmNlbCh0b3VjaEV2ZW50OiBjYy5Ub3VjaCkge1xuICAgICAgICBpZiAoIXRoaXMuX2N1cnJlbnROb2RlKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgSXRlbVByb2R1Y2UgPSB0aGlzLl9jdXJyZW50Tm9kZS5nZXRDb21wb25lbnQoJ0l0ZW1Qcm9kdWNlJyk7XG5cbiAgICAgICAgaWYgKCFJdGVtUHJvZHVjZS5faXRlbUlkKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmhpZGVQcm9kdWNlKCk7XG5cbiAgICAgICAgSXRlbVByb2R1Y2UuZHJhZ092ZXIoKTtcbiAgICAgICAgaWYgKEl0ZW1Qcm9kdWNlLmlzVXNlZCkge1xuICAgICAgICAgICAgdGhpcy5fY3VycmVudE5vZGUgPSBudWxsO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgLy/mo4Dmn6XkuIvmmK/lkKbmi5bmi73liLDlm57mlLbnq5nkuoZcbiAgICAgICAgbGV0IGlzRHJhZ1RvUmVjb3ZlcnkgPSB0aGlzLmNoZWNrSXNEcmFnVG9SZWNvdmVyeShJdGVtUHJvZHVjZS5faW5kZXgsIHRvdWNoRXZlbnQuZ2V0TG9jYXRpb24oKSk7XG4gICAgICAgIGlmICghaXNEcmFnVG9SZWNvdmVyeSkge1xuICAgICAgICAgICAgLy/mi5bliLDkuobml6DmlYjkvY3nva5cbiAgICAgICAgICAgIHRoaXMuaGlkZVZpcnR1YWxJdGVtKHRydWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy/lj5Hotbflm57mlLbnmoTor7fmsYIs5q2k5aSE5bey55SxcmVjb3ZlcnnmjqXmlLbotbBcbiAgICAgICAgICAgIHRoaXMuaGlkZVZpcnR1YWxJdGVtKGZhbHNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuX2N1cnJlbnROb2RlID0gbnVsbDtcbiAgICB9XG5cbiAgICBnZXRDdXJyZW50Tm9kZUJ5VG91Y2hQb3MocG9zOiBjYy5WZWMyKSB7XG4gICAgICAgIGxldCBhcnJOb2RlID0gdGhpcy52aWV3LldvcmtDb250ZW50LmNoaWxkcmVuO1xuICAgICAgICByZXR1cm4gYXJyTm9kZS5maW5kKChpdGVtTm9kZSwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIHZhciBib3ggPSBpdGVtTm9kZS5nZXRCb3VuZGluZ0JveFRvV29ybGQoKTtcbiAgICAgICAgICAgIGlmIChib3guY29udGFpbnMocG9zKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCB0aGlzKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmi5bmi73ml7bljp/niankvZPljYrpgI/mmI7vvIznhLblkI7mi5bmi73nmoTlhbblrp7mmK/kuIDkuKromZrmi5/nmoTljYrpgI/mmI7niankvZNcbiAgICAgKi9cbiAgICBzaG93VmlydHVhbEl0ZW0oaW5mbzogYW55LCBwb3M6IGNjLlZlYzIpIHtcbiAgICAgICAgaWYgKCF0aGlzLl92aXJ0dWFsTm9kZSkge1xuICAgICAgICAgICAgdGhpcy5fdmlydHVhbE5vZGUgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLml0ZW1Qcm9kdWNlUHJlZmFiKTtcbiAgICAgICAgICAgIHRoaXMuX3ZpcnR1YWxOb2RlLnBhcmVudCA9IHRoaXMubm9kZS5wYXJlbnQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLl92aXJ0dWFsTm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5fdmlydHVhbE5vZGUuc3RvcEFjdGlvbkJ5VGFnKFRBR19WSVJUVUFMX01PVkUpO1xuICAgICAgICBsZXQgY2FrZUl0ZW0gPSB0aGlzLl92aXJ0dWFsTm9kZS5nZXRDb21wb25lbnQoJ0l0ZW1Qcm9kdWNlJyk7XG4gICAgICAgIGNha2VJdGVtLnNldFdvcmtiZW5jaEl0ZW1JbmZvKC0xLCBpbmZvKTtcbiAgICAgICAgY2FrZUl0ZW0uc2hvd1ZpcnR1YWwoKTtcbiAgICAgICAgdGhpcy51cGRhdGVWaXJ1dGFsSXRlbVBvcyhwb3MpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOmakOiXj+aLluaLveaXtueahOWNiumAj+aYjuWbvuagh1xuICAgICAqL1xuICAgIGhpZGVWaXJ0dWFsSXRlbShpc0dvQmFjazogYm9vbGVhbikge1xuICAgICAgICBpZiAodGhpcy5fdmlydHVhbE5vZGUgJiYgdGhpcy5fdmlydHVhbE5vZGUuYWN0aXZlKSB7XG4gICAgICAgICAgICBpZiAoaXNHb0JhY2spIHtcbiAgICAgICAgICAgICAgICAvLyB0aGlzLmN1cnJlbnROb2RlLmNvbnZlcnR0b3dcbiAgICAgICAgICAgICAgICBsZXQgcG9zV29ybGQgPSB0aGlzLl9jdXJyZW50Tm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoY2MudjIpO1xuICAgICAgICAgICAgICAgIGxldCBwb3NDdXJyZW50Tm9kZSA9IHRoaXMubm9kZS5wYXJlbnQuY29udmVydFRvTm9kZVNwYWNlQVIocG9zV29ybGQpO1xuICAgICAgICAgICAgICAgIGxldCBkdXJhdGlvbiA9IHBvc0N1cnJlbnROb2RlLnN1Yih0aGlzLl92aXJ0dWFsTm9kZS5wb3NpdGlvbikubWFnKCkgLyBTUEVFRF9WSVJVVEFMX01PVkU7XG4gICAgICAgICAgICAgICAgQ29jb3NIZWxwZXIucnVuVHdlZW5TeW5jKHRoaXMuX3ZpcnR1YWxOb2RlLCBjYy50d2VlbigpLnRvKDAsIGR1cmF0aW9uKS5jYWxsKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdmlydHVhbE5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSkpXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuX3ZpcnR1YWxOb2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5pu05paw5ouW5ou95pe255qE5Y2K6YCP5piO5Zu+5qCH6ZqP5omL5oyH56e75Yqo5L2N572uXG4gICAgICovXG4gICAgdXBkYXRlVmlydXRhbEl0ZW1Qb3MocG9zOiBjYy5WZWMyKSB7XG4gICAgICAgIGlmICh0aGlzLl92aXJ0dWFsTm9kZSAmJiB0aGlzLl92aXJ0dWFsTm9kZS5hY3RpdmUpIHtcbiAgICAgICAgICAgIC8v6ZyA6KaB5Z2Q5qCH6L2s5o2iXG4gICAgICAgICAgICB2YXIgcG9zTm9kZVNwYWNlID0gdGhpcy5ub2RlLnBhcmVudC5jb252ZXJ0VG9Ob2RlU3BhY2VBUihwb3MpO1xuICAgICAgICAgICAgdGhpcy5fdmlydHVhbE5vZGUucG9zaXRpb24gPSBwb3NOb2RlU3BhY2UuYWRkKHRoaXMuX29mZnNldFBvcyk7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZURyYWdQb3MocG9zKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHVwZGF0ZURyYWdQb3MocG9zOiBjYy5WZWMyKSB7XG4gICAgICAgIGxldCBib3ggPSB0aGlzLnZpZXcuUmVjb3ZlcnlCdG4ubm9kZS5nZXRCb3VuZGluZ0JveFRvV29ybGQoKTtcbiAgICAgICAgaWYgKCFib3guY29udGFpbnMocG9zKSkge1xuICAgICAgICAgICAgaWYgKHRoaXMudmlldy5SZWNvdmVyeUJ0bi5ub2RlLnNjYWxlID09PSAxLjEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXcuUmVjb3ZlcnlCdG4ubm9kZS5zY2FsZSA9IDE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy52aWV3LlJlY292ZXJ5QnRuLm5vZGUuc2NhbGUgPT09IDEpIHtcbiAgICAgICAgICAgIHRoaXMudmlldy5SZWNvdmVyeUJ0bi5ub2RlLnNjYWxlID0gMS4xO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy/mo4Dmn6XmmK/lkKbmi5bliLDlm57mlLbnq5nkuoZcbiAgICBhc3luYyBjaGVja0lzRHJhZ1RvUmVjb3Zlcnkod29ya2JlbmNoSWR4OiBudW1iZXIsIHBvczogY2MuVmVjMikge1xuICAgICAgICBsZXQgYm94ID0gdGhpcy52aWV3LlJlY292ZXJ5QnRuLm5vZGUuZ2V0Qm91bmRpbmdCb3hUb1dvcmxkKCk7XG5cbiAgICAgICAgaWYgKCFib3guY29udGFpbnMocG9zKSkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGxldCBkYXRhcyA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiV29ya0JlbmNoQ3RybFwiLCB7IHVzZXJBY2NvdW50OiBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyQWNjb3VudCwgY3RybFR5cGU6IE9wZXJhdGlvblR5cGUuc2VsbCwgY3VyUG9zOiB3b3JrYmVuY2hJZHggfSlcbiAgICAgICAgaWYgKCFkYXRhcy5pc1N1Y2MpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoZGF0YXMuZXJyLm1lc3NhZ2UpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy51cGRhdGVXb3JrYmVuY2god29ya2JlbmNoSWR4LCAtMSwgXCJSZWNvdmVyeVwiKTtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy/ngrnlh7vmi5bliqjml7bmkq3mlL7nm7jlkIznrYnnuqfnianlk4HnmoTliqjnlLtcbiAgICBzaG93UHJvZHVjZShpdGVtUHJvZHVjZTogSXRlbVByb2R1Y2UpIHtcbiAgICAgICAgbGV0IGluZGV4ID0gaXRlbVByb2R1Y2UuX2luZGV4O1xuICAgICAgICBsZXQgaXRlbUlkID0gaXRlbVByb2R1Y2UuX2l0ZW1JZDtcbiAgICAgICAgbGV0IGFyck5vZGUgPSB0aGlzLnZpZXcuV29ya0NvbnRlbnQuY2hpbGRyZW47XG5cbiAgICAgICAgZm9yIChsZXQgaWR4ID0gMDsgaWR4IDwgYXJyTm9kZS5sZW5ndGg7IGlkeCsrKSB7XG4gICAgICAgICAgICBpZiAoaWR4ID09PSBpbmRleCkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsZXQgcHJvZHVjZSA9IGFyck5vZGVbaWR4XS5nZXRDb21wb25lbnQoJ0l0ZW1Qcm9kdWNlJyk7XG4gICAgICAgICAgICBpZiAoIXByb2R1Y2UuaXNVc2VkICYmIHByb2R1Y2UuaXRlbUlkID09PSBpdGVtSWQpIHtcbiAgICAgICAgICAgICAgICAvLyBwcm9kdWNlLnBsYXlQcm9kdWNlQW5pKHRydWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy/pmpDol4/mi5bmi73ml7bnmoTlm77moIdcbiAgICBoaWRlUHJvZHVjZSgpIHtcbiAgICAgICAgbGV0IGFyck5vZGUgPSB0aGlzLnZpZXcuV29ya0NvbnRlbnQuY2hpbGRyZW47XG4gICAgICAgIGZvciAobGV0IGlkeCA9IDA7IGlkeCA8IGFyck5vZGUubGVuZ3RoOyBpZHgrKykge1xuICAgICAgICAgICAgbGV0IHByb2R1Y2UgPSBhcnJOb2RlW2lkeF0uZ2V0Q29tcG9uZW50KCdJdGVtUHJvZHVjZScpO1xuICAgICAgICAgICAgaWYgKCFwcm9kdWNlLmlzVXNlZCkge1xuICAgICAgICAgICAgICAgIC8vIHByb2R1Y2UucGxheVByb2R1Y2VBbmkoZmFsc2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5Yik5pat5piv5Lqk5o2i6L+Y5piv57uT5ZCIXG4gICAgICovXG4gICAgYXN5bmMgY29tYmluT3JTd2FwKG9yaWdpbk5vZGU6IGNjLk5vZGUsIHRhcmdldE5vZGU6IGNjLk5vZGUpIHtcbiAgICAgICAgbGV0IG9yaWdpblNjcmlwdCA9IG9yaWdpbk5vZGUuZ2V0Q29tcG9uZW50KCdJdGVtUHJvZHVjZScpO1xuICAgICAgICBsZXQgdGFyZ2V0U2NyaXB0ID0gdGFyZ2V0Tm9kZS5nZXRDb21wb25lbnQoJ0l0ZW1Qcm9kdWNlJyk7XG4gICAgICAgIGlmIChvcmlnaW5TY3JpcHQuaXNVc2VkIHx8IHRhcmdldFNjcmlwdC5pc1VzZWQpIHtcbiAgICAgICAgICAgIHRoaXMuaGlkZVZpcnR1YWxJdGVtKHRydWUpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IG9yaWdpbkl0ZW0gPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoW29yaWdpblNjcmlwdC5faW5kZXhdO1xuICAgICAgICBsZXQgdGFyZ2V0SXRlbSA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2hbdGFyZ2V0U2NyaXB0Ll9pbmRleF07XG4gICAgICAgIGlmIChvcmlnaW5JdGVtID09IG51bGwgfHwgdGFyZ2V0SXRlbSA9PSBudWxsKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGRhdGFzID0gYXdhaXQgYXBpQ2xpZW50LmNhbGxBcGkoXCJXb3JrQmVuY2hDdHJsXCIsIHsgdXNlckFjY291bnQ6IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJBY2NvdW50LCBjdHJsVHlwZTogT3BlcmF0aW9uVHlwZS5jb25jYXQsIGN1clBvczogb3JpZ2luU2NyaXB0Ll9pbmRleCwgdGFyZ2V0OiB0YXJnZXRTY3JpcHQuX2luZGV4IH0pXG4gICAgICAgIGlmICghZGF0YXMuaXNTdWNjKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKFwi5pWw5o2u6ZSZ6K+vXCIpO1xuICAgICAgICAgICAgdGhpcy51cGRhdGVXb3JrYmVuY2gob3JpZ2luU2NyaXB0Ll9pbmRleCwgdGFyZ2V0U2NyaXB0Ll9pbmRleCwgXCJFeGNoYW5nZVwiKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBpZiAob3JpZ2luU2NyaXB0Ll9pdGVtSWQgPT09IHRhcmdldFNjcmlwdC5faXRlbUlkKSB7XG4gICAgICAgICAgICB0aGlzLmNvbWJpblByb2R1Y2Uob3JpZ2luU2NyaXB0Ll9pbmRleCwgdGFyZ2V0U2NyaXB0Ll9pbmRleCwgZGF0YXMucmVzLnVzZXJXb3JrQmVuY2hbdGFyZ2V0U2NyaXB0Ll9pbmRleF0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy/kvY3nva7kuqTmjaJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiamlhb2h1YW5cIilcbiAgICAgICAgICAgIGlmICh0aGlzLnN3YXBQcm9kdWNlKG9yaWdpblNjcmlwdC5faW5kZXgsIHRhcmdldFNjcmlwdC5faW5kZXgpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGVXb3JrYmVuY2gob3JpZ2luU2NyaXB0Ll9pbmRleCwgdGFyZ2V0U2NyaXB0Ll9pbmRleCwgXCJFeGNoYW5nZVwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLmhpZGVWaXJ0dWFsSXRlbShmYWxzZSk7XG4gICAgfVxuXG4gICAgLy/mm7TmlrDlt6XkvZzlj7BcbiAgICB1cGRhdGVXb3JrYmVuY2gob3JpZ2luSW5kZXg6IG51bWJlciwgdGFyZ2V0SW5kZXg6IG51bWJlciwgdHlwZTogc3RyaW5nKSB7XG4gICAgICAgIGlmICghdHlwZSkge1xuICAgICAgICAgICAgdGhpcy5pbml0V29ya2JlbmNoKCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgb3JpZ2luSW5mbzogSXRlbVByb2R1Y2U7XG4gICAgICAgIGxldCB0YXJnZXRJbmZvOiBJdGVtUHJvZHVjZTtcbiAgICAgICAgaWYgKG9yaWdpbkluZGV4ID49IDApIHtcbiAgICAgICAgICAgIGxldCBvcmlnaW5Ob2RlID0gdGhpcy52aWV3LldvcmtDb250ZW50LmNoaWxkcmVuW29yaWdpbkluZGV4XTtcbiAgICAgICAgICAgIG9yaWdpbkluZm8gPSBvcmlnaW5Ob2RlLmdldENvbXBvbmVudCgnSXRlbVByb2R1Y2UnKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGFyZ2V0SW5kZXggPj0gMCkge1xuICAgICAgICAgICAgbGV0IHRhcmdldE5vZGUgPSB0aGlzLnZpZXcuV29ya0NvbnRlbnQuY2hpbGRyZW5bdGFyZ2V0SW5kZXhdO1xuICAgICAgICAgICAgdGFyZ2V0SW5mbyA9IHRhcmdldE5vZGUuZ2V0Q29tcG9uZW50KCdJdGVtUHJvZHVjZScpO1xuICAgICAgICB9XG5cbiAgICAgICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICAgICAgICBjYXNlIFwiRXhjaGFuZ2VcIjpcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIuS6pOaNolwiKVxuICAgICAgICAgICAgICAgIHRhcmdldEluZm8uc2V0V29ya2JlbmNoSXRlbUluZm8odGFyZ2V0SW5mby5faW5kZXgsIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2hbdGFyZ2V0SW5mby5faW5kZXhdKTtcbiAgICAgICAgICAgICAgICBvcmlnaW5JbmZvLnNldFdvcmtiZW5jaEl0ZW1JbmZvKG9yaWdpbkluZm8uX2luZGV4LCBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoW29yaWdpbkluZm8uX2luZGV4XSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwiQ29tYmluZVwiOlxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi57uE5ZCIXCIpO1xuICAgICAgICAgICAgICAgIG9yaWdpbkluZm8uc2V0V29ya2JlbmNoSXRlbUluZm8ob3JpZ2luSW5mby5faW5kZXgsIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2hbb3JpZ2luSW5mby5faW5kZXhdKTtcbiAgICAgICAgICAgICAgICB0YXJnZXRJbmZvLnNldFdvcmtiZW5jaEl0ZW1JbmZvKHRhcmdldEluZm8uX2luZGV4LCBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoW3RhcmdldEluZm8uX2luZGV4XSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwiUHJvZHVjZVwiOlxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi55Sf5LqnXCIpO1xuICAgICAgICAgICAgICAgIHRhcmdldEluZm8uc2V0V29ya2JlbmNoSXRlbUluZm8odGFyZ2V0SW5mby5faW5kZXgsIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2hbdGFyZ2V0SW5mby5faW5kZXhdKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgXCJSZWNvdmVyeVwiOlxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi5Zue5pS2XCIpO1xuICAgICAgICAgICAgICAgIG9yaWdpbkluZm8uc2V0V29ya2JlbmNoSXRlbUluZm8ob3JpZ2luSW5mby5faW5kZXgsIDApO1xuICAgICAgICAgICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2hbb3JpZ2luSW5mby5faW5kZXhdID0gMDtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuXG4gICAgfVxuICAgIC8qKlxuICAgICAqIOS6pOaNoueUn+S6p+eJqeS9jee9rlxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBvcmlnaW5JbmRleCBcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gdGFyZ2V0SW5kZXggXG4gICAgICovXG4gICAgc3dhcFByb2R1Y2Uob3JpZ2luSW5kZXg6IG51bWJlciwgdGFyZ2V0SW5kZXg6IG51bWJlcikge1xuICAgICAgICBpZiAoIUdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2gpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvcmlnaW5JbmRleCA+PSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoLmxlbmd0aCB8fCB0YXJnZXRJbmRleCA+PSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoLmxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IG9yaWdpbkl0ZW1JZCA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2hbb3JpZ2luSW5kZXhdO1xuICAgICAgICBsZXQgdGFyZ2V0SXRlbUlkID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlcldvcmtiZW5jaFt0YXJnZXRJbmRleF07XG5cbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlcldvcmtiZW5jaFt0YXJnZXRJbmRleF0gPSBvcmlnaW5JdGVtSWQ7XG4gICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2hbb3JpZ2luSW5kZXhdID0gdGFyZ2V0SXRlbUlkO1xuXG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOeUn+S6p+eJqeWQiOW5tlxuICAgICAqIEBwYXJhbSB7TnVtYmVyfSBvcmlnaW5JbmRleCDljp/kvY3nva7nmoRpbmRleCBcbiAgICAgKiBAcGFyYW0ge051bWJlcn0gdGFyZ2V0SW5kZXgg55uu5qCH5L2N572u55qEaW5kZXhcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gaXRlbUlkIOeJqeWTgWlkXG4gICAgICovXG4gICAgY29tYmluUHJvZHVjZShvcmlnaW5JbmRleDogbnVtYmVyLCB0YXJnZXRJbmRleDogbnVtYmVyLCBpdGVtSWQ6IG51bWJlcikge1xuICAgICAgICBsZXQgb3JpZ2luTm9kZSA9IHRoaXMudmlldy5Xb3JrQ29udGVudC5jaGlsZHJlbltvcmlnaW5JbmRleF07XG4gICAgICAgIGxldCB0YXJnZXROb2RlID0gdGhpcy52aWV3LldvcmtDb250ZW50LmNoaWxkcmVuW3RhcmdldEluZGV4XTtcblxuICAgICAgICBsZXQgb3JpZ2luU2NyaXB0ID0gb3JpZ2luTm9kZS5nZXRDb21wb25lbnQoJ0l0ZW1Qcm9kdWNlJyk7XG4gICAgICAgIGxldCB0YXJnZXRTY3JpcHQgPSB0YXJnZXROb2RlLmdldENvbXBvbmVudCgnSXRlbVByb2R1Y2UnKTtcblxuICAgICAgICBvcmlnaW5TY3JpcHQuaXNVc2VkID0gdHJ1ZTsgLy/opoHnu5PlkIjvvIzlhYjlsIbkuKTkuKrnlJ/kuqfniannva7kuLrkuI3lj6/mi5bmi71cbiAgICAgICAgdGFyZ2V0U2NyaXB0LmlzVXNlZCA9IHRydWU7XG5cbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlcldvcmtiZW5jaFtvcmlnaW5JbmRleF0gPSAwO1xuICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoW3RhcmdldEluZGV4XSA9IGl0ZW1JZDtcblxuICAgICAgICB0aGlzLnVwZGF0ZVdvcmtiZW5jaChvcmlnaW5TY3JpcHQuX2luZGV4LCB0YXJnZXRTY3JpcHQuX2luZGV4LCBcIkNvbWJpbmVcIik7XG4gICAgfVxuXG4gICAgLy/otK3kubDkuovku7ZcbiAgICBhc3luYyBvbkJ1eVByb2R1Y2VDbGljaygpIHtcbiAgICAgICAgbGV0IGlzRW5vdWdoID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlckNvaW4gPj0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ3VyQnV5UHJvZHVjZUluZm8uYnV5UHJpY2U7XG4gICAgICAgIGlmICghaXNFbm91Z2gpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoXCLph5HluIHkuI3otrPvvIFcIik7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCFHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby5oYXNQb3NBdFdvcmtiZW5jaCgpKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKFwi6K+35YWI5Ye65ZSu5aSa5L2Z54mp5ZOB77yBXCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGluZGV4ID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8uY2hlY2tOdWxsUG9zKCk7XG4gICAgICAgIGxldCBkYXRhcyA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiV29ya0JlbmNoQ3RybFwiLCB7IHVzZXJBY2NvdW50OiBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyQWNjb3VudCwgY3RybFR5cGU6IE9wZXJhdGlvblR5cGUuYnV5LCBjdXJQb3M6IGluZGV4IH0pXG4gICAgICAgIGlmICghZGF0YXMuaXNTdWNjKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKGRhdGFzLmVyci5tZXNzYWdlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyV29ya2JlbmNoW2luZGV4XSA9IGRhdGFzLnJlcy51c2VyV29ya0JlbmNoW2luZGV4XTtcbiAgICAgICAgdGhpcy51cGRhdGVXb3JrYmVuY2goLTEsIGluZGV4LCBcIlByb2R1Y2VcIik7XG4gICAgfVxuXG4gICAgLy/oh6rliqjlkIjmiJBcbiAgICBvbkJ0bkNvbWJpbmVBdXRvQ2xpY2soKSB7XG4gICAgICAgIGlmICh0aGlzLl9pc0NvbWJpbmluZykge1xuICAgICAgICAgICAgVUlUb2FzdC5wb3BVcChcIuato+WcqOiHquWKqOWQiOaIkOS4rVwiKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBsZXQgYXQgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby5jb21iaW5lQXV0b1RpbWU7XG4gICAgICAgIGlmICghYXQgfHwgYXQgPD0gMCkge1xuICAgICAgICAgICAgLy/lgZzmraLoh6rliqjlkIjmiJBcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVDb21iaW5lT3ZlcigpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCF0aGlzLl9pc0NvbWJpbmluZykge1xuICAgICAgICAgICAgdGhpcy5jaGVja0lzQ29tYmluZUF1dG8oKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8v5qOA5p+l5piv5ZCm5q2j5Zyo6Ieq5Yqo5ZCI5oiQXG4gICAgY2hlY2tJc0NvbWJpbmVBdXRvKCkge1xuICAgICAgICBsZXQgc3BhcmVUaW1lID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8uY29tYmluZUF1dG9UaW1lO1xuICAgICAgICBpZiAoIXNwYXJlVGltZSB8fCBzcGFyZVRpbWUgPD0gMCkge1xuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZUNvbWJpbmVPdmVyKCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5faXNDb21iaW5pbmcgPSB0cnVlO1xuICAgICAgICB0aGlzLnNjaGVkdWxlKHRoaXMuY29tYmluZUF1dG8sIDEpO1xuICAgICAgICB0aGlzLnZpZXcuQ29tYmluZUF1dG9UeHQuc3RyaW5nID0gQ29jb3NIZWxwZXIuZm9ybWF0VGltZUZvclNlY29uZCgxNSk7XG4gICAgICAgIHRoaXMudmlldy5Db21iaW5lQXV0b1R4dC5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogXG4gICAgICog6Ieq5Yqo5ZCI5oiQIFxuICAgICAqL1xuICAgIGNvbWJpbmVBdXRvKCkge1xuICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby5jb21iaW5lQXV0b1RpbWUgLT0gMTtcbiAgICAgICAgaWYgKEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLmNvbWJpbmVBdXRvVGltZSA8PSAwKSB7XG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlQ29tYmluZU92ZXIoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnZpZXcuQ29tYmluZUF1dG9UeHQuc3RyaW5nID0gQ29jb3NIZWxwZXIuZm9ybWF0VGltZUZvclNlY29uZChHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby5jb21iaW5lQXV0b1RpbWUpO1xuXG4gICAgICAgIGxldCBhcnJTb3J0ID0gW107XG4gICAgICAgIGZvciAobGV0IGlkeCA9IDA7IGlkeCA8IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlVzZXJJbmZvLnVzZXJXb3JrYmVuY2gubGVuZ3RoOyBpZHgrKykge1xuICAgICAgICAgICAgbGV0IGNha2VOb2RlID0gdGhpcy52aWV3LldvcmtDb250ZW50LmNoaWxkcmVuW2lkeF07XG4gICAgICAgICAgICBsZXQgY2FrZUl0ZW0gPSBjYWtlTm9kZS5nZXRDb21wb25lbnQoJ0l0ZW1Qcm9kdWNlJyk7XG4gICAgICAgICAgICBsZXQgaXNVc2VkID0gY2FrZUl0ZW0uaXNVc2VkO1xuICAgICAgICAgICAgaWYgKGlzVXNlZCkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsZXQgaXRlbUlkID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlcldvcmtiZW5jaFtpZHhdO1xuICAgICAgICAgICAgaWYgKGl0ZW1JZCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlOyAvL+ekvOebkuaIluiAheaYr+epuuS9jee9ruimgeWxj+iUveaOiVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBhcnJTb3J0LnB1c2goeyBpdGVtSWQ6IGl0ZW1JZCwgaW5kZXg6IGlkeCB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8v5oyJ54Wn6JuL57OVaWTov5vooYzmjpLluo9cbiAgICAgICAgYXJyU29ydC5zb3J0KChhLCBiKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKGEuaXRlbUlkKSAtIE51bWJlcihiLml0ZW1JZCk7XG4gICAgICAgIH0pO1xuICAgICAgICBsZXQgb3JpZ2luSW5kZXggPSAtMTtcbiAgICAgICAgbGV0IHRhcmdldEluZGV4ID0gLTE7XG5cbiAgICAgICAgZm9yIChsZXQgaWR4ID0gMDsgaWR4IDwgYXJyU29ydC5sZW5ndGggLSAxOyBpZHgrKykge1xuICAgICAgICAgICAgaWYgKGFyclNvcnRbaWR4XS5pdGVtSWQgPT09IGFyclNvcnRbaWR4ICsgMV0uaXRlbUlkICYmIGFyclNvcnRbaWR4XS5pdGVtSWQudG9TdHJpbmcoKSAhPT0gXCIzOFwiKSB7IC8v5o6S6Zmk5pyA6auY562J57qn77yM6YG/5YWN5pyA6auY562J57qn5b2x5ZON5ZCO57ut5ZCI5oiQXG4gICAgICAgICAgICAgICAgLy/mib7liLDnrYnnuqfmnIDkvY7nmoTom4vns5XkuoZcbiAgICAgICAgICAgICAgICB0YXJnZXRJbmRleCA9IGFyclNvcnRbaWR4XS5pbmRleDtcbiAgICAgICAgICAgICAgICBvcmlnaW5JbmRleCA9IGFyclNvcnRbaWR4ICsgMV0uaW5kZXg7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAob3JpZ2luSW5kZXggIT09IC0xICYmIHRhcmdldEluZGV4ICE9PSAtMSkge1xuICAgICAgICAgICAgaWYgKHRoaXMuX2N1cnJlbnROb2RlKSB7XG4gICAgICAgICAgICAgICAgbGV0IGNha2VJdGVtID0gdGhpcy5fY3VycmVudE5vZGUuZ2V0Q29tcG9uZW50KCdJdGVtUHJvZHVjZScpO1xuICAgICAgICAgICAgICAgIGlmIChjYWtlSXRlbS5pbmRleCA9PT0gb3JpZ2luSW5kZXggfHwgY2FrZUl0ZW0uaW5kZXggPT09IHRhcmdldEluZGV4KSB7XG4gICAgICAgICAgICAgICAgICAgIGNha2VJdGVtLmRyYWdPdmVyKCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGlkZVByb2R1Y2UoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fY3VycmVudE5vZGUgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhpZGVWaXJ0dWFsSXRlbShmYWxzZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmNvbWJpbk9yU3dhcCh0aGlzLnZpZXcuV29ya0NvbnRlbnQuY2hpbGRyZW5bb3JpZ2luSW5kZXhdLCB0aGlzLnZpZXcuV29ya0NvbnRlbnQuY2hpbGRyZW5bdGFyZ2V0SW5kZXhdKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWFs+mXreWQiOaIkOWAkuiuoeaXtlxuICAgICAqL1xuICAgIHNjaGVkdWxlQ29tYmluZU92ZXIoKSB7XG4gICAgICAgIHRoaXMuX2lzQ29tYmluaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMudmlldy5Db21iaW5lQXV0b1R4dC5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnVuc2NoZWR1bGUodGhpcy5jb21iaW5lQXV0byk7XG4gICAgfVxuXG59Il19