
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Rank/UIRank.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '247beHs7LJE8Zb9iy/PRZFc', 'UIRank');
// Script/UIScript/Rank/UIRank.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var Struct_1 = require("../../Common/Struct");
var SysDefine_1 = require("../../Common/SysDefine");
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var ItemRank_1 = require("./ItemRank");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIRank = /** @class */ (function (_super) {
    __extends(UIRank, _super);
    function UIRank() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.modalType = new Struct_1.ModalType(SysDefine_1.ModalOpacity.OpacityHigh);
        _this.listRank = null;
        _this.type = 0;
        _this.willDestory = false;
        return _this;
    }
    UIRank.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.RankToggleNode.children.forEach(function (item) {
            item.on(cc.Node.EventType.TOUCH_END, function (e) {
                _this.changeType();
            }, _this);
        });
        this.changeType();
    };
    UIRank.prototype.changeType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var index, data, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        index = CocosHelper_1.default.getContenerindex(this.view.RankToggleNode);
                        if (!(index == 0)) return [3 /*break*/, 2];
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("Rank", { type: 1 })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            this.listRank.numItems = 0;
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.DataRankInfo.rank_list = data.res.rank;
                        this.listRank.numItems = data.res.rank.length;
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, RpcConent_1.apiClient.callApi("Rank", { type: 2 })];
                    case 3:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            this.listRank.numItems = 0;
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.DataRankInfo.rank_list = data.res.rank;
                        this.listRank.numItems = data.res.rank.length;
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    //垂直列表渲染器
    UIRank.prototype.onListRankRender = function (item, idx) {
        var index = CocosHelper_1.default.getContenerindex(this.view.RankToggleNode);
        item.getComponent(ItemRank_1.default).setData(index, GameMgr_1.default.dataModalMgr.DataRankInfo.rank_list[idx], idx + 1);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIRank.prototype, "listRank", void 0);
    UIRank = __decorate([
        ccclass
    ], UIRank);
    return UIRank;
}(UIForm_1.UIWindow));
exports.default = UIRank;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvUmFuay9VSVJhbmsudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsNkRBQXdEO0FBQ3hELDhDQUFnRDtBQUNoRCxvREFBc0Q7QUFDdEQsOENBQStDO0FBQy9DLGlEQUE0QztBQUU1QyxpREFBZ0Q7QUFDaEQsdURBQWtEO0FBQ2xELHNDQUFpQztBQUNqQyx1Q0FBa0M7QUFFNUIsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBb0MsMEJBQVE7SUFBNUM7UUFBQSxxRUFxREM7UUFuREcsZUFBUyxHQUFHLElBQUksa0JBQVMsQ0FBQyx3QkFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBSXBELGNBQVEsR0FBYSxJQUFJLENBQUM7UUFDMUIsVUFBSSxHQUFXLENBQUMsQ0FBQztRQUVqQixpQkFBVyxHQUFHLEtBQUssQ0FBQzs7SUE0Q3hCLENBQUM7SUExQ0csc0JBQUssR0FBTDtRQUFBLGlCQVlDO1FBWEcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtZQUMzQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxVQUFDLENBQXNCO2dCQUN4RCxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLEtBQUksQ0FBQyxDQUFBO1FBQ1osQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVLLDJCQUFVLEdBQWhCOzs7Ozs7d0JBQ1EsS0FBSyxHQUFHLHFCQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzs2QkFDL0QsQ0FBQSxLQUFLLElBQUksQ0FBQyxDQUFBLEVBQVYsd0JBQVU7d0JBQ0MscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUFuRCxJQUFJLEdBQUcsU0FBNEM7d0JBQ3ZELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNkLGlCQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQ2hDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQzs0QkFDM0Isc0JBQU87eUJBQ1Y7d0JBQ0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQzt3QkFDNUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDOzs0QkFFbkMscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUFuRCxJQUFJLEdBQUcsU0FBNEM7d0JBQ3ZELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNkLGlCQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQ2hDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQzs0QkFDM0Isc0JBQU87eUJBQ1Y7d0JBQ0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQzt3QkFDNUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDOzs7Ozs7S0FFckQ7SUFFRCxTQUFTO0lBQ1QsaUNBQWdCLEdBQWhCLFVBQWlCLElBQWEsRUFBRSxHQUFXO1FBQ3ZDLElBQUksS0FBSyxHQUFHLHFCQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxDQUFDLGtCQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQzFHLENBQUM7SUE5Q0Q7UUFEQyxRQUFRLENBQUMsa0JBQVEsQ0FBQzs0Q0FDTztJQU5ULE1BQU07UUFEMUIsT0FBTztPQUNhLE1BQU0sQ0FxRDFCO0lBQUQsYUFBQztDQXJERCxBQXFEQyxDQXJEbUMsaUJBQVEsR0FxRDNDO2tCQXJEb0IsTUFBTSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBVSVJhbmtfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlSYW5rX0F1dG9cIjtcbmltcG9ydCBMaXN0VXRpbCBmcm9tIFwiLi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvTGlzdFV0aWxcIjtcbmltcG9ydCB7IE1vZGFsVHlwZSB9IGZyb20gXCIuLi8uLi9Db21tb24vU3RydWN0XCI7XG5pbXBvcnQgeyBNb2RhbE9wYWNpdHkgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1N5c0RlZmluZVwiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IHsgSVJhbmtJdGVtIH0gZnJvbSBcIi4uLy4uL01hbmFnZXIvSW50ZXJmYWNlTWdyXCI7XG5pbXBvcnQgeyBhcGlDbGllbnQgfSBmcm9tIFwiLi4vLi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcbmltcG9ydCBJdGVtUmFuayBmcm9tIFwiLi9JdGVtUmFua1wiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlSYW5rIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgbW9kYWxUeXBlID0gbmV3IE1vZGFsVHlwZShNb2RhbE9wYWNpdHkuT3BhY2l0eUhpZ2gpO1xuICAgIHZpZXc6IFVJUmFua19BdXRvO1xuXG4gICAgQHByb3BlcnR5KExpc3RVdGlsKVxuICAgIGxpc3RSYW5rOiBMaXN0VXRpbCA9IG51bGw7XG4gICAgdHlwZTogbnVtYmVyID0gMDtcblxuICAgIHdpbGxEZXN0b3J5ID0gZmFsc2U7XG5cbiAgICBzdGFydCgpIHtcbiAgICAgICAgdGhpcy52aWV3LkNsb3NlQnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY2xvc2VTZWxmKCk7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5SYW5rVG9nZ2xlTm9kZS5jaGlsZHJlbi5mb3JFYWNoKChpdGVtKSA9PiB7XG4gICAgICAgICAgICBpdGVtLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX0VORCwgKGU6IGNjLkV2ZW50LkV2ZW50VG91Y2gpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmNoYW5nZVR5cGUoKTtcbiAgICAgICAgICAgIH0sIHRoaXMpXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuY2hhbmdlVHlwZSgpO1xuICAgIH1cblxuICAgIGFzeW5jIGNoYW5nZVR5cGUoKSB7XG4gICAgICAgIGxldCBpbmRleCA9IENvY29zSGVscGVyLmdldENvbnRlbmVyaW5kZXgodGhpcy52aWV3LlJhbmtUb2dnbGVOb2RlKTtcbiAgICAgICAgaWYgKGluZGV4ID09IDApIHtcbiAgICAgICAgICAgIGxldCBkYXRhID0gYXdhaXQgYXBpQ2xpZW50LmNhbGxBcGkoXCJSYW5rXCIsIHsgdHlwZTogMSB9KTtcbiAgICAgICAgICAgIGlmICghZGF0YS5pc1N1Y2MpIHtcbiAgICAgICAgICAgICAgICBVSVRvYXN0LnBvcFVwKGRhdGEuZXJyLm1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdFJhbmsubnVtSXRlbXMgPSAwO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFSYW5rSW5mby5yYW5rX2xpc3QgPSBkYXRhLnJlcy5yYW5rO1xuICAgICAgICAgICAgdGhpcy5saXN0UmFuay5udW1JdGVtcyA9IGRhdGEucmVzLnJhbmsubGVuZ3RoO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbGV0IGRhdGEgPSBhd2FpdCBhcGlDbGllbnQuY2FsbEFwaShcIlJhbmtcIiwgeyB0eXBlOiAyIH0pO1xuICAgICAgICAgICAgaWYgKCFkYXRhLmlzU3VjYykge1xuICAgICAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoZGF0YS5lcnIubWVzc2FnZSk7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0UmFuay5udW1JdGVtcyA9IDA7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YVJhbmtJbmZvLnJhbmtfbGlzdCA9IGRhdGEucmVzLnJhbms7XG4gICAgICAgICAgICB0aGlzLmxpc3RSYW5rLm51bUl0ZW1zID0gZGF0YS5yZXMucmFuay5sZW5ndGg7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvL+WeguebtOWIl+ihqOa4suafk+WZqFxuICAgIG9uTGlzdFJhbmtSZW5kZXIoaXRlbTogY2MuTm9kZSwgaWR4OiBudW1iZXIpIHtcbiAgICAgICAgbGV0IGluZGV4ID0gQ29jb3NIZWxwZXIuZ2V0Q29udGVuZXJpbmRleCh0aGlzLnZpZXcuUmFua1RvZ2dsZU5vZGUpO1xuICAgICAgICBpdGVtLmdldENvbXBvbmVudChJdGVtUmFuaykuc2V0RGF0YShpbmRleCwgR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YVJhbmtJbmZvLnJhbmtfbGlzdFtpZHhdLCBpZHggKyAxKTtcbiAgICB9XG59XG4iXX0=