
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Rank/ItemRank.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3e019pVVitIV5ppWULQY8hz', 'ItemRank');
// Script/UIScript/Rank/ItemRank.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemRank = /** @class */ (function (_super) {
    __extends(ItemRank, _super);
    function ItemRank() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.level_bg = null;
        _this.user_level = null;
        _this.user_name = null;
        _this.user_num = null;
        _this.user_head = null;
        _this.icon = null;
        _this.level_bgs = Array();
        _this.icons = Array();
        return _this;
    }
    ItemRank.prototype.setData = function (type, data, level) {
        if (type === 1) {
            this.icon.spriteFrame = this.icons[0];
        }
        else {
            this.icon.spriteFrame = this.icons[1];
        }
        this.level_bg.active = false;
        this.user_num.node.color = new cc.Color(0, 0, 0);
        if (level < 4) {
            this.level_bg.getComponent(cc.Sprite).spriteFrame = this.level_bgs[level - 1];
            this.level_bg.active = true;
            this.user_num.node.color = new cc.Color(229, 27, 27);
        }
        CocosHelper_1.default.loadHead(data.picUrl, this.user_head);
        this.user_level.string = level.toString();
        this.user_name.string = data.name;
        this.user_num.string = data.money.toString();
    };
    __decorate([
        property(cc.Node)
    ], ItemRank.prototype, "level_bg", void 0);
    __decorate([
        property(cc.Label)
    ], ItemRank.prototype, "user_level", void 0);
    __decorate([
        property(cc.Label)
    ], ItemRank.prototype, "user_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemRank.prototype, "user_num", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemRank.prototype, "user_head", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemRank.prototype, "icon", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], ItemRank.prototype, "level_bgs", void 0);
    __decorate([
        property([cc.SpriteFrame])
    ], ItemRank.prototype, "icons", void 0);
    ItemRank = __decorate([
        ccclass
    ], ItemRank);
    return ItemRank;
}(cc.Component));
exports.default = ItemRank;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvUmFuay9JdGVtUmFuay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHQSx1REFBa0Q7QUFFNUMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBc0MsNEJBQVk7SUFBbEQ7UUFBQSxxRUE2Q0M7UUExQ0csY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUc1QixlQUFTLEdBQWEsSUFBSSxDQUFDO1FBRzNCLGNBQVEsR0FBYSxJQUFJLENBQUM7UUFHMUIsZUFBUyxHQUFjLElBQUksQ0FBQztRQUc1QixVQUFJLEdBQWMsSUFBSSxDQUFDO1FBR3ZCLGVBQVMsR0FBcUIsS0FBSyxFQUFrQixDQUFDO1FBR3RELFdBQUssR0FBcUIsS0FBSyxFQUFrQixDQUFDOztJQXFCdEQsQ0FBQztJQW5CVSwwQkFBTyxHQUFkLFVBQWUsSUFBWSxFQUFFLElBQWMsRUFBRSxLQUFhO1FBQ3RELElBQUksSUFBSSxLQUFLLENBQUMsRUFBRTtZQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDekM7YUFBTTtZQUNILElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDekM7UUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2pELElBQUksS0FBSyxHQUFHLENBQUMsRUFBRTtZQUNYLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDOUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztTQUN4RDtRQUNELHFCQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDakQsQ0FBQztJQXhDRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhDQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7Z0RBQ1M7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzsrQ0FDUTtJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzhDQUNPO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7K0NBQ1E7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzswQ0FDRztJQUd2QjtRQURDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQzsrQ0FDMkI7SUFHdEQ7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7MkNBQ3VCO0lBeEJqQyxRQUFRO1FBRDVCLE9BQU87T0FDYSxRQUFRLENBNkM1QjtJQUFELGVBQUM7Q0E3Q0QsQUE2Q0MsQ0E3Q3FDLEVBQUUsQ0FBQyxTQUFTLEdBNkNqRDtrQkE3Q29CLFFBQVEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCB7IElSYW5rSXRlbSB9IGZyb20gXCIuLi8uLi9NYW5hZ2VyL0ludGVyZmFjZU1nclwiO1xuaW1wb3J0IHsgUmFua0l0ZW0gfSBmcm9tIFwiLi4vLi4vTmV0L3NoYXJlZC9wcm90b2NvbHMvUHRsUmFua1wiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSXRlbVJhbmsgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgbGV2ZWxfYmc6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIHVzZXJfbGV2ZWw6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB1c2VyX25hbWU6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB1c2VyX251bTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICB1c2VyX2hlYWQ6IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICAgIGljb246IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoW2NjLlNwcml0ZUZyYW1lXSlcbiAgICBsZXZlbF9iZ3M6IGNjLlNwcml0ZUZyYW1lW10gPSBBcnJheTxjYy5TcHJpdGVGcmFtZT4oKTtcblxuICAgIEBwcm9wZXJ0eShbY2MuU3ByaXRlRnJhbWVdKVxuICAgIGljb25zOiBjYy5TcHJpdGVGcmFtZVtdID0gQXJyYXk8Y2MuU3ByaXRlRnJhbWU+KCk7XG5cbiAgICBwdWJsaWMgc2V0RGF0YSh0eXBlOiBudW1iZXIsIGRhdGE6IFJhbmtJdGVtLCBsZXZlbDogbnVtYmVyKSB7XG4gICAgICAgIGlmICh0eXBlID09PSAxKSB7XG4gICAgICAgICAgICB0aGlzLmljb24uc3ByaXRlRnJhbWUgPSB0aGlzLmljb25zWzBdO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5pY29uLnNwcml0ZUZyYW1lID0gdGhpcy5pY29uc1sxXTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmxldmVsX2JnLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnVzZXJfbnVtLm5vZGUuY29sb3IgPSBuZXcgY2MuQ29sb3IoMCwgMCwgMCk7XG4gICAgICAgIGlmIChsZXZlbCA8IDQpIHtcbiAgICAgICAgICAgIHRoaXMubGV2ZWxfYmcuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmxldmVsX2Jnc1tsZXZlbCAtIDFdO1xuICAgICAgICAgICAgdGhpcy5sZXZlbF9iZy5hY3RpdmUgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy51c2VyX251bS5ub2RlLmNvbG9yID0gbmV3IGNjLkNvbG9yKDIyOSwgMjcsIDI3KTtcbiAgICAgICAgfVxuICAgICAgICBDb2Nvc0hlbHBlci5sb2FkSGVhZChkYXRhLnBpY1VybCwgdGhpcy51c2VyX2hlYWQpO1xuICAgICAgICB0aGlzLnVzZXJfbGV2ZWwuc3RyaW5nID0gbGV2ZWwudG9TdHJpbmcoKTtcbiAgICAgICAgdGhpcy51c2VyX25hbWUuc3RyaW5nID0gZGF0YS5uYW1lO1xuICAgICAgICB0aGlzLnVzZXJfbnVtLnN0cmluZyA9IGRhdGEubW9uZXkudG9TdHJpbmcoKTtcbiAgICB9XG5cbn1cbiJdfQ==