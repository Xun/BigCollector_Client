
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/RotateAndFlip/CardArrayFlip_CardLayout.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '34a14VJ+FVL85T5e314dkhm', 'CardArrayFlip_CardLayout');
// Script/UIScript/RotateAndFlip/CardArrayFlip_CardLayout.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CardArrayFlip_Card_1 = require("./CardArrayFlip_Card");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode;
var CardArrayFlip_Layout = /** @class */ (function (_super) {
    __extends(CardArrayFlip_Layout, _super);
    function CardArrayFlip_Layout() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._radius = 350;
        _this._offset = 90;
        _this._k = 0;
        /** 卡片组件 */
        _this.cards = null;
        return _this;
    }
    Object.defineProperty(CardArrayFlip_Layout.prototype, "radius", {
        get: function () {
            return this._radius;
        },
        set: function (value) {
            this._radius = value;
            this.updateLayout();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CardArrayFlip_Layout.prototype, "offset", {
        get: function () {
            return this._offset;
        },
        set: function (value) {
            this._offset = value;
            this.updateLayout();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CardArrayFlip_Layout.prototype, "k", {
        get: function () {
            return this._k;
        },
        set: function (value) {
            this._k = value;
            this.updateKValue();
        },
        enumerable: false,
        configurable: true
    });
    CardArrayFlip_Layout.prototype.onLoad = function () {
        this.init();
        this.registerEvent();
    };
    CardArrayFlip_Layout.prototype.onDisable = function () {
        this.unregisterEvent();
    };
    /**
     * 初始化
     */
    CardArrayFlip_Layout.prototype.init = function () {
        this.onChildChange();
    };
    /**
     * 注册事件
     */
    CardArrayFlip_Layout.prototype.registerEvent = function () {
        // 节点增删
        this.node.on(cc.Node.EventType.CHILD_ADDED, this.onChildChange, this);
        this.node.on(cc.Node.EventType.CHILD_REMOVED, this.onChildChange, this);
        // 旋转改变
        this.node.on(cc.Node.EventType.ROTATION_CHANGED, this.onRotationChange, this);
    };
    /**
     * 反注册事件
     */
    CardArrayFlip_Layout.prototype.unregisterEvent = function () {
        this.node.off(cc.Node.EventType.CHILD_ADDED, this.onChildChange, this);
        this.node.off(cc.Node.EventType.CHILD_REMOVED, this.onChildChange, this);
        this.node.off(cc.Node.EventType.ROTATION_CHANGED, this.onRotationChange, this);
    };
    /**
     * 子节点变化回调
     */
    CardArrayFlip_Layout.prototype.onChildChange = function () {
        // 重新获取组件
        this.cards = this.getComponentsInChildren(CardArrayFlip_Card_1.default);
        // 更新 k 值
        this.updateKValue();
        // 更新布局
        this.updateLayout();
    };
    /**
     * 旋转变化回调
     */
    CardArrayFlip_Layout.prototype.onRotationChange = function () {
        // 更新层级
        this.updateHierarchy();
    };
    /**
     * 更新布局
     */
    CardArrayFlip_Layout.prototype.updateLayout = function () {
        var nodes = this.node.children, count = nodes.length, radius = this._radius, offset = this._offset, delta = 360 / count;
        for (var i = 0; i < count; i++) {
            var node = nodes[i], angleY = -(delta * i), radian = (Math.PI / 180) * (angleY - offset);
            // 位置
            node.x = radius * Math.cos(radian);
            node.z = -(radius * Math.sin(radian));
            // 角度
            var _a = node.eulerAngles, x = _a.x, z = _a.z;
            node.eulerAngles = cc.v3(x, angleY, z);
            // node.rotationY = angleY;      // keep warning
            // node.eulerAngles.y = angleY;  // not working
        }
        // 更新层级
        this.updateHierarchy();
    };
    /**
     * 更新层级
     */
    CardArrayFlip_Layout.prototype.updateHierarchy = function () {
        var cards = this.cards, length = cards.length;
        // 更新卡片节点在世界坐标系中的 z 值
        for (var i = 0; i < length; i++) {
            cards[i].updateWorldZ();
        }
        // 排序从大到小，z 值越小的显示在越后面，层级 index 也越小
        cards.sort(function (a, b) { return a.z - b.z; });
        // 调整节点层级
        for (var i = 0; i < length; i++) {
            cards[i].setSiblingIndex(i);
        }
    };
    /**
     * 更新卡片的 k 值
     */
    CardArrayFlip_Layout.prototype.updateKValue = function () {
        var cards = this.cards;
        for (var i = 0, l = cards.length; i < l; i++) {
            cards[i].k = this._k;
        }
    };
    __decorate([
        property
    ], CardArrayFlip_Layout.prototype, "_radius", void 0);
    __decorate([
        property({ displayName: CC_DEV && '阵列半径' })
    ], CardArrayFlip_Layout.prototype, "radius", null);
    __decorate([
        property
    ], CardArrayFlip_Layout.prototype, "_offset", void 0);
    __decorate([
        property({ displayName: CC_DEV && '卡片角度偏移' })
    ], CardArrayFlip_Layout.prototype, "offset", null);
    __decorate([
        property
    ], CardArrayFlip_Layout.prototype, "_k", void 0);
    __decorate([
        property({ displayName: CC_DEV && '正反面阈值' })
    ], CardArrayFlip_Layout.prototype, "k", null);
    CardArrayFlip_Layout = __decorate([
        ccclass,
        executeInEditMode
    ], CardArrayFlip_Layout);
    return CardArrayFlip_Layout;
}(cc.Component));
exports.default = CardArrayFlip_Layout;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvUm90YXRlQW5kRmxpcC9DYXJkQXJyYXlGbGlwX0NhcmRMYXlvdXQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMkRBQXNEO0FBRWhELElBQUEsS0FBMkMsRUFBRSxDQUFDLFVBQVUsRUFBdEQsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFBLEVBQUUsaUJBQWlCLHVCQUFrQixDQUFDO0FBSS9EO0lBQWtELHdDQUFZO0lBQTlEO1FBQUEscUVBb0pDO1FBakphLGFBQU8sR0FBVyxHQUFHLENBQUM7UUFXdEIsYUFBTyxHQUFXLEVBQUUsQ0FBQztRQVdyQixRQUFFLEdBQVcsQ0FBQyxDQUFDO1FBVXpCLFdBQVc7UUFDRCxXQUFLLEdBQXlCLElBQUksQ0FBQzs7SUFnSGpELENBQUM7SUEvSUcsc0JBQVcsd0NBQU07YUFBakI7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDeEIsQ0FBQzthQUNELFVBQWtCLEtBQWE7WUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUM7OztPQUpBO0lBU0Qsc0JBQVcsd0NBQU07YUFBakI7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDeEIsQ0FBQzthQUNELFVBQWtCLEtBQWE7WUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUM7OztPQUpBO0lBU0Qsc0JBQVcsbUNBQUM7YUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNuQixDQUFDO2FBQ0QsVUFBYSxLQUFhO1lBQ3RCLElBQUksQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDO1lBQ2hCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN4QixDQUFDOzs7T0FKQTtJQVNTLHFDQUFNLEdBQWhCO1FBQ0ksSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ1osSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFUyx3Q0FBUyxHQUFuQjtRQUNJLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQ7O09BRUc7SUFDTyxtQ0FBSSxHQUFkO1FBQ0ksSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7T0FFRztJQUNPLDRDQUFhLEdBQXZCO1FBQ0ksT0FBTztRQUNQLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hFLE9BQU87UUFDUCxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDbEYsQ0FBQztJQUVEOztPQUVHO0lBQ08sOENBQWUsR0FBekI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDbkYsQ0FBQztJQUVEOztPQUVHO0lBQ08sNENBQWEsR0FBdkI7UUFDSSxTQUFTO1FBQ1QsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsNEJBQWtCLENBQUMsQ0FBQztRQUM5RCxTQUFTO1FBQ1QsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLE9BQU87UUFDUCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVEOztPQUVHO0lBQ08sK0NBQWdCLEdBQTFCO1FBQ0ksT0FBTztRQUNQLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQ7O09BRUc7SUFDSSwyQ0FBWSxHQUFuQjtRQUNJLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUM1QixLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFDcEIsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQ3JCLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxFQUNyQixLQUFLLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQztRQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzVCLElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDakIsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQ3JCLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUM7WUFDakQsS0FBSztZQUNMLElBQUksQ0FBQyxDQUFDLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN0QyxLQUFLO1lBQ0MsSUFBQSxLQUFXLElBQUksQ0FBQyxXQUFXLEVBQXpCLENBQUMsT0FBQSxFQUFFLENBQUMsT0FBcUIsQ0FBQztZQUNsQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN2QyxnREFBZ0Q7WUFDaEQsK0NBQStDO1NBQ2xEO1FBQ0QsT0FBTztRQUNQLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQ7O09BRUc7SUFDSSw4Q0FBZSxHQUF0QjtRQUNJLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQ3BCLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzFCLHFCQUFxQjtRQUNyQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzdCLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUMzQjtRQUNELG1DQUFtQztRQUNuQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBVCxDQUFTLENBQUMsQ0FBQztRQUNoQyxTQUFTO1FBQ1QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM3QixLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQy9CO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ08sMkNBQVksR0FBdEI7UUFDSSxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDMUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQztJQS9JRDtRQURDLFFBQVE7eURBQ3VCO0lBRWhDO1FBREMsUUFBUSxDQUFDLEVBQUUsV0FBVyxFQUFFLE1BQU0sSUFBSSxNQUFNLEVBQUUsQ0FBQztzREFHM0M7SUFPRDtRQURDLFFBQVE7eURBQ3NCO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsV0FBVyxFQUFFLE1BQU0sSUFBSSxRQUFRLEVBQUUsQ0FBQztzREFHN0M7SUFPRDtRQURDLFFBQVE7b0RBQ2dCO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsV0FBVyxFQUFFLE1BQU0sSUFBSSxPQUFPLEVBQUUsQ0FBQztpREFHNUM7SUE3QmdCLG9CQUFvQjtRQUZ4QyxPQUFPO1FBQ1AsaUJBQWlCO09BQ0csb0JBQW9CLENBb0p4QztJQUFELDJCQUFDO0NBcEpELEFBb0pDLENBcEppRCxFQUFFLENBQUMsU0FBUyxHQW9KN0Q7a0JBcEpvQixvQkFBb0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2FyZEFycmF5RmxpcF9DYXJkIGZyb20gJy4vQ2FyZEFycmF5RmxpcF9DYXJkJztcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSwgZXhlY3V0ZUluRWRpdE1vZGUgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5AZXhlY3V0ZUluRWRpdE1vZGVcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENhcmRBcnJheUZsaXBfTGF5b3V0IGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eVxuICAgIHByb3RlY3RlZCBfcmFkaXVzOiBudW1iZXIgPSAzNTA7XG4gICAgQHByb3BlcnR5KHsgZGlzcGxheU5hbWU6IENDX0RFViAmJiAn6Zi15YiX5Y2K5b6EJyB9KVxuICAgIHB1YmxpYyBnZXQgcmFkaXVzKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fcmFkaXVzO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IHJhZGl1cyh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX3JhZGl1cyA9IHZhbHVlO1xuICAgICAgICB0aGlzLnVwZGF0ZUxheW91dCgpO1xuICAgIH1cblxuICAgIEBwcm9wZXJ0eVxuICAgIHByb3RlY3RlZCBfb2Zmc2V0OiBudW1iZXIgPSA5MDtcbiAgICBAcHJvcGVydHkoeyBkaXNwbGF5TmFtZTogQ0NfREVWICYmICfljaHniYfop5LluqblgY/np7snIH0pXG4gICAgcHVibGljIGdldCBvZmZzZXQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9vZmZzZXQ7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgb2Zmc2V0KHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fb2Zmc2V0ID0gdmFsdWU7XG4gICAgICAgIHRoaXMudXBkYXRlTGF5b3V0KCk7XG4gICAgfVxuXG4gICAgQHByb3BlcnR5XG4gICAgcHJvdGVjdGVkIF9rOiBudW1iZXIgPSAwO1xuICAgIEBwcm9wZXJ0eSh7IGRpc3BsYXlOYW1lOiBDQ19ERVYgJiYgJ+ato+WPjemdoumYiOWAvCcgfSlcbiAgICBwdWJsaWMgZ2V0IGsoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9rO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IGsodmFsdWU6IG51bWJlcikge1xuICAgICAgICB0aGlzLl9rID0gdmFsdWU7XG4gICAgICAgIHRoaXMudXBkYXRlS1ZhbHVlKCk7XG4gICAgfVxuXG4gICAgLyoqIOWNoeeJh+e7hOS7tiAqL1xuICAgIHByb3RlY3RlZCBjYXJkczogQ2FyZEFycmF5RmxpcF9DYXJkW10gPSBudWxsO1xuXG4gICAgcHJvdGVjdGVkIG9uTG9hZCgpIHtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgICAgIHRoaXMucmVnaXN0ZXJFdmVudCgpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBvbkRpc2FibGUoKSB7XG4gICAgICAgIHRoaXMudW5yZWdpc3RlckV2ZW50KCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5Yid5aeL5YyWXG4gICAgICovXG4gICAgcHJvdGVjdGVkIGluaXQoKSB7XG4gICAgICAgIHRoaXMub25DaGlsZENoYW5nZSgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOazqOWGjOS6i+S7tlxuICAgICAqL1xuICAgIHByb3RlY3RlZCByZWdpc3RlckV2ZW50KCkge1xuICAgICAgICAvLyDoioLngrnlop7liKBcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLkNISUxEX0FEREVELCB0aGlzLm9uQ2hpbGRDaGFuZ2UsIHRoaXMpO1xuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuQ0hJTERfUkVNT1ZFRCwgdGhpcy5vbkNoaWxkQ2hhbmdlLCB0aGlzKTtcbiAgICAgICAgLy8g5peL6L2s5pS55Y+YXG4gICAgICAgIHRoaXMubm9kZS5vbihjYy5Ob2RlLkV2ZW50VHlwZS5ST1RBVElPTl9DSEFOR0VELCB0aGlzLm9uUm90YXRpb25DaGFuZ2UsIHRoaXMpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWPjeazqOWGjOS6i+S7tlxuICAgICAqL1xuICAgIHByb3RlY3RlZCB1bnJlZ2lzdGVyRXZlbnQoKSB7XG4gICAgICAgIHRoaXMubm9kZS5vZmYoY2MuTm9kZS5FdmVudFR5cGUuQ0hJTERfQURERUQsIHRoaXMub25DaGlsZENoYW5nZSwgdGhpcyk7XG4gICAgICAgIHRoaXMubm9kZS5vZmYoY2MuTm9kZS5FdmVudFR5cGUuQ0hJTERfUkVNT1ZFRCwgdGhpcy5vbkNoaWxkQ2hhbmdlLCB0aGlzKTtcbiAgICAgICAgdGhpcy5ub2RlLm9mZihjYy5Ob2RlLkV2ZW50VHlwZS5ST1RBVElPTl9DSEFOR0VELCB0aGlzLm9uUm90YXRpb25DaGFuZ2UsIHRoaXMpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWtkOiKgueCueWPmOWMluWbnuiwg1xuICAgICAqL1xuICAgIHByb3RlY3RlZCBvbkNoaWxkQ2hhbmdlKCkge1xuICAgICAgICAvLyDph43mlrDojrflj5bnu4Tku7ZcbiAgICAgICAgdGhpcy5jYXJkcyA9IHRoaXMuZ2V0Q29tcG9uZW50c0luQ2hpbGRyZW4oQ2FyZEFycmF5RmxpcF9DYXJkKTtcbiAgICAgICAgLy8g5pu05pawIGsg5YC8XG4gICAgICAgIHRoaXMudXBkYXRlS1ZhbHVlKCk7XG4gICAgICAgIC8vIOabtOaWsOW4g+WxgFxuICAgICAgICB0aGlzLnVwZGF0ZUxheW91dCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOaXi+i9rOWPmOWMluWbnuiwg1xuICAgICAqL1xuICAgIHByb3RlY3RlZCBvblJvdGF0aW9uQ2hhbmdlKCkge1xuICAgICAgICAvLyDmm7TmlrDlsYLnuqdcbiAgICAgICAgdGhpcy51cGRhdGVIaWVyYXJjaHkoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmm7TmlrDluIPlsYBcbiAgICAgKi9cbiAgICBwdWJsaWMgdXBkYXRlTGF5b3V0KCkge1xuICAgICAgICBjb25zdCBub2RlcyA9IHRoaXMubm9kZS5jaGlsZHJlbixcbiAgICAgICAgICAgIGNvdW50ID0gbm9kZXMubGVuZ3RoLFxuICAgICAgICAgICAgcmFkaXVzID0gdGhpcy5fcmFkaXVzLFxuICAgICAgICAgICAgb2Zmc2V0ID0gdGhpcy5fb2Zmc2V0LFxuICAgICAgICAgICAgZGVsdGEgPSAzNjAgLyBjb3VudDtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjb3VudDsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCBub2RlID0gbm9kZXNbaV0sXG4gICAgICAgICAgICAgICAgYW5nbGVZID0gLShkZWx0YSAqIGkpLFxuICAgICAgICAgICAgICAgIHJhZGlhbiA9IChNYXRoLlBJIC8gMTgwKSAqIChhbmdsZVkgLSBvZmZzZXQpO1xuICAgICAgICAgICAgLy8g5L2N572uXG4gICAgICAgICAgICBub2RlLnggPSByYWRpdXMgKiBNYXRoLmNvcyhyYWRpYW4pO1xuICAgICAgICAgICAgbm9kZS56ID0gLShyYWRpdXMgKiBNYXRoLnNpbihyYWRpYW4pKTtcbiAgICAgICAgICAgIC8vIOinkuW6plxuICAgICAgICAgICAgY29uc3QgeyB4LCB6IH0gPSBub2RlLmV1bGVyQW5nbGVzO1xuICAgICAgICAgICAgbm9kZS5ldWxlckFuZ2xlcyA9IGNjLnYzKHgsIGFuZ2xlWSwgeik7XG4gICAgICAgICAgICAvLyBub2RlLnJvdGF0aW9uWSA9IGFuZ2xlWTsgICAgICAvLyBrZWVwIHdhcm5pbmdcbiAgICAgICAgICAgIC8vIG5vZGUuZXVsZXJBbmdsZXMueSA9IGFuZ2xlWTsgIC8vIG5vdCB3b3JraW5nXG4gICAgICAgIH1cbiAgICAgICAgLy8g5pu05paw5bGC57qnXG4gICAgICAgIHRoaXMudXBkYXRlSGllcmFyY2h5KCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5pu05paw5bGC57qnXG4gICAgICovXG4gICAgcHVibGljIHVwZGF0ZUhpZXJhcmNoeSgpIHtcbiAgICAgICAgY29uc3QgY2FyZHMgPSB0aGlzLmNhcmRzLFxuICAgICAgICAgICAgbGVuZ3RoID0gY2FyZHMubGVuZ3RoO1xuICAgICAgICAvLyDmm7TmlrDljaHniYfoioLngrnlnKjkuJbnlYzlnZDmoIfns7vkuK3nmoQgeiDlgLxcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgY2FyZHNbaV0udXBkYXRlV29ybGRaKCk7XG4gICAgICAgIH1cbiAgICAgICAgLy8g5o6S5bqP5LuO5aSn5Yiw5bCP77yMeiDlgLzotorlsI/nmoTmmL7npLrlnKjotorlkI7pnaLvvIzlsYLnuqcgaW5kZXgg5Lmf6LaK5bCPXG4gICAgICAgIGNhcmRzLnNvcnQoKGEsIGIpID0+IGEueiAtIGIueik7XG4gICAgICAgIC8vIOiwg+aVtOiKgueCueWxgue6p1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBjYXJkc1tpXS5zZXRTaWJsaW5nSW5kZXgoaSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmm7TmlrDljaHniYfnmoQgayDlgLxcbiAgICAgKi9cbiAgICBwcm90ZWN0ZWQgdXBkYXRlS1ZhbHVlKCkge1xuICAgICAgICBjb25zdCBjYXJkcyA9IHRoaXMuY2FyZHM7XG4gICAgICAgIGZvciAobGV0IGkgPSAwLCBsID0gY2FyZHMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgICAgICAgICBjYXJkc1tpXS5rID0gdGhpcy5faztcbiAgICAgICAgfVxuICAgIH1cblxufVxuIl19