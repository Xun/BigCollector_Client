
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/RotateAndFlip/UIRotateAndFlip.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '935b68j161MWKlF/Jcki/N7', 'UIRotateAndFlip');
// Script/UIScript/RotateAndFlip/UIRotateAndFlip.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var CardArrayFlip_FrontCardBase_1 = require("./CardArrayFlip_FrontCardBase");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIRotateAndFlip = /** @class */ (function (_super) {
    __extends(UIRotateAndFlip, _super);
    function UIRotateAndFlip() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.card = null;
        return _this;
    }
    Object.defineProperty(UIRotateAndFlip.prototype, "frontArrayCard", {
        get: function () {
            return this.view.ContainerNode.children[this.view.ContainerNode.childrenCount - 1];
        },
        enumerable: false,
        configurable: true
    });
    UIRotateAndFlip.prototype.start = function () {
        this.card = this.view.CardNode.getComponent(CardArrayFlip_FrontCardBase_1.default);
        // GO
        this.play();
    };
    /**
     * GO
     */
    UIRotateAndFlip.prototype.play = function () {
        return __awaiter(this, void 0, void 0, function () {
            var frontCard;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        frontCard = this.card;
                        // 旋转两圈
                        return [4 /*yield*/, this.rotate(2)];
                    case 1:
                        // 旋转两圈
                        _a.sent();
                        // 等一会
                        return [4 /*yield*/, CocosHelper_1.default.sleepSync(0.2)];
                    case 2:
                        // 等一会
                        _a.sent();
                        // 替换卡片
                        frontCard.show();
                        this.frontArrayCard.active = false;
                        // 翻卡
                        return [4 /*yield*/, frontCard.flipToFront()];
                    case 3:
                        // 翻卡
                        _a.sent();
                        // 等一会
                        return [4 /*yield*/, CocosHelper_1.default.sleepSync(0.2)];
                    case 4:
                        // 等一会
                        _a.sent();
                        // 翻卡
                        return [4 /*yield*/, frontCard.flipToBack()];
                    case 5:
                        // 翻卡
                        _a.sent();
                        // 替换卡片
                        this.frontArrayCard.active = true;
                        frontCard.hide();
                        // 等一会
                        return [4 /*yield*/, CocosHelper_1.default.sleepSync(0.2)];
                    case 6:
                        // 等一会
                        _a.sent();
                        // 继续
                        this.play();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 旋转
     * @param round 圈数
     */
    UIRotateAndFlip.prototype.rotate = function (round) {
        var _this = this;
        return new Promise(function (ress) {
            var node = _this.view.ContainerNode, time = 1 * round, _a = _this.node.eulerAngles, x = _a.x, z = _a.z, eulerAngles = cc.v3(x, 360 * round, z);
            cc.tween(node)
                .by(time, { eulerAngles: eulerAngles }, { easing: 'quadOut' })
                .call(ress)
                .start();
        });
    };
    UIRotateAndFlip = __decorate([
        ccclass
    ], UIRotateAndFlip);
    return UIRotateAndFlip;
}(UIForm_1.UIWindow));
exports.default = UIRotateAndFlip;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvUm90YXRlQW5kRmxpcC9VSVJvdGF0ZUFuZEZsaXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsOENBQStDO0FBQy9DLHVEQUFrRDtBQUNsRCw2RUFBd0U7QUFFbEUsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBNkMsbUNBQVE7SUFBckQ7UUFBQSxxRUE0REM7UUF4RGEsVUFBSSxHQUFnQyxJQUFJLENBQUM7O0lBd0R2RCxDQUFDO0lBdERHLHNCQUFjLDJDQUFjO2FBQTVCO1lBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3ZGLENBQUM7OztPQUFBO0lBRUQsK0JBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLHFDQUEyQixDQUFDLENBQUM7UUFDekUsS0FBSztRQUNMLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQixDQUFDO0lBRUQ7O09BRUc7SUFDVSw4QkFBSSxHQUFqQjs7Ozs7O3dCQUNVLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO3dCQUM1QixPQUFPO3dCQUNQLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUE7O3dCQURwQixPQUFPO3dCQUNQLFNBQW9CLENBQUM7d0JBQ3JCLE1BQU07d0JBQ04scUJBQU0scUJBQVcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUE7O3dCQURoQyxNQUFNO3dCQUNOLFNBQWdDLENBQUM7d0JBQ2pDLE9BQU87d0JBQ1AsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUNqQixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7d0JBQ25DLEtBQUs7d0JBQ0wscUJBQU0sU0FBUyxDQUFDLFdBQVcsRUFBRSxFQUFBOzt3QkFEN0IsS0FBSzt3QkFDTCxTQUE2QixDQUFDO3dCQUM5QixNQUFNO3dCQUNOLHFCQUFNLHFCQUFXLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFBOzt3QkFEaEMsTUFBTTt3QkFDTixTQUFnQyxDQUFDO3dCQUNqQyxLQUFLO3dCQUNMLHFCQUFNLFNBQVMsQ0FBQyxVQUFVLEVBQUUsRUFBQTs7d0JBRDVCLEtBQUs7d0JBQ0wsU0FBNEIsQ0FBQzt3QkFDN0IsT0FBTzt3QkFDUCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ2xDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDakIsTUFBTTt3QkFDTixxQkFBTSxxQkFBVyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBQTs7d0JBRGhDLE1BQU07d0JBQ04sU0FBZ0MsQ0FBQzt3QkFDakMsS0FBSzt3QkFDTCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7Ozs7O0tBQ2Y7SUFFRDs7O09BR0c7SUFDSSxnQ0FBTSxHQUFiLFVBQWMsS0FBYTtRQUEzQixpQkFXQztRQVZHLE9BQU8sSUFBSSxPQUFPLENBQU8sVUFBQSxJQUFJO1lBQ3pCLElBQU0sSUFBSSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUNoQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLEtBQUssRUFDaEIsS0FBVyxLQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBOUIsQ0FBQyxPQUFBLEVBQUUsQ0FBQyxPQUFBLEVBQ04sV0FBVyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBRyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDM0MsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7aUJBQ1QsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLFdBQVcsYUFBQSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLENBQUM7aUJBQ2hELElBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ1YsS0FBSyxFQUFFLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBMURnQixlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBNERuQztJQUFELHNCQUFDO0NBNURELEFBNERDLENBNUQ0QyxpQkFBUSxHQTREcEQ7a0JBNURvQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltcG9ydCBVSVJvdGF0ZUFuZEZsaXBfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlSb3RhdGVBbmRGbGlwX0F1dG9cIjtcclxuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xyXG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XHJcbmltcG9ydCBDYXJkQXJyYXlGbGlwX0Zyb250Q2FyZEJhc2UgZnJvbSBcIi4vQ2FyZEFycmF5RmxpcF9Gcm9udENhcmRCYXNlXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlSb3RhdGVBbmRGbGlwIGV4dGVuZHMgVUlXaW5kb3cge1xyXG5cclxuICAgIHZpZXc6IFVJUm90YXRlQW5kRmxpcF9BdXRvO1xyXG5cclxuICAgIHByb3RlY3RlZCBjYXJkOiBDYXJkQXJyYXlGbGlwX0Zyb250Q2FyZEJhc2UgPSBudWxsO1xyXG5cclxuICAgIHByb3RlY3RlZCBnZXQgZnJvbnRBcnJheUNhcmQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmlldy5Db250YWluZXJOb2RlLmNoaWxkcmVuW3RoaXMudmlldy5Db250YWluZXJOb2RlLmNoaWxkcmVuQ291bnQgLSAxXTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICB0aGlzLmNhcmQgPSB0aGlzLnZpZXcuQ2FyZE5vZGUuZ2V0Q29tcG9uZW50KENhcmRBcnJheUZsaXBfRnJvbnRDYXJkQmFzZSk7XHJcbiAgICAgICAgLy8gR09cclxuICAgICAgICB0aGlzLnBsYXkoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdPXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhc3luYyBwbGF5KCkge1xyXG4gICAgICAgIGNvbnN0IGZyb250Q2FyZCA9IHRoaXMuY2FyZDtcclxuICAgICAgICAvLyDml4vovazkuKTlnIhcclxuICAgICAgICBhd2FpdCB0aGlzLnJvdGF0ZSgyKTtcclxuICAgICAgICAvLyDnrYnkuIDkvJpcclxuICAgICAgICBhd2FpdCBDb2Nvc0hlbHBlci5zbGVlcFN5bmMoMC4yKTtcclxuICAgICAgICAvLyDmm7/mjaLljaHniYdcclxuICAgICAgICBmcm9udENhcmQuc2hvdygpO1xyXG4gICAgICAgIHRoaXMuZnJvbnRBcnJheUNhcmQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgLy8g57+75Y2hXHJcbiAgICAgICAgYXdhaXQgZnJvbnRDYXJkLmZsaXBUb0Zyb250KCk7XHJcbiAgICAgICAgLy8g562J5LiA5LyaXHJcbiAgICAgICAgYXdhaXQgQ29jb3NIZWxwZXIuc2xlZXBTeW5jKDAuMik7XHJcbiAgICAgICAgLy8g57+75Y2hXHJcbiAgICAgICAgYXdhaXQgZnJvbnRDYXJkLmZsaXBUb0JhY2soKTtcclxuICAgICAgICAvLyDmm7/mjaLljaHniYdcclxuICAgICAgICB0aGlzLmZyb250QXJyYXlDYXJkLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgZnJvbnRDYXJkLmhpZGUoKTtcclxuICAgICAgICAvLyDnrYnkuIDkvJpcclxuICAgICAgICBhd2FpdCBDb2Nvc0hlbHBlci5zbGVlcFN5bmMoMC4yKTtcclxuICAgICAgICAvLyDnu6fnu61cclxuICAgICAgICB0aGlzLnBsYXkoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIOaXi+i9rFxyXG4gICAgICogQHBhcmFtIHJvdW5kIOWciOaVsFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcm90YXRlKHJvdW5kOiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2U8dm9pZD4ocmVzcyA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5vZGUgPSB0aGlzLnZpZXcuQ29udGFpbmVyTm9kZSxcclxuICAgICAgICAgICAgICAgIHRpbWUgPSAxICogcm91bmQsXHJcbiAgICAgICAgICAgICAgICB7IHgsIHogfSA9IHRoaXMubm9kZS5ldWxlckFuZ2xlcyxcclxuICAgICAgICAgICAgICAgIGV1bGVyQW5nbGVzID0gY2MudjMoeCwgMzYwICogcm91bmQsIHopO1xyXG4gICAgICAgICAgICBjYy50d2Vlbihub2RlKVxyXG4gICAgICAgICAgICAgICAgLmJ5KHRpbWUsIHsgZXVsZXJBbmdsZXMgfSwgeyBlYXNpbmc6ICdxdWFkT3V0JyB9KVxyXG4gICAgICAgICAgICAgICAgLmNhbGwocmVzcylcclxuICAgICAgICAgICAgICAgIC5zdGFydCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=