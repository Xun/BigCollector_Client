
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/RotateAndFlip/CardArrayFlip_FrontCard2D.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '301c82uYRJB0IHWbyIdk4kE', 'CardArrayFlip_FrontCard2D');
// Script/UIScript/RotateAndFlip/CardArrayFlip_FrontCard2D.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CardArrayFlip_FrontCardBase_1 = require("./CardArrayFlip_FrontCardBase");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CardArrayFlip_FrontCard2D = /** @class */ (function (_super) {
    __extends(CardArrayFlip_FrontCard2D, _super);
    function CardArrayFlip_FrontCard2D() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CardArrayFlip_FrontCard2D.prototype.flipToFront = function () {
        var _this = this;
        this.node.is3DNode = true;
        return new Promise(function (res) {
            var tween = cc.tween, duration = 1, half = duration / 2;
            tween(_this.node)
                .to(duration, { scale: 1.1 })
                .start();
            tween(_this.main)
                .parallel(tween().to(half, { scaleX: 0 }, { easing: 'quadIn' }), tween().to(half, { eulerAngles: cc.v3(_this.main.x, -15, _this.main.z) }, { easing: 'quadOut' }))
                .call(function () {
                _this.front.active = true;
                _this.back.active = false;
            })
                .parallel(tween().to(half, { scaleX: -1 }, { easing: 'quadOut' }), tween().to(half, { eulerAngles: cc.v3(_this.main.x, 0, _this.main.z) }, { easing: 'quadIn' }))
                .call(res)
                .start();
        });
    };
    CardArrayFlip_FrontCard2D.prototype.flipToBack = function () {
        var _this = this;
        return new Promise(function (res) {
            var tween = cc.tween, duration = 1, half = duration / 2;
            tween(_this.node)
                .to(duration, { scale: 0.8 })
                .start();
            tween(_this.main)
                .parallel(tween().to(half, { scaleX: 0 }, { easing: 'quadIn' }), tween().to(half, { eulerAngles: cc.v3(_this.main.x, 15, _this.main.z) }, { easing: 'quadOut' }))
                .call(function () {
                _this.front.active = false;
                _this.back.active = true;
            })
                .parallel(tween().to(half, { scaleX: 1 }, { easing: 'quadOut' }), tween().to(half, { eulerAngles: cc.v3(_this.main.x, 0, _this.main.z) }, { easing: 'quadIn' }))
                .call(res)
                .start();
        });
    };
    CardArrayFlip_FrontCard2D = __decorate([
        ccclass
    ], CardArrayFlip_FrontCard2D);
    return CardArrayFlip_FrontCard2D;
}(CardArrayFlip_FrontCardBase_1.default));
exports.default = CardArrayFlip_FrontCard2D;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvUm90YXRlQW5kRmxpcC9DYXJkQXJyYXlGbGlwX0Zyb250Q2FyZDJELnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDZFQUF3RTtBQUVsRSxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUF1RCw2Q0FBMkI7SUFBbEY7O0lBdURBLENBQUM7SUFyRFUsK0NBQVcsR0FBbEI7UUFBQSxpQkF5QkM7UUF4QkcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQzFCLE9BQU8sSUFBSSxPQUFPLENBQU8sVUFBQSxHQUFHO1lBQ3hCLElBQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQ2xCLFFBQVEsR0FBRyxDQUFDLEVBQ1osSUFBSSxHQUFHLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDeEIsS0FBSyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ1gsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQztpQkFDNUIsS0FBSyxFQUFFLENBQUM7WUFDYixLQUFLLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQztpQkFDWCxRQUFRLENBQ0wsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUNyRCxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsV0FBVyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQ2pHO2lCQUNBLElBQUksQ0FBQztnQkFDRixLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUM3QixDQUFDLENBQUM7aUJBQ0QsUUFBUSxDQUNMLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsQ0FBQyxFQUN2RCxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsV0FBVyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUM5RjtpQkFDQSxJQUFJLENBQUMsR0FBRyxDQUFDO2lCQUNULEtBQUssRUFBRSxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLDhDQUFVLEdBQWpCO1FBQUEsaUJBd0JDO1FBdkJHLE9BQU8sSUFBSSxPQUFPLENBQU8sVUFBQSxHQUFHO1lBQ3hCLElBQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQ2xCLFFBQVEsR0FBRyxDQUFDLEVBQ1osSUFBSSxHQUFHLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDeEIsS0FBSyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ1gsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQztpQkFDNUIsS0FBSyxFQUFFLENBQUM7WUFDYixLQUFLLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQztpQkFDWCxRQUFRLENBQ0wsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUNyRCxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsV0FBVyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUNoRztpQkFDQSxJQUFJLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUMxQixLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDNUIsQ0FBQyxDQUFDO2lCQUNELFFBQVEsQ0FDTCxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDLEVBQ3RELEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxXQUFXLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQzlGO2lCQUNBLElBQUksQ0FBQyxHQUFHLENBQUM7aUJBQ1QsS0FBSyxFQUFFLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBckRnQix5QkFBeUI7UUFEN0MsT0FBTztPQUNhLHlCQUF5QixDQXVEN0M7SUFBRCxnQ0FBQztDQXZERCxBQXVEQyxDQXZEc0QscUNBQTJCLEdBdURqRjtrQkF2RG9CLHlCQUF5QiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDYXJkQXJyYXlGbGlwX0Zyb250Q2FyZEJhc2UgZnJvbSBcIi4vQ2FyZEFycmF5RmxpcF9Gcm9udENhcmRCYXNlXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2FyZEFycmF5RmxpcF9Gcm9udENhcmQyRCBleHRlbmRzIENhcmRBcnJheUZsaXBfRnJvbnRDYXJkQmFzZSB7XHJcblxyXG4gICAgcHVibGljIGZsaXBUb0Zyb250KCkge1xyXG4gICAgICAgIHRoaXMubm9kZS5pczNETm9kZSA9IHRydWU7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPHZvaWQ+KHJlcyA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHR3ZWVuID0gY2MudHdlZW4sXHJcbiAgICAgICAgICAgICAgICBkdXJhdGlvbiA9IDEsXHJcbiAgICAgICAgICAgICAgICBoYWxmID0gZHVyYXRpb24gLyAyO1xyXG4gICAgICAgICAgICB0d2Vlbih0aGlzLm5vZGUpXHJcbiAgICAgICAgICAgICAgICAudG8oZHVyYXRpb24sIHsgc2NhbGU6IDEuMSB9KVxyXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XHJcbiAgICAgICAgICAgIHR3ZWVuKHRoaXMubWFpbilcclxuICAgICAgICAgICAgICAgIC5wYXJhbGxlbChcclxuICAgICAgICAgICAgICAgICAgICB0d2VlbigpLnRvKGhhbGYsIHsgc2NhbGVYOiAwIH0sIHsgZWFzaW5nOiAncXVhZEluJyB9KSxcclxuICAgICAgICAgICAgICAgICAgICB0d2VlbigpLnRvKGhhbGYsIHsgZXVsZXJBbmdsZXM6IGNjLnYzKHRoaXMubWFpbi54LCAtMTUsIHRoaXMubWFpbi56KSB9LCB7IGVhc2luZzogJ3F1YWRPdXQnIH0pLFxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZnJvbnQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmJhY2suYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLnBhcmFsbGVsKFxyXG4gICAgICAgICAgICAgICAgICAgIHR3ZWVuKCkudG8oaGFsZiwgeyBzY2FsZVg6IC0xIH0sIHsgZWFzaW5nOiAncXVhZE91dCcgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgdHdlZW4oKS50byhoYWxmLCB7IGV1bGVyQW5nbGVzOiBjYy52Myh0aGlzLm1haW4ueCwgMCwgdGhpcy5tYWluLnopIH0sIHsgZWFzaW5nOiAncXVhZEluJyB9KSxcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgIC5jYWxsKHJlcylcclxuICAgICAgICAgICAgICAgIC5zdGFydCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBmbGlwVG9CYWNrKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTx2b2lkPihyZXMgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB0d2VlbiA9IGNjLnR3ZWVuLFxyXG4gICAgICAgICAgICAgICAgZHVyYXRpb24gPSAxLFxyXG4gICAgICAgICAgICAgICAgaGFsZiA9IGR1cmF0aW9uIC8gMjtcclxuICAgICAgICAgICAgdHdlZW4odGhpcy5ub2RlKVxyXG4gICAgICAgICAgICAgICAgLnRvKGR1cmF0aW9uLCB7IHNjYWxlOiAwLjggfSlcclxuICAgICAgICAgICAgICAgIC5zdGFydCgpO1xyXG4gICAgICAgICAgICB0d2Vlbih0aGlzLm1haW4pXHJcbiAgICAgICAgICAgICAgICAucGFyYWxsZWwoXHJcbiAgICAgICAgICAgICAgICAgICAgdHdlZW4oKS50byhoYWxmLCB7IHNjYWxlWDogMCB9LCB7IGVhc2luZzogJ3F1YWRJbicgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgdHdlZW4oKS50byhoYWxmLCB7IGV1bGVyQW5nbGVzOiBjYy52Myh0aGlzLm1haW4ueCwgMTUsIHRoaXMubWFpbi56KSB9LCB7IGVhc2luZzogJ3F1YWRPdXQnIH0pLFxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZnJvbnQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5iYWNrLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLnBhcmFsbGVsKFxyXG4gICAgICAgICAgICAgICAgICAgIHR3ZWVuKCkudG8oaGFsZiwgeyBzY2FsZVg6IDEgfSwgeyBlYXNpbmc6ICdxdWFkT3V0JyB9KSxcclxuICAgICAgICAgICAgICAgICAgICB0d2VlbigpLnRvKGhhbGYsIHsgZXVsZXJBbmdsZXM6IGNjLnYzKHRoaXMubWFpbi54LCAwLCB0aGlzLm1haW4ueikgfSwgeyBlYXNpbmc6ICdxdWFkSW4nIH0pLFxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgLmNhbGwocmVzKVxyXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==