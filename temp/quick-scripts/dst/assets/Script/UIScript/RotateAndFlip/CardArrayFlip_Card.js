
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/RotateAndFlip/CardArrayFlip_Card.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '659b8MDsJFPz5aiPsq4V0Q2', 'CardArrayFlip_Card');
// Script/UIScript/RotateAndFlip/CardArrayFlip_Card.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode;
var CardArrayFlip_Card = /** @class */ (function (_super) {
    __extends(CardArrayFlip_Card, _super);
    function CardArrayFlip_Card() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.back = null;
        _this.front = null;
        _this.k = 0;
        _this._z = 0;
        return _this;
    }
    Object.defineProperty(CardArrayFlip_Card.prototype, "z", {
        /** 节点在世界坐标中的 z 值 */
        get: function () {
            return this._z;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CardArrayFlip_Card.prototype, "facingScreen", {
        /** 是否面向屏幕 */
        get: function () {
            return this.node.forward.z >= this.k;
        },
        enumerable: false,
        configurable: true
    });
    CardArrayFlip_Card.prototype.onEnable = function () {
        this.updateWorldZ();
    };
    CardArrayFlip_Card.prototype.update = function (dt) {
        this.updateDisplay();
    };
    /**
     * 更新样式
     */
    CardArrayFlip_Card.prototype.updateDisplay = function () {
        var front = this.facingScreen;
        this.front.active = front;
        this.back.active = !front;
    };
    /**
     * 更新节点在世界坐标中的 z 值
     */
    CardArrayFlip_Card.prototype.updateWorldZ = function () {
        var worldPos = this.node.parent.convertToWorldSpaceAR(this.node.position);
        this._z = worldPos.z;
    };
    /**
     * 设置层级
     * @param index 下标
     */
    CardArrayFlip_Card.prototype.setSiblingIndex = function (index) {
        this.node.setSiblingIndex(index);
    };
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_Card.prototype, "back", void 0);
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_Card.prototype, "front", void 0);
    __decorate([
        property
    ], CardArrayFlip_Card.prototype, "k", void 0);
    CardArrayFlip_Card = __decorate([
        ccclass,
        executeInEditMode
    ], CardArrayFlip_Card);
    return CardArrayFlip_Card;
}(cc.Component));
exports.default = CardArrayFlip_Card;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvUm90YXRlQW5kRmxpcC9DYXJkQXJyYXlGbGlwX0NhcmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxLQUEyQyxFQUFFLENBQUMsVUFBVSxFQUF0RCxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxpQkFBaUIsdUJBQWtCLENBQUM7QUFJL0Q7SUFBZ0Qsc0NBQVk7SUFBNUQ7UUFBQSxxRUF3REM7UUFyRGEsVUFBSSxHQUFZLElBQUksQ0FBQztRQUdyQixXQUFLLEdBQVksSUFBSSxDQUFDO1FBR3pCLE9BQUMsR0FBVyxDQUFDLENBQUM7UUFFWCxRQUFFLEdBQVcsQ0FBQyxDQUFDOztJQTZDN0IsQ0FBQztJQTFDRyxzQkFBVyxpQ0FBQztRQURaLG9CQUFvQjthQUNwQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNuQixDQUFDOzs7T0FBQTtJQUdELHNCQUFjLDRDQUFZO1FBRDFCLGFBQWE7YUFDYjtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDekMsQ0FBQzs7O09BQUE7SUFFUyxxQ0FBUSxHQUFsQjtRQUNJLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRVMsbUNBQU0sR0FBaEIsVUFBaUIsRUFBVTtRQUN2QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVEOztPQUVHO0lBQ08sMENBQWEsR0FBdkI7UUFDSSxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQztJQUM5QixDQUFDO0lBRUQ7O09BRUc7SUFDSSx5Q0FBWSxHQUFuQjtRQUNJLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7O09BR0c7SUFDSSw0Q0FBZSxHQUF0QixVQUF1QixLQUFhO1FBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFuREQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztvREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3FEQUNjO0lBR2hDO1FBREMsUUFBUTtpREFDWTtJQVRKLGtCQUFrQjtRQUZ0QyxPQUFPO1FBQ1AsaUJBQWlCO09BQ0csa0JBQWtCLENBd0R0QztJQUFELHlCQUFDO0NBeERELEFBd0RDLENBeEQrQyxFQUFFLENBQUMsU0FBUyxHQXdEM0Q7a0JBeERvQixrQkFBa0IiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5LCBleGVjdXRlSW5FZGl0TW9kZSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbkBleGVjdXRlSW5FZGl0TW9kZVxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2FyZEFycmF5RmxpcF9DYXJkIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIHByb3RlY3RlZCBiYWNrOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIHByb3RlY3RlZCBmcm9udDogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHlcbiAgICBwdWJsaWMgazogbnVtYmVyID0gMDtcblxuICAgIHByb3RlY3RlZCBfejogbnVtYmVyID0gMDtcblxuICAgIC8qKiDoioLngrnlnKjkuJbnlYzlnZDmoIfkuK3nmoQgeiDlgLwgKi9cbiAgICBwdWJsaWMgZ2V0IHooKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl96O1xuICAgIH1cblxuICAgIC8qKiDmmK/lkKbpnaLlkJHlsY/luZUgKi9cbiAgICBwcm90ZWN0ZWQgZ2V0IGZhY2luZ1NjcmVlbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubm9kZS5mb3J3YXJkLnogPj0gdGhpcy5rO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBvbkVuYWJsZSgpIHtcbiAgICAgICAgdGhpcy51cGRhdGVXb3JsZFooKTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgdXBkYXRlKGR0OiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy51cGRhdGVEaXNwbGF5KCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5pu05paw5qC35byPXG4gICAgICovXG4gICAgcHJvdGVjdGVkIHVwZGF0ZURpc3BsYXkoKSB7XG4gICAgICAgIGNvbnN0IGZyb250ID0gdGhpcy5mYWNpbmdTY3JlZW47XG4gICAgICAgIHRoaXMuZnJvbnQuYWN0aXZlID0gZnJvbnQ7XG4gICAgICAgIHRoaXMuYmFjay5hY3RpdmUgPSAhZnJvbnQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5pu05paw6IqC54K55Zyo5LiW55WM5Z2Q5qCH5Lit55qEIHog5YC8XG4gICAgICovXG4gICAgcHVibGljIHVwZGF0ZVdvcmxkWigpIHtcbiAgICAgICAgY29uc3Qgd29ybGRQb3MgPSB0aGlzLm5vZGUucGFyZW50LmNvbnZlcnRUb1dvcmxkU3BhY2VBUih0aGlzLm5vZGUucG9zaXRpb24pO1xuICAgICAgICB0aGlzLl96ID0gd29ybGRQb3MuejtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDorr7nva7lsYLnuqdcbiAgICAgKiBAcGFyYW0gaW5kZXgg5LiL5qCHXG4gICAgICovXG4gICAgcHVibGljIHNldFNpYmxpbmdJbmRleChpbmRleDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMubm9kZS5zZXRTaWJsaW5nSW5kZXgoaW5kZXgpO1xuICAgIH1cblxufVxuIl19