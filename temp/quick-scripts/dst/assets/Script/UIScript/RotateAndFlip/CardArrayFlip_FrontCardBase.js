
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/RotateAndFlip/CardArrayFlip_FrontCardBase.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7d811Mp9K1Gxotu8UyGs7cC', 'CardArrayFlip_FrontCardBase');
// Script/UIScript/RotateAndFlip/CardArrayFlip_FrontCardBase.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CardArrayFlip_FrontCardBase = /** @class */ (function (_super) {
    __extends(CardArrayFlip_FrontCardBase, _super);
    function CardArrayFlip_FrontCardBase() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.main = null;
        _this.back = null;
        _this.front = null;
        return _this;
    }
    CardArrayFlip_FrontCardBase.prototype.onLoad = function () {
        this.init();
    };
    /**
     * 初始化
     */
    CardArrayFlip_FrontCardBase.prototype.init = function () {
        // 隐藏
        this.hide();
        // 重置
        this.front.active = false;
        this.back.active = true;
    };
    /**
     * 展示
     */
    CardArrayFlip_FrontCardBase.prototype.show = function () {
        this.main.active = true;
    };
    /**
     * 隐藏
     */
    CardArrayFlip_FrontCardBase.prototype.hide = function () {
        this.main.active = false;
    };
    /**
     * 翻到正面
     */
    CardArrayFlip_FrontCardBase.prototype.flipToFront = function () {
        return null;
    };
    /**
     * 翻到背面
     */
    CardArrayFlip_FrontCardBase.prototype.flipToBack = function () {
        return null;
    };
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_FrontCardBase.prototype, "main", void 0);
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_FrontCardBase.prototype, "back", void 0);
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_FrontCardBase.prototype, "front", void 0);
    CardArrayFlip_FrontCardBase = __decorate([
        ccclass
    ], CardArrayFlip_FrontCardBase);
    return CardArrayFlip_FrontCardBase;
}(cc.Component));
exports.default = CardArrayFlip_FrontCardBase;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvUm90YXRlQW5kRmxpcC9DYXJkQXJyYXlGbGlwX0Zyb250Q2FyZEJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBeUQsK0NBQVk7SUFBckU7UUFBQSxxRUFzREM7UUFuRGEsVUFBSSxHQUFZLElBQUksQ0FBQztRQUdyQixVQUFJLEdBQVksSUFBSSxDQUFDO1FBR3JCLFdBQUssR0FBWSxJQUFJLENBQUM7O0lBNkNwQyxDQUFDO0lBM0NhLDRDQUFNLEdBQWhCO1FBQ0ksSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNPLDBDQUFJLEdBQWQ7UUFDSSxLQUFLO1FBQ0wsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ1osS0FBSztRQUNMLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDNUIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksMENBQUksR0FBWDtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUM1QixDQUFDO0lBRUQ7O09BRUc7SUFDSSwwQ0FBSSxHQUFYO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRDs7T0FFRztJQUNJLGlEQUFXLEdBQWxCO1FBQ0ksT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0ksZ0RBQVUsR0FBakI7UUFDSSxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBakREO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkRBQ2E7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs2REFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhEQUNjO0lBVGYsMkJBQTJCO1FBRC9DLE9BQU87T0FDYSwyQkFBMkIsQ0FzRC9DO0lBQUQsa0NBQUM7Q0F0REQsQUFzREMsQ0F0RHdELEVBQUUsQ0FBQyxTQUFTLEdBc0RwRTtrQkF0RG9CLDJCQUEyQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYXJkQXJyYXlGbGlwX0Zyb250Q2FyZEJhc2UgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgcHJvdGVjdGVkIG1haW46IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgcHJvdGVjdGVkIGJhY2s6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgcHJvdGVjdGVkIGZyb250OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBwcm90ZWN0ZWQgb25Mb2FkKCkge1xyXG4gICAgICAgIHRoaXMuaW5pdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog5Yid5aeL5YyWXHJcbiAgICAgKi9cclxuICAgIHByb3RlY3RlZCBpbml0KCkge1xyXG4gICAgICAgIC8vIOmakOiXj1xyXG4gICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgICAgIC8vIOmHjee9rlxyXG4gICAgICAgIHRoaXMuZnJvbnQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5iYWNrLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDlsZXnpLpcclxuICAgICAqL1xyXG4gICAgcHVibGljIHNob3coKSB7XHJcbiAgICAgICAgdGhpcy5tYWluLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiDpmpDol49cclxuICAgICAqL1xyXG4gICAgcHVibGljIGhpZGUoKSB7XHJcbiAgICAgICAgdGhpcy5tYWluLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICog57+75Yiw5q2j6Z2iXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBmbGlwVG9Gcm9udCgpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIOe/u+WIsOiDjOmdolxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZmxpcFRvQmFjaygpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbn1cclxuIl19