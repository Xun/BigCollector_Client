
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/UIToast.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b744fdwl65BvaaTzy2b64Qj', 'UIToast');
// Script/UIScript/UIToast.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var SysDefine_1 = require("../Common/SysDefine");
var Scene_1 = require("../Scene/Scene");
var CocosHelper_1 = require("../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIToast = /** @class */ (function (_super) {
    __extends(UIToast, _super);
    function UIToast() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UIToast_1 = UIToast;
    UIToast.popUp = function (str) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var prefab, node, parent, lab, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        Scene_1.default.inst.setInputBlock(true);
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, CocosHelper_1.default.loadResSync("UIToast", cc.Prefab)];
                    case 2:
                        prefab = _b.sent();
                        node = cc.instantiate(prefab);
                        parent = cc.find(SysDefine_1.SysDefine.SYS_UIROOT_NAME + "/" + SysDefine_1.SysDefine.SYS_TOPTIPS_NODE);
                        if (!parent)
                            return [2 /*return*/];
                        parent.addChild(node);
                        lab = (_a = node.children[0].getChildByName("label")) === null || _a === void 0 ? void 0 : _a.getComponent(cc.Label);
                        if (lab)
                            lab.string = str;
                        node.getComponent(UIToast_1).showAnim();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _b.sent();
                        Scene_1.default.inst.setInputBlock(false);
                        return [2 /*return*/];
                    case 4:
                        Scene_1.default.inst.setInputBlock(false);
                        return [2 /*return*/];
                }
            });
        });
    };
    UIToast.prototype.showAnim = function () {
        var _this = this;
        this.node.y = 0;
        this.node.opacity = 255;
        cc.tween(this.node).by(2, { position: cc.v3(0, 80, 0) }).to(0.3, { opacity: 0 }).call(function () {
            _this.node.destroy();
            _this.node.removeFromParent();
        }).start();
    };
    var UIToast_1;
    UIToast = UIToast_1 = __decorate([
        ccclass
    ], UIToast);
    return UIToast;
}(cc.Component));
exports.default = UIToast;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvVUlUb2FzdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxpREFBZ0Q7QUFDaEQsd0NBQW1DO0FBQ25DLG9EQUErQztBQUV6QyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFxQywyQkFBWTtJQUFqRDs7SUE2QkEsQ0FBQztnQkE3Qm9CLE9BQU87SUFDWCxhQUFLLEdBQWxCLFVBQW1CLEdBQVc7Ozs7Ozs7d0JBQzFCLGVBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDOzs7O3dCQUVkLHFCQUFNLHFCQUFXLENBQUMsV0FBVyxDQUFZLFNBQVMsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUF2RSxNQUFNLEdBQUcsU0FBOEQ7d0JBQ3ZFLElBQUksR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUM5QixNQUFNLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxxQkFBUyxDQUFDLGVBQWUsR0FBRyxHQUFHLEdBQUcscUJBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3dCQUNuRixJQUFJLENBQUMsTUFBTTs0QkFBRSxzQkFBTzt3QkFDcEIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbEIsR0FBRyxTQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQywwQ0FBRSxZQUFZLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUMzRSxJQUFJLEdBQUc7NEJBQUUsR0FBRyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7d0JBQzFCLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBTyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7Ozs7d0JBRXRDLGVBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNoQyxzQkFBTzs7d0JBRVgsZUFBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7Ozs7O0tBQ25DO0lBR00sMEJBQVEsR0FBZjtRQUFBLGlCQVFDO1FBUEcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUV4QixFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNsRixLQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7O0lBNUJnQixPQUFPO1FBRDNCLE9BQU87T0FDYSxPQUFPLENBNkIzQjtJQUFELGNBQUM7Q0E3QkQsQUE2QkMsQ0E3Qm9DLEVBQUUsQ0FBQyxTQUFTLEdBNkJoRDtrQkE3Qm9CLE9BQU8iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTeXNEZWZpbmUgfSBmcm9tIFwiLi4vQ29tbW9uL1N5c0RlZmluZVwiO1xuaW1wb3J0IFNjZW5lIGZyb20gXCIuLi9TY2VuZS9TY2VuZVwiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlUb2FzdCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgc3RhdGljIGFzeW5jIHBvcFVwKHN0cjogc3RyaW5nKSB7XG4gICAgICAgIFNjZW5lLmluc3Quc2V0SW5wdXRCbG9jayh0cnVlKTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGxldCBwcmVmYWIgPSBhd2FpdCBDb2Nvc0hlbHBlci5sb2FkUmVzU3luYzxjYy5QcmVmYWI+KFwiVUlUb2FzdFwiLCBjYy5QcmVmYWIpO1xuICAgICAgICAgICAgbGV0IG5vZGUgPSBjYy5pbnN0YW50aWF0ZShwcmVmYWIpO1xuICAgICAgICAgICAgbGV0IHBhcmVudCA9IGNjLmZpbmQoU3lzRGVmaW5lLlNZU19VSVJPT1RfTkFNRSArIFwiL1wiICsgU3lzRGVmaW5lLlNZU19UT1BUSVBTX05PREUpO1xuICAgICAgICAgICAgaWYgKCFwYXJlbnQpIHJldHVybjtcbiAgICAgICAgICAgIHBhcmVudC5hZGRDaGlsZChub2RlKTtcbiAgICAgICAgICAgIGxldCBsYWIgPSBub2RlLmNoaWxkcmVuWzBdLmdldENoaWxkQnlOYW1lKFwibGFiZWxcIik/LmdldENvbXBvbmVudChjYy5MYWJlbCk7XG4gICAgICAgICAgICBpZiAobGFiKSBsYWIuc3RyaW5nID0gc3RyO1xuICAgICAgICAgICAgbm9kZS5nZXRDb21wb25lbnQoVUlUb2FzdCkuc2hvd0FuaW0oKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICAgIFNjZW5lLmluc3Quc2V0SW5wdXRCbG9jayhmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgU2NlbmUuaW5zdC5zZXRJbnB1dEJsb2NrKGZhbHNlKTtcbiAgICB9XG5cblxuICAgIHB1YmxpYyBzaG93QW5pbSgpIHtcbiAgICAgICAgdGhpcy5ub2RlLnkgPSAwO1xuICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDI1NTtcblxuICAgICAgICBjYy50d2Vlbih0aGlzLm5vZGUpLmJ5KDIsIHsgcG9zaXRpb246IGNjLnYzKDAsIDgwLCAwKSB9KS50bygwLjMsIHsgb3BhY2l0eTogMCB9KS5jYWxsKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMubm9kZS5kZXN0cm95KCk7XG4gICAgICAgICAgICB0aGlzLm5vZGUucmVtb3ZlRnJvbVBhcmVudCgpO1xuICAgICAgICB9KS5zdGFydCgpO1xuICAgIH1cbn0iXX0=