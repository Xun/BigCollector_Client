
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/UIScoketReconnent.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2a844K4K1FCX6AJ3H4zgXnS', 'UIScoketReconnent');
// Script/UIScript/UIScoketReconnent.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Struct_1 = require("../Common/Struct");
var SysDefine_1 = require("../Common/SysDefine");
var UIForm_1 = require("../Common/UIForm");
var CocosHelper_1 = require("../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIScoketReconnent = /** @class */ (function (_super) {
    __extends(UIScoketReconnent, _super);
    function UIScoketReconnent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.modalType = new Struct_1.ModalType(SysDefine_1.ModalOpacity.OpacityHigh);
        return _this;
    }
    UIScoketReconnent.prototype.onLoad = function () {
        CocosHelper_1.default.runRepeatTweenSync(this.view.pan, -1, cc.tween().to(6, { angle: -360 }).to(0, { angle: 0 }));
    };
    UIScoketReconnent = __decorate([
        ccclass
    ], UIScoketReconnent);
    return UIScoketReconnent;
}(UIForm_1.UIWindow));
exports.default = UIScoketReconnent;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvVUlTY29rZXRSZWNvbm5lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsMkNBQTZDO0FBQzdDLGlEQUFtRDtBQUNuRCwyQ0FBb0Q7QUFJcEQsb0RBQStDO0FBRXpDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQStDLHFDQUFRO0lBQXZEO1FBQUEscUVBVUM7UUFORyxlQUFTLEdBQUcsSUFBSSxrQkFBUyxDQUFDLHdCQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7O0lBTXhELENBQUM7SUFKRyxrQ0FBTSxHQUFOO1FBQ0kscUJBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDN0csQ0FBQztJQVJnQixpQkFBaUI7UUFEckMsT0FBTztPQUNhLGlCQUFpQixDQVVyQztJQUFELHdCQUFDO0NBVkQsQUFVQyxDQVY4QyxpQkFBUSxHQVV0RDtrQkFWb0IsaUJBQWlCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVJU2Nva2V0UmVjb25uZW50X0F1dG8gZnJvbSBcIi4uL0F1dG9TY3JpcHRzL1VJU2Nva2V0UmVjb25uZW50X0F1dG9cIjtcbmltcG9ydCB7IE1vZGFsVHlwZSB9IGZyb20gXCIuLi9Db21tb24vU3RydWN0XCI7XG5pbXBvcnQgeyBNb2RhbE9wYWNpdHkgfSBmcm9tIFwiLi4vQ29tbW9uL1N5c0RlZmluZVwiO1xuaW1wb3J0IHsgVUlUaXBzLCBVSVdpbmRvdyB9IGZyb20gXCIuLi9Db21tb24vVUlGb3JtXCI7XG5pbXBvcnQgRm9ybU1nciBmcm9tIFwiLi4vTWFuYWdlci9Gb3JtTWdyXCI7XG5pbXBvcnQgVGlwc01nciBmcm9tIFwiLi4vTWFuYWdlci9UaXBzTWdyXCI7XG5pbXBvcnQgVUlDb25maWcgZnJvbSBcIi4uL1VJQ29uZmlnXCI7XG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSVNjb2tldFJlY29ubmVudCBleHRlbmRzIFVJV2luZG93IHtcblxuICAgIHZpZXc6IFVJU2Nva2V0UmVjb25uZW50X0F1dG87XG5cbiAgICBtb2RhbFR5cGUgPSBuZXcgTW9kYWxUeXBlKE1vZGFsT3BhY2l0eS5PcGFjaXR5SGlnaCk7XG5cbiAgICBvbkxvYWQoKSB7XG4gICAgICAgIENvY29zSGVscGVyLnJ1blJlcGVhdFR3ZWVuU3luYyh0aGlzLnZpZXcucGFuLCAtMSwgY2MudHdlZW4oKS50byg2LCB7IGFuZ2xlOiAtMzYwIH0pLnRvKDAsIHsgYW5nbGU6IDAgfSkpO1xuICAgIH1cblxufVxuIl19