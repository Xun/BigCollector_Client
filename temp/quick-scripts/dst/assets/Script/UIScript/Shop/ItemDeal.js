
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/ItemDeal.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '48330AeVTNKorPfGFLD+YFH', 'ItemDeal');
// Script/UIScript/Shop/ItemDeal.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var FormMgr_1 = require("../../Manager/FormMgr");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemDeal = /** @class */ (function (_super) {
    __extends(ItemDeal, _super);
    function ItemDeal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item_name = null;
        _this.item_num = null;
        _this.item_price = null;
        _this.item_icon = null;
        _this.item_buy = null;
        _this.item_sell = null;
        _this.type_str = "";
        _this.orderid = "";
        _this.chip_num = 0;
        _this.chip_name = "";
        return _this;
    }
    ItemDeal.prototype.setData = function (data, type) {
        if (type === 1) {
            this.type_str = "求购数量:";
            this.item_buy.active = false;
            this.item_sell.active = true;
        }
        if (type === 2) {
            this.type_str = "出售数量:";
            this.item_buy.active = true;
            this.item_sell.active = false;
        }
        this.item_name.string = CocosHelper_1.default.getChipNameByID(data.chipID.toString());
        this.chip_name = this.item_name.string.replace("碎片", "");
        CocosHelper_1.default.setDealIcon(this.chip_name, this.item_icon);
        this.item_num.string = this.type_str + data.currentCount;
        this.chip_num = data.currentCount;
        this.item_price.string = data.price.toString();
        this.orderid = data.guid;
    };
    ItemDeal.prototype.onBuy = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                FormMgr_1.default.open(UIConfig_1.default.UIDealTip, { price: Number(this.item_price.string), orderId: this.orderid, chipName: this.chip_name, chipNum: this.chip_num, type: 2 });
                return [2 /*return*/];
            });
        });
    };
    ItemDeal.prototype.onSell = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                FormMgr_1.default.open(UIConfig_1.default.UIDealTip, { price: Number(this.item_price.string), orderId: this.orderid, chipName: this.chip_name, chipNum: this.chip_num, type: 1 });
                return [2 /*return*/];
            });
        });
    };
    __decorate([
        property(cc.Label)
    ], ItemDeal.prototype, "item_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDeal.prototype, "item_num", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDeal.prototype, "item_price", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemDeal.prototype, "item_icon", void 0);
    __decorate([
        property(cc.Node)
    ], ItemDeal.prototype, "item_buy", void 0);
    __decorate([
        property(cc.Node)
    ], ItemDeal.prototype, "item_sell", void 0);
    ItemDeal = __decorate([
        ccclass
    ], ItemDeal);
    return ItemDeal;
}(cc.Component));
exports.default = ItemDeal;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9JdGVtRGVhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxpREFBNEM7QUFJNUMsMkNBQXNDO0FBQ3RDLHVEQUFrRDtBQUk1QyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFzQyw0QkFBWTtJQUFsRDtRQUFBLHFFQW1EQztRQWhERyxlQUFTLEdBQWEsSUFBSSxDQUFDO1FBRzNCLGNBQVEsR0FBYSxJQUFJLENBQUM7UUFHMUIsZ0JBQVUsR0FBYSxJQUFJLENBQUM7UUFHNUIsZUFBUyxHQUFjLElBQUksQ0FBQztRQUc1QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFFbEIsY0FBUSxHQUFXLEVBQUUsQ0FBQztRQUN0QixhQUFPLEdBQVcsRUFBRSxDQUFDO1FBQ3JCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFDckIsZUFBUyxHQUFXLEVBQUUsQ0FBQzs7SUE0Qm5DLENBQUM7SUEzQkcsMEJBQU8sR0FBUCxVQUFRLElBQWtCLEVBQUUsSUFBWTtRQUNwQyxJQUFJLElBQUksS0FBSyxDQUFDLEVBQUU7WUFDWixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQTtZQUN2QixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ2hDO1FBQ0QsSUFBSSxJQUFJLEtBQUssQ0FBQyxFQUFFO1lBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUE7WUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNqQztRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLHFCQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDekQscUJBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ3pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQy9DLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRUssd0JBQUssR0FBWDs7O2dCQUNJLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFBOzs7O0tBQ2hLO0lBRUsseUJBQU0sR0FBWjs7O2dCQUNJLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFBOzs7O0tBQ2hLO0lBL0NEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7K0NBQ1E7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs4Q0FDTztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dEQUNTO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7K0NBQ1E7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs4Q0FDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOytDQUNRO0lBbEJULFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0FtRDVCO0lBQUQsZUFBQztDQW5ERCxBQW1EQyxDQW5EcUMsRUFBRSxDQUFDLFNBQVMsR0FtRGpEO2tCQW5Eb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERhdGFEZWFsSXRlbSB9IGZyb20gXCIuLi8uLi9EYXRhTW9kYWwvRGF0YURlYWxcIjtcbmltcG9ydCBGb3JtTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL0Zvcm1NZ3JcIjtcbmltcG9ydCBHYW1lTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL0dhbWVNZ3JcIjtcbmltcG9ydCBVSU1hbmFnZXIgZnJvbSBcIi4uLy4uL01hbmFnZXIvVUlNYW5hZ2VyXCI7XG5pbXBvcnQgeyBhcGlDbGllbnQgfSBmcm9tIFwiLi4vLi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IFVJQ29uZmlnIGZyb20gXCIuLi8uLi9VSUNvbmZpZ1wiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcbmltcG9ydCBVSURlYWwgZnJvbSBcIi4vVUlEZWFsXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJdGVtRGVhbCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgaXRlbV9uYW1lOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgaXRlbV9udW06IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBpdGVtX3ByaWNlOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuU3ByaXRlKVxuICAgIGl0ZW1faWNvbjogY2MuU3ByaXRlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIGl0ZW1fYnV5OiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIGl0ZW1fc2VsbDogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBwcml2YXRlIHR5cGVfc3RyOiBzdHJpbmcgPSBcIlwiO1xuICAgIHByaXZhdGUgb3JkZXJpZDogc3RyaW5nID0gXCJcIjtcbiAgICBwcml2YXRlIGNoaXBfbnVtOiBudW1iZXIgPSAwO1xuICAgIHByaXZhdGUgY2hpcF9uYW1lOiBzdHJpbmcgPSBcIlwiO1xuICAgIHNldERhdGEoZGF0YTogRGF0YURlYWxJdGVtLCB0eXBlOiBudW1iZXIpIHtcbiAgICAgICAgaWYgKHR5cGUgPT09IDEpIHtcbiAgICAgICAgICAgIHRoaXMudHlwZV9zdHIgPSBcIuaxgui0reaVsOmHjzpcIlxuICAgICAgICAgICAgdGhpcy5pdGVtX2J1eS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMuaXRlbV9zZWxsLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHR5cGUgPT09IDIpIHtcbiAgICAgICAgICAgIHRoaXMudHlwZV9zdHIgPSBcIuWHuuWUruaVsOmHjzpcIlxuICAgICAgICAgICAgdGhpcy5pdGVtX2J1eS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5pdGVtX3NlbGwuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5pdGVtX25hbWUuc3RyaW5nID0gQ29jb3NIZWxwZXIuZ2V0Q2hpcE5hbWVCeUlEKGRhdGEuY2hpcElELnRvU3RyaW5nKCkpO1xuICAgICAgICB0aGlzLmNoaXBfbmFtZSA9IHRoaXMuaXRlbV9uYW1lLnN0cmluZy5yZXBsYWNlKFwi56KO54mHXCIsIFwiXCIpO1xuICAgICAgICBDb2Nvc0hlbHBlci5zZXREZWFsSWNvbih0aGlzLmNoaXBfbmFtZSwgdGhpcy5pdGVtX2ljb24pO1xuICAgICAgICB0aGlzLml0ZW1fbnVtLnN0cmluZyA9IHRoaXMudHlwZV9zdHIgKyBkYXRhLmN1cnJlbnRDb3VudDtcbiAgICAgICAgdGhpcy5jaGlwX251bSA9IGRhdGEuY3VycmVudENvdW50O1xuICAgICAgICB0aGlzLml0ZW1fcHJpY2Uuc3RyaW5nID0gZGF0YS5wcmljZS50b1N0cmluZygpO1xuICAgICAgICB0aGlzLm9yZGVyaWQgPSBkYXRhLmd1aWQ7XG4gICAgfVxuXG4gICAgYXN5bmMgb25CdXkoKSB7XG4gICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSURlYWxUaXAsIHsgcHJpY2U6IE51bWJlcih0aGlzLml0ZW1fcHJpY2Uuc3RyaW5nKSwgb3JkZXJJZDogdGhpcy5vcmRlcmlkLCBjaGlwTmFtZTogdGhpcy5jaGlwX25hbWUsIGNoaXBOdW06IHRoaXMuY2hpcF9udW0sIHR5cGU6IDIgfSlcbiAgICB9XG5cbiAgICBhc3luYyBvblNlbGwoKSB7XG4gICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSURlYWxUaXAsIHsgcHJpY2U6IE51bWJlcih0aGlzLml0ZW1fcHJpY2Uuc3RyaW5nKSwgb3JkZXJJZDogdGhpcy5vcmRlcmlkLCBjaGlwTmFtZTogdGhpcy5jaGlwX25hbWUsIGNoaXBOdW06IHRoaXMuY2hpcF9udW0sIHR5cGU6IDEgfSlcbiAgICB9XG59XG4iXX0=