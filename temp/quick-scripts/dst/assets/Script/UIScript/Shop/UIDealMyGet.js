
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/UIDealMyGet.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a7101JSaQ5EvK2fImv7tG0J', 'UIDealMyGet');
// Script/UIScript/Shop/UIDealMyGet.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var UIToast_1 = require("../UIToast");
var ItemDealMyGet_1 = require("./ItemDealMyGet");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealMyGet = /** @class */ (function (_super) {
    __extends(UIDealMyGet, _super);
    function UIDealMyGet() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listDeal = null;
        return _this;
    }
    // onLoad () {}
    UIDealMyGet.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            var datas;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.view.CloseBtn.addClick(function () {
                            _this.closeSelf();
                        }, this);
                        this.view.BuyAdd.addClick(function () {
                            FormMgr_1.default.open(UIConfig_1.default.UIDealAdd);
                        }, this);
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealOrder", { userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount, type: 2 })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                        }
                        GameMgr_1.default.dataModalMgr.DataDealInfo.dealMyBuyList = datas.res.dealList;
                        console.log(datas.res.dealList.length);
                        this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealMyBuyList.length;
                        return [2 /*return*/];
                }
            });
        });
    };
    UIDealMyGet.prototype.reflashList = function () {
        this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealMyBuyList.length;
        console.log("刷新之后-------------" + this.listDeal.numItems);
    };
    //垂直列表渲染器
    UIDealMyGet.prototype.onListDealMyGetRender = function (item, idx) {
        item.getComponent(ItemDealMyGet_1.default).setData(GameMgr_1.default.dataModalMgr.DataDealInfo.dealMyBuyList[idx]);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIDealMyGet.prototype, "listDeal", void 0);
    UIDealMyGet = __decorate([
        ccclass
    ], UIDealMyGet);
    return UIDealMyGet;
}(UIForm_1.UIWindow));
exports.default = UIDealMyGet;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9VSURlYWxNeUdldC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdsRiw2REFBd0Q7QUFDeEQsOENBQStDO0FBRS9DLGlEQUE0QztBQUM1QyxpREFBNEM7QUFDNUMsaURBQWdEO0FBQ2hELDJDQUFzQztBQUV0QyxzQ0FBaUM7QUFDakMsaURBQTRDO0FBRXRDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQXlDLCtCQUFRO0lBQWpEO1FBQUEscUVBbUNDO1FBOUJHLGNBQVEsR0FBYSxJQUFJLENBQUM7O0lBOEI5QixDQUFDO0lBNUJHLGVBQWU7SUFFVCwyQkFBSyxHQUFYOzs7Ozs7O3dCQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQzs0QkFDeEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO3dCQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDOzRCQUN0QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUNyQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBRUcscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsV0FBVyxFQUFFLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUFqSCxLQUFLLEdBQUcsU0FBeUc7d0JBQ3JILElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFOzRCQUNmLGlCQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ3BDO3dCQUNELGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7d0JBQ3JFLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUE7d0JBQ3RDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDOzs7OztLQUNuRjtJQUNNLGlDQUFXLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7UUFDaEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0lBQzdELENBQUM7SUFDRCxTQUFTO0lBQ1QsMkNBQXFCLEdBQXJCLFVBQXNCLElBQWEsRUFBRSxHQUFXO1FBQzVDLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDbkcsQ0FBQztJQTVCRDtRQURDLFFBQVEsQ0FBQyxrQkFBUSxDQUFDO2lEQUNPO0lBTFQsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQW1DL0I7SUFBRCxrQkFBQztDQW5DRCxBQW1DQyxDQW5Dd0MsaUJBQVEsR0FtQ2hEO2tCQW5Db0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgVUlEZWFsTXlHZXRfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlEZWFsTXlHZXRfQXV0b1wiO1xuaW1wb3J0IExpc3RVdGlsIGZyb20gXCIuLi8uLi9Db21tb24vQ29tcG9uZW50cy9MaXN0VXRpbFwiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IHsgRGF0YVNlZURldGFpbCB9IGZyb20gXCIuLi8uLi9EYXRhTW9kYWwvRGF0YUZyaWVuZFwiO1xuaW1wb3J0IEZvcm1NZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvRm9ybU1nclwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IHsgYXBpQ2xpZW50IH0gZnJvbSBcIi4uLy4uL05ldC9ScGNDb25lbnRcIjtcbmltcG9ydCBVSUNvbmZpZyBmcm9tIFwiLi4vLi4vVUlDb25maWdcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmltcG9ydCBVSVRvYXN0IGZyb20gXCIuLi9VSVRvYXN0XCI7XG5pbXBvcnQgSXRlbURlYWxNeUdldCBmcm9tIFwiLi9JdGVtRGVhbE15R2V0XCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSURlYWxNeUdldCBleHRlbmRzIFVJV2luZG93IHtcblxuICAgIHZpZXc6IFVJRGVhbE15R2V0X0F1dG87XG5cbiAgICBAcHJvcGVydHkoTGlzdFV0aWwpXG4gICAgbGlzdERlYWw6IExpc3RVdGlsID0gbnVsbDtcblxuICAgIC8vIG9uTG9hZCAoKSB7fVxuXG4gICAgYXN5bmMgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5DbG9zZUJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnV5QWRkLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSURlYWxBZGQpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICBsZXQgZGF0YXMgPSBhd2FpdCBhcGlDbGllbnQuY2FsbEFwaShcIkRlYWxPcmRlclwiLCB7IHVzZXJBY2NvdW50OiBHYW1lTWdyLmRhdGFNb2RhbE1nci5Vc2VySW5mby51c2VyQWNjb3VudCwgdHlwZTogMiB9KTtcbiAgICAgICAgaWYgKCFkYXRhcy5pc1N1Y2MpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoZGF0YXMuZXJyLm1lc3NhZ2UpO1xuICAgICAgICB9XG4gICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsTXlCdXlMaXN0ID0gZGF0YXMucmVzLmRlYWxMaXN0O1xuICAgICAgICBjb25zb2xlLmxvZyhkYXRhcy5yZXMuZGVhbExpc3QubGVuZ3RoKVxuICAgICAgICB0aGlzLmxpc3REZWFsLm51bUl0ZW1zID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxNeUJ1eUxpc3QubGVuZ3RoO1xuICAgIH1cbiAgICBwdWJsaWMgcmVmbGFzaExpc3QoKSB7XG4gICAgICAgIHRoaXMubGlzdERlYWwubnVtSXRlbXMgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhRGVhbEluZm8uZGVhbE15QnV5TGlzdC5sZW5ndGg7XG4gICAgICAgIGNvbnNvbGUubG9nKFwi5Yi35paw5LmL5ZCOLS0tLS0tLS0tLS0tLVwiICsgdGhpcy5saXN0RGVhbC5udW1JdGVtcylcbiAgICB9XG4gICAgLy/lnoLnm7TliJfooajmuLLmn5PlmahcbiAgICBvbkxpc3REZWFsTXlHZXRSZW5kZXIoaXRlbTogY2MuTm9kZSwgaWR4OiBudW1iZXIpIHtcbiAgICAgICAgaXRlbS5nZXRDb21wb25lbnQoSXRlbURlYWxNeUdldCkuc2V0RGF0YShHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhRGVhbEluZm8uZGVhbE15QnV5TGlzdFtpZHhdKTtcbiAgICB9XG5cbn1cbiJdfQ==