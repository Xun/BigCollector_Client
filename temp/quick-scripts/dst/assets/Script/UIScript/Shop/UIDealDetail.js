
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/UIDealDetail.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9283ftNhK5JBIw0+NEA+o62', 'UIDealDetail');
// Script/UIScript/Shop/UIDealDetail.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIToast_1 = require("../UIToast");
var ItemDealDetail_1 = require("./ItemDealDetail");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealDetail = /** @class */ (function (_super) {
    __extends(UIDealDetail, _super);
    function UIDealDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listDealDetail = null;
        return _this;
    }
    UIDealDetail.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.view.CloseBtn.addClick(function () {
                            _this.closeSelf();
                        }, this);
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealDetail", {})];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            GameMgr_1.default.dataModalMgr.DataDealInfo.dealDetailList = [];
                            this.listDealDetail.numItems = 0;
                        }
                        GameMgr_1.default.dataModalMgr.DataDealInfo.dealDetailList = data.res.dealList;
                        this.listDealDetail.numItems = data.res.dealList.length;
                        return [2 /*return*/];
                }
            });
        });
    };
    //垂直列表渲染器
    UIDealDetail.prototype.onListDealDetailRender = function (item, idx) {
        item.getComponent(ItemDealDetail_1.default).setData(GameMgr_1.default.dataModalMgr.DataDealInfo.dealDetailList[idx]);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIDealDetail.prototype, "listDealDetail", void 0);
    UIDealDetail = __decorate([
        ccclass
    ], UIDealDetail);
    return UIDealDetail;
}(UIForm_1.UIWindow));
exports.default = UIDealDetail;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9VSURlYWxEZXRhaWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHbEYsNkRBQXdEO0FBQ3hELDhDQUErQztBQUMvQyxpREFBNEM7QUFDNUMsaURBQWdEO0FBQ2hELHNDQUFpQztBQUNqQyxtREFBOEM7QUFFeEMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBMEMsZ0NBQVE7SUFBbEQ7UUFBQSxxRUF5QkM7UUFwQkcsb0JBQWMsR0FBYSxJQUFJLENBQUM7O0lBb0JwQyxDQUFDO0lBbEJTLDRCQUFLLEdBQVg7Ozs7Ozs7d0JBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDOzRCQUN4QixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7d0JBQ3JCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDRSxxQkFBTSxxQkFBUyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLEVBQUE7O3dCQUFoRCxJQUFJLEdBQUcsU0FBeUM7d0JBQ3BELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNkLGlCQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQ2hDLGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDOzRCQUN0RCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7eUJBQ3BDO3dCQUNELGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7d0JBQ3JFLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQzs7Ozs7S0FDM0Q7SUFFRCxTQUFTO0lBQ1QsNkNBQXNCLEdBQXRCLFVBQXVCLElBQWEsRUFBRSxHQUFXO1FBQzdDLElBQUksQ0FBQyxZQUFZLENBQUMsd0JBQWMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDckcsQ0FBQztJQW5CRDtRQURDLFFBQVEsQ0FBQyxrQkFBUSxDQUFDO3dEQUNhO0lBTGYsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQXlCaEM7SUFBRCxtQkFBQztDQXpCRCxBQXlCQyxDQXpCeUMsaUJBQVEsR0F5QmpEO2tCQXpCb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgVUlEZWFsRGV0YWlsX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJRGVhbERldGFpbF9BdXRvXCI7XG5pbXBvcnQgTGlzdFV0aWwgZnJvbSBcIi4uLy4uL0NvbW1vbi9Db21wb25lbnRzL0xpc3RVdGlsXCI7XG5pbXBvcnQgeyBVSVdpbmRvdyB9IGZyb20gXCIuLi8uLi9Db21tb24vVUlGb3JtXCI7XG5pbXBvcnQgR2FtZU1nciBmcm9tIFwiLi4vLi4vTWFuYWdlci9HYW1lTWdyXCI7XG5pbXBvcnQgeyBhcGlDbGllbnQgfSBmcm9tIFwiLi4vLi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcbmltcG9ydCBJdGVtRGVhbERldGFpbCBmcm9tIFwiLi9JdGVtRGVhbERldGFpbFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlEZWFsRGV0YWlsIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlEZWFsRGV0YWlsX0F1dG87XG5cbiAgICBAcHJvcGVydHkoTGlzdFV0aWwpXG4gICAgbGlzdERlYWxEZXRhaWw6IExpc3RVdGlsID0gbnVsbDtcblxuICAgIGFzeW5jIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLnZpZXcuQ2xvc2VCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgICAgIGxldCBkYXRhID0gYXdhaXQgYXBpQ2xpZW50LmNhbGxBcGkoXCJEZWFsRGV0YWlsXCIsIHt9KTtcbiAgICAgICAgaWYgKCFkYXRhLmlzU3VjYykge1xuICAgICAgICAgICAgVUlUb2FzdC5wb3BVcChkYXRhLmVyci5tZXNzYWdlKTtcbiAgICAgICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsRGV0YWlsTGlzdCA9IFtdO1xuICAgICAgICAgICAgdGhpcy5saXN0RGVhbERldGFpbC5udW1JdGVtcyA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxEZXRhaWxMaXN0ID0gZGF0YS5yZXMuZGVhbExpc3Q7XG4gICAgICAgIHRoaXMubGlzdERlYWxEZXRhaWwubnVtSXRlbXMgPSBkYXRhLnJlcy5kZWFsTGlzdC5sZW5ndGg7XG4gICAgfVxuXG4gICAgLy/lnoLnm7TliJfooajmuLLmn5PlmahcbiAgICBvbkxpc3REZWFsRGV0YWlsUmVuZGVyKGl0ZW06IGNjLk5vZGUsIGlkeDogbnVtYmVyKSB7XG4gICAgICAgIGl0ZW0uZ2V0Q29tcG9uZW50KEl0ZW1EZWFsRGV0YWlsKS5zZXREYXRhKEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsRGV0YWlsTGlzdFtpZHhdKTtcbiAgICB9XG59XG4iXX0=