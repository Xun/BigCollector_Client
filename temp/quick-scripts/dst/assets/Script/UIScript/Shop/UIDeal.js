
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/UIDeal.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '15b2aw7TjxJopD6dXRZYwwH', 'UIDeal');
// Script/UIScript/Shop/UIDeal.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var ItemDeal_1 = require("./ItemDeal");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDeal = /** @class */ (function (_super) {
    __extends(UIDeal, _super);
    function UIDeal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listDeal = null;
        _this.chipid = "";
        _this.itemname = "笔";
        _this.deal_type = 0;
        return _this;
    }
    UIDeal.prototype.onLoad = function () {
        this.view.NodeDealItems.active = false;
    };
    UIDeal.prototype.start = function () {
        var _this = this;
        this.view.DealToggleNode.children.forEach(function (item) {
            item.on(cc.Node.EventType.TOUCH_END, function (e) {
                _this.onSelectType(_this.itemname);
            }, _this);
        });
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.BtnCloseNodeItems.addClick(function () {
            _this.view.NodeDealItems.active = false;
        }, this);
        this.view.BtnSelectType.addClick(function () {
            _this.view.NodeDealItems.active = !_this.view.NodeDealItems.active;
        }, this);
        this.view.BtnDealDetail.addClick(function () {
            _this.view.NodeDealItems.active = false;
            FormMgr_1.default.open(UIConfig_1.default.UIDealDetail);
        }, this);
        this.view.BtnMyBuy.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIDealMyGet);
        }, this);
        this.view.BtnMySell.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIDealMySell);
        }, this);
        this.view.BtnBi.addClick(function () {
            _this.onSelectType("笔");
        }, this);
        this.view.BtnZhi.addClick(function () {
            _this.onSelectType("纸");
        }, this);
        this.view.BtnMo.addClick(function () {
            _this.onSelectType("墨");
        }, this);
        this.view.BtnYan.addClick(function () {
            _this.onSelectType("砚");
        }, this);
        this.view.BtnM.addClick(function () {
            _this.onSelectType("梅");
        }, this);
        this.view.BtnL.addClick(function () {
            _this.onSelectType("兰");
        }, this);
        this.view.BtnZ.addClick(function () {
            _this.onSelectType("竹");
        }, this);
        this.view.BtnJ.addClick(function () {
            _this.onSelectType("菊");
        }, this);
        this.view.BtnXYJ.addClick(function () {
            _this.onSelectType("西游记");
        }, this);
        this.view.BtnHLM.addClick(function () {
            _this.onSelectType("红楼梦");
        }, this);
        this.view.BtnSHZ.addClick(function () {
            _this.onSelectType("水浒传");
        }, this);
        this.view.BtnSGYY.addClick(function () {
            _this.onSelectType("三国演义");
        }, this);
        this.view.BtnQL.addClick(function () {
            _this.onSelectType("青龙");
        }, this);
        this.view.BtnBH.addClick(function () {
            _this.onSelectType("白虎");
        }, this);
        this.view.BtnXW.addClick(function () {
            _this.onSelectType("玄武");
        }, this);
        this.view.BtnZQ.addClick(function () {
            _this.onSelectType("朱雀");
        }, this);
        this.view.BtnZG.addClick(function () {
            _this.onSelectType("曾巩");
        }, this);
        this.view.BtnLZY.addClick(function () {
            _this.onSelectType("柳宗元");
        }, this);
        this.view.BtnWAS.addClick(function () {
            _this.onSelectType("王安石");
        }, this);
        this.view.BtnHY.addClick(function () {
            _this.onSelectType("韩愈");
        }, this);
        this.view.BtnOYX.addClick(function () {
            _this.onSelectType("欧阳修");
        }, this);
        this.view.BtnSX.addClick(function () {
            _this.onSelectType("苏洵");
        }, this);
        this.view.BtnSZ.addClick(function () {
            _this.onSelectType("苏辙");
        }, this);
        this.view.BtnSS.addClick(function () {
            _this.onSelectType("苏轼");
        }, this);
        this.view.BtnJC.addClick(function () {
            _this.onSelectType("金蟾");
        }, this);
        this.view.BtnPX.addClick(function () {
            _this.onSelectType("貔貅");
        }, this);
        this.view.BtnJL.addClick(function () {
            _this.onSelectType("金龙");
        }, this);
        this.onSelectType("笔");
        // this.changeType();
    };
    UIDeal.prototype.onSelectType = function (itme_name) {
        return __awaiter(this, void 0, void 0, function () {
            var index, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.itemname = itme_name;
                        CocosHelper_1.default.setDealIcon(itme_name, this.view.DealIcon);
                        this.view.NodeDealItems.active = false;
                        this.chipid = CocosHelper_1.default.getChipInfoByName(itme_name + "碎片");
                        console.log("------------" + this.chipid);
                        index = CocosHelper_1.default.getContenerindex(this.view.DealToggleNode);
                        if (index == 0) {
                            this.deal_type = 2;
                        }
                        if (index == 1) {
                            this.deal_type = 1;
                        }
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("Deal", { type: this.deal_type, chipID: Number(this.chipid) })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        if (index == 0) {
                            console.log("aaaaaa");
                            GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList = datas.res.dealList;
                            this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList.length;
                        }
                        if (index == 1) {
                            console.log("bbbbbb");
                            GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList = datas.res.dealList;
                            this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList.length;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    UIDeal.prototype.reflushBuyList = function () {
        this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList.length;
    };
    UIDeal.prototype.reflushSellList = function () {
        this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList.length;
    };
    //垂直列表渲染器
    UIDeal.prototype.onListDealRender = function (item, idx) {
        var index = CocosHelper_1.default.getContenerindex(this.view.DealToggleNode);
        console.log("-------------index:" + index);
        if (index == 0) {
            if (GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList.length) {
                item.getComponent(ItemDeal_1.default).setData(GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList[idx], 1);
            }
        }
        else {
            if (GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList.length > 0) {
                item.getComponent(ItemDeal_1.default).setData(GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList[idx], 2);
            }
        }
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIDeal.prototype, "listDeal", void 0);
    UIDeal = __decorate([
        ccclass
    ], UIDeal);
    return UIDeal;
}(UIForm_1.UIWindow));
exports.default = UIDeal;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9VSURlYWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsNkRBQXdEO0FBQ3hELDhDQUErQztBQUMvQyxpREFBNEM7QUFDNUMsaURBQTRDO0FBQzVDLGlEQUFnRDtBQUNoRCwyQ0FBc0M7QUFDdEMsdURBQWtEO0FBQ2xELHNDQUFpQztBQUNqQyx1Q0FBa0M7QUFFNUIsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBb0MsMEJBQVE7SUFBNUM7UUFBQSxxRUF5TkM7UUFwTkcsY0FBUSxHQUFhLElBQUksQ0FBQztRQUNsQixZQUFNLEdBQVcsRUFBRSxDQUFDO1FBQ3BCLGNBQVEsR0FBVyxHQUFHLENBQUM7UUFDdkIsZUFBUyxHQUFXLENBQUMsQ0FBQzs7SUFpTmxDLENBQUM7SUFoTkcsdUJBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDM0MsQ0FBQztJQUVELHNCQUFLLEdBQUw7UUFBQSxpQkE4SUM7UUE1SUcsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7WUFDM0MsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsVUFBQyxDQUFzQjtnQkFDeEQsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDckMsQ0FBQyxFQUFFLEtBQUksQ0FBQyxDQUFBO1FBQ1osQ0FBQyxDQUFDLENBQUE7UUFFRixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDeEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDO1lBQ2pDLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDM0MsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO1lBQzdCLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztRQUNyRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDN0IsS0FBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUN2QyxpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUN4QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztZQUN6QixpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNwQixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNwQixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNwQixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNwQixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUN2QixLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztZQUN0QixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUNyQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkIscUJBQXFCO0lBQ3pCLENBQUM7SUFFSyw2QkFBWSxHQUFsQixVQUFtQixTQUFpQjs7Ozs7O3dCQUNoQyxJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQzt3QkFDMUIscUJBQVcsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3ZELElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7d0JBQ3ZDLElBQUksQ0FBQyxNQUFNLEdBQUcscUJBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7d0JBQzlELE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFFdEMsS0FBSyxHQUFHLHFCQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFFbkUsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFOzRCQUNaLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO3lCQUN0Qjt3QkFDRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7NEJBQ1osSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7eUJBQ3RCO3dCQUNXLHFCQUFNLHFCQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsRUFBQTs7d0JBQTlGLEtBQUssR0FBRyxTQUFzRjt3QkFDbEcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7NEJBQ2YsaUJBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDakMsc0JBQU87eUJBQ1Y7d0JBQ0QsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFOzRCQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7NEJBQ3JCLGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7NEJBQ3BFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDO3lCQUVsRjt3QkFDRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7NEJBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTs0QkFDckIsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQzs0QkFDbkUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7eUJBQ2pGOzs7OztLQUVKO0lBRUQsK0JBQWMsR0FBZDtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO0lBQ2xGLENBQUM7SUFFRCxnQ0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7SUFDbkYsQ0FBQztJQUVELFNBQVM7SUFDVCxpQ0FBZ0IsR0FBaEIsVUFBaUIsSUFBYSxFQUFFLEdBQVc7UUFDdkMsSUFBSSxLQUFLLEdBQUcscUJBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ25FLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDM0MsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ1osSUFBSSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtnQkFDdkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxrQkFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDL0Y7U0FHSjthQUFNO1lBQ0gsSUFBSSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzFELElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzlGO1NBRUo7SUFDTCxDQUFDO0lBbE5EO1FBREMsUUFBUSxDQUFDLGtCQUFRLENBQUM7NENBQ087SUFMVCxNQUFNO1FBRDFCLE9BQU87T0FDYSxNQUFNLENBeU4xQjtJQUFELGFBQUM7Q0F6TkQsQUF5TkMsQ0F6Tm1DLGlCQUFRLEdBeU4zQztrQkF6Tm9CLE1BQU0iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVUlEZWFsX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJRGVhbF9BdXRvXCI7XG5pbXBvcnQgTGlzdFV0aWwgZnJvbSBcIi4uLy4uL0NvbW1vbi9Db21wb25lbnRzL0xpc3RVdGlsXCI7XG5pbXBvcnQgeyBVSVdpbmRvdyB9IGZyb20gXCIuLi8uLi9Db21tb24vVUlGb3JtXCI7XG5pbXBvcnQgRm9ybU1nciBmcm9tIFwiLi4vLi4vTWFuYWdlci9Gb3JtTWdyXCI7XG5pbXBvcnQgR2FtZU1nciBmcm9tIFwiLi4vLi4vTWFuYWdlci9HYW1lTWdyXCI7XG5pbXBvcnQgeyBhcGlDbGllbnQgfSBmcm9tIFwiLi4vLi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IFVJQ29uZmlnIGZyb20gXCIuLi8uLi9VSUNvbmZpZ1wiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcbmltcG9ydCBJdGVtRGVhbCBmcm9tIFwiLi9JdGVtRGVhbFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlEZWFsIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlEZWFsX0F1dG87XG5cbiAgICBAcHJvcGVydHkoTGlzdFV0aWwpXG4gICAgbGlzdERlYWw6IExpc3RVdGlsID0gbnVsbDtcbiAgICBwcml2YXRlIGNoaXBpZDogc3RyaW5nID0gXCJcIjtcbiAgICBwcml2YXRlIGl0ZW1uYW1lOiBzdHJpbmcgPSBcIueslFwiO1xuICAgIHByaXZhdGUgZGVhbF90eXBlOiBudW1iZXIgPSAwO1xuICAgIG9uTG9hZCgpIHtcbiAgICAgICAgdGhpcy52aWV3Lk5vZGVEZWFsSXRlbXMuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG5cbiAgICAgICAgdGhpcy52aWV3LkRlYWxUb2dnbGVOb2RlLmNoaWxkcmVuLmZvckVhY2goKGl0ZW0pID0+IHtcbiAgICAgICAgICAgIGl0ZW0ub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfRU5ELCAoZTogY2MuRXZlbnQuRXZlbnRUb3VjaCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKHRoaXMuaXRlbW5hbWUpO1xuICAgICAgICAgICAgfSwgdGhpcylcbiAgICAgICAgfSlcblxuICAgICAgICB0aGlzLnZpZXcuQ2xvc2VCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkNsb3NlTm9kZUl0ZW1zLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMudmlldy5Ob2RlRGVhbEl0ZW1zLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU2VsZWN0VHlwZS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnZpZXcuTm9kZURlYWxJdGVtcy5hY3RpdmUgPSAhdGhpcy52aWV3Lk5vZGVEZWFsSXRlbXMuYWN0aXZlO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuRGVhbERldGFpbC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnZpZXcuTm9kZURlYWxJdGVtcy5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSURlYWxEZXRhaWwpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTXlCdXkuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgRm9ybU1nci5vcGVuKFVJQ29uZmlnLlVJRGVhbE15R2V0KTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bk15U2VsbC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlEZWFsTXlTZWxsKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkJpLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi56yUXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuWmhpLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi57q4XCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTW8uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLloqhcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5ZYW4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnoJpcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5NLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5qKFXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuWFsFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blouYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnq7lcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5KLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6I+KXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuWFlKLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6KW/5ri46K6wXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuSExNLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi57qi5qW85qKmXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU0haLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5rC05rWS5LygXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU0dZWS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuS4ieWbvea8lOS5iVwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blFMLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6Z2S6b6ZXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuQkguYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnmb3omY5cIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5YVy5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIueOhOatplwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blpRLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5pyx6ZuAXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuWkcuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLmm77lt6lcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5MWlkuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLmn7PlrpflhYNcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5XQVMuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnjovlronnn7NcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5IWS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIumfqeaEiFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bk9ZWC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuasp+mYs+S/rlwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blNYLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6IuP5rS1XCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU1ouYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLoi4/ovplcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5TUy5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuiLj+i9vFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkpDLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6YeR6J++XCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuUFguYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLospTosoVcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5KTC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIumHkem+mVwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi56yUXCIpO1xuICAgICAgICAvLyB0aGlzLmNoYW5nZVR5cGUoKTtcbiAgICB9XG5cbiAgICBhc3luYyBvblNlbGVjdFR5cGUoaXRtZV9uYW1lOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5pdGVtbmFtZSA9IGl0bWVfbmFtZTtcbiAgICAgICAgQ29jb3NIZWxwZXIuc2V0RGVhbEljb24oaXRtZV9uYW1lLCB0aGlzLnZpZXcuRGVhbEljb24pO1xuICAgICAgICB0aGlzLnZpZXcuTm9kZURlYWxJdGVtcy5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5jaGlwaWQgPSBDb2Nvc0hlbHBlci5nZXRDaGlwSW5mb0J5TmFtZShpdG1lX25hbWUgKyBcIueijueJh1wiKTtcbiAgICAgICAgY29uc29sZS5sb2coXCItLS0tLS0tLS0tLS1cIiArIHRoaXMuY2hpcGlkKTtcblxuICAgICAgICBsZXQgaW5kZXggPSBDb2Nvc0hlbHBlci5nZXRDb250ZW5lcmluZGV4KHRoaXMudmlldy5EZWFsVG9nZ2xlTm9kZSk7XG5cbiAgICAgICAgaWYgKGluZGV4ID09IDApIHtcbiAgICAgICAgICAgIHRoaXMuZGVhbF90eXBlID0gMjtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaW5kZXggPT0gMSkge1xuICAgICAgICAgICAgdGhpcy5kZWFsX3R5cGUgPSAxO1xuICAgICAgICB9XG4gICAgICAgIGxldCBkYXRhcyA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiRGVhbFwiLCB7IHR5cGU6IHRoaXMuZGVhbF90eXBlLCBjaGlwSUQ6IE51bWJlcih0aGlzLmNoaXBpZCkgfSk7XG4gICAgICAgIGlmICghZGF0YXMuaXNTdWNjKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKGRhdGFzLmVyci5tZXNzYWdlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaW5kZXggPT0gMCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJhYWFhYWFcIilcbiAgICAgICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsU2VsbExpc3QgPSBkYXRhcy5yZXMuZGVhbExpc3Q7XG4gICAgICAgICAgICB0aGlzLmxpc3REZWFsLm51bUl0ZW1zID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxTZWxsTGlzdC5sZW5ndGg7XG5cbiAgICAgICAgfVxuICAgICAgICBpZiAoaW5kZXggPT0gMSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJiYmJiYmJcIilcbiAgICAgICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsQnV5TGlzdCA9IGRhdGFzLnJlcy5kZWFsTGlzdDtcbiAgICAgICAgICAgIHRoaXMubGlzdERlYWwubnVtSXRlbXMgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhRGVhbEluZm8uZGVhbEJ1eUxpc3QubGVuZ3RoO1xuICAgICAgICB9XG5cbiAgICB9XG5cbiAgICByZWZsdXNoQnV5TGlzdCgpIHtcbiAgICAgICAgdGhpcy5saXN0RGVhbC5udW1JdGVtcyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsQnV5TGlzdC5sZW5ndGg7XG4gICAgfVxuXG4gICAgcmVmbHVzaFNlbGxMaXN0KCkge1xuICAgICAgICB0aGlzLmxpc3REZWFsLm51bUl0ZW1zID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxTZWxsTGlzdC5sZW5ndGg7XG4gICAgfVxuXG4gICAgLy/lnoLnm7TliJfooajmuLLmn5PlmahcbiAgICBvbkxpc3REZWFsUmVuZGVyKGl0ZW06IGNjLk5vZGUsIGlkeDogbnVtYmVyKSB7XG4gICAgICAgIGxldCBpbmRleCA9IENvY29zSGVscGVyLmdldENvbnRlbmVyaW5kZXgodGhpcy52aWV3LkRlYWxUb2dnbGVOb2RlKTtcbiAgICAgICAgY29uc29sZS5sb2coXCItLS0tLS0tLS0tLS0taW5kZXg6XCIgKyBpbmRleCk7XG4gICAgICAgIGlmIChpbmRleCA9PSAwKSB7XG4gICAgICAgICAgICBpZiAoR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxTZWxsTGlzdC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBpdGVtLmdldENvbXBvbmVudChJdGVtRGVhbCkuc2V0RGF0YShHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhRGVhbEluZm8uZGVhbFNlbGxMaXN0W2lkeF0sIDEpO1xuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmIChHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhRGVhbEluZm8uZGVhbEJ1eUxpc3QubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIGl0ZW0uZ2V0Q29tcG9uZW50KEl0ZW1EZWFsKS5zZXREYXRhKEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsQnV5TGlzdFtpZHhdLCAyKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG4gICAgfVxuXG59XG4iXX0=