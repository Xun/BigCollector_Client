
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/ItemShop.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '50b6c2NQC1NJqTAcSDt/p8s', 'ItemShop');
// Script/UIScript/Shop/ItemShop.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("../../Common/Components/ButtonPlus");
var RpcConent_1 = require("../../Net/RpcConent");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemShop = /** @class */ (function (_super) {
    __extends(ItemShop, _super);
    function ItemShop() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item_name = null;
        _this.item_icon = null;
        _this.buy_type_sp = null;
        _this.buy_type_lab = null;
        _this.buy_btn = null;
        _this.buyId = 0;
        return _this;
    }
    ItemShop.prototype.start = function () {
        var _this = this;
        this.buy_btn.addClick(function () {
            // FormMgr.open(UIConfig.UIBuyTip);
            _this.onBuy();
        }, this);
    };
    ItemShop.prototype.setData = function (data) {
        this.buyId = Number(data.id);
        if (data.name == "铜钱少" || data.name == "铜钱多") {
            CocosHelper_1.default.setDealIcon("玉石", this.buy_type_sp);
            if (data.name == "铜钱少") {
                this.item_name.string = "50万";
            }
            else {
                this.item_name.string = "500万";
            }
        }
        else {
            CocosHelper_1.default.setDealIcon(data.name, this.buy_type_sp);
            this.item_name.string = data.name;
        }
        CocosHelper_1.default.setShopInfo(data.name, this.item_icon);
        this.buy_type_lab.string = data.need_num;
    };
    ItemShop.prototype.onBuy = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("Convert", { id: this.buyId })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        UIToast_1.default.popUp("购买成功");
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        property(cc.Label)
    ], ItemShop.prototype, "item_name", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemShop.prototype, "item_icon", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemShop.prototype, "buy_type_sp", void 0);
    __decorate([
        property(cc.Label)
    ], ItemShop.prototype, "buy_type_lab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], ItemShop.prototype, "buy_btn", void 0);
    ItemShop = __decorate([
        ccclass
    ], ItemShop);
    return ItemShop;
}(cc.Component));
exports.default = ItemShop;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9JdGVtU2hvcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxpRUFBNEQ7QUFJNUQsaURBQWdEO0FBRWhELHVEQUFrRDtBQUNsRCxzQ0FBaUM7QUFFM0IsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBc0MsNEJBQVk7SUFBbEQ7UUFBQSxxRUFvREM7UUFqREcsZUFBUyxHQUFhLElBQUksQ0FBQztRQUczQixlQUFTLEdBQWMsSUFBSSxDQUFDO1FBRzVCLGlCQUFXLEdBQWMsSUFBSSxDQUFDO1FBRzlCLGtCQUFZLEdBQWEsSUFBSSxDQUFDO1FBRzlCLGFBQU8sR0FBZSxJQUFJLENBQUM7UUFFbkIsV0FBSyxHQUFXLENBQUMsQ0FBQzs7SUFtQzlCLENBQUM7SUFsQ0csd0JBQUssR0FBTDtRQUFBLGlCQUtDO1FBSkcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFDbEIsbUNBQW1DO1lBQ25DLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRU0sMEJBQU8sR0FBZCxVQUFlLElBQWtCO1FBQzdCLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM3QixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksS0FBSyxFQUFFO1lBQzFDLHFCQUFXLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDaEQsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLEtBQUssRUFBRTtnQkFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2FBQ2pDO2lCQUFNO2dCQUNILElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQzthQUNsQztTQUNKO2FBQU07WUFDSCxxQkFBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNyRCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3JDO1FBQ0QscUJBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFbkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUM3QyxDQUFDO0lBRUssd0JBQUssR0FBWDs7Ozs7NEJBQ2UscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFBOzt3QkFBN0QsSUFBSSxHQUFHLFNBQXNEO3dCQUNqRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTs0QkFDZCxpQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDOzRCQUNoQyxzQkFBTzt5QkFDVjt3QkFDRCxpQkFBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQTs7Ozs7S0FDeEI7SUEvQ0Q7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzsrQ0FDUTtJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOytDQUNRO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7aURBQ1U7SUFHOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztrREFDVztJQUc5QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzZDQUNNO0lBZlYsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQW9ENUI7SUFBRCxlQUFDO0NBcERELEFBb0RDLENBcERxQyxFQUFFLENBQUMsU0FBUyxHQW9EakQ7a0JBcERvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiO1xuaW1wb3J0IHsgRGF0YVNob3BJdGVtIH0gZnJvbSBcIi4uLy4uL0RhdGFNb2RhbC9EYXRhU2hvcFwiO1xuaW1wb3J0IEZvcm1NZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvRm9ybU1nclwiO1xuaW1wb3J0IHsgSVNob3BJdGVtIH0gZnJvbSBcIi4uLy4uL01hbmFnZXIvSW50ZXJmYWNlTWdyXCI7XG5pbXBvcnQgeyBhcGlDbGllbnQgfSBmcm9tIFwiLi4vLi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IFVJQ29uZmlnIGZyb20gXCIuLi8uLi9VSUNvbmZpZ1wiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEl0ZW1TaG9wIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBpdGVtX25hbWU6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gICAgaXRlbV9pY29uOiBjYy5TcHJpdGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBidXlfdHlwZV9zcDogY2MuU3ByaXRlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBidXlfdHlwZV9sYWI6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuICAgIGJ1eV9idG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXG4gICAgcHJpdmF0ZSBidXlJZDogbnVtYmVyID0gMDtcbiAgICBzdGFydCgpIHtcbiAgICAgICAgdGhpcy5idXlfYnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIC8vIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSUJ1eVRpcCk7XG4gICAgICAgICAgICB0aGlzLm9uQnV5KCk7XG4gICAgICAgIH0sIHRoaXMpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZXREYXRhKGRhdGE6IERhdGFTaG9wSXRlbSkge1xuICAgICAgICB0aGlzLmJ1eUlkID0gTnVtYmVyKGRhdGEuaWQpO1xuICAgICAgICBpZiAoZGF0YS5uYW1lID09IFwi6ZOc6ZKx5bCRXCIgfHwgZGF0YS5uYW1lID09IFwi6ZOc6ZKx5aSaXCIpIHtcbiAgICAgICAgICAgIENvY29zSGVscGVyLnNldERlYWxJY29uKFwi546J55+zXCIsIHRoaXMuYnV5X3R5cGVfc3ApO1xuICAgICAgICAgICAgaWYgKGRhdGEubmFtZSA9PSBcIumTnOmSseWwkVwiKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5pdGVtX25hbWUuc3RyaW5nID0gXCI1MOS4h1wiO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLml0ZW1fbmFtZS5zdHJpbmcgPSBcIjUwMOS4h1wiO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgQ29jb3NIZWxwZXIuc2V0RGVhbEljb24oZGF0YS5uYW1lLCB0aGlzLmJ1eV90eXBlX3NwKTtcbiAgICAgICAgICAgIHRoaXMuaXRlbV9uYW1lLnN0cmluZyA9IGRhdGEubmFtZTtcbiAgICAgICAgfVxuICAgICAgICBDb2Nvc0hlbHBlci5zZXRTaG9wSW5mbyhkYXRhLm5hbWUsIHRoaXMuaXRlbV9pY29uKTtcblxuICAgICAgICB0aGlzLmJ1eV90eXBlX2xhYi5zdHJpbmcgPSBkYXRhLm5lZWRfbnVtO1xuICAgIH1cblxuICAgIGFzeW5jIG9uQnV5KCkge1xuICAgICAgICBsZXQgZGF0YSA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiQ29udmVydFwiLCB7IGlkOiB0aGlzLmJ1eUlkIH0pO1xuICAgICAgICBpZiAoIWRhdGEuaXNTdWNjKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKGRhdGEuZXJyLm1lc3NhZ2UpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIFVJVG9hc3QucG9wVXAoXCLotK3kubDmiJDlip9cIilcbiAgICB9XG5cbn1cbiJdfQ==