
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/UIDealSell.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7aa42/izR1MnIMrKwgcVXBk', 'UIDealSell');
// Script/UIScript/Shop/UIDealSell.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var UIManager_1 = require("../../Manager/UIManager");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var UIDealMySell_1 = require("./UIDealMySell");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealSell = /** @class */ (function (_super) {
    __extends(UIDealSell, _super);
    function UIDealSell() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deal_num = 0;
        _this.deal_price = 0;
        return _this;
    }
    // onLoad () {}
    UIDealSell.prototype.start = function () {
        var _this = this;
        this.view.LabRemain.string = "可出售数量为：";
        this.view.LabBtn.node.active = true;
        this.view.IconType.node.active = false;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.BtnSelectType.addClick(function () {
            _this.view.NodeDealItems.active = !_this.view.NodeDealItems.active;
        }, this);
        this.view.BtnCloseNodeItems.addClick(function () {
            _this.view.NodeDealItems.active = false;
        }, this);
        this.view.BuySell.addClick(function () {
            _this.onOrderSell();
        }, this);
        this.view.EditeNum.node.on('text-changed', this.onInputNum, this);
        this.view.EditePrice.node.on('text-changed', this.onInputPrice, this);
        this.view.BtnBi.addClick(function () {
            _this.onSelectType("笔");
        }, this);
        this.view.BtnZhi.addClick(function () {
            _this.onSelectType("纸");
        }, this);
        this.view.BtnMo.addClick(function () {
            _this.onSelectType("墨");
        }, this);
        this.view.BtnYan.addClick(function () {
            _this.onSelectType("砚");
        }, this);
        this.view.BtnM.addClick(function () {
            _this.onSelectType("梅");
        }, this);
        this.view.BtnL.addClick(function () {
            _this.onSelectType("兰");
        }, this);
        this.view.BtnZ.addClick(function () {
            _this.onSelectType("竹");
        }, this);
        this.view.BtnJ.addClick(function () {
            _this.onSelectType("菊");
        }, this);
        this.view.BtnXYJ.addClick(function () {
            _this.onSelectType("西游记");
        }, this);
        this.view.BtnHLM.addClick(function () {
            _this.onSelectType("红楼梦");
        }, this);
        this.view.BtnSHZ.addClick(function () {
            _this.onSelectType("水浒传");
        }, this);
        this.view.BtnSGYY.addClick(function () {
            _this.onSelectType("三国演义");
        }, this);
        this.view.BtnQL.addClick(function () {
            _this.onSelectType("青龙");
        }, this);
        this.view.BtnBH.addClick(function () {
            _this.onSelectType("白虎");
        }, this);
        this.view.BtnXW.addClick(function () {
            _this.onSelectType("玄武");
        }, this);
        this.view.BtnZQ.addClick(function () {
            _this.onSelectType("朱雀");
        }, this);
        this.view.BtnZG.addClick(function () {
            _this.onSelectType("曾巩");
        }, this);
        this.view.BtnLZY.addClick(function () {
            _this.onSelectType("柳宗元");
        }, this);
        this.view.BtnWAS.addClick(function () {
            _this.onSelectType("王安石");
        }, this);
        this.view.BtnHY.addClick(function () {
            _this.onSelectType("韩愈");
        }, this);
        this.view.BtnOYX.addClick(function () {
            _this.onSelectType("欧阳修");
        }, this);
        this.view.BtnSX.addClick(function () {
            _this.onSelectType("苏洵");
        }, this);
        this.view.BtnSZ.addClick(function () {
            _this.onSelectType("苏辙");
        }, this);
        this.view.BtnSS.addClick(function () {
            _this.onSelectType("苏轼");
        }, this);
        this.view.BtnJC.addClick(function () {
            _this.onSelectType("金蟾");
        }, this);
        this.view.BtnPX.addClick(function () {
            _this.onSelectType("貔貅");
        }, this);
        this.view.BtnJL.addClick(function () {
            _this.onSelectType("金龙");
        }, this);
    };
    UIDealSell.prototype.onInputNum = function (event) {
        this.deal_num = Number(event.string);
    };
    UIDealSell.prototype.onInputPrice = function (event) {
        this.deal_price = Number(event.string);
    };
    UIDealSell.prototype.onSelectType = function (itme_name) {
        this.view.LabBtn.node.active = false;
        this.view.IconType.node.active = true;
        this.view.IconName.string = itme_name + "碎片";
        CocosHelper_1.default.setDealIcon(itme_name, this.view.IconType);
        this.view.NodeDealItems.active = false;
    };
    UIDealSell.prototype.onOrderSell = function () {
        return __awaiter(this, void 0, void 0, function () {
            var chipid, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.deal_price === 0 || this.deal_price === null) {
                            UIToast_1.default.popUp("请输入您的出售单价");
                            return [2 /*return*/];
                        }
                        if (this.deal_num === 0 || this.deal_num === null) {
                            UIToast_1.default.popUp("请输入您要出售的数量");
                            return [2 /*return*/];
                        }
                        chipid = CocosHelper_1.default.getChipInfoByName(this.view.IconName.string);
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealIssue", { dealType: 1, chipID: Number(chipid), price: this.deal_price, chipNum: this.deal_num })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.DataDealInfo.dealMySellList.push(datas.res.dealList[0]);
                        UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDealMySell.prefabUrl).getComponent(UIDealMySell_1.default).reflashList();
                        UIToast_1.default.popUp("发布订单成功");
                        this.closeSelf();
                        return [2 /*return*/];
                }
            });
        });
    };
    UIDealSell = __decorate([
        ccclass
    ], UIDealSell);
    return UIDealSell;
}(UIForm_1.UIWindow));
exports.default = UIDealSell;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9VSURlYWxTZWxsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR2xGLDhDQUErQztBQUMvQyxpREFBNEM7QUFDNUMscURBQWdEO0FBQ2hELGlEQUFnRDtBQUNoRCwyQ0FBc0M7QUFDdEMsdURBQWtEO0FBQ2xELHNDQUFpQztBQUNqQywrQ0FBMEM7QUFFcEMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBd0MsOEJBQVE7SUFBaEQ7UUFBQSxxRUF1TEM7UUFuTFcsY0FBUSxHQUFXLENBQUMsQ0FBQztRQUNyQixnQkFBVSxHQUFXLENBQUMsQ0FBQzs7SUFrTG5DLENBQUM7SUFoTEcsZUFBZTtJQUVmLDBCQUFLLEdBQUw7UUFBQSxpQkFzSUM7UUFwSUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQztRQUV2QyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNwQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUV2QyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDeEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztZQUM3QixLQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7UUFDckUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUM7WUFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMzQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFDdkIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUV0RSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDcEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDcEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDcEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDcEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFDdkIsS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFFYixDQUFDO0lBRUQsK0JBQVUsR0FBVixVQUFXLEtBQUs7UUFDWixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFFekMsQ0FBQztJQUVELGlDQUFZLEdBQVosVUFBYSxLQUFLO1FBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsU0FBaUI7UUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDN0MscUJBQVcsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUMzQyxDQUFDO0lBRUssZ0NBQVcsR0FBakI7Ozs7Ozt3QkFDSSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFOzRCQUNuRCxpQkFBTyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzs0QkFDM0Isc0JBQU87eUJBQ1Y7d0JBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLElBQUksRUFBRTs0QkFDL0MsaUJBQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7NEJBQzVCLHNCQUFPO3lCQUNWO3dCQUNHLE1BQU0sR0FBRyxxQkFBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3dCQUNwRCxxQkFBTSxxQkFBUyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFBOzt3QkFBMUksS0FBSyxHQUFRLFNBQTZIO3dCQUM5SSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTs0QkFDZixpQkFBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDOzRCQUNqQyxzQkFBTzt5QkFDVjt3QkFDRCxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM3RSxtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxrQkFBUSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsc0JBQVksQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO3dCQUMxRyxpQkFBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDeEIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDOzs7OztLQUNwQjtJQXJMZ0IsVUFBVTtRQUQ5QixPQUFPO09BQ2EsVUFBVSxDQXVMOUI7SUFBRCxpQkFBQztDQXZMRCxBQXVMQyxDQXZMdUMsaUJBQVEsR0F1TC9DO2tCQXZMb0IsVUFBVSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgVUlEZWFsU2VsbF9BdXRvIGZyb20gXCIuLi8uLi9BdXRvU2NyaXB0cy9VSURlYWxTZWxsX0F1dG9cIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCBHYW1lTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL0dhbWVNZ3JcIjtcbmltcG9ydCBVSU1hbmFnZXIgZnJvbSBcIi4uLy4uL01hbmFnZXIvVUlNYW5hZ2VyXCI7XG5pbXBvcnQgeyBhcGlDbGllbnQgfSBmcm9tIFwiLi4vLi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IFVJQ29uZmlnIGZyb20gXCIuLi8uLi9VSUNvbmZpZ1wiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcbmltcG9ydCBVSURlYWxNeVNlbGwgZnJvbSBcIi4vVUlEZWFsTXlTZWxsXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSURlYWxTZWxsIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlEZWFsU2VsbF9BdXRvO1xuXG4gICAgcHJpdmF0ZSBkZWFsX251bTogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIGRlYWxfcHJpY2U6IG51bWJlciA9IDA7XG5cbiAgICAvLyBvbkxvYWQgKCkge31cblxuICAgIHN0YXJ0KCkge1xuXG4gICAgICAgIHRoaXMudmlldy5MYWJSZW1haW4uc3RyaW5nID0gXCLlj6/lh7rllK7mlbDph4/kuLrvvJpcIjtcblxuICAgICAgICB0aGlzLnZpZXcuTGFiQnRuLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy52aWV3Lkljb25UeXBlLm5vZGUuYWN0aXZlID0gZmFsc2U7XG5cbiAgICAgICAgdGhpcy52aWV3LkNsb3NlQnRuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY2xvc2VTZWxmKCk7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5TZWxlY3RUeXBlLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMudmlldy5Ob2RlRGVhbEl0ZW1zLmFjdGl2ZSA9ICF0aGlzLnZpZXcuTm9kZURlYWxJdGVtcy5hY3RpdmU7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5DbG9zZU5vZGVJdGVtcy5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnZpZXcuTm9kZURlYWxJdGVtcy5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ1eVNlbGwuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vbk9yZGVyU2VsbCgpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuRWRpdGVOdW0ubm9kZS5vbigndGV4dC1jaGFuZ2VkJywgdGhpcy5vbklucHV0TnVtLCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LkVkaXRlUHJpY2Uubm9kZS5vbigndGV4dC1jaGFuZ2VkJywgdGhpcy5vbklucHV0UHJpY2UsIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5CaS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIueslFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blpoaS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIue6uFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bk1vLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5aKoXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuWWFuLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi56CaXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuaihVwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkwuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLlhbBcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5aLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi56u5XCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuSi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuiPilwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blhZSi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuilv+a4uOiusFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkhMTS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIue6oualvOaiplwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blNIWi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuawtOa1kuS8oFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blNHWVkuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLkuInlm73mvJTkuYlcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5RTC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIumdkum+mVwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkJILmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi55m96JmOXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuWFcuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnjoTmraZcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5aUS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuacsembgFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blpHLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5pu+5bepXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTFpZLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5p+z5a6X5YWDXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuV0FTLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi546L5a6J55+zXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuSFkuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLpn6nmhIhcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5PWVguYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLmrKfpmLPkv65cIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5TWC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuiLj+a0tVwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blNaLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6IuP6L6ZXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU1MuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLoi4/ovbxcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5KQy5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIumHkeifvlwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blBYLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6LKU6LKFXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuSkwuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLph5HpvplcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgfVxuXG4gICAgb25JbnB1dE51bShldmVudCkge1xuICAgICAgICB0aGlzLmRlYWxfbnVtID0gTnVtYmVyKGV2ZW50LnN0cmluZyk7XG5cbiAgICB9XG5cbiAgICBvbklucHV0UHJpY2UoZXZlbnQpIHtcbiAgICAgICAgdGhpcy5kZWFsX3ByaWNlID0gTnVtYmVyKGV2ZW50LnN0cmluZyk7XG4gICAgfVxuXG4gICAgb25TZWxlY3RUeXBlKGl0bWVfbmFtZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMudmlldy5MYWJCdG4ubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy52aWV3Lkljb25UeXBlLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy52aWV3Lkljb25OYW1lLnN0cmluZyA9IGl0bWVfbmFtZSArIFwi56KO54mHXCI7XG4gICAgICAgIENvY29zSGVscGVyLnNldERlYWxJY29uKGl0bWVfbmFtZSwgdGhpcy52aWV3Lkljb25UeXBlKTtcbiAgICAgICAgdGhpcy52aWV3Lk5vZGVEZWFsSXRlbXMuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgYXN5bmMgb25PcmRlclNlbGwoKSB7XG4gICAgICAgIGlmICh0aGlzLmRlYWxfcHJpY2UgPT09IDAgfHwgdGhpcy5kZWFsX3ByaWNlID09PSBudWxsKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKFwi6K+36L6T5YWl5oKo55qE5Ye65ZSu5Y2V5Lu3XCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmRlYWxfbnVtID09PSAwIHx8IHRoaXMuZGVhbF9udW0gPT09IG51bGwpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoXCLor7fovpPlhaXmgqjopoHlh7rllK7nmoTmlbDph49cIik7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGNoaXBpZCA9IENvY29zSGVscGVyLmdldENoaXBJbmZvQnlOYW1lKHRoaXMudmlldy5JY29uTmFtZS5zdHJpbmcpXG4gICAgICAgIGxldCBkYXRhczogYW55ID0gYXdhaXQgYXBpQ2xpZW50LmNhbGxBcGkoXCJEZWFsSXNzdWVcIiwgeyBkZWFsVHlwZTogMSwgY2hpcElEOiBOdW1iZXIoY2hpcGlkKSwgcHJpY2U6IHRoaXMuZGVhbF9wcmljZSwgY2hpcE51bTogdGhpcy5kZWFsX251bSB9KTtcbiAgICAgICAgaWYgKCFkYXRhcy5pc1N1Y2MpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoZGF0YXMuZXJyLm1lc3NhZ2UpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsTXlTZWxsTGlzdC5wdXNoKGRhdGFzLnJlcy5kZWFsTGlzdFswXSk7XG4gICAgICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLmdldEZvcm0oVUlDb25maWcuVUlEZWFsTXlTZWxsLnByZWZhYlVybCkuZ2V0Q29tcG9uZW50KFVJRGVhbE15U2VsbCkucmVmbGFzaExpc3QoKTtcbiAgICAgICAgVUlUb2FzdC5wb3BVcChcIuWPkeW4g+iuouWNleaIkOWKn1wiKTtcbiAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICB9XG5cbn1cbiJdfQ==