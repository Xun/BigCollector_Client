
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/ItemDealMySell.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5758a1DBa5DaaI4PN+iQ8vo', 'ItemDealMySell');
// Script/UIScript/Shop/ItemDealMySell.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("../../Manager/GameMgr");
var UIManager_1 = require("../../Manager/UIManager");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var UIDealMySell_1 = require("./UIDealMySell");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemDealMySell = /** @class */ (function (_super) {
    __extends(ItemDealMySell, _super);
    function ItemDealMySell() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item_name = null;
        _this.item_num = null;
        _this.item_type = null;
        _this.item_price = null;
        _this.item_icon = null;
        _this.item_cancel = null;
        _this.curType = "进行中";
        _this.deal_id = "";
        return _this;
    }
    ItemDealMySell.prototype.setData = function (data) {
        this.item_name.string = CocosHelper_1.default.getChipNameByID(data.chipID.toString());
        var chip_name = this.item_name.string.replace("碎片", "");
        CocosHelper_1.default.setDealIcon(chip_name, this.item_icon);
        this.item_num.string = "已卖出" + (data.needCount - data.currentCount) + "/" + data.needCount;
        this.item_price.string = data.price.toString();
        if (data.status === -1) {
            this.curType = "订单下架";
        }
        if (data.status === 1) {
            this.curType = "已完成";
        }
        this.item_type.string = this.curType;
        this.deal_id = data.guid;
        this.item_cancel.active = data.status === 0;
    };
    ItemDealMySell.prototype.onCancel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var datas, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealCancel", { type: 1, dealID: this.deal_id })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        for (i = 0; i < GameMgr_1.default.dataModalMgr.DataDealInfo.dealMySellList.length; i++) {
                            if (GameMgr_1.default.dataModalMgr.DataDealInfo.dealMySellList[i].guid == datas.res.dealList[0].guid) {
                                GameMgr_1.default.dataModalMgr.DataDealInfo.dealMySellList[i] = datas.res.dealList[0];
                                UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDealMySell.prefabUrl).getComponent(UIDealMySell_1.default).reflashList();
                                break;
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        property(cc.Label)
    ], ItemDealMySell.prototype, "item_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMySell.prototype, "item_num", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMySell.prototype, "item_type", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMySell.prototype, "item_price", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemDealMySell.prototype, "item_icon", void 0);
    __decorate([
        property(cc.Node)
    ], ItemDealMySell.prototype, "item_cancel", void 0);
    ItemDealMySell = __decorate([
        ccclass
    ], ItemDealMySell);
    return ItemDealMySell;
}(cc.Component));
exports.default = ItemDealMySell;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9JdGVtRGVhbE15U2VsbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdsRixpREFBNEM7QUFDNUMscURBQWdEO0FBQ2hELGlEQUFnRDtBQUNoRCwyQ0FBc0M7QUFDdEMsdURBQWtEO0FBQ2xELHNDQUFpQztBQUNqQywrQ0FBMEM7QUFFcEMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUFzREM7UUFuREcsZUFBUyxHQUFhLElBQUksQ0FBQztRQUczQixjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRzFCLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFHM0IsZ0JBQVUsR0FBYSxJQUFJLENBQUM7UUFHNUIsZUFBUyxHQUFjLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUVwQixhQUFPLEdBQVcsS0FBSyxDQUFDO1FBQ3hCLGFBQU8sR0FBVyxFQUFFLENBQUM7O0lBaUNqQyxDQUFDO0lBaENHLGdDQUFPLEdBQVAsVUFBUSxJQUFrQjtRQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxxQkFBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDNUUsSUFBSSxTQUFTLEdBQVcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNoRSxxQkFBVyxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzNGLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDL0MsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1NBQ3pCO1FBQ0QsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUN4QjtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDckMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFSyxpQ0FBUSxHQUFkOzs7Ozs0QkFDcUIscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUE7O3dCQUFyRixLQUFLLEdBQVEsU0FBd0U7d0JBQ3pGLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFOzRCQUNmLGlCQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQ2pDLHNCQUFPO3lCQUNWO3dCQUVELEtBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQzlFLElBQUksaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFO2dDQUN4RixpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUM1RSxtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxrQkFBUSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsc0JBQVksQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO2dDQUMxRyxNQUFNOzZCQUNUO3lCQUNKOzs7OztLQUNKO0lBbEREO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7cURBQ1E7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztvREFDTztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3FEQUNRO0lBRzNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7c0RBQ1M7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztxREFDUTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3VEQUNVO0lBbEJYLGNBQWM7UUFEbEMsT0FBTztPQUNhLGNBQWMsQ0FzRGxDO0lBQUQscUJBQUM7Q0F0REQsQUFzREMsQ0F0RDJDLEVBQUUsQ0FBQyxTQUFTLEdBc0R2RDtrQkF0RG9CLGNBQWMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IHsgRGF0YURlYWxJdGVtIH0gZnJvbSBcIi4uLy4uL0RhdGFNb2RhbC9EYXRhRGVhbFwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IFVJTWFuYWdlciBmcm9tIFwiLi4vLi4vTWFuYWdlci9VSU1hbmFnZXJcIjtcbmltcG9ydCB7IGFwaUNsaWVudCB9IGZyb20gXCIuLi8uLi9OZXQvUnBjQ29uZW50XCI7XG5pbXBvcnQgVUlDb25maWcgZnJvbSBcIi4uLy4uL1VJQ29uZmlnXCI7XG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5pbXBvcnQgVUlUb2FzdCBmcm9tIFwiLi4vVUlUb2FzdFwiO1xuaW1wb3J0IFVJRGVhbE15U2VsbCBmcm9tIFwiLi9VSURlYWxNeVNlbGxcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEl0ZW1EZWFsTXlTZWxsIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBpdGVtX25hbWU6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBpdGVtX251bTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGl0ZW1fdHlwZTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGl0ZW1fcHJpY2U6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGUpXG4gICAgaXRlbV9pY29uOiBjYy5TcHJpdGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgaXRlbV9jYW5jZWw6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgcHJpdmF0ZSBjdXJUeXBlOiBzdHJpbmcgPSBcIui/m+ihjOS4rVwiO1xuICAgIHByaXZhdGUgZGVhbF9pZDogc3RyaW5nID0gXCJcIjtcbiAgICBzZXREYXRhKGRhdGE6IERhdGFEZWFsSXRlbSkge1xuICAgICAgICB0aGlzLml0ZW1fbmFtZS5zdHJpbmcgPSBDb2Nvc0hlbHBlci5nZXRDaGlwTmFtZUJ5SUQoZGF0YS5jaGlwSUQudG9TdHJpbmcoKSk7XG4gICAgICAgIGxldCBjaGlwX25hbWU6IHN0cmluZyA9IHRoaXMuaXRlbV9uYW1lLnN0cmluZy5yZXBsYWNlKFwi56KO54mHXCIsIFwiXCIpO1xuICAgICAgICBDb2Nvc0hlbHBlci5zZXREZWFsSWNvbihjaGlwX25hbWUsIHRoaXMuaXRlbV9pY29uKTtcbiAgICAgICAgdGhpcy5pdGVtX251bS5zdHJpbmcgPSBcIuW3suWNluWHulwiICsgKGRhdGEubmVlZENvdW50IC0gZGF0YS5jdXJyZW50Q291bnQpICsgXCIvXCIgKyBkYXRhLm5lZWRDb3VudDtcbiAgICAgICAgdGhpcy5pdGVtX3ByaWNlLnN0cmluZyA9IGRhdGEucHJpY2UudG9TdHJpbmcoKTtcbiAgICAgICAgaWYgKGRhdGEuc3RhdHVzID09PSAtMSkge1xuICAgICAgICAgICAgdGhpcy5jdXJUeXBlID0gXCLorqLljZXkuIvmnrZcIjtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZGF0YS5zdGF0dXMgPT09IDEpIHtcbiAgICAgICAgICAgIHRoaXMuY3VyVHlwZSA9IFwi5bey5a6M5oiQXCI7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5pdGVtX3R5cGUuc3RyaW5nID0gdGhpcy5jdXJUeXBlO1xuICAgICAgICB0aGlzLmRlYWxfaWQgPSBkYXRhLmd1aWQ7XG4gICAgICAgIHRoaXMuaXRlbV9jYW5jZWwuYWN0aXZlID0gZGF0YS5zdGF0dXMgPT09IDA7XG4gICAgfVxuXG4gICAgYXN5bmMgb25DYW5jZWwoKSB7XG4gICAgICAgIGxldCBkYXRhczogYW55ID0gYXdhaXQgYXBpQ2xpZW50LmNhbGxBcGkoXCJEZWFsQ2FuY2VsXCIsIHsgdHlwZTogMSwgZGVhbElEOiB0aGlzLmRlYWxfaWQgfSk7XG4gICAgICAgIGlmICghZGF0YXMuaXNTdWNjKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKGRhdGFzLmVyci5tZXNzYWdlKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxNeVNlbGxMaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxNeVNlbGxMaXN0W2ldLmd1aWQgPT0gZGF0YXMucmVzLmRlYWxMaXN0WzBdLmd1aWQpIHtcbiAgICAgICAgICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhRGVhbEluZm8uZGVhbE15U2VsbExpc3RbaV0gPSBkYXRhcy5yZXMuZGVhbExpc3RbMF07XG4gICAgICAgICAgICAgICAgVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkuZ2V0Rm9ybShVSUNvbmZpZy5VSURlYWxNeVNlbGwucHJlZmFiVXJsKS5nZXRDb21wb25lbnQoVUlEZWFsTXlTZWxsKS5yZWZsYXNoTGlzdCgpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuIl19