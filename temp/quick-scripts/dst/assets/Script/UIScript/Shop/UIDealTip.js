
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/UIDealTip.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '817fe3W/UdKyqpf3zAn6UlN', 'UIDealTip');
// Script/UIScript/Shop/UIDealTip.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var UIManager_1 = require("../../Manager/UIManager");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var UIDeal_1 = require("./UIDeal");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealTip = /** @class */ (function (_super) {
    __extends(UIDealTip, _super);
    function UIDealTip() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._dealType = 0;
        _this._chipNum = 0;
        _this._curNum = 0;
        _this._chipName = "";
        _this._dealId = "";
        _this._price = 0;
        return _this;
        // update (dt) {}
    }
    // onLoad () {}
    UIDealTip.prototype.onShow = function (params) {
        if (params.type == 2) {
            this.view.LabBuyOrSell.string = "购买";
            this.view.LabCurNum.string = "购买1个" + params.chipName + "碎片需要" + params.price + "玉石";
            this.view.LabTipType.string = "";
        }
        else {
            this.view.LabBuyOrSell.string = "出售";
            this.view.LabCurNum.string = "当前可出售：999个";
            this.view.LabTipType.string = "出售所得玉石将扣5%手续费";
        }
        this._price = params.price;
        this._dealId = params.orderId;
        this._dealType = params.type;
        this._chipNum = params.chipNum;
        this._chipName = params.chipName;
        this.view.LabName.string = params.chipName + "碎片";
        CocosHelper_1.default.setDealIcon(params.chipName, this.view.SpChipIcon);
    };
    UIDealTip.prototype.start = function () {
        var _this = this;
        this.view.BtnClose.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.EditeNum.node.on('text-changed', this.onInputNum, this);
        this.view.BtnSure.addClick(this.onSureClick, this);
    };
    UIDealTip.prototype.onInputNum = function (event) {
        this._curNum = Number(event.string);
        var curPrice = this._curNum * this._price;
        this.view.LabCurNum.string = "购买" + this._curNum + "个" + this._chipName + "需要" + curPrice + "玉石";
    };
    UIDealTip.prototype.onSureClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this._chipNum <= 0) {
                            UIToast_1.default.popUp("请输入正确的数量");
                            return [2 /*return*/];
                        }
                        if (this._chipNum < this._curNum) {
                            UIToast_1.default.popUp("超过当前订单数量");
                            return [2 /*return*/];
                        }
                        if (!(this._dealType == 2)) return [3 /*break*/, 2];
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealType", { type: this._dealType, dealID: this._dealId, count: this._curNum })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList = data.res.dealList;
                        UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDeal.prefabUrl).getComponent(UIDeal_1.default).reflushBuyList();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealType", { type: this._dealType, dealID: this._dealId, count: this._curNum })];
                    case 3:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList = data.res.dealList;
                        UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDeal.prefabUrl).getComponent(UIDeal_1.default).reflushSellList();
                        _a.label = 4;
                    case 4:
                        this.closeSelf();
                        return [2 /*return*/];
                }
            });
        });
    };
    UIDealTip = __decorate([
        ccclass
    ], UIDealTip);
    return UIDealTip;
}(UIForm_1.UIWindow));
exports.default = UIDealTip;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9VSURlYWxUaXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsOENBQStDO0FBQy9DLGlEQUE0QztBQUM1QyxxREFBZ0Q7QUFDaEQsaURBQWdEO0FBQ2hELDJDQUFzQztBQUN0Qyx1REFBa0Q7QUFDbEQsc0NBQWlDO0FBQ2pDLG1DQUE4QjtBQUV4QixJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUF1Qyw2QkFBUTtJQUEvQztRQUFBLHFFQTJFQztRQXZFVyxlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBQ3RCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFDckIsYUFBTyxHQUFXLENBQUMsQ0FBQztRQUNwQixlQUFTLEdBQVcsRUFBRSxDQUFDO1FBQ3ZCLGFBQU8sR0FBVyxFQUFFLENBQUM7UUFDckIsWUFBTSxHQUFXLENBQUMsQ0FBQzs7UUFpRTNCLGlCQUFpQjtJQUNyQixDQUFDO0lBakVHLGVBQWU7SUFFUiwwQkFBTSxHQUFiLFVBQWMsTUFBVztRQUNyQixJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLE1BQU0sR0FBRyxNQUFNLENBQUMsUUFBUSxHQUFHLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUNyRixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1NBQ3BDO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUM7WUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLGVBQWUsQ0FBQztTQUNqRDtRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQzdCLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUMvQixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2xELHFCQUFXLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBRUQseUJBQUssR0FBTDtRQUFBLGlCQU1DO1FBTEcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCw4QkFBVSxHQUFWLFVBQVcsS0FBSztRQUNaLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksR0FBRyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQ3JHLENBQUM7SUFFSywrQkFBVyxHQUFqQjs7Ozs7O3dCQUNJLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLEVBQUU7NEJBQ3BCLGlCQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUMxQixzQkFBTzt5QkFDVjt3QkFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRTs0QkFDOUIsaUJBQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQzFCLHNCQUFPO3lCQUNWOzZCQUNHLENBQUEsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUEsRUFBbkIsd0JBQW1CO3dCQUNSLHFCQUFNLHFCQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBQTs7d0JBQS9HLElBQUksR0FBRyxTQUF3Rzt3QkFDbkgsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQ2QsaUJBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDaEMsc0JBQU87eUJBQ1Y7d0JBQ0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQzt3QkFDbEUsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsa0JBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQzs7NEJBRXRGLHFCQUFNLHFCQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBQTs7d0JBQS9HLElBQUksR0FBRyxTQUF3Rzt3QkFDbkgsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQ2QsaUJBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDaEMsc0JBQU87eUJBQ1Y7d0JBQ0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQzt3QkFDbkUsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsa0JBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxlQUFlLEVBQUUsQ0FBQzs7O3dCQUV0RyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7Ozs7O0tBQ3BCO0lBeEVnQixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBMkU3QjtJQUFELGdCQUFDO0NBM0VELEFBMkVDLENBM0VzQyxpQkFBUSxHQTJFOUM7a0JBM0VvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgVUlEZWFsVGlwX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJRGVhbFRpcF9BdXRvXCI7XG5pbXBvcnQgeyBVSVdpbmRvdyB9IGZyb20gXCIuLi8uLi9Db21tb24vVUlGb3JtXCI7XG5pbXBvcnQgR2FtZU1nciBmcm9tIFwiLi4vLi4vTWFuYWdlci9HYW1lTWdyXCI7XG5pbXBvcnQgVUlNYW5hZ2VyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL1VJTWFuYWdlclwiO1xuaW1wb3J0IHsgYXBpQ2xpZW50IH0gZnJvbSBcIi4uLy4uL05ldC9ScGNDb25lbnRcIjtcbmltcG9ydCBVSUNvbmZpZyBmcm9tIFwiLi4vLi4vVUlDb25maWdcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmltcG9ydCBVSVRvYXN0IGZyb20gXCIuLi9VSVRvYXN0XCI7XG5pbXBvcnQgVUlEZWFsIGZyb20gXCIuL1VJRGVhbFwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlEZWFsVGlwIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlEZWFsVGlwX0F1dG87XG5cbiAgICBwcml2YXRlIF9kZWFsVHlwZTogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIF9jaGlwTnVtOiBudW1iZXIgPSAwO1xuICAgIHByaXZhdGUgX2N1ck51bTogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIF9jaGlwTmFtZTogc3RyaW5nID0gXCJcIjtcbiAgICBwcml2YXRlIF9kZWFsSWQ6IHN0cmluZyA9IFwiXCI7XG4gICAgcHJpdmF0ZSBfcHJpY2U6IG51bWJlciA9IDA7XG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBwdWJsaWMgb25TaG93KHBhcmFtczogYW55KTogdm9pZCB7XG4gICAgICAgIGlmIChwYXJhbXMudHlwZSA9PSAyKSB7XG4gICAgICAgICAgICB0aGlzLnZpZXcuTGFiQnV5T3JTZWxsLnN0cmluZyA9IFwi6LSt5LmwXCI7XG4gICAgICAgICAgICB0aGlzLnZpZXcuTGFiQ3VyTnVtLnN0cmluZyA9IFwi6LSt5LmwMeS4qlwiICsgcGFyYW1zLmNoaXBOYW1lICsgXCLnoo7niYfpnIDopoFcIiArIHBhcmFtcy5wcmljZSArIFwi546J55+zXCI7XG4gICAgICAgICAgICB0aGlzLnZpZXcuTGFiVGlwVHlwZS5zdHJpbmcgPSBcIlwiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy52aWV3LkxhYkJ1eU9yU2VsbC5zdHJpbmcgPSBcIuWHuuWUrlwiO1xuICAgICAgICAgICAgdGhpcy52aWV3LkxhYkN1ck51bS5zdHJpbmcgPSBcIuW9k+WJjeWPr+WHuuWUru+8mjk5OeS4qlwiO1xuICAgICAgICAgICAgdGhpcy52aWV3LkxhYlRpcFR5cGUuc3RyaW5nID0gXCLlh7rllK7miYDlvpfnjonnn7PlsIbmiaM1JeaJi+e7rei0uVwiO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX3ByaWNlID0gcGFyYW1zLnByaWNlO1xuICAgICAgICB0aGlzLl9kZWFsSWQgPSBwYXJhbXMub3JkZXJJZDtcbiAgICAgICAgdGhpcy5fZGVhbFR5cGUgPSBwYXJhbXMudHlwZTtcbiAgICAgICAgdGhpcy5fY2hpcE51bSA9IHBhcmFtcy5jaGlwTnVtO1xuICAgICAgICB0aGlzLl9jaGlwTmFtZSA9IHBhcmFtcy5jaGlwTmFtZTtcbiAgICAgICAgdGhpcy52aWV3LkxhYk5hbWUuc3RyaW5nID0gcGFyYW1zLmNoaXBOYW1lICsgXCLnoo7niYdcIjtcbiAgICAgICAgQ29jb3NIZWxwZXIuc2V0RGVhbEljb24ocGFyYW1zLmNoaXBOYW1lLCB0aGlzLnZpZXcuU3BDaGlwSWNvbik7XG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5CdG5DbG9zZS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgdGhpcy52aWV3LkVkaXRlTnVtLm5vZGUub24oJ3RleHQtY2hhbmdlZCcsIHRoaXMub25JbnB1dE51bSwgdGhpcyk7XG4gICAgICAgIHRoaXMudmlldy5CdG5TdXJlLmFkZENsaWNrKHRoaXMub25TdXJlQ2xpY2ssIHRoaXMpO1xuICAgIH1cblxuICAgIG9uSW5wdXROdW0oZXZlbnQpIHtcbiAgICAgICAgdGhpcy5fY3VyTnVtID0gTnVtYmVyKGV2ZW50LnN0cmluZyk7XG4gICAgICAgIGxldCBjdXJQcmljZSA9IHRoaXMuX2N1ck51bSAqIHRoaXMuX3ByaWNlO1xuICAgICAgICB0aGlzLnZpZXcuTGFiQ3VyTnVtLnN0cmluZyA9IFwi6LSt5LmwXCIgKyB0aGlzLl9jdXJOdW0gKyBcIuS4qlwiICsgdGhpcy5fY2hpcE5hbWUgKyBcIumcgOimgVwiICsgY3VyUHJpY2UgKyBcIueOieefs1wiO1xuICAgIH1cblxuICAgIGFzeW5jIG9uU3VyZUNsaWNrKCkge1xuICAgICAgICBpZiAodGhpcy5fY2hpcE51bSA8PSAwKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKFwi6K+36L6T5YWl5q2j56Gu55qE5pWw6YePXCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLl9jaGlwTnVtIDwgdGhpcy5fY3VyTnVtKSB7XG4gICAgICAgICAgICBVSVRvYXN0LnBvcFVwKFwi6LaF6L+H5b2T5YmN6K6i5Y2V5pWw6YePXCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLl9kZWFsVHlwZSA9PSAyKSB7XG4gICAgICAgICAgICBsZXQgZGF0YSA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiRGVhbFR5cGVcIiwgeyB0eXBlOiB0aGlzLl9kZWFsVHlwZSwgZGVhbElEOiB0aGlzLl9kZWFsSWQsIGNvdW50OiB0aGlzLl9jdXJOdW0gfSk7XG4gICAgICAgICAgICBpZiAoIWRhdGEuaXNTdWNjKSB7XG4gICAgICAgICAgICAgICAgVUlUb2FzdC5wb3BVcChkYXRhLmVyci5tZXNzYWdlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhRGVhbEluZm8uZGVhbEJ1eUxpc3QgPSBkYXRhLnJlcy5kZWFsTGlzdDtcbiAgICAgICAgICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLmdldEZvcm0oVUlDb25maWcuVUlEZWFsLnByZWZhYlVybCkuZ2V0Q29tcG9uZW50KFVJRGVhbCkucmVmbHVzaEJ1eUxpc3QoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGxldCBkYXRhID0gYXdhaXQgYXBpQ2xpZW50LmNhbGxBcGkoXCJEZWFsVHlwZVwiLCB7IHR5cGU6IHRoaXMuX2RlYWxUeXBlLCBkZWFsSUQ6IHRoaXMuX2RlYWxJZCwgY291bnQ6IHRoaXMuX2N1ck51bSB9KTtcbiAgICAgICAgICAgIGlmICghZGF0YS5pc1N1Y2MpIHtcbiAgICAgICAgICAgICAgICBVSVRvYXN0LnBvcFVwKGRhdGEuZXJyLm1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFEZWFsSW5mby5kZWFsU2VsbExpc3QgPSBkYXRhLnJlcy5kZWFsTGlzdDtcbiAgICAgICAgICAgIFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLmdldEZvcm0oVUlDb25maWcuVUlEZWFsLnByZWZhYlVybCkuZ2V0Q29tcG9uZW50KFVJRGVhbCkucmVmbHVzaFNlbGxMaXN0KCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxufVxuIl19