
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/ItemDealMyGet.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2ad2fuFMYlBuZ+Dk/O9+9jC', 'ItemDealMyGet');
// Script/UIScript/Shop/ItemDealMyGet.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("../../Manager/GameMgr");
var UIManager_1 = require("../../Manager/UIManager");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var UIDealMyGet_1 = require("./UIDealMyGet");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemDealMyGet = /** @class */ (function (_super) {
    __extends(ItemDealMyGet, _super);
    function ItemDealMyGet() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item_name = null;
        _this.item_num = null;
        _this.item_type = null;
        _this.item_price = null;
        _this.item_icon = null;
        _this.item_cancel = null;
        _this.curType = "进行中";
        _this.deal_id = "";
        return _this;
    }
    ItemDealMyGet.prototype.setData = function (data) {
        this.item_name.string = CocosHelper_1.default.getChipNameByID(data.chipID.toString());
        var chip_name = this.item_name.string.replace("碎片", "");
        CocosHelper_1.default.setDealIcon(chip_name, this.item_icon);
        this.item_num.string = "已购入" + (data.needCount - data.currentCount) + "/" + data.needCount;
        this.item_price.string = data.price.toString();
        if (data.status === -1) {
            this.curType = "订单下架";
        }
        if (data.status === 1) {
            this.curType = "已完成";
        }
        this.item_type.string = this.curType;
        this.deal_id = data.guid;
        this.item_cancel.active = data.status === 0;
    };
    ItemDealMyGet.prototype.onCancel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var datas, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealCancel", { type: 2, dealID: this.deal_id })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        for (i = 0; i < GameMgr_1.default.dataModalMgr.DataDealInfo.dealMyBuyList.length; i++) {
                            if (GameMgr_1.default.dataModalMgr.DataDealInfo.dealMyBuyList[i].guid == datas.res.dealList[0].guid) {
                                GameMgr_1.default.dataModalMgr.DataDealInfo.dealMyBuyList[i] = datas.res.dealList[0];
                                UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDealMyGet.prefabUrl).getComponent(UIDealMyGet_1.default).reflashList();
                                break;
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        property(cc.Label)
    ], ItemDealMyGet.prototype, "item_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMyGet.prototype, "item_num", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMyGet.prototype, "item_type", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMyGet.prototype, "item_price", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemDealMyGet.prototype, "item_icon", void 0);
    __decorate([
        property(cc.Node)
    ], ItemDealMyGet.prototype, "item_cancel", void 0);
    ItemDealMyGet = __decorate([
        ccclass
    ], ItemDealMyGet);
    return ItemDealMyGet;
}(cc.Component));
exports.default = ItemDealMyGet;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9JdGVtRGVhbE15R2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvQkFBb0I7QUFDcEIsd0VBQXdFO0FBQ3hFLG1CQUFtQjtBQUNuQixrRkFBa0Y7QUFDbEYsOEJBQThCO0FBQzlCLGtGQUFrRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR2xGLGlEQUE0QztBQUM1QyxxREFBZ0Q7QUFDaEQsaURBQWdEO0FBQ2hELDJDQUFzQztBQUN0Qyx1REFBa0Q7QUFDbEQsc0NBQWlDO0FBQ2pDLDZDQUF3QztBQUVsQyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUEyQyxpQ0FBWTtJQUF2RDtRQUFBLHFFQXFEQztRQWxERyxlQUFTLEdBQWEsSUFBSSxDQUFDO1FBRzNCLGNBQVEsR0FBYSxJQUFJLENBQUM7UUFHMUIsZUFBUyxHQUFhLElBQUksQ0FBQztRQUczQixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUc1QixlQUFTLEdBQWMsSUFBSSxDQUFDO1FBRzVCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRXBCLGFBQU8sR0FBVyxLQUFLLENBQUM7UUFDeEIsYUFBTyxHQUFXLEVBQUUsQ0FBQzs7SUFnQ2pDLENBQUM7SUEvQkcsK0JBQU8sR0FBUCxVQUFRLElBQWtCO1FBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLHFCQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUM1RSxJQUFJLFNBQVMsR0FBVyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLHFCQUFXLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDM0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMvQyxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7U0FDekI7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ3hCO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNyQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVLLGdDQUFRLEdBQWQ7Ozs7OzRCQUNxQixxQkFBTSxxQkFBUyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBQTs7d0JBQXJGLEtBQUssR0FBUSxTQUF3RTt3QkFDekYsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7NEJBQ2YsaUJBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDakMsc0JBQU87eUJBQ1Y7d0JBQ0QsS0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDN0UsSUFBSSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7Z0NBQ3ZGLGlCQUFPLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQzNFLG1CQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLGtCQUFRLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxxQkFBVyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7Z0NBQ3hHLE1BQU07NkJBQ1Q7eUJBQ0o7Ozs7O0tBQ0o7SUFqREQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztvREFDUTtJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO21EQUNPO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7b0RBQ1E7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztxREFDUztJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO29EQUNRO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7c0RBQ1U7SUFsQlgsYUFBYTtRQURqQyxPQUFPO09BQ2EsYUFBYSxDQXFEakM7SUFBRCxvQkFBQztDQXJERCxBQXFEQyxDQXJEMEMsRUFBRSxDQUFDLFNBQVMsR0FxRHREO2tCQXJEb0IsYUFBYSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgeyBEYXRhRGVhbEl0ZW0gfSBmcm9tIFwiLi4vLi4vRGF0YU1vZGFsL0RhdGFEZWFsXCI7XG5pbXBvcnQgR2FtZU1nciBmcm9tIFwiLi4vLi4vTWFuYWdlci9HYW1lTWdyXCI7XG5pbXBvcnQgVUlNYW5hZ2VyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL1VJTWFuYWdlclwiO1xuaW1wb3J0IHsgYXBpQ2xpZW50IH0gZnJvbSBcIi4uLy4uL05ldC9ScGNDb25lbnRcIjtcbmltcG9ydCBVSUNvbmZpZyBmcm9tIFwiLi4vLi4vVUlDb25maWdcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmltcG9ydCBVSVRvYXN0IGZyb20gXCIuLi9VSVRvYXN0XCI7XG5pbXBvcnQgVUlEZWFsTXlHZXQgZnJvbSBcIi4vVUlEZWFsTXlHZXRcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEl0ZW1EZWFsTXlHZXQgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGl0ZW1fbmFtZTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGl0ZW1fbnVtOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgaXRlbV90eXBlOiBjYy5MYWJlbCA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTGFiZWwpXG4gICAgaXRlbV9wcmljZTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBpdGVtX2ljb246IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBpdGVtX2NhbmNlbDogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBwcml2YXRlIGN1clR5cGU6IHN0cmluZyA9IFwi6L+b6KGM5LitXCI7XG4gICAgcHJpdmF0ZSBkZWFsX2lkOiBzdHJpbmcgPSBcIlwiO1xuICAgIHNldERhdGEoZGF0YTogRGF0YURlYWxJdGVtKSB7XG4gICAgICAgIHRoaXMuaXRlbV9uYW1lLnN0cmluZyA9IENvY29zSGVscGVyLmdldENoaXBOYW1lQnlJRChkYXRhLmNoaXBJRC50b1N0cmluZygpKTtcbiAgICAgICAgbGV0IGNoaXBfbmFtZTogc3RyaW5nID0gdGhpcy5pdGVtX25hbWUuc3RyaW5nLnJlcGxhY2UoXCLnoo7niYdcIiwgXCJcIik7XG4gICAgICAgIENvY29zSGVscGVyLnNldERlYWxJY29uKGNoaXBfbmFtZSwgdGhpcy5pdGVtX2ljb24pO1xuICAgICAgICB0aGlzLml0ZW1fbnVtLnN0cmluZyA9IFwi5bey6LSt5YWlXCIgKyAoZGF0YS5uZWVkQ291bnQgLSBkYXRhLmN1cnJlbnRDb3VudCkgKyBcIi9cIiArIGRhdGEubmVlZENvdW50O1xuICAgICAgICB0aGlzLml0ZW1fcHJpY2Uuc3RyaW5nID0gZGF0YS5wcmljZS50b1N0cmluZygpO1xuICAgICAgICBpZiAoZGF0YS5zdGF0dXMgPT09IC0xKSB7XG4gICAgICAgICAgICB0aGlzLmN1clR5cGUgPSBcIuiuouWNleS4i+aetlwiO1xuICAgICAgICB9XG4gICAgICAgIGlmIChkYXRhLnN0YXR1cyA9PT0gMSkge1xuICAgICAgICAgICAgdGhpcy5jdXJUeXBlID0gXCLlt7LlrozmiJBcIjtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLml0ZW1fdHlwZS5zdHJpbmcgPSB0aGlzLmN1clR5cGU7XG4gICAgICAgIHRoaXMuZGVhbF9pZCA9IGRhdGEuZ3VpZDtcbiAgICAgICAgdGhpcy5pdGVtX2NhbmNlbC5hY3RpdmUgPSBkYXRhLnN0YXR1cyA9PT0gMDtcbiAgICB9XG5cbiAgICBhc3luYyBvbkNhbmNlbCgpIHtcbiAgICAgICAgbGV0IGRhdGFzOiBhbnkgPSBhd2FpdCBhcGlDbGllbnQuY2FsbEFwaShcIkRlYWxDYW5jZWxcIiwgeyB0eXBlOiAyLCBkZWFsSUQ6IHRoaXMuZGVhbF9pZCB9KTtcbiAgICAgICAgaWYgKCFkYXRhcy5pc1N1Y2MpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoZGF0YXMuZXJyLm1lc3NhZ2UpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxNeUJ1eUxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhRGVhbEluZm8uZGVhbE15QnV5TGlzdFtpXS5ndWlkID09IGRhdGFzLnJlcy5kZWFsTGlzdFswXS5ndWlkKSB7XG4gICAgICAgICAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxNeUJ1eUxpc3RbaV0gPSBkYXRhcy5yZXMuZGVhbExpc3RbMF07XG4gICAgICAgICAgICAgICAgVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkuZ2V0Rm9ybShVSUNvbmZpZy5VSURlYWxNeUdldC5wcmVmYWJVcmwpLmdldENvbXBvbmVudChVSURlYWxNeUdldCkucmVmbGFzaExpc3QoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==