
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/UIDealAdd.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '25fcfihA/9JfYCL/hbXrsln', 'UIDealAdd');
// Script/UIScript/Shop/UIDealAdd.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var UIManager_1 = require("../../Manager/UIManager");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var UIDealMyGet_1 = require("./UIDealMyGet");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealAdd = /** @class */ (function (_super) {
    __extends(UIDealAdd, _super);
    function UIDealAdd() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.deal_num = 0;
        _this.deal_price = 0;
        return _this;
    }
    UIDealAdd.prototype.onLoad = function () {
    };
    UIDealAdd.prototype.start = function () {
        var _this = this;
        this.view.LabBtn.node.active = true;
        this.view.IconType.node.active = false;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.BtnSelectType.addClick(function () {
            _this.view.NodeDealItems.active = !_this.view.NodeDealItems.active;
        }, this);
        this.view.BtnCloseNodeItems.addClick(function () {
            _this.view.NodeDealItems.active = false;
        }, this);
        this.view.BuyAdd.addClick(function () {
            _this.onOrderGet();
        }, this);
        this.view.EditeNum.node.on('text-changed', this.onInputNum, this);
        this.view.EditePrice.node.on('text-changed', this.onInputPrice, this);
        this.view.BtnBi.addClick(function () {
            _this.onSelectType("笔");
        }, this);
        this.view.BtnZhi.addClick(function () {
            _this.onSelectType("纸");
        }, this);
        this.view.BtnMo.addClick(function () {
            _this.onSelectType("墨");
        }, this);
        this.view.BtnYan.addClick(function () {
            _this.onSelectType("砚");
        }, this);
        this.view.BtnM.addClick(function () {
            _this.onSelectType("梅");
        }, this);
        this.view.BtnL.addClick(function () {
            _this.onSelectType("兰");
        }, this);
        this.view.BtnZ.addClick(function () {
            _this.onSelectType("竹");
        }, this);
        this.view.BtnJ.addClick(function () {
            _this.onSelectType("菊");
        }, this);
        this.view.BtnXYJ.addClick(function () {
            _this.onSelectType("西游记");
        }, this);
        this.view.BtnHLM.addClick(function () {
            _this.onSelectType("红楼梦");
        }, this);
        this.view.BtnSHZ.addClick(function () {
            _this.onSelectType("水浒传");
        }, this);
        this.view.BtnSGYY.addClick(function () {
            _this.onSelectType("三国演义");
        }, this);
        this.view.BtnQL.addClick(function () {
            _this.onSelectType("青龙");
        }, this);
        this.view.BtnBH.addClick(function () {
            _this.onSelectType("白虎");
        }, this);
        this.view.BtnXW.addClick(function () {
            _this.onSelectType("玄武");
        }, this);
        this.view.BtnZQ.addClick(function () {
            _this.onSelectType("朱雀");
        }, this);
        this.view.BtnZG.addClick(function () {
            _this.onSelectType("曾巩");
        }, this);
        this.view.BtnLZY.addClick(function () {
            _this.onSelectType("柳宗元");
        }, this);
        this.view.BtnWAS.addClick(function () {
            _this.onSelectType("王安石");
        }, this);
        this.view.BtnHY.addClick(function () {
            _this.onSelectType("韩愈");
        }, this);
        this.view.BtnOYX.addClick(function () {
            _this.onSelectType("欧阳修");
        }, this);
        this.view.BtnSX.addClick(function () {
            _this.onSelectType("苏洵");
        }, this);
        this.view.BtnSZ.addClick(function () {
            _this.onSelectType("苏辙");
        }, this);
        this.view.BtnSS.addClick(function () {
            _this.onSelectType("苏轼");
        }, this);
        this.view.BtnJC.addClick(function () {
            _this.onSelectType("金蟾");
        }, this);
        this.view.BtnPX.addClick(function () {
            _this.onSelectType("貔貅");
        }, this);
        this.view.BtnJL.addClick(function () {
            _this.onSelectType("金龙");
        }, this);
    };
    UIDealAdd.prototype.onInputNum = function (event) {
        this.deal_num = Number(event.string);
        this.view.LabPay.string = "预付玉石：" + this.deal_num * this.deal_price;
    };
    UIDealAdd.prototype.onInputPrice = function (event) {
        this.deal_price = Number(event.string);
        this.view.LabPay.string = "预付玉石：" + this.deal_num * this.deal_price;
    };
    UIDealAdd.prototype.onSelectType = function (itme_name) {
        this.view.LabBtn.node.active = false;
        this.view.IconType.node.active = true;
        this.view.IconName.string = itme_name + "碎片";
        CocosHelper_1.default.setDealIcon(itme_name, this.view.IconType);
        this.view.NodeDealItems.active = false;
    };
    UIDealAdd.prototype.onOrderGet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var chipid, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.deal_price === 0 || this.deal_price === null) {
                            UIToast_1.default.popUp("请输入您的购买单价");
                            return [2 /*return*/];
                        }
                        if (this.deal_num === 0 || this.deal_num === null) {
                            UIToast_1.default.popUp("请输入您要购买的数量");
                            return [2 /*return*/];
                        }
                        chipid = CocosHelper_1.default.getChipInfoByName(this.view.IconName.string);
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealIssue", { dealType: 2, chipID: Number(chipid), price: this.deal_price, chipNum: this.deal_num })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.DataDealInfo.dealMyBuyList.push(datas.res.dealList[0]);
                        UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDealMyGet.prefabUrl).getComponent(UIDealMyGet_1.default).reflashList();
                        UIToast_1.default.popUp("发布订单成功");
                        this.closeSelf();
                        return [2 /*return*/];
                }
            });
        });
    };
    UIDealAdd = __decorate([
        ccclass
    ], UIDealAdd);
    return UIDealAdd;
}(UIForm_1.UIWindow));
exports.default = UIDealAdd;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9VSURlYWxBZGQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHbEYsOENBQStDO0FBRS9DLGlEQUE0QztBQUM1QyxxREFBZ0Q7QUFDaEQsaURBQWdEO0FBQ2hELDJDQUFzQztBQUN0Qyx1REFBa0Q7QUFDbEQsc0NBQWlDO0FBQ2pDLDZDQUF3QztBQUVsQyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUF1Qyw2QkFBUTtJQUEvQztRQUFBLHFFQXdMQztRQXBMVyxjQUFRLEdBQVcsQ0FBQyxDQUFDO1FBQ3JCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDOztJQW1MbkMsQ0FBQztJQWpMRywwQkFBTSxHQUFOO0lBRUEsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFBQSxpQkFvSUM7UUFsSUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFFdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDN0IsS0FBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1FBQ3JFLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDO1lBQ2pDLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDM0MsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUN0QixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRVQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBRWIsQ0FBQztJQUVELDhCQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3hFLENBQUM7SUFFRCxnQ0FBWSxHQUFaLFVBQWEsS0FBSztRQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN2QyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN4RSxDQUFDO0lBRUQsZ0NBQVksR0FBWixVQUFhLFNBQWlCO1FBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQzdDLHFCQUFXLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDM0MsQ0FBQztJQUVLLDhCQUFVLEdBQWhCOzs7Ozs7d0JBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksRUFBRTs0QkFDbkQsaUJBQU8sQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQzNCLHNCQUFPO3lCQUNWO3dCQUNELElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxJQUFJLEVBQUU7NEJBQy9DLGlCQUFPLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDOzRCQUM1QixzQkFBTzt5QkFDVjt3QkFDRyxNQUFNLEdBQUcscUJBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQTt3QkFDcEQscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBQTs7d0JBQTFJLEtBQUssR0FBUSxTQUE2SDt3QkFDOUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7NEJBQ2YsaUJBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDakMsc0JBQU87eUJBQ1Y7d0JBQ0QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDNUUsbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsa0JBQVEsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLHFCQUFXLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQzt3QkFDeEcsaUJBQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3hCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQzs7Ozs7S0FDcEI7SUF0TGdCLFNBQVM7UUFEN0IsT0FBTztPQUNhLFNBQVMsQ0F3TDdCO0lBQUQsZ0JBQUM7Q0F4TEQsQUF3TEMsQ0F4THNDLGlCQUFRLEdBd0w5QztrQkF4TG9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFVJRGVhbEFkZF9BdXRvIGZyb20gXCIuLi8uLi9BdXRvU2NyaXB0cy9VSURlYWxBZGRfQXV0b1wiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IEZvcm1NZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvRm9ybU1nclwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IFVJTWFuYWdlciBmcm9tIFwiLi4vLi4vTWFuYWdlci9VSU1hbmFnZXJcIjtcbmltcG9ydCB7IGFwaUNsaWVudCB9IGZyb20gXCIuLi8uLi9OZXQvUnBjQ29uZW50XCI7XG5pbXBvcnQgVUlDb25maWcgZnJvbSBcIi4uLy4uL1VJQ29uZmlnXCI7XG5pbXBvcnQgQ29jb3NIZWxwZXIgZnJvbSBcIi4uLy4uL1V0aWxzL0NvY29zSGVscGVyXCI7XG5pbXBvcnQgVUlUb2FzdCBmcm9tIFwiLi4vVUlUb2FzdFwiO1xuaW1wb3J0IFVJRGVhbE15R2V0IGZyb20gXCIuL1VJRGVhbE15R2V0XCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSURlYWxBZGQgZXh0ZW5kcyBVSVdpbmRvdyB7XG5cbiAgICB2aWV3OiBVSURlYWxBZGRfQXV0bztcblxuICAgIHByaXZhdGUgZGVhbF9udW06IG51bWJlciA9IDA7XG4gICAgcHJpdmF0ZSBkZWFsX3ByaWNlOiBudW1iZXIgPSAwO1xuXG4gICAgb25Mb2FkKCkge1xuXG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG5cbiAgICAgICAgdGhpcy52aWV3LkxhYkJ0bi5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMudmlldy5JY29uVHlwZS5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuXG4gICAgICAgIHRoaXMudmlldy5DbG9zZUJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU2VsZWN0VHlwZS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnZpZXcuTm9kZURlYWxJdGVtcy5hY3RpdmUgPSAhdGhpcy52aWV3Lk5vZGVEZWFsSXRlbXMuYWN0aXZlO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuQ2xvc2VOb2RlSXRlbXMuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy52aWV3Lk5vZGVEZWFsSXRlbXMuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdXlBZGQuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vbk9yZGVyR2V0KCk7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5FZGl0ZU51bS5ub2RlLm9uKCd0ZXh0LWNoYW5nZWQnLCB0aGlzLm9uSW5wdXROdW0sIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuRWRpdGVQcmljZS5ub2RlLm9uKCd0ZXh0LWNoYW5nZWQnLCB0aGlzLm9uSW5wdXRQcmljZSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkJpLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi56yUXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuWmhpLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi57q4XCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTW8uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLloqhcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5ZYW4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnoJpcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5NLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5qKFXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuTC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuWFsFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blouYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnq7lcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5KLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6I+KXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuWFlKLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6KW/5ri46K6wXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuSExNLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi57qi5qW85qKmXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU0haLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5rC05rWS5LygXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU0dZWS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuS4ieWbvea8lOS5iVwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blFMLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6Z2S6b6ZXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuQkguYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnmb3omY5cIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5YVy5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIueOhOatplwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blpRLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi5pyx6ZuAXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuWkcuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLmm77lt6lcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5MWlkuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLmn7PlrpflhYNcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5XQVMuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLnjovlronnn7NcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5IWS5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIumfqeaEiFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bk9ZWC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuasp+mYs+S/rlwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0blNYLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6IuP5rS1XCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuU1ouYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLoi4/ovplcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5TUy5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIuiLj+i9vFwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LkJ0bkpDLmFkZENsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMub25TZWxlY3RUeXBlKFwi6YeR6J++XCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnZpZXcuQnRuUFguYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5vblNlbGVjdFR5cGUoXCLospTosoVcIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMudmlldy5CdG5KTC5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm9uU2VsZWN0VHlwZShcIumHkem+mVwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICB9XG5cbiAgICBvbklucHV0TnVtKGV2ZW50KSB7XG4gICAgICAgIHRoaXMuZGVhbF9udW0gPSBOdW1iZXIoZXZlbnQuc3RyaW5nKTtcbiAgICAgICAgdGhpcy52aWV3LkxhYlBheS5zdHJpbmcgPSBcIumihOS7mOeOieefs++8mlwiICsgdGhpcy5kZWFsX251bSAqIHRoaXMuZGVhbF9wcmljZTtcbiAgICB9XG5cbiAgICBvbklucHV0UHJpY2UoZXZlbnQpIHtcbiAgICAgICAgdGhpcy5kZWFsX3ByaWNlID0gTnVtYmVyKGV2ZW50LnN0cmluZyk7XG4gICAgICAgIHRoaXMudmlldy5MYWJQYXkuc3RyaW5nID0gXCLpooTku5jnjonnn7PvvJpcIiArIHRoaXMuZGVhbF9udW0gKiB0aGlzLmRlYWxfcHJpY2U7XG4gICAgfVxuXG4gICAgb25TZWxlY3RUeXBlKGl0bWVfbmFtZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMudmlldy5MYWJCdG4ubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy52aWV3Lkljb25UeXBlLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy52aWV3Lkljb25OYW1lLnN0cmluZyA9IGl0bWVfbmFtZSArIFwi56KO54mHXCI7XG4gICAgICAgIENvY29zSGVscGVyLnNldERlYWxJY29uKGl0bWVfbmFtZSwgdGhpcy52aWV3Lkljb25UeXBlKTtcbiAgICAgICAgdGhpcy52aWV3Lk5vZGVEZWFsSXRlbXMuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgYXN5bmMgb25PcmRlckdldCgpIHtcbiAgICAgICAgaWYgKHRoaXMuZGVhbF9wcmljZSA9PT0gMCB8fCB0aGlzLmRlYWxfcHJpY2UgPT09IG51bGwpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoXCLor7fovpPlhaXmgqjnmoTotK3kubDljZXku7dcIik7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuZGVhbF9udW0gPT09IDAgfHwgdGhpcy5kZWFsX251bSA9PT0gbnVsbCkge1xuICAgICAgICAgICAgVUlUb2FzdC5wb3BVcChcIuivt+i+k+WFpeaCqOimgei0reS5sOeahOaVsOmHj1wiKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBsZXQgY2hpcGlkID0gQ29jb3NIZWxwZXIuZ2V0Q2hpcEluZm9CeU5hbWUodGhpcy52aWV3Lkljb25OYW1lLnN0cmluZylcbiAgICAgICAgbGV0IGRhdGFzOiBhbnkgPSBhd2FpdCBhcGlDbGllbnQuY2FsbEFwaShcIkRlYWxJc3N1ZVwiLCB7IGRlYWxUeXBlOiAyLCBjaGlwSUQ6IE51bWJlcihjaGlwaWQpLCBwcmljZTogdGhpcy5kZWFsX3ByaWNlLCBjaGlwTnVtOiB0aGlzLmRlYWxfbnVtIH0pO1xuICAgICAgICBpZiAoIWRhdGFzLmlzU3VjYykge1xuICAgICAgICAgICAgVUlUb2FzdC5wb3BVcChkYXRhcy5lcnIubWVzc2FnZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YURlYWxJbmZvLmRlYWxNeUJ1eUxpc3QucHVzaChkYXRhcy5yZXMuZGVhbExpc3RbMF0pO1xuICAgICAgICBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5nZXRGb3JtKFVJQ29uZmlnLlVJRGVhbE15R2V0LnByZWZhYlVybCkuZ2V0Q29tcG9uZW50KFVJRGVhbE15R2V0KS5yZWZsYXNoTGlzdCgpO1xuICAgICAgICBVSVRvYXN0LnBvcFVwKFwi5Y+R5biD6K6i5Y2V5oiQ5YqfXCIpO1xuICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgIH1cblxufVxuIl19