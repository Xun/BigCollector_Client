
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/ItemDealDetail.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '20e8dAieq1IiKMrkdUo4PTd', 'ItemDealDetail');
// Script/UIScript/Shop/ItemDealDetail.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemDealDetail = /** @class */ (function (_super) {
    __extends(ItemDealDetail, _super);
    function ItemDealDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item_num = null;
        _this.item_name = null;
        _this.item_time = null;
        _this.item_type = null;
        _this.item_price = null;
        return _this;
    }
    ItemDealDetail.prototype.setData = function (data) {
        this.item_name.string = CocosHelper_1.default.getChipNameByID(data.chipID.toString());
        this.item_num.string = data.count.toString();
        this.item_price.string = data.price.toString();
        this.item_type.string = data.type == 1 ? "卖出" : "买入";
        this.item_time.string = CocosHelper_1.default.changeTime(data.time);
    };
    __decorate([
        property(cc.Label)
    ], ItemDealDetail.prototype, "item_num", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealDetail.prototype, "item_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealDetail.prototype, "item_time", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealDetail.prototype, "item_type", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealDetail.prototype, "item_price", void 0);
    ItemDealDetail = __decorate([
        ccclass
    ], ItemDealDetail);
    return ItemDealDetail;
}(cc.Component));
exports.default = ItemDealDetail;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9JdGVtRGVhbERldGFpbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdsRix1REFBa0Q7QUFFNUMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUF5QkM7UUF0QkcsY0FBUSxHQUFhLElBQUksQ0FBQztRQUcxQixlQUFTLEdBQWEsSUFBSSxDQUFDO1FBRzNCLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFHM0IsZUFBUyxHQUFhLElBQUksQ0FBQztRQUczQixnQkFBVSxHQUFhLElBQUksQ0FBQzs7SUFVaEMsQ0FBQztJQVJHLGdDQUFPLEdBQVAsVUFBUSxJQUF3QjtRQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxxQkFBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNyRCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxxQkFBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQXBCRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO29EQUNPO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7cURBQ1E7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztxREFDUTtJQUczQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3FEQUNRO0lBRzNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7c0RBQ1M7SUFmWCxjQUFjO1FBRGxDLE9BQU87T0FDYSxjQUFjLENBeUJsQztJQUFELHFCQUFDO0NBekJELEFBeUJDLENBekIyQyxFQUFFLENBQUMsU0FBUyxHQXlCdkQ7a0JBekJvQixjQUFjIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCB7IERhdGFEZWFsRGV0YWlsSXRlbSB9IGZyb20gXCIuLi8uLi9EYXRhTW9kYWwvRGF0YURlYWxcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEl0ZW1EZWFsRGV0YWlsIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBpdGVtX251bTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGl0ZW1fbmFtZTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGl0ZW1fdGltZTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGl0ZW1fdHlwZTogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGl0ZW1fcHJpY2U6IGNjLkxhYmVsID0gbnVsbDtcblxuICAgIHNldERhdGEoZGF0YTogRGF0YURlYWxEZXRhaWxJdGVtKSB7XG4gICAgICAgIHRoaXMuaXRlbV9uYW1lLnN0cmluZyA9IENvY29zSGVscGVyLmdldENoaXBOYW1lQnlJRChkYXRhLmNoaXBJRC50b1N0cmluZygpKTtcbiAgICAgICAgdGhpcy5pdGVtX251bS5zdHJpbmcgPSBkYXRhLmNvdW50LnRvU3RyaW5nKCk7XG4gICAgICAgIHRoaXMuaXRlbV9wcmljZS5zdHJpbmcgPSBkYXRhLnByaWNlLnRvU3RyaW5nKCk7XG4gICAgICAgIHRoaXMuaXRlbV90eXBlLnN0cmluZyA9IGRhdGEudHlwZSA9PSAxID8gXCLljZblh7pcIiA6IFwi5Lmw5YWlXCI7XG4gICAgICAgIHRoaXMuaXRlbV90aW1lLnN0cmluZyA9IENvY29zSGVscGVyLmNoYW5nZVRpbWUoZGF0YS50aW1lKTtcbiAgICB9XG5cbn1cbiJdfQ==