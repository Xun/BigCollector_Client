
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Shop/UIShop.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8796eXQYoBPbo5MRJ7Mh+EB', 'UIShop');
// Script/UIScript/Shop/UIShop.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var ItemShop_1 = require("./ItemShop");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIShop = /** @class */ (function (_super) {
    __extends(UIShop, _super);
    function UIShop() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listShop = null;
        return _this;
    }
    UIShop.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        GameMgr_1.default.dataModalMgr.ShopInfo.list_item = GameMgr_1.default.dataModalMgr.ShopInfoJson.shopInfo;
        this.listShop.numItems = GameMgr_1.default.dataModalMgr.ShopInfo.list_item.length;
    };
    //垂直列表渲染器
    UIShop.prototype.onListShopRender = function (item, idx) {
        item.getComponent(ItemShop_1.default).setData(GameMgr_1.default.dataModalMgr.ShopInfo.list_item[idx]);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIShop.prototype, "listShop", void 0);
    UIShop = __decorate([
        ccclass
    ], UIShop);
    return UIShop;
}(UIForm_1.UIWindow));
exports.default = UIShop;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvU2hvcC9VSVNob3AudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9CQUFvQjtBQUNwQix3RUFBd0U7QUFDeEUsbUJBQW1CO0FBQ25CLGtGQUFrRjtBQUNsRiw4QkFBOEI7QUFDOUIsa0ZBQWtGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHbEYsNkRBQXdEO0FBQ3hELDhDQUErQztBQUUvQyxpREFBNEM7QUFFNUMsdUNBQWtDO0FBRTVCLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQW9DLDBCQUFRO0lBQTVDO1FBQUEscUVBbUJDO1FBZEcsY0FBUSxHQUFhLElBQUksQ0FBQzs7SUFjOUIsQ0FBQztJQVpHLHNCQUFLLEdBQUw7UUFBQSxpQkFNQztRQUxHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUN4QixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ1QsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO1FBQ3JGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO0lBQzVFLENBQUM7SUFFRCxTQUFTO0lBQ1QsaUNBQWdCLEdBQWhCLFVBQWlCLElBQWEsRUFBRSxHQUFXO1FBQ3ZDLElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDdEYsQ0FBQztJQWJEO1FBREMsUUFBUSxDQUFDLGtCQUFRLENBQUM7NENBQ087SUFMVCxNQUFNO1FBRDFCLE9BQU87T0FDYSxNQUFNLENBbUIxQjtJQUFELGFBQUM7Q0FuQkQsQUFtQkMsQ0FuQm1DLGlCQUFRLEdBbUIzQztrQkFuQm9CLE1BQU0iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBUeXBlU2NyaXB0OlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvdHlwZXNjcmlwdC5odG1sXG4vLyBMZWFybiBBdHRyaWJ1dGU6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9yZWZlcmVuY2UvYXR0cmlidXRlcy5odG1sXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcblxuaW1wb3J0IFVJU2hvcF9BdXRvIGZyb20gXCIuLi8uLi9BdXRvU2NyaXB0cy9VSVNob3BfQXV0b1wiO1xuaW1wb3J0IExpc3RVdGlsIGZyb20gXCIuLi8uLi9Db21tb24vQ29tcG9uZW50cy9MaXN0VXRpbFwiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IHsgRGF0YVNob3BJdGVtIH0gZnJvbSBcIi4uLy4uL0RhdGFNb2RhbC9EYXRhU2hvcFwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi8uLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IEl0ZW1TaG9wIGZyb20gXCIuL0l0ZW1TaG9wXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSVNob3AgZXh0ZW5kcyBVSVdpbmRvdyB7XG5cbiAgICB2aWV3OiBVSVNob3BfQXV0bztcblxuICAgIEBwcm9wZXJ0eShMaXN0VXRpbClcbiAgICBsaXN0U2hvcDogTGlzdFV0aWwgPSBudWxsO1xuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5DbG9zZUJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuU2hvcEluZm8ubGlzdF9pdGVtID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuU2hvcEluZm9Kc29uLnNob3BJbmZvO1xuICAgICAgICB0aGlzLmxpc3RTaG9wLm51bUl0ZW1zID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuU2hvcEluZm8ubGlzdF9pdGVtLmxlbmd0aDtcbiAgICB9XG5cbiAgICAvL+WeguebtOWIl+ihqOa4suafk+WZqFxuICAgIG9uTGlzdFNob3BSZW5kZXIoaXRlbTogY2MuTm9kZSwgaWR4OiBudW1iZXIpIHtcbiAgICAgICAgaXRlbS5nZXRDb21wb25lbnQoSXRlbVNob3ApLnNldERhdGEoR2FtZU1nci5kYXRhTW9kYWxNZ3IuU2hvcEluZm8ubGlzdF9pdGVtW2lkeF0pO1xuICAgIH1cbn1cbiJdfQ==