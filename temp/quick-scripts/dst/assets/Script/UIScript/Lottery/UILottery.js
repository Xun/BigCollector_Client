
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/Lottery/UILottery.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a2975EwmBRNKaMDx+6NEG7u', 'UILottery');
// Script/UIScript/Lottery/UILottery.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Struct_1 = require("../../Common/Struct");
var SysDefine_1 = require("../../Common/SysDefine");
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UILottery = /** @class */ (function (_super) {
    __extends(UILottery, _super);
    function UILottery() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.modalType = new Struct_1.ModalType(SysDefine_1.ModalOpacity.OpacityHigh);
        _this.lotteryItem = null;
        return _this;
    }
    UILottery.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () { _this.closeSelf(); }, this);
        this.view.GoBtn.addClick(this.onGoClick, this);
        for (var i = 0; i < this.view.LotteryReward.childrenCount; i++) {
            var parentNode = this.view.LotteryReward.children[i];
            var rewardItem = cc.instantiate(this.lotteryItem);
            rewardItem.getComponent("ItemLottery").setInfo({ num: 100 + i, icon: (i + 1).toString() });
            rewardItem.parent = parentNode;
        }
    };
    UILottery.prototype.onGoClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var datas, index;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.view.GoBtn.interactable = false;
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("WheelSurf", {
                                userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount,
                                operationId: 1
                            })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                        }
                        else {
                            this.view.NumTxt.string = "\u6BCF\u592924\uFF1A00\u70B9\u6062\u590D\u62BD\u5956\u6B21\u6570\uFF0C\u5F53\u524D\u5269\u4F59" + datas.res.drawCount + "\u6B21";
                            index = (360 / 8 * datas.res.drawId) + 360 * 5;
                            CocosHelper_1.default.runTweenSync(this.view.LotteryReward, cc.tween()
                                .to(5, { angle: -index }, cc.easeInOut(3.0))).then(function () {
                                _this.view.GoBtn.interactable = true;
                                FormMgr_1.default.open(UIConfig_1.default.UILotteryGetTip, 1);
                                _this.view.LotteryReward.angle = 0;
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        property(cc.Prefab)
    ], UILottery.prototype, "lotteryItem", void 0);
    UILottery = __decorate([
        ccclass
    ], UILottery);
    return UILottery;
}(UIForm_1.UIWindow));
exports.default = UILottery;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvTG90dGVyeS9VSUxvdHRlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsOENBQWdEO0FBQ2hELG9EQUFzRDtBQUN0RCw4Q0FBK0M7QUFDL0MsaURBQTRDO0FBQzVDLGlEQUE0QztBQUM1QyxpREFBZ0Q7QUFDaEQsMkNBQXNDO0FBQ3RDLHVEQUFrRDtBQUNsRCxzQ0FBaUM7QUFFM0IsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBdUMsNkJBQVE7SUFBL0M7UUFBQSxxRUF3Q0M7UUF0Q0csZUFBUyxHQUFHLElBQUksa0JBQVMsQ0FBQyx3QkFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBR3BELGlCQUFXLEdBQWMsSUFBSSxDQUFDOztJQW1DbEMsQ0FBQztJQWpDRyx5QkFBSyxHQUFMO1FBQUEsaUJBU0M7UUFSRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsY0FBUSxLQUFJLENBQUMsU0FBUyxFQUFFLENBQUEsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckQsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDbEQsVUFBVSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzNGLFVBQVUsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDO1NBQ2xDO0lBQ0wsQ0FBQztJQUVLLDZCQUFTLEdBQWY7Ozs7Ozs7d0JBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzt3QkFDekIscUJBQU0scUJBQVMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFO2dDQUM3QyxXQUFXLEVBQUUsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVc7Z0NBQ3RELFdBQVcsRUFBRSxDQUFDOzZCQUNqQixDQUFDLEVBQUE7O3dCQUhFLEtBQUssR0FBRyxTQUdWO3dCQUNGLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFOzRCQUNmLGlCQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ3BDOzZCQUFNOzRCQUNILElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxtR0FBc0IsS0FBSyxDQUFDLEdBQUcsQ0FBQyxTQUFTLFdBQUcsQ0FBQTs0QkFDbEUsS0FBSyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUM7NEJBQ25ELHFCQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUUsQ0FBQyxLQUFLLEVBQUU7aUNBQ3ZELEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQy9DLENBQUMsSUFBSSxDQUFDO2dDQUNILEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0NBQ3BDLGlCQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dDQUMxQyxLQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDOzRCQUN0QyxDQUFDLENBQUMsQ0FBQzt5QkFDTjs7Ozs7S0FHSjtJQWxDRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO2tEQUNVO0lBTGIsU0FBUztRQUQ3QixPQUFPO09BQ2EsU0FBUyxDQXdDN0I7SUFBRCxnQkFBQztDQXhDRCxBQXdDQyxDQXhDc0MsaUJBQVEsR0F3QzlDO2tCQXhDb0IsU0FBUyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBVSUxvdHRlcnlfQXV0byBmcm9tIFwiLi4vLi4vQXV0b1NjcmlwdHMvVUlMb3R0ZXJ5X0F1dG9cIjtcbmltcG9ydCB7IE1vZGFsVHlwZSB9IGZyb20gXCIuLi8uLi9Db21tb24vU3RydWN0XCI7XG5pbXBvcnQgeyBNb2RhbE9wYWNpdHkgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1N5c0RlZmluZVwiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IEZvcm1NZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvRm9ybU1nclwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IHsgYXBpQ2xpZW50IH0gZnJvbSBcIi4uLy4uL05ldC9ScGNDb25lbnRcIjtcbmltcG9ydCBVSUNvbmZpZyBmcm9tIFwiLi4vLi4vVUlDb25maWdcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmltcG9ydCBVSVRvYXN0IGZyb20gXCIuLi9VSVRvYXN0XCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSUxvdHRlcnkgZXh0ZW5kcyBVSVdpbmRvdyB7XG5cbiAgICBtb2RhbFR5cGUgPSBuZXcgTW9kYWxUeXBlKE1vZGFsT3BhY2l0eS5PcGFjaXR5SGlnaCk7XG4gICAgdmlldzogVUlMb3R0ZXJ5X0F1dG87XG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcbiAgICBsb3R0ZXJ5SXRlbTogY2MuUHJlZmFiID0gbnVsbDtcblxuICAgIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLnZpZXcuQ2xvc2VCdG4uYWRkQ2xpY2soKCkgPT4geyB0aGlzLmNsb3NlU2VsZigpIH0sIHRoaXMpO1xuICAgICAgICB0aGlzLnZpZXcuR29CdG4uYWRkQ2xpY2sodGhpcy5vbkdvQ2xpY2ssIHRoaXMpO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudmlldy5Mb3R0ZXJ5UmV3YXJkLmNoaWxkcmVuQ291bnQ7IGkrKykge1xuICAgICAgICAgICAgbGV0IHBhcmVudE5vZGUgPSB0aGlzLnZpZXcuTG90dGVyeVJld2FyZC5jaGlsZHJlbltpXTtcbiAgICAgICAgICAgIGxldCByZXdhcmRJdGVtID0gY2MuaW5zdGFudGlhdGUodGhpcy5sb3R0ZXJ5SXRlbSk7XG4gICAgICAgICAgICByZXdhcmRJdGVtLmdldENvbXBvbmVudChcIkl0ZW1Mb3R0ZXJ5XCIpLnNldEluZm8oeyBudW06IDEwMCArIGksIGljb246IChpICsgMSkudG9TdHJpbmcoKSB9KTtcbiAgICAgICAgICAgIHJld2FyZEl0ZW0ucGFyZW50ID0gcGFyZW50Tm9kZTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFzeW5jIG9uR29DbGljaygpIHtcbiAgICAgICAgdGhpcy52aWV3LkdvQnRuLmludGVyYWN0YWJsZSA9IGZhbHNlO1xuICAgICAgICBsZXQgZGF0YXMgPSBhd2FpdCBhcGlDbGllbnQuY2FsbEFwaShcIldoZWVsU3VyZlwiLCB7XG4gICAgICAgICAgICB1c2VyQWNjb3VudDogR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlckFjY291bnQsXG4gICAgICAgICAgICBvcGVyYXRpb25JZDogMVxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKCFkYXRhcy5pc1N1Y2MpIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoZGF0YXMuZXJyLm1lc3NhZ2UpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy52aWV3Lk51bVR4dC5zdHJpbmcgPSBg5q+P5aSpMjTvvJowMOeCueaBouWkjeaKveWlluasoeaVsO+8jOW9k+WJjeWJqeS9mSR7ZGF0YXMucmVzLmRyYXdDb3VudH3mrKFgXG4gICAgICAgICAgICBsZXQgaW5kZXggPSAoMzYwIC8gOCAqIGRhdGFzLnJlcy5kcmF3SWQpICsgMzYwICogNTtcbiAgICAgICAgICAgIENvY29zSGVscGVyLnJ1blR3ZWVuU3luYyh0aGlzLnZpZXcuTG90dGVyeVJld2FyZCwgY2MudHdlZW4oKVxuICAgICAgICAgICAgICAgIC50byg1LCB7IGFuZ2xlOiAtaW5kZXggfSwgY2MuZWFzZUluT3V0KDMuMCkpXG4gICAgICAgICAgICApLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMudmlldy5Hb0J0bi5pbnRlcmFjdGFibGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5VSUxvdHRlcnlHZXRUaXAsIDEpO1xuICAgICAgICAgICAgICAgIHRoaXMudmlldy5Mb3R0ZXJ5UmV3YXJkLmFuZ2xlID0gMDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cblxuICAgIH1cbn1cbiJdfQ==