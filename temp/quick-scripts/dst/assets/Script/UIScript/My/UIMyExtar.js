
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/My/UIMyExtar.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ec32a86RdVPv6wX2/nK6MG6', 'UIMyExtar');
// Script/UIScript/My/UIMyExtar.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMyExtar = /** @class */ (function (_super) {
    __extends(UIMyExtar, _super);
    function UIMyExtar() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // onLoad () {}
    UIMyExtar.prototype.start = function () {
        var _this = this;
        this.view.BtnClose.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.TodayExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.today_extar_get_money + "元";
        this.view.TodayExtarAddUserNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.today_extar_add_user + "人";
        this.view.MonthExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.month_extar_get_money + "元";
        this.view.MonthExtarAddUserNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.month_extar_add_user + "人";
        this.view.TotalExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_extar_get_money + "元";
        this.view.TotalExtarAddUserNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_extar_add_user + "人";
        this.view.JieDuanNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.cur_stage;
        this.view.JDCurExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.cur_stage_money + "元/";
        this.view.JDTotalExtarMoneyNumLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "元";
        this.view.MultLab.string = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.add_mult + "倍增速中";
        this.view.CompleteMoneyLab.string = "已完成" + GameMgr_1.default.dataModalMgr.DataMyExtarInfo.cur_stage_money / GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "%，完成" + GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_stage_moner + "元自动转入钱包";
        this.view.JDPS.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.DataMyExtarInfo.cur_stage_money / GameMgr_1.default.dataModalMgr.DataMyExtarInfo.total_stage_moner;
    };
    UIMyExtar = __decorate([
        ccclass
    ], UIMyExtar);
    return UIMyExtar;
}(UIForm_1.UIWindow));
exports.default = UIMyExtar;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvTXkvVUlNeUV4dGFyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLDhDQUErQztBQUMvQyxpREFBNEM7QUFFdEMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBdUMsNkJBQVE7SUFBL0M7O0lBMkJBLENBQUM7SUF2QkcsZUFBZTtJQUVmLHlCQUFLLEdBQUw7UUFBQSxpQkFtQkM7UUFsQkcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMscUJBQXFCLEdBQUcsR0FBRyxDQUFDO1FBQzFHLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUM7UUFDM0csSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsQ0FBQztRQUMxRyxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxDQUFDO1FBQzNHLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUM7UUFDMUcsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLG9CQUFvQixHQUFHLEdBQUcsQ0FBQztRQUUzRyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQztRQUNoRixJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNyRyxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLEdBQUcsR0FBRyxDQUFDO1FBQ3hHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQztRQUNsRixJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxLQUFLLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLGVBQWUsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUM7UUFFeE8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLGVBQWUsR0FBRyxpQkFBTyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUM7SUFDekssQ0FBQztJQXpCZ0IsU0FBUztRQUQ3QixPQUFPO09BQ2EsU0FBUyxDQTJCN0I7SUFBRCxnQkFBQztDQTNCRCxBQTJCQyxDQTNCc0MsaUJBQVEsR0EyQjlDO2tCQTNCb0IsU0FBUyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IFVJTXlFeHRhcl9BdXRvIGZyb20gXCIuLi8uLi9BdXRvU2NyaXB0cy9VSU15RXh0YXJfQXV0b1wiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlNeUV4dGFyIGV4dGVuZHMgVUlXaW5kb3cge1xuXG4gICAgdmlldzogVUlNeUV4dGFyX0F1dG87XG5cbiAgICAvLyBvbkxvYWQgKCkge31cblxuICAgIHN0YXJ0KCkge1xuICAgICAgICB0aGlzLnZpZXcuQnRuQ2xvc2UuYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy52aWV3LlRvZGF5RXh0YXJNb25leU51bUxhYi5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhTXlFeHRhckluZm8udG9kYXlfZXh0YXJfZ2V0X21vbmV5ICsgXCLlhYNcIjtcbiAgICAgICAgdGhpcy52aWV3LlRvZGF5RXh0YXJBZGRVc2VyTnVtTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFNeUV4dGFySW5mby50b2RheV9leHRhcl9hZGRfdXNlciArIFwi5Lq6XCI7XG4gICAgICAgIHRoaXMudmlldy5Nb250aEV4dGFyTW9uZXlOdW1MYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YU15RXh0YXJJbmZvLm1vbnRoX2V4dGFyX2dldF9tb25leSArIFwi5YWDXCI7XG4gICAgICAgIHRoaXMudmlldy5Nb250aEV4dGFyQWRkVXNlck51bUxhYi5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhTXlFeHRhckluZm8ubW9udGhfZXh0YXJfYWRkX3VzZXIgKyBcIuS6ulwiO1xuICAgICAgICB0aGlzLnZpZXcuVG90YWxFeHRhck1vbmV5TnVtTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFNeUV4dGFySW5mby50b3RhbF9leHRhcl9nZXRfbW9uZXkgKyBcIuWFg1wiO1xuICAgICAgICB0aGlzLnZpZXcuVG90YWxFeHRhckFkZFVzZXJOdW1MYWIuc3RyaW5nID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YU15RXh0YXJJbmZvLnRvdGFsX2V4dGFyX2FkZF91c2VyICsgXCLkurpcIjtcblxuICAgICAgICB0aGlzLnZpZXcuSmllRHVhbk51bUxhYi5zdHJpbmcgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhTXlFeHRhckluZm8uY3VyX3N0YWdlO1xuICAgICAgICB0aGlzLnZpZXcuSkRDdXJFeHRhck1vbmV5TnVtTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFNeUV4dGFySW5mby5jdXJfc3RhZ2VfbW9uZXkgKyBcIuWFgy9cIjtcbiAgICAgICAgdGhpcy52aWV3LkpEVG90YWxFeHRhck1vbmV5TnVtTGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFNeUV4dGFySW5mby50b3RhbF9zdGFnZV9tb25lciArIFwi5YWDXCI7XG4gICAgICAgIHRoaXMudmlldy5NdWx0TGFiLnN0cmluZyA9IEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFNeUV4dGFySW5mby5hZGRfbXVsdCArIFwi5YCN5aKe6YCf5LitXCI7XG4gICAgICAgIHRoaXMudmlldy5Db21wbGV0ZU1vbmV5TGFiLnN0cmluZyA9IFwi5bey5a6M5oiQXCIgKyBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhTXlFeHRhckluZm8uY3VyX3N0YWdlX21vbmV5IC8gR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YU15RXh0YXJJbmZvLnRvdGFsX3N0YWdlX21vbmVyICsgXCIl77yM5a6M5oiQXCIgKyBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhTXlFeHRhckluZm8udG90YWxfc3RhZ2VfbW9uZXIgKyBcIuWFg+iHquWKqOi9rOWFpemSseWMhVwiO1xuXG4gICAgICAgIHRoaXMudmlldy5KRFBTLmdldENvbXBvbmVudChjYy5Qcm9ncmVzc0JhcikucHJvZ3Jlc3MgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5EYXRhTXlFeHRhckluZm8uY3VyX3N0YWdlX21vbmV5IC8gR2FtZU1nci5kYXRhTW9kYWxNZ3IuRGF0YU15RXh0YXJJbmZvLnRvdGFsX3N0YWdlX21vbmVyO1xuICAgIH1cblxufVxuIl19