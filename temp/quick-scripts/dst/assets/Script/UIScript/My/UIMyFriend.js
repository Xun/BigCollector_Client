
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/My/UIMyFriend.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'adf2cS9dgRF97Ri892cRJya', 'UIMyFriend');
// Script/UIScript/My/UIMyFriend.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var ItemFriend_1 = require("./ItemFriend");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMyFriend = /** @class */ (function (_super) {
    __extends(UIMyFriend, _super);
    function UIMyFriend() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listFriend = null;
        return _this;
    }
    UIMyFriend.prototype.onLoad = function () {
        this.reflashListData();
    };
    UIMyFriend.prototype.start = function () {
        var _this = this;
        this.view.RankToggleNode.children.forEach(function (item) {
            item.on(cc.Node.EventType.TOUCH_END, function (e) {
                _this.reflashListData();
            }, _this);
        });
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
    };
    UIMyFriend.prototype.reflashListData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var index, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        index = CocosHelper_1.default.getContenerindex(this.view.RankToggleNode);
                        index += 1;
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("Linkedln", { userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount, friendType: index })];
                    case 1:
                        datas = _a.sent();
                        if (datas.isSucc) {
                            this.listFriend.numItems = datas.res.friendArray.length;
                            this.listData = datas.res.friendArray;
                        }
                        else {
                            UIToast_1.default.popUp(datas.err.message);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    //垂直列表渲染器
    UIMyFriend.prototype.onListRankRender = function (item, idx) {
        item.getComponent(ItemFriend_1.default).setData(this.listData[idx]);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIMyFriend.prototype, "listFriend", void 0);
    UIMyFriend = __decorate([
        ccclass
    ], UIMyFriend);
    return UIMyFriend;
}(UIForm_1.UIWindow));
exports.default = UIMyFriend;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvTXkvVUlNeUZyaWVuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdsRiw2REFBd0Q7QUFDeEQsOENBQStDO0FBRS9DLGlEQUE0QztBQUM1QyxpREFBZ0Q7QUFDaEQsdURBQWtEO0FBRWxELHNDQUFpQztBQUNqQywyQ0FBc0M7QUFFaEMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBd0MsOEJBQVE7SUFBaEQ7UUFBQSxxRUF1Q0M7UUFuQ0csZ0JBQVUsR0FBYSxJQUFJLENBQUM7O0lBbUNoQyxDQUFDO0lBaENHLDJCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUVELDBCQUFLLEdBQUw7UUFBQSxpQkFVQztRQVRHLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO1lBQzNDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLFVBQUMsQ0FBc0I7Z0JBQ3hELEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUMzQixDQUFDLEVBQUUsS0FBSSxDQUFDLENBQUE7UUFDWixDQUFDLENBQUMsQ0FBQTtRQUVGLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUN4QixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVLLG9DQUFlLEdBQXJCOzs7Ozs7d0JBQ1EsS0FBSyxHQUFHLHFCQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDbkUsS0FBSyxJQUFJLENBQUMsQ0FBQzt3QkFDQyxxQkFBTSxxQkFBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxXQUFXLEVBQUUsaUJBQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBQTs7d0JBQTFILEtBQUssR0FBRyxTQUFrSDt3QkFDOUgsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFOzRCQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzs0QkFDeEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQzt5QkFDekM7NkJBQU07NEJBQ0gsaUJBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzt5QkFDcEM7Ozs7O0tBQ0o7SUFFRCxTQUFTO0lBQ1QscUNBQWdCLEdBQWhCLFVBQWlCLElBQWEsRUFBRSxHQUFXO1FBQ3ZDLElBQUksQ0FBQyxZQUFZLENBQUMsb0JBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQWxDRDtRQURDLFFBQVEsQ0FBQyxrQkFBUSxDQUFDO2tEQUNTO0lBSlgsVUFBVTtRQUQ5QixPQUFPO09BQ2EsVUFBVSxDQXVDOUI7SUFBRCxpQkFBQztDQXZDRCxBQXVDQyxDQXZDdUMsaUJBQVEsR0F1Qy9DO2tCQXZDb0IsVUFBVSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIFR5cGVTY3JpcHQ6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy90eXBlc2NyaXB0Lmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5pbXBvcnQgVUlNeUZyaWVuZF9BdXRvIGZyb20gXCIuLi8uLi9BdXRvU2NyaXB0cy9VSU15RnJpZW5kX0F1dG9cIjtcbmltcG9ydCBMaXN0VXRpbCBmcm9tIFwiLi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvTGlzdFV0aWxcIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uLy4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCB7IERhdGFJdGVtRnJpZW5kSW5mbyB9IGZyb20gXCIuLi8uLi9EYXRhTW9kYWwvRGF0YUZyaWVuZFwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4uLy4uL01hbmFnZXIvR2FtZU1nclwiO1xuaW1wb3J0IHsgYXBpQ2xpZW50IH0gZnJvbSBcIi4uLy4uL05ldC9ScGNDb25lbnRcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmltcG9ydCBJdGVtRnJpZW5kU2VlIGZyb20gXCIuLi9GcmllbmQvSXRlbUZyaWVuZFNlZVwiO1xuaW1wb3J0IFVJVG9hc3QgZnJvbSBcIi4uL1VJVG9hc3RcIjtcbmltcG9ydCBJdGVtRnJpZW5kIGZyb20gXCIuL0l0ZW1GcmllbmRcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJTXlGcmllbmQgZXh0ZW5kcyBVSVdpbmRvdyB7XG5cbiAgICB2aWV3OiBVSU15RnJpZW5kX0F1dG87XG4gICAgQHByb3BlcnR5KExpc3RVdGlsKVxuICAgIGxpc3RGcmllbmQ6IExpc3RVdGlsID0gbnVsbDtcblxuICAgIGxpc3REYXRhOiBEYXRhSXRlbUZyaWVuZEluZm9bXTtcbiAgICBvbkxvYWQoKSB7XG4gICAgICAgIHRoaXMucmVmbGFzaExpc3REYXRhKCk7XG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5SYW5rVG9nZ2xlTm9kZS5jaGlsZHJlbi5mb3JFYWNoKChpdGVtKSA9PiB7XG4gICAgICAgICAgICBpdGVtLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX0VORCwgKGU6IGNjLkV2ZW50LkV2ZW50VG91Y2gpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnJlZmxhc2hMaXN0RGF0YSgpO1xuICAgICAgICAgICAgfSwgdGhpcylcbiAgICAgICAgfSlcblxuICAgICAgICB0aGlzLnZpZXcuQ2xvc2VCdG4uYWRkQ2xpY2soKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5jbG9zZVNlbGYoKTtcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgfVxuXG4gICAgYXN5bmMgcmVmbGFzaExpc3REYXRhKCkge1xuICAgICAgICBsZXQgaW5kZXggPSBDb2Nvc0hlbHBlci5nZXRDb250ZW5lcmluZGV4KHRoaXMudmlldy5SYW5rVG9nZ2xlTm9kZSk7XG4gICAgICAgIGluZGV4ICs9IDE7XG4gICAgICAgIGxldCBkYXRhcyA9IGF3YWl0IGFwaUNsaWVudC5jYWxsQXBpKFwiTGlua2VkbG5cIiwgeyB1c2VyQWNjb3VudDogR2FtZU1nci5kYXRhTW9kYWxNZ3IuVXNlckluZm8udXNlckFjY291bnQsIGZyaWVuZFR5cGU6IGluZGV4IH0pO1xuICAgICAgICBpZiAoZGF0YXMuaXNTdWNjKSB7XG4gICAgICAgICAgICB0aGlzLmxpc3RGcmllbmQubnVtSXRlbXMgPSBkYXRhcy5yZXMuZnJpZW5kQXJyYXkubGVuZ3RoO1xuICAgICAgICAgICAgdGhpcy5saXN0RGF0YSA9IGRhdGFzLnJlcy5mcmllbmRBcnJheTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIFVJVG9hc3QucG9wVXAoZGF0YXMuZXJyLm1lc3NhZ2UpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy/lnoLnm7TliJfooajmuLLmn5PlmahcbiAgICBvbkxpc3RSYW5rUmVuZGVyKGl0ZW06IGNjLk5vZGUsIGlkeDogbnVtYmVyKSB7XG4gICAgICAgIGl0ZW0uZ2V0Q29tcG9uZW50KEl0ZW1GcmllbmQpLnNldERhdGEodGhpcy5saXN0RGF0YVtpZHhdKTtcbiAgICB9XG59XG4iXX0=