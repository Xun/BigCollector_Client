
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/My/UIGetMoneyLog.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '60a43zuBFtAkJxyXpgR2jX+', 'UIGetMoneyLog');
// Script/UIScript/My/UIGetMoneyLog.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var ItemGetMoneyLog_1 = require("./ItemGetMoneyLog");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIGetMoneyLog = /** @class */ (function (_super) {
    __extends(UIGetMoneyLog, _super);
    function UIGetMoneyLog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.list_log = null;
        return _this;
    }
    // onLoad () {}
    UIGetMoneyLog.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
    };
    //垂直列表渲染器
    UIGetMoneyLog.prototype.onListShopRender = function (item, idx) {
        item.getComponent(ItemGetMoneyLog_1.default).setData(GameMgr_1.default.dataModalMgr.DataGetMoneyLogInfo.list_get_money_log[idx]);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIGetMoneyLog.prototype, "list_log", void 0);
    UIGetMoneyLog = __decorate([
        ccclass
    ], UIGetMoneyLog);
    return UIGetMoneyLog;
}(UIForm_1.UIWindow));
exports.default = UIGetMoneyLog;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvTXkvVUlHZXRNb25leUxvZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsb0JBQW9CO0FBQ3BCLHdFQUF3RTtBQUN4RSxtQkFBbUI7QUFDbkIsa0ZBQWtGO0FBQ2xGLDhCQUE4QjtBQUM5QixrRkFBa0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdsRiw2REFBd0Q7QUFDeEQsOENBQStDO0FBRS9DLGlEQUE0QztBQUM1QyxxREFBZ0Q7QUFFMUMsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBMkMsaUNBQVE7SUFBbkQ7UUFBQSxxRUFrQkM7UUFkRyxjQUFRLEdBQWEsSUFBSSxDQUFDOztJQWM5QixDQUFDO0lBWkcsZUFBZTtJQUVmLDZCQUFLLEdBQUw7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztZQUN4QixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVELFNBQVM7SUFDVCx3Q0FBZ0IsR0FBaEIsVUFBaUIsSUFBYSxFQUFFLEdBQVc7UUFDdkMsSUFBSSxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsT0FBTyxDQUFDLGlCQUFPLENBQUMsWUFBWSxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDakgsQ0FBQztJQWJEO1FBREMsUUFBUSxDQUFDLGtCQUFRLENBQUM7bURBQ087SUFKVCxhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBa0JqQztJQUFELG9CQUFDO0NBbEJELEFBa0JDLENBbEIwQyxpQkFBUSxHQWtCbEQ7a0JBbEJvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gTGVhcm4gVHlwZVNjcmlwdDpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3R5cGVzY3JpcHQuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmltcG9ydCBVSUdldE1vbmV5TG9nX0F1dG8gZnJvbSBcIi4uLy4uL0F1dG9TY3JpcHRzL1VJR2V0TW9uZXlMb2dfQXV0b1wiO1xuaW1wb3J0IExpc3RVdGlsIGZyb20gXCIuLi8uLi9Db21tb24vQ29tcG9uZW50cy9MaXN0VXRpbFwiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IHsgRGF0YUl0ZW1HZXRNb25leUxvZyB9IGZyb20gXCIuLi8uLi9EYXRhTW9kYWwvRGF0YUdldE1vbmV5TG9nXCI7XG5pbXBvcnQgR2FtZU1nciBmcm9tIFwiLi4vLi4vTWFuYWdlci9HYW1lTWdyXCI7XG5pbXBvcnQgSXRlbUdldE1vbmV5TG9nIGZyb20gXCIuL0l0ZW1HZXRNb25leUxvZ1wiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlHZXRNb25leUxvZyBleHRlbmRzIFVJV2luZG93IHtcblxuICAgIHZpZXc6IFVJR2V0TW9uZXlMb2dfQXV0bztcbiAgICBAcHJvcGVydHkoTGlzdFV0aWwpXG4gICAgbGlzdF9sb2c6IExpc3RVdGlsID0gbnVsbDtcblxuICAgIC8vIG9uTG9hZCAoKSB7fVxuXG4gICAgc3RhcnQoKSB7XG4gICAgICAgIHRoaXMudmlldy5DbG9zZUJ0bi5hZGRDbGljaygoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICB9XG5cbiAgICAvL+WeguebtOWIl+ihqOa4suafk+WZqFxuICAgIG9uTGlzdFNob3BSZW5kZXIoaXRlbTogY2MuTm9kZSwgaWR4OiBudW1iZXIpIHtcbiAgICAgICAgaXRlbS5nZXRDb21wb25lbnQoSXRlbUdldE1vbmV5TG9nKS5zZXREYXRhKEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkRhdGFHZXRNb25leUxvZ0luZm8ubGlzdF9nZXRfbW9uZXlfbG9nW2lkeF0pO1xuICAgIH1cbn1cbiJdfQ==