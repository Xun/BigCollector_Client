
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/UIScript/My/ItemGetMoneyLog.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '86b442uxNVLvIjwO5NhtVRP', 'ItemGetMoneyLog');
// Script/UIScript/My/ItemGetMoneyLog.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemGetMoneyLog = /** @class */ (function (_super) {
    __extends(ItemGetMoneyLog, _super);
    function ItemGetMoneyLog() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.data_lab = null;
        _this.get_money = null;
        return _this;
    }
    ItemGetMoneyLog.prototype.setData = function (data) {
        this.data_lab.string = data.get_data;
        this.get_money.string = data.get_money_num + "元";
    };
    __decorate([
        property(cc.Label)
    ], ItemGetMoneyLog.prototype, "data_lab", void 0);
    __decorate([
        property(cc.Label)
    ], ItemGetMoneyLog.prototype, "get_money", void 0);
    ItemGetMoneyLog = __decorate([
        ccclass
    ], ItemGetMoneyLog);
    return ItemGetMoneyLog;
}(cc.Component));
exports.default = ItemGetMoneyLog;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVUlTY3JpcHQvTXkvSXRlbUdldE1vbmV5TG9nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVNLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBRzVDO0lBQTZDLG1DQUFZO0lBQXpEO1FBQUEscUVBY0M7UUFYRyxjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRzFCLGVBQVMsR0FBYSxJQUFJLENBQUM7O0lBUS9CLENBQUM7SUFMRyxpQ0FBTyxHQUFQLFVBQVEsSUFBeUI7UUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNyQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQztJQUNyRCxDQUFDO0lBVEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztxREFDTztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3NEQUNRO0lBTlYsZUFBZTtRQURuQyxPQUFPO09BQ2EsZUFBZSxDQWNuQztJQUFELHNCQUFDO0NBZEQsQUFjQyxDQWQ0QyxFQUFFLENBQUMsU0FBUyxHQWN4RDtrQkFkb0IsZUFBZSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERhdGFJdGVtR2V0TW9uZXlMb2cgfSBmcm9tIFwiLi4vLi4vRGF0YU1vZGFsL0RhdGFHZXRNb25leUxvZ1wiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSXRlbUdldE1vbmV5TG9nIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBkYXRhX2xhYjogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLkxhYmVsKVxuICAgIGdldF9tb25leTogY2MuTGFiZWwgPSBudWxsO1xuXG5cbiAgICBzZXREYXRhKGRhdGE6IERhdGFJdGVtR2V0TW9uZXlMb2cpIHtcbiAgICAgICAgdGhpcy5kYXRhX2xhYi5zdHJpbmcgPSBkYXRhLmdldF9kYXRhO1xuICAgICAgICB0aGlzLmdldF9tb25leS5zdHJpbmcgPSBkYXRhLmdldF9tb25leV9udW0gKyBcIuWFg1wiO1xuICAgIH1cblxufSJdfQ==