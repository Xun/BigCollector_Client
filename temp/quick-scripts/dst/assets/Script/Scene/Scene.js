
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Scene/Scene.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b13f3yncLhOvJSRs1QLB0sR', 'Scene');
// Script/Scene/Scene.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
// import AdapterMgr, { AdapterType } from "../Manager/AdapterMgr";
var FormMgr_1 = require("../Manager/FormMgr");
var GameMgr_1 = require("../Manager/GameMgr");
var EventCenter_1 = require("../Net/EventCenter");
var EventType_1 = require("../Net/EventType");
var UIConfig_1 = require("../UIConfig");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Scene = /** @class */ (function (_super) {
    __extends(Scene, _super);
    function Scene() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ndBlock = null;
        /** 设置是否阻挡游戏触摸输入 */
        _this._block = 0;
        return _this;
    }
    Scene_1 = Scene;
    Scene.prototype.onLoad = function () {
        this.initBlockNode();
    };
    Scene.prototype.initBlockNode = function () {
        this.ndBlock = new cc.Node("block");
        this.ndBlock.addComponent(cc.BlockInputEvents);
        this.node.addChild(this.ndBlock, cc.macro.MAX_ZINDEX);
    };
    Scene.prototype.start = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        Scene_1.inst = this;
                        // AdapterMgr.inst.adapteByType(AdapterType.StretchHeight | AdapterType.StretchWidth, this.node);
                        return [4 /*yield*/, this.onGameInit()];
                    case 1:
                        // AdapterMgr.inst.adapteByType(AdapterType.StretchHeight | AdapterType.StretchWidth, this.node);
                        _a.sent();
                        this.registerEvent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 游戏初始化 */
    Scene.prototype.onGameInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // 第一步 展示loading页面，当然有些默认就是loading页面
                        FormMgr_1.default.open(UIConfig_1.default.UILogin);
                        // FormMgr.open(UIConfig.Bottom);
                        // 第二步 初始化游戏（Managers，Configs，SDKs）
                        return [4 /*yield*/, GameMgr_1.default.init(this.node)];
                    case 1:
                        // FormMgr.open(UIConfig.Bottom);
                        // 第二步 初始化游戏（Managers，Configs，SDKs）
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 初始化事件 */
    Scene.prototype.registerEvent = function () {
        cc.game.on(cc.game.EVENT_SHOW, this.onGameShow, this);
        cc.game.on(cc.game.EVENT_HIDE, this.onGameHide, this);
        // EventCenter.on(EventType.login, this.onLogin, this);
    };
    Scene.prototype.onGameShow = function (param) {
        EventCenter_1.EventCenter.emit(EventType_1.EventType.GameShow, param);
        cc.director.resume();
    };
    Scene.prototype.onGameHide = function () {
        EventCenter_1.EventCenter.emit(EventType_1.EventType.GameHide);
        cc.director.pause();
    };
    Scene.prototype.onLogin = function (data) {
        console.log("登陆成功===============");
        FormMgr_1.default.open(UIConfig_1.default.UIHome);
    };
    Scene.prototype.update = function (dt) {
        GameMgr_1.default.update(dt);
    };
    Scene.prototype.lateUpdate = function () {
    };
    Scene.prototype.setInputBlock = function (bool) {
        if (!this.ndBlock) {
            cc.warn("未启用 block input");
            return;
        }
        bool ? ++this._block : --this._block;
        this.ndBlock.active = this._block > 0;
    };
    var Scene_1;
    Scene.inst = null;
    Scene = Scene_1 = __decorate([
        ccclass
    ], Scene);
    return Scene;
}(cc.Component));
exports.default = Scene;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvU2NlbmUvU2NlbmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsbUVBQW1FO0FBQ25FLDhDQUF5QztBQUN6Qyw4Q0FBeUM7QUFFekMsa0RBQWlEO0FBQ2pELDhDQUE2QztBQUM3Qyx3Q0FBbUM7QUFHN0IsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBbUMseUJBQVk7SUFBL0M7UUFBQSxxRUF5RUM7UUF0RVcsYUFBTyxHQUFZLElBQUksQ0FBQztRQTREaEMsbUJBQW1CO1FBQ1gsWUFBTSxHQUFHLENBQUMsQ0FBQzs7SUFTdkIsQ0FBQztjQXpFb0IsS0FBSztJQUt0QixzQkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFTSw2QkFBYSxHQUFwQjtRQUNJLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRVkscUJBQUssR0FBbEI7Ozs7O3dCQUNJLE9BQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO3dCQUNsQixpR0FBaUc7d0JBQ2pHLHFCQUFNLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQTs7d0JBRHZCLGlHQUFpRzt3QkFDakcsU0FBdUIsQ0FBQzt3QkFDeEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDOzs7OztLQUN4QjtJQUNELFlBQVk7SUFDQywwQkFBVSxHQUF2Qjs7Ozs7d0JBQ0ksb0NBQW9DO3dCQUNwQyxpQkFBTyxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUMvQixpQ0FBaUM7d0JBRWpDLG1DQUFtQzt3QkFDbkMscUJBQU0saUJBQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFBOzt3QkFIN0IsaUNBQWlDO3dCQUVqQyxtQ0FBbUM7d0JBQ25DLFNBQTZCLENBQUM7Ozs7O0tBSWpDO0lBQ0QsWUFBWTtJQUNKLDZCQUFhLEdBQXJCO1FBQ0ksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRXRELHVEQUF1RDtJQUMzRCxDQUFDO0lBRU8sMEJBQVUsR0FBbEIsVUFBbUIsS0FBVTtRQUN6Qix5QkFBVyxDQUFDLElBQUksQ0FBQyxxQkFBUyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM1QyxFQUFFLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFBO0lBQ3hCLENBQUM7SUFDTywwQkFBVSxHQUFsQjtRQUNJLHlCQUFXLENBQUMsSUFBSSxDQUFDLHFCQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDckMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsdUJBQU8sR0FBUCxVQUFRLElBQUk7UUFDUixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUE7UUFDbEMsaUJBQU8sQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRU0sc0JBQU0sR0FBYixVQUFjLEVBQVU7UUFDcEIsaUJBQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVNLDBCQUFVLEdBQWpCO0lBRUEsQ0FBQztJQUlNLDZCQUFhLEdBQXBCLFVBQXFCLElBQWE7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZixFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDM0IsT0FBTztTQUNWO1FBQ0QsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUMxQyxDQUFDOztJQXRFYSxVQUFJLEdBQVUsSUFBSSxDQUFDO0lBRmhCLEtBQUs7UUFEekIsT0FBTztPQUNhLEtBQUssQ0F5RXpCO0lBQUQsWUFBQztDQXpFRCxBQXlFQyxDQXpFa0MsRUFBRSxDQUFDLFNBQVMsR0F5RTlDO2tCQXpFb0IsS0FBSyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuLy8gaW1wb3J0IEFkYXB0ZXJNZ3IsIHsgQWRhcHRlclR5cGUgfSBmcm9tIFwiLi4vTWFuYWdlci9BZGFwdGVyTWdyXCI7XG5pbXBvcnQgRm9ybU1nciBmcm9tIFwiLi4vTWFuYWdlci9Gb3JtTWdyXCI7XG5pbXBvcnQgR2FtZU1nciBmcm9tIFwiLi4vTWFuYWdlci9HYW1lTWdyXCI7XG5pbXBvcnQgU2NlbmVNZ3IgZnJvbSBcIi4uL01hbmFnZXIvU2NlbmVNZ3JcIjtcbmltcG9ydCB7IEV2ZW50Q2VudGVyIH0gZnJvbSBcIi4uL05ldC9FdmVudENlbnRlclwiO1xuaW1wb3J0IHsgRXZlbnRUeXBlIH0gZnJvbSBcIi4uL05ldC9FdmVudFR5cGVcIjtcbmltcG9ydCBVSUNvbmZpZyBmcm9tIFwiLi4vVUlDb25maWdcIjtcblxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2NlbmUgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgcHVibGljIHN0YXRpYyBpbnN0OiBTY2VuZSA9IG51bGw7XG4gICAgcHJpdmF0ZSBuZEJsb2NrOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIG9uTG9hZCgpIHtcbiAgICAgICAgdGhpcy5pbml0QmxvY2tOb2RlKCk7XG4gICAgfVxuXG4gICAgcHVibGljIGluaXRCbG9ja05vZGUoKSB7XG4gICAgICAgIHRoaXMubmRCbG9jayA9IG5ldyBjYy5Ob2RlKFwiYmxvY2tcIik7XG4gICAgICAgIHRoaXMubmRCbG9jay5hZGRDb21wb25lbnQoY2MuQmxvY2tJbnB1dEV2ZW50cyk7XG4gICAgICAgIHRoaXMubm9kZS5hZGRDaGlsZCh0aGlzLm5kQmxvY2ssIGNjLm1hY3JvLk1BWF9aSU5ERVgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyBzdGFydCgpIHtcbiAgICAgICAgU2NlbmUuaW5zdCA9IHRoaXM7XG4gICAgICAgIC8vIEFkYXB0ZXJNZ3IuaW5zdC5hZGFwdGVCeVR5cGUoQWRhcHRlclR5cGUuU3RyZXRjaEhlaWdodCB8IEFkYXB0ZXJUeXBlLlN0cmV0Y2hXaWR0aCwgdGhpcy5ub2RlKTtcbiAgICAgICAgYXdhaXQgdGhpcy5vbkdhbWVJbml0KCk7XG4gICAgICAgIHRoaXMucmVnaXN0ZXJFdmVudCgpO1xuICAgIH1cbiAgICAvKiog5ri45oiP5Yid5aeL5YyWICovXG4gICAgcHVibGljIGFzeW5jIG9uR2FtZUluaXQoKSB7XG4gICAgICAgIC8vIOesrOS4gOatpSDlsZXnpLpsb2FkaW5n6aG16Z2i77yM5b2T54S25pyJ5Lqb6buY6K6k5bCx5pivbG9hZGluZ+mhtemdolxuICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlMb2dpbik7XG4gICAgICAgIC8vIEZvcm1NZ3Iub3BlbihVSUNvbmZpZy5Cb3R0b20pO1xuXG4gICAgICAgIC8vIOesrOS6jOatpSDliJ3lp4vljJbmuLjmiI/vvIhNYW5hZ2Vyc++8jENvbmZpZ3PvvIxTREtz77yJXG4gICAgICAgIGF3YWl0IEdhbWVNZ3IuaW5pdCh0aGlzLm5vZGUpO1xuICAgICAgICAvLyDnrKzkuInmraUg5p6E5bu65Yid5aeL5Zy65pmv77yI5Yqg6L295b+F6KaB55qEcHJlZmFi77yM6Z+z6aKR77yMdGV4dHVyZe+8iVxuXG4gICAgICAgIC8vIOesrOWbm+atpSDliqDovb3kuLvnlYzpnaJVSSzlhbPmjolsb2FkaW5n6aG16Z2iLOato+W8j+i/m+WFpea4uOaIj1xuICAgIH1cbiAgICAvKiog5Yid5aeL5YyW5LqL5Lu2ICovXG4gICAgcHJpdmF0ZSByZWdpc3RlckV2ZW50KCkge1xuICAgICAgICBjYy5nYW1lLm9uKGNjLmdhbWUuRVZFTlRfU0hPVywgdGhpcy5vbkdhbWVTaG93LCB0aGlzKTtcbiAgICAgICAgY2MuZ2FtZS5vbihjYy5nYW1lLkVWRU5UX0hJREUsIHRoaXMub25HYW1lSGlkZSwgdGhpcyk7XG5cbiAgICAgICAgLy8gRXZlbnRDZW50ZXIub24oRXZlbnRUeXBlLmxvZ2luLCB0aGlzLm9uTG9naW4sIHRoaXMpO1xuICAgIH1cblxuICAgIHByaXZhdGUgb25HYW1lU2hvdyhwYXJhbTogYW55KSB7XG4gICAgICAgIEV2ZW50Q2VudGVyLmVtaXQoRXZlbnRUeXBlLkdhbWVTaG93LCBwYXJhbSk7XG4gICAgICAgIGNjLmRpcmVjdG9yLnJlc3VtZSgpXG4gICAgfVxuICAgIHByaXZhdGUgb25HYW1lSGlkZSgpIHtcbiAgICAgICAgRXZlbnRDZW50ZXIuZW1pdChFdmVudFR5cGUuR2FtZUhpZGUpO1xuICAgICAgICBjYy5kaXJlY3Rvci5wYXVzZSgpO1xuICAgIH1cblxuICAgIG9uTG9naW4oZGF0YSkge1xuICAgICAgICBjb25zb2xlLmxvZyhcIueZu+mZhuaIkOWKnz09PT09PT09PT09PT09PVwiKVxuICAgICAgICBGb3JtTWdyLm9wZW4oVUlDb25maWcuVUlIb21lKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgdXBkYXRlKGR0OiBudW1iZXIpIHtcbiAgICAgICAgR2FtZU1nci51cGRhdGUoZHQpO1xuICAgIH1cblxuICAgIHB1YmxpYyBsYXRlVXBkYXRlKCkge1xuXG4gICAgfVxuXG4gICAgLyoqIOiuvue9ruaYr+WQpumYu+aMoea4uOaIj+inpuaRuOi+k+WFpSAqL1xuICAgIHByaXZhdGUgX2Jsb2NrID0gMDtcbiAgICBwdWJsaWMgc2V0SW5wdXRCbG9jayhib29sOiBib29sZWFuKSB7XG4gICAgICAgIGlmICghdGhpcy5uZEJsb2NrKSB7XG4gICAgICAgICAgICBjYy53YXJuKFwi5pyq5ZCv55SoIGJsb2NrIGlucHV0XCIpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGJvb2wgPyArK3RoaXMuX2Jsb2NrIDogLS10aGlzLl9ibG9jaztcbiAgICAgICAgdGhpcy5uZEJsb2NrLmFjdGl2ZSA9IHRoaXMuX2Jsb2NrID4gMDtcbiAgICB9XG59Il19