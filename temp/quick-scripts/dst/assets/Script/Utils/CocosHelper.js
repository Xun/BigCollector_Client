
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Utils/CocosHelper.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7d011DLhkxFPakrCRw6PZqk', 'CocosHelper');
// Script/Utils/CocosHelper.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoadProgress = void 0;
////////////////////////////////cocoscreator  Tween动画接口拓展//////////////////////////
/**
 * 倍速播放
 * @param speed 倍速
 * @example tween.speed(2);
 */
cc.Tween.prototype.speed = function (speed) {
    this._finalAction._speedMethod = true;
    this._finalAction._speed = speed;
};
/**
 * 暂停
 * @example tween.pause();
 */
cc.Tween.prototype.pause = function () {
    this._finalAction.paused = true;
};
/**
* 恢复
* @example tween.resume();
*/
cc.Tween.prototype.resume = function () {
    this._finalAction.paused = false;
};
/**
 * 获取持续时间
 * @example let duration = tween.duration();
 */
cc.Tween.prototype.duration = function () {
    return this._finalAction._duration;
};
/**
* 获取已经进行的时间
* @example let elapsed = tween.elapsed();
*/
cc.Tween.prototype.elapsed = function () {
    return this._finalAction._elapsed;
};
/**
 * 跳转到指定时间
 * @param time 时间(秒)
 * @example tween.goto(2);
 */
cc.Tween.prototype.goto = function (time) {
    this._finalAction._goto = true;
    this._finalAction._elapsed = time;
};
///////////////////////////////////////////////////////////////////////////////////////
var GameMgr_1 = require("../Manager/GameMgr");
var LoadProgress = /** @class */ (function () {
    function LoadProgress() {
    }
    return LoadProgress;
}());
exports.LoadProgress = LoadProgress;
/** 一些cocos api 的封装, promise函数统一加上sync后缀 */
var CocosHelper = /** @class */ (function () {
    function CocosHelper() {
    }
    /**
     *
     * @param target
     * @param repeat -1，表示永久执行
     * @param tweens
     */
    CocosHelper.runRepeatTweenSync = function (target, repeat) {
        var tweens = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            tweens[_i - 2] = arguments[_i];
        }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var selfTween = cc.tween(target);
                        for (var _i = 0, tweens_1 = tweens; _i < tweens_1.length; _i++) {
                            var tmpTween = tweens_1[_i];
                            selfTween = selfTween.then(tmpTween);
                        }
                        if (repeat < 0) {
                            cc.tween(target).repeatForever(selfTween).start();
                        }
                        else {
                            cc.tween(target).repeat(repeat, selfTween).call(function () {
                                resolve(true);
                            }).start();
                        }
                    })];
            });
        });
    };
    /** 同步的tween */
    CocosHelper.runTweenSync = function (target) {
        var tweens = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            tweens[_i - 1] = arguments[_i];
        }
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var selfTween = cc.tween(target);
                        for (var _i = 0, tweens_2 = tweens; _i < tweens_2.length; _i++) {
                            var tmpTween = tweens_2[_i];
                            selfTween = selfTween.then(tmpTween);
                        }
                        selfTween.call(function () {
                            resolve();
                        }).start();
                    })];
            });
        });
    };
    /** 停止tween */
    CocosHelper.stopTween = function (target) {
        cc.Tween.stopAllByTarget(target);
    };
    CocosHelper.prototype.stopTweenByTag = function (tag) {
        cc.Tween.stopAllByTag(tag);
    };
    /** 同步的动画 */
    CocosHelper.runAnimSync = function (node, animName) {
        return __awaiter(this, void 0, void 0, function () {
            var anim, clip, clips, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        anim = node.getComponent(cc.Animation);
                        if (!anim)
                            return [2 /*return*/];
                        clip = null;
                        if (!animName)
                            clip = anim.defaultClip;
                        else {
                            clips = anim.getClips();
                            if (typeof (animName) === "number") {
                                clip = clips[animName];
                            }
                            else if (typeof (animName) === "string") {
                                for (i = 0; i < clips.length; i++) {
                                    if (clips[i].name === animName) {
                                        clip = clips[i];
                                        break;
                                    }
                                }
                            }
                        }
                        if (!clip)
                            return [2 /*return*/];
                        return [4 /*yield*/, CocosHelper.sleepSync(clip.duration)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 加载资源异常时抛出错误 */
    CocosHelper.loadResThrowErrorSync = function (url, type, onProgress) {
        return null;
    };
    CocosHelper.loadRes = function (url, type, callback) {
        var _this = this;
        if (this._loadingMap[url]) {
            this._loadingMap[url].push(callback);
            return;
        }
        this._loadingMap[url] = [callback];
        this.loadResSync(url, type).then(function (data) {
            var arr = _this._loadingMap[url];
            for (var _i = 0, arr_1 = arr; _i < arr_1.length; _i++) {
                var func = arr_1[_i];
                func(data);
            }
            _this._loadingMap[url] = null;
            delete _this._loadingMap[url];
        });
    };
    /** 加载资源 */
    CocosHelper.loadResSync = function (url, type, onProgress) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!onProgress)
                onProgress = _this._onProgress;
            cc.resources.load(url, type, onProgress, function (err, asset) {
                if (err) {
                    cc.error(url + " [\u8D44\u6E90\u52A0\u8F7D] \u9519\u8BEF " + err);
                    resolve(null);
                }
                else {
                    resolve(asset);
                }
            });
        });
    };
    /**
     * 加载进度
     * cb方法 其实目的是可以将loader方法的progress
     */
    CocosHelper._onProgress = function (completedCount, totalCount, item) {
        CocosHelper.loadProgress.completedCount = completedCount;
        CocosHelper.loadProgress.totalCount = totalCount;
        CocosHelper.loadProgress.item = item;
        CocosHelper.loadProgress.cb && CocosHelper.loadProgress.cb(completedCount, totalCount, item);
    };
    /**
     * 寻找子节点
     */
    CocosHelper.findChildInNode = function (nodeName, rootNode) {
        if (rootNode.name == nodeName) {
            return rootNode;
        }
        for (var i = 0; i < rootNode.childrenCount; i++) {
            var node = this.findChildInNode(nodeName, rootNode.children[i]);
            if (node) {
                return node;
            }
        }
        return null;
    };
    /** 获得Component的类名 */
    CocosHelper.getComponentName = function (com) {
        var arr = com.name.match(/<.*>$/);
        if (arr && arr.length > 0) {
            return arr[0].slice(1, -1);
        }
        return com.name;
    };
    /** 加载bundle */
    CocosHelper.loadBundleSync = function (url, options) {
        return new Promise(function (resolve, reject) {
            cc.assetManager.loadBundle(url, options, function (err, bundle) {
                if (!err) {
                    cc.error("\u52A0\u8F7Dbundle\u5931\u8D25, url: " + url + ", err:" + err);
                    resolve(null);
                }
                else {
                    resolve(bundle);
                }
            });
        });
    };
    /** 路径是相对分包文件夹路径的相对路径 */
    CocosHelper.loadAssetFromBundleSync = function (bundleName, url) {
        var bundle = cc.assetManager.getBundle(bundleName);
        if (!bundle) {
            cc.error("\u52A0\u8F7Dbundle\u4E2D\u7684\u8D44\u6E90\u5931\u8D25, \u672A\u627E\u5230bundle, bundleUrl:" + bundleName);
            return null;
        }
        return new Promise(function (resolve, reject) {
            bundle.load(url, function (err, asset) {
                if (err) {
                    cc.error("\u52A0\u8F7Dbundle\u4E2D\u7684\u8D44\u6E90\u5931\u8D25, \u672A\u627E\u5230asset, url:" + url + ", err:" + err);
                    resolve(null);
                }
                else {
                    resolve(asset);
                }
            });
        });
    };
    /** 通过路径加载资源, 如果这个资源在bundle内, 会先加载bundle, 在解开bundle获得对应的资源 */
    CocosHelper.loadAssetSync = function (url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            cc.resources.load(url, function (err, assets) {
                if (!err) {
                    cc.error("\u52A0\u8F7Dasset\u5931\u8D25, url:" + url + ", err: " + err);
                    resolve(null);
                }
                else {
                    _this.addRef(assets);
                    resolve(assets);
                }
            });
        });
    };
    /** 释放资源 */
    CocosHelper.releaseAsset = function (assets) {
        this.decRes(assets);
    };
    /** 增加引用计数 */
    CocosHelper.addRef = function (assets) {
        if (assets instanceof Array) {
            for (var _i = 0, assets_1 = assets; _i < assets_1.length; _i++) {
                var a = assets_1[_i];
                a.addRef();
            }
        }
        else {
            assets.addRef();
        }
    };
    /** 减少引用计数, 当引用计数减少到0时,会自动销毁 */
    CocosHelper.decRes = function (assets) {
        if (assets instanceof Array) {
            for (var _i = 0, assets_2 = assets; _i < assets_2.length; _i++) {
                var a = assets_2[_i];
                a.decRef();
            }
        }
        else {
            assets.decRef();
        }
    };
    /** 截图 */
    CocosHelper.captureScreen = function (camera, prop) {
        var newTexture = new cc.RenderTexture();
        // let oldTexture = camera.targetTexture;
        var rect = cc.rect(0, 0, cc.visibleRect.width, cc.visibleRect.height);
        if (prop) {
            if (prop instanceof cc.Node) {
                rect = prop.getBoundingBoxToWorld();
            }
            else {
                rect = prop;
            }
        }
        newTexture.initWithSize(cc.visibleRect.width, cc.visibleRect.height, cc.game._renderContext.STENCIL_INDEX8);
        camera.targetTexture = newTexture;
        camera.render();
        // camera.targetTexture = oldTexture;
        var buffer = new ArrayBuffer(rect.width * rect.height * 4);
        var data = new Uint8Array(buffer);
        newTexture.readPixels(data, rect.x, rect.y, rect.width, rect.height);
        return data;
    };
    /** 加载json资源 */
    CocosHelper.loadJsonSync = function (url, type) {
        return new Promise(function (resolve, reject) {
            cc.resources.load(url, type, function (err, asset) {
                if (err) {
                    cc.error(url + " [\u8D44\u6E90\u52A0\u8F7D] \u9519\u8BEF " + err);
                    resolve(null);
                }
                else {
                    resolve(asset);
                }
            });
        });
    };
    /** 获得单选项索引 */
    CocosHelper.getContenerindex = function (node) {
        if (!node || node.childrenCount < 1)
            return;
        for (var i = 0; i < node.childrenCount; i++) {
            var item = node.children[i];
            var isChecked = item.getComponent(cc.Toggle).isChecked;
            if (isChecked) {
                return i;
            }
        }
    };
    /** 加载头像 */
    CocosHelper.loadHead = function (url, head) {
        return new Promise(function (resolve, reject) {
            cc.assetManager.loadRemote(url, function (err, res) {
                if (err) {
                    console.log("headErr:" + err);
                    resolve(null);
                }
                else {
                    resolve(res);
                }
            });
        }).then(function (res) {
            head.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
        });
    };
    /** 设置单个生产物品的图片 */
    CocosHelper.setItemProduceIcon = function (id, icon, callback) {
        return new Promise(function (resolve, reject) {
            cc.resources.load("imgs/itemProduce/item_" + id, function (err, res) {
                if (err) {
                    resolve(null);
                }
                else {
                    resolve(res);
                }
            });
        }).then(function (res) {
            icon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
            callback();
        });
    };
    /** 根据物品ID，拿到当前物品信息 */
    CocosHelper.getItemProduceInfoById = function (id) {
        var produce = GameMgr_1.default.dataModalMgr.ItemProfucesJson.itemsInfo.json;
        for (var i = 0; i < produce.length; i++) {
            var item = produce[i];
            if (item.id == id) {
                return produce[i];
            }
        }
    };
    /** 根据碎片名字，拿到当前ID */
    CocosHelper.getChipInfoByName = function (chipname) {
        var chips = GameMgr_1.default.dataModalMgr.ChipsInfoJson.chipInfo;
        for (var i = 0; i < chips.length; i++) {
            if (chips[i].name == chipname) {
                return chips[i]["chipId"];
            }
        }
    };
    /** 根据碎片ID，拿到当前名字 */
    CocosHelper.getChipNameByID = function (chipid) {
        var chips = GameMgr_1.default.dataModalMgr.ChipsInfoJson.chipInfo;
        for (var i = 0; i < chips.length; i++) {
            if (chips[i].chipId == chipid) {
                return chips[i]["name"];
            }
        }
    };
    /** 根据碎片ID，拿到当前名字 */
    CocosHelper.getShopNameByID = function (chipid) {
        var shops = GameMgr_1.default.dataModalMgr.ShopInfoJson.shopInfo;
        for (var i = 0; i < shops.length; i++) {
            if (shops[i].id == chipid) {
                return shops[i]["name"];
            }
        }
    };
    /** 根据碎片ID，拿到当前名字 */
    CocosHelper.setShopInfo = function (name, icon) {
        return new Promise(function (resolve, reject) {
            cc.resources.load("imgs/shops/" + name, function (err, res) {
                if (err) {
                    resolve(null);
                }
                else {
                    resolve(res);
                }
            });
        }).then(function (res) {
            icon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
        });
    };
    /** 设置单个生产物品的图片 */
    CocosHelper.setLotteryIcon = function (name, icon) {
        return new Promise(function (resolve, reject) {
            cc.resources.load("imgs/itemProduce/item_" + name, function (err, res) {
                if (err) {
                    resolve(null);
                }
                else {
                    resolve(res);
                }
            });
        }).then(function (res) {
            icon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
        });
    };
    /** 设置物品的图片 */
    CocosHelper.setDealIcon = function (name, icon) {
        return new Promise(function (resolve, reject) {
            cc.resources.load("imgs/chip/" + name, function (err, res) {
                if (err) {
                    resolve(null);
                }
                else {
                    resolve(res);
                }
            });
        }).then(function (res) {
            icon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(res);
        });
    };
    /**
     * 根据剩余秒数格式化剩余时间 返回 HH:MM:SS
     * @param {Number} leftSec
     */
    CocosHelper.formatTimeForSecond = function (leftSec) {
        var timeStr = '';
        var sec = leftSec % 60;
        var leftMin = Math.floor(leftSec / 60);
        leftMin = leftMin < 0 ? 0 : leftMin;
        var hour = Math.floor(leftMin / 60);
        var min = leftMin % 60;
        if (hour > 0) {
            timeStr += hour > 9 ? hour.toString() : '0' + hour;
            timeStr += ':';
        }
        timeStr += min > 9 ? min.toString() : '0' + min;
        timeStr += ':';
        timeStr += sec > 9 ? sec.toString() : '0' + sec;
        return timeStr;
    };
    /**
     * 时间戳转化
     * @param value
     * @returns
     */
    CocosHelper.changeTime = function (value) {
        var date = new Date(value);
        var YY = date.getFullYear();
        var MM = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var DD = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var hh = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var mm = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var ss = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        // 这里修改返回时间的格式
        // return YY + "-" + MM + "-" + DD + " " + hh + ":" + mm + ":" + ss;
        return YY + "-" + MM + "-" + DD;
    };
    /** 加载进度 */
    CocosHelper.loadProgress = new LoadProgress();
    /** 等待时间, 秒为单位 */
    CocosHelper.sleepSync = function (dur) {
        return new Promise(function (resolve, reject) {
            cc.Canvas.instance.scheduleOnce(function () {
                resolve(true);
            }, dur);
        });
    };
    CocosHelper._loadingMap = {};
    return CocosHelper;
}());
exports.default = CocosHelper;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvVXRpbHMvQ29jb3NIZWxwZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUZBQW1GO0FBQ25GOzs7O0dBSUc7QUFDSCxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsVUFBVSxLQUFLO0lBQ3RDLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztJQUN0QyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7QUFDckMsQ0FBQyxDQUFBO0FBRUQ7OztHQUdHO0FBQ0gsRUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHO0lBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztBQUNwQyxDQUFDLENBQUM7QUFFRjs7O0VBR0U7QUFDRixFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUc7SUFDeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0FBQ3JDLENBQUMsQ0FBQztBQUVGOzs7R0FHRztBQUNILEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRztJQUMxQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO0FBQ3ZDLENBQUMsQ0FBQTtBQUVEOzs7RUFHRTtBQUNGLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRztJQUN6QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO0FBQ3RDLENBQUMsQ0FBQTtBQUVEOzs7O0dBSUc7QUFDSCxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsVUFBVSxJQUFJO0lBQ3BDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUMvQixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7QUFDdEMsQ0FBQyxDQUFBO0FBR0QsdUZBQXVGO0FBQ3ZGLDhDQUF5QztBQUV6QztJQUFBO0lBTUEsQ0FBQztJQUFELG1CQUFDO0FBQUQsQ0FOQSxBQU1DLElBQUE7QUFOWSxvQ0FBWTtBQVF6QiwyQ0FBMkM7QUFDM0M7SUFBQTtJQWljQSxDQUFDO0lBbmJHOzs7OztPQUtHO0lBQ2lCLDhCQUFrQixHQUF0QyxVQUF1QyxNQUFXLEVBQUUsTUFBYztRQUFFLGdCQUFxQjthQUFyQixVQUFxQixFQUFyQixxQkFBcUIsRUFBckIsSUFBcUI7WUFBckIsK0JBQXFCOzs7O2dCQUNyRixzQkFBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO3dCQUMvQixJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNqQyxLQUF1QixVQUFNLEVBQU4saUJBQU0sRUFBTixvQkFBTSxFQUFOLElBQU0sRUFBRTs0QkFBMUIsSUFBTSxRQUFRLGVBQUE7NEJBQ2YsU0FBUyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7eUJBQ3hDO3dCQUNELElBQUksTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDWixFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt5QkFDckQ7NkJBQU07NEJBQ0gsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQ0FDNUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNsQixDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt5QkFDZDtvQkFDTCxDQUFDLENBQUMsRUFBQzs7O0tBQ047SUFDRCxlQUFlO0lBQ0ssd0JBQVksR0FBaEMsVUFBaUMsTUFBVztRQUFFLGdCQUFxQjthQUFyQixVQUFxQixFQUFyQixxQkFBcUIsRUFBckIsSUFBcUI7WUFBckIsK0JBQXFCOzt1Q0FBRyxPQUFPOztnQkFDekUsc0JBQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTt3QkFDL0IsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDakMsS0FBdUIsVUFBTSxFQUFOLGlCQUFNLEVBQU4sb0JBQU0sRUFBTixJQUFNLEVBQUU7NEJBQTFCLElBQU0sUUFBUSxlQUFBOzRCQUNmLFNBQVMsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3lCQUN4Qzt3QkFDRCxTQUFTLENBQUMsSUFBSSxDQUFDOzRCQUNYLE9BQU8sRUFBRSxDQUFDO3dCQUNkLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO29CQUNmLENBQUMsQ0FBQyxFQUFDOzs7S0FDTjtJQUNELGNBQWM7SUFDQSxxQkFBUyxHQUF2QixVQUF3QixNQUFXO1FBQy9CLEVBQUUsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFDTSxvQ0FBYyxHQUFyQixVQUFzQixHQUFXO1FBQzdCLEVBQUUsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxZQUFZO0lBQ1EsdUJBQVcsR0FBL0IsVUFBZ0MsSUFBYSxFQUFFLFFBQTBCOzs7Ozs7d0JBQ2pFLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDM0MsSUFBSSxDQUFDLElBQUk7NEJBQUUsc0JBQU87d0JBQ2QsSUFBSSxHQUFxQixJQUFJLENBQUM7d0JBQ2xDLElBQUksQ0FBQyxRQUFROzRCQUFFLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDOzZCQUNsQzs0QkFDRyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDOzRCQUM1QixJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxRQUFRLEVBQUU7Z0NBQ2hDLElBQUksR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7NkJBQzFCO2lDQUFNLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLFFBQVEsRUFBRTtnQ0FDdkMsS0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29DQUNuQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO3dDQUM1QixJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dDQUNoQixNQUFNO3FDQUNUO2lDQUNKOzZCQUNKO3lCQUNKO3dCQUNELElBQUksQ0FBQyxJQUFJOzRCQUFFLHNCQUFPO3dCQUNsQixxQkFBTSxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQTs7d0JBQTFDLFNBQTBDLENBQUM7Ozs7O0tBQzlDO0lBRUQsa0JBQWtCO0lBQ0osaUNBQXFCLEdBQW5DLFVBQXVDLEdBQVcsRUFBRSxJQUFxQixFQUFFLFVBQTRFO1FBQ25KLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFHYSxtQkFBTyxHQUFyQixVQUF5QixHQUFXLEVBQUUsSUFBcUIsRUFBRSxRQUFrQjtRQUEvRSxpQkFjQztRQWJHLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNyQyxPQUFPO1NBQ1Y7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBSSxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBUztZQUMxQyxJQUFJLEdBQUcsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLEtBQW1CLFVBQUcsRUFBSCxXQUFHLEVBQUgsaUJBQUcsRUFBSCxJQUFHLEVBQUU7Z0JBQW5CLElBQU0sSUFBSSxZQUFBO2dCQUNYLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNkO1lBQ0QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDN0IsT0FBTyxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELFdBQVc7SUFDRyx1QkFBVyxHQUF6QixVQUE2QixHQUFXLEVBQUUsSUFBcUIsRUFBRSxVQUE0RTtRQUE3SSxpQkFZQztRQVhHLE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixJQUFJLENBQUMsVUFBVTtnQkFBRSxVQUFVLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQztZQUMvQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxVQUFDLEdBQUcsRUFBRSxLQUFVO2dCQUNyRCxJQUFJLEdBQUcsRUFBRTtvQkFDTCxFQUFFLENBQUMsS0FBSyxDQUFJLEdBQUcsaURBQWMsR0FBSyxDQUFDLENBQUM7b0JBQ3BDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDakI7cUJBQU07b0JBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNsQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0Q7OztPQUdHO0lBQ1ksdUJBQVcsR0FBMUIsVUFBMkIsY0FBc0IsRUFBRSxVQUFrQixFQUFFLElBQVM7UUFDNUUsV0FBVyxDQUFDLFlBQVksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO1FBQ3pELFdBQVcsQ0FBQyxZQUFZLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUNqRCxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDckMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBQ0Q7O09BRUc7SUFDVywyQkFBZSxHQUE3QixVQUE4QixRQUFnQixFQUFFLFFBQWlCO1FBQzdELElBQUksUUFBUSxDQUFDLElBQUksSUFBSSxRQUFRLEVBQUU7WUFDM0IsT0FBTyxRQUFRLENBQUM7U0FDbkI7UUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEUsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sT0FBTyxJQUFJLENBQUM7YUFDZjtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELHFCQUFxQjtJQUNQLDRCQUFnQixHQUE5QixVQUErQixHQUFhO1FBQ3hDLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2xDLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3ZCLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM5QjtRQUNELE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQztJQUNwQixDQUFDO0lBQ0QsZUFBZTtJQUNELDBCQUFjLEdBQTVCLFVBQTZCLEdBQVcsRUFBRSxPQUFZO1FBQ2xELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixFQUFFLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLFVBQUMsR0FBVSxFQUFFLE1BQThCO2dCQUNoRixJQUFJLENBQUMsR0FBRyxFQUFFO29CQUNOLEVBQUUsQ0FBQyxLQUFLLENBQUMsMENBQW9CLEdBQUcsY0FBUyxHQUFLLENBQUMsQ0FBQztvQkFDaEQsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNqQjtxQkFBTTtvQkFDSCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ25CO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx3QkFBd0I7SUFDVixtQ0FBdUIsR0FBckMsVUFBc0MsVUFBa0IsRUFBRSxHQUFzQjtRQUM1RSxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ1QsRUFBRSxDQUFDLEtBQUssQ0FBQyxpR0FBd0MsVUFBWSxDQUFDLENBQUM7WUFDL0QsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxVQUFDLEdBQUcsRUFBRSxLQUE0QjtnQkFDL0MsSUFBSSxHQUFHLEVBQUU7b0JBQ0wsRUFBRSxDQUFDLEtBQUssQ0FBQywwRkFBaUMsR0FBRyxjQUFTLEdBQUssQ0FBQyxDQUFDO29CQUM3RCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2pCO3FCQUFNO29CQUNILE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDbEI7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDZEQUE2RDtJQUMvQyx5QkFBYSxHQUEzQixVQUE0QixHQUFzQjtRQUFsRCxpQkFZQztRQVhHLE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsVUFBQyxHQUFHLEVBQUUsTUFBNkI7Z0JBQ3RELElBQUksQ0FBQyxHQUFHLEVBQUU7b0JBQ04sRUFBRSxDQUFDLEtBQUssQ0FBQyx3Q0FBa0IsR0FBRyxlQUFVLEdBQUssQ0FBQyxDQUFDO29CQUMvQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2pCO3FCQUFNO29CQUNILEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3BCLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDbkI7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELFdBQVc7SUFDRyx3QkFBWSxHQUExQixVQUEyQixNQUE2QjtRQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFDRCxhQUFhO0lBQ0Usa0JBQU0sR0FBckIsVUFBc0IsTUFBNkI7UUFDL0MsSUFBSSxNQUFNLFlBQVksS0FBSyxFQUFFO1lBQ3pCLEtBQWdCLFVBQU0sRUFBTixpQkFBTSxFQUFOLG9CQUFNLEVBQU4sSUFBTSxFQUFFO2dCQUFuQixJQUFNLENBQUMsZUFBQTtnQkFDUixDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDZDtTQUNKO2FBQU07WUFDSCxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDbkI7SUFDTCxDQUFDO0lBQ0QsK0JBQStCO0lBQ2hCLGtCQUFNLEdBQXJCLFVBQXNCLE1BQTZCO1FBQy9DLElBQUksTUFBTSxZQUFZLEtBQUssRUFBRTtZQUN6QixLQUFnQixVQUFNLEVBQU4saUJBQU0sRUFBTixvQkFBTSxFQUFOLElBQU0sRUFBRTtnQkFBbkIsSUFBTSxDQUFDLGVBQUE7Z0JBQ1IsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ2Q7U0FDSjthQUFNO1lBQ0gsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ25CO0lBQ0wsQ0FBQztJQUVELFNBQVM7SUFDSyx5QkFBYSxHQUEzQixVQUE0QixNQUFpQixFQUFFLElBQXdCO1FBQ25FLElBQUksVUFBVSxHQUFHLElBQUksRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3hDLHlDQUF5QztRQUN6QyxJQUFJLElBQUksR0FBWSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRSxJQUFJLElBQUksRUFBRTtZQUNOLElBQUksSUFBSSxZQUFZLEVBQUUsQ0FBQyxJQUFJLEVBQUU7Z0JBQ3pCLElBQUksR0FBRyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQzthQUN2QztpQkFBTTtnQkFDSCxJQUFJLEdBQUcsSUFBSSxDQUFDO2FBQ2Y7U0FDSjtRQUNELFVBQVUsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDNUcsTUFBTSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUM7UUFDbEMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2hCLHFDQUFxQztRQUVyQyxJQUFJLE1BQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDM0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbEMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JFLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxlQUFlO0lBQ0Qsd0JBQVksR0FBMUIsVUFBOEIsR0FBVyxFQUFFLElBQXlCO1FBQ2hFLE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLFVBQUMsR0FBRyxFQUFFLEtBQVU7Z0JBQ3pDLElBQUksR0FBRyxFQUFFO29CQUNMLEVBQUUsQ0FBQyxLQUFLLENBQUksR0FBRyxpREFBYyxHQUFLLENBQUMsQ0FBQztvQkFDcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNqQjtxQkFBTTtvQkFDSCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2xCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxjQUFjO0lBQ0EsNEJBQWdCLEdBQTlCLFVBQStCLElBQWE7UUFDeEMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUM7WUFBRSxPQUFPO1FBRTVDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDO1lBQ3ZELElBQUksU0FBUyxFQUFFO2dCQUNYLE9BQU8sQ0FBQyxDQUFBO2FBQ1g7U0FDSjtJQUNMLENBQUM7SUFFRCxXQUFXO0lBQ0csb0JBQVEsR0FBdEIsVUFBdUIsR0FBVyxFQUFFLElBQWU7UUFDL0MsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9CLEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFpQjtnQkFDbkQsSUFBSSxHQUFHLEVBQUU7b0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLENBQUM7b0JBQzlCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDakI7cUJBQU07b0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUNoQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBaUI7WUFDdEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUMsQ0FBQTtJQUVOLENBQUM7SUFFRCxrQkFBa0I7SUFDSiw4QkFBa0IsR0FBaEMsVUFBaUMsRUFBVSxFQUFFLElBQWUsRUFBRSxRQUFrQjtRQUM1RSxPQUFPLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDL0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQWlCO2dCQUNwRSxJQUFJLEdBQUcsRUFBRTtvQkFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2pCO3FCQUFNO29CQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQTtpQkFDZjtZQUNMLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBaUI7WUFDdEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuRSxRQUFRLEVBQUUsQ0FBQztRQUNmLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELHNCQUFzQjtJQUNSLGtDQUFzQixHQUFwQyxVQUFxQyxFQUFVO1FBQzNDLElBQUksT0FBTyxHQUFHLGlCQUFPLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUE7UUFDbEUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDckMsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUU7Z0JBQ2YsT0FBTyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDckI7U0FDSjtJQUNMLENBQUM7SUFFRCxvQkFBb0I7SUFDTiw2QkFBaUIsR0FBL0IsVUFBZ0MsUUFBZ0I7UUFDNUMsSUFBSSxLQUFLLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQTtRQUN2RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksUUFBUSxFQUFFO2dCQUMzQixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUM3QjtTQUNKO0lBQ0wsQ0FBQztJQUVELG9CQUFvQjtJQUNOLDJCQUFlLEdBQTdCLFVBQThCLE1BQWM7UUFDeEMsSUFBSSxLQUFLLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztRQUN4RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxFQUFFO2dCQUMzQixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUMzQjtTQUNKO0lBQ0wsQ0FBQztJQUVELG9CQUFvQjtJQUNOLDJCQUFlLEdBQTdCLFVBQThCLE1BQWM7UUFDeEMsSUFBSSxLQUFLLEdBQUcsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztRQUN2RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksTUFBTSxFQUFFO2dCQUN2QixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUMzQjtTQUNKO0lBQ0wsQ0FBQztJQUVELG9CQUFvQjtJQUNOLHVCQUFXLEdBQXpCLFVBQTBCLElBQVksRUFBRSxJQUFlO1FBQ25ELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQWlCO2dCQUMzRCxJQUFJLEdBQUcsRUFBRTtvQkFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2pCO3FCQUFNO29CQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDaEI7WUFDTCxDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLEdBQWlCO1lBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkUsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRUQsa0JBQWtCO0lBQ0osMEJBQWMsR0FBNUIsVUFBNkIsSUFBWSxFQUFFLElBQWU7UUFDdEQsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9CLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksRUFBRSxVQUFDLEdBQUcsRUFBRSxHQUFpQjtnQkFDdEUsSUFBSSxHQUFHLEVBQUU7b0JBQ0wsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNqQjtxQkFBTTtvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUE7aUJBQ2Y7WUFDTCxDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLEdBQWlCO1lBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkUsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRUQsY0FBYztJQUNBLHVCQUFXLEdBQXpCLFVBQTBCLElBQVksRUFBRSxJQUFlO1FBQ25ELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxFQUFFLFVBQUMsR0FBRyxFQUFFLEdBQWlCO2dCQUMxRCxJQUFJLEdBQUcsRUFBRTtvQkFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2pCO3FCQUFNO29CQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQTtpQkFDZjtZQUNMLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBaUI7WUFDdEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRDs7O09BR0c7SUFDVywrQkFBbUIsR0FBakMsVUFBa0MsT0FBZTtRQUM3QyxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDakIsSUFBSSxHQUFHLEdBQUcsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUV2QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztRQUN2QyxPQUFPLEdBQUcsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7UUFFcEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDcEMsSUFBSSxHQUFHLEdBQUcsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUV2QixJQUFJLElBQUksR0FBRyxDQUFDLEVBQUU7WUFDVixPQUFPLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO1lBQ25ELE9BQU8sSUFBSSxHQUFHLENBQUM7U0FDbEI7UUFFRCxPQUFPLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2hELE9BQU8sSUFBSSxHQUFHLENBQUM7UUFDZixPQUFPLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2hELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFFRDs7OztPQUlHO0lBQ1csc0JBQVUsR0FBeEIsVUFBeUIsS0FBYTtRQUNsQyxJQUFJLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDNUIsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN0RixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDckUsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hFLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxVQUFVLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUM5RSxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFFOUUsY0FBYztRQUNkLG9FQUFvRTtRQUNwRSxPQUFPLEVBQUUsR0FBRyxHQUFHLEdBQUcsRUFBRSxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUE7SUFDbkMsQ0FBQztJQTVhRCxXQUFXO0lBQ0csd0JBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBRWhELGlCQUFpQjtJQUNILHFCQUFTLEdBQUcsVUFBVSxHQUFXO1FBQzNDLE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUMvQixFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7Z0JBQzVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQTtJQXVFYyx1QkFBVyxHQUFrQyxFQUFFLENBQUM7SUE4V25FLGtCQUFDO0NBamNELEFBaWNDLElBQUE7a0JBamNvQixXQUFXIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9jb2Nvc2NyZWF0b3IgIFR3ZWVu5Yqo55S75o6l5Y+j5ouT5bGVLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbi8qKlxuICog5YCN6YCf5pKt5pS+XG4gKiBAcGFyYW0gc3BlZWQg5YCN6YCfXG4gKiBAZXhhbXBsZSB0d2Vlbi5zcGVlZCgyKTtcbiAqL1xuY2MuVHdlZW4ucHJvdG90eXBlLnNwZWVkID0gZnVuY3Rpb24gKHNwZWVkKSB7XG4gICAgdGhpcy5fZmluYWxBY3Rpb24uX3NwZWVkTWV0aG9kID0gdHJ1ZTtcbiAgICB0aGlzLl9maW5hbEFjdGlvbi5fc3BlZWQgPSBzcGVlZDtcbn1cblxuLyoqXG4gKiDmmoLlgZxcbiAqIEBleGFtcGxlIHR3ZWVuLnBhdXNlKCk7XG4gKi9cbmNjLlR3ZWVuLnByb3RvdHlwZS5wYXVzZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLl9maW5hbEFjdGlvbi5wYXVzZWQgPSB0cnVlO1xufTtcblxuLyoqXG4qIOaBouWkjVxuKiBAZXhhbXBsZSB0d2Vlbi5yZXN1bWUoKTtcbiovXG5jYy5Ud2Vlbi5wcm90b3R5cGUucmVzdW1lID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX2ZpbmFsQWN0aW9uLnBhdXNlZCA9IGZhbHNlO1xufTtcblxuLyoqXG4gKiDojrflj5bmjIHnu63ml7bpl7RcbiAqIEBleGFtcGxlIGxldCBkdXJhdGlvbiA9IHR3ZWVuLmR1cmF0aW9uKCk7XG4gKi9cbmNjLlR3ZWVuLnByb3RvdHlwZS5kdXJhdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5fZmluYWxBY3Rpb24uX2R1cmF0aW9uO1xufVxuXG4vKipcbiog6I635Y+W5bey57uP6L+b6KGM55qE5pe26Ze0XG4qIEBleGFtcGxlIGxldCBlbGFwc2VkID0gdHdlZW4uZWxhcHNlZCgpO1xuKi9cbmNjLlR3ZWVuLnByb3RvdHlwZS5lbGFwc2VkID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9maW5hbEFjdGlvbi5fZWxhcHNlZDtcbn1cblxuLyoqXG4gKiDot7PovazliLDmjIflrprml7bpl7RcbiAqIEBwYXJhbSB0aW1lIOaXtumXtCjnp5IpXG4gKiBAZXhhbXBsZSB0d2Vlbi5nb3RvKDIpO1xuICovXG5jYy5Ud2Vlbi5wcm90b3R5cGUuZ290byA9IGZ1bmN0aW9uICh0aW1lKSB7XG4gICAgdGhpcy5fZmluYWxBY3Rpb24uX2dvdG8gPSB0cnVlO1xuICAgIHRoaXMuX2ZpbmFsQWN0aW9uLl9lbGFwc2VkID0gdGltZTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbmltcG9ydCBHYW1lTWdyIGZyb20gXCIuLi9NYW5hZ2VyL0dhbWVNZ3JcIjtcblxuZXhwb3J0IGNsYXNzIExvYWRQcm9ncmVzcyB7XG4gICAgcHVibGljIHVybDogc3RyaW5nO1xuICAgIHB1YmxpYyBjb21wbGV0ZWRDb3VudDogbnVtYmVyO1xuICAgIHB1YmxpYyB0b3RhbENvdW50OiBudW1iZXI7XG4gICAgcHVibGljIGl0ZW06IGFueTtcbiAgICBwdWJsaWMgY2I/OiBGdW5jdGlvbjtcbn1cblxuLyoqIOS4gOS6m2NvY29zIGFwaSDnmoTlsIHoo4UsIHByb21pc2Xlh73mlbDnu5/kuIDliqDkuIpzeW5j5ZCO57yAICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDb2Nvc0hlbHBlciB7XG5cbiAgICAvKiog5Yqg6L296L+b5bqmICovXG4gICAgcHVibGljIHN0YXRpYyBsb2FkUHJvZ3Jlc3MgPSBuZXcgTG9hZFByb2dyZXNzKCk7XG5cbiAgICAvKiog562J5b6F5pe26Ze0LCDnp5LkuLrljZXkvY0gKi9cbiAgICBwdWJsaWMgc3RhdGljIHNsZWVwU3luYyA9IGZ1bmN0aW9uIChkdXI6IG51bWJlcik6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY2MuQ2FudmFzLmluc3RhbmNlLnNjaGVkdWxlT25jZSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcbiAgICAgICAgICAgIH0sIGR1cik7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFxuICAgICAqIEBwYXJhbSB0YXJnZXQgXG4gICAgICogQHBhcmFtIHJlcGVhdCAtMe+8jOihqOekuuawuOS5heaJp+ihjFxuICAgICAqIEBwYXJhbSB0d2VlbnMgXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBydW5SZXBlYXRUd2VlblN5bmModGFyZ2V0OiBhbnksIHJlcGVhdDogbnVtYmVyLCAuLi50d2VlbnM6IGNjLlR3ZWVuW10pIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGxldCBzZWxmVHdlZW4gPSBjYy50d2Vlbih0YXJnZXQpO1xuICAgICAgICAgICAgZm9yIChjb25zdCB0bXBUd2VlbiBvZiB0d2VlbnMpIHtcbiAgICAgICAgICAgICAgICBzZWxmVHdlZW4gPSBzZWxmVHdlZW4udGhlbih0bXBUd2Vlbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAocmVwZWF0IDwgMCkge1xuICAgICAgICAgICAgICAgIGNjLnR3ZWVuKHRhcmdldCkucmVwZWF0Rm9yZXZlcihzZWxmVHdlZW4pLnN0YXJ0KCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGNjLnR3ZWVuKHRhcmdldCkucmVwZWF0KHJlcGVhdCwgc2VsZlR3ZWVuKS5jYWxsKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcbiAgICAgICAgICAgICAgICB9KS5zdGFydCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG4gICAgLyoqIOWQjOatpeeahHR3ZWVuICovXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBydW5Ud2VlblN5bmModGFyZ2V0OiBhbnksIC4uLnR3ZWVuczogY2MuVHdlZW5bXSk6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgbGV0IHNlbGZUd2VlbiA9IGNjLnR3ZWVuKHRhcmdldCk7XG4gICAgICAgICAgICBmb3IgKGNvbnN0IHRtcFR3ZWVuIG9mIHR3ZWVucykge1xuICAgICAgICAgICAgICAgIHNlbGZUd2VlbiA9IHNlbGZUd2Vlbi50aGVuKHRtcFR3ZWVuKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNlbGZUd2Vlbi5jYWxsKCgpID0+IHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICB9KS5zdGFydCgpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICAgLyoqIOWBnOatonR3ZWVuICovXG4gICAgcHVibGljIHN0YXRpYyBzdG9wVHdlZW4odGFyZ2V0OiBhbnkpIHtcbiAgICAgICAgY2MuVHdlZW4uc3RvcEFsbEJ5VGFyZ2V0KHRhcmdldCk7XG4gICAgfVxuICAgIHB1YmxpYyBzdG9wVHdlZW5CeVRhZyh0YWc6IG51bWJlcikge1xuICAgICAgICBjYy5Ud2Vlbi5zdG9wQWxsQnlUYWcodGFnKTtcbiAgICB9XG5cbiAgICAvKiog5ZCM5q2l55qE5Yqo55S7ICovXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBydW5BbmltU3luYyhub2RlOiBjYy5Ob2RlLCBhbmltTmFtZT86IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBsZXQgYW5pbSA9IG5vZGUuZ2V0Q29tcG9uZW50KGNjLkFuaW1hdGlvbik7XG4gICAgICAgIGlmICghYW5pbSkgcmV0dXJuO1xuICAgICAgICBsZXQgY2xpcDogY2MuQW5pbWF0aW9uQ2xpcCA9IG51bGw7XG4gICAgICAgIGlmICghYW5pbU5hbWUpIGNsaXAgPSBhbmltLmRlZmF1bHRDbGlwO1xuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGxldCBjbGlwcyA9IGFuaW0uZ2V0Q2xpcHMoKTtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgKGFuaW1OYW1lKSA9PT0gXCJudW1iZXJcIikge1xuICAgICAgICAgICAgICAgIGNsaXAgPSBjbGlwc1thbmltTmFtZV07XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiAoYW5pbU5hbWUpID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjbGlwcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBpZiAoY2xpcHNbaV0ubmFtZSA9PT0gYW5pbU5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsaXAgPSBjbGlwc1tpXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmICghY2xpcCkgcmV0dXJuO1xuICAgICAgICBhd2FpdCBDb2Nvc0hlbHBlci5zbGVlcFN5bmMoY2xpcC5kdXJhdGlvbik7XG4gICAgfVxuXG4gICAgLyoqIOWKoOi9vei1hOa6kOW8guW4uOaXtuaKm+WHuumUmeivryAqL1xuICAgIHB1YmxpYyBzdGF0aWMgbG9hZFJlc1Rocm93RXJyb3JTeW5jPFQ+KHVybDogc3RyaW5nLCB0eXBlOiB0eXBlb2YgY2MuQXNzZXQsIG9uUHJvZ3Jlc3M/OiAoY29tcGxldGVkQ291bnQ6IG51bWJlciwgdG90YWxDb3VudDogbnVtYmVyLCBpdGVtOiBhbnkpID0+IHZvaWQpOiBQcm9taXNlPFQ+IHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgX2xvYWRpbmdNYXA6IHsgW2tleTogc3RyaW5nXTogRnVuY3Rpb25bXSB9ID0ge307XG4gICAgcHVibGljIHN0YXRpYyBsb2FkUmVzPFQ+KHVybDogc3RyaW5nLCB0eXBlOiB0eXBlb2YgY2MuQXNzZXQsIGNhbGxiYWNrOiBGdW5jdGlvbikge1xuICAgICAgICBpZiAodGhpcy5fbG9hZGluZ01hcFt1cmxdKSB7XG4gICAgICAgICAgICB0aGlzLl9sb2FkaW5nTWFwW3VybF0ucHVzaChjYWxsYmFjayk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5fbG9hZGluZ01hcFt1cmxdID0gW2NhbGxiYWNrXTtcbiAgICAgICAgdGhpcy5sb2FkUmVzU3luYzxUPih1cmwsIHR5cGUpLnRoZW4oKGRhdGE6IGFueSkgPT4ge1xuICAgICAgICAgICAgbGV0IGFyciA9IHRoaXMuX2xvYWRpbmdNYXBbdXJsXTtcbiAgICAgICAgICAgIGZvciAoY29uc3QgZnVuYyBvZiBhcnIpIHtcbiAgICAgICAgICAgICAgICBmdW5jKGRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5fbG9hZGluZ01hcFt1cmxdID0gbnVsbDtcbiAgICAgICAgICAgIGRlbGV0ZSB0aGlzLl9sb2FkaW5nTWFwW3VybF07XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICAvKiog5Yqg6L296LWE5rqQICovXG4gICAgcHVibGljIHN0YXRpYyBsb2FkUmVzU3luYzxUPih1cmw6IHN0cmluZywgdHlwZTogdHlwZW9mIGNjLkFzc2V0LCBvblByb2dyZXNzPzogKGNvbXBsZXRlZENvdW50OiBudW1iZXIsIHRvdGFsQ291bnQ6IG51bWJlciwgaXRlbTogYW55KSA9PiB2b2lkKTogUHJvbWlzZTxUPiB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBpZiAoIW9uUHJvZ3Jlc3MpIG9uUHJvZ3Jlc3MgPSB0aGlzLl9vblByb2dyZXNzO1xuICAgICAgICAgICAgY2MucmVzb3VyY2VzLmxvYWQodXJsLCB0eXBlLCBvblByb2dyZXNzLCAoZXJyLCBhc3NldDogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICBjYy5lcnJvcihgJHt1cmx9IFvotYTmupDliqDovb1dIOmUmeivryAke2Vycn1gKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShudWxsKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGFzc2V0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIC8qKiBcbiAgICAgKiDliqDovb3ov5vluqZcbiAgICAgKiBjYuaWueazlSDlhbblrp7nm67nmoTmmK/lj6/ku6XlsIZsb2FkZXLmlrnms5XnmoRwcm9ncmVzc1xuICAgICAqL1xuICAgIHByaXZhdGUgc3RhdGljIF9vblByb2dyZXNzKGNvbXBsZXRlZENvdW50OiBudW1iZXIsIHRvdGFsQ291bnQ6IG51bWJlciwgaXRlbTogYW55KSB7XG4gICAgICAgIENvY29zSGVscGVyLmxvYWRQcm9ncmVzcy5jb21wbGV0ZWRDb3VudCA9IGNvbXBsZXRlZENvdW50O1xuICAgICAgICBDb2Nvc0hlbHBlci5sb2FkUHJvZ3Jlc3MudG90YWxDb3VudCA9IHRvdGFsQ291bnQ7XG4gICAgICAgIENvY29zSGVscGVyLmxvYWRQcm9ncmVzcy5pdGVtID0gaXRlbTtcbiAgICAgICAgQ29jb3NIZWxwZXIubG9hZFByb2dyZXNzLmNiICYmIENvY29zSGVscGVyLmxvYWRQcm9ncmVzcy5jYihjb21wbGV0ZWRDb3VudCwgdG90YWxDb3VudCwgaXRlbSk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIOWvu+aJvuWtkOiKgueCuVxuICAgICAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZmluZENoaWxkSW5Ob2RlKG5vZGVOYW1lOiBzdHJpbmcsIHJvb3ROb2RlOiBjYy5Ob2RlKTogY2MuTm9kZSB7XG4gICAgICAgIGlmIChyb290Tm9kZS5uYW1lID09IG5vZGVOYW1lKSB7XG4gICAgICAgICAgICByZXR1cm4gcm9vdE5vZGU7XG4gICAgICAgIH1cbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCByb290Tm9kZS5jaGlsZHJlbkNvdW50OyBpKyspIHtcbiAgICAgICAgICAgIGxldCBub2RlID0gdGhpcy5maW5kQ2hpbGRJbk5vZGUobm9kZU5hbWUsIHJvb3ROb2RlLmNoaWxkcmVuW2ldKTtcbiAgICAgICAgICAgIGlmIChub2RlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5vZGU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgLyoqIOiOt+W+l0NvbXBvbmVudOeahOexu+WQjSAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0Q29tcG9uZW50TmFtZShjb206IEZ1bmN0aW9uKSB7XG4gICAgICAgIGxldCBhcnIgPSBjb20ubmFtZS5tYXRjaCgvPC4qPiQvKTtcbiAgICAgICAgaWYgKGFyciAmJiBhcnIubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgcmV0dXJuIGFyclswXS5zbGljZSgxLCAtMSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNvbS5uYW1lO1xuICAgIH1cbiAgICAvKiog5Yqg6L29YnVuZGxlICovXG4gICAgcHVibGljIHN0YXRpYyBsb2FkQnVuZGxlU3luYyh1cmw6IHN0cmluZywgb3B0aW9uczogYW55KTogUHJvbWlzZTxjYy5Bc3NldE1hbmFnZXIuQnVuZGxlPiB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjYy5hc3NldE1hbmFnZXIubG9hZEJ1bmRsZSh1cmwsIG9wdGlvbnMsIChlcnI6IEVycm9yLCBidW5kbGU6IGNjLkFzc2V0TWFuYWdlci5CdW5kbGUpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoIWVycikge1xuICAgICAgICAgICAgICAgICAgICBjYy5lcnJvcihg5Yqg6L29YnVuZGxl5aSx6LSlLCB1cmw6ICR7dXJsfSwgZXJyOiR7ZXJyfWApO1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG51bGwpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoYnVuZGxlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqIOi3r+W+hOaYr+ebuOWvueWIhuWMheaWh+S7tuWkuei3r+W+hOeahOebuOWvuei3r+W+hCAqL1xuICAgIHB1YmxpYyBzdGF0aWMgbG9hZEFzc2V0RnJvbUJ1bmRsZVN5bmMoYnVuZGxlTmFtZTogc3RyaW5nLCB1cmw6IHN0cmluZyB8IHN0cmluZ1tdKSB7XG4gICAgICAgIGxldCBidW5kbGUgPSBjYy5hc3NldE1hbmFnZXIuZ2V0QnVuZGxlKGJ1bmRsZU5hbWUpO1xuICAgICAgICBpZiAoIWJ1bmRsZSkge1xuICAgICAgICAgICAgY2MuZXJyb3IoYOWKoOi9vWJ1bmRsZeS4reeahOi1hOa6kOWksei0pSwg5pyq5om+5YiwYnVuZGxlLCBidW5kbGVVcmw6JHtidW5kbGVOYW1lfWApO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGJ1bmRsZS5sb2FkKHVybCwgKGVyciwgYXNzZXQ6IGNjLkFzc2V0IHwgY2MuQXNzZXRbXSkgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgY2MuZXJyb3IoYOWKoOi9vWJ1bmRsZeS4reeahOi1hOa6kOWksei0pSwg5pyq5om+5YiwYXNzZXQsIHVybDoke3VybH0sIGVycjoke2Vycn1gKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShudWxsKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGFzc2V0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqIOmAmui/h+i3r+W+hOWKoOi9vei1hOa6kCwg5aaC5p6c6L+Z5Liq6LWE5rqQ5ZyoYnVuZGxl5YaFLCDkvJrlhYjliqDovb1idW5kbGUsIOWcqOino+W8gGJ1bmRsZeiOt+W+l+WvueW6lOeahOi1hOa6kCAqL1xuICAgIHB1YmxpYyBzdGF0aWMgbG9hZEFzc2V0U3luYyh1cmw6IHN0cmluZyB8IHN0cmluZ1tdKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjYy5yZXNvdXJjZXMubG9hZCh1cmwsIChlcnIsIGFzc2V0czogY2MuQXNzZXQgfCBjYy5Bc3NldFtdKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKCFlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgY2MuZXJyb3IoYOWKoOi9vWFzc2V05aSx6LSlLCB1cmw6JHt1cmx9LCBlcnI6ICR7ZXJyfWApO1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG51bGwpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkUmVmKGFzc2V0cyk7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoYXNzZXRzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIC8qKiDph4rmlL7otYTmupAgKi9cbiAgICBwdWJsaWMgc3RhdGljIHJlbGVhc2VBc3NldChhc3NldHM6IGNjLkFzc2V0IHwgY2MuQXNzZXRbXSkge1xuICAgICAgICB0aGlzLmRlY1Jlcyhhc3NldHMpO1xuICAgIH1cbiAgICAvKiog5aKe5Yqg5byV55So6K6h5pWwICovXG4gICAgcHJpdmF0ZSBzdGF0aWMgYWRkUmVmKGFzc2V0czogY2MuQXNzZXQgfCBjYy5Bc3NldFtdKSB7XG4gICAgICAgIGlmIChhc3NldHMgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgICAgICAgZm9yIChjb25zdCBhIG9mIGFzc2V0cykge1xuICAgICAgICAgICAgICAgIGEuYWRkUmVmKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhc3NldHMuYWRkUmVmKCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLyoqIOWHj+WwkeW8leeUqOiuoeaVsCwg5b2T5byV55So6K6h5pWw5YeP5bCR5YiwMOaXtizkvJroh6rliqjplIDmr4EgKi9cbiAgICBwcml2YXRlIHN0YXRpYyBkZWNSZXMoYXNzZXRzOiBjYy5Bc3NldCB8IGNjLkFzc2V0W10pIHtcbiAgICAgICAgaWYgKGFzc2V0cyBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICAgICAgICBmb3IgKGNvbnN0IGEgb2YgYXNzZXRzKSB7XG4gICAgICAgICAgICAgICAgYS5kZWNSZWYoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFzc2V0cy5kZWNSZWYoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKiDmiKrlm74gKi9cbiAgICBwdWJsaWMgc3RhdGljIGNhcHR1cmVTY3JlZW4oY2FtZXJhOiBjYy5DYW1lcmEsIHByb3A/OiBjYy5Ob2RlIHwgY2MuUmVjdCkge1xuICAgICAgICBsZXQgbmV3VGV4dHVyZSA9IG5ldyBjYy5SZW5kZXJUZXh0dXJlKCk7XG4gICAgICAgIC8vIGxldCBvbGRUZXh0dXJlID0gY2FtZXJhLnRhcmdldFRleHR1cmU7XG4gICAgICAgIGxldCByZWN0OiBjYy5SZWN0ID0gY2MucmVjdCgwLCAwLCBjYy52aXNpYmxlUmVjdC53aWR0aCwgY2MudmlzaWJsZVJlY3QuaGVpZ2h0KTtcbiAgICAgICAgaWYgKHByb3ApIHtcbiAgICAgICAgICAgIGlmIChwcm9wIGluc3RhbmNlb2YgY2MuTm9kZSkge1xuICAgICAgICAgICAgICAgIHJlY3QgPSBwcm9wLmdldEJvdW5kaW5nQm94VG9Xb3JsZCgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZWN0ID0gcHJvcDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBuZXdUZXh0dXJlLmluaXRXaXRoU2l6ZShjYy52aXNpYmxlUmVjdC53aWR0aCwgY2MudmlzaWJsZVJlY3QuaGVpZ2h0LCBjYy5nYW1lLl9yZW5kZXJDb250ZXh0LlNURU5DSUxfSU5ERVg4KTtcbiAgICAgICAgY2FtZXJhLnRhcmdldFRleHR1cmUgPSBuZXdUZXh0dXJlO1xuICAgICAgICBjYW1lcmEucmVuZGVyKCk7XG4gICAgICAgIC8vIGNhbWVyYS50YXJnZXRUZXh0dXJlID0gb2xkVGV4dHVyZTtcblxuICAgICAgICBsZXQgYnVmZmVyID0gbmV3IEFycmF5QnVmZmVyKHJlY3Qud2lkdGggKiByZWN0LmhlaWdodCAqIDQpO1xuICAgICAgICBsZXQgZGF0YSA9IG5ldyBVaW50OEFycmF5KGJ1ZmZlcik7XG4gICAgICAgIG5ld1RleHR1cmUucmVhZFBpeGVscyhkYXRhLCByZWN0LngsIHJlY3QueSwgcmVjdC53aWR0aCwgcmVjdC5oZWlnaHQpO1xuICAgICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG5cbiAgICAvKiog5Yqg6L29anNvbui1hOa6kCAqL1xuICAgIHB1YmxpYyBzdGF0aWMgbG9hZEpzb25TeW5jPFQ+KHVybDogc3RyaW5nLCB0eXBlOiB0eXBlb2YgY2MuSnNvbkFzc2V0LCk6IFByb21pc2U8VD4ge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY2MucmVzb3VyY2VzLmxvYWQodXJsLCB0eXBlLCAoZXJyLCBhc3NldDogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICBjYy5lcnJvcihgJHt1cmx9IFvotYTmupDliqDovb1dIOmUmeivryAke2Vycn1gKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShudWxsKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGFzc2V0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqIOiOt+W+l+WNlemAiemhuee0ouW8lSAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0Q29udGVuZXJpbmRleChub2RlOiBjYy5Ob2RlKSB7XG4gICAgICAgIGlmICghbm9kZSB8fCBub2RlLmNoaWxkcmVuQ291bnQgPCAxKSByZXR1cm47XG5cbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBub2RlLmNoaWxkcmVuQ291bnQ7IGkrKykge1xuICAgICAgICAgICAgbGV0IGl0ZW0gPSBub2RlLmNoaWxkcmVuW2ldO1xuICAgICAgICAgICAgbGV0IGlzQ2hlY2tlZCA9IGl0ZW0uZ2V0Q29tcG9uZW50KGNjLlRvZ2dsZSkuaXNDaGVja2VkO1xuICAgICAgICAgICAgaWYgKGlzQ2hlY2tlZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKiog5Yqg6L295aS05YOPICovXG4gICAgcHVibGljIHN0YXRpYyBsb2FkSGVhZCh1cmw6IHN0cmluZywgaGVhZDogY2MuU3ByaXRlKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjYy5hc3NldE1hbmFnZXIubG9hZFJlbW90ZSh1cmwsIChlcnIsIHJlczogY2MuVGV4dHVyZTJEKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImhlYWRFcnI6XCIgKyBlcnIpO1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG51bGwpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSkudGhlbigocmVzOiBjYy5UZXh0dXJlMkQpID0+IHtcbiAgICAgICAgICAgIGhlYWQuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSBuZXcgY2MuU3ByaXRlRnJhbWUocmVzKTtcbiAgICAgICAgfSlcblxuICAgIH1cblxuICAgIC8qKiDorr7nva7ljZXkuKrnlJ/kuqfnianlk4HnmoTlm77niYcgKi9cbiAgICBwdWJsaWMgc3RhdGljIHNldEl0ZW1Qcm9kdWNlSWNvbihpZDogbnVtYmVyLCBpY29uOiBjYy5TcHJpdGUsIGNhbGxiYWNrOiBGdW5jdGlvbikge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY2MucmVzb3VyY2VzLmxvYWQoXCJpbWdzL2l0ZW1Qcm9kdWNlL2l0ZW1fXCIgKyBpZCwgKGVyciwgcmVzOiBjYy5UZXh0dXJlMkQpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUobnVsbCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShyZXMpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSkudGhlbigocmVzOiBjYy5UZXh0dXJlMkQpID0+IHtcbiAgICAgICAgICAgIGljb24uZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSBuZXcgY2MuU3ByaXRlRnJhbWUocmVzKTtcbiAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgLyoqIOagueaNrueJqeWTgUlE77yM5ou/5Yiw5b2T5YmN54mp5ZOB5L+h5oGvICovXG4gICAgcHVibGljIHN0YXRpYyBnZXRJdGVtUHJvZHVjZUluZm9CeUlkKGlkOiBudW1iZXIpIHtcbiAgICAgICAgbGV0IHByb2R1Y2UgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5JdGVtUHJvZnVjZXNKc29uLml0ZW1zSW5mby5qc29uXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvZHVjZS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgbGV0IGl0ZW0gPSBwcm9kdWNlW2ldO1xuICAgICAgICAgICAgaWYgKGl0ZW0uaWQgPT0gaWQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcHJvZHVjZVtpXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKiDmoLnmja7noo7niYflkI3lrZfvvIzmi7/liLDlvZPliY1JRCAqL1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0Q2hpcEluZm9CeU5hbWUoY2hpcG5hbWU6IHN0cmluZykge1xuICAgICAgICBsZXQgY2hpcHMgPSBHYW1lTWdyLmRhdGFNb2RhbE1nci5DaGlwc0luZm9Kc29uLmNoaXBJbmZvXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2hpcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChjaGlwc1tpXS5uYW1lID09IGNoaXBuYW1lKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNoaXBzW2ldW1wiY2hpcElkXCJdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqIOagueaNrueijueJh0lE77yM5ou/5Yiw5b2T5YmN5ZCN5a2XICovXG4gICAgcHVibGljIHN0YXRpYyBnZXRDaGlwTmFtZUJ5SUQoY2hpcGlkOiBzdHJpbmcpIHtcbiAgICAgICAgbGV0IGNoaXBzID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuQ2hpcHNJbmZvSnNvbi5jaGlwSW5mbztcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjaGlwcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGNoaXBzW2ldLmNoaXBJZCA9PSBjaGlwaWQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gY2hpcHNbaV1bXCJuYW1lXCJdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqIOagueaNrueijueJh0lE77yM5ou/5Yiw5b2T5YmN5ZCN5a2XICovXG4gICAgcHVibGljIHN0YXRpYyBnZXRTaG9wTmFtZUJ5SUQoY2hpcGlkOiBzdHJpbmcpIHtcbiAgICAgICAgbGV0IHNob3BzID0gR2FtZU1nci5kYXRhTW9kYWxNZ3IuU2hvcEluZm9Kc29uLnNob3BJbmZvO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHNob3BzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoc2hvcHNbaV0uaWQgPT0gY2hpcGlkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHNob3BzW2ldW1wibmFtZVwiXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKiDmoLnmja7noo7niYdJRO+8jOaLv+WIsOW9k+WJjeWQjeWtlyAqL1xuICAgIHB1YmxpYyBzdGF0aWMgc2V0U2hvcEluZm8obmFtZTogc3RyaW5nLCBpY29uOiBjYy5TcHJpdGUpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwiaW1ncy9zaG9wcy9cIiArIG5hbWUsIChlcnIsIHJlczogY2MuVGV4dHVyZTJEKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG51bGwpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICB9KS50aGVuKChyZXM6IGNjLlRleHR1cmUyRCkgPT4ge1xuICAgICAgICAgICAgaWNvbi5nZXRDb21wb25lbnQoY2MuU3ByaXRlKS5zcHJpdGVGcmFtZSA9IG5ldyBjYy5TcHJpdGVGcmFtZShyZXMpO1xuICAgICAgICB9KVxuICAgIH1cblxuICAgIC8qKiDorr7nva7ljZXkuKrnlJ/kuqfnianlk4HnmoTlm77niYcgKi9cbiAgICBwdWJsaWMgc3RhdGljIHNldExvdHRlcnlJY29uKG5hbWU6IHN0cmluZywgaWNvbjogY2MuU3ByaXRlKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjYy5yZXNvdXJjZXMubG9hZChcImltZ3MvaXRlbVByb2R1Y2UvaXRlbV9cIiArIG5hbWUsIChlcnIsIHJlczogY2MuVGV4dHVyZTJEKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKGVycikge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG51bGwpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUocmVzKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pLnRoZW4oKHJlczogY2MuVGV4dHVyZTJEKSA9PiB7XG4gICAgICAgICAgICBpY29uLmdldENvbXBvbmVudChjYy5TcHJpdGUpLnNwcml0ZUZyYW1lID0gbmV3IGNjLlNwcml0ZUZyYW1lKHJlcyk7XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgLyoqIOiuvue9rueJqeWTgeeahOWbvueJhyAqL1xuICAgIHB1YmxpYyBzdGF0aWMgc2V0RGVhbEljb24obmFtZTogc3RyaW5nLCBpY29uOiBjYy5TcHJpdGUpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGNjLnJlc291cmNlcy5sb2FkKFwiaW1ncy9jaGlwL1wiICsgbmFtZSwgKGVyciwgcmVzOiBjYy5UZXh0dXJlMkQpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUobnVsbCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShyZXMpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSkudGhlbigocmVzOiBjYy5UZXh0dXJlMkQpID0+IHtcbiAgICAgICAgICAgIGljb24uZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSBuZXcgY2MuU3ByaXRlRnJhbWUocmVzKTtcbiAgICAgICAgfSlcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDmoLnmja7liankvZnnp5LmlbDmoLzlvI/ljJbliankvZnml7bpl7Qg6L+U5ZueIEhIOk1NOlNTXG4gICAgICogQHBhcmFtIHtOdW1iZXJ9IGxlZnRTZWMgXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBmb3JtYXRUaW1lRm9yU2Vjb25kKGxlZnRTZWM6IG51bWJlcikge1xuICAgICAgICB2YXIgdGltZVN0ciA9ICcnO1xuICAgICAgICB2YXIgc2VjID0gbGVmdFNlYyAlIDYwO1xuXG4gICAgICAgIHZhciBsZWZ0TWluID0gTWF0aC5mbG9vcihsZWZ0U2VjIC8gNjApO1xuICAgICAgICBsZWZ0TWluID0gbGVmdE1pbiA8IDAgPyAwIDogbGVmdE1pbjtcblxuICAgICAgICB2YXIgaG91ciA9IE1hdGguZmxvb3IobGVmdE1pbiAvIDYwKTtcbiAgICAgICAgdmFyIG1pbiA9IGxlZnRNaW4gJSA2MDtcblxuICAgICAgICBpZiAoaG91ciA+IDApIHtcbiAgICAgICAgICAgIHRpbWVTdHIgKz0gaG91ciA+IDkgPyBob3VyLnRvU3RyaW5nKCkgOiAnMCcgKyBob3VyO1xuICAgICAgICAgICAgdGltZVN0ciArPSAnOic7XG4gICAgICAgIH1cblxuICAgICAgICB0aW1lU3RyICs9IG1pbiA+IDkgPyBtaW4udG9TdHJpbmcoKSA6ICcwJyArIG1pbjtcbiAgICAgICAgdGltZVN0ciArPSAnOic7XG4gICAgICAgIHRpbWVTdHIgKz0gc2VjID4gOSA/IHNlYy50b1N0cmluZygpIDogJzAnICsgc2VjO1xuICAgICAgICByZXR1cm4gdGltZVN0cjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDml7bpl7TmiLPovazljJZcbiAgICAgKiBAcGFyYW0gdmFsdWUgXG4gICAgICogQHJldHVybnMgXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBjaGFuZ2VUaW1lKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgbGV0IGRhdGUgPSBuZXcgRGF0ZSh2YWx1ZSk7XG4gICAgICAgIGxldCBZWSA9IGRhdGUuZ2V0RnVsbFllYXIoKTtcbiAgICAgICAgbGV0IE1NID0gZGF0ZS5nZXRNb250aCgpICsgMSA8IDEwID8gXCIwXCIgKyAoZGF0ZS5nZXRNb250aCgpICsgMSkgOiBkYXRlLmdldE1vbnRoKCkgKyAxO1xuICAgICAgICBsZXQgREQgPSBkYXRlLmdldERhdGUoKSA8IDEwID8gXCIwXCIgKyBkYXRlLmdldERhdGUoKSA6IGRhdGUuZ2V0RGF0ZSgpO1xuICAgICAgICBsZXQgaGggPSBkYXRlLmdldEhvdXJzKCkgPCAxMCA/IFwiMFwiICsgZGF0ZS5nZXRIb3VycygpIDogZGF0ZS5nZXRIb3VycygpO1xuICAgICAgICBsZXQgbW0gPSBkYXRlLmdldE1pbnV0ZXMoKSA8IDEwID8gXCIwXCIgKyBkYXRlLmdldE1pbnV0ZXMoKSA6IGRhdGUuZ2V0TWludXRlcygpO1xuICAgICAgICBsZXQgc3MgPSBkYXRlLmdldFNlY29uZHMoKSA8IDEwID8gXCIwXCIgKyBkYXRlLmdldFNlY29uZHMoKSA6IGRhdGUuZ2V0U2Vjb25kcygpO1xuXG4gICAgICAgIC8vIOi/memHjOS/ruaUuei/lOWbnuaXtumXtOeahOagvOW8j1xuICAgICAgICAvLyByZXR1cm4gWVkgKyBcIi1cIiArIE1NICsgXCItXCIgKyBERCArIFwiIFwiICsgaGggKyBcIjpcIiArIG1tICsgXCI6XCIgKyBzcztcbiAgICAgICAgcmV0dXJuIFlZICsgXCItXCIgKyBNTSArIFwiLVwiICsgRERcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDlpKfmlbDovazmjaJcbiAgICAgKi9cbiAgICAvLyAgICAgYmlnTnVtQ29udmVyKG51bTogbnVtYmVyKSB7XG4gICAgLy8gICAgICAgICAxMDAwPTFLXG4gICAgLy8gMTAwMEs9MU1cbiAgICAvLyAxMDAwTT0xQlxuICAgIC8vIDEwMDBCPTFUXG4gICAgLy8gMTAwMFQ9MVBcbiAgICAvLyAxMDAwUD0xRVxuICAgIC8vIDEwMDBFPTFaXG4gICAgLy8gMTAwMFo9MVlcbiAgICAvLyAxMDAwWT0xU1xuICAgIC8vIDEwMDBTPTFMXG4gICAgLy8gMTAwMEw9MVhcbiAgICAvLyAxMDAwWD0xRFxuICAgIC8vICAgICB9XG59XG5cbiJdfQ==