
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Guide/UIGaide.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4d9093UAbJHWYQvaRKxoPg1', 'UIGaide');
// Script/Guide/UIGaide.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventCenter_1 = require("../Net/EventCenter");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIGaide = /** @class */ (function (_super) {
    __extends(UIGaide, _super);
    function UIGaide() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.maskNode = null;
        return _this;
    }
    UIGaide.prototype.start = function () {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchBlockEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchBlockEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchBlockEvent, this);
        EventCenter_1.EventCenter.on("shouguide", this.Show, this);
        this.node.active = false;
    };
    UIGaide.prototype.Show = function (node) {
        //得到当前节点大小
        var contentSize = node.getContentSize();
        this.maskNode.width = contentSize.width + 20;
        this.maskNode.height = contentSize.height + 20;
        //转换世界坐标，再转换到引导节点的本地坐标
        var pos = node.parent.convertToWorldSpaceAR(node.position);
        var localpos = this.maskNode.parent.convertToNodeSpaceAR(pos);
        this.maskNode.setPosition(cc.v2(localpos.x, localpos.y));
        this.node.active = true;
    };
    UIGaide.prototype.onTouchBlockEvent = function (event) {
        //setSwallowTouches：true 不向下触摸
        var pt = this.maskNode.convertToNodeSpaceAR(event.getLocation());
        var rect = cc.rect(0, 0, this.maskNode.width, this.maskNode.height);
        //如果没有命中目标节点，则吞噬向下触摸时间，命中则隐藏引导节点
        if (!rect.contains(pt)) {
            console.log("没有命中");
            this.node._touchListener.setSwallowTouches(true);
            event.stopPropagationImmediate();
            return;
        }
        else {
            console.log("命中");
            this.node._touchListener.setSwallowTouches(false);
        }
        this.node.active = false;
    };
    __decorate([
        property(cc.Node)
    ], UIGaide.prototype, "maskNode", void 0);
    UIGaide = __decorate([
        ccclass
    ], UIGaide);
    return UIGaide;
}(cc.Component));
exports.default = UIGaide;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvR3VpZGUvVUlHYWlkZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxrREFBaUQ7QUFFM0MsSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBcUMsMkJBQVk7SUFBakQ7UUFBQSxxRUE2Q0M7UUExQ0csY0FBUSxHQUFZLElBQUksQ0FBQzs7SUEwQzdCLENBQUM7SUF4Q0csdUJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hFLHlCQUFXLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRU0sc0JBQUksR0FBWCxVQUFZLElBQWE7UUFDckIsVUFBVTtRQUNWLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUUvQyxzQkFBc0I7UUFDdEIsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDM0QsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXpELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUM1QixDQUFDO0lBRUQsbUNBQWlCLEdBQWpCLFVBQWtCLEtBQTBCO1FBQ3hDLDhCQUE4QjtRQUM5QixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQ2pFLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BFLGdDQUFnQztRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2pELEtBQUssQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1lBQ2pDLE9BQU87U0FDVjthQUFNO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUNqQixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNyRDtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBeENEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ087SUFIUixPQUFPO1FBRDNCLE9BQU87T0FDYSxPQUFPLENBNkMzQjtJQUFELGNBQUM7Q0E3Q0QsQUE2Q0MsQ0E3Q29DLEVBQUUsQ0FBQyxTQUFTLEdBNkNoRDtrQkE3Q29CLE9BQU8iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCB7IEV2ZW50Q2VudGVyIH0gZnJvbSBcIi4uL05ldC9FdmVudENlbnRlclwiO1xuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlHYWlkZSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBtYXNrTm9kZTogY2MuTm9kZSA9IG51bGw7XG5cbiAgICBzdGFydCgpIHtcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX1NUQVJULCB0aGlzLm9uVG91Y2hCbG9ja0V2ZW50LCB0aGlzKTtcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX01PVkUsIHRoaXMub25Ub3VjaEJsb2NrRXZlbnQsIHRoaXMpO1xuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfRU5ELCB0aGlzLm9uVG91Y2hCbG9ja0V2ZW50LCB0aGlzKTtcbiAgICAgICAgRXZlbnRDZW50ZXIub24oXCJzaG91Z3VpZGVcIiwgdGhpcy5TaG93LCB0aGlzKTtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIHB1YmxpYyBTaG93KG5vZGU6IGNjLk5vZGUpIHtcbiAgICAgICAgLy/lvpfliLDlvZPliY3oioLngrnlpKflsI9cbiAgICAgICAgbGV0IGNvbnRlbnRTaXplID0gbm9kZS5nZXRDb250ZW50U2l6ZSgpO1xuICAgICAgICB0aGlzLm1hc2tOb2RlLndpZHRoID0gY29udGVudFNpemUud2lkdGggKyAyMDtcbiAgICAgICAgdGhpcy5tYXNrTm9kZS5oZWlnaHQgPSBjb250ZW50U2l6ZS5oZWlnaHQgKyAyMDtcblxuICAgICAgICAvL+i9rOaNouS4lueVjOWdkOagh++8jOWGjei9rOaNouWIsOW8leWvvOiKgueCueeahOacrOWcsOWdkOagh1xuICAgICAgICBsZXQgcG9zID0gbm9kZS5wYXJlbnQuY29udmVydFRvV29ybGRTcGFjZUFSKG5vZGUucG9zaXRpb24pO1xuICAgICAgICBsZXQgbG9jYWxwb3MgPSB0aGlzLm1hc2tOb2RlLnBhcmVudC5jb252ZXJ0VG9Ob2RlU3BhY2VBUihwb3MpO1xuICAgICAgICB0aGlzLm1hc2tOb2RlLnNldFBvc2l0aW9uKGNjLnYyKGxvY2FscG9zLngsIGxvY2FscG9zLnkpKTtcblxuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBvblRvdWNoQmxvY2tFdmVudChldmVudDogY2MuRXZlbnQuRXZlbnRUb3VjaCkge1xuICAgICAgICAvL3NldFN3YWxsb3dUb3VjaGVz77yadHJ1ZSDkuI3lkJHkuIvop6bmkbhcbiAgICAgICAgbGV0IHB0ID0gdGhpcy5tYXNrTm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihldmVudC5nZXRMb2NhdGlvbigpKTtcbiAgICAgICAgbGV0IHJlY3QgPSBjYy5yZWN0KDAsIDAsIHRoaXMubWFza05vZGUud2lkdGgsIHRoaXMubWFza05vZGUuaGVpZ2h0KTtcbiAgICAgICAgLy/lpoLmnpzmsqHmnInlkb3kuK3nm67moIfoioLngrnvvIzliJnlkJ7lmazlkJHkuIvop6bmkbjml7bpl7TvvIzlkb3kuK3liJnpmpDol4/lvJXlr7zoioLngrlcbiAgICAgICAgaWYgKCFyZWN0LmNvbnRhaW5zKHB0KSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCLmsqHmnInlkb3kuK1cIilcbiAgICAgICAgICAgIHRoaXMubm9kZS5fdG91Y2hMaXN0ZW5lci5zZXRTd2FsbG93VG91Y2hlcyh0cnVlKTtcbiAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbkltbWVkaWF0ZSgpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCLlkb3kuK1cIilcbiAgICAgICAgICAgIHRoaXMubm9kZS5fdG91Y2hMaXN0ZW5lci5zZXRTd2FsbG93VG91Y2hlcyhmYWxzZSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgfVxuXG59XG4iXX0=