
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Common/Pool.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '284d8zxqTdJtYjT+CGS5XDE', 'Pool');
// Script/Common/Pool.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pool = void 0;
var Pool = /** @class */ (function () {
    function Pool(fn, size) {
        this._fn = fn;
        this._idx = size - 1;
        this._frees = new Array(size);
        for (var i = 0; i < size; i++) {
            this._frees[i] = fn();
        }
    }
    Object.defineProperty(Pool.prototype, "freeCount", {
        get: function () {
            return this._frees.length;
        },
        enumerable: false,
        configurable: true
    });
    Pool.prototype.alloc = function () {
        if (this._idx < 0) {
            this._expand(Math.round(this._frees.length * 1.2) + 1);
        }
        var obj = this._frees[this._idx];
        this._frees.splice(this._idx);
        --this._idx;
        obj.use && obj.use();
        return obj;
    };
    Pool.prototype.free = function (obj) {
        ++this._idx;
        obj.free && obj.free();
        this._frees[this._idx] = obj;
    };
    Pool.prototype.clear = function (fn) {
        for (var i = 0; i < this._idx; i++) {
            fn && fn(this._frees[i]);
        }
        this._frees.splice(0);
        this._idx = -1;
    };
    Pool.prototype._expand = function (size) {
        var old = this._frees;
        this._frees = new Array(size);
        var len = size - old.length;
        for (var i = 0; i < len; i++) {
            this._frees[i] = this._fn();
        }
        for (var i = len, j = 0; i < size; ++i, ++j) {
            this._frees[i] = old[j];
        }
        this._idx += len;
    };
    return Pool;
}());
exports.Pool = Pool;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQ29tbW9uL1Bvb2wudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSUE7SUFTSSxjQUFZLEVBQVcsRUFBRSxJQUFZO1FBQ2pDLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxLQUFLLENBQUksSUFBSSxDQUFDLENBQUM7UUFFakMsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQztJQVpELHNCQUFXLDJCQUFTO2FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM5QixDQUFDOzs7T0FBQTtJQVlNLG9CQUFLLEdBQVo7UUFDSSxJQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQzFEO1FBQ0QsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQztRQUVaLEdBQUcsQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVNLG1CQUFJLEdBQVgsVUFBWSxHQUFNO1FBQ2QsRUFBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ2IsR0FBRyxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQ2pDLENBQUM7SUFFTSxvQkFBSyxHQUFaLFVBQWEsRUFBb0I7UUFDN0IsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDM0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ25CLENBQUM7SUFHTyxzQkFBTyxHQUFmLFVBQWdCLElBQVk7UUFDeEIsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN4QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTlCLElBQU0sR0FBRyxHQUFHLElBQUksR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO1FBQzlCLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDL0I7UUFFRCxLQUFJLElBQUksQ0FBQyxHQUFDLEdBQUcsRUFBQyxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUU7WUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDM0I7UUFFRCxJQUFJLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQztJQUNyQixDQUFDO0lBQ0wsV0FBQztBQUFELENBN0RBLEFBNkRDLElBQUE7QUE3RFksb0JBQUkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElQb29sIHtcbiAgICB1c2U/KCk6IGFueTtcbiAgICBmcmVlPygpOiBhbnk7XG59XG5leHBvcnQgY2xhc3MgUG9vbDxUIGV4dGVuZHMgSVBvb2w+IHtcbiAgICBwcml2YXRlIF9mbjogKCkgPT4gVDtcbiAgICBwcml2YXRlIF9pZHg6IG51bWJlcjtcbiAgICBwcml2YXRlIF9mcmVlczogVFtdO1xuXG4gICAgcHVibGljIGdldCBmcmVlQ291bnQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9mcmVlcy5sZW5ndGg7XG4gICAgfVxuXG4gICAgY29uc3RydWN0b3IoZm46ICgpID0+IFQsIHNpemU6IG51bWJlcikge1xuICAgICAgICB0aGlzLl9mbiA9IGZuO1xuICAgICAgICB0aGlzLl9pZHggPSBzaXplIC0gMTtcbiAgICAgICAgdGhpcy5fZnJlZXMgPSBuZXcgQXJyYXk8VD4oc2l6ZSk7XG5cbiAgICAgICAgZm9yKGxldCBpPTA7IGk8c2l6ZTsgaSsrKSB7XG4gICAgICAgICAgICB0aGlzLl9mcmVlc1tpXSA9IGZuKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgYWxsb2MoKTogVCB7XG4gICAgICAgIGlmKHRoaXMuX2lkeCA8IDApIHtcbiAgICAgICAgICAgIHRoaXMuX2V4cGFuZChNYXRoLnJvdW5kKHRoaXMuX2ZyZWVzLmxlbmd0aCAqIDEuMikgKyAxKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBvYmogPSB0aGlzLl9mcmVlc1t0aGlzLl9pZHhdO1xuICAgICAgICB0aGlzLl9mcmVlcy5zcGxpY2UodGhpcy5faWR4KTtcbiAgICAgICAgLS10aGlzLl9pZHg7XG5cbiAgICAgICAgb2JqLnVzZSAmJiBvYmoudXNlKCk7XG4gICAgICAgIHJldHVybiBvYmo7XG4gICAgfVxuXG4gICAgcHVibGljIGZyZWUob2JqOiBUKSB7XG4gICAgICAgICsrIHRoaXMuX2lkeDtcbiAgICAgICAgb2JqLmZyZWUgJiYgb2JqLmZyZWUoKTtcbiAgICAgICAgdGhpcy5fZnJlZXNbdGhpcy5faWR4XSA9IG9iajtcbiAgICB9XG5cbiAgICBwdWJsaWMgY2xlYXIoZm46IChvYmo6IFQpID0+IHZvaWQpIHtcbiAgICAgICAgZm9yKGxldCBpPTA7IGk8dGhpcy5faWR4OyBpKyspIHtcbiAgICAgICAgICAgIGZuICYmIGZuKHRoaXMuX2ZyZWVzW2ldKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9mcmVlcy5zcGxpY2UoMCk7XG4gICAgICAgIHRoaXMuX2lkeCA9IC0xO1xuICAgIH1cblxuXG4gICAgcHJpdmF0ZSBfZXhwYW5kKHNpemU6IG51bWJlcikge1xuICAgICAgICBjb25zdCBvbGQgPSB0aGlzLl9mcmVlcztcbiAgICAgICAgdGhpcy5fZnJlZXMgPSBuZXcgQXJyYXkoc2l6ZSk7XG5cbiAgICAgICAgY29uc3QgbGVuID0gc2l6ZSAtIG9sZC5sZW5ndGg7XG4gICAgICAgIGZvcihsZXQgaT0wOyBpPGxlbjsgaSsrKSB7XG4gICAgICAgICAgICB0aGlzLl9mcmVlc1tpXSA9IHRoaXMuX2ZuKCk7XG4gICAgICAgIH1cblxuICAgICAgICBmb3IobGV0IGk9bGVuLGo9MDsgaTxzaXplOyArK2ksICsraikge1xuICAgICAgICAgICAgdGhpcy5fZnJlZXNbaV0gPSBvbGRbal07XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl9pZHggKz0gbGVuO1xuICAgIH1cbn0iXX0=