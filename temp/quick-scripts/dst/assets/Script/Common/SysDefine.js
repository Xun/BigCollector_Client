
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Common/SysDefine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7f8ffNRvZxCz4zNi1L6dBHr', 'SysDefine');
// Script/Common/SysDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SysDefine = exports.UIState = exports.ModalOpacity = exports.FormType = void 0;
var UIConfig_1 = require("../UIConfig");
/**窗体类型 */
var FormType;
(function (FormType) {
    /** 屏幕 */
    FormType["Screen"] = "UIScreen";
    /** 固定窗口 */
    FormType["Fixed"] = "UIFixed";
    /** 弹出窗口 */
    FormType["Window"] = "UIWindow";
    /** 独立窗口 */
    FormType["Tips"] = "UITips";
})(FormType = exports.FormType || (exports.FormType = {}));
/**透明度类型 */
var ModalOpacity;
(function (ModalOpacity) {
    /** 没有mask, 可以穿透 */
    ModalOpacity[ModalOpacity["None"] = 0] = "None";
    /** 完全透明，不能穿透 */
    ModalOpacity[ModalOpacity["OpacityZero"] = 1] = "OpacityZero";
    /** 高透明度，不能穿透 */
    ModalOpacity[ModalOpacity["OpacityLow"] = 2] = "OpacityLow";
    /** 半透明，不能穿透 */
    ModalOpacity[ModalOpacity["OpacityHalf"] = 3] = "OpacityHalf";
    /** 低透明度, 不能穿透 */
    ModalOpacity[ModalOpacity["OpacityHigh"] = 4] = "OpacityHigh";
    /** 完全不透明 */
    ModalOpacity[ModalOpacity["OpacityFull"] = 5] = "OpacityFull";
})(ModalOpacity = exports.ModalOpacity || (exports.ModalOpacity = {}));
/** UI的状态 */
var UIState;
(function (UIState) {
    UIState[UIState["None"] = 0] = "None";
    UIState[UIState["Loading"] = 1] = "Loading";
    UIState[UIState["Showing"] = 2] = "Showing";
    UIState[UIState["Hiding"] = 3] = "Hiding";
})(UIState = exports.UIState || (exports.UIState = {}));
/** 常量 */
var SysDefine = /** @class */ (function () {
    function SysDefine() {
    }
    SysDefine.defaultLoadingForm = UIConfig_1.default.UILoading;
    /* 路径常量 */
    SysDefine.SYS_PATH_CANVAS = "Canvas";
    SysDefine.SYS_PATH_UIFORMS_CONFIG_INFO = "UIFormsConfigInfo";
    SysDefine.SYS_PATH_CONFIG_INFO = "SysConfigInfo";
    /* 标签常量 */
    SysDefine.SYS_UIROOT_NAME = "Canvas/Scene/UIROOT";
    SysDefine.SYS_UIMODAL_NAME = "Canvas/Scene/UIROOT/UIModalManager";
    SysDefine.SYS_UIAdapter_NAME = "Canvas/Scene/UIROOT/UIAdapterManager";
    /* 节点常量 */
    SysDefine.SYS_SCENE_NODE = "Scene";
    SysDefine.SYS_UIROOT_NODE = "UIROOT";
    SysDefine.SYS_SCREEN_NODE = "Screen";
    SysDefine.SYS_FIXED_NODE = "FixedUI";
    SysDefine.SYS_POPUP_NODE = "PopUp";
    SysDefine.SYS_TOPTIPS_NODE = "TopTips";
    /** 规范符号 */
    SysDefine.SYS_STANDARD_Prefix = '_';
    SysDefine.SYS_STANDARD_Separator = '$';
    SysDefine.SYS_STANDARD_End = '#';
    SysDefine.UI_PATH_ROOT = 'UIForms/';
    SysDefine.SeparatorMap = {
        "_Node": "cc.Node",
        "_Label": "cc.Label",
        "_Button": "cc.Button",
        "_Sprite": "cc.Sprite",
        "_RichText": "cc.RichText",
        "_Mask": "cc.Mask",
        "_MotionStreak": "cc.MotionStreak",
        "_TiledMap": "cc.TiledMap",
        "_TiledTile": "cc.TiledTile",
        "_Spine": "sp.Skeleton",
        "_Graphics": "cc.Graphics",
        "_Animation": "cc.Animation",
        "_WebView": "cc.WebView",
        "_EditBox": "cc.EditBox",
        "_ScrollView": "cc.ScrollView",
        "_VideoPlayer": "cc.VideoPlayer",
        "_ProgressBar": "cc.ProgressBar",
        "_PageView": "cc.PageView",
        "_Slider": "cc.Slider",
        "_Toggle": "cc.Toggle",
        "_ButtonPlus": "ButtonPlus",
    };
    return SysDefine;
}());
exports.SysDefine = SysDefine;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQ29tbW9uL1N5c0RlZmluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3Q0FBbUM7QUFHbkMsVUFBVTtBQUNWLElBQVksUUFTWDtBQVRELFdBQVksUUFBUTtJQUNoQixTQUFTO0lBQ1QsK0JBQW1CLENBQUE7SUFDbkIsV0FBVztJQUNYLDZCQUFpQixDQUFBO0lBQ2pCLFdBQVc7SUFDWCwrQkFBbUIsQ0FBQTtJQUNuQixXQUFXO0lBQ1gsMkJBQWUsQ0FBQTtBQUNuQixDQUFDLEVBVFcsUUFBUSxHQUFSLGdCQUFRLEtBQVIsZ0JBQVEsUUFTbkI7QUFDRCxXQUFXO0FBQ1gsSUFBWSxZQWFYO0FBYkQsV0FBWSxZQUFZO0lBQ3BCLG1CQUFtQjtJQUNuQiwrQ0FBSSxDQUFBO0lBQ0osZ0JBQWdCO0lBQ2hCLDZEQUFXLENBQUE7SUFDWCxnQkFBZ0I7SUFDaEIsMkRBQVUsQ0FBQTtJQUNWLGVBQWU7SUFDZiw2REFBVyxDQUFBO0lBQ1gsaUJBQWlCO0lBQ2pCLDZEQUFXLENBQUE7SUFDWCxZQUFZO0lBQ1osNkRBQVcsQ0FBQTtBQUNmLENBQUMsRUFiVyxZQUFZLEdBQVosb0JBQVksS0FBWixvQkFBWSxRQWF2QjtBQUNELFlBQVk7QUFDWixJQUFZLE9BS1g7QUFMRCxXQUFZLE9BQU87SUFDZixxQ0FBUSxDQUFBO0lBQ1IsMkNBQVcsQ0FBQTtJQUNYLDJDQUFXLENBQUE7SUFDWCx5Q0FBVSxDQUFBO0FBQ2QsQ0FBQyxFQUxXLE9BQU8sR0FBUCxlQUFPLEtBQVAsZUFBTyxRQUtsQjtBQUNELFNBQVM7QUFDVDtJQUFBO0lBZ0RBLENBQUM7SUEvQ2lCLDRCQUFrQixHQUFnQixrQkFBUSxDQUFDLFNBQVMsQ0FBQztJQUNuRSxVQUFVO0lBQ0kseUJBQWUsR0FBRyxRQUFRLENBQUM7SUFDM0Isc0NBQTRCLEdBQUcsbUJBQW1CLENBQUM7SUFDbkQsOEJBQW9CLEdBQUcsZUFBZSxDQUFDO0lBQ3JELFVBQVU7SUFDSSx5QkFBZSxHQUFHLHFCQUFxQixDQUFDO0lBQ3hDLDBCQUFnQixHQUFHLG9DQUFvQyxDQUFDO0lBQ3hELDRCQUFrQixHQUFHLHNDQUFzQyxDQUFDO0lBQzFFLFVBQVU7SUFDSSx3QkFBYyxHQUFHLE9BQU8sQ0FBQTtJQUN4Qix5QkFBZSxHQUFHLFFBQVEsQ0FBQztJQUMzQix5QkFBZSxHQUFHLFFBQVEsQ0FBQztJQUMzQix3QkFBYyxHQUFHLFNBQVMsQ0FBQztJQUMzQix3QkFBYyxHQUFHLE9BQU8sQ0FBQztJQUN6QiwwQkFBZ0IsR0FBRyxTQUFTLENBQUM7SUFDM0MsV0FBVztJQUNHLDZCQUFtQixHQUFHLEdBQUcsQ0FBQztJQUMxQixnQ0FBc0IsR0FBRyxHQUFHLENBQUM7SUFDN0IsMEJBQWdCLEdBQUcsR0FBRyxDQUFDO0lBRXZCLHNCQUFZLEdBQUcsVUFBVSxDQUFDO0lBRTFCLHNCQUFZLEdBQThCO1FBQ3BELE9BQU8sRUFBRSxTQUFTO1FBQ2xCLFFBQVEsRUFBRSxVQUFVO1FBQ3BCLFNBQVMsRUFBRSxXQUFXO1FBQ3RCLFNBQVMsRUFBRSxXQUFXO1FBQ3RCLFdBQVcsRUFBRSxhQUFhO1FBQzFCLE9BQU8sRUFBRSxTQUFTO1FBQ2xCLGVBQWUsRUFBRSxpQkFBaUI7UUFDbEMsV0FBVyxFQUFFLGFBQWE7UUFDMUIsWUFBWSxFQUFFLGNBQWM7UUFDNUIsUUFBUSxFQUFFLGFBQWE7UUFDdkIsV0FBVyxFQUFFLGFBQWE7UUFDMUIsWUFBWSxFQUFFLGNBQWM7UUFDNUIsVUFBVSxFQUFFLFlBQVk7UUFDeEIsVUFBVSxFQUFFLFlBQVk7UUFDeEIsYUFBYSxFQUFFLGVBQWU7UUFDOUIsY0FBYyxFQUFFLGdCQUFnQjtRQUNoQyxjQUFjLEVBQUUsZ0JBQWdCO1FBQ2hDLFdBQVcsRUFBRSxhQUFhO1FBQzFCLFNBQVMsRUFBRSxXQUFXO1FBQ3RCLFNBQVMsRUFBRSxXQUFXO1FBQ3RCLGFBQWEsRUFBRSxZQUFZO0tBQzlCLENBQUM7SUFFTixnQkFBQztDQWhERCxBQWdEQyxJQUFBO0FBaERZLDhCQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFVJQ29uZmlnIGZyb20gXCIuLi9VSUNvbmZpZ1wiO1xuaW1wb3J0IHsgSUZvcm1Db25maWcgfSBmcm9tIFwiLi9TdHJ1Y3RcIjtcblxuLyoq56qX5L2T57G75Z6LICovXG5leHBvcnQgZW51bSBGb3JtVHlwZSB7XG4gICAgLyoqIOWxj+W5lSAqL1xuICAgIFNjcmVlbiA9IFwiVUlTY3JlZW5cIixcbiAgICAvKiog5Zu65a6a56qX5Y+jICovXG4gICAgRml4ZWQgPSBcIlVJRml4ZWRcIixcbiAgICAvKiog5by55Ye656qX5Y+jICovXG4gICAgV2luZG93ID0gXCJVSVdpbmRvd1wiLFxuICAgIC8qKiDni6znq4vnqpflj6MgKi9cbiAgICBUaXBzID0gXCJVSVRpcHNcIixcbn1cbi8qKumAj+aYjuW6puexu+WeiyAqL1xuZXhwb3J0IGVudW0gTW9kYWxPcGFjaXR5IHtcbiAgICAvKiog5rKh5pyJbWFzaywg5Y+v5Lul56m/6YCPICovXG4gICAgTm9uZSxcbiAgICAvKiog5a6M5YWo6YCP5piO77yM5LiN6IO956m/6YCPICovXG4gICAgT3BhY2l0eVplcm8sXG4gICAgLyoqIOmrmOmAj+aYjuW6pu+8jOS4jeiDveepv+mAjyAqL1xuICAgIE9wYWNpdHlMb3csXG4gICAgLyoqIOWNiumAj+aYju+8jOS4jeiDveepv+mAjyAqL1xuICAgIE9wYWNpdHlIYWxmLFxuICAgIC8qKiDkvY7pgI/mmI7luqYsIOS4jeiDveepv+mAjyAqL1xuICAgIE9wYWNpdHlIaWdoLFxuICAgIC8qKiDlrozlhajkuI3pgI/mmI4gKi9cbiAgICBPcGFjaXR5RnVsbCxcbn1cbi8qKiBVSeeahOeKtuaAgSAqL1xuZXhwb3J0IGVudW0gVUlTdGF0ZSB7XG4gICAgTm9uZSA9IDAsXG4gICAgTG9hZGluZyA9IDEsXG4gICAgU2hvd2luZyA9IDIsXG4gICAgSGlkaW5nID0gM1xufVxuLyoqIOW4uOmHjyAqL1xuZXhwb3J0IGNsYXNzIFN5c0RlZmluZSB7XG4gICAgcHVibGljIHN0YXRpYyBkZWZhdWx0TG9hZGluZ0Zvcm06IElGb3JtQ29uZmlnID0gVUlDb25maWcuVUlMb2FkaW5nO1xuICAgIC8qIOi3r+W+hOW4uOmHjyAqL1xuICAgIHB1YmxpYyBzdGF0aWMgU1lTX1BBVEhfQ0FOVkFTID0gXCJDYW52YXNcIjtcbiAgICBwdWJsaWMgc3RhdGljIFNZU19QQVRIX1VJRk9STVNfQ09ORklHX0lORk8gPSBcIlVJRm9ybXNDb25maWdJbmZvXCI7XG4gICAgcHVibGljIHN0YXRpYyBTWVNfUEFUSF9DT05GSUdfSU5GTyA9IFwiU3lzQ29uZmlnSW5mb1wiO1xuICAgIC8qIOagh+etvuW4uOmHjyAqL1xuICAgIHB1YmxpYyBzdGF0aWMgU1lTX1VJUk9PVF9OQU1FID0gXCJDYW52YXMvU2NlbmUvVUlST09UXCI7XG4gICAgcHVibGljIHN0YXRpYyBTWVNfVUlNT0RBTF9OQU1FID0gXCJDYW52YXMvU2NlbmUvVUlST09UL1VJTW9kYWxNYW5hZ2VyXCI7XG4gICAgcHVibGljIHN0YXRpYyBTWVNfVUlBZGFwdGVyX05BTUUgPSBcIkNhbnZhcy9TY2VuZS9VSVJPT1QvVUlBZGFwdGVyTWFuYWdlclwiO1xuICAgIC8qIOiKgueCueW4uOmHjyAqL1xuICAgIHB1YmxpYyBzdGF0aWMgU1lTX1NDRU5FX05PREUgPSBcIlNjZW5lXCJcbiAgICBwdWJsaWMgc3RhdGljIFNZU19VSVJPT1RfTk9ERSA9IFwiVUlST09UXCI7XG4gICAgcHVibGljIHN0YXRpYyBTWVNfU0NSRUVOX05PREUgPSBcIlNjcmVlblwiO1xuICAgIHB1YmxpYyBzdGF0aWMgU1lTX0ZJWEVEX05PREUgPSBcIkZpeGVkVUlcIjtcbiAgICBwdWJsaWMgc3RhdGljIFNZU19QT1BVUF9OT0RFID0gXCJQb3BVcFwiO1xuICAgIHB1YmxpYyBzdGF0aWMgU1lTX1RPUFRJUFNfTk9ERSA9IFwiVG9wVGlwc1wiO1xuICAgIC8qKiDop4TojIPnrKblj7cgKi9cbiAgICBwdWJsaWMgc3RhdGljIFNZU19TVEFOREFSRF9QcmVmaXggPSAnXyc7XG4gICAgcHVibGljIHN0YXRpYyBTWVNfU1RBTkRBUkRfU2VwYXJhdG9yID0gJyQnO1xuICAgIHB1YmxpYyBzdGF0aWMgU1lTX1NUQU5EQVJEX0VuZCA9ICcjJztcblxuICAgIHB1YmxpYyBzdGF0aWMgVUlfUEFUSF9ST09UID0gJ1VJRm9ybXMvJztcblxuICAgIHB1YmxpYyBzdGF0aWMgU2VwYXJhdG9yTWFwOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9ID0ge1xuICAgICAgICBcIl9Ob2RlXCI6IFwiY2MuTm9kZVwiLFxuICAgICAgICBcIl9MYWJlbFwiOiBcImNjLkxhYmVsXCIsXG4gICAgICAgIFwiX0J1dHRvblwiOiBcImNjLkJ1dHRvblwiLFxuICAgICAgICBcIl9TcHJpdGVcIjogXCJjYy5TcHJpdGVcIixcbiAgICAgICAgXCJfUmljaFRleHRcIjogXCJjYy5SaWNoVGV4dFwiLFxuICAgICAgICBcIl9NYXNrXCI6IFwiY2MuTWFza1wiLFxuICAgICAgICBcIl9Nb3Rpb25TdHJlYWtcIjogXCJjYy5Nb3Rpb25TdHJlYWtcIixcbiAgICAgICAgXCJfVGlsZWRNYXBcIjogXCJjYy5UaWxlZE1hcFwiLFxuICAgICAgICBcIl9UaWxlZFRpbGVcIjogXCJjYy5UaWxlZFRpbGVcIixcbiAgICAgICAgXCJfU3BpbmVcIjogXCJzcC5Ta2VsZXRvblwiLFxuICAgICAgICBcIl9HcmFwaGljc1wiOiBcImNjLkdyYXBoaWNzXCIsXG4gICAgICAgIFwiX0FuaW1hdGlvblwiOiBcImNjLkFuaW1hdGlvblwiLFxuICAgICAgICBcIl9XZWJWaWV3XCI6IFwiY2MuV2ViVmlld1wiLFxuICAgICAgICBcIl9FZGl0Qm94XCI6IFwiY2MuRWRpdEJveFwiLFxuICAgICAgICBcIl9TY3JvbGxWaWV3XCI6IFwiY2MuU2Nyb2xsVmlld1wiLFxuICAgICAgICBcIl9WaWRlb1BsYXllclwiOiBcImNjLlZpZGVvUGxheWVyXCIsXG4gICAgICAgIFwiX1Byb2dyZXNzQmFyXCI6IFwiY2MuUHJvZ3Jlc3NCYXJcIixcbiAgICAgICAgXCJfUGFnZVZpZXdcIjogXCJjYy5QYWdlVmlld1wiLFxuICAgICAgICBcIl9TbGlkZXJcIjogXCJjYy5TbGlkZXJcIixcbiAgICAgICAgXCJfVG9nZ2xlXCI6IFwiY2MuVG9nZ2xlXCIsXG4gICAgICAgIFwiX0J1dHRvblBsdXNcIjogXCJCdXR0b25QbHVzXCIsXG4gICAgfTtcblxufSJdfQ==