
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Common/Components/ButtonPlus.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3eaf8iLxgtDEKVcEmFvrbqy', 'ButtonPlus');
// Script/Common/Components/ButtonPlus.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SoundMgr_1 = require("../../Manager/SoundMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode, menu = _a.menu, help = _a.help, inspector = _a.inspector;
var ButtonPlus = /** @class */ (function (_super) {
    __extends(ButtonPlus, _super);
    function ButtonPlus() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.audioUrl = '';
        _this.openContinuous = true;
        _this.continuousTime = 1;
        // false表示可以点击
        _this.continuous = false;
        // 定时器
        _this._continuousTimer = null;
        // 长按触发
        _this.openLongPress = false;
        // 触发时间
        _this.longPressTime = 1;
        _this.longPressFlag = false;
        _this.longPressTimer = null;
        return _this;
    }
    ButtonPlus.prototype.onEnable = function () {
        this.continuous = false;
        _super.prototype.onEnable.call(this);
        if (!CC_EDITOR) {
        }
    };
    ButtonPlus.prototype.onDisable = function () {
        if (this._continuousTimer) {
            clearTimeout(this._continuousTimer);
            this._continuousTimer = null;
        }
        if (this.longPressTimer) {
            clearTimeout(this.longPressTimer);
            this.longPressTimer = null;
        }
        _super.prototype.onDisable.call(this);
    };
    /** 重写 */
    ButtonPlus.prototype._onTouchBegan = function (event) {
        if (!this.interactable || !this.enabledInHierarchy)
            return;
        if (this.openLongPress && !this.longPressFlag) { // 开启长按
            if (this.longPressTimer)
                clearTimeout(this.longPressTimer);
            this.longPressTimer = setTimeout(function () {
                // 还在触摸中 触发事件
                if (this["_pressed"]) {
                    this.node.emit('longclickStart', this);
                    this.longPressFlag = true;
                }
            }.bind(this), this.longPressTime * 1000);
        }
        this["_pressed"] = true;
        this["_updateState"]();
        event.stopPropagation();
    };
    ButtonPlus.prototype._onTouchEnded = function (event) {
        if (!this.interactable || !this.enabledInHierarchy)
            return;
        if (this["_pressed"] && this.longPressFlag) {
            this.node.emit('longclickEnd', this);
            this.longPressFlag = false;
        }
        else if (this["_pressed"] && !this.continuous) {
            this.continuous = this.openContinuous ? true : false;
            cc.Component.EventHandler.emitEvents(this.clickEvents, event);
            this.node.emit('click', event);
            SoundMgr_1.default.inst.playEffect(this.audioUrl);
            if (this.openContinuous) {
                this._continuousTimer = setTimeout(function () {
                    this.continuous = false;
                }.bind(this), this.continuousTime * 1000);
            }
        }
        this["_pressed"] = false;
        this["_updateState"]();
        event.stopPropagation();
    };
    ButtonPlus.prototype._onTouchCancel = function () {
        if (!this.interactable || !this.enabledInHierarchy)
            return;
        if (this["_pressed"] && this.longPressFlag) {
            this.node.emit('longclickEnd', this);
            this.longPressFlag = false;
        }
        this["_pressed"] = false;
        this["_updateState"]();
    };
    /** 添加点击事件 */
    ButtonPlus.prototype.addClick = function (callback, target) {
        this.node.off('click');
        this.node.on('click', callback, target);
    };
    /** 添加一个长按事件 */
    ButtonPlus.prototype.addLongClick = function (startFunc, endFunc, target) {
        this.node.off('longclickStart');
        this.node.off('longclickEnd');
        this.node.on('longclickStart', startFunc, target);
        this.node.on('longclickEnd', endFunc, target);
    };
    __decorate([
        property({ tooltip: "音效路径", type: '', multiline: true, formerlySerializedAs: '_N$string' })
    ], ButtonPlus.prototype, "audioUrl", void 0);
    __decorate([
        property({ tooltip: "屏蔽连续点击" })
    ], ButtonPlus.prototype, "openContinuous", void 0);
    __decorate([
        property({ tooltip: "屏蔽时间, 单位:秒" })
    ], ButtonPlus.prototype, "continuousTime", void 0);
    __decorate([
        property({ tooltip: "是否开启长按事件" })
    ], ButtonPlus.prototype, "openLongPress", void 0);
    __decorate([
        property({ tooltip: "长按时间" })
    ], ButtonPlus.prototype, "longPressTime", void 0);
    ButtonPlus = __decorate([
        ccclass,
        menu('i18n:MAIN_MENU.component.ui/ButtonPlus'),
        executeInEditMode,
        help('i18n:COMPONENT.help_url.button'),
        inspector('packages://buttonplus/inspector.js')
    ], ButtonPlus);
    return ButtonPlus;
}(cc.Button));
exports.default = ButtonPlus;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtREFBOEM7QUFHeEMsSUFBQSxLQUFrRSxFQUFFLENBQUMsVUFBVSxFQUE3RSxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxpQkFBaUIsdUJBQUEsRUFBRSxJQUFJLFVBQUEsRUFBRSxJQUFJLFVBQUEsRUFBRSxTQUFTLGVBQWtCLENBQUM7QUFNdEY7SUFBd0MsOEJBQVM7SUFBakQ7UUFBQSxxRUF1R0M7UUFwR0csY0FBUSxHQUFHLEVBQUUsQ0FBQztRQUVkLG9CQUFjLEdBQUcsSUFBSSxDQUFDO1FBRXRCLG9CQUFjLEdBQUcsQ0FBQyxDQUFDO1FBRW5CLGNBQWM7UUFDZCxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixNQUFNO1FBQ04sc0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBR3hCLE9BQU87UUFFUCxtQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixPQUFPO1FBRVAsbUJBQWEsR0FBRyxDQUFDLENBQUM7UUFDbEIsbUJBQWEsR0FBRyxLQUFLLENBQUM7UUFFZCxvQkFBYyxHQUFHLElBQUksQ0FBQzs7SUFnRmxDLENBQUM7SUE5RUcsNkJBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLGlCQUFNLFFBQVEsV0FBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxTQUFTLEVBQUU7U0FDZjtJQUNMLENBQUM7SUFDRCw4QkFBUyxHQUFUO1FBQ0ksSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkIsWUFBWSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDaEM7UUFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDckIsWUFBWSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM5QjtRQUNELGlCQUFNLFNBQVMsV0FBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCxTQUFTO0lBQ1Qsa0NBQWEsR0FBYixVQUFjLEtBQUs7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0I7WUFBRSxPQUFPO1FBRTNELElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBSyxPQUFPO1lBQ3ZELElBQUksSUFBSSxDQUFDLGNBQWM7Z0JBQUUsWUFBWSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUMzRCxJQUFJLENBQUMsY0FBYyxHQUFHLFVBQVUsQ0FBQztnQkFDN0IsYUFBYTtnQkFDYixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTtvQkFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ3ZDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2lCQUM3QjtZQUNMLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsQ0FBQztTQUM1QztRQUVELElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7UUFDdkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFDRCxrQ0FBYSxHQUFiLFVBQWMsS0FBSztRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQjtZQUFFLE9BQU87UUFDM0QsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7U0FDOUI7YUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDN0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNyRCxFQUFFLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM5RCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDL0Isa0JBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQTtZQUN2QyxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxVQUFVLENBQUM7b0JBQy9CLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO2dCQUM1QixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLENBQUM7YUFDN0M7U0FDSjtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7UUFDdkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFDRCxtQ0FBYyxHQUFkO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCO1lBQUUsT0FBTztRQUMzRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUNELGFBQWE7SUFDYiw2QkFBUSxHQUFSLFVBQVMsUUFBa0IsRUFBRSxNQUFjO1FBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUNELGVBQWU7SUFDZixpQ0FBWSxHQUFaLFVBQWEsU0FBbUIsRUFBRSxPQUFpQixFQUFFLE1BQWM7UUFDL0QsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBbkdEO1FBREMsUUFBUSxDQUFDLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsb0JBQW9CLEVBQUUsV0FBVyxFQUFFLENBQUM7Z0RBQzlFO0lBRWQ7UUFEQyxRQUFRLENBQUMsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLENBQUM7c0RBQ1Y7SUFFdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLENBQUM7c0RBQ2pCO0lBVW5CO1FBREMsUUFBUSxDQUFDLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDO3FEQUNaO0lBR3RCO1FBREMsUUFBUSxDQUFDLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxDQUFDO3FEQUNaO0lBcEJELFVBQVU7UUFMOUIsT0FBTztRQUNQLElBQUksQ0FBQyx3Q0FBd0MsQ0FBQztRQUM5QyxpQkFBaUI7UUFDakIsSUFBSSxDQUFDLGdDQUFnQyxDQUFDO1FBQ3RDLFNBQVMsQ0FBQyxvQ0FBb0MsQ0FBQztPQUMzQixVQUFVLENBdUc5QjtJQUFELGlCQUFDO0NBdkdELEFBdUdDLENBdkd1QyxFQUFFLENBQUMsTUFBTSxHQXVHaEQ7a0JBdkdvQixVQUFVIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFNvdW5kTWdyIGZyb20gXCIuLi8uLi9NYW5hZ2VyL1NvdW5kTWdyXCI7XG5cblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSwgZXhlY3V0ZUluRWRpdE1vZGUsIG1lbnUsIGhlbHAsIGluc3BlY3RvciB9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5AbWVudSgnaTE4bjpNQUlOX01FTlUuY29tcG9uZW50LnVpL0J1dHRvblBsdXMnKVxuQGV4ZWN1dGVJbkVkaXRNb2RlXG5AaGVscCgnaTE4bjpDT01QT05FTlQuaGVscF91cmwuYnV0dG9uJylcbkBpbnNwZWN0b3IoJ3BhY2thZ2VzOi8vYnV0dG9ucGx1cy9pbnNwZWN0b3IuanMnKVxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnV0dG9uUGx1cyBleHRlbmRzIGNjLkJ1dHRvbiB7XG5cbiAgICBAcHJvcGVydHkoeyB0b29sdGlwOiBcIumfs+aViOi3r+W+hFwiLCB0eXBlOiAnJywgbXVsdGlsaW5lOiB0cnVlLCBmb3JtZXJseVNlcmlhbGl6ZWRBczogJ19OJHN0cmluZycgfSlcbiAgICBhdWRpb1VybCA9ICcnO1xuICAgIEBwcm9wZXJ0eSh7IHRvb2x0aXA6IFwi5bGP6JS96L+e57ut54K55Ye7XCIgfSlcbiAgICBvcGVuQ29udGludW91cyA9IHRydWU7XG4gICAgQHByb3BlcnR5KHsgdG9vbHRpcDogXCLlsY/olL3ml7bpl7QsIOWNleS9jTrnp5JcIiB9KVxuICAgIGNvbnRpbnVvdXNUaW1lID0gMTtcblxuICAgIC8vIGZhbHNl6KGo56S65Y+v5Lul54K55Ye7XG4gICAgY29udGludW91czogYm9vbGVhbiA9IGZhbHNlO1xuICAgIC8vIOWumuaXtuWZqFxuICAgIF9jb250aW51b3VzVGltZXIgPSBudWxsO1xuXG5cbiAgICAvLyDplb/mjInop6blj5FcbiAgICBAcHJvcGVydHkoeyB0b29sdGlwOiBcIuaYr+WQpuW8gOWQr+mVv+aMieS6i+S7tlwiIH0pXG4gICAgb3BlbkxvbmdQcmVzcyA9IGZhbHNlO1xuICAgIC8vIOinpuWPkeaXtumXtFxuICAgIEBwcm9wZXJ0eSh7IHRvb2x0aXA6IFwi6ZW/5oyJ5pe26Ze0XCIgfSlcbiAgICBsb25nUHJlc3NUaW1lID0gMTtcbiAgICBsb25nUHJlc3NGbGFnID0gZmFsc2U7XG5cbiAgICBwcml2YXRlIGxvbmdQcmVzc1RpbWVyID0gbnVsbDtcblxuICAgIG9uRW5hYmxlKCkge1xuICAgICAgICB0aGlzLmNvbnRpbnVvdXMgPSBmYWxzZTtcbiAgICAgICAgc3VwZXIub25FbmFibGUoKTtcbiAgICAgICAgaWYgKCFDQ19FRElUT1IpIHtcbiAgICAgICAgfVxuICAgIH1cbiAgICBvbkRpc2FibGUoKSB7XG4gICAgICAgIGlmICh0aGlzLl9jb250aW51b3VzVGltZXIpIHtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLl9jb250aW51b3VzVGltZXIpO1xuICAgICAgICAgICAgdGhpcy5fY29udGludW91c1RpbWVyID0gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5sb25nUHJlc3NUaW1lcikge1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMubG9uZ1ByZXNzVGltZXIpO1xuICAgICAgICAgICAgdGhpcy5sb25nUHJlc3NUaW1lciA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgc3VwZXIub25EaXNhYmxlKCk7XG4gICAgfVxuXG4gICAgLyoqIOmHjeWGmSAqL1xuICAgIF9vblRvdWNoQmVnYW4oZXZlbnQpIHtcbiAgICAgICAgaWYgKCF0aGlzLmludGVyYWN0YWJsZSB8fCAhdGhpcy5lbmFibGVkSW5IaWVyYXJjaHkpIHJldHVybjtcblxuICAgICAgICBpZiAodGhpcy5vcGVuTG9uZ1ByZXNzICYmICF0aGlzLmxvbmdQcmVzc0ZsYWcpIHsgICAgLy8g5byA5ZCv6ZW/5oyJXG4gICAgICAgICAgICBpZiAodGhpcy5sb25nUHJlc3NUaW1lcikgY2xlYXJUaW1lb3V0KHRoaXMubG9uZ1ByZXNzVGltZXIpO1xuICAgICAgICAgICAgdGhpcy5sb25nUHJlc3NUaW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIC8vIOi/mOWcqOinpuaRuOS4rSDop6blj5Hkuovku7ZcbiAgICAgICAgICAgICAgICBpZiAodGhpc1tcIl9wcmVzc2VkXCJdKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZS5lbWl0KCdsb25nY2xpY2tTdGFydCcsIHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvbmdQcmVzc0ZsYWcgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgdGhpcy5sb25nUHJlc3NUaW1lICogMTAwMCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzW1wiX3ByZXNzZWRcIl0gPSB0cnVlO1xuICAgICAgICB0aGlzW1wiX3VwZGF0ZVN0YXRlXCJdKCk7XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgIH1cbiAgICBfb25Ub3VjaEVuZGVkKGV2ZW50KSB7XG4gICAgICAgIGlmICghdGhpcy5pbnRlcmFjdGFibGUgfHwgIXRoaXMuZW5hYmxlZEluSGllcmFyY2h5KSByZXR1cm47XG4gICAgICAgIGlmICh0aGlzW1wiX3ByZXNzZWRcIl0gJiYgdGhpcy5sb25nUHJlc3NGbGFnKSB7XG4gICAgICAgICAgICB0aGlzLm5vZGUuZW1pdCgnbG9uZ2NsaWNrRW5kJywgdGhpcyk7XG4gICAgICAgICAgICB0aGlzLmxvbmdQcmVzc0ZsYWcgPSBmYWxzZTtcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzW1wiX3ByZXNzZWRcIl0gJiYgIXRoaXMuY29udGludW91cykge1xuICAgICAgICAgICAgdGhpcy5jb250aW51b3VzID0gdGhpcy5vcGVuQ29udGludW91cyA/IHRydWUgOiBmYWxzZTtcbiAgICAgICAgICAgIGNjLkNvbXBvbmVudC5FdmVudEhhbmRsZXIuZW1pdEV2ZW50cyh0aGlzLmNsaWNrRXZlbnRzLCBldmVudCk7XG4gICAgICAgICAgICB0aGlzLm5vZGUuZW1pdCgnY2xpY2snLCBldmVudCk7XG4gICAgICAgICAgICBTb3VuZE1nci5pbnN0LnBsYXlFZmZlY3QodGhpcy5hdWRpb1VybClcbiAgICAgICAgICAgIGlmICh0aGlzLm9wZW5Db250aW51b3VzKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fY29udGludW91c1RpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29udGludW91cyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH0uYmluZCh0aGlzKSwgdGhpcy5jb250aW51b3VzVGltZSAqIDEwMDApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXNbXCJfcHJlc3NlZFwiXSA9IGZhbHNlO1xuICAgICAgICB0aGlzW1wiX3VwZGF0ZVN0YXRlXCJdKCk7XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgIH1cbiAgICBfb25Ub3VjaENhbmNlbCgpIHtcbiAgICAgICAgaWYgKCF0aGlzLmludGVyYWN0YWJsZSB8fCAhdGhpcy5lbmFibGVkSW5IaWVyYXJjaHkpIHJldHVybjtcbiAgICAgICAgaWYgKHRoaXNbXCJfcHJlc3NlZFwiXSAmJiB0aGlzLmxvbmdQcmVzc0ZsYWcpIHtcbiAgICAgICAgICAgIHRoaXMubm9kZS5lbWl0KCdsb25nY2xpY2tFbmQnLCB0aGlzKTtcbiAgICAgICAgICAgIHRoaXMubG9uZ1ByZXNzRmxhZyA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHRoaXNbXCJfcHJlc3NlZFwiXSA9IGZhbHNlO1xuICAgICAgICB0aGlzW1wiX3VwZGF0ZVN0YXRlXCJdKCk7XG4gICAgfVxuICAgIC8qKiDmt7vliqDngrnlh7vkuovku7YgKi9cbiAgICBhZGRDbGljayhjYWxsYmFjazogRnVuY3Rpb24sIHRhcmdldDogT2JqZWN0KSB7XG4gICAgICAgIHRoaXMubm9kZS5vZmYoJ2NsaWNrJyk7XG4gICAgICAgIHRoaXMubm9kZS5vbignY2xpY2snLCBjYWxsYmFjaywgdGFyZ2V0KTtcbiAgICB9XG4gICAgLyoqIOa3u+WKoOS4gOS4qumVv+aMieS6i+S7tiAqL1xuICAgIGFkZExvbmdDbGljayhzdGFydEZ1bmM6IEZ1bmN0aW9uLCBlbmRGdW5jOiBGdW5jdGlvbiwgdGFyZ2V0OiBPYmplY3QpIHtcbiAgICAgICAgdGhpcy5ub2RlLm9mZignbG9uZ2NsaWNrU3RhcnQnKTtcbiAgICAgICAgdGhpcy5ub2RlLm9mZignbG9uZ2NsaWNrRW5kJyk7XG4gICAgICAgIHRoaXMubm9kZS5vbignbG9uZ2NsaWNrU3RhcnQnLCBzdGFydEZ1bmMsIHRhcmdldCk7XG4gICAgICAgIHRoaXMubm9kZS5vbignbG9uZ2NsaWNrRW5kJywgZW5kRnVuYywgdGFyZ2V0KTtcbiAgICB9XG59Il19