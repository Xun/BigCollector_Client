
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Common/UIModalScript.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a6a5a+wskdJZJdS5tPl1qBP', 'UIModalScript');
// Script/Common/UIModalScript.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ContentAdapter_1 = require("../Adapter/ContentAdapter");
var UIManager_1 = require("../Manager/UIManager");
var WindowMgr_1 = require("../Manager/WindowMgr");
var CocosHelper_1 = require("../Utils/CocosHelper");
var SysDefine_1 = require("./SysDefine");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIModalScript = /** @class */ (function (_super) {
    __extends(UIModalScript, _super);
    function UIModalScript() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /** 代码创建一个单色texture */
        _this._texture = null;
        return _this;
    }
    UIModalScript.prototype.getSingleTexture = function () {
        if (this._texture)
            return this._texture;
        var data = new Uint8Array(2 * 2 * 4);
        for (var i = 0; i < 2; i++) {
            for (var j = 0; j < 2; j++) {
                data[i * 2 * 4 + j * 4 + 0] = 255;
                data[i * 2 * 4 + j * 4 + 1] = 255;
                data[i * 2 * 4 + j * 4 + 2] = 255;
                data[i * 2 * 4 + j * 4 + 3] = 255;
            }
        }
        var texture = new cc.Texture2D();
        texture.name = 'single color';
        texture.initWithData(data, cc.Texture2D.PixelFormat.RGBA8888, 2, 2);
        texture.handleLoadedTexture();
        this._texture = texture;
        // texture.packable = true;
        texture.addRef();
        return this._texture;
    };
    /**
     * 初始化
     */
    UIModalScript.prototype.init = function () {
        var maskTexture = this.getSingleTexture();
        var size = cc.view.getVisibleSize();
        this.node.height = size.height;
        this.node.width = size.width;
        this.node.addComponent(cc.Button);
        this.node.addComponent(ContentAdapter_1.default);
        this.node.on('click', this.clickMaskWindow, this);
        var sprite = this.node.addComponent(cc.Sprite);
        sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
        sprite.type = cc.Sprite.Type.SIMPLE;
        sprite.spriteFrame = new cc.SpriteFrame(maskTexture);
        this.node.color = new cc.Color(0, 0, 0);
        this.node.opacity = 0;
        this.node.active = false;
    };
    // 
    UIModalScript.prototype.showModal = function (lucenyType, time, isEasing) {
        if (time === void 0) { time = 0.6; }
        if (isEasing === void 0) { isEasing = true; }
        return __awaiter(this, void 0, void 0, function () {
            var o;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        o = 0;
                        switch (lucenyType) {
                            case SysDefine_1.ModalOpacity.None:
                                this.node.active = false;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityZero:
                                o = 0;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityLow:
                                o = 63;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityHalf:
                                o = 126;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityHigh:
                                o = 189;
                                break;
                            case SysDefine_1.ModalOpacity.OpacityFull:
                                o = 255;
                                break;
                        }
                        if (!this.node.active)
                            return [2 /*return*/];
                        if (!isEasing) return [3 /*break*/, 2];
                        return [4 /*yield*/, CocosHelper_1.default.runTweenSync(this.node, cc.tween().to(time, { opacity: o }))];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        this.node.opacity = o;
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UIModalScript.prototype.clickMaskWindow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        com = UIManager_1.default.getInstance().getForm(this.fid);
                        if (!(com && com.modalType.clickMaskClose)) return [3 /*break*/, 2];
                        return [4 /*yield*/, WindowMgr_1.default.close(this.fid)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    UIModalScript = __decorate([
        ccclass
    ], UIModalScript);
    return UIModalScript;
}(cc.Component));
exports.default = UIModalScript;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQ29tbW9uL1VJTW9kYWxTY3JpcHQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsNERBQXVEO0FBQ3ZELGtEQUE2QztBQUM3QyxrREFBNkM7QUFDN0Msb0RBQStDO0FBQy9DLHlDQUEyQztBQUlyQyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUEyQyxpQ0FBWTtJQUF2RDtRQUFBLHFFQXdGQztRQXJGRyxzQkFBc0I7UUFDZCxjQUFRLEdBQWlCLElBQUksQ0FBQzs7SUFvRjFDLENBQUM7SUFuRlcsd0NBQWdCLEdBQXhCO1FBQ0ksSUFBSSxJQUFJLENBQUMsUUFBUTtZQUFFLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN4QyxJQUFJLElBQUksR0FBUSxJQUFJLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzFDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO2dCQUNsQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztnQkFDbEMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO2FBQ3JDO1NBQ0o7UUFDRCxJQUFJLE9BQU8sR0FBRyxJQUFJLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQyxPQUFPLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQTtRQUM3QixPQUFPLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BFLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBQ3hCLDJCQUEyQjtRQUMzQixPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7UUFFakIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7T0FFRztJQUNJLDRCQUFJLEdBQVg7UUFDSSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUUxQyxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUU3QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsd0JBQWMsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRWxELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUM5QyxNQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztRQUM1QyxNQUFNLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNwQyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUVyRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRCxHQUFHO0lBQ1UsaUNBQVMsR0FBdEIsVUFBdUIsVUFBa0IsRUFBRSxJQUFrQixFQUFFLFFBQXdCO1FBQTVDLHFCQUFBLEVBQUEsVUFBa0I7UUFBRSx5QkFBQSxFQUFBLGVBQXdCOzs7Ozs7d0JBQy9FLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ1YsUUFBUSxVQUFVLEVBQUU7NEJBQ2hCLEtBQUssd0JBQVksQ0FBQyxJQUFJO2dDQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0NBQ3pCLE1BQU07NEJBQ1YsS0FBSyx3QkFBWSxDQUFDLFdBQVc7Z0NBQ3pCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0NBQ04sTUFBTTs0QkFDVixLQUFLLHdCQUFZLENBQUMsVUFBVTtnQ0FDeEIsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQ0FDUCxNQUFNOzRCQUNWLEtBQUssd0JBQVksQ0FBQyxXQUFXO2dDQUN6QixDQUFDLEdBQUcsR0FBRyxDQUFDO2dDQUNSLE1BQU07NEJBQ1YsS0FBSyx3QkFBWSxDQUFDLFdBQVc7Z0NBQ3pCLENBQUMsR0FBRyxHQUFHLENBQUM7Z0NBQ1IsTUFBTTs0QkFDVixLQUFLLHdCQUFZLENBQUMsV0FBVztnQ0FDekIsQ0FBQyxHQUFHLEdBQUcsQ0FBQztnQ0FDUixNQUFNO3lCQUNiO3dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU07NEJBQUUsc0JBQU87NkJBQzFCLFFBQVEsRUFBUix3QkFBUTt3QkFDUixxQkFBTSxxQkFBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBQTs7d0JBQTlFLFNBQThFLENBQUM7Ozt3QkFFL0UsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDOzs7Ozs7S0FFN0I7SUFFWSx1Q0FBZSxHQUE1Qjs7Ozs7O3dCQUNRLEdBQUcsR0FBRyxtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFhLENBQUM7NkJBQzVELENBQUEsR0FBRyxJQUFJLEdBQUcsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFBLEVBQW5DLHdCQUFtQzt3QkFDbkMscUJBQU0sbUJBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFBOzt3QkFBL0IsU0FBK0IsQ0FBQzs7Ozs7O0tBRXZDO0lBdkZnQixhQUFhO1FBRGpDLE9BQU87T0FDYSxhQUFhLENBd0ZqQztJQUFELG9CQUFDO0NBeEZELEFBd0ZDLENBeEYwQyxFQUFFLENBQUMsU0FBUyxHQXdGdEQ7a0JBeEZvQixhQUFhIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENvbnRlbnRBZGFwdGVyIGZyb20gXCIuLi9BZGFwdGVyL0NvbnRlbnRBZGFwdGVyXCI7XG5pbXBvcnQgVUlNYW5hZ2VyIGZyb20gXCIuLi9NYW5hZ2VyL1VJTWFuYWdlclwiO1xuaW1wb3J0IFdpbmRvd01nciBmcm9tIFwiLi4vTWFuYWdlci9XaW5kb3dNZ3JcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmltcG9ydCB7IE1vZGFsT3BhY2l0eSB9IGZyb20gXCIuL1N5c0RlZmluZVwiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi9VSUZvcm1cIjtcblxuXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlNb2RhbFNjcmlwdCBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBwdWJsaWMgZmlkOiBzdHJpbmc7XG4gICAgLyoqIOS7o+eggeWIm+W7uuS4gOS4quWNleiJsnRleHR1cmUgKi9cbiAgICBwcml2YXRlIF90ZXh0dXJlOiBjYy5UZXh0dXJlMkQgPSBudWxsO1xuICAgIHByaXZhdGUgZ2V0U2luZ2xlVGV4dHVyZSgpIHtcbiAgICAgICAgaWYgKHRoaXMuX3RleHR1cmUpIHJldHVybiB0aGlzLl90ZXh0dXJlO1xuICAgICAgICBsZXQgZGF0YTogYW55ID0gbmV3IFVpbnQ4QXJyYXkoMiAqIDIgKiA0KTtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCAyOyBpKyspIHtcbiAgICAgICAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgMjsgaisrKSB7XG4gICAgICAgICAgICAgICAgZGF0YVtpICogMiAqIDQgKyBqICogNCArIDBdID0gMjU1O1xuICAgICAgICAgICAgICAgIGRhdGFbaSAqIDIgKiA0ICsgaiAqIDQgKyAxXSA9IDI1NTtcbiAgICAgICAgICAgICAgICBkYXRhW2kgKiAyICogNCArIGogKiA0ICsgMl0gPSAyNTU7XG4gICAgICAgICAgICAgICAgZGF0YVtpICogMiAqIDQgKyBqICogNCArIDNdID0gMjU1O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGxldCB0ZXh0dXJlID0gbmV3IGNjLlRleHR1cmUyRCgpO1xuICAgICAgICB0ZXh0dXJlLm5hbWUgPSAnc2luZ2xlIGNvbG9yJ1xuICAgICAgICB0ZXh0dXJlLmluaXRXaXRoRGF0YShkYXRhLCBjYy5UZXh0dXJlMkQuUGl4ZWxGb3JtYXQuUkdCQTg4ODgsIDIsIDIpO1xuICAgICAgICB0ZXh0dXJlLmhhbmRsZUxvYWRlZFRleHR1cmUoKTtcbiAgICAgICAgdGhpcy5fdGV4dHVyZSA9IHRleHR1cmU7XG4gICAgICAgIC8vIHRleHR1cmUucGFja2FibGUgPSB0cnVlO1xuICAgICAgICB0ZXh0dXJlLmFkZFJlZigpO1xuXG4gICAgICAgIHJldHVybiB0aGlzLl90ZXh0dXJlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOWIneWni+WMllxuICAgICAqL1xuICAgIHB1YmxpYyBpbml0KCkge1xuICAgICAgICBsZXQgbWFza1RleHR1cmUgPSB0aGlzLmdldFNpbmdsZVRleHR1cmUoKTtcblxuICAgICAgICBsZXQgc2l6ZSA9IGNjLnZpZXcuZ2V0VmlzaWJsZVNpemUoKTtcbiAgICAgICAgdGhpcy5ub2RlLmhlaWdodCA9IHNpemUuaGVpZ2h0O1xuICAgICAgICB0aGlzLm5vZGUud2lkdGggPSBzaXplLndpZHRoO1xuXG4gICAgICAgIHRoaXMubm9kZS5hZGRDb21wb25lbnQoY2MuQnV0dG9uKTtcbiAgICAgICAgdGhpcy5ub2RlLmFkZENvbXBvbmVudChDb250ZW50QWRhcHRlcik7XG4gICAgICAgIHRoaXMubm9kZS5vbignY2xpY2snLCB0aGlzLmNsaWNrTWFza1dpbmRvdywgdGhpcyk7XG5cbiAgICAgICAgbGV0IHNwcml0ZSA9IHRoaXMubm9kZS5hZGRDb21wb25lbnQoY2MuU3ByaXRlKVxuICAgICAgICBzcHJpdGUuc2l6ZU1vZGUgPSBjYy5TcHJpdGUuU2l6ZU1vZGUuQ1VTVE9NO1xuICAgICAgICBzcHJpdGUudHlwZSA9IGNjLlNwcml0ZS5UeXBlLlNJTVBMRTtcbiAgICAgICAgc3ByaXRlLnNwcml0ZUZyYW1lID0gbmV3IGNjLlNwcml0ZUZyYW1lKG1hc2tUZXh0dXJlKTtcblxuICAgICAgICB0aGlzLm5vZGUuY29sb3IgPSBuZXcgY2MuQ29sb3IoMCwgMCwgMCk7XG4gICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMDtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIC8vIFxuICAgIHB1YmxpYyBhc3luYyBzaG93TW9kYWwobHVjZW55VHlwZTogbnVtYmVyLCB0aW1lOiBudW1iZXIgPSAwLjYsIGlzRWFzaW5nOiBib29sZWFuID0gdHJ1ZSkge1xuICAgICAgICBsZXQgbyA9IDA7XG4gICAgICAgIHN3aXRjaCAobHVjZW55VHlwZSkge1xuICAgICAgICAgICAgY2FzZSBNb2RhbE9wYWNpdHkuTm9uZTpcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIE1vZGFsT3BhY2l0eS5PcGFjaXR5WmVybzpcbiAgICAgICAgICAgICAgICBvID0gMDtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgTW9kYWxPcGFjaXR5Lk9wYWNpdHlMb3c6XG4gICAgICAgICAgICAgICAgbyA9IDYzO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBNb2RhbE9wYWNpdHkuT3BhY2l0eUhhbGY6XG4gICAgICAgICAgICAgICAgbyA9IDEyNjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgTW9kYWxPcGFjaXR5Lk9wYWNpdHlIaWdoOlxuICAgICAgICAgICAgICAgIG8gPSAxODk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIE1vZGFsT3BhY2l0eS5PcGFjaXR5RnVsbDpcbiAgICAgICAgICAgICAgICBvID0gMjU1O1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5ub2RlLmFjdGl2ZSkgcmV0dXJuO1xuICAgICAgICBpZiAoaXNFYXNpbmcpIHtcbiAgICAgICAgICAgIGF3YWl0IENvY29zSGVscGVyLnJ1blR3ZWVuU3luYyh0aGlzLm5vZGUsIGNjLnR3ZWVuKCkudG8odGltZSwgeyBvcGFjaXR5OiBvIH0pKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gbztcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyBjbGlja01hc2tXaW5kb3coKSB7XG4gICAgICAgIGxldCBjb20gPSBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5nZXRGb3JtKHRoaXMuZmlkKSBhcyBVSVdpbmRvdztcbiAgICAgICAgaWYgKGNvbSAmJiBjb20ubW9kYWxUeXBlLmNsaWNrTWFza0Nsb3NlKSB7XG4gICAgICAgICAgICBhd2FpdCBXaW5kb3dNZ3IuY2xvc2UodGhpcy5maWQpO1xuICAgICAgICB9XG4gICAgfVxufSJdfQ==