
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Common/UIForm.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fecebM0tgRBP4NNiSEz6NLd', 'UIForm');
// Script/Common/UIForm.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UITips = exports.UIFixed = exports.UIWindow = exports.UIScreen = void 0;
var FormMgr_1 = require("../Manager/FormMgr");
var CocosHelper_1 = require("../Utils/CocosHelper");
var Struct_1 = require("./Struct");
var SysDefine_1 = require("./SysDefine");
var UIBase_1 = require("./UIBase");
var UIScreen = /** @class */ (function (_super) {
    __extends(UIScreen, _super);
    function UIScreen() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.formType = SysDefine_1.FormType.Screen;
        _this.willDestory = true;
        return _this;
    }
    UIScreen.prototype.closeSelf = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, FormMgr_1.default.close({ prefabUrl: this.fid, type: this.formType })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return UIScreen;
}(UIBase_1.default));
exports.UIScreen = UIScreen;
var UIWindow = /** @class */ (function (_super) {
    __extends(UIWindow, _super);
    function UIWindow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.formType = SysDefine_1.FormType.Window;
        _this.modalType = new Struct_1.ModalType(); // 阴影类型
        _this.willDestory = true;
        return _this;
    }
    /** 显示效果 */
    UIWindow.prototype.showEffect = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.node.scale = 0;
                        return [4 /*yield*/, CocosHelper_1.default.runTweenSync(this.node, cc.tween().to(0.3, { scale: 1 }, cc.easeBackOut()))];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    UIWindow.prototype.closeSelf = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, FormMgr_1.default.close({ prefabUrl: this.fid, type: this.formType })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return UIWindow;
}(UIBase_1.default));
exports.UIWindow = UIWindow;
var UIFixed = /** @class */ (function (_super) {
    __extends(UIFixed, _super);
    function UIFixed() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.formType = SysDefine_1.FormType.Fixed;
        _this.willDestory = true;
        return _this;
    }
    UIFixed.prototype.closeSelf = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, FormMgr_1.default.close({ prefabUrl: this.fid, type: this.formType })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return UIFixed;
}(UIBase_1.default));
exports.UIFixed = UIFixed;
var UITips = /** @class */ (function (_super) {
    __extends(UITips, _super);
    function UITips() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.formType = SysDefine_1.FormType.Tips;
        _this.willDestory = true;
        return _this;
    }
    UITips.prototype.closeSelf = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, FormMgr_1.default.close({ prefabUrl: this.fid, type: this.formType })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return UITips;
}(UIBase_1.default));
exports.UITips = UITips;
// @ts-ignore
cc.UIScreen = UIScreen;
// @ts-ignore
cc.UIWindow = UIWindow;
// @ts-ignore
cc.UIFixed = UIFixed;
// @ts-ignore
cc.UITips = UITips;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQ29tbW9uL1VJRm9ybS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsOENBQXlDO0FBQ3pDLG9EQUErQztBQUMvQyxtQ0FBcUM7QUFDckMseUNBQXVDO0FBQ3ZDLG1DQUE4QjtBQUc5QjtJQUE4Qiw0QkFBTTtJQUFwQztRQUFBLHFFQU9DO1FBTkcsY0FBUSxHQUFHLG9CQUFRLENBQUMsTUFBTSxDQUFDO1FBQzNCLGlCQUFXLEdBQUcsSUFBSSxDQUFDOztJQUt2QixDQUFDO0lBSGdCLDRCQUFTLEdBQXRCO3VDQUEwQixPQUFPOzs7NEJBQ3RCLHFCQUFNLGlCQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFBOzRCQUF4RSxzQkFBTyxTQUFpRSxFQUFDOzs7O0tBQzVFO0lBQ0wsZUFBQztBQUFELENBUEEsQUFPQyxDQVA2QixnQkFBTSxHQU9uQztBQVBZLDRCQUFRO0FBU3JCO0lBQThCLDRCQUFNO0lBQXBDO1FBQUEscUVBZUM7UUFkRyxjQUFRLEdBQUcsb0JBQVEsQ0FBQyxNQUFNLENBQUM7UUFDM0IsZUFBUyxHQUFHLElBQUksa0JBQVMsRUFBRSxDQUFDLENBQWdCLE9BQU87UUFDbkQsaUJBQVcsR0FBRyxJQUFJLENBQUM7O0lBWXZCLENBQUM7SUFWRyxXQUFXO0lBQ0UsNkJBQVUsR0FBdkI7Ozs7O3dCQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQzt3QkFDcEIscUJBQU0scUJBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUFBOzt3QkFBN0YsU0FBNkYsQ0FBQzs7Ozs7S0FDakc7SUFFWSw0QkFBUyxHQUF0Qjt1Q0FBMEIsT0FBTzs7OzRCQUN0QixxQkFBTSxpQkFBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBQTs0QkFBeEUsc0JBQU8sU0FBaUUsRUFBQzs7OztLQUM1RTtJQUVMLGVBQUM7QUFBRCxDQWZBLEFBZUMsQ0FmNkIsZ0JBQU0sR0FlbkM7QUFmWSw0QkFBUTtBQWlCckI7SUFBNkIsMkJBQU07SUFBbkM7UUFBQSxxRUFRQztRQVBHLGNBQVEsR0FBRyxvQkFBUSxDQUFDLEtBQUssQ0FBQztRQUMxQixpQkFBVyxHQUFHLElBQUksQ0FBQzs7SUFNdkIsQ0FBQztJQUpnQiwyQkFBUyxHQUF0Qjt1Q0FBMEIsT0FBTzs7OzRCQUN0QixxQkFBTSxpQkFBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBQTs0QkFBeEUsc0JBQU8sU0FBaUUsRUFBQzs7OztLQUM1RTtJQUVMLGNBQUM7QUFBRCxDQVJBLEFBUUMsQ0FSNEIsZ0JBQU0sR0FRbEM7QUFSWSwwQkFBTztBQVVwQjtJQUE0QiwwQkFBTTtJQUFsQztRQUFBLHFFQU1DO1FBTEcsY0FBUSxHQUFHLG9CQUFRLENBQUMsSUFBSSxDQUFDO1FBQ3pCLGlCQUFXLEdBQUcsSUFBSSxDQUFDOztJQUl2QixDQUFDO0lBSGdCLDBCQUFTLEdBQXRCO3VDQUEwQixPQUFPOzs7NEJBQ3RCLHFCQUFNLGlCQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFBOzRCQUF4RSxzQkFBTyxTQUFpRSxFQUFDOzs7O0tBQzVFO0lBQ0wsYUFBQztBQUFELENBTkEsQUFNQyxDQU4yQixnQkFBTSxHQU1qQztBQU5ZLHdCQUFNO0FBUW5CLGFBQWE7QUFDYixFQUFFLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztBQUN2QixhQUFhO0FBQ2IsRUFBRSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7QUFDdkIsYUFBYTtBQUNiLEVBQUUsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0FBQ3JCLGFBQWE7QUFDYixFQUFFLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBGb3JtTWdyIGZyb20gXCIuLi9NYW5hZ2VyL0Zvcm1NZ3JcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmltcG9ydCB7IE1vZGFsVHlwZSB9IGZyb20gXCIuL1N0cnVjdFwiO1xuaW1wb3J0IHsgRm9ybVR5cGUgfSBmcm9tIFwiLi9TeXNEZWZpbmVcIjtcbmltcG9ydCBVSUJhc2UgZnJvbSBcIi4vVUlCYXNlXCI7XG5cblxuZXhwb3J0IGNsYXNzIFVJU2NyZWVuIGV4dGVuZHMgVUlCYXNlIHtcbiAgICBmb3JtVHlwZSA9IEZvcm1UeXBlLlNjcmVlbjtcbiAgICB3aWxsRGVzdG9yeSA9IHRydWU7XG5cbiAgICBwdWJsaWMgYXN5bmMgY2xvc2VTZWxmKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgICAgICByZXR1cm4gYXdhaXQgRm9ybU1nci5jbG9zZSh7IHByZWZhYlVybDogdGhpcy5maWQsIHR5cGU6IHRoaXMuZm9ybVR5cGUgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgVUlXaW5kb3cgZXh0ZW5kcyBVSUJhc2Uge1xuICAgIGZvcm1UeXBlID0gRm9ybVR5cGUuV2luZG93O1xuICAgIG1vZGFsVHlwZSA9IG5ldyBNb2RhbFR5cGUoKTsgICAgICAgICAgICAgICAgLy8g6Zi05b2x57G75Z6LXG4gICAgd2lsbERlc3RvcnkgPSB0cnVlO1xuXG4gICAgLyoqIOaYvuekuuaViOaenCAqL1xuICAgIHB1YmxpYyBhc3luYyBzaG93RWZmZWN0KCkge1xuICAgICAgICB0aGlzLm5vZGUuc2NhbGUgPSAwO1xuICAgICAgICBhd2FpdCBDb2Nvc0hlbHBlci5ydW5Ud2VlblN5bmModGhpcy5ub2RlLCBjYy50d2VlbigpLnRvKDAuMywgeyBzY2FsZTogMSB9LCBjYy5lYXNlQmFja091dCgpKSk7XG4gICAgfVxuXG4gICAgcHVibGljIGFzeW5jIGNsb3NlU2VsZigpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICAgICAgcmV0dXJuIGF3YWl0IEZvcm1NZ3IuY2xvc2UoeyBwcmVmYWJVcmw6IHRoaXMuZmlkLCB0eXBlOiB0aGlzLmZvcm1UeXBlIH0pO1xuICAgIH1cblxufVxuXG5leHBvcnQgY2xhc3MgVUlGaXhlZCBleHRlbmRzIFVJQmFzZSB7XG4gICAgZm9ybVR5cGUgPSBGb3JtVHlwZS5GaXhlZDtcbiAgICB3aWxsRGVzdG9yeSA9IHRydWU7XG5cbiAgICBwdWJsaWMgYXN5bmMgY2xvc2VTZWxmKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgICAgICByZXR1cm4gYXdhaXQgRm9ybU1nci5jbG9zZSh7IHByZWZhYlVybDogdGhpcy5maWQsIHR5cGU6IHRoaXMuZm9ybVR5cGUgfSk7XG4gICAgfVxuXG59XG5cbmV4cG9ydCBjbGFzcyBVSVRpcHMgZXh0ZW5kcyBVSUJhc2Uge1xuICAgIGZvcm1UeXBlID0gRm9ybVR5cGUuVGlwcztcbiAgICB3aWxsRGVzdG9yeSA9IHRydWU7XG4gICAgcHVibGljIGFzeW5jIGNsb3NlU2VsZigpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICAgICAgcmV0dXJuIGF3YWl0IEZvcm1NZ3IuY2xvc2UoeyBwcmVmYWJVcmw6IHRoaXMuZmlkLCB0eXBlOiB0aGlzLmZvcm1UeXBlIH0pO1xuICAgIH1cbn1cblxuLy8gQHRzLWlnbm9yZVxuY2MuVUlTY3JlZW4gPSBVSVNjcmVlbjtcbi8vIEB0cy1pZ25vcmVcbmNjLlVJV2luZG93ID0gVUlXaW5kb3c7XG4vLyBAdHMtaWdub3JlXG5jYy5VSUZpeGVkID0gVUlGaXhlZDtcbi8vIEB0cy1pZ25vcmVcbmNjLlVJVGlwcyA9IFVJVGlwcztcbiJdfQ==