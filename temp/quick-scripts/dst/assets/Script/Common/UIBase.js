
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Common/UIBase.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5ba19xmlHJNg4x0oLeSB7Ay', 'UIBase');
// Script/Common/UIBase.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var AdapterMgr_1 = require("../Manager/AdapterMgr");
var ResMgr_1 = require("../Manager/ResMgr");
var UIManager_1 = require("../Manager/UIManager");
var UIBase = /** @class */ (function (_super) {
    __extends(UIBase, _super);
    function UIBase() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /** 窗体数据 */
        _this.formData = null;
        /** 关闭窗口后销毁, 会将其依赖的资源一并销毁, 采用了引用计数的管理, 不用担心会影响其他窗体 */
        _this.willDestory = false;
        /** 是否已经调用过preinit方法 */
        _this._inited = false;
        _this.view = null;
        _this.model = null;
        /** 设置是否挡住触摸事件 */
        _this._blocker = null;
        return _this;
    }
    /** 预先初始化 */
    UIBase.prototype._preInit = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var errorMsg;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this._inited)
                            return [2 /*return*/];
                        this._inited = true;
                        this.view = this.getComponent(this.node.name + "_Auto");
                        return [4 /*yield*/, this.load(params)];
                    case 1:
                        errorMsg = _a.sent();
                        if (errorMsg) {
                            cc.error(errorMsg);
                            this.closeSelf();
                            return [2 /*return*/];
                        }
                        this.onInit(params);
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 可以在这里进行一些资源的加载, 具体实现可以看test下的代码 */
    UIBase.prototype.load = function (params) {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, null];
            });
        });
    };
    /** 初始化, 只调用一次 */
    UIBase.prototype.onInit = function (params) { };
    // 显示回调
    UIBase.prototype.onShow = function (params) { };
    // 在显示动画结束后回调
    UIBase.prototype.onAfterShow = function (params) { };
    // 隐藏回调
    UIBase.prototype.onHide = function () { };
    // 在隐藏动画结束后回调
    UIBase.prototype.onAfterHide = function () { };
    // 关闭自己
    UIBase.prototype.closeSelf = function () {
        return __awaiter(this, void 0, Promise, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, UIManager_1.default.getInstance().closeForm(this.fid)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 弹窗动画
     */
    UIBase.prototype.showEffect = function () {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); });
    };
    UIBase.prototype.hideEffect = function () {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); });
    };
    UIBase.prototype.setBlockInput = function (block) {
        if (!this._blocker) {
            var node = new cc.Node('block_input_events');
            this._blocker = node.addComponent(cc.BlockInputEvents);
            this._blocker.node.setContentSize(AdapterMgr_1.default.inst.visibleSize);
            this.node.addChild(this._blocker.node, cc.macro.MAX_ZINDEX);
        }
        this._blocker.node.active = block;
    };
    UIBase.prototype.loadRes = function (url, type) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ResMgr_1.default.inst.loadDynamicRes(url, type, this.fid)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return UIBase;
}(cc.Component));
exports.default = UIBase;
//@ts-ignore
cc.UIBase = UIBase;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQ29tbW9uL1VJQmFzZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvREFBK0M7QUFDL0MsNENBQXVDO0FBQ3ZDLGtEQUE2QztBQUk3QztJQUFvQywwQkFBWTtJQUFoRDtRQUFBLHFFQXlFQztRQXRFRyxXQUFXO1FBQ0osY0FBUSxHQUFjLElBQUssQ0FBQztRQUduQyxxREFBcUQ7UUFDOUMsaUJBQVcsR0FBRyxLQUFLLENBQUM7UUFDM0IsdUJBQXVCO1FBQ2YsYUFBTyxHQUFHLEtBQUssQ0FBQztRQUVqQixVQUFJLEdBQWlCLElBQUssQ0FBQztRQWlCbEMsV0FBSyxHQUFRLElBQUksQ0FBQztRQTZCbEIsaUJBQWlCO1FBQ1QsY0FBUSxHQUF3QixJQUFJLENBQUM7O0lBY2pELENBQUM7SUEzREcsWUFBWTtJQUNDLHlCQUFRLEdBQXJCLFVBQXNCLE1BQVc7Ozs7Ozt3QkFDN0IsSUFBSSxJQUFJLENBQUMsT0FBTzs0QkFBRSxzQkFBTzt3QkFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7d0JBQ3BCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksVUFBTyxDQUFDLENBQUM7d0JBRXpDLHFCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUFsQyxRQUFRLEdBQUcsU0FBdUI7d0JBQ3RDLElBQUksUUFBUSxFQUFFOzRCQUNWLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQ25CLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQzs0QkFDakIsc0JBQU87eUJBQ1Y7d0JBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzs7Ozs7S0FDdkI7SUFJRCxzQ0FBc0M7SUFDekIscUJBQUksR0FBakIsVUFBa0IsTUFBVzt1Q0FBRyxPQUFPOztnQkFDbkMsc0JBQU8sSUFBSSxFQUFDOzs7S0FDZjtJQUVELGlCQUFpQjtJQUNWLHVCQUFNLEdBQWIsVUFBYyxNQUFXLElBQUksQ0FBQztJQUM5QixPQUFPO0lBQ0EsdUJBQU0sR0FBYixVQUFjLE1BQVcsSUFBSSxDQUFDO0lBQzlCLGFBQWE7SUFDTiw0QkFBVyxHQUFsQixVQUFtQixNQUFXLElBQUksQ0FBQztJQUNuQyxPQUFPO0lBQ0EsdUJBQU0sR0FBYixjQUFrQixDQUFDO0lBQ25CLGFBQWE7SUFDTiw0QkFBVyxHQUFsQixjQUF1QixDQUFDO0lBRXhCLE9BQU87SUFDTSwwQkFBUyxHQUF0Qjt1Q0FBMEIsT0FBTzs7OzRCQUN0QixxQkFBTSxtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUE7NEJBQXhELHNCQUFPLFNBQWlELEVBQUM7Ozs7S0FDNUQ7SUFFRDs7T0FFRztJQUNVLDJCQUFVLEdBQXZCOzs7O0tBQTZCO0lBQ2hCLDJCQUFVLEdBQXZCOzs7O0tBQTZCO0lBSXRCLDhCQUFhLEdBQXBCLFVBQXFCLEtBQWM7UUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQy9EO1FBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QyxDQUFDO0lBRVksd0JBQU8sR0FBcEIsVUFBcUIsR0FBVyxFQUFFLElBQXNCOzs7OzRCQUM3QyxxQkFBTSxnQkFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUE7NEJBQTVELHNCQUFPLFNBQXFELEVBQUM7Ozs7S0FDaEU7SUFDTCxhQUFDO0FBQUQsQ0F6RUEsQUF5RUMsQ0F6RW1DLEVBQUUsQ0FBQyxTQUFTLEdBeUUvQzs7QUFFRCxZQUFZO0FBQ1osRUFBRSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQWRhcHRlck1nciBmcm9tIFwiLi4vTWFuYWdlci9BZGFwdGVyTWdyXCI7XG5pbXBvcnQgUmVzTWdyIGZyb20gXCIuLi9NYW5hZ2VyL1Jlc01nclwiO1xuaW1wb3J0IFVJTWFuYWdlciBmcm9tIFwiLi4vTWFuYWdlci9VSU1hbmFnZXJcIjtcbmltcG9ydCB7IElGb3JtRGF0YSB9IGZyb20gXCIuL1N0cnVjdFwiO1xuaW1wb3J0IHsgRm9ybVR5cGUgfSBmcm9tIFwiLi9TeXNEZWZpbmVcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlCYXNlIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcbiAgICAvKiog56qX5L2TaWQs6K+l56qX5L2T55qE5ZSv5LiA5qCH56S6KOivt+S4jeimgeWvuei/meS4quWAvOi/m+ihjOi1i+WAvOaTjeS9nCwg5YaF6YOo5bey57uP5a6e546w5LqG5a+55bqU55qE6LWL5YC8KSAqL1xuICAgIHB1YmxpYyBmaWQ6IHN0cmluZztcbiAgICAvKiog56qX5L2T5pWw5o2uICovXG4gICAgcHVibGljIGZvcm1EYXRhOiBJRm9ybURhdGEgPSBudWxsITtcbiAgICAvKiog56qX5L2T57G75Z6LICovXG4gICAgcHVibGljIGZvcm1UeXBlOiBGb3JtVHlwZTtcbiAgICAvKiog5YWz6Zet56qX5Y+j5ZCO6ZSA5q+BLCDkvJrlsIblhbbkvp3otZbnmoTotYTmupDkuIDlubbplIDmr4EsIOmHh+eUqOS6huW8leeUqOiuoeaVsOeahOeuoeeQhiwg5LiN55So5ouF5b+D5Lya5b2x5ZON5YW25LuW56qX5L2TICovXG4gICAgcHVibGljIHdpbGxEZXN0b3J5ID0gZmFsc2U7XG4gICAgLyoqIOaYr+WQpuW3sue7j+iwg+eUqOi/h3ByZWluaXTmlrnms5UgKi9cbiAgICBwcml2YXRlIF9pbml0ZWQgPSBmYWxzZTtcblxuICAgIHB1YmxpYyB2aWV3OiBjYy5Db21wb25lbnQgPSBudWxsITtcblxuICAgIC8qKiDpooTlhYjliJ3lp4vljJYgKi9cbiAgICBwdWJsaWMgYXN5bmMgX3ByZUluaXQocGFyYW1zOiBhbnkpIHtcbiAgICAgICAgaWYgKHRoaXMuX2luaXRlZCkgcmV0dXJuO1xuICAgICAgICB0aGlzLl9pbml0ZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLnZpZXcgPSB0aGlzLmdldENvbXBvbmVudChgJHt0aGlzLm5vZGUubmFtZX1fQXV0b2ApO1xuICAgICAgICAvLyDliqDovb3ov5nkuKpVSeS+nei1lueahOWFtuS7lui1hOa6kFxuICAgICAgICBsZXQgZXJyb3JNc2cgPSBhd2FpdCB0aGlzLmxvYWQocGFyYW1zKTtcbiAgICAgICAgaWYgKGVycm9yTXNnKSB7XG4gICAgICAgICAgICBjYy5lcnJvcihlcnJvck1zZyk7XG4gICAgICAgICAgICB0aGlzLmNsb3NlU2VsZigpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub25Jbml0KHBhcmFtcyk7XG4gICAgfVxuXG4gICAgbW9kZWw6IGFueSA9IG51bGw7XG5cbiAgICAvKiog5Y+v5Lul5Zyo6L+Z6YeM6L+b6KGM5LiA5Lqb6LWE5rqQ55qE5Yqg6L29LCDlhbfkvZPlrp7njrDlj6/ku6XnnIt0ZXN05LiL55qE5Luj56CBICovXG4gICAgcHVibGljIGFzeW5jIGxvYWQocGFyYW1zOiBhbnkpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICAvKiog5Yid5aeL5YyWLCDlj6rosIPnlKjkuIDmrKEgKi9cbiAgICBwdWJsaWMgb25Jbml0KHBhcmFtczogYW55KSB7IH1cbiAgICAvLyDmmL7npLrlm57osINcbiAgICBwdWJsaWMgb25TaG93KHBhcmFtczogYW55KSB7IH1cbiAgICAvLyDlnKjmmL7npLrliqjnlLvnu5PmnZ/lkI7lm57osINcbiAgICBwdWJsaWMgb25BZnRlclNob3cocGFyYW1zOiBhbnkpIHsgfVxuICAgIC8vIOmakOiXj+Wbnuiwg1xuICAgIHB1YmxpYyBvbkhpZGUoKSB7IH1cbiAgICAvLyDlnKjpmpDol4/liqjnlLvnu5PmnZ/lkI7lm57osINcbiAgICBwdWJsaWMgb25BZnRlckhpZGUoKSB7IH1cblxuICAgIC8vIOWFs+mXreiHquW3sVxuICAgIHB1YmxpYyBhc3luYyBjbG9zZVNlbGYoKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgICAgIHJldHVybiBhd2FpdCBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5jbG9zZUZvcm0odGhpcy5maWQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOW8ueeql+WKqOeUu1xuICAgICAqL1xuICAgIHB1YmxpYyBhc3luYyBzaG93RWZmZWN0KCkgeyB9XG4gICAgcHVibGljIGFzeW5jIGhpZGVFZmZlY3QoKSB7IH1cblxuICAgIC8qKiDorr7nva7mmK/lkKbmjKHkvY/op6bmkbjkuovku7YgKi9cbiAgICBwcml2YXRlIF9ibG9ja2VyOiBjYy5CbG9ja0lucHV0RXZlbnRzID0gbnVsbDtcbiAgICBwdWJsaWMgc2V0QmxvY2tJbnB1dChibG9jazogYm9vbGVhbikge1xuICAgICAgICBpZiAoIXRoaXMuX2Jsb2NrZXIpIHtcbiAgICAgICAgICAgIGxldCBub2RlID0gbmV3IGNjLk5vZGUoJ2Jsb2NrX2lucHV0X2V2ZW50cycpO1xuICAgICAgICAgICAgdGhpcy5fYmxvY2tlciA9IG5vZGUuYWRkQ29tcG9uZW50KGNjLkJsb2NrSW5wdXRFdmVudHMpO1xuICAgICAgICAgICAgdGhpcy5fYmxvY2tlci5ub2RlLnNldENvbnRlbnRTaXplKEFkYXB0ZXJNZ3IuaW5zdC52aXNpYmxlU2l6ZSk7XG4gICAgICAgICAgICB0aGlzLm5vZGUuYWRkQ2hpbGQodGhpcy5fYmxvY2tlci5ub2RlLCBjYy5tYWNyby5NQVhfWklOREVYKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9ibG9ja2VyLm5vZGUuYWN0aXZlID0gYmxvY2s7XG4gICAgfVxuXG4gICAgcHVibGljIGFzeW5jIGxvYWRSZXModXJsOiBzdHJpbmcsIHR5cGU/OiB0eXBlb2YgY2MuQXNzZXQpIHtcbiAgICAgICAgcmV0dXJuIGF3YWl0IFJlc01nci5pbnN0LmxvYWREeW5hbWljUmVzKHVybCwgdHlwZSwgdGhpcy5maWQpO1xuICAgIH1cbn1cblxuLy9AdHMtaWdub3JlXG5jYy5VSUJhc2UgPSBVSUJhc2U7Il19