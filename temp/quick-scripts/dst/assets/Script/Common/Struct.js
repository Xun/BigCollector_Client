
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Common/Struct.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '294d4cuj8FOwrGZgHqxSfsR', 'Struct');
// Script/Common/Struct.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EPriority = exports.ModalType = void 0;
var SysDefine_1 = require("./SysDefine");
var ModalType = /** @class */ (function () {
    function ModalType(opacity, ClickMaskClose, IsEasing, EasingTime) {
        if (opacity === void 0) { opacity = SysDefine_1.ModalOpacity.OpacityHalf; }
        if (ClickMaskClose === void 0) { ClickMaskClose = false; }
        if (IsEasing === void 0) { IsEasing = true; }
        if (EasingTime === void 0) { EasingTime = 0.2; }
        this.opacity = SysDefine_1.ModalOpacity.OpacityHalf;
        this.clickMaskClose = false; // 点击阴影关闭
        this.isEasing = true; // 缓动实现
        this.easingTime = 0.2; // 缓动时间
        this.opacity = opacity;
        this.clickMaskClose = ClickMaskClose;
        this.isEasing = IsEasing;
        this.easingTime = EasingTime;
    }
    return ModalType;
}());
exports.ModalType = ModalType;
var EPriority;
(function (EPriority) {
    EPriority[EPriority["ZERO"] = 0] = "ZERO";
    EPriority[EPriority["ONE"] = 1] = "ONE";
    EPriority[EPriority["TWO"] = 2] = "TWO";
    EPriority[EPriority["THREE"] = 3] = "THREE";
    EPriority[EPriority["FOUR"] = 4] = "FOUR";
    EPriority[EPriority["FIVE"] = 5] = "FIVE";
    EPriority[EPriority["SIX"] = 6] = "SIX";
    EPriority[EPriority["SEVEN"] = 7] = "SEVEN";
    EPriority[EPriority["EIGHT"] = 8] = "EIGHT";
    EPriority[EPriority["NINE"] = 9] = "NINE";
})(EPriority = exports.EPriority || (exports.EPriority = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQ29tbW9uL1N0cnVjdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx5Q0FBMkM7QUFFM0M7SUFNSSxtQkFBWSxPQUFrQyxFQUFFLGNBQXNCLEVBQUUsUUFBZSxFQUFFLFVBQWdCO1FBQTdGLHdCQUFBLEVBQUEsVUFBVSx3QkFBWSxDQUFDLFdBQVc7UUFBRSwrQkFBQSxFQUFBLHNCQUFzQjtRQUFFLHlCQUFBLEVBQUEsZUFBZTtRQUFFLDJCQUFBLEVBQUEsZ0JBQWdCO1FBTGxHLFlBQU8sR0FBaUIsd0JBQVksQ0FBQyxXQUFXLENBQUM7UUFDakQsbUJBQWMsR0FBRyxLQUFLLENBQUMsQ0FBTSxTQUFTO1FBQ3RDLGFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBYSxPQUFPO1FBQ3BDLGVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBWSxPQUFPO1FBR3ZDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO0lBQ2pDLENBQUM7SUFDTCxnQkFBQztBQUFELENBWkEsQUFZQyxJQUFBO0FBWlksOEJBQVM7QUFnQ3RCLElBQVksU0FXWDtBQVhELFdBQVksU0FBUztJQUNqQix5Q0FBSSxDQUFBO0lBQ0osdUNBQUcsQ0FBQTtJQUNILHVDQUFHLENBQUE7SUFDSCwyQ0FBSyxDQUFBO0lBQ0wseUNBQUksQ0FBQTtJQUNKLHlDQUFJLENBQUE7SUFDSix1Q0FBRyxDQUFBO0lBQ0gsMkNBQUssQ0FBQTtJQUNMLDJDQUFLLENBQUE7SUFDTCx5Q0FBSSxDQUFBO0FBQ1IsQ0FBQyxFQVhXLFNBQVMsR0FBVCxpQkFBUyxLQUFULGlCQUFTLFFBV3BCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9kYWxPcGFjaXR5IH0gZnJvbSBcIi4vU3lzRGVmaW5lXCI7XG5cbmV4cG9ydCBjbGFzcyBNb2RhbFR5cGUge1xuICAgIHB1YmxpYyBvcGFjaXR5OiBNb2RhbE9wYWNpdHkgPSBNb2RhbE9wYWNpdHkuT3BhY2l0eUhhbGY7XG4gICAgcHVibGljIGNsaWNrTWFza0Nsb3NlID0gZmFsc2U7ICAgICAgLy8g54K55Ye76Zi05b2x5YWz6ZetXG4gICAgcHVibGljIGlzRWFzaW5nID0gdHJ1ZTsgICAgICAgICAgICAgLy8g57yT5Yqo5a6e546wXG4gICAgcHVibGljIGVhc2luZ1RpbWUgPSAwLjI7ICAgICAgICAgICAgLy8g57yT5Yqo5pe26Ze0XG5cbiAgICBjb25zdHJ1Y3RvcihvcGFjaXR5ID0gTW9kYWxPcGFjaXR5Lk9wYWNpdHlIYWxmLCBDbGlja01hc2tDbG9zZSA9IGZhbHNlLCBJc0Vhc2luZyA9IHRydWUsIEVhc2luZ1RpbWUgPSAwLjIpIHtcbiAgICAgICAgdGhpcy5vcGFjaXR5ID0gb3BhY2l0eTtcbiAgICAgICAgdGhpcy5jbGlja01hc2tDbG9zZSA9IENsaWNrTWFza0Nsb3NlO1xuICAgICAgICB0aGlzLmlzRWFzaW5nID0gSXNFYXNpbmc7XG4gICAgICAgIHRoaXMuZWFzaW5nVGltZSA9IEVhc2luZ1RpbWU7XG4gICAgfVxufVxuXG4vKipcbiAqIEAtLSBwcmVmYWJVcmw6IHByZWZhYui3r+W+hFxuICogQC0tIHR5cGU6IHByZWZhYuexu+Wei1xuICovXG5leHBvcnQgaW50ZXJmYWNlIElGb3JtQ29uZmlnIHtcbiAgICBwcmVmYWJVcmw6IHN0cmluZztcbiAgICB0eXBlOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUZvcm1EYXRhIHtcbiAgICBpc1Nob3dMb2FkaW5nPzogYm9vbGVhbjtcbiAgICBsb2FkaW5nRm9ybT86IElGb3JtQ29uZmlnO1xuICAgIG9uQ2xvc2U/OiBGdW5jdGlvbjtcbiAgICAvLyB3aW5kb3fnsbvlnovnmoRmb3Jt5omN5pyJXG4gICAgcHJpb3JpdHk/OiBFUHJpb3JpdHk7ICAgICAgIC8vIOW9k+WJjeacieW3sue7j+aYvuekuueahHdpbmRvd+aXtiwg5Lya5pS+562J5b6F5YiX6KGo6YeMLCDnm7TliLDlvZPliY3msqHmnInmraPlnKjmmL7npLrnmoR3aW5kb3fml7bmiY3ooqvmmL7npLpcbiAgICBzaG93V2FpdD86IGJvb2xlYW47ICAgICAgICAgLy8g5LyY5YWI57qnKOS8muW9seWTjeW8ueeql+eahOWxgue6pywg5YWI5Yik5pat5LyY5YWI57qnLCDlnKjliKTmlq3mt7vliqDpobrluo8pXG59XG5cbmV4cG9ydCBlbnVtIEVQcmlvcml0eSB7XG4gICAgWkVSTyxcbiAgICBPTkUsXG4gICAgVFdPLFxuICAgIFRIUkVFLFxuICAgIEZPVVIsXG4gICAgRklWRSxcbiAgICBTSVgsXG4gICAgU0VWRU4sXG4gICAgRUlHSFQsXG4gICAgTklORSxcbn0iXX0=