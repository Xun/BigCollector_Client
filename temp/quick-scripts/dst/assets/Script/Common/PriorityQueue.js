
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Common/PriorityQueue.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f2318nn5GJChLoRevUhhLkg', 'PriorityQueue');
// Script/Common/PriorityQueue.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PriorityElement = void 0;
/**
 * @author liupengxun
 * 优先队列
 */
var PriorityElement = /** @class */ (function () {
    function PriorityElement(data, priority) {
        this.data = data;
        this.priority = priority;
    }
    return PriorityElement;
}());
exports.PriorityElement = PriorityElement;
var PriorityQueue = /** @class */ (function () {
    function PriorityQueue() {
        this.queue = new Array(32);
        this._size = 0;
    }
    Object.defineProperty(PriorityQueue.prototype, "size", {
        get: function () {
            return this._size;
        },
        enumerable: false,
        configurable: true
    });
    /** 是否有这个元素 */
    PriorityQueue.prototype.hasElement = function (t) {
        for (var _i = 0, _a = this.queue; _i < _a.length; _i++) {
            var e = _a[_i];
            if (e.data === t) {
                return true;
            }
        }
        return false;
    };
    /** 入队 */
    PriorityQueue.prototype.enqueue = function (e, priority) {
        if (priority === void 0) { priority = 0; }
        if (this._size > this.queue.length) {
            this._expand();
        }
        this.queue[this._size++] = new PriorityElement(e, priority);
        this.upAdjust();
    };
    /** 出队 */
    PriorityQueue.prototype.dequeue = function () {
        if (this._size <= 0)
            return null;
        var head = this.queue[0];
        this.queue[0] = this.queue[--this._size];
        this.downAdjust();
        return head.data;
    };
    PriorityQueue.prototype.clear = function () {
        this.queue = [];
        this._size = 0;
    };
    /** 上调, 入队时判断入队元素优先级 */
    PriorityQueue.prototype.upAdjust = function () {
        var childIndex = this._size - 1;
        var parentIndex = Math.floor(childIndex / 2);
        var tmp = this.queue[childIndex];
        while (childIndex > 0 && tmp.priority > this.queue[parentIndex].priority) {
            this.queue[childIndex] = this.queue[parentIndex];
            childIndex = parentIndex;
            parentIndex = Math.floor(parentIndex / 2);
        }
        this.queue[childIndex] = tmp;
    };
    /** 出队 */
    PriorityQueue.prototype.downAdjust = function () {
        var parentIndex = 0;
        var tmp = this.queue[parentIndex];
        var childIndex = 1;
        while (childIndex < this._size) {
            if (childIndex + 1 < this._size && this.queue[childIndex + 1].priority > this.queue[childIndex].priority) {
                childIndex++;
            }
            if (tmp.priority >= this.queue[childIndex].priority) {
                break;
            }
            this.queue[parentIndex] = this.queue[childIndex];
            parentIndex = childIndex;
            childIndex = 2 * childIndex + 1;
        }
        this.queue[parentIndex] = tmp;
    };
    /** 扩列 */
    PriorityQueue.prototype._expand = function () {
        var newSize = Math.round(this.queue.length * 1.2) + 1;
        var oldQueue = this.queue;
        this.queue = new Array(newSize);
        for (var i = 0; i < oldQueue.length; i++) {
            this.queue[i] = oldQueue[i];
        }
    };
    PriorityQueue.prototype.toString = function () {
        var s = '';
        for (var i = 0; i < this._size; i++) {
            var data = this.queue[i].data;
            if (data.toString) {
                s += data.toString();
            }
            else {
                s += typeof data === "object" ? JSON.stringify(data) : data;
            }
        }
        return s;
    };
    return PriorityQueue;
}());
exports.default = PriorityQueue;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQ29tbW9uL1ByaW9yaXR5UXVldWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7OztHQUdHO0FBQ0g7SUFJSSx5QkFBWSxJQUFPLEVBQUUsUUFBZ0I7UUFDakMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7SUFDN0IsQ0FBQztJQUNMLHNCQUFDO0FBQUQsQ0FSQSxBQVFDLElBQUE7QUFSWSwwQ0FBZTtBQVU1QjtJQVFJO1FBUFEsVUFBSyxHQUE4QixJQUFJLEtBQUssQ0FBcUIsRUFBRSxDQUFDLENBQUM7UUFDckUsVUFBSyxHQUFXLENBQUMsQ0FBQztJQU8xQixDQUFDO0lBTEQsc0JBQVcsK0JBQUk7YUFBZjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDOzs7T0FBQTtJQUtELGNBQWM7SUFDUCxrQ0FBVSxHQUFqQixVQUFrQixDQUFJO1FBQ2xCLEtBQWdCLFVBQVUsRUFBVixLQUFBLElBQUksQ0FBQyxLQUFLLEVBQVYsY0FBVSxFQUFWLElBQVUsRUFBRTtZQUF2QixJQUFNLENBQUMsU0FBQTtZQUNSLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLEVBQUU7Z0JBQ2QsT0FBTyxJQUFJLENBQUM7YUFDZjtTQUNKO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELFNBQVM7SUFDRiwrQkFBTyxHQUFkLFVBQWUsQ0FBSSxFQUFFLFFBQW9CO1FBQXBCLHlCQUFBLEVBQUEsWUFBb0I7UUFDckMsSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNsQjtRQUNELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLEdBQUcsSUFBSSxlQUFlLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsU0FBUztJQUNGLCtCQUFPLEdBQWQ7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQztZQUFFLE9BQU8sSUFBSSxDQUFDO1FBQ2pDLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVNLDZCQUFLLEdBQVo7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztJQUNuQixDQUFDO0lBQ0QsdUJBQXVCO0lBQ2YsZ0NBQVEsR0FBaEI7UUFDSSxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNoQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM3QyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBRWhDLE9BQU8sVUFBVSxHQUFHLENBQUMsSUFBSSxHQUFHLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsUUFBUSxFQUFFO1lBQ3RFLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNqRCxVQUFVLEdBQUcsV0FBVyxDQUFDO1lBQ3pCLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQztTQUM3QztRQUVELElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQ2pDLENBQUM7SUFDRCxTQUFTO0lBQ0Qsa0NBQVUsR0FBbEI7UUFDSSxJQUFJLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDcEIsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNsQyxJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUM7UUFDbkIsT0FBTyxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUM1QixJQUFJLFVBQVUsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3RHLFVBQVUsRUFBRSxDQUFDO2FBQ2hCO1lBQ0QsSUFBSSxHQUFHLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsUUFBUSxFQUFFO2dCQUNqRCxNQUFNO2FBQ1Q7WUFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakQsV0FBVyxHQUFHLFVBQVUsQ0FBQztZQUN6QixVQUFVLEdBQUcsQ0FBQyxHQUFHLFVBQVUsR0FBRyxDQUFDLENBQUM7U0FDbkM7UUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEdBQUcsQ0FBQztJQUNsQyxDQUFDO0lBQ0QsU0FBUztJQUNELCtCQUFPLEdBQWY7UUFDSSxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN0RCxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDaEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDL0I7SUFDTCxDQUFDO0lBRU0sZ0NBQVEsR0FBZjtRQUNJLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNYLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2pDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzlCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDZixDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNILENBQUMsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzthQUMvRDtTQUNKO1FBQ0QsT0FBTyxDQUFDLENBQUM7SUFDYixDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQWxHQSxBQWtHQyxJQUFBIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBAYXV0aG9yIGxpdXBlbmd4dW5cbiAqIOS8mOWFiOmYn+WIl1xuICovXG5leHBvcnQgY2xhc3MgUHJpb3JpdHlFbGVtZW50PFQ+IHtcbiAgICBwdWJsaWMgZGF0YTogVDtcbiAgICBwdWJsaWMgcHJpb3JpdHk6IG51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKGRhdGE6IFQsIHByaW9yaXR5OiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5kYXRhID0gZGF0YTtcbiAgICAgICAgdGhpcy5wcmlvcml0eSA9IHByaW9yaXR5O1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUHJpb3JpdHlRdWV1ZTxUPiB7XG4gICAgcHJpdmF0ZSBxdWV1ZTogQXJyYXk8UHJpb3JpdHlFbGVtZW50PFQ+PiA9IG5ldyBBcnJheTxQcmlvcml0eUVsZW1lbnQ8VD4+KDMyKTtcbiAgICBwcml2YXRlIF9zaXplOiBudW1iZXIgPSAwO1xuXG4gICAgcHVibGljIGdldCBzaXplKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fc2l6ZTtcbiAgICB9XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICB9XG5cbiAgICAvKiog5piv5ZCm5pyJ6L+Z5Liq5YWD57SgICovXG4gICAgcHVibGljIGhhc0VsZW1lbnQodDogVCkge1xuICAgICAgICBmb3IgKGNvbnN0IGUgb2YgdGhpcy5xdWV1ZSkge1xuICAgICAgICAgICAgaWYgKGUuZGF0YSA9PT0gdCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICAvKiog5YWl6ZifICovXG4gICAgcHVibGljIGVucXVldWUoZTogVCwgcHJpb3JpdHk6IG51bWJlciA9IDApIHtcbiAgICAgICAgaWYgKHRoaXMuX3NpemUgPiB0aGlzLnF1ZXVlLmxlbmd0aCkge1xuICAgICAgICAgICAgdGhpcy5fZXhwYW5kKCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5xdWV1ZVt0aGlzLl9zaXplKytdID0gbmV3IFByaW9yaXR5RWxlbWVudChlLCBwcmlvcml0eSk7XG4gICAgICAgIHRoaXMudXBBZGp1c3QoKTtcbiAgICB9XG5cbiAgICAvKiog5Ye66ZifICovXG4gICAgcHVibGljIGRlcXVldWUoKSB7XG4gICAgICAgIGlmICh0aGlzLl9zaXplIDw9IDApIHJldHVybiBudWxsO1xuICAgICAgICBjb25zdCBoZWFkID0gdGhpcy5xdWV1ZVswXTtcbiAgICAgICAgdGhpcy5xdWV1ZVswXSA9IHRoaXMucXVldWVbLS10aGlzLl9zaXplXTtcbiAgICAgICAgdGhpcy5kb3duQWRqdXN0KCk7XG4gICAgICAgIHJldHVybiBoZWFkLmRhdGE7XG4gICAgfVxuXG4gICAgcHVibGljIGNsZWFyKCkge1xuICAgICAgICB0aGlzLnF1ZXVlID0gW107XG4gICAgICAgIHRoaXMuX3NpemUgPSAwO1xuICAgIH1cbiAgICAvKiog5LiK6LCDLCDlhaXpmJ/ml7bliKTmlq3lhaXpmJ/lhYPntKDkvJjlhYjnuqcgKi9cbiAgICBwcml2YXRlIHVwQWRqdXN0KCkge1xuICAgICAgICBsZXQgY2hpbGRJbmRleCA9IHRoaXMuX3NpemUgLSAxO1xuICAgICAgICBsZXQgcGFyZW50SW5kZXggPSBNYXRoLmZsb29yKGNoaWxkSW5kZXggLyAyKTtcbiAgICAgICAgbGV0IHRtcCA9IHRoaXMucXVldWVbY2hpbGRJbmRleF1cblxuICAgICAgICB3aGlsZSAoY2hpbGRJbmRleCA+IDAgJiYgdG1wLnByaW9yaXR5ID4gdGhpcy5xdWV1ZVtwYXJlbnRJbmRleF0ucHJpb3JpdHkpIHtcbiAgICAgICAgICAgIHRoaXMucXVldWVbY2hpbGRJbmRleF0gPSB0aGlzLnF1ZXVlW3BhcmVudEluZGV4XTtcbiAgICAgICAgICAgIGNoaWxkSW5kZXggPSBwYXJlbnRJbmRleDtcbiAgICAgICAgICAgIHBhcmVudEluZGV4ID0gTWF0aC5mbG9vcihwYXJlbnRJbmRleCAvIDIpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5xdWV1ZVtjaGlsZEluZGV4XSA9IHRtcDtcbiAgICB9XG4gICAgLyoqIOWHuumYnyAqL1xuICAgIHByaXZhdGUgZG93bkFkanVzdCgpIHtcbiAgICAgICAgbGV0IHBhcmVudEluZGV4ID0gMDtcbiAgICAgICAgbGV0IHRtcCA9IHRoaXMucXVldWVbcGFyZW50SW5kZXhdO1xuICAgICAgICBsZXQgY2hpbGRJbmRleCA9IDE7XG4gICAgICAgIHdoaWxlIChjaGlsZEluZGV4IDwgdGhpcy5fc2l6ZSkge1xuICAgICAgICAgICAgaWYgKGNoaWxkSW5kZXggKyAxIDwgdGhpcy5fc2l6ZSAmJiB0aGlzLnF1ZXVlW2NoaWxkSW5kZXggKyAxXS5wcmlvcml0eSA+IHRoaXMucXVldWVbY2hpbGRJbmRleF0ucHJpb3JpdHkpIHtcbiAgICAgICAgICAgICAgICBjaGlsZEluZGV4Kys7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodG1wLnByaW9yaXR5ID49IHRoaXMucXVldWVbY2hpbGRJbmRleF0ucHJpb3JpdHkpIHtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5xdWV1ZVtwYXJlbnRJbmRleF0gPSB0aGlzLnF1ZXVlW2NoaWxkSW5kZXhdO1xuICAgICAgICAgICAgcGFyZW50SW5kZXggPSBjaGlsZEluZGV4O1xuICAgICAgICAgICAgY2hpbGRJbmRleCA9IDIgKiBjaGlsZEluZGV4ICsgMTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnF1ZXVlW3BhcmVudEluZGV4XSA9IHRtcDtcbiAgICB9XG4gICAgLyoqIOaJqeWIlyAqL1xuICAgIHByaXZhdGUgX2V4cGFuZCgpIHtcbiAgICAgICAgbGV0IG5ld1NpemUgPSBNYXRoLnJvdW5kKHRoaXMucXVldWUubGVuZ3RoICogMS4yKSArIDE7XG4gICAgICAgIGNvbnN0IG9sZFF1ZXVlID0gdGhpcy5xdWV1ZTtcbiAgICAgICAgdGhpcy5xdWV1ZSA9IG5ldyBBcnJheShuZXdTaXplKTtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBvbGRRdWV1ZS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgdGhpcy5xdWV1ZVtpXSA9IG9sZFF1ZXVlW2ldO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHRvU3RyaW5nKCkge1xuICAgICAgICBsZXQgcyA9ICcnO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuX3NpemU7IGkrKykge1xuICAgICAgICAgICAgbGV0IGRhdGEgPSB0aGlzLnF1ZXVlW2ldLmRhdGE7XG4gICAgICAgICAgICBpZiAoZGF0YS50b1N0cmluZykge1xuICAgICAgICAgICAgICAgIHMgKz0gZGF0YS50b1N0cmluZygpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBzICs9IHR5cGVvZiBkYXRhID09PSBcIm9iamVjdFwiID8gSlNPTi5zdHJpbmdpZnkoZGF0YSkgOiBkYXRhO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzO1xuICAgIH1cbn0iXX0=