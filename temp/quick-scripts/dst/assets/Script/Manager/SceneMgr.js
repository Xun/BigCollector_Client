
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/SceneMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8337d/EvnBHjrW/BJTtgaeW', 'SceneMgr');
// Script/Manager/SceneMgr.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var SysDefine_1 = require("../Common/SysDefine");
var TipsMgr_1 = require("./TipsMgr");
var UIManager_1 = require("./UIManager");
var TAG = "SceneMgr";
var SceneMgr = /** @class */ (function () {
    function SceneMgr() {
        this._scenes = [];
        this._currScene = "";
    }
    SceneMgr.prototype.getCurrScene = function () {
        return UIManager_1.default.getInstance().getForm(this._currScene);
    };
    /** 打开一个场景 */
    SceneMgr.prototype.open = function (scenePath, params, formData) {
        return __awaiter(this, void 0, void 0, function () {
            var currScene, idx, com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this._currScene == scenePath) {
                            cc.warn(TAG, "当前场景和需要open的场景是同一个");
                            return [2 /*return*/, null];
                        }
                        if (!(formData && formData.isShowLoading)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.openLoading(formData === null || formData === void 0 ? void 0 : formData.loadingForm, params, formData)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!(this._scenes.length > 0)) return [3 /*break*/, 4];
                        currScene = this._scenes[this._scenes.length - 1];
                        return [4 /*yield*/, UIManager_1.default.getInstance().closeForm(currScene)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        idx = this._scenes.indexOf(scenePath);
                        if (idx == -1) {
                            this._scenes.push(scenePath);
                        }
                        else {
                            this._scenes.length = idx + 1;
                        }
                        this._currScene = scenePath;
                        return [4 /*yield*/, UIManager_1.default.getInstance().openForm(scenePath, params, formData)];
                    case 5:
                        com = _a.sent();
                        if (!(formData && formData.isShowLoading)) return [3 /*break*/, 7];
                        return [4 /*yield*/, this.closeLoading(formData === null || formData === void 0 ? void 0 : formData.loadingForm)];
                    case 6:
                        _a.sent();
                        _a.label = 7;
                    case 7: return [2 /*return*/, com];
                }
            });
        });
    };
    /** 回退一个场景 */
    SceneMgr.prototype.back = function (params, formData) {
        return __awaiter(this, void 0, void 0, function () {
            var currScene;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this._scenes.length <= 1) {
                            cc.warn(TAG, "已经是最后一个场景了, 无处可退");
                            return [2 /*return*/];
                        }
                        if (!(formData && formData.isShowLoading)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.openLoading(formData === null || formData === void 0 ? void 0 : formData.loadingForm, params, formData)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        currScene = this._scenes.pop();
                        return [4 /*yield*/, UIManager_1.default.getInstance().closeForm(currScene)];
                    case 3:
                        _a.sent();
                        this._currScene = this._scenes[this._scenes.length - 1];
                        return [4 /*yield*/, UIManager_1.default.getInstance().openForm(this._currScene, params, formData)];
                    case 4:
                        _a.sent();
                        if (!(formData && formData.isShowLoading)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.closeLoading(formData === null || formData === void 0 ? void 0 : formData.loadingForm)];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    SceneMgr.prototype.close = function (scenePath) {
        return __awaiter(this, void 0, void 0, function () {
            var com;
            return __generator(this, function (_a) {
                com = UIManager_1.default.getInstance().getForm(scenePath);
                if (com) {
                    return [2 /*return*/, UIManager_1.default.getInstance().closeForm(scenePath)];
                }
                return [2 /*return*/];
            });
        });
    };
    SceneMgr.prototype.openLoading = function (formConfig, params, formData) {
        return __awaiter(this, void 0, void 0, function () {
            var form;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        form = formConfig || SysDefine_1.SysDefine.defaultLoadingForm;
                        if (!form)
                            return [2 /*return*/];
                        return [4 /*yield*/, TipsMgr_1.default.open(form.prefabUrl, params, formData)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SceneMgr.prototype.closeLoading = function (formConfig) {
        return __awaiter(this, void 0, void 0, function () {
            var form;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        form = formConfig || SysDefine_1.SysDefine.defaultLoadingForm;
                        if (!form)
                            return [2 /*return*/];
                        return [4 /*yield*/, TipsMgr_1.default.close(form.prefabUrl)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return SceneMgr;
}());
exports.default = new SceneMgr();

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9TY2VuZU1nci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLGlEQUFnRDtBQUNoRCxxQ0FBZ0M7QUFDaEMseUNBQW9DO0FBRXBDLElBQU0sR0FBRyxHQUFHLFVBQVUsQ0FBQztBQUN2QjtJQUFBO1FBQ1ksWUFBTyxHQUFrQixFQUFFLENBQUM7UUFDNUIsZUFBVSxHQUFXLEVBQUUsQ0FBQztJQTRFcEMsQ0FBQztJQTFFVSwrQkFBWSxHQUFuQjtRQUNJLE9BQU8sbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCxhQUFhO0lBQ0EsdUJBQUksR0FBakIsVUFBa0IsU0FBaUIsRUFBRSxNQUFZLEVBQUUsUUFBb0I7Ozs7Ozt3QkFDbkUsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLFNBQVMsRUFBRTs0QkFDOUIsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsb0JBQW9CLENBQUMsQ0FBQzs0QkFDbkMsc0JBQU8sSUFBSSxFQUFDO3lCQUNmOzZCQUVHLENBQUEsUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUEsRUFBbEMsd0JBQWtDO3dCQUNsQyxxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxFQUFBOzt3QkFBL0QsU0FBK0QsQ0FBQzs7OzZCQUdoRSxDQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQSxFQUF2Qix3QkFBdUI7d0JBQ25CLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUN0RCxxQkFBTSxtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBQTs7d0JBQWxELFNBQWtELENBQUM7Ozt3QkFHbkQsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUMxQyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsRUFBRTs0QkFDWCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt5QkFDaEM7NkJBQU07NEJBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQzt5QkFDakM7d0JBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7d0JBRWxCLHFCQUFNLG1CQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLEVBQUE7O3dCQUF6RSxHQUFHLEdBQUcsU0FBbUU7NkJBQ3pFLENBQUEsUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUEsRUFBbEMsd0JBQWtDO3dCQUNsQyxxQkFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxXQUFXLENBQUMsRUFBQTs7d0JBQTlDLFNBQThDLENBQUM7OzRCQUVuRCxzQkFBTyxHQUFHLEVBQUM7Ozs7S0FDZDtJQUVELGFBQWE7SUFDQSx1QkFBSSxHQUFqQixVQUFrQixNQUFZLEVBQUUsUUFBb0I7Ozs7Ozt3QkFDaEQsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7NEJBQzFCLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLGtCQUFrQixDQUFDLENBQUM7NEJBQ2pDLHNCQUFPO3lCQUNWOzZCQUNHLENBQUEsUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUEsRUFBbEMsd0JBQWtDO3dCQUNsQyxxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxFQUFBOzt3QkFBL0QsU0FBK0QsQ0FBQzs7O3dCQUVoRSxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQzt3QkFDbkMscUJBQU0sbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUE7O3dCQUFsRCxTQUFrRCxDQUFDO3dCQUVuRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQ3hELHFCQUFNLG1CQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxFQUFBOzt3QkFBekUsU0FBeUUsQ0FBQzs2QkFDdEUsQ0FBQSxRQUFRLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQSxFQUFsQyx3QkFBa0M7d0JBQ2xDLHFCQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLFdBQVcsQ0FBQyxFQUFBOzt3QkFBOUMsU0FBOEMsQ0FBQzs7Ozs7O0tBRXREO0lBRVksd0JBQUssR0FBbEIsVUFBbUIsU0FBaUI7Ozs7Z0JBQzVCLEdBQUcsR0FBRyxtQkFBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDckQsSUFBSSxHQUFHLEVBQUU7b0JBQ0wsc0JBQU8sbUJBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUM7aUJBQ3ZEOzs7O0tBQ0o7SUFFYSw4QkFBVyxHQUF6QixVQUEwQixVQUF1QixFQUFFLE1BQVcsRUFBRSxRQUFtQjs7Ozs7O3dCQUMzRSxJQUFJLEdBQUcsVUFBVSxJQUFJLHFCQUFTLENBQUMsa0JBQWtCLENBQUM7d0JBQ3RELElBQUksQ0FBQyxJQUFJOzRCQUFFLHNCQUFPO3dCQUNsQixxQkFBTSxpQkFBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsRUFBQTs7d0JBQXBELFNBQW9ELENBQUM7Ozs7O0tBQ3hEO0lBRWEsK0JBQVksR0FBMUIsVUFBMkIsVUFBdUI7Ozs7Ozt3QkFDMUMsSUFBSSxHQUFHLFVBQVUsSUFBSSxxQkFBUyxDQUFDLGtCQUFrQixDQUFDO3dCQUN0RCxJQUFJLENBQUMsSUFBSTs0QkFBRSxzQkFBTzt3QkFDbEIscUJBQU0saUJBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFBOzt3QkFBbkMsU0FBbUMsQ0FBQzs7Ozs7S0FDdkM7SUFFTCxlQUFDO0FBQUQsQ0E5RUEsQUE4RUMsSUFBQTtBQUVELGtCQUFlLElBQUksUUFBUSxFQUFFLENBQUMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCB7IElGb3JtQ29uZmlnLCBJRm9ybURhdGEgfSBmcm9tIFwiLi4vQ29tbW9uL1N0cnVjdFwiO1xuaW1wb3J0IHsgU3lzRGVmaW5lIH0gZnJvbSBcIi4uL0NvbW1vbi9TeXNEZWZpbmVcIjtcbmltcG9ydCBUaXBzTWdyIGZyb20gXCIuL1RpcHNNZ3JcIjtcbmltcG9ydCBVSU1hbmFnZXIgZnJvbSBcIi4vVUlNYW5hZ2VyXCI7XG5cbmNvbnN0IFRBRyA9IFwiU2NlbmVNZ3JcIjtcbmNsYXNzIFNjZW5lTWdyIHtcbiAgICBwcml2YXRlIF9zY2VuZXM6IEFycmF5PHN0cmluZz4gPSBbXTtcbiAgICBwcml2YXRlIF9jdXJyU2NlbmU6IHN0cmluZyA9IFwiXCI7XG5cbiAgICBwdWJsaWMgZ2V0Q3VyclNjZW5lKCkge1xuICAgICAgICByZXR1cm4gVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkuZ2V0Rm9ybSh0aGlzLl9jdXJyU2NlbmUpO1xuICAgIH1cblxuICAgIC8qKiDmiZPlvIDkuIDkuKrlnLrmma8gKi9cbiAgICBwdWJsaWMgYXN5bmMgb3BlbihzY2VuZVBhdGg6IHN0cmluZywgcGFyYW1zPzogYW55LCBmb3JtRGF0YT86IElGb3JtRGF0YSkge1xuICAgICAgICBpZiAodGhpcy5fY3VyclNjZW5lID09IHNjZW5lUGF0aCkge1xuICAgICAgICAgICAgY2Mud2FybihUQUcsIFwi5b2T5YmN5Zy65pmv5ZKM6ZyA6KaBb3BlbueahOWcuuaZr+aYr+WQjOS4gOS4qlwiKTtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGZvcm1EYXRhICYmIGZvcm1EYXRhLmlzU2hvd0xvYWRpbmcpIHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMub3BlbkxvYWRpbmcoZm9ybURhdGE/LmxvYWRpbmdGb3JtLCBwYXJhbXMsIGZvcm1EYXRhKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLl9zY2VuZXMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgbGV0IGN1cnJTY2VuZSA9IHRoaXMuX3NjZW5lc1t0aGlzLl9zY2VuZXMubGVuZ3RoIC0gMV07XG4gICAgICAgICAgICBhd2FpdCBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5jbG9zZUZvcm0oY3VyclNjZW5lKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBpZHggPSB0aGlzLl9zY2VuZXMuaW5kZXhPZihzY2VuZVBhdGgpO1xuICAgICAgICBpZiAoaWR4ID09IC0xKSB7XG4gICAgICAgICAgICB0aGlzLl9zY2VuZXMucHVzaChzY2VuZVBhdGgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5fc2NlbmVzLmxlbmd0aCA9IGlkeCArIDE7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl9jdXJyU2NlbmUgPSBzY2VuZVBhdGg7XG5cbiAgICAgICAgbGV0IGNvbSA9IGF3YWl0IFVJTWFuYWdlci5nZXRJbnN0YW5jZSgpLm9wZW5Gb3JtKHNjZW5lUGF0aCwgcGFyYW1zLCBmb3JtRGF0YSk7XG4gICAgICAgIGlmIChmb3JtRGF0YSAmJiBmb3JtRGF0YS5pc1Nob3dMb2FkaW5nKSB7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLmNsb3NlTG9hZGluZyhmb3JtRGF0YT8ubG9hZGluZ0Zvcm0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjb207XG4gICAgfVxuXG4gICAgLyoqIOWbnumAgOS4gOS4quWcuuaZryAqL1xuICAgIHB1YmxpYyBhc3luYyBiYWNrKHBhcmFtcz86IGFueSwgZm9ybURhdGE/OiBJRm9ybURhdGEpIHtcbiAgICAgICAgaWYgKHRoaXMuX3NjZW5lcy5sZW5ndGggPD0gMSkge1xuICAgICAgICAgICAgY2Mud2FybihUQUcsIFwi5bey57uP5piv5pyA5ZCO5LiA5Liq5Zy65pmv5LqGLCDml6DlpITlj6/pgIBcIik7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGZvcm1EYXRhICYmIGZvcm1EYXRhLmlzU2hvd0xvYWRpbmcpIHtcbiAgICAgICAgICAgIGF3YWl0IHRoaXMub3BlbkxvYWRpbmcoZm9ybURhdGE/LmxvYWRpbmdGb3JtLCBwYXJhbXMsIGZvcm1EYXRhKTtcbiAgICAgICAgfVxuICAgICAgICBsZXQgY3VyclNjZW5lID0gdGhpcy5fc2NlbmVzLnBvcCgpO1xuICAgICAgICBhd2FpdCBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5jbG9zZUZvcm0oY3VyclNjZW5lKTtcblxuICAgICAgICB0aGlzLl9jdXJyU2NlbmUgPSB0aGlzLl9zY2VuZXNbdGhpcy5fc2NlbmVzLmxlbmd0aCAtIDFdO1xuICAgICAgICBhd2FpdCBVSU1hbmFnZXIuZ2V0SW5zdGFuY2UoKS5vcGVuRm9ybSh0aGlzLl9jdXJyU2NlbmUsIHBhcmFtcywgZm9ybURhdGEpO1xuICAgICAgICBpZiAoZm9ybURhdGEgJiYgZm9ybURhdGEuaXNTaG93TG9hZGluZykge1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5jbG9zZUxvYWRpbmcoZm9ybURhdGE/LmxvYWRpbmdGb3JtKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBhc3luYyBjbG9zZShzY2VuZVBhdGg6IHN0cmluZykge1xuICAgICAgICBsZXQgY29tID0gVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkuZ2V0Rm9ybShzY2VuZVBhdGgpO1xuICAgICAgICBpZiAoY29tKSB7XG4gICAgICAgICAgICByZXR1cm4gVUlNYW5hZ2VyLmdldEluc3RhbmNlKCkuY2xvc2VGb3JtKHNjZW5lUGF0aCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIG9wZW5Mb2FkaW5nKGZvcm1Db25maWc6IElGb3JtQ29uZmlnLCBwYXJhbXM6IGFueSwgZm9ybURhdGE6IElGb3JtRGF0YSkge1xuICAgICAgICBsZXQgZm9ybSA9IGZvcm1Db25maWcgfHwgU3lzRGVmaW5lLmRlZmF1bHRMb2FkaW5nRm9ybTtcbiAgICAgICAgaWYgKCFmb3JtKSByZXR1cm47XG4gICAgICAgIGF3YWl0IFRpcHNNZ3Iub3Blbihmb3JtLnByZWZhYlVybCwgcGFyYW1zLCBmb3JtRGF0YSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBjbG9zZUxvYWRpbmcoZm9ybUNvbmZpZzogSUZvcm1Db25maWcpIHtcbiAgICAgICAgbGV0IGZvcm0gPSBmb3JtQ29uZmlnIHx8IFN5c0RlZmluZS5kZWZhdWx0TG9hZGluZ0Zvcm07XG4gICAgICAgIGlmICghZm9ybSkgcmV0dXJuO1xuICAgICAgICBhd2FpdCBUaXBzTWdyLmNsb3NlKGZvcm0ucHJlZmFiVXJsKTtcbiAgICB9XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgbmV3IFNjZW5lTWdyKCk7Il19