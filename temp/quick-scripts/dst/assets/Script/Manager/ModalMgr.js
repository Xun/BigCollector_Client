
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/ModalMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a6cfbGOowFD6rGCFIN9QFMs', 'ModalMgr');
// Script/Manager/ModalMgr.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var SysDefine_1 = require("../Common/SysDefine");
var UIModalScript_1 = require("../Common/UIModalScript");
/**
 * 遮罩管理
 */
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ModalMgr = /** @class */ (function (_super) {
    __extends(ModalMgr, _super);
    function ModalMgr() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.uiModal = null;
        return _this;
    }
    ModalMgr_1 = ModalMgr;
    Object.defineProperty(ModalMgr, "inst", {
        get: function () {
            if (this._inst == null) {
                this._inst = new ModalMgr_1();
                var node = new cc.Node("UIModalNode");
                cc.find(ModalMgr_1.popUpRoot).addChild(node);
                ModalMgr_1.inst.uiModal = node.addComponent(UIModalScript_1.default);
                ModalMgr_1.inst.uiModal.init();
            }
            return this._inst;
        },
        enumerable: false,
        configurable: true
    });
    /** 为mask添加颜色 */
    ModalMgr.prototype.showModal = function (maskType) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.uiModal.showModal(maskType.opacity, maskType.easingTime, maskType.isEasing)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ModalMgr.prototype.checkModalWindow = function (coms) {
        if (coms.length <= 0) {
            this.uiModal.node.active = false;
            return;
        }
        this.uiModal.node.active = true;
        if (this.uiModal.node.parent) {
            this.uiModal.node.removeFromParent();
        }
        for (var i = coms.length - 1; i >= 0; i--) {
            if (coms[i].modalType.opacity > 0) {
                cc.find(ModalMgr_1.popUpRoot).addChild(this.uiModal.node, Math.max(coms[i].node.zIndex - 1, 0));
                this.uiModal.fid = coms[i].fid;
                this.showModal(coms[i].modalType);
                break;
            }
        }
    };
    var ModalMgr_1;
    ModalMgr.popUpRoot = SysDefine_1.SysDefine.SYS_UIROOT_NAME + '/' + SysDefine_1.SysDefine.SYS_POPUP_NODE;
    ModalMgr._inst = null;
    ModalMgr = ModalMgr_1 = __decorate([
        ccclass
    ], ModalMgr);
    return ModalMgr;
}(cc.Component));
exports.default = ModalMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9Nb2RhbE1nci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxpREFBZ0Q7QUFFaEQseURBQW9EO0FBRXBEOztHQUVHO0FBQ0csSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBc0MsNEJBQVk7SUFBbEQ7UUFBQSxxRUFzQ0M7UUF6QlcsYUFBTyxHQUFrQixJQUFJLENBQUM7O0lBeUIxQyxDQUFDO2lCQXRDb0IsUUFBUTtJQUd6QixzQkFBa0IsZ0JBQUk7YUFBdEI7WUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO2dCQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksVUFBUSxFQUFFLENBQUM7Z0JBQzVCLElBQUksSUFBSSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDdEMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQyxVQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLHVCQUFhLENBQUMsQ0FBQztnQkFDekQsVUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDaEM7WUFDRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFHRCxnQkFBZ0I7SUFDRiw0QkFBUyxHQUF2QixVQUF3QixRQUFtQjs7Ozs0QkFDdkMscUJBQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBQTs7d0JBQXRGLFNBQXNGLENBQUM7Ozs7O0tBQzFGO0lBRU0sbUNBQWdCLEdBQXZCLFVBQXdCLElBQWdCO1FBQ3BDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDbEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNqQyxPQUFPO1NBQ1Y7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDeEM7UUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDdkMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxDQUFDLEVBQUU7Z0JBQy9CLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5RixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO2dCQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDbEMsTUFBTTthQUNUO1NBQ0o7SUFDTCxDQUFDOztJQXBDYSxrQkFBUyxHQUFHLHFCQUFTLENBQUMsZUFBZSxHQUFHLEdBQUcsR0FBRyxxQkFBUyxDQUFDLGNBQWMsQ0FBQztJQUN2RSxjQUFLLEdBQWEsSUFBSSxDQUFDO0lBRnBCLFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0FzQzVCO0lBQUQsZUFBQztDQXRDRCxBQXNDQyxDQXRDcUMsRUFBRSxDQUFDLFNBQVMsR0FzQ2pEO2tCQXRDb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vZGFsVHlwZSB9IGZyb20gXCIuLi9Db21tb24vU3RydWN0XCI7XG5pbXBvcnQgeyBTeXNEZWZpbmUgfSBmcm9tIFwiLi4vQ29tbW9uL1N5c0RlZmluZVwiO1xuaW1wb3J0IHsgVUlXaW5kb3cgfSBmcm9tIFwiLi4vQ29tbW9uL1VJRm9ybVwiO1xuaW1wb3J0IFVJTW9kYWxTY3JpcHQgZnJvbSBcIi4uL0NvbW1vbi9VSU1vZGFsU2NyaXB0XCI7XG5cbi8qKlxuICog6YGu572p566h55CGXG4gKi9cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNb2RhbE1nciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG4gICAgcHVibGljIHN0YXRpYyBwb3BVcFJvb3QgPSBTeXNEZWZpbmUuU1lTX1VJUk9PVF9OQU1FICsgJy8nICsgU3lzRGVmaW5lLlNZU19QT1BVUF9OT0RFO1xuICAgIHB1YmxpYyBzdGF0aWMgX2luc3Q6IE1vZGFsTWdyID0gbnVsbDtcbiAgICBwdWJsaWMgc3RhdGljIGdldCBpbnN0KCkge1xuICAgICAgICBpZiAodGhpcy5faW5zdCA9PSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLl9pbnN0ID0gbmV3IE1vZGFsTWdyKCk7XG4gICAgICAgICAgICBsZXQgbm9kZSA9IG5ldyBjYy5Ob2RlKFwiVUlNb2RhbE5vZGVcIik7XG4gICAgICAgICAgICBjYy5maW5kKE1vZGFsTWdyLnBvcFVwUm9vdCkuYWRkQ2hpbGQobm9kZSk7XG4gICAgICAgICAgICBNb2RhbE1nci5pbnN0LnVpTW9kYWwgPSBub2RlLmFkZENvbXBvbmVudChVSU1vZGFsU2NyaXB0KTtcbiAgICAgICAgICAgIE1vZGFsTWdyLmluc3QudWlNb2RhbC5pbml0KCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX2luc3Q7XG4gICAgfVxuICAgIHByaXZhdGUgdWlNb2RhbDogVUlNb2RhbFNjcmlwdCA9IG51bGw7XG5cbiAgICAvKiog5Li6bWFza+a3u+WKoOminOiJsiAqL1xuICAgIHByaXZhdGUgYXN5bmMgc2hvd01vZGFsKG1hc2tUeXBlOiBNb2RhbFR5cGUpIHtcbiAgICAgICAgYXdhaXQgdGhpcy51aU1vZGFsLnNob3dNb2RhbChtYXNrVHlwZS5vcGFjaXR5LCBtYXNrVHlwZS5lYXNpbmdUaW1lLCBtYXNrVHlwZS5pc0Vhc2luZyk7XG4gICAgfVxuXG4gICAgcHVibGljIGNoZWNrTW9kYWxXaW5kb3coY29tczogVUlXaW5kb3dbXSkge1xuICAgICAgICBpZiAoY29tcy5sZW5ndGggPD0gMCkge1xuICAgICAgICAgICAgdGhpcy51aU1vZGFsLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy51aU1vZGFsLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgaWYgKHRoaXMudWlNb2RhbC5ub2RlLnBhcmVudCkge1xuICAgICAgICAgICAgdGhpcy51aU1vZGFsLm5vZGUucmVtb3ZlRnJvbVBhcmVudCgpO1xuICAgICAgICB9XG4gICAgICAgIGZvciAobGV0IGkgPSBjb21zLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgICBpZiAoY29tc1tpXS5tb2RhbFR5cGUub3BhY2l0eSA+IDApIHtcbiAgICAgICAgICAgICAgICBjYy5maW5kKE1vZGFsTWdyLnBvcFVwUm9vdCkuYWRkQ2hpbGQodGhpcy51aU1vZGFsLm5vZGUsIE1hdGgubWF4KGNvbXNbaV0ubm9kZS56SW5kZXggLSAxLCAwKSk7XG4gICAgICAgICAgICAgICAgdGhpcy51aU1vZGFsLmZpZCA9IGNvbXNbaV0uZmlkO1xuICAgICAgICAgICAgICAgIHRoaXMuc2hvd01vZGFsKGNvbXNbaV0ubW9kYWxUeXBlKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0iXX0=