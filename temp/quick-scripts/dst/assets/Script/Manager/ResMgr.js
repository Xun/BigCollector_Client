
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/ResMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a1946LwxMZELpC3XyersJGl', 'ResMgr');
// Script/Manager/ResMgr.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var EventCenter_1 = require("../Net/EventCenter");
var CocosHelper_1 = require("../Utils/CocosHelper");
/**
 * 资源加载, 针对的是Form
 * 首先将资源分为两类
 * 一种是在编辑器时将其拖上去图片, 这里将其称为静态图片,
 * 一种是在代码中使用cc.loader加载的图片, 这里将其称为动态图片
 *
 * 对于静态资源
 * 1, 加载  在加载prefab时, cocos会将其依赖的图片一并加载, 所有不需要我们担心
 * 2, 释放  这里采用的引用计数的管理方法, 只需要调用destoryForm即可
 *
 * 加载一个窗体
 * 第一步 加载prefab, 第二步 实例化prefab 获得 node
 * 所以销毁一个窗体 也需要两步, 销毁node, 销毁prefab
 */
var ResMgr = /** @class */ (function () {
    function ResMgr() {
        /**
         * 采用计数管理的办法, 管理form所依赖的资源
         */
        this._prefabDepends = cc.js.createMap();
        this._dynamicTags = cc.js.createMap();
        this._tmpAssetsDepends = []; // 临时缓存
        this._assetsReference = cc.js.createMap(); // 资源引用计数
    }
    Object.defineProperty(ResMgr, "inst", {
        get: function () {
            if (this.instance === null) {
                this.instance = new ResMgr();
            }
            return this.instance;
        },
        enumerable: false,
        configurable: true
    });
    /** 加载窗体 */
    ResMgr.prototype.loadForm = function (fid) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, res, deps;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this._loadResWithReference(fid, cc.Prefab)];
                    case 1:
                        _a = _b.sent(), res = _a.res, deps = _a.deps;
                        this._prefabDepends[fid] = deps;
                        return [2 /*return*/, cc.instantiate(res)];
                }
            });
        });
    };
    /** 销毁窗体 */
    ResMgr.prototype.destoryForm = function (com) {
        if (!com)
            return;
        EventCenter_1.EventCenter.targetOff(com);
        // 销毁依赖的资源
        this._destoryResWithReference(this._prefabDepends[com.fid]);
        this._prefabDepends[com.fid] = null;
        delete this._prefabDepends[com.fid];
        // 销毁node
        com.node.destroy();
    };
    /** 动态资源管理, 通过tag标记当前资源, 统一释放 */
    ResMgr.prototype.loadDynamicRes = function (url, type, tag) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, res, deps;
            var _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, this._loadResWithReference(url, type)];
                    case 1:
                        _a = _c.sent(), res = _a.res, deps = _a.deps;
                        if (!this._dynamicTags[tag]) {
                            this._dynamicTags[tag] = [];
                        }
                        (_b = this._dynamicTags[tag]).push.apply(_b, deps);
                        return [2 /*return*/, res];
                }
            });
        });
    };
    /** 销毁动态资源  */
    ResMgr.prototype.destoryDynamicRes = function (tag) {
        if (!this._dynamicTags[tag]) { // 销毁
            return false;
        }
        this._destoryResWithReference(this._dynamicTags[tag]);
        this._dynamicTags[tag] = null;
        delete this._dynamicTags[tag];
        return true;
    };
    /** 加载资源并添加引用计数 */
    ResMgr.prototype._loadResWithReference = function (url, type) {
        return __awaiter(this, void 0, void 0, function () {
            var res, deps;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, CocosHelper_1.default.loadResSync(url, type, this._addTmpAssetsDepends.bind(this))];
                    case 1:
                        res = _a.sent();
                        if (!res) {
                            this._clearTmpAssetsDepends();
                            return [2 /*return*/, null];
                        }
                        this._clearTmpAssetsDepends();
                        deps = cc.assetManager.dependUtil.getDepsRecursively(res['_uuid']) || [];
                        deps.push(res['_uuid']);
                        this.addAssetsDepends(deps);
                        return [2 /*return*/, {
                                res: res,
                                deps: deps
                            }];
                }
            });
        });
    };
    /** 更加引用销毁资源 */
    ResMgr.prototype._destoryResWithReference = function (deps) {
        var _toDeletes = this.removeAssetsDepends(deps);
        this._destoryAssets(_toDeletes);
        return true;
    };
    /** 添加资源计数 */
    ResMgr.prototype.addAssetsDepends = function (deps) {
        for (var _i = 0, deps_1 = deps; _i < deps_1.length; _i++) {
            var s = deps_1[_i];
            if (this._checkIsBuiltinAssets(s))
                continue;
            if (this._assetsReference[s]) {
                this._assetsReference[s] += 1;
            }
            else {
                this._assetsReference[s] = 1;
            }
        }
    };
    /** 删除资源计数 */
    ResMgr.prototype.removeAssetsDepends = function (deps) {
        var _deletes = [];
        for (var _i = 0, deps_2 = deps; _i < deps_2.length; _i++) {
            var s = deps_2[_i];
            if (!this._assetsReference[s] || this._assetsReference[s] === 0)
                continue;
            this._assetsReference[s]--;
            if (this._assetsReference[s] === 0) {
                _deletes.push(s);
                delete this._assetsReference[s]; // 删除key;
            }
        }
        return _deletes;
    };
    ResMgr.prototype._destoryAssets = function (urls) {
        for (var _i = 0, urls_1 = urls; _i < urls_1.length; _i++) {
            var url = urls_1[_i];
            this._destoryAsset(url);
        }
    };
    /** 销毁资源 */
    ResMgr.prototype._destoryAsset = function (url) {
        if (this._checkIsBuiltinAssets(url))
            return;
        cc.assetManager.assets.remove(url); // 从缓存中清除
        var asset = cc.assetManager.assets.get(url); // 销毁该资源
        asset && asset.destroy();
        cc.assetManager.dependUtil['remove'](url); // 从依赖中删除
    };
    /** 临时添加资源计数 */
    ResMgr.prototype._addTmpAssetsDepends = function (completedCount, totalCount, item) {
        var _a;
        var deps = (cc.assetManager.dependUtil.getDepsRecursively(item.uuid) || []);
        deps.push(item.uuid);
        this.addAssetsDepends(deps);
        (_a = this._tmpAssetsDepends).push.apply(_a, deps);
    };
    /** 删除临时添加的计数 */
    ResMgr.prototype._clearTmpAssetsDepends = function () {
        for (var _i = 0, _a = this._tmpAssetsDepends; _i < _a.length; _i++) {
            var s = _a[_i];
            if (!this._assetsReference[s] || this._assetsReference[s] === 0)
                continue;
            this._assetsReference[s]--;
            if (this._assetsReference[s] === 0) {
                delete this._assetsReference[s]; // 这里不清理缓存
            }
        }
        this._tmpAssetsDepends = [];
    };
    /** 检查是否是builtin内的资源 */
    ResMgr.prototype._checkIsBuiltinAssets = function (url) {
        var asset = cc.assetManager.assets.get(url);
        if (asset && asset['_name'].indexOf("builtin") != -1) {
            return true;
        }
        return false;
    };
    /** 计算当前纹理数量和缓存 */
    ResMgr.prototype.computeTextureCache = function () {
        var cache = cc.assetManager.assets;
        var totalTextureSize = 0;
        var count = 0;
        cache.forEach(function (item, key) {
            var type = (item && item['__classname__']) ? item['__classname__'] : '';
            if (type == 'cc.Texture2D') {
                var texture = item;
                var textureSize = texture.width * texture.height * ((texture['_native'] === '.jpg' ? 3 : 4) / 1024 / 1024);
                // debugger
                totalTextureSize += textureSize;
                count++;
            }
        });
        return "\u7F13\u5B58 [\u7EB9\u7406\u603B\u6570:" + count + "][\u7EB9\u7406\u7F13\u5B58:" + (totalTextureSize.toFixed(2) + 'M') + "]";
    };
    ResMgr.instance = null;
    return ResMgr;
}());
exports.default = ResMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9SZXNNZ3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxrREFBaUQ7QUFDakQsb0RBQStDO0FBRy9DOzs7Ozs7Ozs7Ozs7O0dBYUc7QUFDSDtJQUFBO1FBU0k7O1dBRUc7UUFDSyxtQkFBYyxHQUFxQyxFQUFFLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JFLGlCQUFZLEdBQXFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7UUFFbkUsc0JBQWlCLEdBQWEsRUFBRSxDQUFDLENBQXVDLE9BQU87UUFDL0UscUJBQWdCLEdBQThCLEVBQUUsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBVSxTQUFTO0lBZ0svRixDQUFDO0lBOUtHLHNCQUFrQixjQUFJO2FBQXRCO1lBQ0ksSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLElBQUksRUFBRTtnQkFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLE1BQU0sRUFBRSxDQUFDO2FBQ2hDO1lBQ0QsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pCLENBQUM7OztPQUFBO0lBWUQsV0FBVztJQUNFLHlCQUFRLEdBQXJCLFVBQXNCLEdBQVc7Ozs7OzRCQUNULHFCQUFNLElBQUksQ0FBQyxxQkFBcUIsQ0FBWSxHQUFHLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBM0UsS0FBZ0IsU0FBMkQsRUFBekUsR0FBRyxTQUFBLEVBQUUsSUFBSSxVQUFBO3dCQUNmLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO3dCQUNoQyxzQkFBTyxFQUFFLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDOzs7O0tBQzlCO0lBRUQsV0FBVztJQUNKLDRCQUFXLEdBQWxCLFVBQW1CLEdBQVc7UUFDMUIsSUFBSSxDQUFDLEdBQUc7WUFBRSxPQUFPO1FBQ2pCLHlCQUFXLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTNCLFVBQVU7UUFDVixJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUU1RCxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDcEMsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVwQyxTQUFTO1FBQ1QsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBR0QsZ0NBQWdDO0lBQ25CLCtCQUFjLEdBQTNCLFVBQStCLEdBQVcsRUFBRSxJQUFxQixFQUFFLEdBQVc7Ozs7Ozs0QkFDdEQscUJBQU0sSUFBSSxDQUFDLHFCQUFxQixDQUFJLEdBQUcsRUFBRSxJQUFJLENBQUMsRUFBQTs7d0JBQTlELEtBQWdCLFNBQThDLEVBQTVELEdBQUcsU0FBQSxFQUFFLElBQUksVUFBQTt3QkFDZixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsRUFBRTs0QkFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7eUJBQy9CO3dCQUNELENBQUEsS0FBQSxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFBLENBQUMsSUFBSSxXQUFJLElBQUksRUFBRTt3QkFDckMsc0JBQU8sR0FBRyxFQUFDOzs7O0tBQ2Q7SUFFRCxjQUFjO0lBQ1Asa0NBQWlCLEdBQXhCLFVBQXlCLEdBQVc7UUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBUSxLQUFLO1lBQ3RDLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtRQUVyRCxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUM5QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFOUIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUdELGtCQUFrQjtJQUNKLHNDQUFxQixHQUFuQyxVQUF1QyxHQUFXLEVBQUUsSUFBcUI7Ozs7OzRCQUMzRCxxQkFBTSxxQkFBVyxDQUFDLFdBQVcsQ0FBSSxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBQTs7d0JBQXZGLEdBQUcsR0FBRyxTQUFpRjt3QkFDM0YsSUFBSSxDQUFDLEdBQUcsRUFBRTs0QkFDTixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzs0QkFDOUIsc0JBQU8sSUFBSSxFQUFDO3lCQUNmO3dCQUNELElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO3dCQUMxQixJQUFJLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUM3RSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUN4QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBRTVCLHNCQUFPO2dDQUNILEdBQUcsRUFBRSxHQUFHO2dDQUNSLElBQUksRUFBRSxJQUFJOzZCQUNiLEVBQUM7Ozs7S0FDTDtJQUVELGVBQWU7SUFDUCx5Q0FBd0IsR0FBaEMsVUFBaUMsSUFBYztRQUMzQyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoQyxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsYUFBYTtJQUNMLGlDQUFnQixHQUF4QixVQUF5QixJQUFtQjtRQUN4QyxLQUFjLFVBQUksRUFBSixhQUFJLEVBQUosa0JBQUksRUFBSixJQUFJLEVBQUU7WUFBZixJQUFJLENBQUMsYUFBQTtZQUNOLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztnQkFBRSxTQUFTO1lBQzVDLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUMxQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2pDO2lCQUFNO2dCQUNILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDaEM7U0FDSjtJQUNMLENBQUM7SUFDRCxhQUFhO0lBQ0wsb0NBQW1CLEdBQTNCLFVBQTRCLElBQW1CO1FBQzNDLElBQUksUUFBUSxHQUFhLEVBQUUsQ0FBQztRQUM1QixLQUFjLFVBQUksRUFBSixhQUFJLEVBQUosa0JBQUksRUFBSixJQUFJLEVBQUU7WUFBZixJQUFJLENBQUMsYUFBQTtZQUNOLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQUUsU0FBUztZQUMxRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUMzQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2hDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQWtCLFNBQVM7YUFDOUQ7U0FDSjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFDTywrQkFBYyxHQUF0QixVQUF1QixJQUFjO1FBQ2pDLEtBQWtCLFVBQUksRUFBSixhQUFJLEVBQUosa0JBQUksRUFBSixJQUFJLEVBQUU7WUFBbkIsSUFBTSxHQUFHLGFBQUE7WUFDVixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzNCO0lBQ0wsQ0FBQztJQUNELFdBQVc7SUFDSCw4QkFBYSxHQUFyQixVQUFzQixHQUFXO1FBQzdCLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQztZQUFFLE9BQU87UUFDNUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQWUsU0FBUztRQUMzRCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBTSxRQUFRO1FBQzFELEtBQUssSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDekIsRUFBRSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBUSxTQUFTO0lBQy9ELENBQUM7SUFFRCxlQUFlO0lBQ1AscUNBQW9CLEdBQTVCLFVBQTZCLGNBQXNCLEVBQUUsVUFBa0IsRUFBRSxJQUFTOztRQUM5RSxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFNUIsQ0FBQSxLQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQSxDQUFDLElBQUksV0FBSSxJQUFJLEVBQUU7SUFDekMsQ0FBQztJQUNELGdCQUFnQjtJQUNSLHVDQUFzQixHQUE5QjtRQUNJLEtBQWMsVUFBc0IsRUFBdEIsS0FBQSxJQUFJLENBQUMsaUJBQWlCLEVBQXRCLGNBQXNCLEVBQXRCLElBQXNCLEVBQUU7WUFBakMsSUFBSSxDQUFDLFNBQUE7WUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUFFLFNBQVM7WUFDMUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDM0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNoQyxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFXLFVBQVU7YUFDeEQ7U0FDSjtRQUNELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELHVCQUF1QjtJQUNmLHNDQUFxQixHQUE3QixVQUE4QixHQUFXO1FBQ3JDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFO1lBQ2xELE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsa0JBQWtCO0lBQ1gsb0NBQW1CLEdBQTFCO1FBQ0ksSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7UUFDbkMsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7UUFDekIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQWMsRUFBRSxHQUFXO1lBQ3RDLElBQUksSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUN4RSxJQUFJLElBQUksSUFBSSxjQUFjLEVBQUU7Z0JBQ3hCLElBQUksT0FBTyxHQUFHLElBQW9CLENBQUM7Z0JBQ25DLElBQUksV0FBVyxHQUFHLE9BQU8sQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUM7Z0JBQzNHLFdBQVc7Z0JBQ1gsZ0JBQWdCLElBQUksV0FBVyxDQUFDO2dCQUNoQyxLQUFLLEVBQUUsQ0FBQzthQUNYO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLDRDQUFZLEtBQUssb0NBQVUsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsT0FBRyxDQUFDO0lBQzNFLENBQUM7SUE3S2MsZUFBUSxHQUFXLElBQUksQ0FBQztJQStLM0MsYUFBQztDQWhMRCxBQWdMQyxJQUFBO2tCQWhMb0IsTUFBTSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBVSUJhc2UgZnJvbSBcIi4uL0NvbW1vbi9VSUJhc2VcIjtcbmltcG9ydCB7IEV2ZW50Q2VudGVyIH0gZnJvbSBcIi4uL05ldC9FdmVudENlbnRlclwiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuXG5cbi8qKlxuICog6LWE5rqQ5Yqg6L29LCDpkojlr7nnmoTmmK9Gb3JtXG4gKiDpppblhYjlsIbotYTmupDliIbkuLrkuKTnsbtcbiAqIOS4gOenjeaYr+WcqOe8lui+keWZqOaXtuWwhuWFtuaLluS4iuWOu+WbvueJhywg6L+Z6YeM5bCG5YW256ew5Li66Z2Z5oCB5Zu+54mHLCBcbiAqIOS4gOenjeaYr+WcqOS7o+eggeS4reS9v+eUqGNjLmxvYWRlcuWKoOi9veeahOWbvueJhywg6L+Z6YeM5bCG5YW256ew5Li65Yqo5oCB5Zu+54mHXG4gKiBcbiAqIOWvueS6jumdmeaAgei1hOa6kFxuICogMSwg5Yqg6L29ICDlnKjliqDovb1wcmVmYWLml7YsIGNvY29z5Lya5bCG5YW25L6d6LWW55qE5Zu+54mH5LiA5bm25Yqg6L29LCDmiYDmnInkuI3pnIDopoHmiJHku6zmi4Xlv4NcbiAqIDIsIOmHiuaUviAg6L+Z6YeM6YeH55So55qE5byV55So6K6h5pWw55qE566h55CG5pa55rOVLCDlj6rpnIDopoHosIPnlKhkZXN0b3J5Rm9ybeWNs+WPr1xuICogXG4gKiDliqDovb3kuIDkuKrnqpfkvZNcbiAqIOesrOS4gOatpSDliqDovb1wcmVmYWIsIOesrOS6jOatpSDlrp7kvovljJZwcmVmYWIg6I635b6XIG5vZGVcbiAqIOaJgOS7pemUgOavgeS4gOS4queql+S9kyDkuZ/pnIDopoHkuKTmraUsIOmUgOavgW5vZGUsIOmUgOavgXByZWZhYlxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXNNZ3Ige1xuICAgIHByaXZhdGUgc3RhdGljIGluc3RhbmNlOiBSZXNNZ3IgPSBudWxsO1xuICAgIHB1YmxpYyBzdGF0aWMgZ2V0IGluc3QoKSB7XG4gICAgICAgIGlmICh0aGlzLmluc3RhbmNlID09PSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLmluc3RhbmNlID0gbmV3IFJlc01ncigpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLmluc3RhbmNlO1xuICAgIH1cblxuICAgIC8qKiBcbiAgICAgKiDph4fnlKjorqHmlbDnrqHnkIbnmoTlip7ms5UsIOeuoeeQhmZvcm3miYDkvp3otZbnmoTotYTmupBcbiAgICAgKi9cbiAgICBwcml2YXRlIF9wcmVmYWJEZXBlbmRzOiB7IFtrZXk6IHN0cmluZ106IEFycmF5PHN0cmluZz4gfSA9IGNjLmpzLmNyZWF0ZU1hcCgpO1xuICAgIHByaXZhdGUgX2R5bmFtaWNUYWdzOiB7IFtrZXk6IHN0cmluZ106IEFycmF5PHN0cmluZz4gfSA9IGNjLmpzLmNyZWF0ZU1hcCgpO1xuXG4gICAgcHJpdmF0ZSBfdG1wQXNzZXRzRGVwZW5kczogc3RyaW5nW10gPSBbXTsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyDkuLTml7bnvJPlrZhcbiAgICBwcml2YXRlIF9hc3NldHNSZWZlcmVuY2U6IHsgW2tleTogc3RyaW5nXTogbnVtYmVyIH0gPSBjYy5qcy5jcmVhdGVNYXAoKTsgICAgICAgICAgLy8g6LWE5rqQ5byV55So6K6h5pWwXG5cblxuICAgIC8qKiDliqDovb3nqpfkvZMgKi9cbiAgICBwdWJsaWMgYXN5bmMgbG9hZEZvcm0oZmlkOiBzdHJpbmcpIHtcbiAgICAgICAgbGV0IHsgcmVzLCBkZXBzIH0gPSBhd2FpdCB0aGlzLl9sb2FkUmVzV2l0aFJlZmVyZW5jZTxjYy5QcmVmYWI+KGZpZCwgY2MuUHJlZmFiKTtcbiAgICAgICAgdGhpcy5fcHJlZmFiRGVwZW5kc1tmaWRdID0gZGVwcztcbiAgICAgICAgcmV0dXJuIGNjLmluc3RhbnRpYXRlKHJlcyk7XG4gICAgfVxuXG4gICAgLyoqIOmUgOavgeeql+S9kyAqL1xuICAgIHB1YmxpYyBkZXN0b3J5Rm9ybShjb206IFVJQmFzZSkge1xuICAgICAgICBpZiAoIWNvbSkgcmV0dXJuO1xuICAgICAgICBFdmVudENlbnRlci50YXJnZXRPZmYoY29tKTtcblxuICAgICAgICAvLyDplIDmr4Hkvp3otZbnmoTotYTmupBcbiAgICAgICAgdGhpcy5fZGVzdG9yeVJlc1dpdGhSZWZlcmVuY2UodGhpcy5fcHJlZmFiRGVwZW5kc1tjb20uZmlkXSk7XG5cbiAgICAgICAgdGhpcy5fcHJlZmFiRGVwZW5kc1tjb20uZmlkXSA9IG51bGw7XG4gICAgICAgIGRlbGV0ZSB0aGlzLl9wcmVmYWJEZXBlbmRzW2NvbS5maWRdO1xuXG4gICAgICAgIC8vIOmUgOavgW5vZGVcbiAgICAgICAgY29tLm5vZGUuZGVzdHJveSgpO1xuICAgIH1cblxuXG4gICAgLyoqIOWKqOaAgei1hOa6kOeuoeeQhiwg6YCa6L+HdGFn5qCH6K6w5b2T5YmN6LWE5rqQLCDnu5/kuIDph4rmlL4gKi9cbiAgICBwdWJsaWMgYXN5bmMgbG9hZER5bmFtaWNSZXM8VD4odXJsOiBzdHJpbmcsIHR5cGU6IHR5cGVvZiBjYy5Bc3NldCwgdGFnOiBzdHJpbmcpIHtcbiAgICAgICAgbGV0IHsgcmVzLCBkZXBzIH0gPSBhd2FpdCB0aGlzLl9sb2FkUmVzV2l0aFJlZmVyZW5jZTxUPih1cmwsIHR5cGUpO1xuICAgICAgICBpZiAoIXRoaXMuX2R5bmFtaWNUYWdzW3RhZ10pIHtcbiAgICAgICAgICAgIHRoaXMuX2R5bmFtaWNUYWdzW3RhZ10gPSBbXTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9keW5hbWljVGFnc1t0YWddLnB1c2goLi4uZGVwcyk7XG4gICAgICAgIHJldHVybiByZXM7XG4gICAgfVxuXG4gICAgLyoqIOmUgOavgeWKqOaAgei1hOa6kCAgKi9cbiAgICBwdWJsaWMgZGVzdG9yeUR5bmFtaWNSZXModGFnOiBzdHJpbmcpIHtcbiAgICAgICAgaWYgKCF0aGlzLl9keW5hbWljVGFnc1t0YWddKSB7ICAgICAgIC8vIOmUgOavgVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2Rlc3RvcnlSZXNXaXRoUmVmZXJlbmNlKHRoaXMuX2R5bmFtaWNUYWdzW3RhZ10pXG5cbiAgICAgICAgdGhpcy5fZHluYW1pY1RhZ3NbdGFnXSA9IG51bGw7XG4gICAgICAgIGRlbGV0ZSB0aGlzLl9keW5hbWljVGFnc1t0YWddO1xuXG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuXG4gICAgLyoqIOWKoOi9vei1hOa6kOW5tua3u+WKoOW8leeUqOiuoeaVsCAqL1xuICAgIHByaXZhdGUgYXN5bmMgX2xvYWRSZXNXaXRoUmVmZXJlbmNlPFQ+KHVybDogc3RyaW5nLCB0eXBlOiB0eXBlb2YgY2MuQXNzZXQpIHtcbiAgICAgICAgbGV0IHJlcyA9IGF3YWl0IENvY29zSGVscGVyLmxvYWRSZXNTeW5jPFQ+KHVybCwgdHlwZSwgdGhpcy5fYWRkVG1wQXNzZXRzRGVwZW5kcy5iaW5kKHRoaXMpKTtcbiAgICAgICAgaWYgKCFyZXMpIHtcbiAgICAgICAgICAgIHRoaXMuX2NsZWFyVG1wQXNzZXRzRGVwZW5kcygpO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5fY2xlYXJUbXBBc3NldHNEZXBlbmRzKCk7XG4gICAgICAgIGxldCBkZXBzID0gY2MuYXNzZXRNYW5hZ2VyLmRlcGVuZFV0aWwuZ2V0RGVwc1JlY3Vyc2l2ZWx5KHJlc1snX3V1aWQnXSkgfHwgW107XG4gICAgICAgIGRlcHMucHVzaChyZXNbJ191dWlkJ10pO1xuICAgICAgICB0aGlzLmFkZEFzc2V0c0RlcGVuZHMoZGVwcyk7XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlczogcmVzLFxuICAgICAgICAgICAgZGVwczogZGVwc1xuICAgICAgICB9O1xuICAgIH1cblxuICAgIC8qKiDmm7TliqDlvJXnlKjplIDmr4HotYTmupAgKi9cbiAgICBwcml2YXRlIF9kZXN0b3J5UmVzV2l0aFJlZmVyZW5jZShkZXBzOiBzdHJpbmdbXSkge1xuICAgICAgICBsZXQgX3RvRGVsZXRlcyA9IHRoaXMucmVtb3ZlQXNzZXRzRGVwZW5kcyhkZXBzKTtcbiAgICAgICAgdGhpcy5fZGVzdG9yeUFzc2V0cyhfdG9EZWxldGVzKTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgLyoqIOa3u+WKoOi1hOa6kOiuoeaVsCAqL1xuICAgIHByaXZhdGUgYWRkQXNzZXRzRGVwZW5kcyhkZXBzOiBBcnJheTxzdHJpbmc+KSB7XG4gICAgICAgIGZvciAobGV0IHMgb2YgZGVwcykge1xuICAgICAgICAgICAgaWYgKHRoaXMuX2NoZWNrSXNCdWlsdGluQXNzZXRzKHMpKSBjb250aW51ZTtcbiAgICAgICAgICAgIGlmICh0aGlzLl9hc3NldHNSZWZlcmVuY2Vbc10pIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9hc3NldHNSZWZlcmVuY2Vbc10gKz0gMTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fYXNzZXRzUmVmZXJlbmNlW3NdID0gMTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICAvKiog5Yig6Zmk6LWE5rqQ6K6h5pWwICovXG4gICAgcHJpdmF0ZSByZW1vdmVBc3NldHNEZXBlbmRzKGRlcHM6IEFycmF5PHN0cmluZz4pIHtcbiAgICAgICAgbGV0IF9kZWxldGVzOiBzdHJpbmdbXSA9IFtdO1xuICAgICAgICBmb3IgKGxldCBzIG9mIGRlcHMpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5fYXNzZXRzUmVmZXJlbmNlW3NdIHx8IHRoaXMuX2Fzc2V0c1JlZmVyZW5jZVtzXSA9PT0gMCkgY29udGludWU7XG4gICAgICAgICAgICB0aGlzLl9hc3NldHNSZWZlcmVuY2Vbc10tLTtcbiAgICAgICAgICAgIGlmICh0aGlzLl9hc3NldHNSZWZlcmVuY2Vbc10gPT09IDApIHtcbiAgICAgICAgICAgICAgICBfZGVsZXRlcy5wdXNoKHMpO1xuICAgICAgICAgICAgICAgIGRlbGV0ZSB0aGlzLl9hc3NldHNSZWZlcmVuY2Vbc107ICAgICAgICAgICAgICAgICAgLy8g5Yig6Zmka2V5O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBfZGVsZXRlcztcbiAgICB9XG4gICAgcHJpdmF0ZSBfZGVzdG9yeUFzc2V0cyh1cmxzOiBzdHJpbmdbXSkge1xuICAgICAgICBmb3IgKGNvbnN0IHVybCBvZiB1cmxzKSB7XG4gICAgICAgICAgICB0aGlzLl9kZXN0b3J5QXNzZXQodXJsKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICAvKiog6ZSA5q+B6LWE5rqQICovXG4gICAgcHJpdmF0ZSBfZGVzdG9yeUFzc2V0KHVybDogc3RyaW5nKSB7XG4gICAgICAgIGlmICh0aGlzLl9jaGVja0lzQnVpbHRpbkFzc2V0cyh1cmwpKSByZXR1cm47XG4gICAgICAgIGNjLmFzc2V0TWFuYWdlci5hc3NldHMucmVtb3ZlKHVybCk7ICAgICAgICAgICAgICAgLy8g5LuO57yT5a2Y5Lit5riF6ZmkXG4gICAgICAgIGxldCBhc3NldCA9IGNjLmFzc2V0TWFuYWdlci5hc3NldHMuZ2V0KHVybCk7ICAgICAgLy8g6ZSA5q+B6K+l6LWE5rqQXG4gICAgICAgIGFzc2V0ICYmIGFzc2V0LmRlc3Ryb3koKTtcbiAgICAgICAgY2MuYXNzZXRNYW5hZ2VyLmRlcGVuZFV0aWxbJ3JlbW92ZSddKHVybCk7ICAgICAgICAvLyDku47kvp3otZbkuK3liKDpmaRcbiAgICB9XG5cbiAgICAvKiog5Li05pe25re75Yqg6LWE5rqQ6K6h5pWwICovXG4gICAgcHJpdmF0ZSBfYWRkVG1wQXNzZXRzRGVwZW5kcyhjb21wbGV0ZWRDb3VudDogbnVtYmVyLCB0b3RhbENvdW50OiBudW1iZXIsIGl0ZW06IGFueSkge1xuICAgICAgICBsZXQgZGVwcyA9IChjYy5hc3NldE1hbmFnZXIuZGVwZW5kVXRpbC5nZXREZXBzUmVjdXJzaXZlbHkoaXRlbS51dWlkKSB8fCBbXSk7XG4gICAgICAgIGRlcHMucHVzaChpdGVtLnV1aWQpO1xuICAgICAgICB0aGlzLmFkZEFzc2V0c0RlcGVuZHMoZGVwcyk7XG5cbiAgICAgICAgdGhpcy5fdG1wQXNzZXRzRGVwZW5kcy5wdXNoKC4uLmRlcHMpO1xuICAgIH1cbiAgICAvKiog5Yig6Zmk5Li05pe25re75Yqg55qE6K6h5pWwICovXG4gICAgcHJpdmF0ZSBfY2xlYXJUbXBBc3NldHNEZXBlbmRzKCkge1xuICAgICAgICBmb3IgKGxldCBzIG9mIHRoaXMuX3RtcEFzc2V0c0RlcGVuZHMpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5fYXNzZXRzUmVmZXJlbmNlW3NdIHx8IHRoaXMuX2Fzc2V0c1JlZmVyZW5jZVtzXSA9PT0gMCkgY29udGludWU7XG4gICAgICAgICAgICB0aGlzLl9hc3NldHNSZWZlcmVuY2Vbc10tLTtcbiAgICAgICAgICAgIGlmICh0aGlzLl9hc3NldHNSZWZlcmVuY2Vbc10gPT09IDApIHtcbiAgICAgICAgICAgICAgICBkZWxldGUgdGhpcy5fYXNzZXRzUmVmZXJlbmNlW3NdOyAgICAgICAgICAgLy8g6L+Z6YeM5LiN5riF55CG57yT5a2YXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5fdG1wQXNzZXRzRGVwZW5kcyA9IFtdO1xuICAgIH1cblxuICAgIC8qKiDmo4Dmn6XmmK/lkKbmmK9idWlsdGlu5YaF55qE6LWE5rqQICovXG4gICAgcHJpdmF0ZSBfY2hlY2tJc0J1aWx0aW5Bc3NldHModXJsOiBzdHJpbmcpIHtcbiAgICAgICAgbGV0IGFzc2V0ID0gY2MuYXNzZXRNYW5hZ2VyLmFzc2V0cy5nZXQodXJsKTtcbiAgICAgICAgaWYgKGFzc2V0ICYmIGFzc2V0WydfbmFtZSddLmluZGV4T2YoXCJidWlsdGluXCIpICE9IC0xKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgLyoqIOiuoeeul+W9k+WJjee6ueeQhuaVsOmHj+WSjOe8k+WtmCAqL1xuICAgIHB1YmxpYyBjb21wdXRlVGV4dHVyZUNhY2hlKCkge1xuICAgICAgICBsZXQgY2FjaGUgPSBjYy5hc3NldE1hbmFnZXIuYXNzZXRzO1xuICAgICAgICBsZXQgdG90YWxUZXh0dXJlU2l6ZSA9IDA7XG4gICAgICAgIGxldCBjb3VudCA9IDA7XG4gICAgICAgIGNhY2hlLmZvckVhY2goKGl0ZW06IGNjLkFzc2V0LCBrZXk6IHN0cmluZykgPT4ge1xuICAgICAgICAgICAgbGV0IHR5cGUgPSAoaXRlbSAmJiBpdGVtWydfX2NsYXNzbmFtZV9fJ10pID8gaXRlbVsnX19jbGFzc25hbWVfXyddIDogJyc7XG4gICAgICAgICAgICBpZiAodHlwZSA9PSAnY2MuVGV4dHVyZTJEJykge1xuICAgICAgICAgICAgICAgIGxldCB0ZXh0dXJlID0gaXRlbSBhcyBjYy5UZXh0dXJlMkQ7XG4gICAgICAgICAgICAgICAgbGV0IHRleHR1cmVTaXplID0gdGV4dHVyZS53aWR0aCAqIHRleHR1cmUuaGVpZ2h0ICogKCh0ZXh0dXJlWydfbmF0aXZlJ10gPT09ICcuanBnJyA/IDMgOiA0KSAvIDEwMjQgLyAxMDI0KTtcbiAgICAgICAgICAgICAgICAvLyBkZWJ1Z2dlclxuICAgICAgICAgICAgICAgIHRvdGFsVGV4dHVyZVNpemUgKz0gdGV4dHVyZVNpemU7XG4gICAgICAgICAgICAgICAgY291bnQrKztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBg57yT5a2YIFvnurnnkIbmgLvmlbA6JHtjb3VudH1dW+e6ueeQhue8k+WtmDoke3RvdGFsVGV4dHVyZVNpemUudG9GaXhlZCgyKSArICdNJ31dYDtcbiAgICB9XG5cbn0iXX0=