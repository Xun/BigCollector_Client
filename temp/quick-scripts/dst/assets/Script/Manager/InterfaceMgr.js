
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/InterfaceMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4a5c15ZXLJH9J7cFLh3WcX6', 'InterfaceMgr');
// Script/Manager/InterfaceMgr.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9JbnRlcmZhY2VNZ3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgVXNlckluZm9zIHtcbiAgICBhY2NvdW50OiBzdHJpbmc7XG4gICAgbmlja25hbWU6IHN0cmluZztcbiAgICBhdmF0YXJVcmw6IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBXb3JrQmVuY2gge1xuICAgIGxpc3Rfd29ya2JlbmNoOiBBcnJheTxudW1iZXI+O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElpZW1Mb3R0ZXJ5IHtcbiAgICBudW06IHN0cmluZztcbiAgICBpY29uOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVJhbmtJdGVtIHtcbiAgICBsZXZlbDogbnVtYmVyO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICBoZWFkPzogc3RyaW5nO1xuICAgIG51bTogbnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElTaG9wSXRlbSB7XG4gICAgbmFtZTogc3RyaW5nO1xuICAgIGl0ZW1faWNvbjogc3RyaW5nO1xuICAgIGJ1eV9pY29uOiBzdHJpbmc7XG4gICAgYnV5X251bTogbnVtYmVyO1xufSJdfQ==