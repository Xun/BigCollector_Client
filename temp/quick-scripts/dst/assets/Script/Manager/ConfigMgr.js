
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/ConfigMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '99faeZt2e5AobpvQuyUBgaI', 'ConfigMgr');
// Script/Manager/ConfigMgr.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../Utils/CocosHelper");
var GameMgr_1 = require("./GameMgr");
var ConfigMgr = /** @class */ (function () {
    function ConfigMgr() {
    }
    /** 加载配置文件 */
    ConfigMgr.prototype.loadConfigs = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = GameMgr_1.default.dataModalMgr.ItemProfucesJson;
                        return [4 /*yield*/, CocosHelper_1.default.loadJsonSync("JsonFile/items", cc.JsonAsset)];
                    case 1:
                        _a.itemsInfo = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return ConfigMgr;
}());
exports.default = ConfigMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9Db25maWdNZ3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxvREFBK0M7QUFDL0MscUNBQWdDO0FBRWhDO0lBQUE7SUFPQSxDQUFDO0lBTkcsYUFBYTtJQUNQLCtCQUFXLEdBQWpCOzs7Ozs7d0JBQ0ksS0FBQSxpQkFBTyxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQTt3QkFBYSxxQkFBTSxxQkFBVyxDQUFDLFlBQVksQ0FBZSxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUE7O3dCQUE5SCxHQUFzQyxTQUFTLEdBQUcsU0FBNEUsQ0FBQzs7Ozs7S0FHbEk7SUFDTCxnQkFBQztBQUFELENBUEEsQUFPQyxJQUFBIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4vR2FtZU1nclwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDb25maWdNZ3Ige1xuICAgIC8qKiDliqDovb3phY3nva7mlofku7YgKi9cbiAgICBhc3luYyBsb2FkQ29uZmlncygpIHtcbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuSXRlbVByb2Z1Y2VzSnNvbi5pdGVtc0luZm8gPSBhd2FpdCBDb2Nvc0hlbHBlci5sb2FkSnNvblN5bmM8Y2MuSnNvbkFzc2V0PihcIkpzb25GaWxlL2l0ZW1zXCIsIGNjLkpzb25Bc3NldCk7XG4gICAgICAgIC8vIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkNoaXBzSW5mb0pzb24uY2hpcEluZm8gPSBhd2FpdCBDb2Nvc0hlbHBlci5sb2FkSnNvblN5bmM8Y2MuSnNvbkFzc2V0PihcIkpzb25GaWxlL2NoaXBzXCIsIGNjLkpzb25Bc3NldCk7XG4gICAgICAgIC8vIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLlNob3BJbmZvSnNvbi5zaG9wSW5mbyA9IGF3YWl0IENvY29zSGVscGVyLmxvYWRKc29uU3luYzxjYy5Kc29uQXNzZXQ+KFwiSnNvbkZpbGUvc2hvcFwiLCBjYy5Kc29uQXNzZXQpO1xuICAgIH1cbn0iXX0=