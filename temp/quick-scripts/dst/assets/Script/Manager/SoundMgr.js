
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/SoundMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '712729Tx0JBoI/gunNGv3kP', 'SoundMgr');
// Script/Manager/SoundMgr.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var SysDefine_1 = require("../Common/SysDefine");
var CocosHelper_1 = require("../Utils/CocosHelper");
var LocalStorageMgr_1 = require("./LocalStorageMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SoundMgr = /** @class */ (function (_super) {
    __extends(SoundMgr, _super);
    function SoundMgr() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.audioCache = cc.js.createMap();
        _this.currEffectId = -1;
        _this.currMusicId = -1;
        /** volume */
        _this.volume = new Volume();
        return _this;
        // update (dt) {}
    }
    Object.defineProperty(SoundMgr, "inst", {
        get: function () {
            if (this._inst == null) {
                this._inst = cc.find(SysDefine_1.SysDefine.SYS_UIROOT_NAME).addComponent(this);
            }
            return this._inst;
        },
        enumerable: false,
        configurable: true
    });
    SoundMgr.prototype.onLoad = function () {
        var volume = this.getVolumeToLocal();
        if (volume) {
            this.volume = volume;
        }
        else {
            this.volume.musicVolume = 1;
            this.volume.effectVolume = 1;
        }
        this.setVolumeToLocal();
        cc.game.on(cc.game.EVENT_HIDE, function () {
            cc.audioEngine.pauseAll();
        }, this);
        cc.game.on(cc.game.EVENT_SHOW, function () {
            cc.audioEngine.resumeAll();
        }, this);
    };
    SoundMgr.prototype.getVolume = function () {
        return this.volume;
    };
    SoundMgr.prototype.start = function () {
    };
    /**  */
    SoundMgr.prototype.setMusicVolume = function (musicVolume) {
        this.volume.musicVolume = musicVolume;
        this.setVolumeToLocal();
    };
    SoundMgr.prototype.setEffectVolume = function (effectVolume) {
        this.volume.effectVolume = effectVolume;
        this.setVolumeToLocal();
    };
    /** 播放背景音乐 */
    SoundMgr.prototype.playMusic = function (url, loop) {
        if (loop === void 0) { loop = true; }
        return __awaiter(this, void 0, void 0, function () {
            var sound;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!url || url === '')
                            return [2 /*return*/];
                        if (this.audioCache[url]) {
                            cc.audioEngine.playMusic(this.audioCache[url], loop);
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, CocosHelper_1.default.loadResSync(url, cc.AudioClip)];
                    case 1:
                        sound = _a.sent();
                        this.audioCache[url] = sound;
                        this.currMusicId = cc.audioEngine.playMusic(sound, loop);
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 播放音效 */
    SoundMgr.prototype.playEffect = function (url, loop) {
        if (loop === void 0) { loop = false; }
        return __awaiter(this, void 0, void 0, function () {
            var sound;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!url || url === '')
                            return [2 /*return*/];
                        if (this.audioCache[url]) {
                            cc.audioEngine.playEffect(this.audioCache[url], loop);
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, CocosHelper_1.default.loadResSync(url, cc.AudioClip)];
                    case 1:
                        sound = _a.sent();
                        this.audioCache[url] = sound;
                        this.currEffectId = cc.audioEngine.playEffect(sound, loop);
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 从本地读取 */
    SoundMgr.prototype.getVolumeToLocal = function () {
        var objStr = LocalStorageMgr_1.default.getItem("Volume");
        if (!objStr) {
            return null;
        }
        return objStr;
    };
    /** 设置音量 */
    SoundMgr.prototype.setVolumeToLocal = function () {
        cc.audioEngine.setMusicVolume(this.volume.musicVolume);
        cc.audioEngine.setEffectsVolume(this.volume.effectVolume);
        LocalStorageMgr_1.default.setItem("Volume", this.volume);
    };
    SoundMgr.prototype.setEffectActive = function (active, id) {
        if (id === void 0) { id = -1; }
        if (active) {
            cc.audioEngine.stop(id < 0 ? this.currEffectId : id);
        }
        else {
            cc.audioEngine.resume(id < 0 ? this.currEffectId : id);
        }
    };
    SoundMgr._inst = null; // 单例
    SoundMgr = __decorate([
        ccclass
    ], SoundMgr);
    return SoundMgr;
}(cc.Component));
exports.default = SoundMgr;
var Volume = /** @class */ (function () {
    function Volume() {
    }
    return Volume;
}());

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9Tb3VuZE1nci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxpREFBZ0Q7QUFDaEQsb0RBQStDO0FBQy9DLHFEQUFnRDtBQUUxQyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFzQyw0QkFBWTtJQUFsRDtRQUFBLHFFQW1HQztRQWpHVyxnQkFBVSxHQUFvQyxFQUFFLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBVWhFLGtCQUFZLEdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDMUIsaUJBQVcsR0FBVyxDQUFDLENBQUMsQ0FBQztRQW1CakMsYUFBYTtRQUNMLFlBQU0sR0FBVyxJQUFJLE1BQU0sRUFBRSxDQUFDOztRQWlFdEMsaUJBQWlCO0lBQ3JCLENBQUM7SUE5Rkcsc0JBQWtCLGdCQUFJO2FBQXRCO1lBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksRUFBRTtnQkFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLHFCQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsWUFBWSxDQUFXLElBQUksQ0FBQyxDQUFDO2FBQ2hGO1lBQ0QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3RCLENBQUM7OztPQUFBO0lBS0QseUJBQU0sR0FBTjtRQUNJLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3JDLElBQUksTUFBTSxFQUFFO1lBQ1IsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDeEI7YUFBTTtZQUNILElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7U0FDaEM7UUFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUV4QixFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUMzQixFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzlCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNULEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQzNCLEVBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDL0IsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUdELDRCQUFTLEdBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVELHdCQUFLLEdBQUw7SUFFQSxDQUFDO0lBQ0QsT0FBTztJQUNBLGlDQUFjLEdBQXJCLFVBQXNCLFdBQW1CO1FBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUN0QyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBQ00sa0NBQWUsR0FBdEIsVUFBdUIsWUFBb0I7UUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFDRCxhQUFhO0lBQ0EsNEJBQVMsR0FBdEIsVUFBdUIsR0FBVyxFQUFFLElBQVc7UUFBWCxxQkFBQSxFQUFBLFdBQVc7Ozs7Ozt3QkFDM0MsSUFBSSxDQUFDLEdBQUcsSUFBSSxHQUFHLEtBQUssRUFBRTs0QkFBRSxzQkFBTzt3QkFFL0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFOzRCQUN0QixFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDOzRCQUNyRCxzQkFBTzt5QkFDVjt3QkFDVyxxQkFBTSxxQkFBVyxDQUFDLFdBQVcsQ0FBZSxHQUFHLEVBQUUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxFQUFBOzt3QkFBdEUsS0FBSyxHQUFHLFNBQThEO3dCQUMxRSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQzt3QkFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7Ozs7O0tBQzVEO0lBQ0QsV0FBVztJQUNFLDZCQUFVLEdBQXZCLFVBQXdCLEdBQVcsRUFBRSxJQUFZO1FBQVoscUJBQUEsRUFBQSxZQUFZOzs7Ozs7d0JBQzdDLElBQUksQ0FBQyxHQUFHLElBQUksR0FBRyxLQUFLLEVBQUU7NEJBQUUsc0JBQU87d0JBRS9CLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTs0QkFDdEIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzs0QkFDdEQsc0JBQU87eUJBQ1Y7d0JBQ1cscUJBQU0scUJBQVcsQ0FBQyxXQUFXLENBQWUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBQTs7d0JBQXRFLEtBQUssR0FBRyxTQUE4RDt3QkFDMUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUM7d0JBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDOzs7OztLQUM5RDtJQUVELFlBQVk7SUFDTCxtQ0FBZ0IsR0FBdkI7UUFDSSxJQUFJLE1BQU0sR0FBRyx5QkFBZSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUM5QyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ1QsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFDRCxXQUFXO0lBQ0gsbUNBQWdCLEdBQXhCO1FBQ0ksRUFBRSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN2RCxFQUFFLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDMUQseUJBQWUsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRU0sa0NBQWUsR0FBdEIsVUFBdUIsTUFBZSxFQUFFLEVBQWU7UUFBZixtQkFBQSxFQUFBLE1BQWMsQ0FBQztRQUNuRCxJQUFJLE1BQU0sRUFBRTtZQUNSLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3hEO2FBQU07WUFDSCxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUMxRDtJQUNMLENBQUM7SUE1RmMsY0FBSyxHQUFhLElBQUksQ0FBQyxDQUFxQixLQUFLO0lBSi9DLFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0FtRzVCO0lBQUQsZUFBQztDQW5HRCxBQW1HQyxDQW5HcUMsRUFBRSxDQUFDLFNBQVMsR0FtR2pEO2tCQW5Hb0IsUUFBUTtBQXFHN0I7SUFBQTtJQUdBLENBQUM7SUFBRCxhQUFDO0FBQUQsQ0FIQSxBQUdDLElBQUEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTeXNEZWZpbmUgfSBmcm9tIFwiLi4vQ29tbW9uL1N5c0RlZmluZVwiO1xuaW1wb3J0IENvY29zSGVscGVyIGZyb20gXCIuLi9VdGlscy9Db2Nvc0hlbHBlclwiO1xuaW1wb3J0IExvY2FsU3RvcmFnZU1nciBmcm9tIFwiLi9Mb2NhbFN0b3JhZ2VNZ3JcIjtcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNvdW5kTWdyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIHByaXZhdGUgYXVkaW9DYWNoZTogeyBba2V5OiBzdHJpbmddOiBjYy5BdWRpb0NsaXAgfSA9IGNjLmpzLmNyZWF0ZU1hcCgpO1xuXG4gICAgcHJpdmF0ZSBzdGF0aWMgX2luc3Q6IFNvdW5kTWdyID0gbnVsbDsgICAgICAgICAgICAgICAgICAgICAvLyDljZXkvotcbiAgICBwdWJsaWMgc3RhdGljIGdldCBpbnN0KCk6IFNvdW5kTWdyIHtcbiAgICAgICAgaWYgKHRoaXMuX2luc3QgPT0gbnVsbCkge1xuICAgICAgICAgICAgdGhpcy5faW5zdCA9IGNjLmZpbmQoU3lzRGVmaW5lLlNZU19VSVJPT1RfTkFNRSkuYWRkQ29tcG9uZW50PFNvdW5kTWdyPih0aGlzKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5faW5zdDtcbiAgICB9XG5cbiAgICBwcml2YXRlIGN1cnJFZmZlY3RJZDogbnVtYmVyID0gLTE7XG4gICAgcHJpdmF0ZSBjdXJyTXVzaWNJZDogbnVtYmVyID0gLTE7XG5cbiAgICBvbkxvYWQoKSB7XG4gICAgICAgIGxldCB2b2x1bWUgPSB0aGlzLmdldFZvbHVtZVRvTG9jYWwoKTtcbiAgICAgICAgaWYgKHZvbHVtZSkge1xuICAgICAgICAgICAgdGhpcy52b2x1bWUgPSB2b2x1bWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnZvbHVtZS5tdXNpY1ZvbHVtZSA9IDE7XG4gICAgICAgICAgICB0aGlzLnZvbHVtZS5lZmZlY3RWb2x1bWUgPSAxO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2V0Vm9sdW1lVG9Mb2NhbCgpO1xuXG4gICAgICAgIGNjLmdhbWUub24oY2MuZ2FtZS5FVkVOVF9ISURFLCAoKSA9PiB7XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wYXVzZUFsbCgpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgICAgY2MuZ2FtZS5vbihjYy5nYW1lLkVWRU5UX1NIT1csICgpID0+IHtcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnJlc3VtZUFsbCgpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICB9XG4gICAgLyoqIHZvbHVtZSAqL1xuICAgIHByaXZhdGUgdm9sdW1lOiBWb2x1bWUgPSBuZXcgVm9sdW1lKCk7XG4gICAgZ2V0Vm9sdW1lKCkge1xuICAgICAgICByZXR1cm4gdGhpcy52b2x1bWU7XG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG5cbiAgICB9XG4gICAgLyoqICAqL1xuICAgIHB1YmxpYyBzZXRNdXNpY1ZvbHVtZShtdXNpY1ZvbHVtZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMudm9sdW1lLm11c2ljVm9sdW1lID0gbXVzaWNWb2x1bWU7XG4gICAgICAgIHRoaXMuc2V0Vm9sdW1lVG9Mb2NhbCgpO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0RWZmZWN0Vm9sdW1lKGVmZmVjdFZvbHVtZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMudm9sdW1lLmVmZmVjdFZvbHVtZSA9IGVmZmVjdFZvbHVtZTtcbiAgICAgICAgdGhpcy5zZXRWb2x1bWVUb0xvY2FsKCk7XG4gICAgfVxuICAgIC8qKiDmkq3mlL7og4zmma/pn7PkuZAgKi9cbiAgICBwdWJsaWMgYXN5bmMgcGxheU11c2ljKHVybDogc3RyaW5nLCBsb29wID0gdHJ1ZSkge1xuICAgICAgICBpZiAoIXVybCB8fCB1cmwgPT09ICcnKSByZXR1cm47XG5cbiAgICAgICAgaWYgKHRoaXMuYXVkaW9DYWNoZVt1cmxdKSB7XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5TXVzaWModGhpcy5hdWRpb0NhY2hlW3VybF0sIGxvb3ApO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGxldCBzb3VuZCA9IGF3YWl0IENvY29zSGVscGVyLmxvYWRSZXNTeW5jPGNjLkF1ZGlvQ2xpcD4odXJsLCBjYy5BdWRpb0NsaXApO1xuICAgICAgICB0aGlzLmF1ZGlvQ2FjaGVbdXJsXSA9IHNvdW5kO1xuICAgICAgICB0aGlzLmN1cnJNdXNpY0lkID0gY2MuYXVkaW9FbmdpbmUucGxheU11c2ljKHNvdW5kLCBsb29wKTtcbiAgICB9XG4gICAgLyoqIOaSreaUvumfs+aViCAqL1xuICAgIHB1YmxpYyBhc3luYyBwbGF5RWZmZWN0KHVybDogc3RyaW5nLCBsb29wID0gZmFsc2UpIHtcbiAgICAgICAgaWYgKCF1cmwgfHwgdXJsID09PSAnJykgcmV0dXJuO1xuXG4gICAgICAgIGlmICh0aGlzLmF1ZGlvQ2FjaGVbdXJsXSkge1xuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdCh0aGlzLmF1ZGlvQ2FjaGVbdXJsXSwgbG9vcCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHNvdW5kID0gYXdhaXQgQ29jb3NIZWxwZXIubG9hZFJlc1N5bmM8Y2MuQXVkaW9DbGlwPih1cmwsIGNjLkF1ZGlvQ2xpcCk7XG4gICAgICAgIHRoaXMuYXVkaW9DYWNoZVt1cmxdID0gc291bmQ7XG4gICAgICAgIHRoaXMuY3VyckVmZmVjdElkID0gY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChzb3VuZCwgbG9vcCk7XG4gICAgfVxuXG4gICAgLyoqIOS7juacrOWcsOivu+WPliAqL1xuICAgIHB1YmxpYyBnZXRWb2x1bWVUb0xvY2FsKCkge1xuICAgICAgICBsZXQgb2JqU3RyID0gTG9jYWxTdG9yYWdlTWdyLmdldEl0ZW0oXCJWb2x1bWVcIilcbiAgICAgICAgaWYgKCFvYmpTdHIpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBvYmpTdHI7XG4gICAgfVxuICAgIC8qKiDorr7nva7pn7Pph48gKi9cbiAgICBwcml2YXRlIHNldFZvbHVtZVRvTG9jYWwoKSB7XG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnNldE11c2ljVm9sdW1lKHRoaXMudm9sdW1lLm11c2ljVm9sdW1lKTtcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUuc2V0RWZmZWN0c1ZvbHVtZSh0aGlzLnZvbHVtZS5lZmZlY3RWb2x1bWUpO1xuICAgICAgICBMb2NhbFN0b3JhZ2VNZ3Iuc2V0SXRlbShcIlZvbHVtZVwiLCB0aGlzLnZvbHVtZSk7XG4gICAgfVxuXG4gICAgcHVibGljIHNldEVmZmVjdEFjdGl2ZShhY3RpdmU6IGJvb2xlYW4sIGlkOiBudW1iZXIgPSAtMSkge1xuICAgICAgICBpZiAoYWN0aXZlKSB7XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5zdG9wKGlkIDwgMCA/IHRoaXMuY3VyckVmZmVjdElkIDogaWQpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucmVzdW1lKGlkIDwgMCA/IHRoaXMuY3VyckVmZmVjdElkIDogaWQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cbn1cblxuY2xhc3MgVm9sdW1lIHtcbiAgICBtdXNpY1ZvbHVtZTogbnVtYmVyO1xuICAgIGVmZmVjdFZvbHVtZTogbnVtYmVyO1xufSJdfQ==