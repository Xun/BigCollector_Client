
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/AdapterMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9848co1bh1O76pCrg2kIN9s', 'AdapterMgr');
// Script/Manager/AdapterMgr.ts

"use strict";
/**
 * @Author: 邓朗
 * @Date: 2019-06-12 17:18:04
 * @Describe: 适配组件, 主要适配背景大小,窗体的位置
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdapterType = void 0;
var flagOffset = 0;
var _None = 1 << flagOffset++;
var _Left = 1 << flagOffset++; // 左对齐
var _Right = 1 << flagOffset++; // 右对齐
var _Top = 1 << flagOffset++; // 上对齐
var _Bottom = 1 << flagOffset++; // 下对齐
var _StretchWidth = _Left | _Right; // 拉伸宽
var _StretchHeight = _Top | _Bottom; // 拉伸高
var _FullWidth = 1 << flagOffset++; // 等比充满宽
var _FullHeight = 1 << flagOffset++; // 等比充满高
var _Final = 1 << flagOffset++;
/**  */
var AdapterType;
(function (AdapterType) {
    AdapterType[AdapterType["Top"] = _Top] = "Top";
    AdapterType[AdapterType["Bottom"] = _Bottom] = "Bottom";
    AdapterType[AdapterType["Left"] = _Left] = "Left";
    AdapterType[AdapterType["Right"] = _Right] = "Right";
    AdapterType[AdapterType["StretchWidth"] = _StretchWidth] = "StretchWidth";
    AdapterType[AdapterType["StretchHeight"] = _StretchHeight] = "StretchHeight";
    AdapterType[AdapterType["FullWidth"] = _FullWidth] = "FullWidth";
    AdapterType[AdapterType["FullHeight"] = _FullHeight] = "FullHeight";
})(AdapterType = exports.AdapterType || (exports.AdapterType = {}));
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var AdapterMgr = /** @class */ (function () {
    function AdapterMgr() {
    }
    AdapterMgr_1 = AdapterMgr;
    Object.defineProperty(AdapterMgr, "inst", {
        get: function () {
            if (this._instance == null) {
                this._instance = new AdapterMgr_1();
                this._instance.visibleSize = cc.view.getVisibleSize();
                console.log("visiable size: " + this._instance.visibleSize);
            }
            return this._instance;
        },
        enumerable: false,
        configurable: true
    });
    AdapterMgr.prototype.adapteByType = function (flag, node, distance) {
        if (distance === void 0) { distance = 0; }
        var widget = node.getComponent(cc.Widget);
        this._doAdapte(flag, node, distance);
        widget.updateAlignment();
    };
    AdapterMgr.prototype._doAdapte = function (flag, node, distance) {
        if (distance === void 0) { distance = 0; }
        var widget = node.getComponent(cc.Widget);
        if (!widget) {
            widget = node.addComponent(cc.Widget);
        }
        switch (flag) {
            case _None:
                break;
            case _Left:
                widget.left = distance ? distance : 0;
                break;
            case _Right:
                widget.right = distance ? distance : 0;
                break;
            case _Top:
                widget.top = distance ? distance : 0;
                break;
            case _Bottom:
                widget.bottom = distance ? distance : 0;
                break;
            case _FullWidth:
                node.height /= node.width / this.visibleSize.width;
                node.width = this.visibleSize.width;
                break;
            case _FullHeight:
                node.width /= node.height / this.visibleSize.height;
                node.height = this.visibleSize.height;
                break;
        }
    };
    /** 移除 */
    AdapterMgr.prototype.removeAdaptater = function (node) {
        if (node.getComponent(cc.Widget)) {
            node.removeComponent(cc.Widget);
        }
    };
    AdapterMgr.prototype.checkIsIphoneX = function () {
        if (cc.view.getFrameSize().width === 375 && cc.view.getFrameSize().height === 812
            || cc.view.getFrameSize().width === 414 && cc.view.getFrameSize().height === 896
            || cc.view.getFrameSize().width === 390 && cc.view.getFrameSize().height === 844
            || cc.view.getFrameSize().width === 428 && cc.view.getFrameSize().height === 926) {
            return true;
        }
        return false;
    };
    var AdapterMgr_1;
    AdapterMgr._instance = null; // 单例
    AdapterMgr = AdapterMgr_1 = __decorate([
        ccclass
    ], AdapterMgr);
    return AdapterMgr;
}());
exports.default = AdapterMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9BZGFwdGVyTWdyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7OztHQUlHOzs7Ozs7Ozs7QUFFSCxJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUM7QUFDbkIsSUFBTSxLQUFLLEdBQUcsQ0FBQyxJQUFJLFVBQVUsRUFBRSxDQUFDO0FBQ2hDLElBQU0sS0FBSyxHQUFHLENBQUMsSUFBSSxVQUFVLEVBQUUsQ0FBQyxDQUFZLE1BQU07QUFDbEQsSUFBTSxNQUFNLEdBQUcsQ0FBQyxJQUFJLFVBQVUsRUFBRSxDQUFDLENBQVcsTUFBTTtBQUNsRCxJQUFNLElBQUksR0FBRyxDQUFDLElBQUksVUFBVSxFQUFFLENBQUMsQ0FBYSxNQUFNO0FBQ2xELElBQU0sT0FBTyxHQUFHLENBQUMsSUFBSSxVQUFVLEVBQUUsQ0FBQyxDQUFVLE1BQU07QUFDbEQsSUFBTSxhQUFhLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFVLE1BQU07QUFDckQsSUFBTSxjQUFjLEdBQUcsSUFBSSxHQUFHLE9BQU8sQ0FBQyxDQUFTLE1BQU07QUFFckQsSUFBTSxVQUFVLEdBQUcsQ0FBQyxJQUFJLFVBQVUsRUFBRSxDQUFDLENBQU8sUUFBUTtBQUNwRCxJQUFNLFdBQVcsR0FBRyxDQUFDLElBQUksVUFBVSxFQUFFLENBQUMsQ0FBTSxRQUFRO0FBQ3BELElBQU0sTUFBTSxHQUFHLENBQUMsSUFBSSxVQUFVLEVBQUUsQ0FBQztBQUVqQyxPQUFPO0FBQ1AsSUFBWSxXQVdYO0FBWEQsV0FBWSxXQUFXO0lBQ25CLGlDQUFNLElBQUksU0FBQSxDQUFBO0lBQ1Ysb0NBQVMsT0FBTyxZQUFBLENBQUE7SUFDaEIsa0NBQU8sS0FBSyxVQUFBLENBQUE7SUFDWixtQ0FBUSxNQUFNLFdBQUEsQ0FBQTtJQUVkLDBDQUFlLGFBQWEsa0JBQUEsQ0FBQTtJQUM1QiwyQ0FBZ0IsY0FBYyxtQkFBQSxDQUFBO0lBRTlCLHVDQUFZLFVBQVUsZUFBQSxDQUFBO0lBQ3RCLHdDQUFhLFdBQVcsZ0JBQUEsQ0FBQTtBQUM1QixDQUFDLEVBWFcsV0FBVyxHQUFYLG1CQUFXLEtBQVgsbUJBQVcsUUFXdEI7QUFFSyxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUc1QztJQUFBO0lBcUVBLENBQUM7bUJBckVvQixVQUFVO0lBRzNCLHNCQUFrQixrQkFBSTthQUF0QjtZQUNJLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxZQUFVLEVBQUUsQ0FBQztnQkFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBa0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFhLENBQUMsQ0FBQzthQUMvRDtZQUNELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDOzs7T0FBQTtJQUtNLGlDQUFZLEdBQW5CLFVBQW9CLElBQVksRUFBRSxJQUFhLEVBQUUsUUFBWTtRQUFaLHlCQUFBLEVBQUEsWUFBWTtRQUN6RCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDckMsTUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFTyw4QkFBUyxHQUFqQixVQUFrQixJQUFZLEVBQUUsSUFBYSxFQUFFLFFBQW9CO1FBQXBCLHlCQUFBLEVBQUEsWUFBb0I7UUFDL0QsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNULE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6QztRQUNELFFBQVEsSUFBSSxFQUFFO1lBQ1YsS0FBSyxLQUFLO2dCQUNOLE1BQU07WUFDVixLQUFLLEtBQUs7Z0JBQ04sTUFBTSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QyxNQUFNO1lBQ1YsS0FBSyxNQUFNO2dCQUNQLE1BQU0sQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkMsTUFBTTtZQUNWLEtBQUssSUFBSTtnQkFDTCxNQUFNLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLE1BQU07WUFDVixLQUFLLE9BQU87Z0JBQ1IsTUFBTSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxNQUFNO1lBQ1YsS0FBSyxVQUFVO2dCQUNYLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztnQkFDbkQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztnQkFDcEMsTUFBTTtZQUNWLEtBQUssV0FBVztnQkFDWixJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7Z0JBQ3RDLE1BQU07U0FDYjtJQUNMLENBQUM7SUFHRCxTQUFTO0lBQ1Qsb0NBQWUsR0FBZixVQUFnQixJQUFhO1FBQ3pCLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDOUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDbkM7SUFDTCxDQUFDO0lBRU0sbUNBQWMsR0FBckI7UUFDSSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsS0FBSyxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLE1BQU0sS0FBSyxHQUFHO2VBQzFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsS0FBSyxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLE1BQU0sS0FBSyxHQUFHO2VBQzdFLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsS0FBSyxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLE1BQU0sS0FBSyxHQUFHO2VBQzdFLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsS0FBSyxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7WUFDbEYsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7O0lBbEVjLG9CQUFTLEdBQWUsSUFBSSxDQUFDLENBQXFCLEtBQUs7SUFGckQsVUFBVTtRQUQ5QixPQUFPO09BQ2EsVUFBVSxDQXFFOUI7SUFBRCxpQkFBQztDQXJFRCxBQXFFQyxJQUFBO2tCQXJFb0IsVUFBVSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQEF1dGhvcjog6YKT5pyXIFxuICogQERhdGU6IDIwMTktMDYtMTIgMTc6MTg6MDQgIFxuICogQERlc2NyaWJlOiDpgILphY3nu4Tku7YsIOS4u+imgemAgumFjeiDjOaZr+Wkp+WwjyznqpfkvZPnmoTkvY3nva5cbiAqL1xuXG5sZXQgZmxhZ09mZnNldCA9IDA7XG5jb25zdCBfTm9uZSA9IDEgPDwgZmxhZ09mZnNldCsrO1xuY29uc3QgX0xlZnQgPSAxIDw8IGZsYWdPZmZzZXQrKzsgICAgICAgICAgICAvLyDlt6blr7npvZBcbmNvbnN0IF9SaWdodCA9IDEgPDwgZmxhZ09mZnNldCsrOyAgICAgICAgICAgLy8g5Y+z5a+56b2QXG5jb25zdCBfVG9wID0gMSA8PCBmbGFnT2Zmc2V0Kys7ICAgICAgICAgICAgIC8vIOS4iuWvuem9kFxuY29uc3QgX0JvdHRvbSA9IDEgPDwgZmxhZ09mZnNldCsrOyAgICAgICAgICAvLyDkuIvlr7npvZBcbmNvbnN0IF9TdHJldGNoV2lkdGggPSBfTGVmdCB8IF9SaWdodDsgICAgICAgICAgLy8g5ouJ5Ly45a69XG5jb25zdCBfU3RyZXRjaEhlaWdodCA9IF9Ub3AgfCBfQm90dG9tOyAgICAgICAgIC8vIOaLieS8uOmrmFxuXG5jb25zdCBfRnVsbFdpZHRoID0gMSA8PCBmbGFnT2Zmc2V0Kys7ICAgICAgIC8vIOetieavlOWFhea7oeWuvVxuY29uc3QgX0Z1bGxIZWlnaHQgPSAxIDw8IGZsYWdPZmZzZXQrKzsgICAgICAvLyDnrYnmr5TlhYXmu6Hpq5hcbmNvbnN0IF9GaW5hbCA9IDEgPDwgZmxhZ09mZnNldCsrO1xuXG4vKiogICovXG5leHBvcnQgZW51bSBBZGFwdGVyVHlwZSB7XG4gICAgVG9wID0gX1RvcCxcbiAgICBCb3R0b20gPSBfQm90dG9tLFxuICAgIExlZnQgPSBfTGVmdCxcbiAgICBSaWdodCA9IF9SaWdodCxcblxuICAgIFN0cmV0Y2hXaWR0aCA9IF9TdHJldGNoV2lkdGgsXG4gICAgU3RyZXRjaEhlaWdodCA9IF9TdHJldGNoSGVpZ2h0LFxuXG4gICAgRnVsbFdpZHRoID0gX0Z1bGxXaWR0aCxcbiAgICBGdWxsSGVpZ2h0ID0gX0Z1bGxIZWlnaHQsXG59XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBBZGFwdGVyTWdyIHtcblxuICAgIHByaXZhdGUgc3RhdGljIF9pbnN0YW5jZTogQWRhcHRlck1nciA9IG51bGw7ICAgICAgICAgICAgICAgICAgICAgLy8g5Y2V5L6LXG4gICAgcHVibGljIHN0YXRpYyBnZXQgaW5zdCgpIHtcbiAgICAgICAgaWYgKHRoaXMuX2luc3RhbmNlID09IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMuX2luc3RhbmNlID0gbmV3IEFkYXB0ZXJNZ3IoKTtcbiAgICAgICAgICAgIHRoaXMuX2luc3RhbmNlLnZpc2libGVTaXplID0gY2Mudmlldy5nZXRWaXNpYmxlU2l6ZSgpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coYHZpc2lhYmxlIHNpemU6ICR7dGhpcy5faW5zdGFuY2UudmlzaWJsZVNpemV9YCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX2luc3RhbmNlO1xuICAgIH1cblxuICAgIC8qKiDlsY/luZXlsLrlr7ggKi9cbiAgICBwdWJsaWMgdmlzaWJsZVNpemU6IGNjLlNpemU7XG5cbiAgICBwdWJsaWMgYWRhcHRlQnlUeXBlKGZsYWc6IG51bWJlciwgbm9kZTogY2MuTm9kZSwgZGlzdGFuY2UgPSAwKSB7XG4gICAgICAgIGxldCB3aWRnZXQgPSBub2RlLmdldENvbXBvbmVudChjYy5XaWRnZXQpO1xuICAgICAgICB0aGlzLl9kb0FkYXB0ZShmbGFnLCBub2RlLCBkaXN0YW5jZSk7XG4gICAgICAgIHdpZGdldC51cGRhdGVBbGlnbm1lbnQoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9kb0FkYXB0ZShmbGFnOiBudW1iZXIsIG5vZGU6IGNjLk5vZGUsIGRpc3RhbmNlOiBudW1iZXIgPSAwKSB7XG4gICAgICAgIGxldCB3aWRnZXQgPSBub2RlLmdldENvbXBvbmVudChjYy5XaWRnZXQpO1xuICAgICAgICBpZiAoIXdpZGdldCkge1xuICAgICAgICAgICAgd2lkZ2V0ID0gbm9kZS5hZGRDb21wb25lbnQoY2MuV2lkZ2V0KTtcbiAgICAgICAgfVxuICAgICAgICBzd2l0Y2ggKGZsYWcpIHtcbiAgICAgICAgICAgIGNhc2UgX05vbmU6XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIF9MZWZ0OlxuICAgICAgICAgICAgICAgIHdpZGdldC5sZWZ0ID0gZGlzdGFuY2UgPyBkaXN0YW5jZSA6IDA7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIF9SaWdodDpcbiAgICAgICAgICAgICAgICB3aWRnZXQucmlnaHQgPSBkaXN0YW5jZSA/IGRpc3RhbmNlIDogMDtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgX1RvcDpcbiAgICAgICAgICAgICAgICB3aWRnZXQudG9wID0gZGlzdGFuY2UgPyBkaXN0YW5jZSA6IDA7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIF9Cb3R0b206XG4gICAgICAgICAgICAgICAgd2lkZ2V0LmJvdHRvbSA9IGRpc3RhbmNlID8gZGlzdGFuY2UgOiAwO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBfRnVsbFdpZHRoOlxuICAgICAgICAgICAgICAgIG5vZGUuaGVpZ2h0IC89IG5vZGUud2lkdGggLyB0aGlzLnZpc2libGVTaXplLndpZHRoO1xuICAgICAgICAgICAgICAgIG5vZGUud2lkdGggPSB0aGlzLnZpc2libGVTaXplLndpZHRoO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBfRnVsbEhlaWdodDpcbiAgICAgICAgICAgICAgICBub2RlLndpZHRoIC89IG5vZGUuaGVpZ2h0IC8gdGhpcy52aXNpYmxlU2l6ZS5oZWlnaHQ7XG4gICAgICAgICAgICAgICAgbm9kZS5oZWlnaHQgPSB0aGlzLnZpc2libGVTaXplLmhlaWdodDtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgLyoqIOenu+mZpCAqL1xuICAgIHJlbW92ZUFkYXB0YXRlcihub2RlOiBjYy5Ob2RlKSB7XG4gICAgICAgIGlmIChub2RlLmdldENvbXBvbmVudChjYy5XaWRnZXQpKSB7XG4gICAgICAgICAgICBub2RlLnJlbW92ZUNvbXBvbmVudChjYy5XaWRnZXQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGNoZWNrSXNJcGhvbmVYKCkge1xuICAgICAgICBpZiAoY2Mudmlldy5nZXRGcmFtZVNpemUoKS53aWR0aCA9PT0gMzc1ICYmIGNjLnZpZXcuZ2V0RnJhbWVTaXplKCkuaGVpZ2h0ID09PSA4MTJcbiAgICAgICAgICAgIHx8IGNjLnZpZXcuZ2V0RnJhbWVTaXplKCkud2lkdGggPT09IDQxNCAmJiBjYy52aWV3LmdldEZyYW1lU2l6ZSgpLmhlaWdodCA9PT0gODk2XG4gICAgICAgICAgICB8fCBjYy52aWV3LmdldEZyYW1lU2l6ZSgpLndpZHRoID09PSAzOTAgJiYgY2Mudmlldy5nZXRGcmFtZVNpemUoKS5oZWlnaHQgPT09IDg0NFxuICAgICAgICAgICAgfHwgY2Mudmlldy5nZXRGcmFtZVNpemUoKS53aWR0aCA9PT0gNDI4ICYmIGNjLnZpZXcuZ2V0RnJhbWVTaXplKCkuaGVpZ2h0ID09PSA5MjYpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG59XG4iXX0=