
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/NativeMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3a5e4ZLEzlMspl1zSdRocfn', 'NativeMgr');
// Script/Manager/NativeMgr.ts

"use strict";
/*

函数定义：
    andrid -> org.cocos2dx.javascript.Native.SayHello(String helloString, String cbName);
    ios    -> Native.SayHello : (NSString*) helloString
                         arg1 : (NSString*) cbName;
js调用写法
native.call("SayHello", "hello world", (ok) => { })
native.callClass("Native", "SayHello", "hello world", (ok) => { })

注意：
    Android 这边 接受数字的函数都要用float。
        double 在 cocos c++ 代码中未实现
*/
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NativeMgr = exports.AndrodSign = void 0;
var AndrodSign;
(function (AndrodSign) {
    AndrodSign["Void"] = "V";
    AndrodSign["String"] = "Ljava/lang/String;";
    AndrodSign["Boolean"] = "Z";
    AndrodSign["Float"] = "F";
    AndrodSign["Double"] = "D";
    AndrodSign["Int"] = "I";
})(AndrodSign = exports.AndrodSign || (exports.AndrodSign = {}));
var NativeMgr = /** @class */ (function () {
    function NativeMgr() {
    }
    NativeMgr.callback = function (cbID) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var func = this.cbs[cbID];
        // console.log("func:" + func)
        if (func) {
            delete this.cbs[cbID];
            func.apply(void 0, args);
            // console.log("只行了：" + func);
        }
        else {
            cc.log("no func ", cbID);
        }
    };
    NativeMgr._newCB = function (f) {
        this.cbIdx++;
        var cbID = "" + this.cbIdx;
        this.cbs[cbID] = f;
        return cbID;
    };
    NativeMgr.getStr = function (clazz, method) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        return this.callWithPackage.apply(this, __spreadArrays([this.defaultPackage, clazz, method, AndrodSign.String], args));
    };
    NativeMgr.callNativeClass = function (clazz, method) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        console.log("调用1：" + clazz + "   " + method);
        this.callWithPackage.apply(this, __spreadArrays([this.defaultPackage, clazz, method, AndrodSign.Void], args));
    };
    NativeMgr.callWithPackage = function (pkg, clazz, method, returnTypeAndroid) {
        var _a, _b;
        var args = [];
        for (var _i = 4; _i < arguments.length; _i++) {
            args[_i - 4] = arguments[_i];
        }
        var real_args = [];
        console.log("调用2：" + pkg + clazz + "   " + method);
        cc.log("clazz:", clazz);
        cc.log("method:", method);
        if (cc.sys.os == cc.sys.OS_ANDROID) {
            var sig = "";
            for (var i = 0; i < args.length; i++) {
                var v = args[i];
                switch (typeof v) {
                    case 'boolean':
                        sig += AndrodSign.Boolean;
                        real_args.push(v + "");
                        break;
                    case 'string':
                        sig += AndrodSign.String;
                        real_args.push(v);
                        break;
                    case 'number':
                        sig += AndrodSign.Float;
                        real_args.push(v);
                        break;
                    case 'function':
                        sig += AndrodSign.String;
                        real_args.push(this._newCB(v));
                        break;
                }
            }
            return (_a = jsb.reflection).callStaticMethod.apply(_a, __spreadArrays([pkg + clazz, method, "(" + sig + ")" + returnTypeAndroid], real_args));
        }
        if (cc.sys.os == cc.sys.OS_IOS) {
            for (var i = 0; i < args.length; i++) {
                var v = args[i];
                if (typeof v == "function") {
                    real_args.push(this._newCB(v));
                }
                else {
                    real_args.push(v);
                }
                if (i == 0) {
                    method += ":";
                }
                else {
                    method += "arg" + i + ":";
                }
            }
            console.log("clazz:" + clazz);
            console.log("method:" + method);
            //@ts-ignore
            return (_b = jsb.reflection).callStaticMethod.apply(_b, __spreadArrays([clazz, method], real_args));
        }
    };
    NativeMgr.cbIdx = 0;
    NativeMgr.cbs = {};
    NativeMgr.defaultPackage = "org/cocos2dx/javascript/";
    return NativeMgr;
}());
exports.NativeMgr = NativeMgr;
//@ts-ignore
window.nativeMgr = NativeMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9OYXRpdmVNZ3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBOzs7Ozs7Ozs7Ozs7O0VBYUU7Ozs7Ozs7Ozs7QUFFRixJQUFZLFVBT1g7QUFQRCxXQUFZLFVBQVU7SUFDbEIsd0JBQVUsQ0FBQTtJQUNWLDJDQUE2QixDQUFBO0lBQzdCLDJCQUFhLENBQUE7SUFDYix5QkFBVyxDQUFBO0lBQ1gsMEJBQVksQ0FBQTtJQUNaLHVCQUFTLENBQUE7QUFDYixDQUFDLEVBUFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFPckI7QUFFRDtJQUFBO0lBd0ZBLENBQUM7SUFsRmlCLGtCQUFRLEdBQXRCLFVBQXVCLElBQVk7UUFBRSxjQUFjO2FBQWQsVUFBYyxFQUFkLHFCQUFjLEVBQWQsSUFBYztZQUFkLDZCQUFjOztRQUMvQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLDhCQUE4QjtRQUM5QixJQUFJLElBQUksRUFBRTtZQUNOLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QixJQUFJLGVBQUksSUFBSSxFQUFFO1lBQ2QsOEJBQThCO1NBQ2pDO2FBQU07WUFDSCxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFFYyxnQkFBTSxHQUFyQixVQUFzQixDQUFDO1FBQ25CLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNiLElBQUksSUFBSSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFYSxnQkFBTSxHQUFwQixVQUFxQixLQUFhLEVBQUUsTUFBYztRQUFFLGNBQWM7YUFBZCxVQUFjLEVBQWQscUJBQWMsRUFBZCxJQUFjO1lBQWQsNkJBQWM7O1FBQzlELE9BQU8sSUFBSSxDQUFDLGVBQWUsT0FBcEIsSUFBSSxrQkFBaUIsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFVBQVUsQ0FBQyxNQUFNLEdBQUssSUFBSSxHQUFFO0lBQ2hHLENBQUM7SUFFTSx5QkFBZSxHQUF0QixVQUF1QixLQUFhLEVBQUUsTUFBYztRQUFFLGNBQWM7YUFBZCxVQUFjLEVBQWQscUJBQWMsRUFBZCxJQUFjO1lBQWQsNkJBQWM7O1FBQ2hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLEtBQUssR0FBRyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLGVBQWUsT0FBcEIsSUFBSSxrQkFBaUIsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFVBQVUsQ0FBQyxJQUFJLEdBQUssSUFBSSxHQUFFO0lBQ3ZGLENBQUM7SUFFTSx5QkFBZSxHQUF0QixVQUF1QixHQUFXLEVBQUUsS0FBYSxFQUFFLE1BQWMsRUFBRSxpQkFBNkI7O1FBQUUsY0FBYzthQUFkLFVBQWMsRUFBZCxxQkFBYyxFQUFkLElBQWM7WUFBZCw2QkFBYzs7UUFDNUcsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxLQUFLLEdBQUcsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDO1FBQ25ELEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3hCLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBRTFCLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUU7WUFDaEMsSUFBSSxHQUFHLEdBQVcsRUFBRSxDQUFDO1lBQ3JCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNsQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hCLFFBQVEsT0FBTyxDQUFDLEVBQUU7b0JBQ2QsS0FBSyxTQUFTO3dCQUNWLEdBQUcsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDO3dCQUMxQixTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDdkIsTUFBTTtvQkFDVixLQUFLLFFBQVE7d0JBQ1QsR0FBRyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUM7d0JBQ3pCLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2xCLE1BQU07b0JBQ1YsS0FBSyxRQUFRO3dCQUNULEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDO3dCQUN4QixTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNsQixNQUFNO29CQUNWLEtBQUssVUFBVTt3QkFDWCxHQUFHLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQzt3QkFDekIsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQy9CLE1BQU07aUJBQ2I7YUFDSjtZQUNELE9BQU8sQ0FBQSxLQUFBLEdBQUcsQ0FBQyxVQUFVLENBQUEsQ0FBQyxnQkFBZ0IsMkJBQUMsR0FBRyxHQUFHLEtBQUssRUFBRSxNQUFNLEVBQUUsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsaUJBQWlCLEdBQUssU0FBUyxHQUFDO1NBQ2pIO1FBR0QsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRTtZQUM1QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixJQUFJLE9BQU8sQ0FBQyxJQUFJLFVBQVUsRUFBRTtvQkFDeEIsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7aUJBQ2pDO3FCQUFNO29CQUNILFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7aUJBQ3BCO2dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDUixNQUFNLElBQUksR0FBRyxDQUFBO2lCQUNoQjtxQkFBTTtvQkFDSCxNQUFNLElBQUksS0FBSyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUE7aUJBQzVCO2FBQ0o7WUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsQ0FBQztZQUNoQyxZQUFZO1lBQ1osT0FBTyxDQUFBLEtBQUEsR0FBRyxDQUFDLFVBQVUsQ0FBQSxDQUFDLGdCQUFnQiwyQkFBQyxLQUFLLEVBQUUsTUFBTSxHQUFLLFNBQVMsR0FBQztTQUN0RTtJQUNMLENBQUM7SUFyRk0sZUFBSyxHQUFXLENBQUMsQ0FBQztJQUNsQixhQUFHLEdBQUcsRUFBRSxDQUFDO0lBQ1Qsd0JBQWMsR0FBRywwQkFBMEIsQ0FBQztJQW9GdkQsZ0JBQUM7Q0F4RkQsQUF3RkMsSUFBQTtBQXhGWSw4QkFBUztBQTBGdEIsWUFBWTtBQUNaLE1BQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLypcblxu5Ye95pWw5a6a5LmJ77yaXG4gICAgYW5kcmlkIC0+IG9yZy5jb2NvczJkeC5qYXZhc2NyaXB0Lk5hdGl2ZS5TYXlIZWxsbyhTdHJpbmcgaGVsbG9TdHJpbmcsIFN0cmluZyBjYk5hbWUpO1xuICAgIGlvcyAgICAtPiBOYXRpdmUuU2F5SGVsbG8gOiAoTlNTdHJpbmcqKSBoZWxsb1N0cmluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgIGFyZzEgOiAoTlNTdHJpbmcqKSBjYk5hbWU7XG5qc+iwg+eUqOWGmeazlVxubmF0aXZlLmNhbGwoXCJTYXlIZWxsb1wiLCBcImhlbGxvIHdvcmxkXCIsIChvaykgPT4geyB9KVxubmF0aXZlLmNhbGxDbGFzcyhcIk5hdGl2ZVwiLCBcIlNheUhlbGxvXCIsIFwiaGVsbG8gd29ybGRcIiwgKG9rKSA9PiB7IH0pXG5cbuazqOaEj++8mlxuICAgIEFuZHJvaWQg6L+Z6L65IOaOpeWPl+aVsOWtl+eahOWHveaVsOmDveimgeeUqGZsb2F044CCXG4gICAgICAgIGRvdWJsZSDlnKggY29jb3MgYysrIOS7o+eggeS4reacquWunueOsFxuKi9cblxuZXhwb3J0IGVudW0gQW5kcm9kU2lnbiB7XG4gICAgVm9pZCA9IFwiVlwiLFxuICAgIFN0cmluZyA9IFwiTGphdmEvbGFuZy9TdHJpbmc7XCIsXG4gICAgQm9vbGVhbiA9IFwiWlwiLFxuICAgIEZsb2F0ID0gXCJGXCIsXG4gICAgRG91YmxlID0gXCJEXCIsXG4gICAgSW50ID0gXCJJXCIsXG59XG5cbmV4cG9ydCBjbGFzcyBOYXRpdmVNZ3Ige1xuXG4gICAgc3RhdGljIGNiSWR4OiBudW1iZXIgPSAwO1xuICAgIHN0YXRpYyBjYnMgPSB7fTtcbiAgICBzdGF0aWMgZGVmYXVsdFBhY2thZ2UgPSBcIm9yZy9jb2NvczJkeC9qYXZhc2NyaXB0L1wiO1xuXG4gICAgcHVibGljIHN0YXRpYyBjYWxsYmFjayhjYklEOiBzdHJpbmcsIC4uLmFyZ3M6IGFueVtdKSB7XG4gICAgICAgIGxldCBmdW5jID0gdGhpcy5jYnNbY2JJRF07XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwiZnVuYzpcIiArIGZ1bmMpXG4gICAgICAgIGlmIChmdW5jKSB7XG4gICAgICAgICAgICBkZWxldGUgdGhpcy5jYnNbY2JJRF07XG4gICAgICAgICAgICBmdW5jKC4uLmFyZ3MpO1xuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCLlj6rooYzkuobvvJpcIiArIGZ1bmMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2MubG9nKFwibm8gZnVuYyBcIiwgY2JJRCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIHN0YXRpYyBfbmV3Q0IoZikge1xuICAgICAgICB0aGlzLmNiSWR4Kys7XG4gICAgICAgIGxldCBjYklEID0gXCJcIiArIHRoaXMuY2JJZHg7XG4gICAgICAgIHRoaXMuY2JzW2NiSURdID0gZjtcbiAgICAgICAgcmV0dXJuIGNiSUQ7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXRTdHIoY2xheno6IHN0cmluZywgbWV0aG9kOiBzdHJpbmcsIC4uLmFyZ3M6IGFueVtdKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNhbGxXaXRoUGFja2FnZSh0aGlzLmRlZmF1bHRQYWNrYWdlLCBjbGF6eiwgbWV0aG9kLCBBbmRyb2RTaWduLlN0cmluZywgLi4uYXJncyk7XG4gICAgfVxuXG4gICAgc3RhdGljIGNhbGxOYXRpdmVDbGFzcyhjbGF6ejogc3RyaW5nLCBtZXRob2Q6IHN0cmluZywgLi4uYXJnczogYW55W10pIHtcbiAgICAgICAgY29uc29sZS5sb2coXCLosIPnlKgx77yaXCIgKyBjbGF6eiArIFwiICAgXCIgKyBtZXRob2QpO1xuICAgICAgICB0aGlzLmNhbGxXaXRoUGFja2FnZSh0aGlzLmRlZmF1bHRQYWNrYWdlLCBjbGF6eiwgbWV0aG9kLCBBbmRyb2RTaWduLlZvaWQsIC4uLmFyZ3MpO1xuICAgIH1cblxuICAgIHN0YXRpYyBjYWxsV2l0aFBhY2thZ2UocGtnOiBzdHJpbmcsIGNsYXp6OiBzdHJpbmcsIG1ldGhvZDogc3RyaW5nLCByZXR1cm5UeXBlQW5kcm9pZDogQW5kcm9kU2lnbiwgLi4uYXJnczogYW55W10pIHtcbiAgICAgICAgbGV0IHJlYWxfYXJncyA9IFtdO1xuICAgICAgICBjb25zb2xlLmxvZyhcIuiwg+eUqDLvvJpcIiArIHBrZyArIGNsYXp6ICsgXCIgICBcIiArIG1ldGhvZCk7XG4gICAgICAgIGNjLmxvZyhcImNsYXp6OlwiLCBjbGF6eik7XG4gICAgICAgIGNjLmxvZyhcIm1ldGhvZDpcIiwgbWV0aG9kKTtcblxuICAgICAgICBpZiAoY2Muc3lzLm9zID09IGNjLnN5cy5PU19BTkRST0lEKSB7XG4gICAgICAgICAgICB2YXIgc2lnOiBzdHJpbmcgPSBcIlwiO1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhcmdzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgbGV0IHYgPSBhcmdzW2ldO1xuICAgICAgICAgICAgICAgIHN3aXRjaCAodHlwZW9mIHYpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnYm9vbGVhbic6XG4gICAgICAgICAgICAgICAgICAgICAgICBzaWcgKz0gQW5kcm9kU2lnbi5Cb29sZWFuO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVhbF9hcmdzLnB1c2godiArIFwiXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3N0cmluZyc6XG4gICAgICAgICAgICAgICAgICAgICAgICBzaWcgKz0gQW5kcm9kU2lnbi5TdHJpbmc7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWFsX2FyZ3MucHVzaCh2KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdudW1iZXInOlxuICAgICAgICAgICAgICAgICAgICAgICAgc2lnICs9IEFuZHJvZFNpZ24uRmxvYXQ7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWFsX2FyZ3MucHVzaCh2KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdmdW5jdGlvbic6XG4gICAgICAgICAgICAgICAgICAgICAgICBzaWcgKz0gQW5kcm9kU2lnbi5TdHJpbmc7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWFsX2FyZ3MucHVzaCh0aGlzLl9uZXdDQih2KSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4ganNiLnJlZmxlY3Rpb24uY2FsbFN0YXRpY01ldGhvZChwa2cgKyBjbGF6eiwgbWV0aG9kLCBcIihcIiArIHNpZyArIFwiKVwiICsgcmV0dXJuVHlwZUFuZHJvaWQsIC4uLnJlYWxfYXJncylcbiAgICAgICAgfVxuXG5cbiAgICAgICAgaWYgKGNjLnN5cy5vcyA9PSBjYy5zeXMuT1NfSU9TKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBsZXQgdiA9IGFyZ3NbaV07XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB2ID09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgICAgICByZWFsX2FyZ3MucHVzaCh0aGlzLl9uZXdDQih2KSlcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZWFsX2FyZ3MucHVzaCh2KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoaSA9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIG1ldGhvZCArPSBcIjpcIlxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIG1ldGhvZCArPSBcImFyZ1wiICsgaSArIFwiOlwiXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImNsYXp6OlwiICsgY2xhenopO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJtZXRob2Q6XCIgKyBtZXRob2QpO1xuICAgICAgICAgICAgLy9AdHMtaWdub3JlXG4gICAgICAgICAgICByZXR1cm4ganNiLnJlZmxlY3Rpb24uY2FsbFN0YXRpY01ldGhvZChjbGF6eiwgbWV0aG9kLCAuLi5yZWFsX2FyZ3MpXG4gICAgICAgIH1cbiAgICB9XG59XG5cbi8vQHRzLWlnbm9yZVxud2luZG93Lm5hdGl2ZU1nciA9IE5hdGl2ZU1ncjtcbiJdfQ==