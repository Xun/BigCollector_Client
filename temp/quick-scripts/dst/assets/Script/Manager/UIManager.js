
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/UIManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f18581vC/JBkoXNDtnFIDo4', 'UIManager');
// Script/Manager/UIManager.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ResMgr_1 = require("./ResMgr");
var ModalMgr_1 = require("./ModalMgr");
// import AdapterMgr, { AdapterType } from "./AdapterMgr";
var Scene_1 = require("../Scene/Scene");
var EventCenter_1 = require("../Net/EventCenter");
var EventType_1 = require("../Net/EventType");
var UIBase_1 = require("../Common/UIBase");
var SysDefine_1 = require("../Common/SysDefine");
var ContentAdapter_1 = require("../Adapter/ContentAdapter");
var UIManager = /** @class */ (function () {
    function UIManager() {
        this._ndScreen = null; // 全屏显示的UI 挂载结点
        this._ndFixed = null; // 固定显示的UI
        this._ndPopUp = null; // 弹出窗口
        this._ndTips = null; // 独立窗体
        this._windows = []; // 存储弹出的窗体
        this._allForms = cc.js.createMap(); // 所有已经挂载的窗体, 可能没有显示
        this._showingForms = cc.js.createMap(); // 正在显示的窗体
        this._tipsForms = cc.js.createMap(); // 独立窗体 独立于其他窗体, 不受其他窗体的影响
        this._loadingForm = cc.js.createMap(); // 正在加载的form 
    }
    UIManager.getInstance = function () {
        var _this = this;
        if (this.instance == null) {
            this.instance = new UIManager();
            var canvas = cc.director.getScene().getChildByName("Canvas");
            var scene = canvas.getChildByName(SysDefine_1.SysDefine.SYS_SCENE_NODE);
            if (!scene) {
                scene = new cc.Node(SysDefine_1.SysDefine.SYS_SCENE_NODE);
                scene.addComponent(Scene_1.default);
                scene.parent = canvas;
            }
            var UIROOT = new cc.Node(SysDefine_1.SysDefine.SYS_UIROOT_NODE);
            scene.addChild(UIROOT);
            UIROOT.addChild(this.instance._ndScreen = new cc.Node(SysDefine_1.SysDefine.SYS_SCREEN_NODE));
            UIROOT.addChild(this.instance._ndFixed = new cc.Node(SysDefine_1.SysDefine.SYS_FIXED_NODE));
            UIROOT.addChild(this.instance._ndPopUp = new cc.Node(SysDefine_1.SysDefine.SYS_POPUP_NODE));
            UIROOT.addChild(this.instance._ndTips = new cc.Node(SysDefine_1.SysDefine.SYS_TOPTIPS_NODE));
            cc.director.once(cc.Director.EVENT_BEFORE_SCENE_LAUNCH, function () {
                _this.instance = null;
            });
        }
        return this.instance;
    };
    /** 预加载UIForm */
    UIManager.prototype.loadUIForm = function (prefabPath) {
        return __awaiter(this, void 0, void 0, function () {
            var uiBase;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadForm(prefabPath)];
                    case 1:
                        uiBase = _a.sent();
                        if (!uiBase) {
                            console.warn(uiBase + "\u6CA1\u6709\u88AB\u6210\u529F\u52A0\u8F7D");
                            return [2 /*return*/, null];
                        }
                        return [2 /*return*/, uiBase];
                }
            });
        });
    };
    /**
     * 加载显示一个UIForm
     * @param prefabPath
     * @param obj 初始化信息, 可以不要
     */
    UIManager.prototype.openForm = function (prefabPath, params, formData) {
        return __awaiter(this, void 0, void 0, function () {
            var com, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!prefabPath || prefabPath.length <= 0) {
                            cc.warn(prefabPath + ", \u53C2\u6570\u9519\u8BEF");
                            return [2 /*return*/];
                        }
                        if (this.checkFormShowing(prefabPath)) {
                            cc.warn(prefabPath + ", \u7A97\u4F53\u6B63\u5728\u663E\u793A\u4E2D");
                            return [2 /*return*/, null];
                        }
                        return [4 /*yield*/, this.loadForm(prefabPath)];
                    case 1:
                        com = _b.sent();
                        if (!com) {
                            cc.warn(prefabPath + " \u52A0\u8F7D\u5931\u8D25\u4E86!");
                            return [2 /*return*/, null];
                        }
                        // 初始化窗体名称
                        com.fid = prefabPath;
                        com.formData = formData;
                        _a = com.formType;
                        switch (_a) {
                            case SysDefine_1.FormType.Screen: return [3 /*break*/, 2];
                            case SysDefine_1.FormType.Fixed: return [3 /*break*/, 4];
                            case SysDefine_1.FormType.Window: return [3 /*break*/, 6];
                            case SysDefine_1.FormType.Tips: return [3 /*break*/, 8];
                        }
                        return [3 /*break*/, 10];
                    case 2: return [4 /*yield*/, this.enterToScreen(com.fid, params)];
                    case 3:
                        _b.sent();
                        return [3 /*break*/, 10];
                    case 4: return [4 /*yield*/, this.enterToFixed(com.fid, params)];
                    case 5:
                        _b.sent();
                        return [3 /*break*/, 10];
                    case 6: return [4 /*yield*/, this.enterToPopup(com.fid, params)];
                    case 7:
                        _b.sent();
                        return [3 /*break*/, 10];
                    case 8: // 独立显示
                    return [4 /*yield*/, this.enterToTips(com.fid, params)];
                    case 9:
                        _b.sent();
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/, com];
                }
            });
        });
    };
    /**
     * 重要方法 关闭一个UIForm
     * @param prefabPath
     */
    UIManager.prototype.closeForm = function (prefabPath) {
        return __awaiter(this, void 0, void 0, function () {
            var com, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!prefabPath || prefabPath.length <= 0) {
                            cc.warn(prefabPath + ", \u53C2\u6570\u9519\u8BEF");
                            return [2 /*return*/];
                        }
                        ;
                        com = this._allForms[prefabPath];
                        if (!com)
                            return [2 /*return*/, false];
                        _a = com.formType;
                        switch (_a) {
                            case SysDefine_1.FormType.Screen: return [3 /*break*/, 1];
                            case SysDefine_1.FormType.Fixed: return [3 /*break*/, 3];
                            case SysDefine_1.FormType.Window: return [3 /*break*/, 5];
                            case SysDefine_1.FormType.Tips: return [3 /*break*/, 7];
                        }
                        return [3 /*break*/, 9];
                    case 1: return [4 /*yield*/, this.exitToScreen(prefabPath)];
                    case 2:
                        _b.sent();
                        return [3 /*break*/, 9];
                    case 3: // 普通模式显示
                    return [4 /*yield*/, this.exitToFixed(prefabPath)];
                    case 4:
                        _b.sent();
                        return [3 /*break*/, 9];
                    case 5: return [4 /*yield*/, this.exitToPopup(prefabPath)];
                    case 6:
                        _b.sent();
                        EventCenter_1.EventCenter.emit(EventType_1.EventType.WindowClosed, prefabPath);
                        return [3 /*break*/, 9];
                    case 7: return [4 /*yield*/, this.exitToTips(prefabPath)];
                    case 8:
                        _b.sent();
                        return [3 /*break*/, 9];
                    case 9:
                        EventCenter_1.EventCenter.emit(EventType_1.EventType.FormClosed, prefabPath);
                        if (com.formData) {
                            com.formData.onClose && com.formData.onClose();
                        }
                        // 判断是否销毁该窗体
                        if (com.willDestory) {
                            this.destoryForm(com);
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    };
    /**
     * 从窗口缓存中加载(如果没有就会在load加载), 并挂载到结点上
     */
    UIManager.prototype.loadForm = function (prefabPath) {
        return __awaiter(this, void 0, Promise, function () {
            var com;
            var _this = this;
            return __generator(this, function (_a) {
                com = this._allForms[prefabPath];
                if (com)
                    return [2 /*return*/, com];
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        if (_this._loadingForm[prefabPath]) {
                            _this._loadingForm[prefabPath].push(resolve);
                            return;
                        }
                        _this._loadingForm[prefabPath] = [resolve];
                        _this._doLoadUIForm(prefabPath).then(function (com) {
                            for (var _i = 0, _a = _this._loadingForm[prefabPath]; _i < _a.length; _i++) {
                                var func = _a[_i];
                                func(com);
                            }
                            _this._loadingForm[prefabPath] = null;
                            delete _this._loadingForm[prefabPath];
                        });
                    })];
            });
        });
    };
    /**
     * 从resources中加载
     * @param prefabPath
     */
    UIManager.prototype._doLoadUIForm = function (prefabPath) {
        return __awaiter(this, void 0, void 0, function () {
            var node, com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ResMgr_1.default.inst.loadForm(prefabPath)];
                    case 1:
                        node = _a.sent();
                        com = node.getComponent(UIBase_1.default);
                        if (!com) {
                            cc.warn(prefabPath + " \u7ED3\u70B9\u6CA1\u6709\u7ED1\u5B9AUIBase");
                            return [2 /*return*/, null];
                        }
                        node.active = false; // 避免baseCom调用了onload方法
                        node.addComponent(ContentAdapter_1.default);
                        switch (com.formType) {
                            case SysDefine_1.FormType.Screen:
                                this._ndScreen.addChild(node);
                                break;
                            case SysDefine_1.FormType.Fixed:
                                this._ndFixed.addChild(node);
                                break;
                            case SysDefine_1.FormType.Window:
                                this._ndPopUp.addChild(node);
                                break;
                            case SysDefine_1.FormType.Tips:
                                this._ndTips.addChild(node);
                                break;
                        }
                        this._allForms[prefabPath] = com;
                        return [2 /*return*/, com];
                }
            });
        });
    };
    /** 添加到screen中 */
    UIManager.prototype.enterToScreen = function (fid, params) {
        return __awaiter(this, void 0, void 0, function () {
            var arr, key, com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        arr = [];
                        for (key in this._showingForms) {
                            arr.push(this._showingForms[key].closeSelf());
                        }
                        return [4 /*yield*/, Promise.all(arr)];
                    case 1:
                        _a.sent();
                        com = this._allForms[fid];
                        if (!com)
                            return [2 /*return*/];
                        this._showingForms[fid] = com;
                        // AdapterMgr.inst.adapteByType(AdapterType.StretchHeight | AdapterType.StretchWidth, com.node);
                        return [4 /*yield*/, com._preInit(params)];
                    case 2:
                        // AdapterMgr.inst.adapteByType(AdapterType.StretchHeight | AdapterType.StretchWidth, com.node);
                        _a.sent();
                        com.onShow(params);
                        return [4 /*yield*/, this.showEffect(com)];
                    case 3:
                        _a.sent();
                        com.onAfterShow(params);
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 添加到Fixed中 */
    UIManager.prototype.enterToFixed = function (fid, params) {
        return __awaiter(this, void 0, void 0, function () {
            var com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        com = this._allForms[fid];
                        if (!com)
                            return [2 /*return*/];
                        return [4 /*yield*/, com._preInit(params)];
                    case 1:
                        _a.sent();
                        com.onShow(params);
                        this._showingForms[fid] = com;
                        return [4 /*yield*/, this.showEffect(com)];
                    case 2:
                        _a.sent();
                        com.onAfterShow(params);
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 添加到popup中 */
    UIManager.prototype.enterToPopup = function (fid, params) {
        return __awaiter(this, void 0, void 0, function () {
            var com, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        com = this._allForms[fid];
                        if (!com)
                            return [2 /*return*/];
                        return [4 /*yield*/, com._preInit(params)];
                    case 1:
                        _a.sent();
                        this._windows.push(com);
                        for (i = 0; i < this._windows.length; i++) {
                            this._windows[i].node.zIndex = i + 1;
                        }
                        com.onShow(params);
                        this._showingForms[fid] = com;
                        ModalMgr_1.default.inst.checkModalWindow(this._windows);
                        return [4 /*yield*/, this.showEffect(com)];
                    case 2:
                        _a.sent();
                        com.onAfterShow(params);
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 加载到tips中 */
    UIManager.prototype.enterToTips = function (fid, params) {
        return __awaiter(this, void 0, void 0, function () {
            var com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        com = this._allForms[fid];
                        if (!com)
                            return [2 /*return*/];
                        return [4 /*yield*/, com._preInit(params)];
                    case 1:
                        _a.sent();
                        this._tipsForms[fid] = com;
                        com.onShow(params);
                        return [4 /*yield*/, this.showEffect(com)];
                    case 2:
                        _a.sent();
                        com.onAfterShow(params);
                        return [2 /*return*/];
                }
            });
        });
    };
    UIManager.prototype.exitToScreen = function (fid) {
        return __awaiter(this, void 0, void 0, function () {
            var com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        com = this._showingForms[fid];
                        if (!com)
                            return [2 /*return*/];
                        com.onHide();
                        return [4 /*yield*/, this.hideEffect(com)];
                    case 1:
                        _a.sent();
                        com.onAfterHide();
                        this._showingForms[fid] = null;
                        delete this._showingForms[fid];
                        return [2 /*return*/];
                }
            });
        });
    };
    UIManager.prototype.exitToFixed = function (fid) {
        return __awaiter(this, void 0, void 0, function () {
            var com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        com = this._allForms[fid];
                        if (!com)
                            return [2 /*return*/];
                        com.onHide();
                        return [4 /*yield*/, this.hideEffect(com)];
                    case 1:
                        _a.sent();
                        com.onAfterHide();
                        this._showingForms[fid] = null;
                        delete this._showingForms[fid];
                        return [2 /*return*/];
                }
            });
        });
    };
    UIManager.prototype.exitToPopup = function (fid) {
        return __awaiter(this, void 0, void 0, function () {
            var com, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this._windows.length <= 0)
                            return [2 /*return*/];
                        com = null;
                        for (i = this._windows.length - 1; i >= 0; i--) {
                            if (this._windows[i].fid === fid) {
                                com = this._windows[i];
                                this._windows.splice(i, 1);
                            }
                        }
                        if (!com)
                            return [2 /*return*/];
                        com.onHide();
                        ModalMgr_1.default.inst.checkModalWindow(this._windows);
                        return [4 /*yield*/, this.hideEffect(com)];
                    case 1:
                        _a.sent();
                        com.onAfterHide();
                        this._showingForms[fid] = null;
                        delete this._showingForms[fid];
                        return [2 /*return*/];
                }
            });
        });
    };
    UIManager.prototype.exitToTips = function (fid) {
        return __awaiter(this, void 0, void 0, function () {
            var com;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        com = this._allForms[fid];
                        if (!com)
                            return [2 /*return*/];
                        com.onHide();
                        return [4 /*yield*/, this.hideEffect(com)];
                    case 1:
                        _a.sent();
                        com.onAfterHide();
                        this._tipsForms[fid] = null;
                        delete this._tipsForms[fid];
                        return [2 /*return*/];
                }
            });
        });
    };
    UIManager.prototype.showEffect = function (baseUI) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        baseUI.node.active = true;
                        return [4 /*yield*/, baseUI.showEffect()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    UIManager.prototype.hideEffect = function (baseUI) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, baseUI.hideEffect()];
                    case 1:
                        _a.sent();
                        baseUI.node.active = false;
                        return [2 /*return*/];
                }
            });
        });
    };
    /** 销毁 */
    UIManager.prototype.destoryForm = function (com) {
        ResMgr_1.default.inst.destoryDynamicRes(com.fid);
        ResMgr_1.default.inst.destoryForm(com);
        // 从allmap中删除
        this._allForms[com.fid] = null;
        delete this._allForms[com.fid];
    };
    /** 窗体是否正在显示 */
    UIManager.prototype.checkFormShowing = function (fid) {
        var com = this._allForms[fid];
        if (!com)
            return false;
        return com.node.active;
    };
    /** 窗体是否正在加载 */
    UIManager.prototype.checkFormLoading = function (prefabPath) {
        var com = this._loadingForm[prefabPath];
        return !!com;
    };
    /** 获得Component */
    UIManager.prototype.getForm = function (fId) {
        return this._allForms[fId];
    };
    UIManager.instance = null; // 单例
    return UIManager;
}());
exports.default = UIManager;
if (CC_DEBUG) {
    window['UIManager'] = UIManager;
}

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9VSU1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxtQ0FBOEI7QUFDOUIsdUNBQWtDO0FBQ2xDLDBEQUEwRDtBQUMxRCx3Q0FBbUM7QUFFbkMsa0RBQWlEO0FBQ2pELDhDQUE2QztBQUU3QywyQ0FBc0M7QUFDdEMsaURBQTBEO0FBRTFELDREQUF1RDtBQUV2RDtJQUFBO1FBQ1ksY0FBUyxHQUFZLElBQUksQ0FBQyxDQUFFLGVBQWU7UUFDM0MsYUFBUSxHQUFZLElBQUksQ0FBQyxDQUFFLFVBQVU7UUFDckMsYUFBUSxHQUFZLElBQUksQ0FBQyxDQUFFLE9BQU87UUFDbEMsWUFBTyxHQUFZLElBQUksQ0FBQyxDQUFFLE9BQU87UUFFakMsYUFBUSxHQUFlLEVBQUUsQ0FBQyxDQUFtQixVQUFVO1FBQ3ZELGNBQVMsR0FBOEIsRUFBRSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFJLG9CQUFvQjtRQUNqRixrQkFBYSxHQUE4QixFQUFFLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUksVUFBVTtRQUMzRSxlQUFVLEdBQThCLEVBQUUsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBSSwwQkFBMEI7UUFDeEYsaUJBQVksR0FBbUQsRUFBRSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFJLGFBQWE7SUFvVTlHLENBQUM7SUFqVWlCLHFCQUFXLEdBQXpCO1FBQUEsaUJBc0JDO1FBckJHLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFNBQVMsRUFBRSxDQUFDO1lBQ2hDLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdELElBQUksS0FBSyxHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMscUJBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUM1RCxJQUFJLENBQUMsS0FBSyxFQUFFO2dCQUNSLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMscUJBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDOUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxlQUFLLENBQUMsQ0FBQztnQkFDMUIsS0FBSyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7YUFDekI7WUFDRCxJQUFJLE1BQU0sR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMscUJBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNwRCxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRXZCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLHFCQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUNsRixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxxQkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDaEYsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMscUJBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ2hGLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLHFCQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQ2pGLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMseUJBQXlCLEVBQUU7Z0JBQ3BELEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVELGdCQUFnQjtJQUNILDhCQUFVLEdBQXZCLFVBQXdCLFVBQWtCOzs7Ozs0QkFDekIscUJBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBQTs7d0JBQXhDLE1BQU0sR0FBRyxTQUErQjt3QkFDNUMsSUFBSSxDQUFDLE1BQU0sRUFBRTs0QkFDVCxPQUFPLENBQUMsSUFBSSxDQUFJLE1BQU0sK0NBQVMsQ0FBQyxDQUFDOzRCQUNqQyxzQkFBTyxJQUFJLEVBQUM7eUJBQ2Y7d0JBQ0Qsc0JBQU8sTUFBTSxFQUFDOzs7O0tBQ2pCO0lBRUQ7Ozs7T0FJRztJQUNVLDRCQUFRLEdBQXJCLFVBQXNCLFVBQWtCLEVBQUUsTUFBWSxFQUFFLFFBQW9COzs7Ozs7d0JBQ3hFLElBQUksQ0FBQyxVQUFVLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7NEJBQ3ZDLEVBQUUsQ0FBQyxJQUFJLENBQUksVUFBVSwrQkFBUSxDQUFDLENBQUM7NEJBQy9CLHNCQUFPO3lCQUNWO3dCQUNELElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxFQUFFOzRCQUNuQyxFQUFFLENBQUMsSUFBSSxDQUFJLFVBQVUsaURBQVcsQ0FBQyxDQUFDOzRCQUNsQyxzQkFBTyxJQUFJLEVBQUM7eUJBQ2Y7d0JBQ1MscUJBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBQTs7d0JBQXJDLEdBQUcsR0FBRyxTQUErQjt3QkFDekMsSUFBSSxDQUFDLEdBQUcsRUFBRTs0QkFDTixFQUFFLENBQUMsSUFBSSxDQUFJLFVBQVUscUNBQVMsQ0FBQyxDQUFDOzRCQUNoQyxzQkFBTyxJQUFJLEVBQUM7eUJBQ2Y7d0JBQ0QsVUFBVTt3QkFDVixHQUFHLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQzt3QkFDckIsR0FBRyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7d0JBRWhCLEtBQUEsR0FBRyxDQUFDLFFBQVEsQ0FBQTs7aUNBQ1gsb0JBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBaEIsd0JBQWU7aUNBR2Ysb0JBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBZix3QkFBYztpQ0FHZCxvQkFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFoQix3QkFBZTtpQ0FHZixvQkFBUSxDQUFDLElBQUksQ0FBQyxDQUFkLHdCQUFhOzs7NEJBUmQscUJBQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxFQUFBOzt3QkFBekMsU0FBeUMsQ0FBQzt3QkFDMUMseUJBQU07NEJBRU4scUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxFQUFBOzt3QkFBeEMsU0FBd0MsQ0FBQzt3QkFDekMseUJBQU07NEJBRU4scUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxFQUFBOzt3QkFBeEMsU0FBd0MsQ0FBQzt3QkFDekMseUJBQU07NEJBQzJDLE9BQU87b0JBQ3hELHFCQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsRUFBQTs7d0JBQXZDLFNBQXVDLENBQUM7d0JBQ3hDLHlCQUFNOzZCQUdkLHNCQUFPLEdBQUcsRUFBQzs7OztLQUNkO0lBQ0Q7OztPQUdHO0lBQ1UsNkJBQVMsR0FBdEIsVUFBdUIsVUFBa0I7Ozs7Ozt3QkFDckMsSUFBSSxDQUFDLFVBQVUsSUFBSSxVQUFVLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTs0QkFDdkMsRUFBRSxDQUFDLElBQUksQ0FBSSxVQUFVLCtCQUFRLENBQUMsQ0FBQzs0QkFDL0Isc0JBQU87eUJBQ1Y7d0JBQUEsQ0FBQzt3QkFDRSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQzt3QkFDckMsSUFBSSxDQUFDLEdBQUc7NEJBQUUsc0JBQU8sS0FBSyxFQUFDO3dCQUVmLEtBQUEsR0FBRyxDQUFDLFFBQVEsQ0FBQTs7aUNBQ1gsb0JBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBaEIsd0JBQWU7aUNBR2Ysb0JBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBZix3QkFBYztpQ0FHZCxvQkFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFoQix3QkFBZTtpQ0FJZixvQkFBUSxDQUFDLElBQUksQ0FBQyxDQUFkLHdCQUFhOzs7NEJBVGQscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsRUFBQTs7d0JBQW5DLFNBQW1DLENBQUM7d0JBQ3BDLHdCQUFNOzRCQUN1QyxTQUFTO29CQUN0RCxxQkFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxFQUFBOzt3QkFBbEMsU0FBa0MsQ0FBQzt3QkFDbkMsd0JBQU07NEJBRU4scUJBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsRUFBQTs7d0JBQWxDLFNBQWtDLENBQUM7d0JBQ25DLHlCQUFXLENBQUMsSUFBSSxDQUFDLHFCQUFTLENBQUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxDQUFDO3dCQUNyRCx3QkFBTTs0QkFFTixxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxFQUFBOzt3QkFBakMsU0FBaUMsQ0FBQzt3QkFDbEMsd0JBQU07O3dCQUdkLHlCQUFXLENBQUMsSUFBSSxDQUFDLHFCQUFTLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO3dCQUVuRCxJQUFJLEdBQUcsQ0FBQyxRQUFRLEVBQUU7NEJBQ2QsR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQzt5QkFDbEQ7d0JBQ0QsWUFBWTt3QkFDWixJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUU7NEJBQ2pCLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7eUJBQ3pCO3dCQUNELHNCQUFPLElBQUksRUFBQzs7OztLQUNmO0lBRUQ7O09BRUc7SUFDVyw0QkFBUSxHQUF0QixVQUF1QixVQUFrQjt1Q0FBRyxPQUFPOzs7O2dCQUMzQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDckMsSUFBSSxHQUFHO29CQUFFLHNCQUFPLEdBQUcsRUFBQztnQkFDcEIsc0JBQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTt3QkFDL0IsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxFQUFFOzRCQUMvQixLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDNUMsT0FBTzt5QkFDVjt3QkFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQzFDLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBVzs0QkFDNUMsS0FBbUIsVUFBNkIsRUFBN0IsS0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxFQUE3QixjQUE2QixFQUE3QixJQUE2QixFQUFFO2dDQUE3QyxJQUFNLElBQUksU0FBQTtnQ0FDWCxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7NkJBQ2I7NEJBQ0QsS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7NEJBQ3JDLE9BQU8sS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQzt3QkFDekMsQ0FBQyxDQUFDLENBQUM7b0JBQ1AsQ0FBQyxDQUFDLEVBQUM7OztLQUNOO0lBRUQ7OztPQUdHO0lBQ1csaUNBQWEsR0FBM0IsVUFBNEIsVUFBa0I7Ozs7OzRCQUMvQixxQkFBTSxnQkFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUE7O3dCQUE3QyxJQUFJLEdBQUcsU0FBc0M7d0JBQzdDLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFNLENBQUMsQ0FBQzt3QkFDcEMsSUFBSSxDQUFDLEdBQUcsRUFBRTs0QkFDTixFQUFFLENBQUMsSUFBSSxDQUFJLFVBQVUsZ0RBQWUsQ0FBQyxDQUFDOzRCQUN0QyxzQkFBTyxJQUFJLEVBQUM7eUJBQ2Y7d0JBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBb0IsdUJBQXVCO3dCQUMvRCxJQUFJLENBQUMsWUFBWSxDQUFDLHdCQUFjLENBQUMsQ0FBQzt3QkFDbEMsUUFBUSxHQUFHLENBQUMsUUFBUSxFQUFFOzRCQUNsQixLQUFLLG9CQUFRLENBQUMsTUFBTTtnQ0FDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQzlCLE1BQU07NEJBQ1YsS0FBSyxvQkFBUSxDQUFDLEtBQUs7Z0NBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQzdCLE1BQU07NEJBQ1YsS0FBSyxvQkFBUSxDQUFDLE1BQU07Z0NBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dDQUM3QixNQUFNOzRCQUNWLEtBQUssb0JBQVEsQ0FBQyxJQUFJO2dDQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dDQUM1QixNQUFNO3lCQUNiO3dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDO3dCQUVqQyxzQkFBTyxHQUFHLEVBQUM7Ozs7S0FDZDtJQUVELGlCQUFpQjtJQUNILGlDQUFhLEdBQTNCLFVBQTRCLEdBQVcsRUFBRSxNQUFXOzs7Ozs7d0JBRTVDLEdBQUcsR0FBNEIsRUFBRSxDQUFDO3dCQUN0QyxLQUFTLEdBQUcsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFOzRCQUNoQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQzt5QkFDakQ7d0JBQ0QscUJBQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBQTs7d0JBQXRCLFNBQXNCLENBQUM7d0JBRW5CLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUM5QixJQUFJLENBQUMsR0FBRzs0QkFBRSxzQkFBTzt3QkFDakIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUM7d0JBRTlCLGdHQUFnRzt3QkFFaEcscUJBQU0sR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBRjFCLGdHQUFnRzt3QkFFaEcsU0FBMEIsQ0FBQzt3QkFDM0IsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFFbkIscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQTs7d0JBQTFCLFNBQTBCLENBQUM7d0JBQzNCLEdBQUcsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7O0tBQzNCO0lBRUQsZ0JBQWdCO0lBQ0YsZ0NBQVksR0FBMUIsVUFBMkIsR0FBVyxFQUFFLE1BQVc7Ozs7Ozt3QkFDM0MsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzlCLElBQUksQ0FBQyxHQUFHOzRCQUFFLHNCQUFPO3dCQUNqQixxQkFBTSxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBMUIsU0FBMEIsQ0FBQzt3QkFFM0IsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDbkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUM7d0JBQzlCLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUE7O3dCQUExQixTQUEwQixDQUFDO3dCQUMzQixHQUFHLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7OztLQUMzQjtJQUVELGdCQUFnQjtJQUNGLGdDQUFZLEdBQTFCLFVBQTJCLEdBQVcsRUFBRSxNQUFXOzs7Ozs7d0JBQzNDLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBYSxDQUFDO3dCQUMxQyxJQUFJLENBQUMsR0FBRzs0QkFBRSxzQkFBTzt3QkFDakIscUJBQU0sR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQTFCLFNBQTBCLENBQUM7d0JBRTNCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUV4QixLQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOzRCQUMzQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQzt5QkFDeEM7d0JBRUQsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDbkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUM7d0JBRTlCLGtCQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDOUMscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQTs7d0JBQTFCLFNBQTBCLENBQUM7d0JBQzNCLEdBQUcsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7O0tBQzNCO0lBRUQsZUFBZTtJQUNELCtCQUFXLEdBQXpCLFVBQTBCLEdBQVcsRUFBRSxNQUFXOzs7Ozs7d0JBQzFDLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUM5QixJQUFJLENBQUMsR0FBRzs0QkFBRSxzQkFBTzt3QkFDakIscUJBQU0sR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQTFCLFNBQTBCLENBQUM7d0JBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO3dCQUUzQixHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNuQixxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFBOzt3QkFBMUIsU0FBMEIsQ0FBQzt3QkFDM0IsR0FBRyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7Ozs7S0FDM0I7SUFFYSxnQ0FBWSxHQUExQixVQUEyQixHQUFXOzs7Ozs7d0JBQzlCLEdBQUcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUNsQyxJQUFJLENBQUMsR0FBRzs0QkFBRSxzQkFBTzt3QkFDakIsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUNiLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUE7O3dCQUExQixTQUEwQixDQUFDO3dCQUMzQixHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7d0JBRWxCLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO3dCQUMvQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7Ozs7O0tBQ2xDO0lBRWEsK0JBQVcsR0FBekIsVUFBMEIsR0FBVzs7Ozs7O3dCQUM3QixHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDOUIsSUFBSSxDQUFDLEdBQUc7NEJBQUUsc0JBQU87d0JBQ2pCLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQzt3QkFDYixxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFBOzt3QkFBMUIsU0FBMEIsQ0FBQzt3QkFDM0IsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO3dCQUVsQixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQzt3QkFDL0IsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDOzs7OztLQUNsQztJQUVhLCtCQUFXLEdBQXpCLFVBQTBCLEdBQVc7Ozs7Ozt3QkFDakMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDOzRCQUFFLHNCQUFPO3dCQUNsQyxHQUFHLEdBQWEsSUFBSSxDQUFDO3dCQUN6QixLQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDaEQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxHQUFHLEVBQUU7Z0NBQzlCLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUN2QixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7NkJBQzlCO3lCQUNKO3dCQUNELElBQUksQ0FBQyxHQUFHOzRCQUFFLHNCQUFPO3dCQUVqQixHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7d0JBQ2Isa0JBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUM5QyxxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFBOzt3QkFBMUIsU0FBMEIsQ0FBQzt3QkFDM0IsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO3dCQUVsQixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQzt3QkFDL0IsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDOzs7OztLQUNsQztJQUVhLDhCQUFVLEdBQXhCLFVBQXlCLEdBQVc7Ozs7Ozt3QkFDNUIsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzlCLElBQUksQ0FBQyxHQUFHOzRCQUFFLHNCQUFPO3dCQUNqQixHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7d0JBQ2IscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQTs7d0JBQTFCLFNBQTBCLENBQUM7d0JBQzNCLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQzt3QkFFbEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7d0JBQzVCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQzs7Ozs7S0FDL0I7SUFFYSw4QkFBVSxHQUF4QixVQUF5QixNQUFjOzs7Ozt3QkFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO3dCQUMxQixxQkFBTSxNQUFNLENBQUMsVUFBVSxFQUFFLEVBQUE7O3dCQUF6QixTQUF5QixDQUFDOzs7OztLQUM3QjtJQUNhLDhCQUFVLEdBQXhCLFVBQXlCLE1BQWM7Ozs7NEJBQ25DLHFCQUFNLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBQTs7d0JBQXpCLFNBQXlCLENBQUM7d0JBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQzs7Ozs7S0FDOUI7SUFFRCxTQUFTO0lBQ0QsK0JBQVcsR0FBbkIsVUFBb0IsR0FBVztRQUMzQixnQkFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkMsZ0JBQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLGFBQWE7UUFDYixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDL0IsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBQ0QsZUFBZTtJQUNSLG9DQUFnQixHQUF2QixVQUF3QixHQUFXO1FBQy9CLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDOUIsSUFBSSxDQUFDLEdBQUc7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUN2QixPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQzNCLENBQUM7SUFFRCxlQUFlO0lBQ1Isb0NBQWdCLEdBQXZCLFVBQXdCLFVBQWtCO1FBQ3RDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxrQkFBa0I7SUFDWCwyQkFBTyxHQUFkLFVBQWUsR0FBVztRQUN0QixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQWpVYyxrQkFBUSxHQUFjLElBQUksQ0FBQyxDQUFpRCxLQUFLO0lBa1VwRyxnQkFBQztDQTlVRCxBQThVQyxJQUFBO2tCQTlVb0IsU0FBUztBQWdWOUIsSUFBSSxRQUFRLEVBQUU7SUFDVixNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsU0FBUyxDQUFDO0NBQ25DIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlc01nciBmcm9tIFwiLi9SZXNNZ3JcIjtcbmltcG9ydCBNb2RhbE1nciBmcm9tIFwiLi9Nb2RhbE1nclwiO1xuLy8gaW1wb3J0IEFkYXB0ZXJNZ3IsIHsgQWRhcHRlclR5cGUgfSBmcm9tIFwiLi9BZGFwdGVyTWdyXCI7XG5pbXBvcnQgU2NlbmUgZnJvbSBcIi4uL1NjZW5lL1NjZW5lXCI7XG5cbmltcG9ydCB7IEV2ZW50Q2VudGVyIH0gZnJvbSBcIi4uL05ldC9FdmVudENlbnRlclwiO1xuaW1wb3J0IHsgRXZlbnRUeXBlIH0gZnJvbSBcIi4uL05ldC9FdmVudFR5cGVcIjtcbmltcG9ydCB7IFVJV2luZG93IH0gZnJvbSBcIi4uL0NvbW1vbi9VSUZvcm1cIjtcbmltcG9ydCBVSUJhc2UgZnJvbSBcIi4uL0NvbW1vbi9VSUJhc2VcIjtcbmltcG9ydCB7IEZvcm1UeXBlLCBTeXNEZWZpbmUgfSBmcm9tIFwiLi4vQ29tbW9uL1N5c0RlZmluZVwiO1xuaW1wb3J0IHsgSUZvcm1EYXRhIH0gZnJvbSBcIi4uL0NvbW1vbi9TdHJ1Y3RcIjtcbmltcG9ydCBDb250ZW50QWRhcHRlciBmcm9tIFwiLi4vQWRhcHRlci9Db250ZW50QWRhcHRlclwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSU1hbmFnZXIge1xuICAgIHByaXZhdGUgX25kU2NyZWVuOiBjYy5Ob2RlID0gbnVsbDsgIC8vIOWFqOWxj+aYvuekuueahFVJIOaMgui9vee7k+eCuVxuICAgIHByaXZhdGUgX25kRml4ZWQ6IGNjLk5vZGUgPSBudWxsOyAgLy8g5Zu65a6a5pi+56S655qEVUlcbiAgICBwcml2YXRlIF9uZFBvcFVwOiBjYy5Ob2RlID0gbnVsbDsgIC8vIOW8ueWHuueql+WPo1xuICAgIHByaXZhdGUgX25kVGlwczogY2MuTm9kZSA9IG51bGw7ICAvLyDni6znq4vnqpfkvZNcblxuICAgIHByaXZhdGUgX3dpbmRvd3M6IFVJV2luZG93W10gPSBbXTsgICAgICAgICAgICAgICAgICAgLy8g5a2Y5YKo5by55Ye655qE56qX5L2TXG4gICAgcHJpdmF0ZSBfYWxsRm9ybXM6IHsgW2tleTogc3RyaW5nXTogVUlCYXNlIH0gPSBjYy5qcy5jcmVhdGVNYXAoKTsgICAgLy8g5omA5pyJ5bey57uP5oyC6L2955qE56qX5L2TLCDlj6/og73msqHmnInmmL7npLpcbiAgICBwcml2YXRlIF9zaG93aW5nRm9ybXM6IHsgW2tleTogc3RyaW5nXTogVUlCYXNlIH0gPSBjYy5qcy5jcmVhdGVNYXAoKTsgICAgLy8g5q2j5Zyo5pi+56S655qE56qX5L2TXG4gICAgcHJpdmF0ZSBfdGlwc0Zvcm1zOiB7IFtrZXk6IHN0cmluZ106IFVJQmFzZSB9ID0gY2MuanMuY3JlYXRlTWFwKCk7ICAgIC8vIOeLrOeri+eql+S9kyDni6znq4vkuo7lhbbku5bnqpfkvZMsIOS4jeWPl+WFtuS7lueql+S9k+eahOW9seWTjVxuICAgIHByaXZhdGUgX2xvYWRpbmdGb3JtOiB7IFtrZXk6IHN0cmluZ106ICgodmFsdWU6IFVJQmFzZSkgPT4gdm9pZClbXSB9ID0gY2MuanMuY3JlYXRlTWFwKCk7ICAgIC8vIOato+WcqOWKoOi9veeahGZvcm0gXG5cbiAgICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZTogVUlNYW5hZ2VyID0gbnVsbDsgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8g5Y2V5L6LXG4gICAgcHVibGljIHN0YXRpYyBnZXRJbnN0YW5jZSgpOiBVSU1hbmFnZXIge1xuICAgICAgICBpZiAodGhpcy5pbnN0YW5jZSA9PSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLmluc3RhbmNlID0gbmV3IFVJTWFuYWdlcigpO1xuICAgICAgICAgICAgbGV0IGNhbnZhcyA9IGNjLmRpcmVjdG9yLmdldFNjZW5lKCkuZ2V0Q2hpbGRCeU5hbWUoXCJDYW52YXNcIik7XG4gICAgICAgICAgICBsZXQgc2NlbmUgPSBjYW52YXMuZ2V0Q2hpbGRCeU5hbWUoU3lzRGVmaW5lLlNZU19TQ0VORV9OT0RFKTtcbiAgICAgICAgICAgIGlmICghc2NlbmUpIHtcbiAgICAgICAgICAgICAgICBzY2VuZSA9IG5ldyBjYy5Ob2RlKFN5c0RlZmluZS5TWVNfU0NFTkVfTk9ERSk7XG4gICAgICAgICAgICAgICAgc2NlbmUuYWRkQ29tcG9uZW50KFNjZW5lKTtcbiAgICAgICAgICAgICAgICBzY2VuZS5wYXJlbnQgPSBjYW52YXM7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBsZXQgVUlST09UID0gbmV3IGNjLk5vZGUoU3lzRGVmaW5lLlNZU19VSVJPT1RfTk9ERSk7XG4gICAgICAgICAgICBzY2VuZS5hZGRDaGlsZChVSVJPT1QpO1xuXG4gICAgICAgICAgICBVSVJPT1QuYWRkQ2hpbGQodGhpcy5pbnN0YW5jZS5fbmRTY3JlZW4gPSBuZXcgY2MuTm9kZShTeXNEZWZpbmUuU1lTX1NDUkVFTl9OT0RFKSk7XG4gICAgICAgICAgICBVSVJPT1QuYWRkQ2hpbGQodGhpcy5pbnN0YW5jZS5fbmRGaXhlZCA9IG5ldyBjYy5Ob2RlKFN5c0RlZmluZS5TWVNfRklYRURfTk9ERSkpO1xuICAgICAgICAgICAgVUlST09ULmFkZENoaWxkKHRoaXMuaW5zdGFuY2UuX25kUG9wVXAgPSBuZXcgY2MuTm9kZShTeXNEZWZpbmUuU1lTX1BPUFVQX05PREUpKTtcbiAgICAgICAgICAgIFVJUk9PVC5hZGRDaGlsZCh0aGlzLmluc3RhbmNlLl9uZFRpcHMgPSBuZXcgY2MuTm9kZShTeXNEZWZpbmUuU1lTX1RPUFRJUFNfTk9ERSkpO1xuICAgICAgICAgICAgY2MuZGlyZWN0b3Iub25jZShjYy5EaXJlY3Rvci5FVkVOVF9CRUZPUkVfU0NFTkVfTEFVTkNILCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5pbnN0YW5jZSA9IG51bGw7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZTtcbiAgICB9XG5cbiAgICAvKiog6aKE5Yqg6L29VUlGb3JtICovXG4gICAgcHVibGljIGFzeW5jIGxvYWRVSUZvcm0ocHJlZmFiUGF0aDogc3RyaW5nKSB7XG4gICAgICAgIGxldCB1aUJhc2UgPSBhd2FpdCB0aGlzLmxvYWRGb3JtKHByZWZhYlBhdGgpO1xuICAgICAgICBpZiAoIXVpQmFzZSkge1xuICAgICAgICAgICAgY29uc29sZS53YXJuKGAke3VpQmFzZX3msqHmnInooqvmiJDlip/liqDovb1gKTtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB1aUJhc2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5Yqg6L295pi+56S65LiA5LiqVUlGb3JtXG4gICAgICogQHBhcmFtIHByZWZhYlBhdGggXG4gICAgICogQHBhcmFtIG9iaiDliJ3lp4vljJbkv6Hmga8sIOWPr+S7peS4jeimgVxuICAgICAqL1xuICAgIHB1YmxpYyBhc3luYyBvcGVuRm9ybShwcmVmYWJQYXRoOiBzdHJpbmcsIHBhcmFtcz86IGFueSwgZm9ybURhdGE/OiBJRm9ybURhdGEpIHtcbiAgICAgICAgaWYgKCFwcmVmYWJQYXRoIHx8IHByZWZhYlBhdGgubGVuZ3RoIDw9IDApIHtcbiAgICAgICAgICAgIGNjLndhcm4oYCR7cHJlZmFiUGF0aH0sIOWPguaVsOmUmeivr2ApO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmNoZWNrRm9ybVNob3dpbmcocHJlZmFiUGF0aCkpIHtcbiAgICAgICAgICAgIGNjLndhcm4oYCR7cHJlZmFiUGF0aH0sIOeql+S9k+ato+WcqOaYvuekuuS4rWApO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGNvbSA9IGF3YWl0IHRoaXMubG9hZEZvcm0ocHJlZmFiUGF0aCk7XG4gICAgICAgIGlmICghY29tKSB7XG4gICAgICAgICAgICBjYy53YXJuKGAke3ByZWZhYlBhdGh9IOWKoOi9veWksei0peS6hiFgKTtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIC8vIOWIneWni+WMlueql+S9k+WQjeensFxuICAgICAgICBjb20uZmlkID0gcHJlZmFiUGF0aDtcbiAgICAgICAgY29tLmZvcm1EYXRhID0gZm9ybURhdGE7XG5cbiAgICAgICAgc3dpdGNoIChjb20uZm9ybVR5cGUpIHtcbiAgICAgICAgICAgIGNhc2UgRm9ybVR5cGUuU2NyZWVuOlxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuZW50ZXJUb1NjcmVlbihjb20uZmlkLCBwYXJhbXMpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBGb3JtVHlwZS5GaXhlZDpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmVudGVyVG9GaXhlZChjb20uZmlkLCBwYXJhbXMpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBGb3JtVHlwZS5XaW5kb3c6XG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5lbnRlclRvUG9wdXAoY29tLmZpZCwgcGFyYW1zKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgRm9ybVR5cGUuVGlwczogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8g54us56uL5pi+56S6XG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5lbnRlclRvVGlwcyhjb20uZmlkLCBwYXJhbXMpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGNvbTtcbiAgICB9XG4gICAgLyoqXG4gICAgICog6YeN6KaB5pa55rOVIOWFs+mXreS4gOS4qlVJRm9ybVxuICAgICAqIEBwYXJhbSBwcmVmYWJQYXRoIFxuICAgICAqL1xuICAgIHB1YmxpYyBhc3luYyBjbG9zZUZvcm0ocHJlZmFiUGF0aDogc3RyaW5nKSB7XG4gICAgICAgIGlmICghcHJlZmFiUGF0aCB8fCBwcmVmYWJQYXRoLmxlbmd0aCA8PSAwKSB7XG4gICAgICAgICAgICBjYy53YXJuKGAke3ByZWZhYlBhdGh9LCDlj4LmlbDplJnor69gKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfTtcbiAgICAgICAgbGV0IGNvbSA9IHRoaXMuX2FsbEZvcm1zW3ByZWZhYlBhdGhdO1xuICAgICAgICBpZiAoIWNvbSkgcmV0dXJuIGZhbHNlO1xuXG4gICAgICAgIHN3aXRjaCAoY29tLmZvcm1UeXBlKSB7XG4gICAgICAgICAgICBjYXNlIEZvcm1UeXBlLlNjcmVlbjpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmV4aXRUb1NjcmVlbihwcmVmYWJQYXRoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgRm9ybVR5cGUuRml4ZWQ6ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyDmma7pgJrmqKHlvI/mmL7npLpcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmV4aXRUb0ZpeGVkKHByZWZhYlBhdGgpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBGb3JtVHlwZS5XaW5kb3c6XG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5leGl0VG9Qb3B1cChwcmVmYWJQYXRoKTtcbiAgICAgICAgICAgICAgICBFdmVudENlbnRlci5lbWl0KEV2ZW50VHlwZS5XaW5kb3dDbG9zZWQsIHByZWZhYlBhdGgpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBGb3JtVHlwZS5UaXBzOlxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuZXhpdFRvVGlwcyhwcmVmYWJQYXRoKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuXG4gICAgICAgIEV2ZW50Q2VudGVyLmVtaXQoRXZlbnRUeXBlLkZvcm1DbG9zZWQsIHByZWZhYlBhdGgpO1xuXG4gICAgICAgIGlmIChjb20uZm9ybURhdGEpIHtcbiAgICAgICAgICAgIGNvbS5mb3JtRGF0YS5vbkNsb3NlICYmIGNvbS5mb3JtRGF0YS5vbkNsb3NlKCk7XG4gICAgICAgIH1cbiAgICAgICAgLy8g5Yik5pat5piv5ZCm6ZSA5q+B6K+l56qX5L2TXG4gICAgICAgIGlmIChjb20ud2lsbERlc3RvcnkpIHtcbiAgICAgICAgICAgIHRoaXMuZGVzdG9yeUZvcm0oY29tKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDku47nqpflj6PnvJPlrZjkuK3liqDovb0o5aaC5p6c5rKh5pyJ5bCx5Lya5ZyobG9hZOWKoOi9vSksIOW5tuaMgui9veWIsOe7k+eCueS4ilxuICAgICAqL1xuICAgIHByaXZhdGUgYXN5bmMgbG9hZEZvcm0ocHJlZmFiUGF0aDogc3RyaW5nKTogUHJvbWlzZTxVSUJhc2U+IHtcbiAgICAgICAgbGV0IGNvbSA9IHRoaXMuX2FsbEZvcm1zW3ByZWZhYlBhdGhdO1xuICAgICAgICBpZiAoY29tKSByZXR1cm4gY29tO1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgaWYgKHRoaXMuX2xvYWRpbmdGb3JtW3ByZWZhYlBhdGhdKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fbG9hZGluZ0Zvcm1bcHJlZmFiUGF0aF0ucHVzaChyZXNvbHZlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLl9sb2FkaW5nRm9ybVtwcmVmYWJQYXRoXSA9IFtyZXNvbHZlXTtcbiAgICAgICAgICAgIHRoaXMuX2RvTG9hZFVJRm9ybShwcmVmYWJQYXRoKS50aGVuKChjb206IFVJQmFzZSkgPT4ge1xuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgZnVuYyBvZiB0aGlzLl9sb2FkaW5nRm9ybVtwcmVmYWJQYXRoXSkge1xuICAgICAgICAgICAgICAgICAgICBmdW5jKGNvbSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMuX2xvYWRpbmdGb3JtW3ByZWZhYlBhdGhdID0gbnVsbDtcbiAgICAgICAgICAgICAgICBkZWxldGUgdGhpcy5fbG9hZGluZ0Zvcm1bcHJlZmFiUGF0aF07XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog5LuOcmVzb3VyY2Vz5Lit5Yqg6L29XG4gICAgICogQHBhcmFtIHByZWZhYlBhdGggXG4gICAgICovXG4gICAgcHJpdmF0ZSBhc3luYyBfZG9Mb2FkVUlGb3JtKHByZWZhYlBhdGg6IHN0cmluZykge1xuICAgICAgICBsZXQgbm9kZSA9IGF3YWl0IFJlc01nci5pbnN0LmxvYWRGb3JtKHByZWZhYlBhdGgpO1xuICAgICAgICBsZXQgY29tID0gbm9kZS5nZXRDb21wb25lbnQoVUlCYXNlKTtcbiAgICAgICAgaWYgKCFjb20pIHtcbiAgICAgICAgICAgIGNjLndhcm4oYCR7cHJlZmFiUGF0aH0g57uT54K55rKh5pyJ57uR5a6aVUlCYXNlYCk7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICBub2RlLmFjdGl2ZSA9IGZhbHNlOyAgICAgICAgICAgICAgICAgICAgLy8g6YG/5YWNYmFzZUNvbeiwg+eUqOS6hm9ubG9hZOaWueazlVxuICAgICAgICBub2RlLmFkZENvbXBvbmVudChDb250ZW50QWRhcHRlcik7XG4gICAgICAgIHN3aXRjaCAoY29tLmZvcm1UeXBlKSB7XG4gICAgICAgICAgICBjYXNlIEZvcm1UeXBlLlNjcmVlbjpcbiAgICAgICAgICAgICAgICB0aGlzLl9uZFNjcmVlbi5hZGRDaGlsZChub2RlKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgRm9ybVR5cGUuRml4ZWQ6XG4gICAgICAgICAgICAgICAgdGhpcy5fbmRGaXhlZC5hZGRDaGlsZChub2RlKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgRm9ybVR5cGUuV2luZG93OlxuICAgICAgICAgICAgICAgIHRoaXMuX25kUG9wVXAuYWRkQ2hpbGQobm9kZSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIEZvcm1UeXBlLlRpcHM6XG4gICAgICAgICAgICAgICAgdGhpcy5fbmRUaXBzLmFkZENoaWxkKG5vZGUpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2FsbEZvcm1zW3ByZWZhYlBhdGhdID0gY29tO1xuXG4gICAgICAgIHJldHVybiBjb207XG4gICAgfVxuXG4gICAgLyoqIOa3u+WKoOWIsHNjcmVlbuS4rSAqL1xuICAgIHByaXZhdGUgYXN5bmMgZW50ZXJUb1NjcmVlbihmaWQ6IHN0cmluZywgcGFyYW1zOiBhbnkpIHtcbiAgICAgICAgLy8g5YWz6Zet5YW25LuW5pi+56S655qE56qX5Y+jIFxuICAgICAgICBsZXQgYXJyOiBBcnJheTxQcm9taXNlPGJvb2xlYW4+PiA9IFtdO1xuICAgICAgICBmb3IgKGxldCBrZXkgaW4gdGhpcy5fc2hvd2luZ0Zvcm1zKSB7XG4gICAgICAgICAgICBhcnIucHVzaCh0aGlzLl9zaG93aW5nRm9ybXNba2V5XS5jbG9zZVNlbGYoKSk7XG4gICAgICAgIH1cbiAgICAgICAgYXdhaXQgUHJvbWlzZS5hbGwoYXJyKTtcblxuICAgICAgICBsZXQgY29tID0gdGhpcy5fYWxsRm9ybXNbZmlkXTtcbiAgICAgICAgaWYgKCFjb20pIHJldHVybjtcbiAgICAgICAgdGhpcy5fc2hvd2luZ0Zvcm1zW2ZpZF0gPSBjb207XG5cbiAgICAgICAgLy8gQWRhcHRlck1nci5pbnN0LmFkYXB0ZUJ5VHlwZShBZGFwdGVyVHlwZS5TdHJldGNoSGVpZ2h0IHwgQWRhcHRlclR5cGUuU3RyZXRjaFdpZHRoLCBjb20ubm9kZSk7XG5cbiAgICAgICAgYXdhaXQgY29tLl9wcmVJbml0KHBhcmFtcyk7XG4gICAgICAgIGNvbS5vblNob3cocGFyYW1zKTtcblxuICAgICAgICBhd2FpdCB0aGlzLnNob3dFZmZlY3QoY29tKTtcbiAgICAgICAgY29tLm9uQWZ0ZXJTaG93KHBhcmFtcyk7XG4gICAgfVxuXG4gICAgLyoqIOa3u+WKoOWIsEZpeGVk5LitICovXG4gICAgcHJpdmF0ZSBhc3luYyBlbnRlclRvRml4ZWQoZmlkOiBzdHJpbmcsIHBhcmFtczogYW55KSB7XG4gICAgICAgIGxldCBjb20gPSB0aGlzLl9hbGxGb3Jtc1tmaWRdO1xuICAgICAgICBpZiAoIWNvbSkgcmV0dXJuO1xuICAgICAgICBhd2FpdCBjb20uX3ByZUluaXQocGFyYW1zKTtcblxuICAgICAgICBjb20ub25TaG93KHBhcmFtcyk7XG4gICAgICAgIHRoaXMuX3Nob3dpbmdGb3Jtc1tmaWRdID0gY29tO1xuICAgICAgICBhd2FpdCB0aGlzLnNob3dFZmZlY3QoY29tKTtcbiAgICAgICAgY29tLm9uQWZ0ZXJTaG93KHBhcmFtcyk7XG4gICAgfVxuXG4gICAgLyoqIOa3u+WKoOWIsHBvcHVw5LitICovXG4gICAgcHJpdmF0ZSBhc3luYyBlbnRlclRvUG9wdXAoZmlkOiBzdHJpbmcsIHBhcmFtczogYW55KSB7XG4gICAgICAgIGxldCBjb20gPSB0aGlzLl9hbGxGb3Jtc1tmaWRdIGFzIFVJV2luZG93O1xuICAgICAgICBpZiAoIWNvbSkgcmV0dXJuO1xuICAgICAgICBhd2FpdCBjb20uX3ByZUluaXQocGFyYW1zKTtcblxuICAgICAgICB0aGlzLl93aW5kb3dzLnB1c2goY29tKTtcblxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuX3dpbmRvd3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHRoaXMuX3dpbmRvd3NbaV0ubm9kZS56SW5kZXggPSBpICsgMTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbS5vblNob3cocGFyYW1zKTtcbiAgICAgICAgdGhpcy5fc2hvd2luZ0Zvcm1zW2ZpZF0gPSBjb207XG5cbiAgICAgICAgTW9kYWxNZ3IuaW5zdC5jaGVja01vZGFsV2luZG93KHRoaXMuX3dpbmRvd3MpO1xuICAgICAgICBhd2FpdCB0aGlzLnNob3dFZmZlY3QoY29tKTtcbiAgICAgICAgY29tLm9uQWZ0ZXJTaG93KHBhcmFtcyk7XG4gICAgfVxuXG4gICAgLyoqIOWKoOi9veWIsHRpcHPkuK0gKi9cbiAgICBwcml2YXRlIGFzeW5jIGVudGVyVG9UaXBzKGZpZDogc3RyaW5nLCBwYXJhbXM6IGFueSkge1xuICAgICAgICBsZXQgY29tID0gdGhpcy5fYWxsRm9ybXNbZmlkXTtcbiAgICAgICAgaWYgKCFjb20pIHJldHVybjtcbiAgICAgICAgYXdhaXQgY29tLl9wcmVJbml0KHBhcmFtcyk7XG4gICAgICAgIHRoaXMuX3RpcHNGb3Jtc1tmaWRdID0gY29tO1xuXG4gICAgICAgIGNvbS5vblNob3cocGFyYW1zKTtcbiAgICAgICAgYXdhaXQgdGhpcy5zaG93RWZmZWN0KGNvbSk7XG4gICAgICAgIGNvbS5vbkFmdGVyU2hvdyhwYXJhbXMpO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgZXhpdFRvU2NyZWVuKGZpZDogc3RyaW5nKSB7XG4gICAgICAgIGxldCBjb20gPSB0aGlzLl9zaG93aW5nRm9ybXNbZmlkXTtcbiAgICAgICAgaWYgKCFjb20pIHJldHVybjtcbiAgICAgICAgY29tLm9uSGlkZSgpO1xuICAgICAgICBhd2FpdCB0aGlzLmhpZGVFZmZlY3QoY29tKTtcbiAgICAgICAgY29tLm9uQWZ0ZXJIaWRlKCk7XG5cbiAgICAgICAgdGhpcy5fc2hvd2luZ0Zvcm1zW2ZpZF0gPSBudWxsO1xuICAgICAgICBkZWxldGUgdGhpcy5fc2hvd2luZ0Zvcm1zW2ZpZF07XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBleGl0VG9GaXhlZChmaWQ6IHN0cmluZykge1xuICAgICAgICBsZXQgY29tID0gdGhpcy5fYWxsRm9ybXNbZmlkXTtcbiAgICAgICAgaWYgKCFjb20pIHJldHVybjtcbiAgICAgICAgY29tLm9uSGlkZSgpO1xuICAgICAgICBhd2FpdCB0aGlzLmhpZGVFZmZlY3QoY29tKTtcbiAgICAgICAgY29tLm9uQWZ0ZXJIaWRlKCk7XG5cbiAgICAgICAgdGhpcy5fc2hvd2luZ0Zvcm1zW2ZpZF0gPSBudWxsO1xuICAgICAgICBkZWxldGUgdGhpcy5fc2hvd2luZ0Zvcm1zW2ZpZF07XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBleGl0VG9Qb3B1cChmaWQ6IHN0cmluZykge1xuICAgICAgICBpZiAodGhpcy5fd2luZG93cy5sZW5ndGggPD0gMCkgcmV0dXJuO1xuICAgICAgICBsZXQgY29tOiBVSVdpbmRvdyA9IG51bGw7XG4gICAgICAgIGZvciAobGV0IGkgPSB0aGlzLl93aW5kb3dzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5fd2luZG93c1tpXS5maWQgPT09IGZpZCkge1xuICAgICAgICAgICAgICAgIGNvbSA9IHRoaXMuX3dpbmRvd3NbaV07XG4gICAgICAgICAgICAgICAgdGhpcy5fd2luZG93cy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCFjb20pIHJldHVybjtcblxuICAgICAgICBjb20ub25IaWRlKCk7XG4gICAgICAgIE1vZGFsTWdyLmluc3QuY2hlY2tNb2RhbFdpbmRvdyh0aGlzLl93aW5kb3dzKTtcbiAgICAgICAgYXdhaXQgdGhpcy5oaWRlRWZmZWN0KGNvbSk7XG4gICAgICAgIGNvbS5vbkFmdGVySGlkZSgpO1xuXG4gICAgICAgIHRoaXMuX3Nob3dpbmdGb3Jtc1tmaWRdID0gbnVsbDtcbiAgICAgICAgZGVsZXRlIHRoaXMuX3Nob3dpbmdGb3Jtc1tmaWRdO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgZXhpdFRvVGlwcyhmaWQ6IHN0cmluZykge1xuICAgICAgICBsZXQgY29tID0gdGhpcy5fYWxsRm9ybXNbZmlkXTtcbiAgICAgICAgaWYgKCFjb20pIHJldHVybjtcbiAgICAgICAgY29tLm9uSGlkZSgpO1xuICAgICAgICBhd2FpdCB0aGlzLmhpZGVFZmZlY3QoY29tKTtcbiAgICAgICAgY29tLm9uQWZ0ZXJIaWRlKCk7XG5cbiAgICAgICAgdGhpcy5fdGlwc0Zvcm1zW2ZpZF0gPSBudWxsO1xuICAgICAgICBkZWxldGUgdGhpcy5fdGlwc0Zvcm1zW2ZpZF07XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBzaG93RWZmZWN0KGJhc2VVSTogVUlCYXNlKSB7XG4gICAgICAgIGJhc2VVSS5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIGF3YWl0IGJhc2VVSS5zaG93RWZmZWN0KCk7XG4gICAgfVxuICAgIHByaXZhdGUgYXN5bmMgaGlkZUVmZmVjdChiYXNlVUk6IFVJQmFzZSkge1xuICAgICAgICBhd2FpdCBiYXNlVUkuaGlkZUVmZmVjdCgpO1xuICAgICAgICBiYXNlVUkubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICAvKiog6ZSA5q+BICovXG4gICAgcHJpdmF0ZSBkZXN0b3J5Rm9ybShjb206IFVJQmFzZSkge1xuICAgICAgICBSZXNNZ3IuaW5zdC5kZXN0b3J5RHluYW1pY1Jlcyhjb20uZmlkKTtcbiAgICAgICAgUmVzTWdyLmluc3QuZGVzdG9yeUZvcm0oY29tKTtcbiAgICAgICAgLy8g5LuOYWxsbWFw5Lit5Yig6ZmkXG4gICAgICAgIHRoaXMuX2FsbEZvcm1zW2NvbS5maWRdID0gbnVsbDtcbiAgICAgICAgZGVsZXRlIHRoaXMuX2FsbEZvcm1zW2NvbS5maWRdO1xuICAgIH1cbiAgICAvKiog56qX5L2T5piv5ZCm5q2j5Zyo5pi+56S6ICovXG4gICAgcHVibGljIGNoZWNrRm9ybVNob3dpbmcoZmlkOiBzdHJpbmcpIHtcbiAgICAgICAgbGV0IGNvbSA9IHRoaXMuX2FsbEZvcm1zW2ZpZF07XG4gICAgICAgIGlmICghY29tKSByZXR1cm4gZmFsc2U7XG4gICAgICAgIHJldHVybiBjb20ubm9kZS5hY3RpdmU7XG4gICAgfVxuXG4gICAgLyoqIOeql+S9k+aYr+WQpuato+WcqOWKoOi9vSAqL1xuICAgIHB1YmxpYyBjaGVja0Zvcm1Mb2FkaW5nKHByZWZhYlBhdGg6IHN0cmluZykge1xuICAgICAgICBsZXQgY29tID0gdGhpcy5fbG9hZGluZ0Zvcm1bcHJlZmFiUGF0aF07XG4gICAgICAgIHJldHVybiAhIWNvbTtcbiAgICB9XG5cbiAgICAvKiog6I635b6XQ29tcG9uZW50ICovXG4gICAgcHVibGljIGdldEZvcm0oZklkOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FsbEZvcm1zW2ZJZF07XG4gICAgfVxufVxuXG5pZiAoQ0NfREVCVUcpIHtcbiAgICB3aW5kb3dbJ1VJTWFuYWdlciddID0gVUlNYW5hZ2VyO1xufSJdfQ==