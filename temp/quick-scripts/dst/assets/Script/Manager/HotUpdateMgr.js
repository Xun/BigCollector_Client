
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/HotUpdateMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '78d3a8ppNJDa7ONQzcot2ZE', 'HotUpdateMgr');
// Script/Manager/HotUpdateMgr.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var HotUpdateMgr = /** @class */ (function (_super) {
    __extends(HotUpdateMgr, _super);
    function HotUpdateMgr() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**显示更新信息 */
        _this.label = null;
        /**当前版本信息 */
        _this.versionStr = null;
        /**远程版本资源缓存路径 */
        _this.storagePath = "";
        /**资源管理 */
        _this.am = null;
        /**更新状态 */
        _this.updating = false;
        /**版本资源信息文件 */
        _this.mainifestUrl = null;
        /**远程版本 */
        _this.remoteVersion = "";
        /**热更后的搜索路径 */
        _this.HotUpdateSearchPaths = "HotUpdateSearchPaths";
        /**远程资源缓存路径 */
        _this.remotePath = "remotePath";
        _this.curVersion = "curVersion";
        return _this;
    }
    HotUpdateMgr.prototype.onLoad = function () {
        this.versionStr.string = this.getCurVesion();
        this.storagePath = this.getRootPath();
        cc.log('远程版本缓存路径 : ' + this.storagePath);
        this.am = new jsb.AssetsManager('', this.storagePath, this.versionCompareHanle);
        this.am.setVerifyCallback(this.setVerifycb.bind(this));
        if (cc.sys.isNative) {
            this.checkUpdate();
        }
    };
    HotUpdateMgr.prototype.setVerifycb = function (path, asset) {
        var compressed = asset.compressed;
        var expectedMD5 = asset.md5;
        var relativePath = asset.path;
        var size = asset.size;
        cc.log("assetPath", path);
        cc.log("assetSize:", size);
        if (compressed) {
            this.label.string = "检查压缩 : " + relativePath;
            return true;
        }
        else {
            this.label.string = "检查压缩 : " + relativePath + ' (' + expectedMD5 + ')';
            return true;
        }
    };
    /**比较版本 版本不同则更新*/
    HotUpdateMgr.prototype.versionCompareHanle = function (versionA, versionB) {
        console.log("\u5F53\u524D\u7248\u672C :  " + versionA + " , \u8FDC\u7A0B\u7248\u672C : " + versionB);
        var vA = versionA.split('.');
        var vB = versionB.split('.');
        for (var i = 0; i < vA.length && i < vB.length; ++i) {
            var a = parseInt(vA[i]);
            var b = parseInt(vB[i]);
            if (a === b) {
                continue;
            }
            else {
                return -1;
            }
        }
        if (vB.length > vA.length) {
            return -1;
        }
        return 0;
    };
    HotUpdateMgr.prototype.checkUpdate = function () {
        if (this.updating) {
            return;
        }
        var url = this.mainifestUrl.nativeUrl;
        cc.log("原包版本信息url:", this.mainifestUrl.nativeUrl);
        if (this.am.getState() === jsb.AssetsManager.State.UNINITED) {
            if (cc.loader.md5Pipe) {
                url = cc.loader.md5Pipe.transformURL(url);
            }
            this.am.loadLocalManifest(url);
        }
        if (!this.am.getLocalManifest() || !this.am.getLocalManifest().isLoaded()) {
            this.label.string = '加载本地manifest文件失败';
            return;
        }
        this.am.setEventCallback(this.checkCb.bind(this));
        this.am.checkUpdate();
        this.updating = true;
    };
    HotUpdateMgr.prototype.checkCb = function (event) {
        cc.log('Code: ' + event.getEventCode());
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                this.label.string = "没有本地manifest文件，跳过热更.";
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                this.label.string = "下载远程manifest文件失败，跳过热更.";
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                this.label.string = "已经更新到远程最新版本.";
                break;
            case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                this.label.string = '发现新版本，请尝试热更';
                break;
            default:
                return;
        }
        this.am.setEventCallback(null);
        this.updating = false;
    };
    HotUpdateMgr.prototype.hotUpdate = function () {
        if (this.am && !this.updating) {
            this.am.setEventCallback(this.updateCb.bind(this));
            if (this.am.getState() === jsb.AssetsManager.State.UNINITED) {
                var url = this.mainifestUrl.nativeUrl;
                if (cc.loader.md5Pipe) {
                    url = cc.loader.md5Pipe.transformURL(url);
                }
                this.am.loadLocalManifest(url);
            }
            this.am.update();
            this.updating = true;
        }
    };
    HotUpdateMgr.prototype.updateCb = function (event) {
        cc.log("热更回调");
        var needRestart = false;
        var failed = false;
        var mm = event.getMessage();
        if (mm) {
            this.label.string = 'Updated file: ' + mm;
        }
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                this.label.string = '没有本地manifest文件，跳过热更.';
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                console.log("当前下载文件数", event.getDownloadedFiles());
                console.log("总文件数", event.getTotalFiles());
                var msg = event.getMessage();
                if (msg) {
                    this.label.string = '更新的文件：: ' + msg;
                }
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                this.label.string = '下载远程manifest文件失败，跳过热更.';
                failed = true;
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                this.label.string = '已经更新到远程最新版本.';
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FINISHED:
                this.label.string = '更新完成，即将重启游戏. ' + event.getMessage();
                needRestart = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FAILED:
                this.label.string = '更新失败. ' + event.getMessage();
                this.updating = false;
                // this._canRetry = true;
                break;
            case jsb.EventAssetsManager.ERROR_UPDATING:
                this.label.string = 'Asset 更新错误: ' + event.getAssetId() + ', ' + event.getMessage();
                break;
            case jsb.EventAssetsManager.ERROR_DECOMPRESS:
                this.label.string = event.getMessage();
                break;
            default:
                break;
        }
        if (failed) {
            this.updating = false;
        }
        /**重启游戏，并把缓存资源路径前置到搜索路径 */
        if (needRestart) {
            this.remoteVersion = this.getRemoteVersion();
            cc.log("remoteversion:", this.remoteVersion);
            this.am.setEventCallback(null);
            var searchPaths = jsb.fileUtils.getSearchPaths();
            var newPaths = this.am.getLocalManifest().getSearchPaths();
            Array.prototype.unshift(searchPaths, newPaths);
            cc.sys.localStorage.setItem(this.HotUpdateSearchPaths, JSON.stringify(searchPaths));
            jsb.fileUtils.setSearchPaths(searchPaths);
            cc.sys.localStorage.setItem(this.curVersion, this.remoteVersion);
            cc.game.restart();
        }
    };
    HotUpdateMgr.prototype.getRemoteVersion = function () {
        var storagePath = this.getRootPath();
        console.log("有下载的manifest文件", storagePath);
        var loadManifest = jsb.fileUtils.getStringFromFile(storagePath + '/project.manifest');
        var manifestObject = JSON.parse(loadManifest);
        return manifestObject.version;
    };
    /**获取缓存路径 */
    HotUpdateMgr.prototype.getRootPath = function () {
        return ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + this.remotePath);
    };
    /**
     * 获取版本号
     * 如果已存有版本号则直接返回，如果没有则在本地manifest文件中取版本号；
     */
    HotUpdateMgr.prototype.getCurVesion = function () {
        var curversion = cc.sys.localStorage.getItem(this.curVersion);
        cc.log("curversion", curversion);
        if (curversion)
            return curversion;
        var storagePath = this.getRootPath();
        storagePath = this.mainifestUrl.nativeUrl;
        if (storagePath) {
            var loadManifest = jsb.fileUtils.getStringFromFile(storagePath);
            var manifestObject = JSON.parse(loadManifest);
            curversion = manifestObject.version;
            console.log("当前版本号origin：", curversion);
        }
        return curversion;
    };
    /**获取远程动态 url和版本号 ，版本号相同，直接进入游戏，否则重新设置project.manifest地址数据 然后更新 */
    HotUpdateMgr.prototype.configUpdate = function () {
        cc.sys.localStorage.removeItem("curVersion");
        var curVersion = this.getCurVesion();
        var remoteVersion = "1.1.0";
        // 大版本号
        // curVersion第一个数字为主版本号，如果大版本大于本地版本则重新下载包（大版本更新部分）
        var curMainId = 1;
        var bigVersion = 1;
        if (bigVersion > curMainId) {
            var rootpath = this.getRootPath();
            //删掉之前大版本的缓存路径
            jsb.fileUtils.removeDirectory(rootpath);
            //清除缓存的版本号 和 版本路径  
            cc.sys.localStorage.removeItem("curversion");
            var packageUrl = "apk/api地址";
            cc.sys.openURL(packageUrl);
            return;
        }
        //下面是 资源的更新检查（小版本更新部分）
        var needcheck = this.versionCompareHanle(curVersion, remoteVersion);
        // 本地版本和远程版本 相同直接跳转登陆, 这里 加判断是为了 防止每次都下载 manifest文件
        if (needcheck == 0) {
            console.log("小版本相同 进入登陆界面");
            return;
        }
        //拼接远程资源的下载地址
        var scripturl = "远程服务器地址";
        this._modifyAppLoadUrlForManifestFile(scripturl, this.mainifestUrl.nativeUrl);
    };
    /**
    * 修改.manifest文件,如果有缓存则比较缓存的版本和应用包的版本，取较高者 热更，如果没有缓存则下载远程的版本文件 取版本号，这里统一修改地址为
    * 远程地址，不管 如何变化 都是从最新的地址下载版本文件。这里远程
    * @param {新的升级包地址} newAppHotUpdateUrl
    * @param {本地project.manifest文件地址} localManifestPath
    */
    HotUpdateMgr.prototype._modifyAppLoadUrlForManifestFile = function (newAppHotUpdateUrl, localManifestPath) {
        var isWritten = false;
        if (jsb.fileUtils.isFileExist(jsb.fileUtils.getWritablePath() + 'plane/project.manifest')) {
            var storagePath = this.getRootPath();
            console.log("有下载的manifest文件", storagePath);
            var loadManifest = jsb.fileUtils.getStringFromFile(storagePath + '/project.manifest');
            var manifestObject = JSON.parse(loadManifest);
            manifestObject.packageUrl = newAppHotUpdateUrl;
            manifestObject.remoteManifestUrl = manifestObject.packageUrl + "project.manifest";
            manifestObject.remoteVersionUrl = manifestObject.packageUrl + "version.manifest";
            var afterString = JSON.stringify(manifestObject);
            isWritten = jsb.fileUtils.writeStringToFile(afterString, storagePath + '/project.manifest');
        }
        else {
            /**
             * 执行到这里说明App之前没有进行过热更，所以不存在热更的plane文件夹。
             */
            /**
             * plane文件夹不存在的时候，我们就主动创建“plane”文件夹，并将打包时候的project.manifest文件中升级包地址修改后，存放到“plane”文件夹下面。
             */
            var initializedManifestPath = this.getRootPath();
            if (!jsb.fileUtils.isDirectoryExist(initializedManifestPath)) {
                jsb.fileUtils.createDirectory(initializedManifestPath);
            }
            //修改原始manifest文件
            var originManifestPath = localManifestPath;
            var originManifest = jsb.fileUtils.getStringFromFile(originManifestPath);
            var originManifestObject = JSON.parse(originManifest);
            originManifestObject.packageUrl = newAppHotUpdateUrl;
            originManifestObject.remoteManifestUrl = originManifestObject.packageUrl + 'project.manifest';
            originManifestObject.remoteVersionUrl = originManifestObject.packageUrl + 'version.manifest';
            var afterString = JSON.stringify(originManifestObject);
            isWritten = jsb.fileUtils.writeStringToFile(afterString, initializedManifestPath + '/project.manifest');
        }
        cc.log("Written Status : ", isWritten);
        if (isWritten) {
            this.checkUpdate();
        }
    };
    __decorate([
        property(cc.Label)
    ], HotUpdateMgr.prototype, "label", void 0);
    __decorate([
        property(cc.Label)
    ], HotUpdateMgr.prototype, "versionStr", void 0);
    __decorate([
        property(cc.Asset)
    ], HotUpdateMgr.prototype, "mainifestUrl", void 0);
    HotUpdateMgr = __decorate([
        ccclass
    ], HotUpdateMgr);
    return HotUpdateMgr;
}(cc.Component));
exports.default = HotUpdateMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9Ib3RVcGRhdGVNZ3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRU0sSUFBQSxLQUF3QixFQUFFLENBQUMsVUFBVSxFQUFuQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWtCLENBQUM7QUFHNUM7SUFBMEMsZ0NBQVk7SUFBdEQ7UUFBQSxxRUF3VEM7UUF2VEcsWUFBWTtRQUVaLFdBQUssR0FBYSxJQUFJLENBQUM7UUFDdkIsWUFBWTtRQUVaLGdCQUFVLEdBQWEsSUFBSSxDQUFDO1FBQzVCLGdCQUFnQjtRQUNoQixpQkFBVyxHQUFXLEVBQUUsQ0FBQztRQUN6QixVQUFVO1FBQ1YsUUFBRSxHQUFzQixJQUFJLENBQUM7UUFDN0IsVUFBVTtRQUNWLGNBQVEsR0FBWSxLQUFLLENBQUM7UUFDMUIsY0FBYztRQUVkLGtCQUFZLEdBQWEsSUFBSSxDQUFDO1FBQzlCLFVBQVU7UUFDRixtQkFBYSxHQUFXLEVBQUUsQ0FBQztRQUNuQyxjQUFjO1FBQ04sMEJBQW9CLEdBQVcsc0JBQXNCLENBQUM7UUFDOUQsY0FBYztRQUNOLGdCQUFVLEdBQVcsWUFBWSxDQUFDO1FBQ2xDLGdCQUFVLEdBQVcsWUFBWSxDQUFDOztJQWtTOUMsQ0FBQztJQWhTRyw2QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzdDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksR0FBRyxDQUFDLGFBQWEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNoRixJQUFJLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDdkQsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDdEI7SUFDTCxDQUFDO0lBRUQsa0NBQVcsR0FBWCxVQUFZLElBQUksRUFBRSxLQUFLO1FBQ25CLElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7UUFDbEMsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUM1QixJQUFJLFlBQVksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQzlCLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDdEIsRUFBRSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUE7UUFDekIsRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxVQUFVLEVBQUU7WUFDWixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxTQUFTLEdBQUcsWUFBWSxDQUFDO1lBQzdDLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7YUFDSTtZQUNELElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsR0FBRyxZQUFZLEdBQUcsSUFBSSxHQUFHLFdBQVcsR0FBRyxHQUFHLENBQUM7WUFDeEUsT0FBTyxJQUFJLENBQUM7U0FDZjtJQUNMLENBQUM7SUFDRCxpQkFBaUI7SUFDakIsMENBQW1CLEdBQW5CLFVBQW9CLFFBQWdCLEVBQUUsUUFBZ0I7UUFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBVyxRQUFRLHNDQUFhLFFBQVUsQ0FBQyxDQUFDO1FBQ3hELElBQUksRUFBRSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDN0IsSUFBSSxFQUFFLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM3QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtZQUNqRCxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDVCxTQUFTO2FBQ1o7aUJBQ0k7Z0JBQ0QsT0FBTyxDQUFDLENBQUMsQ0FBQzthQUNiO1NBQ0o7UUFDRCxJQUFJLEVBQUUsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRTtZQUN2QixPQUFPLENBQUMsQ0FBQyxDQUFDO1NBQ2I7UUFDRCxPQUFPLENBQUMsQ0FBQztJQUNiLENBQUM7SUFJRCxrQ0FBVyxHQUFYO1FBQ0ksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsT0FBTztTQUNWO1FBQ0QsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7UUFDdEMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUNqRCxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEtBQUssR0FBRyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO1lBQ3pELElBQUksRUFBRSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUU7Z0JBQ25CLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDN0M7WUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ2xDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUN2RSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQztZQUN2QyxPQUFPO1NBQ1Y7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDO0lBRUQsOEJBQU8sR0FBUCxVQUFRLEtBQUs7UUFDVCxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztRQUN4QyxRQUFRLEtBQUssQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUMxQixLQUFLLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyx1QkFBdUI7Z0JBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLHNCQUFzQixDQUFDO2dCQUMzQyxNQUFNO1lBQ1YsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCLENBQUM7WUFDcEQsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsb0JBQW9CO2dCQUM1QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyx3QkFBd0IsQ0FBQztnQkFDN0MsTUFBTTtZQUNWLEtBQUssR0FBRyxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQjtnQkFDMUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsY0FBYyxDQUFDO2dCQUNuQyxNQUFNO1lBQ1YsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCO2dCQUN6QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxhQUFhLENBQUM7Z0JBQ2xDLE1BQU07WUFDVjtnQkFDSSxPQUFPO1NBQ2Q7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFFRCxnQ0FBUyxHQUFUO1FBQ0ksSUFBSSxJQUFJLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUMzQixJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbkQsSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxLQUFLLEdBQUcsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRTtnQkFDekQsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7Z0JBQ3RDLElBQUksRUFBRSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ25CLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzdDO2dCQUNELElBQUksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDbEM7WUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQztJQUVELCtCQUFRLEdBQVIsVUFBUyxLQUFLO1FBQ1YsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUNkLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxFQUFFLEdBQUcsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQzVCLElBQUksRUFBRSxFQUFFO1lBQ0osSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1NBQzdDO1FBQ0QsUUFBUSxLQUFLLENBQUMsWUFBWSxFQUFFLEVBQUU7WUFFMUIsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCO2dCQUMvQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQztnQkFDM0MsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDZCxNQUFNO1lBQ1YsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCO2dCQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFBO2dCQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQTtnQkFDMUMsSUFBSSxHQUFHLEdBQUcsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUM3QixJQUFJLEdBQUcsRUFBRTtvQkFDTCxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxVQUFVLEdBQUcsR0FBRyxDQUFDO2lCQUN4QztnQkFDRCxNQUFNO1lBQ1YsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCLENBQUM7WUFDcEQsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsb0JBQW9CO2dCQUM1QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyx3QkFBd0IsQ0FBQztnQkFDN0MsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDZCxNQUFNO1lBQ1YsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCO2dCQUMxQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxjQUFjLENBQUM7Z0JBQ25DLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ2QsTUFBTTtZQUNWLEtBQUssR0FBRyxDQUFDLGtCQUFrQixDQUFDLGVBQWU7Z0JBQ3ZDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLGVBQWUsR0FBRyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ3pELFdBQVcsR0FBRyxJQUFJLENBQUM7Z0JBQ25CLE1BQU07WUFDVixLQUFLLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhO2dCQUNyQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxRQUFRLEdBQUcsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNsRCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztnQkFDdEIseUJBQXlCO2dCQUN6QixNQUFNO1lBQ1YsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsY0FBYztnQkFDdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsY0FBYyxHQUFHLEtBQUssQ0FBQyxVQUFVLEVBQUUsR0FBRyxJQUFJLEdBQUcsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNwRixNQUFNO1lBQ1YsS0FBSyxHQUFHLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCO2dCQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ3ZDLE1BQU07WUFDVjtnQkFDSSxNQUFNO1NBQ2I7UUFHRCxJQUFJLE1BQU0sRUFBRTtZQUNSLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1NBQ3pCO1FBQ0QsMEJBQTBCO1FBQzFCLElBQUksV0FBVyxFQUFFO1lBQ2IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUM3QyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtZQUM1QyxJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9CLElBQUksV0FBVyxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDakQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzNELEtBQUssQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUMvQyxFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNwRixHQUFHLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMxQyxFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7WUFDaEUsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNyQjtJQUNMLENBQUM7SUFFRCx1Q0FBZ0IsR0FBaEI7UUFDSSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUMzQyxJQUFJLFlBQVksR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3RGLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDOUMsT0FBTyxjQUFjLENBQUMsT0FBTyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxZQUFZO0lBQ1osa0NBQVcsR0FBWDtRQUNJLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN2RixDQUFDO0lBQ0Q7OztPQUdHO0lBQ0gsbUNBQVksR0FBWjtRQUNJLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDOUQsRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDLENBQUE7UUFDaEMsSUFBSSxVQUFVO1lBQUUsT0FBTyxVQUFVLENBQUE7UUFDakMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztRQUMxQyxJQUFJLFdBQVcsRUFBRTtZQUNiLElBQUksWUFBWSxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDaEUsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUM5QyxVQUFVLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQztZQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsQ0FBQTtTQUMxQztRQUNELE9BQU8sVUFBVSxDQUFDO0lBQ3RCLENBQUM7SUFHRCxrRUFBa0U7SUFDbEUsbUNBQVksR0FBWjtRQUNJLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUM1QyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckMsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFBO1FBQzNCLE9BQU87UUFDUCxrREFBa0Q7UUFDbEQsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFBO1FBQ2pCLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQTtRQUNsQixJQUFJLFVBQVUsR0FBRyxTQUFTLEVBQUU7WUFDeEIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ2xDLGNBQWM7WUFDZCxHQUFHLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN4QyxtQkFBbUI7WUFDbkIsRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzdDLElBQUksVUFBVSxHQUFHLFdBQVcsQ0FBQztZQUM3QixFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMzQixPQUFPO1NBQ1Y7UUFDRCxzQkFBc0I7UUFDdEIsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUNwRSxtREFBbUQ7UUFDbkQsSUFBSSxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUE7WUFDM0IsT0FBTztTQUNWO1FBQ0QsYUFBYTtRQUNiLElBQUksU0FBUyxHQUFHLFNBQVMsQ0FBQTtRQUN6QixJQUFJLENBQUMsZ0NBQWdDLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDbEYsQ0FBQztJQUNEOzs7OztNQUtFO0lBQ0YsdURBQWdDLEdBQWhDLFVBQWlDLGtCQUFrQixFQUFFLGlCQUFpQjtRQUNsRSxJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLGVBQWUsRUFBRSxHQUFHLHdCQUF3QixDQUFDLEVBQUU7WUFDdkYsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDM0MsSUFBSSxZQUFZLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsbUJBQW1CLENBQUMsQ0FBQztZQUN0RixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzlDLGNBQWMsQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLENBQUM7WUFDL0MsY0FBYyxDQUFDLGlCQUFpQixHQUFHLGNBQWMsQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLENBQUM7WUFDbEYsY0FBYyxDQUFDLGdCQUFnQixHQUFHLGNBQWMsQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLENBQUM7WUFDakYsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNqRCxTQUFTLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsV0FBVyxHQUFHLG1CQUFtQixDQUFDLENBQUM7U0FDL0Y7YUFBTTtZQUNIOztlQUVHO1lBRUg7O2VBRUc7WUFDSCxJQUFJLHVCQUF1QixHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFO2dCQUMxRCxHQUFHLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO2FBQzFEO1lBQ0QsZ0JBQWdCO1lBQ2hCLElBQUksa0JBQWtCLEdBQUcsaUJBQWlCLENBQUM7WUFDM0MsSUFBSSxjQUFjLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3pFLElBQUksb0JBQW9CLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN0RCxvQkFBb0IsQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLENBQUM7WUFDckQsb0JBQW9CLENBQUMsaUJBQWlCLEdBQUcsb0JBQW9CLENBQUMsVUFBVSxHQUFHLGtCQUFrQixDQUFDO1lBQzlGLG9CQUFvQixDQUFDLGdCQUFnQixHQUFHLG9CQUFvQixDQUFDLFVBQVUsR0FBRyxrQkFBa0IsQ0FBQztZQUU3RixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDdkQsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLHVCQUF1QixHQUFHLG1CQUFtQixDQUFDLENBQUM7U0FDM0c7UUFDRCxFQUFFLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksU0FBUyxFQUFFO1lBQ1gsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3RCO0lBRUwsQ0FBQztJQW5URDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOytDQUNJO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7b0RBQ1M7SUFTNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztzREFDVztJQWZiLFlBQVk7UUFEaEMsT0FBTztPQUNhLFlBQVksQ0F3VGhDO0lBQUQsbUJBQUM7Q0F4VEQsQUF3VEMsQ0F4VHlDLEVBQUUsQ0FBQyxTQUFTLEdBd1RyRDtrQkF4VG9CLFlBQVkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgRm9ybU1nciBmcm9tIFwiLi9Gb3JtTWdyXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBIb3RVcGRhdGVNZ3IgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuICAgIC8qKuaYvuekuuabtOaWsOS/oeaBryAqL1xuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBsYWJlbDogY2MuTGFiZWwgPSBudWxsO1xuICAgIC8qKuW9k+WJjeeJiOacrOS/oeaBryAqL1xuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICB2ZXJzaW9uU3RyOiBjYy5MYWJlbCA9IG51bGw7XG4gICAgLyoq6L+c56iL54mI5pys6LWE5rqQ57yT5a2Y6Lev5b6EICovXG4gICAgc3RvcmFnZVBhdGg6IHN0cmluZyA9IFwiXCI7XG4gICAgLyoq6LWE5rqQ566h55CGICovXG4gICAgYW06IGpzYi5Bc3NldHNNYW5hZ2VyID0gbnVsbDtcbiAgICAvKirmm7TmlrDnirbmgIEgKi9cbiAgICB1cGRhdGluZzogYm9vbGVhbiA9IGZhbHNlO1xuICAgIC8qKueJiOacrOi1hOa6kOS/oeaBr+aWh+S7tiAqL1xuICAgIEBwcm9wZXJ0eShjYy5Bc3NldClcbiAgICBtYWluaWZlc3RVcmw6IGNjLkFzc2V0ID0gbnVsbDtcbiAgICAvKirov5znqIvniYjmnKwgKi9cbiAgICBwcml2YXRlIHJlbW90ZVZlcnNpb246IHN0cmluZyA9IFwiXCI7XG4gICAgLyoq54Ot5pu05ZCO55qE5pCc57Si6Lev5b6EICovXG4gICAgcHJpdmF0ZSBIb3RVcGRhdGVTZWFyY2hQYXRoczogc3RyaW5nID0gXCJIb3RVcGRhdGVTZWFyY2hQYXRoc1wiO1xuICAgIC8qKui/nOeoi+i1hOa6kOe8k+WtmOi3r+W+hCAqL1xuICAgIHByaXZhdGUgcmVtb3RlUGF0aDogc3RyaW5nID0gXCJyZW1vdGVQYXRoXCI7XG4gICAgcHJpdmF0ZSBjdXJWZXJzaW9uOiBzdHJpbmcgPSBcImN1clZlcnNpb25cIjtcblxuICAgIG9uTG9hZCgpIHtcbiAgICAgICAgdGhpcy52ZXJzaW9uU3RyLnN0cmluZyA9IHRoaXMuZ2V0Q3VyVmVzaW9uKCk7XG4gICAgICAgIHRoaXMuc3RvcmFnZVBhdGggPSB0aGlzLmdldFJvb3RQYXRoKCk7XG4gICAgICAgIGNjLmxvZygn6L+c56iL54mI5pys57yT5a2Y6Lev5b6EIDogJyArIHRoaXMuc3RvcmFnZVBhdGgpO1xuICAgICAgICB0aGlzLmFtID0gbmV3IGpzYi5Bc3NldHNNYW5hZ2VyKCcnLCB0aGlzLnN0b3JhZ2VQYXRoLCB0aGlzLnZlcnNpb25Db21wYXJlSGFubGUpO1xuICAgICAgICB0aGlzLmFtLnNldFZlcmlmeUNhbGxiYWNrKHRoaXMuc2V0VmVyaWZ5Y2IuYmluZCh0aGlzKSk7XG4gICAgICAgIGlmIChjYy5zeXMuaXNOYXRpdmUpIHtcbiAgICAgICAgICAgIHRoaXMuY2hlY2tVcGRhdGUoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNldFZlcmlmeWNiKHBhdGgsIGFzc2V0KSB7XG4gICAgICAgIGxldCBjb21wcmVzc2VkID0gYXNzZXQuY29tcHJlc3NlZDtcbiAgICAgICAgbGV0IGV4cGVjdGVkTUQ1ID0gYXNzZXQubWQ1O1xuICAgICAgICBsZXQgcmVsYXRpdmVQYXRoID0gYXNzZXQucGF0aDtcbiAgICAgICAgbGV0IHNpemUgPSBhc3NldC5zaXplO1xuICAgICAgICBjYy5sb2coXCJhc3NldFBhdGhcIiwgcGF0aClcbiAgICAgICAgY2MubG9nKFwiYXNzZXRTaXplOlwiLCBzaXplKTtcbiAgICAgICAgaWYgKGNvbXByZXNzZWQpIHtcbiAgICAgICAgICAgIHRoaXMubGFiZWwuc3RyaW5nID0gXCLmo4Dmn6XljovnvKkgOiBcIiArIHJlbGF0aXZlUGF0aDtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSBcIuajgOafpeWOi+e8qSA6IFwiICsgcmVsYXRpdmVQYXRoICsgJyAoJyArIGV4cGVjdGVkTUQ1ICsgJyknO1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICB9XG4gICAgLyoq5q+U6L6D54mI5pysIOeJiOacrOS4jeWQjOWImeabtOaWsCovXG4gICAgdmVyc2lvbkNvbXBhcmVIYW5sZSh2ZXJzaW9uQTogc3RyaW5nLCB2ZXJzaW9uQjogc3RyaW5nKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGDlvZPliY3niYjmnKwgOiAgJHt2ZXJzaW9uQX0gLCDov5znqIvniYjmnKwgOiAke3ZlcnNpb25CfWApO1xuICAgICAgICBsZXQgdkEgPSB2ZXJzaW9uQS5zcGxpdCgnLicpO1xuICAgICAgICBsZXQgdkIgPSB2ZXJzaW9uQi5zcGxpdCgnLicpO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHZBLmxlbmd0aCAmJiBpIDwgdkIubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICAgIGxldCBhID0gcGFyc2VJbnQodkFbaV0pO1xuICAgICAgICAgICAgbGV0IGIgPSBwYXJzZUludCh2QltpXSk7XG4gICAgICAgICAgICBpZiAoYSA9PT0gYikge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmICh2Qi5sZW5ndGggPiB2QS5sZW5ndGgpIHtcbiAgICAgICAgICAgIHJldHVybiAtMTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gMDtcbiAgICB9XG5cblxuXG4gICAgY2hlY2tVcGRhdGUoKSB7XG4gICAgICAgIGlmICh0aGlzLnVwZGF0aW5nKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgbGV0IHVybCA9IHRoaXMubWFpbmlmZXN0VXJsLm5hdGl2ZVVybDtcbiAgICAgICAgY2MubG9nKFwi5Y6f5YyF54mI5pys5L+h5oGvdXJsOlwiLCB0aGlzLm1haW5pZmVzdFVybC5uYXRpdmVVcmwpXG4gICAgICAgIGlmICh0aGlzLmFtLmdldFN0YXRlKCkgPT09IGpzYi5Bc3NldHNNYW5hZ2VyLlN0YXRlLlVOSU5JVEVEKSB7XG4gICAgICAgICAgICBpZiAoY2MubG9hZGVyLm1kNVBpcGUpIHtcbiAgICAgICAgICAgICAgICB1cmwgPSBjYy5sb2FkZXIubWQ1UGlwZS50cmFuc2Zvcm1VUkwodXJsKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuYW0ubG9hZExvY2FsTWFuaWZlc3QodXJsKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIXRoaXMuYW0uZ2V0TG9jYWxNYW5pZmVzdCgpIHx8ICF0aGlzLmFtLmdldExvY2FsTWFuaWZlc3QoKS5pc0xvYWRlZCgpKSB7XG4gICAgICAgICAgICB0aGlzLmxhYmVsLnN0cmluZyA9ICfliqDovb3mnKzlnLBtYW5pZmVzdOaWh+S7tuWksei0pSc7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5hbS5zZXRFdmVudENhbGxiYWNrKHRoaXMuY2hlY2tDYi5iaW5kKHRoaXMpKTtcbiAgICAgICAgdGhpcy5hbS5jaGVja1VwZGF0ZSgpO1xuICAgICAgICB0aGlzLnVwZGF0aW5nID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBjaGVja0NiKGV2ZW50KSB7XG4gICAgICAgIGNjLmxvZygnQ29kZTogJyArIGV2ZW50LmdldEV2ZW50Q29kZSgpKTtcbiAgICAgICAgc3dpdGNoIChldmVudC5nZXRFdmVudENvZGUoKSkge1xuICAgICAgICAgICAgY2FzZSBqc2IuRXZlbnRBc3NldHNNYW5hZ2VyLkVSUk9SX05PX0xPQ0FMX01BTklGRVNUOlxuICAgICAgICAgICAgICAgIHRoaXMubGFiZWwuc3RyaW5nID0gXCLmsqHmnInmnKzlnLBtYW5pZmVzdOaWh+S7tu+8jOi3s+i/h+eDreabtC5cIjtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UganNiLkV2ZW50QXNzZXRzTWFuYWdlci5FUlJPUl9ET1dOTE9BRF9NQU5JRkVTVDpcbiAgICAgICAgICAgIGNhc2UganNiLkV2ZW50QXNzZXRzTWFuYWdlci5FUlJPUl9QQVJTRV9NQU5JRkVTVDpcbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVsLnN0cmluZyA9IFwi5LiL6L296L+c56iLbWFuaWZlc3Tmlofku7blpLHotKXvvIzot7Pov4fng63mm7QuXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIGpzYi5FdmVudEFzc2V0c01hbmFnZXIuQUxSRUFEWV9VUF9UT19EQVRFOlxuICAgICAgICAgICAgICAgIHRoaXMubGFiZWwuc3RyaW5nID0gXCLlt7Lnu4/mm7TmlrDliLDov5znqIvmnIDmlrDniYjmnKwuXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIGpzYi5FdmVudEFzc2V0c01hbmFnZXIuTkVXX1ZFUlNJT05fRk9VTkQ6XG4gICAgICAgICAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSAn5Y+R546w5paw54mI5pys77yM6K+35bCd6K+V54Ot5pu0JztcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuYW0uc2V0RXZlbnRDYWxsYmFjayhudWxsKTtcbiAgICAgICAgdGhpcy51cGRhdGluZyA9IGZhbHNlO1xuICAgIH1cblxuICAgIGhvdFVwZGF0ZSgpIHtcbiAgICAgICAgaWYgKHRoaXMuYW0gJiYgIXRoaXMudXBkYXRpbmcpIHtcbiAgICAgICAgICAgIHRoaXMuYW0uc2V0RXZlbnRDYWxsYmFjayh0aGlzLnVwZGF0ZUNiLmJpbmQodGhpcykpO1xuICAgICAgICAgICAgaWYgKHRoaXMuYW0uZ2V0U3RhdGUoKSA9PT0ganNiLkFzc2V0c01hbmFnZXIuU3RhdGUuVU5JTklURUQpIHtcbiAgICAgICAgICAgICAgICBsZXQgdXJsID0gdGhpcy5tYWluaWZlc3RVcmwubmF0aXZlVXJsO1xuICAgICAgICAgICAgICAgIGlmIChjYy5sb2FkZXIubWQ1UGlwZSkge1xuICAgICAgICAgICAgICAgICAgICB1cmwgPSBjYy5sb2FkZXIubWQ1UGlwZS50cmFuc2Zvcm1VUkwodXJsKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5hbS5sb2FkTG9jYWxNYW5pZmVzdCh1cmwpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5hbS51cGRhdGUoKTtcbiAgICAgICAgICAgIHRoaXMudXBkYXRpbmcgPSB0cnVlO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgdXBkYXRlQ2IoZXZlbnQpIHtcbiAgICAgICAgY2MubG9nKFwi54Ot5pu05Zue6LCDXCIpXG4gICAgICAgIHZhciBuZWVkUmVzdGFydCA9IGZhbHNlO1xuICAgICAgICB2YXIgZmFpbGVkID0gZmFsc2U7XG4gICAgICAgIGxldCBtbSA9IGV2ZW50LmdldE1lc3NhZ2UoKTtcbiAgICAgICAgaWYgKG1tKSB7XG4gICAgICAgICAgICB0aGlzLmxhYmVsLnN0cmluZyA9ICdVcGRhdGVkIGZpbGU6ICcgKyBtbTtcbiAgICAgICAgfVxuICAgICAgICBzd2l0Y2ggKGV2ZW50LmdldEV2ZW50Q29kZSgpKSB7XG5cbiAgICAgICAgICAgIGNhc2UganNiLkV2ZW50QXNzZXRzTWFuYWdlci5FUlJPUl9OT19MT0NBTF9NQU5JRkVTVDpcbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVsLnN0cmluZyA9ICfmsqHmnInmnKzlnLBtYW5pZmVzdOaWh+S7tu+8jOi3s+i/h+eDreabtC4nO1xuICAgICAgICAgICAgICAgIGZhaWxlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIGpzYi5FdmVudEFzc2V0c01hbmFnZXIuVVBEQVRFX1BST0dSRVNTSU9OOlxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi5b2T5YmN5LiL6L295paH5Lu25pWwXCIsIGV2ZW50LmdldERvd25sb2FkZWRGaWxlcygpKVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi5oC75paH5Lu25pWwXCIsIGV2ZW50LmdldFRvdGFsRmlsZXMoKSlcbiAgICAgICAgICAgICAgICB2YXIgbXNnID0gZXZlbnQuZ2V0TWVzc2FnZSgpO1xuICAgICAgICAgICAgICAgIGlmIChtc2cpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSAn5pu05paw55qE5paH5Lu277yaOiAnICsgbXNnO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UganNiLkV2ZW50QXNzZXRzTWFuYWdlci5FUlJPUl9ET1dOTE9BRF9NQU5JRkVTVDpcbiAgICAgICAgICAgIGNhc2UganNiLkV2ZW50QXNzZXRzTWFuYWdlci5FUlJPUl9QQVJTRV9NQU5JRkVTVDpcbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVsLnN0cmluZyA9ICfkuIvovb3ov5znqIttYW5pZmVzdOaWh+S7tuWksei0pe+8jOi3s+i/h+eDreabtC4nO1xuICAgICAgICAgICAgICAgIGZhaWxlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIGpzYi5FdmVudEFzc2V0c01hbmFnZXIuQUxSRUFEWV9VUF9UT19EQVRFOlxuICAgICAgICAgICAgICAgIHRoaXMubGFiZWwuc3RyaW5nID0gJ+W3sue7j+abtOaWsOWIsOi/nOeoi+acgOaWsOeJiOacrC4nO1xuICAgICAgICAgICAgICAgIGZhaWxlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIGpzYi5FdmVudEFzc2V0c01hbmFnZXIuVVBEQVRFX0ZJTklTSEVEOlxuICAgICAgICAgICAgICAgIHRoaXMubGFiZWwuc3RyaW5nID0gJ+abtOaWsOWujOaIkO+8jOWNs+WwhumHjeWQr+a4uOaIjy4gJyArIGV2ZW50LmdldE1lc3NhZ2UoKTtcbiAgICAgICAgICAgICAgICBuZWVkUmVzdGFydCA9IHRydWU7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIGpzYi5FdmVudEFzc2V0c01hbmFnZXIuVVBEQVRFX0ZBSUxFRDpcbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVsLnN0cmluZyA9ICfmm7TmlrDlpLHotKUuICcgKyBldmVudC5nZXRNZXNzYWdlKCk7XG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIC8vIHRoaXMuX2NhblJldHJ5ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UganNiLkV2ZW50QXNzZXRzTWFuYWdlci5FUlJPUl9VUERBVElORzpcbiAgICAgICAgICAgICAgICB0aGlzLmxhYmVsLnN0cmluZyA9ICdBc3NldCDmm7TmlrDplJnor686ICcgKyBldmVudC5nZXRBc3NldElkKCkgKyAnLCAnICsgZXZlbnQuZ2V0TWVzc2FnZSgpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBqc2IuRXZlbnRBc3NldHNNYW5hZ2VyLkVSUk9SX0RFQ09NUFJFU1M6XG4gICAgICAgICAgICAgICAgdGhpcy5sYWJlbC5zdHJpbmcgPSBldmVudC5nZXRNZXNzYWdlKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cblxuICAgICAgICBpZiAoZmFpbGVkKSB7XG4gICAgICAgICAgICB0aGlzLnVwZGF0aW5nID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgLyoq6YeN5ZCv5ri45oiP77yM5bm25oqK57yT5a2Y6LWE5rqQ6Lev5b6E5YmN572u5Yiw5pCc57Si6Lev5b6EICovXG4gICAgICAgIGlmIChuZWVkUmVzdGFydCkge1xuICAgICAgICAgICAgdGhpcy5yZW1vdGVWZXJzaW9uID0gdGhpcy5nZXRSZW1vdGVWZXJzaW9uKCk7XG4gICAgICAgICAgICBjYy5sb2coXCJyZW1vdGV2ZXJzaW9uOlwiLCB0aGlzLnJlbW90ZVZlcnNpb24pXG4gICAgICAgICAgICB0aGlzLmFtLnNldEV2ZW50Q2FsbGJhY2sobnVsbCk7XG4gICAgICAgICAgICBsZXQgc2VhcmNoUGF0aHMgPSBqc2IuZmlsZVV0aWxzLmdldFNlYXJjaFBhdGhzKCk7XG4gICAgICAgICAgICBsZXQgbmV3UGF0aHMgPSB0aGlzLmFtLmdldExvY2FsTWFuaWZlc3QoKS5nZXRTZWFyY2hQYXRocygpO1xuICAgICAgICAgICAgQXJyYXkucHJvdG90eXBlLnVuc2hpZnQoc2VhcmNoUGF0aHMsIG5ld1BhdGhzKTtcbiAgICAgICAgICAgIGNjLnN5cy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLkhvdFVwZGF0ZVNlYXJjaFBhdGhzLCBKU09OLnN0cmluZ2lmeShzZWFyY2hQYXRocykpO1xuICAgICAgICAgICAganNiLmZpbGVVdGlscy5zZXRTZWFyY2hQYXRocyhzZWFyY2hQYXRocyk7XG4gICAgICAgICAgICBjYy5zeXMubG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy5jdXJWZXJzaW9uLCB0aGlzLnJlbW90ZVZlcnNpb24pXG4gICAgICAgICAgICBjYy5nYW1lLnJlc3RhcnQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldFJlbW90ZVZlcnNpb24oKSB7XG4gICAgICAgIGxldCBzdG9yYWdlUGF0aCA9IHRoaXMuZ2V0Um9vdFBhdGgoKTtcbiAgICAgICAgY29uc29sZS5sb2coXCLmnInkuIvovb3nmoRtYW5pZmVzdOaWh+S7tlwiLCBzdG9yYWdlUGF0aCk7XG4gICAgICAgIGxldCBsb2FkTWFuaWZlc3QgPSBqc2IuZmlsZVV0aWxzLmdldFN0cmluZ0Zyb21GaWxlKHN0b3JhZ2VQYXRoICsgJy9wcm9qZWN0Lm1hbmlmZXN0Jyk7XG4gICAgICAgIGxldCBtYW5pZmVzdE9iamVjdCA9IEpTT04ucGFyc2UobG9hZE1hbmlmZXN0KTtcbiAgICAgICAgcmV0dXJuIG1hbmlmZXN0T2JqZWN0LnZlcnNpb247XG4gICAgfVxuXG4gICAgLyoq6I635Y+W57yT5a2Y6Lev5b6EICovXG4gICAgZ2V0Um9vdFBhdGgoKSB7XG4gICAgICAgIHJldHVybiAoKGpzYi5maWxlVXRpbHMgPyBqc2IuZmlsZVV0aWxzLmdldFdyaXRhYmxlUGF0aCgpIDogJy8nKSArIHRoaXMucmVtb3RlUGF0aCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIOiOt+WPlueJiOacrOWPt1xuICAgICAqIOWmguaenOW3suWtmOacieeJiOacrOWPt+WImeebtOaOpei/lOWbnu+8jOWmguaenOayoeacieWImeWcqOacrOWcsG1hbmlmZXN05paH5Lu25Lit5Y+W54mI5pys5Y+377ybXG4gICAgICovXG4gICAgZ2V0Q3VyVmVzaW9uKCkge1xuICAgICAgICBsZXQgY3VydmVyc2lvbiA9IGNjLnN5cy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLmN1clZlcnNpb24pO1xuICAgICAgICBjYy5sb2coXCJjdXJ2ZXJzaW9uXCIsIGN1cnZlcnNpb24pXG4gICAgICAgIGlmIChjdXJ2ZXJzaW9uKSByZXR1cm4gY3VydmVyc2lvblxuICAgICAgICBsZXQgc3RvcmFnZVBhdGggPSB0aGlzLmdldFJvb3RQYXRoKCk7XG4gICAgICAgIHN0b3JhZ2VQYXRoID0gdGhpcy5tYWluaWZlc3RVcmwubmF0aXZlVXJsO1xuICAgICAgICBpZiAoc3RvcmFnZVBhdGgpIHtcbiAgICAgICAgICAgIGxldCBsb2FkTWFuaWZlc3QgPSBqc2IuZmlsZVV0aWxzLmdldFN0cmluZ0Zyb21GaWxlKHN0b3JhZ2VQYXRoKTtcbiAgICAgICAgICAgIGxldCBtYW5pZmVzdE9iamVjdCA9IEpTT04ucGFyc2UobG9hZE1hbmlmZXN0KTtcbiAgICAgICAgICAgIGN1cnZlcnNpb24gPSBtYW5pZmVzdE9iamVjdC52ZXJzaW9uO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCLlvZPliY3niYjmnKzlj7dvcmlnaW7vvJpcIiwgY3VydmVyc2lvbilcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY3VydmVyc2lvbjtcbiAgICB9XG5cblxuICAgIC8qKuiOt+WPlui/nOeoi+WKqOaAgSB1cmzlkozniYjmnKzlj7cg77yM54mI5pys5Y+355u45ZCM77yM55u05o6l6L+b5YWl5ri45oiP77yM5ZCm5YiZ6YeN5paw6K6+572ucHJvamVjdC5tYW5pZmVzdOWcsOWdgOaVsOaNriDnhLblkI7mm7TmlrAgKi9cbiAgICBjb25maWdVcGRhdGUoKSB7XG4gICAgICAgIGNjLnN5cy5sb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1clZlcnNpb25cIilcbiAgICAgICAgbGV0IGN1clZlcnNpb24gPSB0aGlzLmdldEN1clZlc2lvbigpO1xuICAgICAgICBsZXQgcmVtb3RlVmVyc2lvbiA9IFwiMS4xLjBcIlxuICAgICAgICAvLyDlpKfniYjmnKzlj7dcbiAgICAgICAgLy8gY3VyVmVyc2lvbuesrOS4gOS4quaVsOWtl+S4uuS4u+eJiOacrOWPt++8jOWmguaenOWkp+eJiOacrOWkp+S6juacrOWcsOeJiOacrOWImemHjeaWsOS4i+i9veWMhe+8iOWkp+eJiOacrOabtOaWsOmDqOWIhu+8iVxuICAgICAgICBsZXQgY3VyTWFpbklkID0gMVxuICAgICAgICBsZXQgYmlnVmVyc2lvbiA9IDFcbiAgICAgICAgaWYgKGJpZ1ZlcnNpb24gPiBjdXJNYWluSWQpIHtcbiAgICAgICAgICAgIGxldCByb290cGF0aCA9IHRoaXMuZ2V0Um9vdFBhdGgoKTtcbiAgICAgICAgICAgIC8v5Yig5o6J5LmL5YmN5aSn54mI5pys55qE57yT5a2Y6Lev5b6EXG4gICAgICAgICAgICBqc2IuZmlsZVV0aWxzLnJlbW92ZURpcmVjdG9yeShyb290cGF0aCk7XG4gICAgICAgICAgICAvL+a4hemZpOe8k+WtmOeahOeJiOacrOWPtyDlkowg54mI5pys6Lev5b6EICBcbiAgICAgICAgICAgIGNjLnN5cy5sb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnZlcnNpb25cIik7XG4gICAgICAgICAgICBsZXQgcGFja2FnZVVybCA9IFwiYXBrL2FwaeWcsOWdgFwiO1xuICAgICAgICAgICAgY2Muc3lzLm9wZW5VUkwocGFja2FnZVVybCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgLy/kuIvpnaLmmK8g6LWE5rqQ55qE5pu05paw5qOA5p+l77yI5bCP54mI5pys5pu05paw6YOo5YiG77yJXG4gICAgICAgIGxldCBuZWVkY2hlY2sgPSB0aGlzLnZlcnNpb25Db21wYXJlSGFubGUoY3VyVmVyc2lvbiwgcmVtb3RlVmVyc2lvbik7XG4gICAgICAgIC8vIOacrOWcsOeJiOacrOWSjOi/nOeoi+eJiOacrCDnm7jlkIznm7TmjqXot7PovaznmbvpmYYsIOi/memHjCDliqDliKTmlq3mmK/kuLrkuoYg6Ziy5q2i5q+P5qyh6YO95LiL6L29IG1hbmlmZXN05paH5Lu2XG4gICAgICAgIGlmIChuZWVkY2hlY2sgPT0gMCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCLlsI/niYjmnKznm7jlkIwg6L+b5YWl55m76ZmG55WM6Z2iXCIpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgLy/mi7zmjqXov5znqIvotYTmupDnmoTkuIvovb3lnLDlnYBcbiAgICAgICAgbGV0IHNjcmlwdHVybCA9IFwi6L+c56iL5pyN5Yqh5Zmo5Zyw5Z2AXCJcbiAgICAgICAgdGhpcy5fbW9kaWZ5QXBwTG9hZFVybEZvck1hbmlmZXN0RmlsZShzY3JpcHR1cmwsIHRoaXMubWFpbmlmZXN0VXJsLm5hdGl2ZVVybCk7XG4gICAgfVxuICAgIC8qKlxuICAgICog5L+u5pS5Lm1hbmlmZXN05paH5Lu2LOWmguaenOaciee8k+WtmOWImeavlOi+g+e8k+WtmOeahOeJiOacrOWSjOW6lOeUqOWMheeahOeJiOacrO+8jOWPlui+g+mrmOiAhSDng63mm7TvvIzlpoLmnpzmsqHmnInnvJPlrZjliJnkuIvovb3ov5znqIvnmoTniYjmnKzmlofku7Yg5Y+W54mI5pys5Y+377yM6L+Z6YeM57uf5LiA5L+u5pS55Zyw5Z2A5Li6XG4gICAgKiDov5znqIvlnLDlnYDvvIzkuI3nrqEg5aaC5L2V5Y+Y5YyWIOmDveaYr+S7juacgOaWsOeahOWcsOWdgOS4i+i9veeJiOacrOaWh+S7tuOAgui/memHjOi/nOeoi1xuICAgICogQHBhcmFtIHvmlrDnmoTljYfnuqfljIXlnLDlnYB9IG5ld0FwcEhvdFVwZGF0ZVVybCBcbiAgICAqIEBwYXJhbSB75pys5ZywcHJvamVjdC5tYW5pZmVzdOaWh+S7tuWcsOWdgH0gbG9jYWxNYW5pZmVzdFBhdGggXG4gICAgKi9cbiAgICBfbW9kaWZ5QXBwTG9hZFVybEZvck1hbmlmZXN0RmlsZShuZXdBcHBIb3RVcGRhdGVVcmwsIGxvY2FsTWFuaWZlc3RQYXRoKSB7XG4gICAgICAgIGxldCBpc1dyaXR0ZW4gPSBmYWxzZTtcbiAgICAgICAgaWYgKGpzYi5maWxlVXRpbHMuaXNGaWxlRXhpc3QoanNiLmZpbGVVdGlscy5nZXRXcml0YWJsZVBhdGgoKSArICdwbGFuZS9wcm9qZWN0Lm1hbmlmZXN0JykpIHtcbiAgICAgICAgICAgIGxldCBzdG9yYWdlUGF0aCA9IHRoaXMuZ2V0Um9vdFBhdGgoKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi5pyJ5LiL6L2955qEbWFuaWZlc3Tmlofku7ZcIiwgc3RvcmFnZVBhdGgpO1xuICAgICAgICAgICAgbGV0IGxvYWRNYW5pZmVzdCA9IGpzYi5maWxlVXRpbHMuZ2V0U3RyaW5nRnJvbUZpbGUoc3RvcmFnZVBhdGggKyAnL3Byb2plY3QubWFuaWZlc3QnKTtcbiAgICAgICAgICAgIGxldCBtYW5pZmVzdE9iamVjdCA9IEpTT04ucGFyc2UobG9hZE1hbmlmZXN0KTtcbiAgICAgICAgICAgIG1hbmlmZXN0T2JqZWN0LnBhY2thZ2VVcmwgPSBuZXdBcHBIb3RVcGRhdGVVcmw7XG4gICAgICAgICAgICBtYW5pZmVzdE9iamVjdC5yZW1vdGVNYW5pZmVzdFVybCA9IG1hbmlmZXN0T2JqZWN0LnBhY2thZ2VVcmwgKyBcInByb2plY3QubWFuaWZlc3RcIjtcbiAgICAgICAgICAgIG1hbmlmZXN0T2JqZWN0LnJlbW90ZVZlcnNpb25VcmwgPSBtYW5pZmVzdE9iamVjdC5wYWNrYWdlVXJsICsgXCJ2ZXJzaW9uLm1hbmlmZXN0XCI7XG4gICAgICAgICAgICBsZXQgYWZ0ZXJTdHJpbmcgPSBKU09OLnN0cmluZ2lmeShtYW5pZmVzdE9iamVjdCk7XG4gICAgICAgICAgICBpc1dyaXR0ZW4gPSBqc2IuZmlsZVV0aWxzLndyaXRlU3RyaW5nVG9GaWxlKGFmdGVyU3RyaW5nLCBzdG9yYWdlUGF0aCArICcvcHJvamVjdC5tYW5pZmVzdCcpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiDmiafooYzliLDov5nph4zor7TmmI5BcHDkuYvliY3msqHmnInov5vooYzov4fng63mm7TvvIzmiYDku6XkuI3lrZjlnKjng63mm7TnmoRwbGFuZeaWh+S7tuWkueOAglxuICAgICAgICAgICAgICovXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogcGxhbmXmlofku7blpLnkuI3lrZjlnKjnmoTml7blgJnvvIzmiJHku6zlsLHkuLvliqjliJvlu7rigJxwbGFuZeKAneaWh+S7tuWkue+8jOW5tuWwhuaJk+WMheaXtuWAmeeahHByb2plY3QubWFuaWZlc3Tmlofku7bkuK3ljYfnuqfljIXlnLDlnYDkv67mlLnlkI7vvIzlrZjmlL7liLDigJxwbGFuZeKAneaWh+S7tuWkueS4i+mdouOAglxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBsZXQgaW5pdGlhbGl6ZWRNYW5pZmVzdFBhdGggPSB0aGlzLmdldFJvb3RQYXRoKCk7XG4gICAgICAgICAgICBpZiAoIWpzYi5maWxlVXRpbHMuaXNEaXJlY3RvcnlFeGlzdChpbml0aWFsaXplZE1hbmlmZXN0UGF0aCkpIHtcbiAgICAgICAgICAgICAgICBqc2IuZmlsZVV0aWxzLmNyZWF0ZURpcmVjdG9yeShpbml0aWFsaXplZE1hbmlmZXN0UGF0aCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvL+S/ruaUueWOn+Wni21hbmlmZXN05paH5Lu2XG4gICAgICAgICAgICBsZXQgb3JpZ2luTWFuaWZlc3RQYXRoID0gbG9jYWxNYW5pZmVzdFBhdGg7XG4gICAgICAgICAgICBsZXQgb3JpZ2luTWFuaWZlc3QgPSBqc2IuZmlsZVV0aWxzLmdldFN0cmluZ0Zyb21GaWxlKG9yaWdpbk1hbmlmZXN0UGF0aCk7XG4gICAgICAgICAgICBsZXQgb3JpZ2luTWFuaWZlc3RPYmplY3QgPSBKU09OLnBhcnNlKG9yaWdpbk1hbmlmZXN0KTtcbiAgICAgICAgICAgIG9yaWdpbk1hbmlmZXN0T2JqZWN0LnBhY2thZ2VVcmwgPSBuZXdBcHBIb3RVcGRhdGVVcmw7XG4gICAgICAgICAgICBvcmlnaW5NYW5pZmVzdE9iamVjdC5yZW1vdGVNYW5pZmVzdFVybCA9IG9yaWdpbk1hbmlmZXN0T2JqZWN0LnBhY2thZ2VVcmwgKyAncHJvamVjdC5tYW5pZmVzdCc7XG4gICAgICAgICAgICBvcmlnaW5NYW5pZmVzdE9iamVjdC5yZW1vdGVWZXJzaW9uVXJsID0gb3JpZ2luTWFuaWZlc3RPYmplY3QucGFja2FnZVVybCArICd2ZXJzaW9uLm1hbmlmZXN0JztcblxuICAgICAgICAgICAgbGV0IGFmdGVyU3RyaW5nID0gSlNPTi5zdHJpbmdpZnkob3JpZ2luTWFuaWZlc3RPYmplY3QpO1xuICAgICAgICAgICAgaXNXcml0dGVuID0ganNiLmZpbGVVdGlscy53cml0ZVN0cmluZ1RvRmlsZShhZnRlclN0cmluZywgaW5pdGlhbGl6ZWRNYW5pZmVzdFBhdGggKyAnL3Byb2plY3QubWFuaWZlc3QnKTtcbiAgICAgICAgfVxuICAgICAgICBjYy5sb2coXCJXcml0dGVuIFN0YXR1cyA6IFwiLCBpc1dyaXR0ZW4pO1xuICAgICAgICBpZiAoaXNXcml0dGVuKSB7XG4gICAgICAgICAgICB0aGlzLmNoZWNrVXBkYXRlKCk7XG4gICAgICAgIH1cblxuICAgIH1cblxufVxuIl19