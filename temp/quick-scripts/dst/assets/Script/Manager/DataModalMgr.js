
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/DataModalMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd47afYEAFhDBI9nPCEezcSK', 'DataModalMgr');
// Script/Manager/DataModalMgr.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataModalMgr = void 0;
var DataBag_1 = require("../DataModal/DataBag");
var DataCollection_1 = require("../DataModal/DataCollection");
var DataDeal_1 = require("../DataModal/DataDeal");
var DataFriend_1 = require("../DataModal/DataFriend");
var DataGetMoneyLog_1 = require("../DataModal/DataGetMoneyLog");
var DataMyExtar_1 = require("../DataModal/DataMyExtar");
var DataProduce_1 = require("../DataModal/DataProduce");
var DataRank_1 = require("../DataModal/DataRank");
var DataShop_1 = require("../DataModal/DataShop");
var DataUser_1 = require("../DataModal/DataUser");
var DataModalMgr = /** @class */ (function () {
    function DataModalMgr() {
        //用户信息
        this.UserInfo = new DataUser_1.UserInfo();
        this.CurBuyProduceInfo = new DataProduce_1.DataCurBuyProduce();
        this.ItemProfucesJson = new DataProduce_1.ProducesJson();
        this.CollectionInfo = new DataCollection_1.DataCollection();
        this.BagInfo = new DataBag_1.DataBagInfo();
        this.ShopInfo = new DataShop_1.DataShop();
        this.FriendInfo = new DataFriend_1.DataFriend();
        this.FriendMonerDetailInfo = new DataFriend_1.DataFriendMoneyDetail();
        this.DataGetMoneyLogInfo = new DataGetMoneyLog_1.DataGetMoneyLog();
        this.DataMyExtarInfo = new DataMyExtar_1.DataMyExtar();
        this.DataFriendGiveDetailInfo = new DataFriend_1.DataFriendGiveDetail();
        this.DataSeeDetailInfo = new DataFriend_1.DataSeeDetail();
        this.DataDealInfo = new DataDeal_1.DataDeal();
        this.ChipsInfoJson = new DataShop_1.ChipJson();
        this.ShopInfoJson = new DataShop_1.ShopJson();
        this.DataRankInfo = new DataRank_1.DataRank();
    }
    return DataModalMgr;
}());
exports.DataModalMgr = DataModalMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9EYXRhTW9kYWxNZ3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsZ0RBQW1EO0FBQ25ELDhEQUE2RDtBQUM3RCxrREFBaUQ7QUFDakQsc0RBQWlIO0FBQ2pILGdFQUErRDtBQUMvRCx3REFBdUQ7QUFDdkQsd0RBQTJFO0FBQzNFLGtEQUFpRDtBQUNqRCxrREFBcUU7QUFDckUsa0RBQWlEO0FBRWpEO0lBQUE7UUFDSSxNQUFNO1FBQ0MsYUFBUSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO1FBQzFCLHNCQUFpQixHQUFHLElBQUksK0JBQWlCLEVBQUUsQ0FBQztRQUM1QyxxQkFBZ0IsR0FBRyxJQUFJLDBCQUFZLEVBQUUsQ0FBQztRQUN0QyxtQkFBYyxHQUFHLElBQUksK0JBQWMsRUFBRSxDQUFDO1FBQ3RDLFlBQU8sR0FBRyxJQUFJLHFCQUFXLEVBQUUsQ0FBQztRQUM1QixhQUFRLEdBQUcsSUFBSSxtQkFBUSxFQUFFLENBQUM7UUFDMUIsZUFBVSxHQUFHLElBQUksdUJBQVUsRUFBRSxDQUFDO1FBQzlCLDBCQUFxQixHQUFHLElBQUksa0NBQXFCLEVBQUUsQ0FBQztRQUNwRCx3QkFBbUIsR0FBRyxJQUFJLGlDQUFlLEVBQUUsQ0FBQztRQUM1QyxvQkFBZSxHQUFHLElBQUkseUJBQVcsRUFBRSxDQUFDO1FBQ3BDLDZCQUF3QixHQUFHLElBQUksaUNBQW9CLEVBQUUsQ0FBQztRQUN0RCxzQkFBaUIsR0FBRyxJQUFJLDBCQUFhLEVBQUUsQ0FBQztRQUN4QyxpQkFBWSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO1FBQzlCLGtCQUFhLEdBQUcsSUFBSSxtQkFBUSxFQUFFLENBQUM7UUFDL0IsaUJBQVksR0FBRyxJQUFJLG1CQUFRLEVBQUUsQ0FBQztRQUM5QixpQkFBWSxHQUFHLElBQUksbUJBQVEsRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFBRCxtQkFBQztBQUFELENBbEJBLEFBa0JDLElBQUE7QUFsQlksb0NBQVkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEYXRhQmFnSW5mbyB9IGZyb20gXCIuLi9EYXRhTW9kYWwvRGF0YUJhZ1wiO1xuaW1wb3J0IHsgRGF0YUNvbGxlY3Rpb24gfSBmcm9tIFwiLi4vRGF0YU1vZGFsL0RhdGFDb2xsZWN0aW9uXCI7XG5pbXBvcnQgeyBEYXRhRGVhbCB9IGZyb20gXCIuLi9EYXRhTW9kYWwvRGF0YURlYWxcIjtcbmltcG9ydCB7IERhdGFGcmllbmQsIERhdGFGcmllbmRHaXZlRGV0YWlsLCBEYXRhRnJpZW5kTW9uZXlEZXRhaWwsIERhdGFTZWVEZXRhaWwgfSBmcm9tIFwiLi4vRGF0YU1vZGFsL0RhdGFGcmllbmRcIjtcbmltcG9ydCB7IERhdGFHZXRNb25leUxvZyB9IGZyb20gXCIuLi9EYXRhTW9kYWwvRGF0YUdldE1vbmV5TG9nXCI7XG5pbXBvcnQgeyBEYXRhTXlFeHRhciB9IGZyb20gXCIuLi9EYXRhTW9kYWwvRGF0YU15RXh0YXJcIjtcbmltcG9ydCB7IERhdGFDdXJCdXlQcm9kdWNlLCBQcm9kdWNlc0pzb24gfSBmcm9tIFwiLi4vRGF0YU1vZGFsL0RhdGFQcm9kdWNlXCI7XG5pbXBvcnQgeyBEYXRhUmFuayB9IGZyb20gXCIuLi9EYXRhTW9kYWwvRGF0YVJhbmtcIjtcbmltcG9ydCB7IENoaXBKc29uLCBEYXRhU2hvcCwgU2hvcEpzb24gfSBmcm9tIFwiLi4vRGF0YU1vZGFsL0RhdGFTaG9wXCI7XG5pbXBvcnQgeyBVc2VySW5mbyB9IGZyb20gXCIuLi9EYXRhTW9kYWwvRGF0YVVzZXJcIjtcblxuZXhwb3J0IGNsYXNzIERhdGFNb2RhbE1nciB7XG4gICAgLy/nlKjmiLfkv6Hmga9cbiAgICBwdWJsaWMgVXNlckluZm8gPSBuZXcgVXNlckluZm8oKTtcbiAgICBwdWJsaWMgQ3VyQnV5UHJvZHVjZUluZm8gPSBuZXcgRGF0YUN1ckJ1eVByb2R1Y2UoKTtcbiAgICBwdWJsaWMgSXRlbVByb2Z1Y2VzSnNvbiA9IG5ldyBQcm9kdWNlc0pzb24oKTtcbiAgICBwdWJsaWMgQ29sbGVjdGlvbkluZm8gPSBuZXcgRGF0YUNvbGxlY3Rpb24oKTtcbiAgICBwdWJsaWMgQmFnSW5mbyA9IG5ldyBEYXRhQmFnSW5mbygpO1xuICAgIHB1YmxpYyBTaG9wSW5mbyA9IG5ldyBEYXRhU2hvcCgpO1xuICAgIHB1YmxpYyBGcmllbmRJbmZvID0gbmV3IERhdGFGcmllbmQoKTtcbiAgICBwdWJsaWMgRnJpZW5kTW9uZXJEZXRhaWxJbmZvID0gbmV3IERhdGFGcmllbmRNb25leURldGFpbCgpO1xuICAgIHB1YmxpYyBEYXRhR2V0TW9uZXlMb2dJbmZvID0gbmV3IERhdGFHZXRNb25leUxvZygpO1xuICAgIHB1YmxpYyBEYXRhTXlFeHRhckluZm8gPSBuZXcgRGF0YU15RXh0YXIoKTtcbiAgICBwdWJsaWMgRGF0YUZyaWVuZEdpdmVEZXRhaWxJbmZvID0gbmV3IERhdGFGcmllbmRHaXZlRGV0YWlsKCk7XG4gICAgcHVibGljIERhdGFTZWVEZXRhaWxJbmZvID0gbmV3IERhdGFTZWVEZXRhaWwoKTtcbiAgICBwdWJsaWMgRGF0YURlYWxJbmZvID0gbmV3IERhdGFEZWFsKCk7XG4gICAgcHVibGljIENoaXBzSW5mb0pzb24gPSBuZXcgQ2hpcEpzb24oKTtcbiAgICBwdWJsaWMgU2hvcEluZm9Kc29uID0gbmV3IFNob3BKc29uKCk7XG4gICAgcHVibGljIERhdGFSYW5rSW5mbyA9IG5ldyBEYXRhUmFuaygpO1xufVxuXG4iXX0=