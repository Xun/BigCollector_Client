
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/LocalStorageMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8a430yDeHJLG78dMGiAA1LI', 'LocalStorageMgr');
// Script/Manager/LocalStorageMgr.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LocalStorageMgr = /** @class */ (function () {
    function LocalStorageMgr() {
    }
    LocalStorageMgr.setItem = function (key, value) {
        cc.sys.localStorage.setItem("model_" + key, JSON.stringify(value));
    };
    LocalStorageMgr.getItem = function (key) {
        var data = JSON.parse(cc.sys.localStorage.getItem("model_" + key));
        if (!data || data === "") {
            return {};
        }
        return data;
    };
    LocalStorageMgr.removeItem = function (key) {
        cc.sys.localStorage.removeItem("model_" + key);
    };
    LocalStorageMgr.clearAll = function () {
        cc.sys.localStorage.clear();
    };
    return LocalStorageMgr;
}());
exports.default = LocalStorageMgr;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9Mb2NhbFN0b3JhZ2VNZ3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtJQUFBO0lBc0JBLENBQUM7SUFwQmlCLHVCQUFPLEdBQXJCLFVBQXNCLEdBQVcsRUFBRSxLQUFVO1FBQ3pDLEVBQUUsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFTLEdBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVhLHVCQUFPLEdBQXJCLFVBQXNCLEdBQVc7UUFDN0IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBUyxHQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxLQUFLLEVBQUUsRUFBRTtZQUN0QixPQUFPLEVBQUUsQ0FBQTtTQUNaO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVhLDBCQUFVLEdBQXhCLFVBQXlCLEdBQVc7UUFDaEMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLFdBQVMsR0FBSyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVhLHdCQUFRLEdBQXRCO1FBQ0ksRUFBRSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVMLHNCQUFDO0FBQUQsQ0F0QkEsQUFzQkMsSUFBQSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTG9jYWxTdG9yYWdlTWdyIHtcblxuICAgIHB1YmxpYyBzdGF0aWMgc2V0SXRlbShrZXk6IHN0cmluZywgdmFsdWU6IGFueSk6IHZvaWQge1xuICAgICAgICBjYy5zeXMubG9jYWxTdG9yYWdlLnNldEl0ZW0oYG1vZGVsXyR7a2V5fWAsIEpTT04uc3RyaW5naWZ5KHZhbHVlKSk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXRJdGVtKGtleTogc3RyaW5nKTogYW55IHtcbiAgICAgICAgbGV0IGRhdGEgPSBKU09OLnBhcnNlKGNjLnN5cy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbShgbW9kZWxfJHtrZXl9YCkpO1xuICAgICAgICBpZiAoIWRhdGEgfHwgZGF0YSA9PT0gXCJcIikge1xuICAgICAgICAgICAgcmV0dXJuIHt9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyByZW1vdmVJdGVtKGtleTogc3RyaW5nKTogdm9pZCB7XG4gICAgICAgIGNjLnN5cy5sb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShgbW9kZWxfJHtrZXl9YCk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBjbGVhckFsbCgpOiB2b2lkIHtcbiAgICAgICAgY2Muc3lzLmxvY2FsU3RvcmFnZS5jbGVhcigpO1xuICAgIH1cblxufVxuIl19