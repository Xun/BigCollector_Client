
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Manager/GameMgr.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '30ba5eNytVM3pckqs/AGbqm', 'GameMgr');
// Script/Manager/GameMgr.ts

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ConfigMgr_1 = require("./ConfigMgr");
var DataModalMgr_1 = require("./DataModalMgr");
/**
 * 掌管逻辑层
 */
var GameMgr = /** @class */ (function () {
    function GameMgr() {
        this.inited = false;
        this.configMgr = null;
        this.dataModalMgr = null;
    }
    GameMgr.prototype.init = function (uiRoot) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.configMgr = new ConfigMgr_1.default();
                        this.dataModalMgr = new DataModalMgr_1.DataModalMgr();
                        // 初始化平台sdk
                        // todo...
                        // 加载配置
                        return [4 /*yield*/, this.configMgr.loadConfigs()];
                    case 1:
                        // 初始化平台sdk
                        // todo...
                        // 加载配置
                        _a.sent();
                        // this.clientChannel = new ClientChannel(WebSocket);
                        // await this.clientChannel.connect("ws://112.74.43.142:5000/ws/test_ticket");
                        // 
                        this.inited = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    GameMgr.prototype.onGameShow = function () {
    };
    GameMgr.prototype.onGameHide = function () {
    };
    /**
     * 逻辑层的时间更新控制
     * @param dt
     */
    GameMgr.prototype.update = function (dt) {
        if (!this.inited)
            return;
        // 例如Task.update(dt); 更新任务进度
    };
    return GameMgr;
}());
exports.default = new GameMgr();

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFuYWdlci9HYW1lTWdyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EseUNBQW9DO0FBQ3BDLCtDQUE4QztBQUU5Qzs7R0FFRztBQUNIO0lBQUE7UUFFVyxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsY0FBUyxHQUFjLElBQUksQ0FBQztRQUM1QixpQkFBWSxHQUFpQixJQUFJLENBQUM7SUE4QjdDLENBQUM7SUE3QmdCLHNCQUFJLEdBQWpCLFVBQWtCLE1BQWU7Ozs7O3dCQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksbUJBQVMsRUFBRSxDQUFDO3dCQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksMkJBQVksRUFBRSxDQUFDO3dCQUN2QyxXQUFXO3dCQUNYLFVBQVU7d0JBRVYsT0FBTzt3QkFDUCxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxFQUFBOzt3QkFKbEMsV0FBVzt3QkFDWCxVQUFVO3dCQUVWLE9BQU87d0JBQ1AsU0FBa0MsQ0FBQzt3QkFDbkMscURBQXFEO3dCQUNyRCw4RUFBOEU7d0JBQzlFLEdBQUc7d0JBQ0gsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Ozs7O0tBQ3RCO0lBRUQsNEJBQVUsR0FBVjtJQUVBLENBQUM7SUFDRCw0QkFBVSxHQUFWO0lBRUEsQ0FBQztJQUVEOzs7T0FHRztJQUNJLHdCQUFNLEdBQWIsVUFBYyxFQUFVO1FBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTTtZQUFFLE9BQU87UUFDekIsNEJBQTRCO0lBQ2hDLENBQUM7SUFDTCxjQUFDO0FBQUQsQ0FsQ0EsQUFrQ0MsSUFBQTtBQUVELGtCQUFlLElBQUksT0FBTyxFQUFFLENBQUMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBDb25maWdNZ3IgZnJvbSBcIi4vQ29uZmlnTWdyXCI7XG5pbXBvcnQgeyBEYXRhTW9kYWxNZ3IgfSBmcm9tIFwiLi9EYXRhTW9kYWxNZ3JcIjtcblxuLyoqXG4gKiDmjoznrqHpgLvovpHlsYJcbiAqL1xuY2xhc3MgR2FtZU1nciB7XG5cbiAgICBwdWJsaWMgaW5pdGVkID0gZmFsc2U7XG4gICAgcHVibGljIGNvbmZpZ01ncjogQ29uZmlnTWdyID0gbnVsbDtcbiAgICBwdWJsaWMgZGF0YU1vZGFsTWdyOiBEYXRhTW9kYWxNZ3IgPSBudWxsO1xuICAgIHB1YmxpYyBhc3luYyBpbml0KHVpUm9vdDogY2MuTm9kZSkge1xuICAgICAgICB0aGlzLmNvbmZpZ01nciA9IG5ldyBDb25maWdNZ3IoKTtcbiAgICAgICAgdGhpcy5kYXRhTW9kYWxNZ3IgPSBuZXcgRGF0YU1vZGFsTWdyKCk7XG4gICAgICAgIC8vIOWIneWni+WMluW5s+WPsHNka1xuICAgICAgICAvLyB0b2RvLi4uXG5cbiAgICAgICAgLy8g5Yqg6L296YWN572uXG4gICAgICAgIGF3YWl0IHRoaXMuY29uZmlnTWdyLmxvYWRDb25maWdzKCk7XG4gICAgICAgIC8vIHRoaXMuY2xpZW50Q2hhbm5lbCA9IG5ldyBDbGllbnRDaGFubmVsKFdlYlNvY2tldCk7XG4gICAgICAgIC8vIGF3YWl0IHRoaXMuY2xpZW50Q2hhbm5lbC5jb25uZWN0KFwid3M6Ly8xMTIuNzQuNDMuMTQyOjUwMDAvd3MvdGVzdF90aWNrZXRcIik7XG4gICAgICAgIC8vIFxuICAgICAgICB0aGlzLmluaXRlZCA9IHRydWU7XG4gICAgfVxuXG4gICAgb25HYW1lU2hvdygpIHtcblxuICAgIH1cbiAgICBvbkdhbWVIaWRlKCkge1xuXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICog6YC76L6R5bGC55qE5pe26Ze05pu05paw5o6n5Yi2XG4gICAgICogQHBhcmFtIGR0IFxuICAgICAqL1xuICAgIHB1YmxpYyB1cGRhdGUoZHQ6IG51bWJlcikge1xuICAgICAgICBpZiAoIXRoaXMuaW5pdGVkKSByZXR1cm47XG4gICAgICAgIC8vIOS+i+WmglRhc2sudXBkYXRlKGR0KTsg5pu05paw5Lu75Yqh6L+b5bqmXG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgR2FtZU1ncigpOyJdfQ==