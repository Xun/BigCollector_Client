
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Net/shared/protocols/MsgUpdateBackpack.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a80431JwZZK5JP3TRrxfjH8', 'MsgUpdateBackpack');
// Script/Net/shared/protocols/MsgUpdateBackpack.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTmV0L3NoYXJlZC9wcm90b2NvbHMvTXNnVXBkYXRlQmFja3BhY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGludGVyZmFjZSBJdGVtSW5mbyB7XG4gICAgaXRlbUlEOiBudW1iZXIsXG4gICAgaXRlbUNvdW50OiBudW1iZXIsXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgTXNnVXBkYXRlQmFja3BhY2sge1xuICAgIGNoaXBJRD86IG51bWJlcixcbiAgICBjaGlwQ291bnQ/OiBudW1iZXIsXG4gICAgYmFja3BhY2s6IEl0ZW1JbmZvW10sXG4gICAgYmFnVm9sOiBudW1iZXIsXG59Il19