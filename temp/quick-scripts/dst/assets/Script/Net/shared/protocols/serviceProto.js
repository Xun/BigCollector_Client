
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Net/shared/protocols/serviceProto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cf520EQdM1Bv7ZY01WPSsFp', 'serviceProto');
// Script/Net/shared/protocols/serviceProto.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.serviceProto = void 0;
exports.serviceProto = {
    "version": 3,
    "services": [
        {
            "id": 0,
            "name": "gate/GetServer",
            "type": "api"
        },
        {
            "id": 1,
            "name": "Chat",
            "type": "msg"
        },
        {
            "id": 2,
            "name": "GameJson",
            "type": "msg"
        },
        {
            "id": 3,
            "name": "LvUp",
            "type": "msg"
        },
        {
            "id": 4,
            "name": "MakeCoin",
            "type": "msg"
        },
        {
            "id": 5,
            "name": "Social",
            "type": "msg"
        },
        {
            "id": 6,
            "name": "UpdateBackpack",
            "type": "msg"
        },
        {
            "id": 7,
            "name": "UpdateUserInfo",
            "type": "msg"
        },
        {
            "id": 8,
            "name": "WorkBench",
            "type": "msg"
        },
        {
            "id": 9,
            "name": "BagInfo",
            "type": "api"
        },
        {
            "id": 10,
            "name": "Convert",
            "type": "api"
        },
        {
            "id": 11,
            "name": "ConvertCoin",
            "type": "api"
        },
        {
            "id": 12,
            "name": "Deal",
            "type": "api"
        },
        {
            "id": 13,
            "name": "DealCancel",
            "type": "api"
        },
        {
            "id": 14,
            "name": "DealDetail",
            "type": "api"
        },
        {
            "id": 15,
            "name": "DealIssue",
            "type": "api"
        },
        {
            "id": 16,
            "name": "DealOrder",
            "type": "api"
        },
        {
            "id": 17,
            "name": "DealType",
            "type": "api"
        },
        {
            "id": 39,
            "name": "Earnings",
            "type": "api"
        },
        {
            "id": 40,
            "name": "FriendEarningsDetail",
            "type": "api"
        },
        {
            "id": 18,
            "name": "GiveFriend",
            "type": "api"
        },
        {
            "id": 41,
            "name": "InviteCode",
            "type": "api"
        },
        {
            "id": 19,
            "name": "Linkedln",
            "type": "api"
        },
        {
            "id": 20,
            "name": "Login",
            "type": "api"
        },
        {
            "id": 21,
            "name": "Rank",
            "type": "api"
        },
        {
            "id": 22,
            "name": "Send",
            "type": "api"
        },
        {
            "id": 23,
            "name": "SerchFriend",
            "type": "api"
        },
        {
            "id": 24,
            "name": "Sign",
            "type": "api"
        },
        {
            "id": 25,
            "name": "SocialSet",
            "type": "api"
        },
        {
            "id": 26,
            "name": "WheelSurf",
            "type": "api"
        },
        {
            "id": 27,
            "name": "WorkBenchCtrl",
            "type": "api"
        },
        {
            "id": 28,
            "name": "remote/Connect",
            "type": "msg"
        },
        {
            "id": 29,
            "name": "remote/Modify",
            "type": "msg"
        },
        {
            "id": 30,
            "name": "remote/ModifyItem",
            "type": "msg"
        },
        {
            "id": 31,
            "name": "remote/ModifyMoney",
            "type": "msg"
        },
        {
            "id": 32,
            "name": "remote/Remote",
            "type": "msg"
        },
        {
            "id": 33,
            "name": "remote/GetGameServer",
            "type": "api"
        },
        {
            "id": 34,
            "name": "remote/Modify",
            "type": "api"
        },
        {
            "id": 35,
            "name": "remote/ModifyItem",
            "type": "api"
        },
        {
            "id": 36,
            "name": "remote/ModifyMoney",
            "type": "api"
        },
        {
            "id": 37,
            "name": "remote/UserLogin",
            "type": "api"
        },
        {
            "id": 38,
            "name": "remote/UserLogout",
            "type": "api"
        }
    ],
    "types": {
        "gate/PtlGetServer/ReqGetServer": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "code",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "gate/PtlGetServer/ResGetServer": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "host",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "port",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgChat/MsgChat": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "content",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "time",
                    "type": {
                        "type": "Date"
                    }
                }
            ]
        },
        "MsgGameJson/MsgGameJson": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "gameJsons",
                    "type": {
                        "type": "Object"
                    }
                }
            ]
        },
        "MsgLvUp/MsgLvUp": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "lv",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "money",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgMakeCoin/MsgMakeCoin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "coin",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgSocial/MsgSocial": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "weChat",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "QQ",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "MsgUpdateBackpack/MsgUpdateBackpack": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 1,
                    "name": "chipCount",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 2,
                    "name": "backpack",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "MsgUpdateBackpack/ItemInfo"
                        }
                    }
                },
                {
                    "id": 3,
                    "name": "bagVol",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgUpdateBackpack/ItemInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "itemID",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "itemCount",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgUpdateUserInfo/MsgUpdateUserInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "coin",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "diamond",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "money",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgWorkBench/MsgWorkBench": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userWorkBench",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Number"
                        }
                    }
                },
                {
                    "id": 1,
                    "name": "makeCoin",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "buyNeedCoin",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlBagInfo/ReqBagInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlBagInfo/ResBagInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "bagVol",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "bagList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "MsgUpdateBackpack/ItemInfo"
                        }
                    }
                }
            ]
        },
        "PtlConvert/ReqConvert": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlConvert/ResConvert": {
            "type": "Interface"
        },
        "PtlConvertCoin/ReqConvertCoin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "num",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlConvertCoin/ResConvertCoin": {
            "type": "Interface"
        },
        "PtlDeal/ReqDeal": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDeal/ResDeal": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlDeal/TransactionInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "guid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "needCount",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "currentCount",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "price",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 5,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 6,
                    "name": "status",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealCancel/ReqDealCancel": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "dealID",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlDealCancel/ResDealCancel": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlDealDetail/ReqDealDetail": {
            "type": "Interface"
        },
        "PtlDealDetail/ResDealDetail": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDealDetail/DealDetailInfo"
                        }
                    }
                }
            ]
        },
        "PtlDealDetail/DealDetailInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "time",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "price",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealIssue/ReqDealIssue": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealType",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "chipID",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "price",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "chipNum",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealIssue/ResDealIssue": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlDealOrder/ReqDealOrder": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealOrder/ResDealOrder": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlDealType/ReqDealType": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "dealID",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlDealType/ResDealType": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "dealList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlDeal/TransactionInfo"
                        }
                    }
                }
            ]
        },
        "PtlEarnings/ReqEarnings": {
            "type": "Interface"
        },
        "PtlEarnings/ResEarnings": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "myParentInfo",
                    "type": {
                        "type": "Interface",
                        "properties": [
                            {
                                "id": 0,
                                "name": "name",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 1,
                                "name": "url",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 2,
                                "name": "weChat",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 3,
                                "name": "qq",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 4,
                                "name": "id",
                                "type": {
                                    "type": "String"
                                }
                            }
                        ]
                    }
                },
                {
                    "id": 1,
                    "name": "todayEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "totalEarnings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "friendNum",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlFriendEarningsDetail/ReqFriendEarningsDetail": {
            "type": "Interface"
        },
        "PtlFriendEarningsDetail/ResFriendEarningsDetail": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "todayEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "totalEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "eraningsDetailList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlFriendEarningsDetail/earningsDetailItem"
                        }
                    }
                }
            ]
        },
        "PtlFriendEarningsDetail/earningsDetailItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "time",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "oneFriendEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "twoFriendEranings",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "todayTotalEranings",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlGiveFriend/ReqGiveFriend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "num",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "id",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlGiveFriend/ResGiveFriend": {
            "type": "Interface"
        },
        "PtlInviteCode/ReqInviteCode": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "code",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlInviteCode/ResInviteCode": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "parentInfo",
                    "type": {
                        "type": "Interface",
                        "properties": [
                            {
                                "id": 0,
                                "name": "name",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 1,
                                "name": "url",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 2,
                                "name": "weChat",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 3,
                                "name": "qq",
                                "type": {
                                    "type": "String"
                                }
                            }
                        ]
                    }
                }
            ]
        },
        "PtlLinkedln/ReqLinkedln": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "friendType",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlLinkedln/ResLinkedln": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "friendArray",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlLinkedln/LinkedlnItem"
                        }
                    }
                }
            ]
        },
        "PtlLinkedln/LinkedlnItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "name",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "url",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "time",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "lv",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "active",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlLogin/ReqLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "code",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlLogin/ResLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userInfo",
                    "type": {
                        "type": "Interface",
                        "properties": [
                            {
                                "id": 0,
                                "name": "userName",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 1,
                                "name": "userAvatarUrl",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 2,
                                "name": "userAccount",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 3,
                                "name": "userCoin",
                                "type": {
                                    "type": "Number"
                                }
                            },
                            {
                                "id": 4,
                                "name": "userDiamond",
                                "type": {
                                    "type": "Number"
                                }
                            },
                            {
                                "id": 7,
                                "name": "userMoney",
                                "type": {
                                    "type": "Number"
                                }
                            }
                        ]
                    }
                }
            ]
        },
        "PtlRank/ReqRank": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlRank/ResRank": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "rank",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "PtlRank/RankItem"
                        }
                    }
                }
            ]
        },
        "PtlRank/RankItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "name",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "money",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "picUrl",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlSend/ReqSend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "content",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlSend/ResSend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "time",
                    "type": {
                        "type": "Date"
                    }
                }
            ]
        },
        "PtlSerchFriend/ReqSerchFriend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlSerchFriend/ResSerchFriend": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "serchInfo",
                    "type": {
                        "type": "Interface",
                        "properties": [
                            {
                                "id": 0,
                                "name": "name",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 1,
                                "name": "url",
                                "type": {
                                    "type": "String"
                                }
                            },
                            {
                                "id": 2,
                                "name": "id",
                                "type": {
                                    "type": "String"
                                }
                            }
                        ]
                    }
                }
            ]
        },
        "PtlSign/ReqSign": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "receiveIndex",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                }
            ]
        },
        "PtlSign/ResSign": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "signDay",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "maxSignDay",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "receive",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Number"
                        }
                    }
                }
            ]
        },
        "PtlSocialSet/ReqSocialSet": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "weChat",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "QQ",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlSocialSet/ResSocialSet": {
            "type": "Interface"
        },
        "PtlWheelSurf/ReqWheelSurf": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "operationId",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlWheelSurf/ResWheelSurf": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "drawId",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "drawCount",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "advertCount",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlWorkBenchCtrl/ReqWorkBenchCtrl": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userAccount",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "ctrlType",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "curPos",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 3,
                    "name": "target",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                }
            ]
        },
        "PtlWorkBenchCtrl/ResWorkBenchCtrl": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "userWorkBench",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Number"
                        }
                    }
                }
            ]
        },
        "remote/MsgConnect/MsgConnect": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "serverID",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                }
            ]
        },
        "remote/MsgModify/MsgModify": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "cid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/MsgModifyItem/MsgModifyItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "cid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/MsgModifyMoney/MsgModifyMoney": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "type",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/MsgRemote/MsgRemote": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "arg",
                    "type": {
                        "type": "Any"
                    }
                }
            ]
        },
        "remote/PtlGetGameServer/ReqGetGameServer": {
            "type": "Interface"
        },
        "remote/PtlGetGameServer/ResGetGameServer": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "serverID",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/PtlModify/ReqModify": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "cid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "type",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/PtlModify/ResModify": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "result",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "remote/PtlModifyItem/ReqModifyItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "cid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/PtlModifyItem/ResModifyItem": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "result",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "remote/PtlModifyMoney/ReqModifyMoney": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "type",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "count",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "remote/PtlModifyMoney/ResModifyMoney": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "result",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "remote/PtlUserLogin/ReqUserLogin": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "sid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "remote/PtlUserLogin/ResUserLogin": {
            "type": "Interface"
        },
        "remote/PtlUserLogout/ReqUserLogout": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "sid",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "uid",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "remote/PtlUserLogout/ResUserLogout": {
            "type": "Interface"
        }
    }
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTmV0L3NoYXJlZC9wcm90b2NvbHMvc2VydmljZVByb3RvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9MYSxRQUFBLFlBQVksR0FBOEI7SUFDbkQsU0FBUyxFQUFFLENBQUM7SUFDWixVQUFVLEVBQUU7UUFDUjtZQUNJLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLGdCQUFnQjtZQUN4QixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLENBQUM7WUFDUCxNQUFNLEVBQUUsTUFBTTtZQUNkLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsQ0FBQztZQUNQLE1BQU0sRUFBRSxVQUFVO1lBQ2xCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsQ0FBQztZQUNQLE1BQU0sRUFBRSxNQUFNO1lBQ2QsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLFVBQVU7WUFDbEIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLFFBQVE7WUFDaEIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxDQUFDO1lBQ1AsTUFBTSxFQUFFLGdCQUFnQjtZQUN4QixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLENBQUM7WUFDUCxNQUFNLEVBQUUsZ0JBQWdCO1lBQ3hCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsQ0FBQztZQUNQLE1BQU0sRUFBRSxXQUFXO1lBQ25CLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsQ0FBQztZQUNQLE1BQU0sRUFBRSxTQUFTO1lBQ2pCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxTQUFTO1lBQ2pCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxhQUFhO1lBQ3JCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxNQUFNO1lBQ2QsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLFlBQVk7WUFDcEIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLFlBQVk7WUFDcEIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLFdBQVc7WUFDbkIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLFdBQVc7WUFDbkIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLFVBQVU7WUFDbEIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLFVBQVU7WUFDbEIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLHNCQUFzQjtZQUM5QixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsWUFBWTtZQUNwQixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsWUFBWTtZQUNwQixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsVUFBVTtZQUNsQixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsT0FBTztZQUNmLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxNQUFNO1lBQ2QsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLE1BQU07WUFDZCxNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsYUFBYTtZQUNyQixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsTUFBTTtZQUNkLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxXQUFXO1lBQ25CLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxXQUFXO1lBQ25CLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxlQUFlO1lBQ3ZCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxnQkFBZ0I7WUFDeEIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLGVBQWU7WUFDdkIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLG1CQUFtQjtZQUMzQixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsb0JBQW9CO1lBQzVCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxlQUFlO1lBQ3ZCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxzQkFBc0I7WUFDOUIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLGVBQWU7WUFDdkIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLG1CQUFtQjtZQUMzQixNQUFNLEVBQUUsS0FBSztTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLEVBQUU7WUFDUixNQUFNLEVBQUUsb0JBQW9CO1lBQzVCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCO1FBQ0Q7WUFDSSxJQUFJLEVBQUUsRUFBRTtZQUNSLE1BQU0sRUFBRSxrQkFBa0I7WUFDMUIsTUFBTSxFQUFFLEtBQUs7U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLG1CQUFtQjtZQUMzQixNQUFNLEVBQUUsS0FBSztTQUNoQjtLQUNKO0lBQ0QsT0FBTyxFQUFFO1FBQ0wsZ0NBQWdDLEVBQUU7WUFDOUIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsZ0NBQWdDLEVBQUU7WUFDOUIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE1BQU07b0JBQ2QsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxpQkFBaUIsRUFBRTtZQUNmLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsU0FBUztvQkFDakIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLE1BQU07cUJBQ2pCO2lCQUNKO2FBQ0o7U0FDSjtRQUNELHlCQUF5QixFQUFFO1lBQ3ZCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsV0FBVztvQkFDbkIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxpQkFBaUIsRUFBRTtZQUNmLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxJQUFJO29CQUNaLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE9BQU87b0JBQ2YsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsUUFBUTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCx5QkFBeUIsRUFBRTtZQUN2QixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE1BQU07b0JBQ2QsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxxQkFBcUIsRUFBRTtZQUNuQixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLElBQUk7b0JBQ1osTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxxQ0FBcUMsRUFBRTtZQUNuQyxNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7b0JBQ0QsVUFBVSxFQUFFLElBQUk7aUJBQ25CO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxXQUFXO29CQUNuQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO29CQUNELFVBQVUsRUFBRSxJQUFJO2lCQUNuQjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsVUFBVTtvQkFDbEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxPQUFPO3dCQUNmLGFBQWEsRUFBRTs0QkFDWCxNQUFNLEVBQUUsV0FBVzs0QkFDbkIsUUFBUSxFQUFFLDRCQUE0Qjt5QkFDekM7cUJBQ0o7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsNEJBQTRCLEVBQUU7WUFDMUIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxRQUFRO29CQUNoQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxXQUFXO29CQUNuQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELHFDQUFxQyxFQUFFO1lBQ25DLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxTQUFTO29CQUNqQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxPQUFPO29CQUNmLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsMkJBQTJCLEVBQUU7WUFDekIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxlQUFlO29CQUN2QixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLE9BQU87d0JBQ2YsYUFBYSxFQUFFOzRCQUNYLE1BQU0sRUFBRSxRQUFRO3lCQUNuQjtxQkFDSjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsVUFBVTtvQkFDbEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsYUFBYTtvQkFDckIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCx1QkFBdUIsRUFBRTtZQUNyQixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE1BQU07b0JBQ2QsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCx1QkFBdUIsRUFBRTtZQUNyQixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFNBQVM7b0JBQ2pCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsT0FBTzt3QkFDZixhQUFhLEVBQUU7NEJBQ1gsTUFBTSxFQUFFLFdBQVc7NEJBQ25CLFFBQVEsRUFBRSw0QkFBNEI7eUJBQ3pDO3FCQUNKO2lCQUNKO2FBQ0o7U0FDSjtRQUNELHVCQUF1QixFQUFFO1lBQ3JCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsSUFBSTtvQkFDWixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELHVCQUF1QixFQUFFO1lBQ3JCLE1BQU0sRUFBRSxXQUFXO1NBQ3RCO1FBQ0QsK0JBQStCLEVBQUU7WUFDN0IsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsK0JBQStCLEVBQUU7WUFDN0IsTUFBTSxFQUFFLFdBQVc7U0FDdEI7UUFDRCxpQkFBaUIsRUFBRTtZQUNmLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxRQUFRO29CQUNoQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELGlCQUFpQixFQUFFO1lBQ2YsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxVQUFVO29CQUNsQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLE9BQU87d0JBQ2YsYUFBYSxFQUFFOzRCQUNYLE1BQU0sRUFBRSxXQUFXOzRCQUNuQixRQUFRLEVBQUUseUJBQXlCO3lCQUN0QztxQkFDSjtpQkFDSjthQUNKO1NBQ0o7UUFDRCx5QkFBeUIsRUFBRTtZQUN2QixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE1BQU07b0JBQ2QsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsUUFBUTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsV0FBVztvQkFDbkIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsY0FBYztvQkFDdEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsT0FBTztvQkFDZixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxhQUFhO29CQUNyQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxRQUFRO29CQUNoQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELDZCQUE2QixFQUFFO1lBQzNCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxRQUFRO29CQUNoQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELDZCQUE2QixFQUFFO1lBQzNCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsVUFBVTtvQkFDbEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxPQUFPO3dCQUNmLGFBQWEsRUFBRTs0QkFDWCxNQUFNLEVBQUUsV0FBVzs0QkFDbkIsUUFBUSxFQUFFLHlCQUF5Qjt5QkFDdEM7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsNkJBQTZCLEVBQUU7WUFDM0IsTUFBTSxFQUFFLFdBQVc7U0FDdEI7UUFDRCw2QkFBNkIsRUFBRTtZQUMzQixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFVBQVU7b0JBQ2xCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsT0FBTzt3QkFDZixhQUFhLEVBQUU7NEJBQ1gsTUFBTSxFQUFFLFdBQVc7NEJBQ25CLFFBQVEsRUFBRSw4QkFBOEI7eUJBQzNDO3FCQUNKO2lCQUNKO2FBQ0o7U0FDSjtRQUNELDhCQUE4QixFQUFFO1lBQzVCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsUUFBUTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE9BQU87b0JBQ2YsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsT0FBTztvQkFDZixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELDJCQUEyQixFQUFFO1lBQ3pCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsVUFBVTtvQkFDbEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsUUFBUTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsT0FBTztvQkFDZixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxTQUFTO29CQUNqQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELDJCQUEyQixFQUFFO1lBQ3pCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsVUFBVTtvQkFDbEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxPQUFPO3dCQUNmLGFBQWEsRUFBRTs0QkFDWCxNQUFNLEVBQUUsV0FBVzs0QkFDbkIsUUFBUSxFQUFFLHlCQUF5Qjt5QkFDdEM7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsMkJBQTJCLEVBQUU7WUFDekIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxhQUFhO29CQUNyQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsMkJBQTJCLEVBQUU7WUFDekIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxVQUFVO29CQUNsQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLE9BQU87d0JBQ2YsYUFBYSxFQUFFOzRCQUNYLE1BQU0sRUFBRSxXQUFXOzRCQUNuQixRQUFRLEVBQUUseUJBQXlCO3lCQUN0QztxQkFDSjtpQkFDSjthQUNKO1NBQ0o7UUFDRCx5QkFBeUIsRUFBRTtZQUN2QixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE1BQU07b0JBQ2QsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsUUFBUTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsT0FBTztvQkFDZixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELHlCQUF5QixFQUFFO1lBQ3ZCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsVUFBVTtvQkFDbEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxPQUFPO3dCQUNmLGFBQWEsRUFBRTs0QkFDWCxNQUFNLEVBQUUsV0FBVzs0QkFDbkIsUUFBUSxFQUFFLHlCQUF5Qjt5QkFDdEM7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QseUJBQXlCLEVBQUU7WUFDdkIsTUFBTSxFQUFFLFdBQVc7U0FDdEI7UUFDRCx5QkFBeUIsRUFBRTtZQUN2QixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLGNBQWM7b0JBQ3RCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsV0FBVzt3QkFDbkIsWUFBWSxFQUFFOzRCQUNWO2dDQUNJLElBQUksRUFBRSxDQUFDO2dDQUNQLE1BQU0sRUFBRSxNQUFNO2dDQUNkLE1BQU0sRUFBRTtvQ0FDSixNQUFNLEVBQUUsUUFBUTtpQ0FDbkI7NkJBQ0o7NEJBQ0Q7Z0NBQ0ksSUFBSSxFQUFFLENBQUM7Z0NBQ1AsTUFBTSxFQUFFLEtBQUs7Z0NBQ2IsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsUUFBUTtnQ0FDaEIsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsSUFBSTtnQ0FDWixNQUFNLEVBQUU7b0NBQ0osTUFBTSxFQUFFLFFBQVE7aUNBQ25COzZCQUNKOzRCQUNEO2dDQUNJLElBQUksRUFBRSxDQUFDO2dDQUNQLE1BQU0sRUFBRSxJQUFJO2dDQUNaLE1BQU0sRUFBRTtvQ0FDSixNQUFNLEVBQUUsUUFBUTtpQ0FDbkI7NkJBQ0o7eUJBQ0o7cUJBQ0o7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLGVBQWU7b0JBQ3ZCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLGVBQWU7b0JBQ3ZCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFdBQVc7b0JBQ25CLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsaURBQWlELEVBQUU7WUFDL0MsTUFBTSxFQUFFLFdBQVc7U0FDdEI7UUFDRCxpREFBaUQsRUFBRTtZQUMvQyxNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLGVBQWU7b0JBQ3ZCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLGVBQWU7b0JBQ3ZCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLG9CQUFvQjtvQkFDNUIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxPQUFPO3dCQUNmLGFBQWEsRUFBRTs0QkFDWCxNQUFNLEVBQUUsV0FBVzs0QkFDbkIsUUFBUSxFQUFFLDRDQUE0Qzt5QkFDekQ7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsNENBQTRDLEVBQUU7WUFDMUMsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLG1CQUFtQjtvQkFDM0IsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsbUJBQW1CO29CQUMzQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxvQkFBb0I7b0JBQzVCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsNkJBQTZCLEVBQUU7WUFDM0IsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLElBQUk7b0JBQ1osTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCw2QkFBNkIsRUFBRTtZQUMzQixNQUFNLEVBQUUsV0FBVztTQUN0QjtRQUNELDZCQUE2QixFQUFFO1lBQzNCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELDZCQUE2QixFQUFFO1lBQzNCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsWUFBWTtvQkFDcEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxXQUFXO3dCQUNuQixZQUFZLEVBQUU7NEJBQ1Y7Z0NBQ0ksSUFBSSxFQUFFLENBQUM7Z0NBQ1AsTUFBTSxFQUFFLE1BQU07Z0NBQ2QsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsS0FBSztnQ0FDYixNQUFNLEVBQUU7b0NBQ0osTUFBTSxFQUFFLFFBQVE7aUNBQ25COzZCQUNKOzRCQUNEO2dDQUNJLElBQUksRUFBRSxDQUFDO2dDQUNQLE1BQU0sRUFBRSxRQUFRO2dDQUNoQixNQUFNLEVBQUU7b0NBQ0osTUFBTSxFQUFFLFFBQVE7aUNBQ25COzZCQUNKOzRCQUNEO2dDQUNJLElBQUksRUFBRSxDQUFDO2dDQUNQLE1BQU0sRUFBRSxJQUFJO2dDQUNaLE1BQU0sRUFBRTtvQ0FDSixNQUFNLEVBQUUsUUFBUTtpQ0FDbkI7NkJBQ0o7eUJBQ0o7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QseUJBQXlCLEVBQUU7WUFDdkIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxhQUFhO29CQUNyQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxZQUFZO29CQUNwQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELHlCQUF5QixFQUFFO1lBQ3ZCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsYUFBYTtvQkFDckIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxPQUFPO3dCQUNmLGFBQWEsRUFBRTs0QkFDWCxNQUFNLEVBQUUsV0FBVzs0QkFDbkIsUUFBUSxFQUFFLDBCQUEwQjt5QkFDdkM7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsMEJBQTBCLEVBQUU7WUFDeEIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxJQUFJO29CQUNaLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsbUJBQW1CLEVBQUU7WUFDakIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsbUJBQW1CLEVBQUU7WUFDakIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxVQUFVO29CQUNsQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFdBQVc7d0JBQ25CLFlBQVksRUFBRTs0QkFDVjtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsVUFBVTtnQ0FDbEIsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsZUFBZTtnQ0FDdkIsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsYUFBYTtnQ0FDckIsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsVUFBVTtnQ0FDbEIsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsYUFBYTtnQ0FDckIsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsV0FBVztnQ0FDbkIsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjt5QkFDSjtxQkFDSjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxpQkFBaUIsRUFBRTtZQUNmLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELGlCQUFpQixFQUFFO1lBQ2YsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsT0FBTzt3QkFDZixhQUFhLEVBQUU7NEJBQ1gsTUFBTSxFQUFFLFdBQVc7NEJBQ25CLFFBQVEsRUFBRSxrQkFBa0I7eUJBQy9CO3FCQUNKO2lCQUNKO2FBQ0o7U0FDSjtRQUNELGtCQUFrQixFQUFFO1lBQ2hCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsS0FBSztvQkFDYixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE9BQU87b0JBQ2YsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsUUFBUTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxpQkFBaUIsRUFBRTtZQUNmLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsU0FBUztvQkFDakIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxpQkFBaUIsRUFBRTtZQUNmLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLE1BQU07cUJBQ2pCO2lCQUNKO2FBQ0o7U0FDSjtRQUNELCtCQUErQixFQUFFO1lBQzdCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsSUFBSTtvQkFDWixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELCtCQUErQixFQUFFO1lBQzdCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsV0FBVztvQkFDbkIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxXQUFXO3dCQUNuQixZQUFZLEVBQUU7NEJBQ1Y7Z0NBQ0ksSUFBSSxFQUFFLENBQUM7Z0NBQ1AsTUFBTSxFQUFFLE1BQU07Z0NBQ2QsTUFBTSxFQUFFO29DQUNKLE1BQU0sRUFBRSxRQUFRO2lDQUNuQjs2QkFDSjs0QkFDRDtnQ0FDSSxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsS0FBSztnQ0FDYixNQUFNLEVBQUU7b0NBQ0osTUFBTSxFQUFFLFFBQVE7aUNBQ25COzZCQUNKOzRCQUNEO2dDQUNJLElBQUksRUFBRSxDQUFDO2dDQUNQLE1BQU0sRUFBRSxJQUFJO2dDQUNaLE1BQU0sRUFBRTtvQ0FDSixNQUFNLEVBQUUsUUFBUTtpQ0FDbkI7NkJBQ0o7eUJBQ0o7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsaUJBQWlCLEVBQUU7WUFDZixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLGFBQWE7b0JBQ3JCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLGNBQWM7b0JBQ3RCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7b0JBQ0QsVUFBVSxFQUFFLElBQUk7aUJBQ25CO2FBQ0o7U0FDSjtRQUNELGlCQUFpQixFQUFFO1lBQ2YsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxTQUFTO29CQUNqQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxZQUFZO29CQUNwQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxTQUFTO29CQUNqQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLE9BQU87d0JBQ2YsYUFBYSxFQUFFOzRCQUNYLE1BQU0sRUFBRSxRQUFRO3lCQUNuQjtxQkFDSjtpQkFDSjthQUNKO1NBQ0o7UUFDRCwyQkFBMkIsRUFBRTtZQUN6QixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLElBQUk7b0JBQ1osTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCwyQkFBMkIsRUFBRTtZQUN6QixNQUFNLEVBQUUsV0FBVztTQUN0QjtRQUNELDJCQUEyQixFQUFFO1lBQ3pCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsYUFBYTtvQkFDckIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsYUFBYTtvQkFDckIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCwyQkFBMkIsRUFBRTtZQUN6QixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFdBQVc7b0JBQ25CLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLGFBQWE7b0JBQ3JCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsbUNBQW1DLEVBQUU7WUFDakMsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxhQUFhO29CQUNyQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxVQUFVO29CQUNsQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxRQUFRO29CQUNoQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO29CQUNELFVBQVUsRUFBRSxJQUFJO2lCQUNuQjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsUUFBUTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtvQkFDRCxVQUFVLEVBQUUsSUFBSTtpQkFDbkI7YUFDSjtTQUNKO1FBQ0QsbUNBQW1DLEVBQUU7WUFDakMsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxlQUFlO29CQUN2QixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLE9BQU87d0JBQ2YsYUFBYSxFQUFFOzRCQUNYLE1BQU0sRUFBRSxRQUFRO3lCQUNuQjtxQkFDSjtpQkFDSjthQUNKO1NBQ0o7UUFDRCw4QkFBOEIsRUFBRTtZQUM1QixNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE1BQU07b0JBQ2QsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsVUFBVTtvQkFDbEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtvQkFDRCxVQUFVLEVBQUUsSUFBSTtpQkFDbkI7YUFDSjtTQUNKO1FBQ0QsNEJBQTRCLEVBQUU7WUFDMUIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELG9DQUFvQyxFQUFFO1lBQ2xDLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsS0FBSztvQkFDYixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE9BQU87b0JBQ2YsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxzQ0FBc0MsRUFBRTtZQUNwQyxNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxPQUFPO29CQUNmLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsNEJBQTRCLEVBQUU7WUFDMUIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsS0FBSztxQkFDaEI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsMENBQTBDLEVBQUU7WUFDeEMsTUFBTSxFQUFFLFdBQVc7U0FDdEI7UUFDRCwwQ0FBMEMsRUFBRTtZQUN4QyxNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFVBQVU7b0JBQ2xCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsNEJBQTRCLEVBQUU7WUFDMUIsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2FBQ0o7U0FDSjtRQUNELDRCQUE0QixFQUFFO1lBQzFCLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsUUFBUTtvQkFDaEIsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxTQUFTO3FCQUNwQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxvQ0FBb0MsRUFBRTtZQUNsQyxNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsS0FBSztvQkFDYixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxPQUFPO29CQUNmLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0Qsb0NBQW9DLEVBQUU7WUFDbEMsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxRQUFRO29CQUNoQixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFNBQVM7cUJBQ3BCO2lCQUNKO2FBQ0o7U0FDSjtRQUNELHNDQUFzQyxFQUFFO1lBQ3BDLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsS0FBSztvQkFDYixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLE9BQU87b0JBQ2YsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxzQ0FBc0MsRUFBRTtZQUNwQyxNQUFNLEVBQUUsV0FBVztZQUNuQixZQUFZLEVBQUU7Z0JBQ1Y7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLFFBQVE7b0JBQ2hCLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsU0FBUztxQkFDcEI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0Qsa0NBQWtDLEVBQUU7WUFDaEMsTUFBTSxFQUFFLFdBQVc7WUFDbkIsWUFBWSxFQUFFO2dCQUNWO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLENBQUM7b0JBQ1AsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsTUFBTSxFQUFFO3dCQUNKLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7UUFDRCxrQ0FBa0MsRUFBRTtZQUNoQyxNQUFNLEVBQUUsV0FBVztTQUN0QjtRQUNELG9DQUFvQyxFQUFFO1lBQ2xDLE1BQU0sRUFBRSxXQUFXO1lBQ25CLFlBQVksRUFBRTtnQkFDVjtvQkFDSSxJQUFJLEVBQUUsQ0FBQztvQkFDUCxNQUFNLEVBQUUsS0FBSztvQkFDYixNQUFNLEVBQUU7d0JBQ0osTUFBTSxFQUFFLFFBQVE7cUJBQ25CO2lCQUNKO2dCQUNEO29CQUNJLElBQUksRUFBRSxDQUFDO29CQUNQLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRTt3QkFDSixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7aUJBQ0o7YUFDSjtTQUNKO1FBQ0Qsb0NBQW9DLEVBQUU7WUFDbEMsTUFBTSxFQUFFLFdBQVc7U0FDdEI7S0FDSjtDQUNKLENBQUMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTZXJ2aWNlUHJvdG8gfSBmcm9tICd0c3JwYy1wcm90byc7XG5pbXBvcnQgeyBSZXFHZXRTZXJ2ZXIsIFJlc0dldFNlcnZlciB9IGZyb20gJy4vZ2F0ZS9QdGxHZXRTZXJ2ZXInO1xuaW1wb3J0IHsgTXNnQ2hhdCB9IGZyb20gJy4vTXNnQ2hhdCc7XG5pbXBvcnQgeyBNc2dHYW1lSnNvbiB9IGZyb20gJy4vTXNnR2FtZUpzb24nO1xuaW1wb3J0IHsgTXNnTHZVcCB9IGZyb20gJy4vTXNnTHZVcCc7XG5pbXBvcnQgeyBNc2dNYWtlQ29pbiB9IGZyb20gJy4vTXNnTWFrZUNvaW4nO1xuaW1wb3J0IHsgTXNnU29jaWFsIH0gZnJvbSAnLi9Nc2dTb2NpYWwnO1xuaW1wb3J0IHsgTXNnVXBkYXRlQmFja3BhY2sgfSBmcm9tICcuL01zZ1VwZGF0ZUJhY2twYWNrJztcbmltcG9ydCB7IE1zZ1VwZGF0ZVVzZXJJbmZvIH0gZnJvbSAnLi9Nc2dVcGRhdGVVc2VySW5mbyc7XG5pbXBvcnQgeyBNc2dXb3JrQmVuY2ggfSBmcm9tICcuL01zZ1dvcmtCZW5jaCc7XG5pbXBvcnQgeyBSZXFCYWdJbmZvLCBSZXNCYWdJbmZvIH0gZnJvbSAnLi9QdGxCYWdJbmZvJztcbmltcG9ydCB7IFJlcUNvbnZlcnQsIFJlc0NvbnZlcnQgfSBmcm9tICcuL1B0bENvbnZlcnQnO1xuaW1wb3J0IHsgUmVxQ29udmVydENvaW4sIFJlc0NvbnZlcnRDb2luIH0gZnJvbSAnLi9QdGxDb252ZXJ0Q29pbic7XG5pbXBvcnQgeyBSZXFEZWFsLCBSZXNEZWFsIH0gZnJvbSAnLi9QdGxEZWFsJztcbmltcG9ydCB7IFJlcURlYWxDYW5jZWwsIFJlc0RlYWxDYW5jZWwgfSBmcm9tICcuL1B0bERlYWxDYW5jZWwnO1xuaW1wb3J0IHsgUmVxRGVhbERldGFpbCwgUmVzRGVhbERldGFpbCB9IGZyb20gJy4vUHRsRGVhbERldGFpbCc7XG5pbXBvcnQgeyBSZXFEZWFsSXNzdWUsIFJlc0RlYWxJc3N1ZSB9IGZyb20gJy4vUHRsRGVhbElzc3VlJztcbmltcG9ydCB7IFJlcURlYWxPcmRlciwgUmVzRGVhbE9yZGVyIH0gZnJvbSAnLi9QdGxEZWFsT3JkZXInO1xuaW1wb3J0IHsgUmVxRGVhbFR5cGUsIFJlc0RlYWxUeXBlIH0gZnJvbSAnLi9QdGxEZWFsVHlwZSc7XG5pbXBvcnQgeyBSZXFFYXJuaW5ncywgUmVzRWFybmluZ3MgfSBmcm9tICcuL1B0bEVhcm5pbmdzJztcbmltcG9ydCB7IFJlcUZyaWVuZEVhcm5pbmdzRGV0YWlsLCBSZXNGcmllbmRFYXJuaW5nc0RldGFpbCB9IGZyb20gJy4vUHRsRnJpZW5kRWFybmluZ3NEZXRhaWwnO1xuaW1wb3J0IHsgUmVxR2l2ZUZyaWVuZCwgUmVzR2l2ZUZyaWVuZCB9IGZyb20gJy4vUHRsR2l2ZUZyaWVuZCc7XG5pbXBvcnQgeyBSZXFJbnZpdGVDb2RlLCBSZXNJbnZpdGVDb2RlIH0gZnJvbSAnLi9QdGxJbnZpdGVDb2RlJztcbmltcG9ydCB7IFJlcUxpbmtlZGxuLCBSZXNMaW5rZWRsbiB9IGZyb20gJy4vUHRsTGlua2VkbG4nO1xuaW1wb3J0IHsgUmVxTG9naW4sIFJlc0xvZ2luIH0gZnJvbSAnLi9QdGxMb2dpbic7XG5pbXBvcnQgeyBSZXFSYW5rLCBSZXNSYW5rIH0gZnJvbSAnLi9QdGxSYW5rJztcbmltcG9ydCB7IFJlcVNlbmQsIFJlc1NlbmQgfSBmcm9tICcuL1B0bFNlbmQnO1xuaW1wb3J0IHsgUmVxU2VyY2hGcmllbmQsIFJlc1NlcmNoRnJpZW5kIH0gZnJvbSAnLi9QdGxTZXJjaEZyaWVuZCc7XG5pbXBvcnQgeyBSZXFTaWduLCBSZXNTaWduIH0gZnJvbSAnLi9QdGxTaWduJztcbmltcG9ydCB7IFJlcVNvY2lhbFNldCwgUmVzU29jaWFsU2V0IH0gZnJvbSAnLi9QdGxTb2NpYWxTZXQnO1xuaW1wb3J0IHsgUmVxV2hlZWxTdXJmLCBSZXNXaGVlbFN1cmYgfSBmcm9tICcuL1B0bFdoZWVsU3VyZic7XG5pbXBvcnQgeyBSZXFXb3JrQmVuY2hDdHJsLCBSZXNXb3JrQmVuY2hDdHJsIH0gZnJvbSAnLi9QdGxXb3JrQmVuY2hDdHJsJztcbmltcG9ydCB7IE1zZ0Nvbm5lY3QgfSBmcm9tICcuL3JlbW90ZS9Nc2dDb25uZWN0JztcbmltcG9ydCB7IE1zZ01vZGlmeSB9IGZyb20gJy4vcmVtb3RlL01zZ01vZGlmeSc7XG5pbXBvcnQgeyBNc2dNb2RpZnlJdGVtIH0gZnJvbSAnLi9yZW1vdGUvTXNnTW9kaWZ5SXRlbSc7XG5pbXBvcnQgeyBNc2dNb2RpZnlNb25leSB9IGZyb20gJy4vcmVtb3RlL01zZ01vZGlmeU1vbmV5JztcbmltcG9ydCB7IE1zZ1JlbW90ZSB9IGZyb20gJy4vcmVtb3RlL01zZ1JlbW90ZSc7XG5pbXBvcnQgeyBSZXFHZXRHYW1lU2VydmVyLCBSZXNHZXRHYW1lU2VydmVyIH0gZnJvbSAnLi9yZW1vdGUvUHRsR2V0R2FtZVNlcnZlcic7XG5pbXBvcnQgeyBSZXFNb2RpZnksIFJlc01vZGlmeSB9IGZyb20gJy4vcmVtb3RlL1B0bE1vZGlmeSc7XG5pbXBvcnQgeyBSZXFNb2RpZnlJdGVtLCBSZXNNb2RpZnlJdGVtIH0gZnJvbSAnLi9yZW1vdGUvUHRsTW9kaWZ5SXRlbSc7XG5pbXBvcnQgeyBSZXFNb2RpZnlNb25leSwgUmVzTW9kaWZ5TW9uZXkgfSBmcm9tICcuL3JlbW90ZS9QdGxNb2RpZnlNb25leSc7XG5pbXBvcnQgeyBSZXFVc2VyTG9naW4sIFJlc1VzZXJMb2dpbiB9IGZyb20gJy4vcmVtb3RlL1B0bFVzZXJMb2dpbic7XG5pbXBvcnQgeyBSZXFVc2VyTG9nb3V0LCBSZXNVc2VyTG9nb3V0IH0gZnJvbSAnLi9yZW1vdGUvUHRsVXNlckxvZ291dCc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgU2VydmljZVR5cGUge1xuICAgIGFwaToge1xuICAgICAgICBcImdhdGUvR2V0U2VydmVyXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxR2V0U2VydmVyLFxuICAgICAgICAgICAgcmVzOiBSZXNHZXRTZXJ2ZXJcbiAgICAgICAgfSxcbiAgICAgICAgXCJCYWdJbmZvXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxQmFnSW5mbyxcbiAgICAgICAgICAgIHJlczogUmVzQmFnSW5mb1xuICAgICAgICB9LFxuICAgICAgICBcIkNvbnZlcnRcIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFDb252ZXJ0LFxuICAgICAgICAgICAgcmVzOiBSZXNDb252ZXJ0XG4gICAgICAgIH0sXG4gICAgICAgIFwiQ29udmVydENvaW5cIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFDb252ZXJ0Q29pbixcbiAgICAgICAgICAgIHJlczogUmVzQ29udmVydENvaW5cbiAgICAgICAgfSxcbiAgICAgICAgXCJEZWFsXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxRGVhbCxcbiAgICAgICAgICAgIHJlczogUmVzRGVhbFxuICAgICAgICB9LFxuICAgICAgICBcIkRlYWxDYW5jZWxcIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFEZWFsQ2FuY2VsLFxuICAgICAgICAgICAgcmVzOiBSZXNEZWFsQ2FuY2VsXG4gICAgICAgIH0sXG4gICAgICAgIFwiRGVhbERldGFpbFwiOiB7XG4gICAgICAgICAgICByZXE6IFJlcURlYWxEZXRhaWwsXG4gICAgICAgICAgICByZXM6IFJlc0RlYWxEZXRhaWxcbiAgICAgICAgfSxcbiAgICAgICAgXCJEZWFsSXNzdWVcIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFEZWFsSXNzdWUsXG4gICAgICAgICAgICByZXM6IFJlc0RlYWxJc3N1ZVxuICAgICAgICB9LFxuICAgICAgICBcIkRlYWxPcmRlclwiOiB7XG4gICAgICAgICAgICByZXE6IFJlcURlYWxPcmRlcixcbiAgICAgICAgICAgIHJlczogUmVzRGVhbE9yZGVyXG4gICAgICAgIH0sXG4gICAgICAgIFwiRGVhbFR5cGVcIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFEZWFsVHlwZSxcbiAgICAgICAgICAgIHJlczogUmVzRGVhbFR5cGVcbiAgICAgICAgfSxcbiAgICAgICAgXCJFYXJuaW5nc1wiOiB7XG4gICAgICAgICAgICByZXE6IFJlcUVhcm5pbmdzLFxuICAgICAgICAgICAgcmVzOiBSZXNFYXJuaW5nc1xuICAgICAgICB9LFxuICAgICAgICBcIkZyaWVuZEVhcm5pbmdzRGV0YWlsXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxRnJpZW5kRWFybmluZ3NEZXRhaWwsXG4gICAgICAgICAgICByZXM6IFJlc0ZyaWVuZEVhcm5pbmdzRGV0YWlsXG4gICAgICAgIH0sXG4gICAgICAgIFwiR2l2ZUZyaWVuZFwiOiB7XG4gICAgICAgICAgICByZXE6IFJlcUdpdmVGcmllbmQsXG4gICAgICAgICAgICByZXM6IFJlc0dpdmVGcmllbmRcbiAgICAgICAgfSxcbiAgICAgICAgXCJJbnZpdGVDb2RlXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxSW52aXRlQ29kZSxcbiAgICAgICAgICAgIHJlczogUmVzSW52aXRlQ29kZVxuICAgICAgICB9LFxuICAgICAgICBcIkxpbmtlZGxuXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxTGlua2VkbG4sXG4gICAgICAgICAgICByZXM6IFJlc0xpbmtlZGxuXG4gICAgICAgIH0sXG4gICAgICAgIFwiTG9naW5cIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFMb2dpbixcbiAgICAgICAgICAgIHJlczogUmVzTG9naW5cbiAgICAgICAgfSxcbiAgICAgICAgXCJSYW5rXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxUmFuayxcbiAgICAgICAgICAgIHJlczogUmVzUmFua1xuICAgICAgICB9LFxuICAgICAgICBcIlNlbmRcIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFTZW5kLFxuICAgICAgICAgICAgcmVzOiBSZXNTZW5kXG4gICAgICAgIH0sXG4gICAgICAgIFwiU2VyY2hGcmllbmRcIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFTZXJjaEZyaWVuZCxcbiAgICAgICAgICAgIHJlczogUmVzU2VyY2hGcmllbmRcbiAgICAgICAgfSxcbiAgICAgICAgXCJTaWduXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxU2lnbixcbiAgICAgICAgICAgIHJlczogUmVzU2lnblxuICAgICAgICB9LFxuICAgICAgICBcIlNvY2lhbFNldFwiOiB7XG4gICAgICAgICAgICByZXE6IFJlcVNvY2lhbFNldCxcbiAgICAgICAgICAgIHJlczogUmVzU29jaWFsU2V0XG4gICAgICAgIH0sXG4gICAgICAgIFwiV2hlZWxTdXJmXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxV2hlZWxTdXJmLFxuICAgICAgICAgICAgcmVzOiBSZXNXaGVlbFN1cmZcbiAgICAgICAgfSxcbiAgICAgICAgXCJXb3JrQmVuY2hDdHJsXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxV29ya0JlbmNoQ3RybCxcbiAgICAgICAgICAgIHJlczogUmVzV29ya0JlbmNoQ3RybFxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9HZXRHYW1lU2VydmVyXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxR2V0R2FtZVNlcnZlcixcbiAgICAgICAgICAgIHJlczogUmVzR2V0R2FtZVNlcnZlclxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9Nb2RpZnlcIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFNb2RpZnksXG4gICAgICAgICAgICByZXM6IFJlc01vZGlmeVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9Nb2RpZnlJdGVtXCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxTW9kaWZ5SXRlbSxcbiAgICAgICAgICAgIHJlczogUmVzTW9kaWZ5SXRlbVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9Nb2RpZnlNb25leVwiOiB7XG4gICAgICAgICAgICByZXE6IFJlcU1vZGlmeU1vbmV5LFxuICAgICAgICAgICAgcmVzOiBSZXNNb2RpZnlNb25leVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9Vc2VyTG9naW5cIjoge1xuICAgICAgICAgICAgcmVxOiBSZXFVc2VyTG9naW4sXG4gICAgICAgICAgICByZXM6IFJlc1VzZXJMb2dpblxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9Vc2VyTG9nb3V0XCI6IHtcbiAgICAgICAgICAgIHJlcTogUmVxVXNlckxvZ291dCxcbiAgICAgICAgICAgIHJlczogUmVzVXNlckxvZ291dFxuICAgICAgICB9XG4gICAgfSxcbiAgICBtc2c6IHtcbiAgICAgICAgXCJDaGF0XCI6IE1zZ0NoYXQsXG4gICAgICAgIFwiR2FtZUpzb25cIjogTXNnR2FtZUpzb24sXG4gICAgICAgIFwiTHZVcFwiOiBNc2dMdlVwLFxuICAgICAgICBcIk1ha2VDb2luXCI6IE1zZ01ha2VDb2luLFxuICAgICAgICBcIlNvY2lhbFwiOiBNc2dTb2NpYWwsXG4gICAgICAgIFwiVXBkYXRlQmFja3BhY2tcIjogTXNnVXBkYXRlQmFja3BhY2ssXG4gICAgICAgIFwiVXBkYXRlVXNlckluZm9cIjogTXNnVXBkYXRlVXNlckluZm8sXG4gICAgICAgIFwiV29ya0JlbmNoXCI6IE1zZ1dvcmtCZW5jaCxcbiAgICAgICAgXCJyZW1vdGUvQ29ubmVjdFwiOiBNc2dDb25uZWN0LFxuICAgICAgICBcInJlbW90ZS9Nb2RpZnlcIjogTXNnTW9kaWZ5LFxuICAgICAgICBcInJlbW90ZS9Nb2RpZnlJdGVtXCI6IE1zZ01vZGlmeUl0ZW0sXG4gICAgICAgIFwicmVtb3RlL01vZGlmeU1vbmV5XCI6IE1zZ01vZGlmeU1vbmV5LFxuICAgICAgICBcInJlbW90ZS9SZW1vdGVcIjogTXNnUmVtb3RlXG4gICAgfVxufVxuXG5leHBvcnQgY29uc3Qgc2VydmljZVByb3RvOiBTZXJ2aWNlUHJvdG88U2VydmljZVR5cGU+ID0ge1xuICAgIFwidmVyc2lvblwiOiAzLFxuICAgIFwic2VydmljZXNcIjogW1xuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJnYXRlL0dldFNlcnZlclwiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiYXBpXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiQ2hhdFwiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwibXNnXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiR2FtZUpzb25cIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIm1zZ1wiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMyxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIkx2VXBcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIm1zZ1wiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogNCxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIk1ha2VDb2luXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJtc2dcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDUsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJTb2NpYWxcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIm1zZ1wiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogNixcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIlVwZGF0ZUJhY2twYWNrXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJtc2dcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDcsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJVcGRhdGVVc2VySW5mb1wiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwibXNnXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiA4LFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiV29ya0JlbmNoXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJtc2dcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDksXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJCYWdJbmZvXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDEwLFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiQ29udmVydFwiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiYXBpXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiAxMSxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIkNvbnZlcnRDb2luXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDEyLFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiRGVhbFwiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiYXBpXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiAxMyxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIkRlYWxDYW5jZWxcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMTQsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJEZWFsRGV0YWlsXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDE1LFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiRGVhbElzc3VlXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDE2LFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiRGVhbE9yZGVyXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDE3LFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiRGVhbFR5cGVcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMzksXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJFYXJuaW5nc1wiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiYXBpXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiA0MCxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIkZyaWVuZEVhcm5pbmdzRGV0YWlsXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDE4LFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiR2l2ZUZyaWVuZFwiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiYXBpXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiA0MSxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIkludml0ZUNvZGVcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMTksXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJMaW5rZWRsblwiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiYXBpXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiAyMCxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIkxvZ2luXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDIxLFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiUmFua1wiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiYXBpXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiAyMixcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIlNlbmRcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMjMsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJTZXJjaEZyaWVuZFwiLFxuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiYXBpXCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJpZFwiOiAyNCxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIlNpZ25cIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMjUsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJTb2NpYWxTZXRcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMjYsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJXaGVlbFN1cmZcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMjcsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJXb3JrQmVuY2hDdHJsXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDI4LFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwicmVtb3RlL0Nvbm5lY3RcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIm1zZ1wiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMjksXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJyZW1vdGUvTW9kaWZ5XCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJtc2dcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDMwLFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwicmVtb3RlL01vZGlmeUl0ZW1cIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIm1zZ1wiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMzEsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJyZW1vdGUvTW9kaWZ5TW9uZXlcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIm1zZ1wiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMzIsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJyZW1vdGUvUmVtb3RlXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJtc2dcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDMzLFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwicmVtb3RlL0dldEdhbWVTZXJ2ZXJcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMzQsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJyZW1vdGUvTW9kaWZ5XCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDM1LFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwicmVtb3RlL01vZGlmeUl0ZW1cIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMzYsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJyZW1vdGUvTW9kaWZ5TW9uZXlcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiaWRcIjogMzcsXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJyZW1vdGUvVXNlckxvZ2luXCIsXG4gICAgICAgICAgICBcInR5cGVcIjogXCJhcGlcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgICBcImlkXCI6IDM4LFxuICAgICAgICAgICAgXCJuYW1lXCI6IFwicmVtb3RlL1VzZXJMb2dvdXRcIixcbiAgICAgICAgICAgIFwidHlwZVwiOiBcImFwaVwiXG4gICAgICAgIH1cbiAgICBdLFxuICAgIFwidHlwZXNcIjoge1xuICAgICAgICBcImdhdGUvUHRsR2V0U2VydmVyL1JlcUdldFNlcnZlclwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNvZGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiZ2F0ZS9QdGxHZXRTZXJ2ZXIvUmVzR2V0U2VydmVyXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiaG9zdFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInBvcnRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiTXNnQ2hhdC9Nc2dDaGF0XCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiY29udGVudFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInRpbWVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkRhdGVcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIk1zZ0dhbWVKc29uL01zZ0dhbWVKc29uXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiZ2FtZUpzb25zXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJPYmplY3RcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIk1zZ0x2VXAvTXNnTHZVcFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJsdlwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIm1vbmV5XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMyxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiY2hpcElEXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIk1zZ01ha2VDb2luL01zZ01ha2VDb2luXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiY29pblwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJNc2dTb2NpYWwvTXNnU29jaWFsXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwid2VDaGF0XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiUVFcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiTXNnVXBkYXRlQmFja3BhY2svTXNnVXBkYXRlQmFja3BhY2tcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjaGlwSURcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIFwib3B0aW9uYWxcIjogdHJ1ZVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNoaXBDb3VudFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgXCJvcHRpb25hbFwiOiB0cnVlXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMixcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiYmFja3BhY2tcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJSZWZlcmVuY2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRhcmdldFwiOiBcIk1zZ1VwZGF0ZUJhY2twYWNrL0l0ZW1JbmZvXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDMsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImJhZ1ZvbFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJNc2dVcGRhdGVCYWNrcGFjay9JdGVtSW5mb1wiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIml0ZW1JRFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIml0ZW1Db3VudFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJNc2dVcGRhdGVVc2VySW5mby9Nc2dVcGRhdGVVc2VySW5mb1wiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNvaW5cIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJkaWFtb25kXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogNCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwibW9uZXlcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiTXNnV29ya0JlbmNoL01zZ1dvcmtCZW5jaFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVzZXJXb3JrQmVuY2hcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwibWFrZUNvaW5cIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJidXlOZWVkQ29pblwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxCYWdJbmZvL1JlcUJhZ0luZm9cIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ0eXBlXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bEJhZ0luZm8vUmVzQmFnSW5mb1wiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImJhZ1ZvbFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImJhZ0xpc3RcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJSZWZlcmVuY2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRhcmdldFwiOiBcIk1zZ1VwZGF0ZUJhY2twYWNrL0l0ZW1JbmZvXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxDb252ZXJ0L1JlcUNvbnZlcnRcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxDb252ZXJ0L1Jlc0NvbnZlcnRcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxDb252ZXJ0Q29pbi9SZXFDb252ZXJ0Q29pblwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIm51bVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxDb252ZXJ0Q29pbi9SZXNDb252ZXJ0Q29pblwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIlxuICAgICAgICB9LFxuICAgICAgICBcIlB0bERlYWwvUmVxRGVhbFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjaGlwSURcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsRGVhbC9SZXNEZWFsXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiZGVhbExpc3RcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJSZWZlcmVuY2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRhcmdldFwiOiBcIlB0bERlYWwvVHJhbnNhY3Rpb25JbmZvXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxEZWFsL1RyYW5zYWN0aW9uSW5mb1wiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImd1aWRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjaGlwSURcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJuZWVkQ291bnRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAzLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjdXJyZW50Q291bnRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiA0LFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJwcmljZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDUsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVzZXJBY2NvdW50XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogNixcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwic3RhdHVzXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bERlYWxDYW5jZWwvUmVxRGVhbENhbmNlbFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJkZWFsSURcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsRGVhbENhbmNlbC9SZXNEZWFsQ2FuY2VsXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiZGVhbExpc3RcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJSZWZlcmVuY2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRhcmdldFwiOiBcIlB0bERlYWwvVHJhbnNhY3Rpb25JbmZvXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxEZWFsRGV0YWlsL1JlcURlYWxEZXRhaWxcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxEZWFsRGV0YWlsL1Jlc0RlYWxEZXRhaWxcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJkZWFsTGlzdFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiQXJyYXlcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZWxlbWVudFR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlJlZmVyZW5jZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGFyZ2V0XCI6IFwiUHRsRGVhbERldGFpbC9EZWFsRGV0YWlsSW5mb1wiXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsRGVhbERldGFpbC9EZWFsRGV0YWlsSW5mb1wiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNoaXBJRFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInRpbWVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ0eXBlXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMyxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiY291bnRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiA0LFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJwcmljZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxEZWFsSXNzdWUvUmVxRGVhbElzc3VlXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiZGVhbFR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjaGlwSURcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJwcmljZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDMsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNoaXBOdW1cIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsRGVhbElzc3VlL1Jlc0RlYWxJc3N1ZVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImRlYWxMaXN0XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJBcnJheVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJlbGVtZW50VHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiUmVmZXJlbmNlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0YXJnZXRcIjogXCJQdGxEZWFsL1RyYW5zYWN0aW9uSW5mb1wiXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsRGVhbE9yZGVyL1JlcURlYWxPcmRlclwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVzZXJBY2NvdW50XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidHlwZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxEZWFsT3JkZXIvUmVzRGVhbE9yZGVyXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiZGVhbExpc3RcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJSZWZlcmVuY2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRhcmdldFwiOiBcIlB0bERlYWwvVHJhbnNhY3Rpb25JbmZvXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxEZWFsVHlwZS9SZXFEZWFsVHlwZVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJkZWFsSURcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjb3VudFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxEZWFsVHlwZS9SZXNEZWFsVHlwZVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImRlYWxMaXN0XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJBcnJheVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJlbGVtZW50VHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiUmVmZXJlbmNlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0YXJnZXRcIjogXCJQdGxEZWFsL1RyYW5zYWN0aW9uSW5mb1wiXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsRWFybmluZ3MvUmVxRWFybmluZ3NcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxFYXJuaW5ncy9SZXNFYXJuaW5nc1wiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIm15UGFyZW50SW5mb1wiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJuYW1lXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidXJsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwid2VDaGF0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwicXFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiA0LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJpZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInRvZGF5RXJhbmluZ3NcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ0b3RhbEVhcm5pbmdzXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMyxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiZnJpZW5kTnVtXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bEZyaWVuZEVhcm5pbmdzRGV0YWlsL1JlcUZyaWVuZEVhcm5pbmdzRGV0YWlsXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsRnJpZW5kRWFybmluZ3NEZXRhaWwvUmVzRnJpZW5kRWFybmluZ3NEZXRhaWxcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ0b2RheUVyYW5pbmdzXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidG90YWxFcmFuaW5nc1wiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImVyYW5pbmdzRGV0YWlsTGlzdFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiQXJyYXlcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZWxlbWVudFR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlJlZmVyZW5jZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidGFyZ2V0XCI6IFwiUHRsRnJpZW5kRWFybmluZ3NEZXRhaWwvZWFybmluZ3NEZXRhaWxJdGVtXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxGcmllbmRFYXJuaW5nc0RldGFpbC9lYXJuaW5nc0RldGFpbEl0ZW1cIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ0aW1lXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwib25lRnJpZW5kRXJhbmluZ3NcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ0d29GcmllbmRFcmFuaW5nc1wiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDMsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInRvZGF5VG90YWxFcmFuaW5nc1wiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxHaXZlRnJpZW5kL1JlcUdpdmVGcmllbmRcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJudW1cIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxHaXZlRnJpZW5kL1Jlc0dpdmVGcmllbmRcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxJbnZpdGVDb2RlL1JlcUludml0ZUNvZGVcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjb2RlXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bEludml0ZUNvZGUvUmVzSW52aXRlQ29kZVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInBhcmVudEluZm9cIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwibmFtZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVybFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIndlQ2hhdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDMsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInFxXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bExpbmtlZGxuL1JlcUxpbmtlZGxuXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidXNlckFjY291bnRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJmcmllbmRUeXBlXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bExpbmtlZGxuL1Jlc0xpbmtlZGxuXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiZnJpZW5kQXJyYXlcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJSZWZlcmVuY2VcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRhcmdldFwiOiBcIlB0bExpbmtlZGxuL0xpbmtlZGxuSXRlbVwiXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsTGlua2VkbG4vTGlua2VkbG5JdGVtXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwibmFtZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVybFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInRpbWVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAzLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJsdlwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDQsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImFjdGl2ZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxMb2dpbi9SZXFMb2dpblwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNvZGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsTG9naW4vUmVzTG9naW5cIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ1c2VySW5mb1wiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ1c2VyTmFtZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVzZXJBdmF0YXJVcmxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ1c2VyQWNjb3VudFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDMsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVzZXJDb2luXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjogNCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidXNlckRpYW1vbmRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiA3LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ1c2VyTW9uZXlcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsUmFuay9SZXFSYW5rXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidHlwZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxSYW5rL1Jlc1JhbmtcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJyYW5rXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJBcnJheVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJlbGVtZW50VHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiUmVmZXJlbmNlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0YXJnZXRcIjogXCJQdGxSYW5rL1JhbmtJdGVtXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxSYW5rL1JhbmtJdGVtXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidWlkXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwibmFtZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIm1vbmV5XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMyxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwicGljVXJsXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bFNlbmQvUmVxU2VuZFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNvbnRlbnRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsU2VuZC9SZXNTZW5kXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidGltZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiRGF0ZVwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsU2VyY2hGcmllbmQvUmVxU2VyY2hGcmllbmRcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxTZXJjaEZyaWVuZC9SZXNTZXJjaEZyaWVuZFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInNlcmNoSW5mb1wiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJuYW1lXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidXJsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiaWRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsU2lnbi9SZXFTaWduXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidXNlckFjY291bnRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJyZWNlaXZlSW5kZXhcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIFwib3B0aW9uYWxcIjogdHJ1ZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxTaWduL1Jlc1NpZ25cIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJzaWduRGF5XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwibWF4U2lnbkRheVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInJlY2VpdmVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bFNvY2lhbFNldC9SZXFTb2NpYWxTZXRcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ3ZUNoYXRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAxLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJRUVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJQdGxTb2NpYWxTZXQvUmVzU29jaWFsU2V0XCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsV2hlZWxTdXJmL1JlcVdoZWVsU3VyZlwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVzZXJBY2NvdW50XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwib3BlcmF0aW9uSWRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwiUHRsV2hlZWxTdXJmL1Jlc1doZWVsU3VyZlwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImRyYXdJZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImRyYXdDb3VudFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImFkdmVydENvdW50XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bFdvcmtCZW5jaEN0cmwvUmVxV29ya0JlbmNoQ3RybFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVzZXJBY2NvdW50XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiY3RybFR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjdXJQb3NcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIFwib3B0aW9uYWxcIjogdHJ1ZVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDMsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInRhcmdldFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgXCJvcHRpb25hbFwiOiB0cnVlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcIlB0bFdvcmtCZW5jaEN0cmwvUmVzV29ya0JlbmNoQ3RybFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVzZXJXb3JrQmVuY2hcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkFycmF5XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBcImVsZW1lbnRUeXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9Nc2dDb25uZWN0L01zZ0Nvbm5lY3RcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJ0eXBlXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwic2VydmVySURcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIFwib3B0aW9uYWxcIjogdHJ1ZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJyZW1vdGUvTXNnTW9kaWZ5L01zZ01vZGlmeVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIk51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwicmVtb3RlL01zZ01vZGlmeUl0ZW0vTXNnTW9kaWZ5SXRlbVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNvdW50XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9Nc2dNb2RpZnlNb25leS9Nc2dNb2RpZnlNb25leVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjb3VudFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJyZW1vdGUvTXNnUmVtb3RlL01zZ1JlbW90ZVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImFyZ1wiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiQW55XCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJyZW1vdGUvUHRsR2V0R2FtZVNlcnZlci9SZXFHZXRHYW1lU2VydmVyXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiXG4gICAgICAgIH0sXG4gICAgICAgIFwicmVtb3RlL1B0bEdldEdhbWVTZXJ2ZXIvUmVzR2V0R2FtZVNlcnZlclwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInNlcnZlcklEXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9QdGxNb2RpZnkvUmVxTW9kaWZ5XCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidWlkXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwiY2lkXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMixcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidHlwZVwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJyZW1vdGUvUHRsTW9kaWZ5L1Jlc01vZGlmeVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInJlc3VsdFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiQm9vbGVhblwiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0sXG4gICAgICAgIFwicmVtb3RlL1B0bE1vZGlmeUl0ZW0vUmVxTW9kaWZ5SXRlbVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDIsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcImNvdW50XCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9QdGxNb2RpZnlJdGVtL1Jlc01vZGlmeUl0ZW1cIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJyZXN1bHRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkJvb2xlYW5cIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9QdGxNb2RpZnlNb25leS9SZXFNb2RpZnlNb25leVwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInR5cGVcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIlN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAyLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJjb3VudFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJyZW1vdGUvUHRsTW9kaWZ5TW9uZXkvUmVzTW9kaWZ5TW9uZXlcIjoge1xuICAgICAgICAgICAgXCJ0eXBlXCI6IFwiSW50ZXJmYWNlXCIsXG4gICAgICAgICAgICBcInByb3BlcnRpZXNcIjogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgXCJpZFwiOiAwLFxuICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJyZXN1bHRcIixcbiAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiBcIkJvb2xlYW5cIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9QdGxVc2VyTG9naW4vUmVxVXNlckxvZ2luXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiLFxuICAgICAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMCxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwic2lkXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJOdW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIFwiaWRcIjogMSxcbiAgICAgICAgICAgICAgICAgICAgXCJuYW1lXCI6IFwidWlkXCIsXG4gICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJTdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9LFxuICAgICAgICBcInJlbW90ZS9QdGxVc2VyTG9naW4vUmVzVXNlckxvZ2luXCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiXG4gICAgICAgIH0sXG4gICAgICAgIFwicmVtb3RlL1B0bFVzZXJMb2dvdXQvUmVxVXNlckxvZ291dFwiOiB7XG4gICAgICAgICAgICBcInR5cGVcIjogXCJJbnRlcmZhY2VcIixcbiAgICAgICAgICAgIFwicHJvcGVydGllc1wiOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDAsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInNpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiTnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBcImlkXCI6IDEsXG4gICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcInVpZFwiLFxuICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6IFwiU3RyaW5nXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSxcbiAgICAgICAgXCJyZW1vdGUvUHRsVXNlckxvZ291dC9SZXNVc2VyTG9nb3V0XCI6IHtcbiAgICAgICAgICAgIFwidHlwZVwiOiBcIkludGVyZmFjZVwiXG4gICAgICAgIH1cbiAgICB9XG59OyJdfQ==