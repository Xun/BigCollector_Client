
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Net/EventCenter.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '15cf5C/OS9IyY5TeN5ujTW/', 'EventCenter');
// Script/Net/EventCenter.ts

"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventCenter = exports.EventInfo = void 0;
var Pool_1 = require("../Common/Pool");
var EventInfo = /** @class */ (function () {
    function EventInfo() {
    }
    EventInfo.prototype.free = function () {
        this.callback = null;
        this.target = null;
        this.once = false;
    };
    EventInfo.prototype.init = function (callback, target, once) {
        this.callback = callback;
        this.target = target;
        this.once = once;
    };
    return EventInfo;
}());
exports.EventInfo = EventInfo;
var RemoveCommand = /** @class */ (function () {
    function RemoveCommand(eventName, callback, targetId) {
        this.eventName = eventName;
        this.callback = callback;
        this.targetId = targetId;
    }
    return RemoveCommand;
}());
var idSeed = 1; // 这里有一个小缺陷就是idSeed有最大值,Number.MAX_VALUE
var EventCenter = /** @class */ (function () {
    function EventCenter() {
    }
    EventCenter.on = function (eventName, callback, target, once) {
        if (target === void 0) { target = undefined; }
        if (once === void 0) { once = false; }
        target = target || this;
        var targetId = target['uuid'] || target['id'];
        if (targetId === undefined) {
            target['uuid'] = targetId = '' + idSeed++;
        }
        this.onById(eventName, targetId, target, callback, once);
    };
    EventCenter.once = function (eventName, callback, target) {
        if (target === void 0) { target = undefined; }
        this.on(eventName, callback, target, true);
    };
    EventCenter.onById = function (eventName, targetId, target, cb, once) {
        var collection = this._listeners[eventName];
        if (!collection) {
            collection = this._listeners[eventName] = {};
        }
        var events = collection[targetId];
        if (!events) {
            events = collection[targetId] = [];
        }
        var eventInfo = this._eventPool.alloc();
        eventInfo.init(cb, target, once);
        events.push(eventInfo);
    };
    EventCenter.off = function (eventName, callback, target) {
        if (target === void 0) { target = undefined; }
        target = target || this;
        var targetId = target['uuid'] || target['id'];
        if (!targetId)
            return;
        this.offById(eventName, callback, targetId);
    };
    EventCenter.targetOff = function (target) {
        target = target || this;
        var targetId = target['uuid'] || target['id'];
        if (!targetId)
            return;
        for (var event in this._listeners) {
            var collection = this._listeners[event];
            if (collection[targetId] !== undefined) {
                delete collection[targetId];
            }
        }
    };
    EventCenter.offById = function (eventName, callback, targetId) {
        if (this._dispatching > 0) {
            var cmd = new RemoveCommand(eventName, callback, targetId);
            this._removeCommands.push(cmd);
        }
        else {
            this.doOff(eventName, callback, targetId);
        }
    };
    EventCenter.doOff = function (eventName, callback, targetId) {
        var collection = this._listeners[eventName];
        if (!collection)
            return;
        var events = collection[targetId];
        if (!events)
            return;
        for (var i = events.length - 1; i >= 0; i--) {
            if (events[i].callback === callback) {
                events.splice(i, 1);
            }
        }
        if (events.length === 0) {
            collection[targetId] = null;
            delete collection[targetId];
        }
    };
    EventCenter.doRemoveCommands = function () {
        if (this._dispatching !== 0) {
            return;
        }
        for (var _i = 0, _a = this._removeCommands; _i < _a.length; _i++) {
            var cmd = _a[_i];
            this.doOff(cmd.eventName, cmd.callback, cmd.targetId);
        }
        this._removeCommands.length = 0;
    };
    EventCenter.emit = function (eventName) {
        var _a;
        var param = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            param[_i - 1] = arguments[_i];
        }
        var collection = this._listeners[eventName];
        if (!collection)
            return false;
        this._dispatching++;
        for (var targetId in collection) {
            for (var _b = 0, _c = collection[targetId]; _b < _c.length; _b++) {
                var eventInfo = _c[_b];
                (_a = eventInfo.callback).call.apply(_a, __spreadArrays([eventInfo.target], param));
                if (eventInfo.once) {
                    var cmd = new RemoveCommand(eventName, eventInfo.callback, targetId);
                    this._removeCommands.push(cmd);
                }
            }
        }
        this._dispatching--;
        this.doRemoveCommands();
    };
    EventCenter._listeners = cc.js.createMap();
    EventCenter._dispatching = 0;
    EventCenter._removeCommands = [];
    EventCenter._eventPool = new Pool_1.Pool(function () {
        return new EventInfo();
    }, 10);
    return EventCenter;
}());
exports.EventCenter = EventCenter;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTmV0L0V2ZW50Q2VudGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBNkM7QUFJN0M7SUFBQTtJQWdCQSxDQUFDO0lBWEcsd0JBQUksR0FBSjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRCx3QkFBSSxHQUFKLFVBQUssUUFBa0IsRUFBRSxNQUFjLEVBQUUsSUFBYTtRQUNsRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNyQixDQUFDO0lBQ0wsZ0JBQUM7QUFBRCxDQWhCQSxBQWdCQyxJQUFBO0FBaEJZLDhCQUFTO0FBa0J0QjtJQUtJLHVCQUFZLFNBQWlCLEVBQUUsUUFBa0IsRUFBRSxRQUFnQjtRQUMvRCxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMzQixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztJQUM3QixDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQVZBLEFBVUMsSUFBQTtBQUVELElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFTLHdDQUF3QztBQUNoRTtJQUFBO0lBc0dBLENBQUM7SUE1RmlCLGNBQUUsR0FBaEIsVUFBaUIsU0FBaUIsRUFBRSxRQUFrQixFQUFFLE1BQXVCLEVBQUUsSUFBWTtRQUFyQyx1QkFBQSxFQUFBLGtCQUF1QjtRQUFFLHFCQUFBLEVBQUEsWUFBWTtRQUN6RixNQUFNLEdBQUcsTUFBTSxJQUFJLElBQUksQ0FBQztRQUN4QixJQUFJLFFBQVEsR0FBVyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RELElBQUksUUFBUSxLQUFLLFNBQVMsRUFBRTtZQUN4QixNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsUUFBUSxHQUFHLEVBQUUsR0FBRyxNQUFNLEVBQUUsQ0FBQztTQUM3QztRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFDYSxnQkFBSSxHQUFsQixVQUFtQixTQUFpQixFQUFFLFFBQWtCLEVBQUUsTUFBdUI7UUFBdkIsdUJBQUEsRUFBQSxrQkFBdUI7UUFDN0UsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBQ2Msa0JBQU0sR0FBckIsVUFBc0IsU0FBaUIsRUFBRSxRQUFnQixFQUFFLE1BQVcsRUFBRSxFQUFZLEVBQUUsSUFBYTtRQUMvRixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDYixVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDaEQ7UUFDRCxJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNULE1BQU0sR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN4QyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDakMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRWEsZUFBRyxHQUFqQixVQUFrQixTQUFpQixFQUFFLFFBQWtCLEVBQUUsTUFBdUI7UUFBdkIsdUJBQUEsRUFBQSxrQkFBdUI7UUFDNUUsTUFBTSxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUM7UUFDeEIsSUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsUUFBUTtZQUFFLE9BQU87UUFDdEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFDYSxxQkFBUyxHQUF2QixVQUF3QixNQUFXO1FBQy9CLE1BQU0sR0FBRyxNQUFNLElBQUksSUFBSSxDQUFDO1FBQ3hCLElBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLFFBQVE7WUFBRSxPQUFPO1FBQ3RCLEtBQUssSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUMvQixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hDLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxLQUFLLFNBQVMsRUFBRTtnQkFDcEMsT0FBTyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDL0I7U0FDSjtJQUNMLENBQUM7SUFDYyxtQkFBTyxHQUF0QixVQUF1QixTQUFpQixFQUFFLFFBQWtCLEVBQUUsUUFBZ0I7UUFDMUUsSUFBSSxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsRUFBRTtZQUN2QixJQUFJLEdBQUcsR0FBRyxJQUFJLGFBQWEsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ2xDO2FBQU07WUFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDN0M7SUFDTCxDQUFDO0lBQ2MsaUJBQUssR0FBcEIsVUFBcUIsU0FBaUIsRUFBRSxRQUFrQixFQUFFLFFBQWdCO1FBQ3hFLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFVBQVU7WUFBRSxPQUFPO1FBQ3hCLElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsTUFBTTtZQUFFLE9BQU87UUFDcEIsS0FBSyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7Z0JBQ2pDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ3ZCO1NBQ0o7UUFDRCxJQUFJLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3JCLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDNUIsT0FBTyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDL0I7SUFDTCxDQUFDO0lBRWMsNEJBQWdCLEdBQS9CO1FBQ0ksSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLENBQUMsRUFBRTtZQUN6QixPQUFPO1NBQ1Y7UUFDRCxLQUFnQixVQUFvQixFQUFwQixLQUFBLElBQUksQ0FBQyxlQUFlLEVBQXBCLGNBQW9CLEVBQXBCLElBQW9CLEVBQUU7WUFBakMsSUFBSSxHQUFHLFNBQUE7WUFDUixJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDekQ7UUFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVhLGdCQUFJLEdBQWxCLFVBQW1CLFNBQWlCOztRQUFFLGVBQWU7YUFBZixVQUFlLEVBQWYscUJBQWUsRUFBZixJQUFlO1lBQWYsOEJBQWU7O1FBQ2pELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFVBQVU7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUM5QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsS0FBSyxJQUFJLFFBQVEsSUFBSSxVQUFVLEVBQUU7WUFDN0IsS0FBc0IsVUFBb0IsRUFBcEIsS0FBQSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQXBCLGNBQW9CLEVBQXBCLElBQW9CLEVBQUU7Z0JBQXZDLElBQUksU0FBUyxTQUFBO2dCQUNkLENBQUEsS0FBQSxTQUFTLENBQUMsUUFBUSxDQUFBLENBQUMsSUFBSSwyQkFBQyxTQUFTLENBQUMsTUFBTSxHQUFLLEtBQUssR0FBRTtnQkFDcEQsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFO29CQUNoQixJQUFJLEdBQUcsR0FBRyxJQUFJLGFBQWEsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQztvQkFDckUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ2xDO2FBQ0o7U0FDSjtRQUNELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBbkdjLHNCQUFVLEdBQWdFLEVBQUUsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDNUYsd0JBQVksR0FBVyxDQUFDLENBQUM7SUFDekIsMkJBQWUsR0FBb0IsRUFBRSxDQUFDO0lBRXRDLHNCQUFVLEdBQW9CLElBQUksV0FBSSxDQUFZO1FBQzdELE9BQU8sSUFBSSxTQUFTLEVBQUUsQ0FBQztJQUMzQixDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUE4Rlgsa0JBQUM7Q0F0R0QsQUFzR0MsSUFBQTtBQXRHWSxrQ0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElQb29sLCBQb29sIH0gZnJvbSBcIi4uL0NvbW1vbi9Qb29sXCI7XG5cblxuXG5leHBvcnQgY2xhc3MgRXZlbnRJbmZvIGltcGxlbWVudHMgSVBvb2wge1xuICAgIGNhbGxiYWNrOiBGdW5jdGlvbjtcbiAgICB0YXJnZXQ6IGFueTtcbiAgICBvbmNlOiBib29sZWFuO1xuXG4gICAgZnJlZSgpIHtcbiAgICAgICAgdGhpcy5jYWxsYmFjayA9IG51bGw7XG4gICAgICAgIHRoaXMudGFyZ2V0ID0gbnVsbDtcbiAgICAgICAgdGhpcy5vbmNlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaW5pdChjYWxsYmFjazogRnVuY3Rpb24sIHRhcmdldDogT2JqZWN0LCBvbmNlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuY2FsbGJhY2sgPSBjYWxsYmFjaztcbiAgICAgICAgdGhpcy50YXJnZXQgPSB0YXJnZXQ7XG4gICAgICAgIHRoaXMub25jZSA9IG9uY2U7XG4gICAgfVxufVxuXG5jbGFzcyBSZW1vdmVDb21tYW5kIHtcbiAgICBwdWJsaWMgZXZlbnROYW1lOiBzdHJpbmc7XG4gICAgcHVibGljIHRhcmdldElkOiBzdHJpbmc7XG4gICAgcHVibGljIGNhbGxiYWNrOiBGdW5jdGlvbjtcblxuICAgIGNvbnN0cnVjdG9yKGV2ZW50TmFtZTogc3RyaW5nLCBjYWxsYmFjazogRnVuY3Rpb24sIHRhcmdldElkOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5ldmVudE5hbWUgPSBldmVudE5hbWU7XG4gICAgICAgIHRoaXMuY2FsbGJhY2sgPSBjYWxsYmFjaztcbiAgICAgICAgdGhpcy50YXJnZXRJZCA9IHRhcmdldElkO1xuICAgIH1cbn1cblxubGV0IGlkU2VlZCA9IDE7ICAgICAgICAgLy8g6L+Z6YeM5pyJ5LiA5Liq5bCP57y66Zm35bCx5pivaWRTZWVk5pyJ5pyA5aSn5YC8LE51bWJlci5NQVhfVkFMVUVcbmV4cG9ydCBjbGFzcyBFdmVudENlbnRlciB7XG5cbiAgICBwcml2YXRlIHN0YXRpYyBfbGlzdGVuZXJzOiB7IFtldmVudE5hbWU6IHN0cmluZ106IHsgW2lkOiBzdHJpbmddOiBBcnJheTxFdmVudEluZm8+IH0gfSA9IGNjLmpzLmNyZWF0ZU1hcCgpO1xuICAgIHByaXZhdGUgc3RhdGljIF9kaXNwYXRjaGluZzogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIHN0YXRpYyBfcmVtb3ZlQ29tbWFuZHM6IFJlbW92ZUNvbW1hbmRbXSA9IFtdO1xuXG4gICAgcHJpdmF0ZSBzdGF0aWMgX2V2ZW50UG9vbDogUG9vbDxFdmVudEluZm8+ID0gbmV3IFBvb2w8RXZlbnRJbmZvPigoKSA9PiB7XG4gICAgICAgIHJldHVybiBuZXcgRXZlbnRJbmZvKCk7XG4gICAgfSwgMTApO1xuXG4gICAgcHVibGljIHN0YXRpYyBvbihldmVudE5hbWU6IHN0cmluZywgY2FsbGJhY2s6IEZ1bmN0aW9uLCB0YXJnZXQ6IGFueSA9IHVuZGVmaW5lZCwgb25jZSA9IGZhbHNlKSB7XG4gICAgICAgIHRhcmdldCA9IHRhcmdldCB8fCB0aGlzO1xuICAgICAgICBsZXQgdGFyZ2V0SWQ6IHN0cmluZyA9IHRhcmdldFsndXVpZCddIHx8IHRhcmdldFsnaWQnXTtcbiAgICAgICAgaWYgKHRhcmdldElkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRhcmdldFsndXVpZCddID0gdGFyZ2V0SWQgPSAnJyArIGlkU2VlZCsrO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMub25CeUlkKGV2ZW50TmFtZSwgdGFyZ2V0SWQsIHRhcmdldCwgY2FsbGJhY2ssIG9uY2UpO1xuICAgIH1cbiAgICBwdWJsaWMgc3RhdGljIG9uY2UoZXZlbnROYW1lOiBzdHJpbmcsIGNhbGxiYWNrOiBGdW5jdGlvbiwgdGFyZ2V0OiBhbnkgPSB1bmRlZmluZWQpIHtcbiAgICAgICAgdGhpcy5vbihldmVudE5hbWUsIGNhbGxiYWNrLCB0YXJnZXQsIHRydWUpO1xuICAgIH1cbiAgICBwcml2YXRlIHN0YXRpYyBvbkJ5SWQoZXZlbnROYW1lOiBzdHJpbmcsIHRhcmdldElkOiBzdHJpbmcsIHRhcmdldDogYW55LCBjYjogRnVuY3Rpb24sIG9uY2U6IGJvb2xlYW4pIHtcbiAgICAgICAgbGV0IGNvbGxlY3Rpb24gPSB0aGlzLl9saXN0ZW5lcnNbZXZlbnROYW1lXTtcbiAgICAgICAgaWYgKCFjb2xsZWN0aW9uKSB7XG4gICAgICAgICAgICBjb2xsZWN0aW9uID0gdGhpcy5fbGlzdGVuZXJzW2V2ZW50TmFtZV0gPSB7fTtcbiAgICAgICAgfVxuICAgICAgICBsZXQgZXZlbnRzID0gY29sbGVjdGlvblt0YXJnZXRJZF07XG4gICAgICAgIGlmICghZXZlbnRzKSB7XG4gICAgICAgICAgICBldmVudHMgPSBjb2xsZWN0aW9uW3RhcmdldElkXSA9IFtdO1xuICAgICAgICB9XG4gICAgICAgIGxldCBldmVudEluZm8gPSB0aGlzLl9ldmVudFBvb2wuYWxsb2MoKTtcbiAgICAgICAgZXZlbnRJbmZvLmluaXQoY2IsIHRhcmdldCwgb25jZSk7XG4gICAgICAgIGV2ZW50cy5wdXNoKGV2ZW50SW5mbyk7XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBvZmYoZXZlbnROYW1lOiBzdHJpbmcsIGNhbGxiYWNrOiBGdW5jdGlvbiwgdGFyZ2V0OiBhbnkgPSB1bmRlZmluZWQpIHtcbiAgICAgICAgdGFyZ2V0ID0gdGFyZ2V0IHx8IHRoaXM7XG4gICAgICAgIGxldCB0YXJnZXRJZCA9IHRhcmdldFsndXVpZCddIHx8IHRhcmdldFsnaWQnXTtcbiAgICAgICAgaWYgKCF0YXJnZXRJZCkgcmV0dXJuO1xuICAgICAgICB0aGlzLm9mZkJ5SWQoZXZlbnROYW1lLCBjYWxsYmFjaywgdGFyZ2V0SWQpO1xuICAgIH1cbiAgICBwdWJsaWMgc3RhdGljIHRhcmdldE9mZih0YXJnZXQ6IGFueSkge1xuICAgICAgICB0YXJnZXQgPSB0YXJnZXQgfHwgdGhpcztcbiAgICAgICAgbGV0IHRhcmdldElkID0gdGFyZ2V0Wyd1dWlkJ10gfHwgdGFyZ2V0WydpZCddO1xuICAgICAgICBpZiAoIXRhcmdldElkKSByZXR1cm47XG4gICAgICAgIGZvciAobGV0IGV2ZW50IGluIHRoaXMuX2xpc3RlbmVycykge1xuICAgICAgICAgICAgbGV0IGNvbGxlY3Rpb24gPSB0aGlzLl9saXN0ZW5lcnNbZXZlbnRdO1xuICAgICAgICAgICAgaWYgKGNvbGxlY3Rpb25bdGFyZ2V0SWRdICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBkZWxldGUgY29sbGVjdGlvblt0YXJnZXRJZF07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgcHJpdmF0ZSBzdGF0aWMgb2ZmQnlJZChldmVudE5hbWU6IHN0cmluZywgY2FsbGJhY2s6IEZ1bmN0aW9uLCB0YXJnZXRJZDogc3RyaW5nKSB7XG4gICAgICAgIGlmICh0aGlzLl9kaXNwYXRjaGluZyA+IDApIHtcbiAgICAgICAgICAgIGxldCBjbWQgPSBuZXcgUmVtb3ZlQ29tbWFuZChldmVudE5hbWUsIGNhbGxiYWNrLCB0YXJnZXRJZCk7XG4gICAgICAgICAgICB0aGlzLl9yZW1vdmVDb21tYW5kcy5wdXNoKGNtZCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmRvT2ZmKGV2ZW50TmFtZSwgY2FsbGJhY2ssIHRhcmdldElkKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBwcml2YXRlIHN0YXRpYyBkb09mZihldmVudE5hbWU6IHN0cmluZywgY2FsbGJhY2s6IEZ1bmN0aW9uLCB0YXJnZXRJZDogc3RyaW5nKSB7XG4gICAgICAgIGxldCBjb2xsZWN0aW9uID0gdGhpcy5fbGlzdGVuZXJzW2V2ZW50TmFtZV07XG4gICAgICAgIGlmICghY29sbGVjdGlvbikgcmV0dXJuO1xuICAgICAgICBsZXQgZXZlbnRzID0gY29sbGVjdGlvblt0YXJnZXRJZF07XG4gICAgICAgIGlmICghZXZlbnRzKSByZXR1cm47XG4gICAgICAgIGZvciAobGV0IGkgPSBldmVudHMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICAgIGlmIChldmVudHNbaV0uY2FsbGJhY2sgPT09IGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgICAgZXZlbnRzLnNwbGljZShpLCAxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpZiAoZXZlbnRzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgY29sbGVjdGlvblt0YXJnZXRJZF0gPSBudWxsO1xuICAgICAgICAgICAgZGVsZXRlIGNvbGxlY3Rpb25bdGFyZ2V0SWRdO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzdGF0aWMgZG9SZW1vdmVDb21tYW5kcygpIHtcbiAgICAgICAgaWYgKHRoaXMuX2Rpc3BhdGNoaW5nICE9PSAwKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgZm9yIChsZXQgY21kIG9mIHRoaXMuX3JlbW92ZUNvbW1hbmRzKSB7XG4gICAgICAgICAgICB0aGlzLmRvT2ZmKGNtZC5ldmVudE5hbWUsIGNtZC5jYWxsYmFjaywgY21kLnRhcmdldElkKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9yZW1vdmVDb21tYW5kcy5sZW5ndGggPSAwO1xuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgZW1pdChldmVudE5hbWU6IHN0cmluZywgLi4ucGFyYW06IGFueVtdKSB7XG4gICAgICAgIGxldCBjb2xsZWN0aW9uID0gdGhpcy5fbGlzdGVuZXJzW2V2ZW50TmFtZV07XG4gICAgICAgIGlmICghY29sbGVjdGlvbikgcmV0dXJuIGZhbHNlO1xuICAgICAgICB0aGlzLl9kaXNwYXRjaGluZysrO1xuICAgICAgICBmb3IgKGxldCB0YXJnZXRJZCBpbiBjb2xsZWN0aW9uKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBldmVudEluZm8gb2YgY29sbGVjdGlvblt0YXJnZXRJZF0pIHtcbiAgICAgICAgICAgICAgICBldmVudEluZm8uY2FsbGJhY2suY2FsbChldmVudEluZm8udGFyZ2V0LCAuLi5wYXJhbSk7XG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50SW5mby5vbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBjbWQgPSBuZXcgUmVtb3ZlQ29tbWFuZChldmVudE5hbWUsIGV2ZW50SW5mby5jYWxsYmFjaywgdGFyZ2V0SWQpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9yZW1vdmVDb21tYW5kcy5wdXNoKGNtZCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2Rpc3BhdGNoaW5nLS07XG4gICAgICAgIHRoaXMuZG9SZW1vdmVDb21tYW5kcygpO1xuICAgIH1cbn1cbiJdfQ==