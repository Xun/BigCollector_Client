
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Net/EventType.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd91c3NJBEVLOImFlZf1j8Q7', 'EventType');
// Script/Net/EventType.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventType = void 0;
/** 事件名称 */
var EventType;
(function (EventType) {
    EventType["GameShow"] = "GameShow";
    EventType["GameHide"] = "GameHide";
    EventType["WindowClosed"] = "WindowClosed";
    EventType["FormClosed"] = "FormClosed";
})(EventType = exports.EventType || (exports.EventType = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTmV0L0V2ZW50VHlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxXQUFXO0FBQ1gsSUFBWSxTQU9YO0FBUEQsV0FBWSxTQUFTO0lBQ2pCLGtDQUFxQixDQUFBO0lBQ3JCLGtDQUFxQixDQUFBO0lBRXJCLDBDQUE2QixDQUFBO0lBQzdCLHNDQUF5QixDQUFBO0FBRTdCLENBQUMsRUFQVyxTQUFTLEdBQVQsaUJBQVMsS0FBVCxpQkFBUyxRQU9wQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8qKiDkuovku7blkI3np7AgKi9cbmV4cG9ydCBlbnVtIEV2ZW50VHlwZSB7XG4gICAgR2FtZVNob3cgPSBcIkdhbWVTaG93XCIsXG4gICAgR2FtZUhpZGUgPSBcIkdhbWVIaWRlXCIsXG5cbiAgICBXaW5kb3dDbG9zZWQgPSBcIldpbmRvd0Nsb3NlZFwiLFxuICAgIEZvcm1DbG9zZWQgPSBcIkZvcm1DbG9zZWRcIixcblxufSJdfQ==