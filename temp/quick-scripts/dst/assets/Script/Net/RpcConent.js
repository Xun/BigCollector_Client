
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Net/RpcConent.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '24e90+N+BJCD69wJUWlBqdD', 'RpcConent');
// Script/Net/RpcConent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiClient = void 0;
var tsrpc_browser_1 = require("tsrpc-browser");
var serviceProto_1 = require("./shared/protocols/serviceProto");
exports.apiClient = new tsrpc_browser_1.WsClient(serviceProto_1.serviceProto, {
    server: 'ws://192.168.0.104:3004',
    json: false,
    // jsonPrune: false,
    logger: console,
    heartbeat: {
        interval: 3000,
        timeout: 5000
    }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTmV0L1JwY0NvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSwrQ0FBeUM7QUFDekMsZ0VBQStEO0FBRWxELFFBQUEsU0FBUyxHQUFHLElBQUksd0JBQVEsQ0FBQywyQkFBWSxFQUFFO0lBQ2hELE1BQU0sRUFBRSx5QkFBeUI7SUFDakMsSUFBSSxFQUFFLEtBQUs7SUFDWCxvQkFBb0I7SUFDcEIsTUFBTSxFQUFFLE9BQU87SUFDZixTQUFTLEVBQUU7UUFDUCxRQUFRLEVBQUUsSUFBSTtRQUNkLE9BQU8sRUFBRSxJQUFJO0tBQ2hCO0NBQ0osQ0FBQyxDQUFDIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgV3NDbGllbnQgfSBmcm9tICd0c3JwYy1icm93c2VyJztcbmltcG9ydCB7IHNlcnZpY2VQcm90byB9IGZyb20gJy4vc2hhcmVkL3Byb3RvY29scy9zZXJ2aWNlUHJvdG8nO1xuXG5leHBvcnQgY29uc3QgYXBpQ2xpZW50ID0gbmV3IFdzQ2xpZW50KHNlcnZpY2VQcm90bywge1xuICAgIHNlcnZlcjogJ3dzOi8vMTkyLjE2OC4wLjEwNDozMDA0JyxcbiAgICBqc29uOiBmYWxzZSxcbiAgICAvLyBqc29uUHJ1bmU6IGZhbHNlLFxuICAgIGxvZ2dlcjogY29uc29sZSxcbiAgICBoZWFydGJlYXQ6IHtcbiAgICAgICAgaW50ZXJ2YWw6IDMwMDAsXG4gICAgICAgIHRpbWVvdXQ6IDUwMDBcbiAgICB9XG59KTsiXX0=