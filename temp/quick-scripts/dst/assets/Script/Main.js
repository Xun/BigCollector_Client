
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/Main.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e1b90/rohdEk4SdmmEZANaD', 'Main');
// Script/Main.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("./Manager/GameMgr");
var RpcConent_1 = require("./Net/RpcConent");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Main = /** @class */ (function (_super) {
    __extends(Main, _super);
    function Main() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Main.prototype.onLoad = function () {
    };
    Main.prototype.start = function () {
        var _this = this;
        RpcConent_1.apiClient.listenMsg("UpdateBackpack", function (data) {
            _this.upDataChips(data);
        });
        RpcConent_1.apiClient.listenMsg("GameJson", function (data) {
            _this.setGameJsons(data);
        });
    };
    Main.prototype.upDataChips = function (data) {
        GameMgr_1.default.dataModalMgr.BagInfo.list_item = data.backpack;
        GameMgr_1.default.dataModalMgr.BagInfo.total_bag_capacity = data.bagVol;
    };
    Main.prototype.setGameJsons = function (data) {
        for (var key in data.gameJsons.shop) {
            var element = data.gameJsons.shop[key];
            GameMgr_1.default.dataModalMgr.ShopInfoJson.shopInfo.push(element);
        }
        GameMgr_1.default.dataModalMgr.ChipsInfoJson.chipInfo = data.gameJsons.chips;
    };
    Main.prototype.adaptiveNoteLayout = function () {
        var winSize = cc.winSize; //获取当前游戏窗口大小
        cc.log("--当前游戏窗口大小  w:" + winSize.width + "   h:" + winSize.height);
        var viewSize = cc.view.getFrameSize();
        cc.log("--视图边框尺寸：w:" + viewSize.width + "  h:" + viewSize.height);
        var canvasSize = cc.view.getCanvasSize(); //视图中canvas尺寸
        cc.log("--视图中canvas尺寸  w:" + canvasSize.width + "  H:" + canvasSize.height);
        var visibleSize = cc.view.getVisibleSize();
        cc.log("--视图中窗口可见区域的尺寸 w:" + visibleSize.width + "   h:" + visibleSize.height);
        var designSize = cc.view.getDesignResolutionSize();
        cc.log("--设计分辨率：" + designSize.width + "    h: " + designSize.height);
        cc.log("--当前节点的尺寸 w:" + this.node.width + "   h:" + this.node.height);
    };
    Main = __decorate([
        ccclass
    ], Main);
    return Main;
}(cc.Component));
exports.default = Main;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvTWFpbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHQSw2Q0FBd0M7QUFJeEMsNkNBQTRDO0FBSXRDLElBQUEsS0FBd0IsRUFBRSxDQUFDLFVBQVUsRUFBbkMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFrQixDQUFDO0FBSTVDO0lBQWtDLHdCQUFZO0lBQTlDOztJQWlEQSxDQUFDO0lBL0NHLHFCQUFNLEdBQU47SUFFQSxDQUFDO0lBRUQsb0JBQUssR0FBTDtRQUFBLGlCQVNDO1FBUEcscUJBQVMsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsVUFBQyxJQUFJO1lBQ3ZDLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7UUFFSCxxQkFBUyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsVUFBQyxJQUFJO1lBQ2pDLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBR0QsMEJBQVcsR0FBWCxVQUFZLElBQVM7UUFDakIsaUJBQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3ZELGlCQUFPLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ2xFLENBQUM7SUFFRCwyQkFBWSxHQUFaLFVBQWEsSUFBSTtRQUNiLEtBQUssSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUU7WUFDakMsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkMsaUJBQU8sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDNUQ7UUFDRCxpQkFBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCxpQ0FBa0IsR0FBbEI7UUFDSSxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUEsWUFBWTtRQUNyQyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxLQUFLLEdBQUcsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVwRSxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxLQUFLLEdBQUcsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVsRSxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUEsYUFBYTtRQUN0RCxFQUFFLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxLQUFLLEdBQUcsTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUU1RSxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzNDLEVBQUUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsV0FBVyxDQUFDLEtBQUssR0FBRyxPQUFPLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRS9FLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztRQUNuRCxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsS0FBSyxHQUFHLFNBQVMsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFdEUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUUsQ0FBQztJQWhEZ0IsSUFBSTtRQUR4QixPQUFPO09BQ2EsSUFBSSxDQWlEeEI7SUFBRCxXQUFDO0NBakRELEFBaURDLENBakRpQyxFQUFFLENBQUMsU0FBUyxHQWlEN0M7a0JBakRvQixJQUFJIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdHlwZXMgfSBmcm9tIFwicHJvdG9idWZqc1wiO1xuaW1wb3J0IHsgRGF0YU1vZGFsTWdyIH0gZnJvbSBcIi4vTWFuYWdlci9EYXRhTW9kYWxNZ3JcIjtcbmltcG9ydCBGb3JtTWdyIGZyb20gXCIuL01hbmFnZXIvRm9ybU1nclwiO1xuaW1wb3J0IEdhbWVNZ3IgZnJvbSBcIi4vTWFuYWdlci9HYW1lTWdyXCI7XG5pbXBvcnQgVUlNYW5hZ2VyIGZyb20gXCIuL01hbmFnZXIvVUlNYW5hZ2VyXCI7XG5pbXBvcnQgeyBFdmVudENlbnRlciB9IGZyb20gXCIuL05ldC9FdmVudENlbnRlclwiO1xuaW1wb3J0IHsgRXZlbnRUeXBlIH0gZnJvbSBcIi4vTmV0L0V2ZW50VHlwZVwiO1xuaW1wb3J0IHsgYXBpQ2xpZW50IH0gZnJvbSBcIi4vTmV0L1JwY0NvbmVudFwiO1xuaW1wb3J0IFVJQ29uZmlnIGZyb20gXCIuL1VJQ29uZmlnXCI7XG5pbXBvcnQgVUlIb21lIGZyb20gXCIuL1VJU2NyaXB0L0hvbWUvVUlIb21lXCI7XG5cbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XG5cblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1haW4gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgb25Mb2FkKCkge1xuXG4gICAgfVxuXG4gICAgc3RhcnQoKSB7XG5cbiAgICAgICAgYXBpQ2xpZW50Lmxpc3Rlbk1zZyhcIlVwZGF0ZUJhY2twYWNrXCIsIChkYXRhKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnVwRGF0YUNoaXBzKGRhdGEpO1xuICAgICAgICB9KTtcblxuICAgICAgICBhcGlDbGllbnQubGlzdGVuTXNnKFwiR2FtZUpzb25cIiwgKGRhdGEpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0R2FtZUpzb25zKGRhdGEpO1xuICAgICAgICB9KVxuICAgIH1cblxuXG4gICAgdXBEYXRhQ2hpcHMoZGF0YTogYW55KSB7XG4gICAgICAgIEdhbWVNZ3IuZGF0YU1vZGFsTWdyLkJhZ0luZm8ubGlzdF9pdGVtID0gZGF0YS5iYWNrcGFjaztcbiAgICAgICAgR2FtZU1nci5kYXRhTW9kYWxNZ3IuQmFnSW5mby50b3RhbF9iYWdfY2FwYWNpdHkgPSBkYXRhLmJhZ1ZvbDtcbiAgICB9XG5cbiAgICBzZXRHYW1lSnNvbnMoZGF0YSkge1xuICAgICAgICBmb3IgKGxldCBrZXkgaW4gZGF0YS5nYW1lSnNvbnMuc2hvcCkge1xuICAgICAgICAgICAgbGV0IGVsZW1lbnQgPSBkYXRhLmdhbWVKc29ucy5zaG9wW2tleV07XG4gICAgICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5TaG9wSW5mb0pzb24uc2hvcEluZm8ucHVzaChlbGVtZW50KTtcbiAgICAgICAgfVxuICAgICAgICBHYW1lTWdyLmRhdGFNb2RhbE1nci5DaGlwc0luZm9Kc29uLmNoaXBJbmZvID0gZGF0YS5nYW1lSnNvbnMuY2hpcHM7XG4gICAgfVxuXG4gICAgYWRhcHRpdmVOb3RlTGF5b3V0KCkge1xuICAgICAgICBsZXQgd2luU2l6ZSA9IGNjLndpblNpemU7Ly/ojrflj5blvZPliY3muLjmiI/nqpflj6PlpKflsI9cbiAgICAgICAgY2MubG9nKFwiLS3lvZPliY3muLjmiI/nqpflj6PlpKflsI8gIHc6XCIgKyB3aW5TaXplLndpZHRoICsgXCIgICBoOlwiICsgd2luU2l6ZS5oZWlnaHQpO1xuXG4gICAgICAgIGxldCB2aWV3U2l6ZSA9IGNjLnZpZXcuZ2V0RnJhbWVTaXplKCk7XG4gICAgICAgIGNjLmxvZyhcIi0t6KeG5Zu+6L655qGG5bC65a+477yadzpcIiArIHZpZXdTaXplLndpZHRoICsgXCIgIGg6XCIgKyB2aWV3U2l6ZS5oZWlnaHQpO1xuXG4gICAgICAgIGxldCBjYW52YXNTaXplID0gY2Mudmlldy5nZXRDYW52YXNTaXplKCk7Ly/op4blm77kuK1jYW52YXPlsLrlr7hcbiAgICAgICAgY2MubG9nKFwiLS3op4blm77kuK1jYW52YXPlsLrlr7ggIHc6XCIgKyBjYW52YXNTaXplLndpZHRoICsgXCIgIEg6XCIgKyBjYW52YXNTaXplLmhlaWdodCk7XG5cbiAgICAgICAgbGV0IHZpc2libGVTaXplID0gY2Mudmlldy5nZXRWaXNpYmxlU2l6ZSgpO1xuICAgICAgICBjYy5sb2coXCItLeinhuWbvuS4reeql+WPo+WPr+ingeWMuuWfn+eahOWwuuWvuCB3OlwiICsgdmlzaWJsZVNpemUud2lkdGggKyBcIiAgIGg6XCIgKyB2aXNpYmxlU2l6ZS5oZWlnaHQpO1xuXG4gICAgICAgIGxldCBkZXNpZ25TaXplID0gY2Mudmlldy5nZXREZXNpZ25SZXNvbHV0aW9uU2l6ZSgpO1xuICAgICAgICBjYy5sb2coXCItLeiuvuiuoeWIhui+qOeOh++8mlwiICsgZGVzaWduU2l6ZS53aWR0aCArIFwiICAgIGg6IFwiICsgZGVzaWduU2l6ZS5oZWlnaHQpO1xuXG4gICAgICAgIGNjLmxvZyhcIi0t5b2T5YmN6IqC54K555qE5bC65a+4IHc6XCIgKyB0aGlzLm5vZGUud2lkdGggKyBcIiAgIGg6XCIgKyB0aGlzLm5vZGUuaGVpZ2h0KTtcbiAgICB9XG59XG4iXX0=