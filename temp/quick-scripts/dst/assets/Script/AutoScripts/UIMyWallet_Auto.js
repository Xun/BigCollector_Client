
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIMyWallet_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0ba37eeEWlGJrYOCXeBstW3', 'UIMyWallet_Auto');
// Script/AutoScripts/UIMyWallet_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMyWallet_Auto = /** @class */ (function (_super) {
    __extends(UIMyWallet_Auto, _super);
    function UIMyWallet_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.MyMoneyLab = null;
        _this.BtnTips = null;
        _this.BtnGetMoney = null;
        _this.BtnGetMoneyLog = null;
        _this.MoneyToggleContainer = null;
        _this.BtnWeiXin = null;
        _this.WeiXinShiMingStateLab = null;
        _this.BtnZhiFuBao = null;
        _this.ZhiFuBaoShiMingStateLab = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyWallet_Auto.prototype, "MyMoneyLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnTips", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnGetMoney", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnGetMoneyLog", void 0);
    __decorate([
        property(cc.Node)
    ], UIMyWallet_Auto.prototype, "MoneyToggleContainer", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnWeiXin", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyWallet_Auto.prototype, "WeiXinShiMingStateLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyWallet_Auto.prototype, "BtnZhiFuBao", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyWallet_Auto.prototype, "ZhiFuBaoShiMingStateLab", void 0);
    UIMyWallet_Auto = __decorate([
        ccclass
    ], UIMyWallet_Auto);
    return UIMyWallet_Auto;
}(cc.Component));
exports.default = UIMyWallet_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlNeVdhbGxldF9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUE2QyxtQ0FBWTtJQUF6RDtRQUFBLHFFQXNCQztRQXBCQSxjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLGdCQUFVLEdBQWEsSUFBSSxDQUFDO1FBRTVCLGFBQU8sR0FBZSxJQUFJLENBQUM7UUFFM0IsaUJBQVcsR0FBZSxJQUFJLENBQUM7UUFFL0Isb0JBQWMsR0FBZSxJQUFJLENBQUM7UUFFbEMsMEJBQW9CLEdBQVksSUFBSSxDQUFDO1FBRXJDLGVBQVMsR0FBZSxJQUFJLENBQUM7UUFFN0IsMkJBQXFCLEdBQWEsSUFBSSxDQUFDO1FBRXZDLGlCQUFXLEdBQWUsSUFBSSxDQUFDO1FBRS9CLDZCQUF1QixHQUFhLElBQUksQ0FBQzs7SUFFMUMsQ0FBQztJQXBCQTtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3FEQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7dURBQ1M7SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztvREFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3dEQUNVO0lBRS9CO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7MkRBQ2E7SUFFbEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztpRUFDbUI7SUFFckM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztzREFDUTtJQUU3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2tFQUNvQjtJQUV2QztRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3dEQUNVO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7b0VBQ3NCO0lBcEJyQixlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBc0JuQztJQUFELHNCQUFDO0NBdEJELEFBc0JDLENBdEI0QyxFQUFFLENBQUMsU0FBUyxHQXNCeEQ7a0JBdEJvQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCJcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlNeVdhbGxldF9BdXRvIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkNsb3NlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRNeU1vbmV5TGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5UaXBzOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkdldE1vbmV5OiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkdldE1vbmV5TG9nOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdE1vbmV5VG9nZ2xlQ29udGFpbmVyOiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bldlaVhpbjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0V2VpWGluU2hpTWluZ1N0YXRlTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5aaGlGdUJhbzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0WmhpRnVCYW9TaGlNaW5nU3RhdGVMYWI6IGNjLkxhYmVsID0gbnVsbDtcbiBcbn0iXX0=