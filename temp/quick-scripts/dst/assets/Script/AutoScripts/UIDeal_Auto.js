
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIDeal_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fd205FAJMBOjLBN37u8ieyG', 'UIDeal_Auto');
// Script/AutoScripts/UIDeal_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDeal_Auto = /** @class */ (function (_super) {
    __extends(UIDeal_Auto, _super);
    function UIDeal_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.CloseBtn = null;
        _this.BtnMyBuy = null;
        _this.BtnMySell = null;
        _this.BtnSelectType = null;
        _this.DealIcon = null;
        _this.BtnDealDetail = null;
        _this.DealToggleNode = null;
        _this.ContentViewNode = null;
        _this.NodeDealItems = null;
        _this.BtnCloseNodeItems = null;
        _this.BtnBi = null;
        _this.BtnMo = null;
        _this.BtnZhi = null;
        _this.BtnYan = null;
        _this.BtnSGYY = null;
        _this.BtnHLM = null;
        _this.BtnXYJ = null;
        _this.BtnSHZ = null;
        _this.BtnL = null;
        _this.BtnM = null;
        _this.BtnZ = null;
        _this.BtnJ = null;
        _this.BtnZQ = null;
        _this.BtnXW = null;
        _this.BtnBH = null;
        _this.BtnQL = null;
        _this.BtnZG = null;
        _this.BtnLZY = null;
        _this.BtnOYX = null;
        _this.BtnWAS = null;
        _this.BtnSX = null;
        _this.BtnSS = null;
        _this.BtnSZ = null;
        _this.BtnHY = null;
        _this.BtnJC = null;
        _this.BtnPX = null;
        _this.BtnJL = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "CloseBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnMyBuy", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnMySell", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSelectType", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIDeal_Auto.prototype, "DealIcon", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnDealDetail", void 0);
    __decorate([
        property(cc.Node)
    ], UIDeal_Auto.prototype, "DealToggleNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIDeal_Auto.prototype, "ContentViewNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIDeal_Auto.prototype, "NodeDealItems", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnCloseNodeItems", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnBi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnMo", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnZhi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnYan", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSGYY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnHLM", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnXYJ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSHZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnL", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnM", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnJ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnZQ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnXW", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnBH", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnQL", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnZG", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnLZY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnOYX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnWAS", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSS", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnSZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnHY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnJC", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnPX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDeal_Auto.prototype, "BtnJL", void 0);
    UIDeal_Auto = __decorate([
        ccclass
    ], UIDeal_Auto);
    return UIDeal_Auto;
}(cc.Component));
exports.default = UIDeal_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlEZWFsX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQXlDLCtCQUFZO0lBQXJEO1FBQUEscUVBNEVDO1FBMUVBLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixlQUFTLEdBQWUsSUFBSSxDQUFDO1FBRTdCLG1CQUFhLEdBQWUsSUFBSSxDQUFDO1FBRWpDLGNBQVEsR0FBYyxJQUFJLENBQUM7UUFFM0IsbUJBQWEsR0FBZSxJQUFJLENBQUM7UUFFakMsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFFL0IscUJBQWUsR0FBWSxJQUFJLENBQUM7UUFFaEMsbUJBQWEsR0FBWSxJQUFJLENBQUM7UUFFOUIsdUJBQWlCLEdBQWUsSUFBSSxDQUFDO1FBRXJDLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsYUFBTyxHQUFlLElBQUksQ0FBQztRQUUzQixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixVQUFJLEdBQWUsSUFBSSxDQUFDO1FBRXhCLFVBQUksR0FBZSxJQUFJLENBQUM7UUFFeEIsVUFBSSxHQUFlLElBQUksQ0FBQztRQUV4QixVQUFJLEdBQWUsSUFBSSxDQUFDO1FBRXhCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDOztJQUUxQixDQUFDO0lBMUVBO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7aURBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztpREFDTztJQUU1QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNRO0lBRTdCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7c0RBQ1k7SUFFakM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztpREFDTztJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3NEQUNZO0lBRWpDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7dURBQ2E7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt3REFDYztJQUVoQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3NEQUNZO0lBRTlCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7MERBQ2dCO0lBRXJDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7OENBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs4Q0FDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytDQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7K0NBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztnREFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytDQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7K0NBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzsrQ0FDSztJQUUxQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzZDQUNHO0lBRXhCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7NkNBQ0c7SUFFeEI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs2Q0FDRztJQUV4QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzZDQUNHO0lBRXhCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7OENBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs4Q0FDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzhDQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7OENBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs4Q0FDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytDQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7K0NBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzsrQ0FDSztJQUUxQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzhDQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7OENBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs4Q0FDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzhDQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7OENBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs4Q0FDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzhDQUNJO0lBMUVMLFdBQVc7UUFEL0IsT0FBTztPQUNhLFdBQVcsQ0E0RS9CO0lBQUQsa0JBQUM7Q0E1RUQsQUE0RUMsQ0E1RXdDLEVBQUUsQ0FBQyxTQUFTLEdBNEVwRDtrQkE1RW9CLFdBQVciLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSURlYWxfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRDbG9zZUJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5NeUJ1eTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5NeVNlbGw6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU2VsZWN0VHlwZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5TcHJpdGUpXG5cdERlYWxJY29uOiBjYy5TcHJpdGUgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuRGVhbERldGFpbDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Ob2RlKVxuXHREZWFsVG9nZ2xlTm9kZTogY2MuTm9kZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Ob2RlKVxuXHRDb250ZW50Vmlld05vZGU6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTm9kZSlcblx0Tm9kZURlYWxJdGVtczogY2MuTm9kZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5DbG9zZU5vZGVJdGVtczogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5CaTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5NbzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5aaGk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuWWFuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blNHWVk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuSExNOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blhZSjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TSFo6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuTDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5NOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blo6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuSjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5aUTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5YVzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5CSDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5RTDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5aRzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5MWlk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuT1lYOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bldBUzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TWDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TUzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TWjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5IWTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5KQzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5QWDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5KTDogQnV0dG9uUGx1cyA9IG51bGw7XG4gXG59Il19