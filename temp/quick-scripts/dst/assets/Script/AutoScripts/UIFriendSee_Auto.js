
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIFriendSee_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e84e9e0KjZLxacECA/SKApq', 'UIFriendSee_Auto');
// Script/AutoScripts/UIFriendSee_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendSee_Auto = /** @class */ (function (_super) {
    __extends(UIFriendSee_Auto, _super);
    function UIFriendSee_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.AddFriendBtn = null;
        _this.ContentNode = null;
        _this.ContentViewNode = null;
        _this.SeeDetailBtn = null;
        _this.InviteBtn = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendSee_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendSee_Auto.prototype, "AddFriendBtn", void 0);
    __decorate([
        property(cc.Node)
    ], UIFriendSee_Auto.prototype, "ContentNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIFriendSee_Auto.prototype, "ContentViewNode", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendSee_Auto.prototype, "SeeDetailBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendSee_Auto.prototype, "InviteBtn", void 0);
    UIFriendSee_Auto = __decorate([
        ccclass
    ], UIFriendSee_Auto);
    return UIFriendSee_Auto;
}(cc.Component));
exports.default = UIFriendSee_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlGcmllbmRTZWVfQXV0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxnRUFBMEQ7QUFFcEQsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFFMUM7SUFBOEMsb0NBQVk7SUFBMUQ7UUFBQSxxRUFjQztRQVpBLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsa0JBQVksR0FBZSxJQUFJLENBQUM7UUFFaEMsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFFNUIscUJBQWUsR0FBWSxJQUFJLENBQUM7UUFFaEMsa0JBQVksR0FBZSxJQUFJLENBQUM7UUFFaEMsZUFBUyxHQUFlLElBQUksQ0FBQzs7SUFFOUIsQ0FBQztJQVpBO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7c0RBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzswREFDVztJQUVoQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3lEQUNVO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkRBQ2M7SUFFaEM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzswREFDVztJQUVoQztRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3VEQUNRO0lBWlQsZ0JBQWdCO1FBRHBDLE9BQU87T0FDYSxnQkFBZ0IsQ0FjcEM7SUFBRCx1QkFBQztDQWRELEFBY0MsQ0FkNkMsRUFBRSxDQUFDLFNBQVMsR0FjekQ7a0JBZG9CLGdCQUFnQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSBcIi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJRnJpZW5kU2VlX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuQ2xvc2U6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QWRkRnJpZW5kQnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdENvbnRlbnROb2RlOiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdENvbnRlbnRWaWV3Tm9kZTogY2MuTm9kZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRTZWVEZXRhaWxCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0SW52aXRlQnRuOiBCdXR0b25QbHVzID0gbnVsbDtcbiBcbn0iXX0=