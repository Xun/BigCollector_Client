
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIFriendMonerDetail_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'dbcbbCGpTpNgrFvVqCfFAK2', 'UIFriendMonerDetail_Auto');
// Script/AutoScripts/UIFriendMonerDetail_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendMonerDetail_Auto = /** @class */ (function (_super) {
    __extends(UIFriendMonerDetail_Auto, _super);
    function UIFriendMonerDetail_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.TodayGetMoneyLab = null;
        _this.TotalGetMoneyLab = null;
        _this.ContentNode = null;
        _this.ContentViewNode = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendMonerDetail_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriendMonerDetail_Auto.prototype, "TodayGetMoneyLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriendMonerDetail_Auto.prototype, "TotalGetMoneyLab", void 0);
    __decorate([
        property(cc.Node)
    ], UIFriendMonerDetail_Auto.prototype, "ContentNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIFriendMonerDetail_Auto.prototype, "ContentViewNode", void 0);
    UIFriendMonerDetail_Auto = __decorate([
        ccclass
    ], UIFriendMonerDetail_Auto);
    return UIFriendMonerDetail_Auto;
}(cc.Component));
exports.default = UIFriendMonerDetail_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlGcmllbmRNb25lckRldGFpbF9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUFzRCw0Q0FBWTtJQUFsRTtRQUFBLHFFQVlDO1FBVkEsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixzQkFBZ0IsR0FBYSxJQUFJLENBQUM7UUFFbEMsc0JBQWdCLEdBQWEsSUFBSSxDQUFDO1FBRWxDLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRTVCLHFCQUFlLEdBQVksSUFBSSxDQUFDOztJQUVqQyxDQUFDO0lBVkE7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs4REFDTztJQUU1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3NFQUNlO0lBRWxDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7c0VBQ2U7SUFFbEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztpRUFDVTtJQUU1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3FFQUNjO0lBVlosd0JBQXdCO1FBRDVDLE9BQU87T0FDYSx3QkFBd0IsQ0FZNUM7SUFBRCwrQkFBQztDQVpELEFBWUMsQ0FacUQsRUFBRSxDQUFDLFNBQVMsR0FZakU7a0JBWm9CLHdCQUF3QiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSBcIi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJRnJpZW5kTW9uZXJEZXRhaWxfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5DbG9zZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0VG9kYXlHZXRNb25leUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdFRvdGFsR2V0TW9uZXlMYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdENvbnRlbnROb2RlOiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdENvbnRlbnRWaWV3Tm9kZTogY2MuTm9kZSA9IG51bGw7XG4gXG59Il19