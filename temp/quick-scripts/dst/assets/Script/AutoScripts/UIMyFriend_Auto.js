
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIMyFriend_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4ef20lq6GRFR5BOhBXfRnUE', 'UIMyFriend_Auto');
// Script/AutoScripts/UIMyFriend_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMyFriend_Auto = /** @class */ (function (_super) {
    __extends(UIMyFriend_Auto, _super);
    function UIMyFriend_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.RankToggleNode = null;
        _this.CoinContentNode = null;
        _this.CoinContentViewNode = null;
        _this.CloseBtn = null;
        return _this;
    }
    __decorate([
        property(cc.Node)
    ], UIMyFriend_Auto.prototype, "RankToggleNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIMyFriend_Auto.prototype, "CoinContentNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIMyFriend_Auto.prototype, "CoinContentViewNode", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyFriend_Auto.prototype, "CloseBtn", void 0);
    UIMyFriend_Auto = __decorate([
        ccclass
    ], UIMyFriend_Auto);
    return UIMyFriend_Auto;
}(cc.Component));
exports.default = UIMyFriend_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlNeUZyaWVuZF9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUE2QyxtQ0FBWTtJQUF6RDtRQUFBLHFFQVVDO1FBUkEsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFFL0IscUJBQWUsR0FBWSxJQUFJLENBQUM7UUFFaEMseUJBQW1CLEdBQVksSUFBSSxDQUFDO1FBRXBDLGNBQVEsR0FBZSxJQUFJLENBQUM7O0lBRTdCLENBQUM7SUFSQTtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzJEQUNhO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NERBQ2M7SUFFaEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnRUFDa0I7SUFFcEM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztxREFDTztJQVJSLGVBQWU7UUFEbkMsT0FBTztPQUNhLGVBQWUsQ0FVbkM7SUFBRCxzQkFBQztDQVZELEFBVUMsQ0FWNEMsRUFBRSxDQUFDLFNBQVMsR0FVeEQ7a0JBVm9CLGVBQWUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSU15RnJpZW5kX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoY2MuTm9kZSlcblx0UmFua1RvZ2dsZU5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTm9kZSlcblx0Q29pbkNvbnRlbnROb2RlOiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdENvaW5Db250ZW50Vmlld05vZGU6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0Q2xvc2VCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuIFxufSJdfQ==