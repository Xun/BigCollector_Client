
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIColleCtionConversion_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6d5d4lMWydEZZCbxR6GZL2Y', 'UIColleCtionConversion_Auto');
// Script/AutoScripts/UIColleCtionConversion_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIColleCtionConversion_Auto = /** @class */ (function (_super) {
    __extends(UIColleCtionConversion_Auto, _super);
    function UIColleCtionConversion_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Msg = null;
        _this.MoneyNameLab = null;
        _this.BtnSure = null;
        _this.BtnClose = null;
        _this.MoneyIconSp = null;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], UIColleCtionConversion_Auto.prototype, "Msg", void 0);
    __decorate([
        property(cc.Label)
    ], UIColleCtionConversion_Auto.prototype, "MoneyNameLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIColleCtionConversion_Auto.prototype, "BtnSure", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIColleCtionConversion_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIColleCtionConversion_Auto.prototype, "MoneyIconSp", void 0);
    UIColleCtionConversion_Auto = __decorate([
        ccclass
    ], UIColleCtionConversion_Auto);
    return UIColleCtionConversion_Auto;
}(cc.Component));
exports.default = UIColleCtionConversion_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlDb2xsZUN0aW9uQ29udmVyc2lvbl9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXdCLEVBQUUsQ0FBQyxVQUFVLEVBQW5DLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBa0IsQ0FBQztBQUU1QztJQUF5RCwrQ0FBWTtJQUFyRTtRQUFBLHFFQVlDO1FBVkEsU0FBRyxHQUFhLElBQUksQ0FBQztRQUVyQixrQkFBWSxHQUFhLElBQUksQ0FBQztRQUU5QixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsaUJBQVcsR0FBYyxJQUFJLENBQUM7O0lBRS9CLENBQUM7SUFWQTtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzREQUNFO0lBRXJCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7cUVBQ1c7SUFFOUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztnRUFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2lFQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7b0VBQ1U7SUFWViwyQkFBMkI7UUFEL0MsT0FBTztPQUNhLDJCQUEyQixDQVkvQztJQUFELGtDQUFDO0NBWkQsQUFZQyxDQVp3RCxFQUFFLENBQUMsU0FBUyxHQVlwRTtrQkFab0IsMkJBQTJCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCJcblxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSUNvbGxlQ3Rpb25Db252ZXJzaW9uX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE1zZzogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE1vbmV5TmFtZUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU3VyZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5DbG9zZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5TcHJpdGUpXG5cdE1vbmV5SWNvblNwOiBjYy5TcHJpdGUgPSBudWxsO1xuXG59Il19