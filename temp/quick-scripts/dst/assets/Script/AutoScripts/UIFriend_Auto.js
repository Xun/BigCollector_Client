
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIFriend_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '1e1abFX9YpOM4gB7QJl3WyR', 'UIFriend_Auto');
// Script/AutoScripts/UIFriend_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriend_Auto = /** @class */ (function (_super) {
    __extends(UIFriend_Auto, _super);
    function UIFriend_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.BtnInvoedFriend = null;
        _this.BtnMyFriend = null;
        _this.MyFriendNumLab = null;
        _this.ProbablyMoneyLab = null;
        _this.ProbablyDetail = null;
        _this.TodayGetMoneyLab = null;
        _this.TotalGetMoneyLab = null;
        _this.UpChannel = null;
        _this.ExtarTip = null;
        _this.TodayExtarGetMoneyLab = null;
        _this.TodayExtarGetMoneyTipsLab = null;
        _this.Call = null;
        _this.Name = null;
        _this.HeadSp = null;
        _this.TodayAddYushi = null;
        _this.TotalYushi = null;
        _this.BtnGive = null;
        _this.BtnVisit = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnInvoedFriend", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnMyFriend", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "MyFriendNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "ProbablyMoneyLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "ProbablyDetail", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TodayGetMoneyLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TotalGetMoneyLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "UpChannel", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "ExtarTip", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TodayExtarGetMoneyLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TodayExtarGetMoneyTipsLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "Call", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "Name", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIFriend_Auto.prototype, "HeadSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TodayAddYushi", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriend_Auto.prototype, "TotalYushi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnGive", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriend_Auto.prototype, "BtnVisit", void 0);
    UIFriend_Auto = __decorate([
        ccclass
    ], UIFriend_Auto);
    return UIFriend_Auto;
}(cc.Component));
exports.default = UIFriend_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlGcmllbmRfQXV0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxnRUFBMEQ7QUFFcEQsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFFMUM7SUFBMkMsaUNBQVk7SUFBdkQ7UUFBQSxxRUF3Q0M7UUF0Q0EsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixxQkFBZSxHQUFlLElBQUksQ0FBQztRQUVuQyxpQkFBVyxHQUFlLElBQUksQ0FBQztRQUUvQixvQkFBYyxHQUFhLElBQUksQ0FBQztRQUVoQyxzQkFBZ0IsR0FBYSxJQUFJLENBQUM7UUFFbEMsb0JBQWMsR0FBZSxJQUFJLENBQUM7UUFFbEMsc0JBQWdCLEdBQWEsSUFBSSxDQUFDO1FBRWxDLHNCQUFnQixHQUFhLElBQUksQ0FBQztRQUVsQyxlQUFTLEdBQWUsSUFBSSxDQUFDO1FBRTdCLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsMkJBQXFCLEdBQWEsSUFBSSxDQUFDO1FBRXZDLCtCQUF5QixHQUFhLElBQUksQ0FBQztRQUUzQyxVQUFJLEdBQWUsSUFBSSxDQUFDO1FBRXhCLFVBQUksR0FBYSxJQUFJLENBQUM7UUFFdEIsWUFBTSxHQUFjLElBQUksQ0FBQztRQUV6QixtQkFBYSxHQUFhLElBQUksQ0FBQztRQUUvQixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUU1QixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLGNBQVEsR0FBZSxJQUFJLENBQUM7O0lBRTdCLENBQUM7SUF0Q0E7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDTztJQUU1QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzBEQUNjO0lBRW5DO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7c0RBQ1U7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzt5REFDYTtJQUVoQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzJEQUNlO0lBRWxDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7eURBQ2E7SUFFbEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzsyREFDZTtJQUVsQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzJEQUNlO0lBRWxDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7b0RBQ1E7SUFFN0I7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDTztJQUU1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dFQUNvQjtJQUV2QztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO29FQUN3QjtJQUUzQztRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytDQUNHO0lBRXhCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7K0NBQ0c7SUFFdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztpREFDSztJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3dEQUNZO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7cURBQ1M7SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO21EQUNPO0lBdENSLGFBQWE7UUFEakMsT0FBTztPQUNhLGFBQWEsQ0F3Q2pDO0lBQUQsb0JBQUM7Q0F4Q0QsQUF3Q0MsQ0F4QzBDLEVBQUUsQ0FBQyxTQUFTLEdBd0N0RDtrQkF4Q29CLGFBQWEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSUZyaWVuZF9BdXRvIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkNsb3NlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkludm9lZEZyaWVuZDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5NeUZyaWVuZDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TXlGcmllbmROdW1MYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRQcm9iYWJseU1vbmV5TGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRQcm9iYWJseURldGFpbDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0VG9kYXlHZXRNb25leUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdFRvdGFsR2V0TW9uZXlMYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdFVwQ2hhbm5lbDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRFeHRhclRpcDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0VG9kYXlFeHRhckdldE1vbmV5TGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0VG9kYXlFeHRhckdldE1vbmV5VGlwc0xhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0Q2FsbDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TmFtZTogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuU3ByaXRlKVxuXHRIZWFkU3A6IGNjLlNwcml0ZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0VG9kYXlBZGRZdXNoaTogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdFRvdGFsWXVzaGk6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkdpdmU6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuVmlzaXQ6IEJ1dHRvblBsdXMgPSBudWxsO1xuIFxufSJdfQ==