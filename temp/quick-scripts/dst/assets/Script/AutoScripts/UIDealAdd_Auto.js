
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIDealAdd_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a20f3OLQhVAnZuAdLNuoa68', 'UIDealAdd_Auto');
// Script/AutoScripts/UIDealAdd_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealAdd_Auto = /** @class */ (function (_super) {
    __extends(UIDealAdd_Auto, _super);
    function UIDealAdd_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.CloseBtn = null;
        _this.BtnSelectType = null;
        _this.IconType = null;
        _this.IconName = null;
        _this.LabBtn = null;
        _this.BuyAdd = null;
        _this.EditePrice = null;
        _this.EditeNum = null;
        _this.LabRemain = null;
        _this.LabPay = null;
        _this.NodeDealItems = null;
        _this.BtnCloseNodeItems = null;
        _this.BtnBi = null;
        _this.BtnMo = null;
        _this.BtnZhi = null;
        _this.BtnYan = null;
        _this.BtnSGYY = null;
        _this.BtnHLM = null;
        _this.BtnXYJ = null;
        _this.BtnSHZ = null;
        _this.BtnL = null;
        _this.BtnM = null;
        _this.BtnZ = null;
        _this.BtnJ = null;
        _this.BtnZQ = null;
        _this.BtnXW = null;
        _this.BtnBH = null;
        _this.BtnQL = null;
        _this.BtnZG = null;
        _this.BtnLZY = null;
        _this.BtnOYX = null;
        _this.BtnWAS = null;
        _this.BtnSX = null;
        _this.BtnSS = null;
        _this.BtnSZ = null;
        _this.BtnHY = null;
        _this.BtnJC = null;
        _this.BtnPX = null;
        _this.BtnJL = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "CloseBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnSelectType", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIDealAdd_Auto.prototype, "IconType", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealAdd_Auto.prototype, "IconName", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealAdd_Auto.prototype, "LabBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BuyAdd", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIDealAdd_Auto.prototype, "EditePrice", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIDealAdd_Auto.prototype, "EditeNum", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealAdd_Auto.prototype, "LabRemain", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealAdd_Auto.prototype, "LabPay", void 0);
    __decorate([
        property(cc.Node)
    ], UIDealAdd_Auto.prototype, "NodeDealItems", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnCloseNodeItems", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnBi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnMo", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnZhi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnYan", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnSGYY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnHLM", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnXYJ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnSHZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnL", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnM", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnJ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnZQ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnXW", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnBH", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnQL", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnZG", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnLZY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnOYX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnWAS", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnSX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnSS", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnSZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnHY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnJC", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnPX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealAdd_Auto.prototype, "BtnJL", void 0);
    UIDealAdd_Auto = __decorate([
        ccclass
    ], UIDealAdd_Auto);
    return UIDealAdd_Auto;
}(cc.Component));
exports.default = UIDealAdd_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlEZWFsQWRkX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQTRDLGtDQUFZO0lBQXhEO1FBQUEscUVBZ0ZDO1FBOUVBLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsbUJBQWEsR0FBZSxJQUFJLENBQUM7UUFFakMsY0FBUSxHQUFjLElBQUksQ0FBQztRQUUzQixjQUFRLEdBQWEsSUFBSSxDQUFDO1FBRTFCLFlBQU0sR0FBYSxJQUFJLENBQUM7UUFFeEIsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixnQkFBVSxHQUFlLElBQUksQ0FBQztRQUU5QixjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFFM0IsWUFBTSxHQUFhLElBQUksQ0FBQztRQUV4QixtQkFBYSxHQUFZLElBQUksQ0FBQztRQUU5Qix1QkFBaUIsR0FBZSxJQUFJLENBQUM7UUFFckMsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLFVBQUksR0FBZSxJQUFJLENBQUM7UUFFeEIsVUFBSSxHQUFlLElBQUksQ0FBQztRQUV4QixVQUFJLEdBQWUsSUFBSSxDQUFDO1FBRXhCLFVBQUksR0FBZSxJQUFJLENBQUM7UUFFeEIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7O0lBRTFCLENBQUM7SUE5RUE7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztvREFDTztJQUU1QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3lEQUNZO0lBRWpDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7b0RBQ087SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztvREFDTztJQUUxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2tEQUNLO0lBRXhCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztzREFDUztJQUU5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO29EQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7cURBQ1E7SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztrREFDSztJQUV4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3lEQUNZO0lBRTlCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7NkRBQ2dCO0lBRXJDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7aURBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztpREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDSztJQUUxQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2dEQUNHO0lBRXhCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7Z0RBQ0c7SUFFeEI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztnREFDRztJQUV4QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2dEQUNHO0lBRXhCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7aURBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztpREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2lEQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7aURBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztpREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDSztJQUUxQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2lEQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7aURBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztpREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2lEQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7aURBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztpREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2lEQUNJO0lBOUVMLGNBQWM7UUFEbEMsT0FBTztPQUNhLGNBQWMsQ0FnRmxDO0lBQUQscUJBQUM7Q0FoRkQsQUFnRkMsQ0FoRjJDLEVBQUUsQ0FBQyxTQUFTLEdBZ0Z2RDtrQkFoRm9CLGNBQWMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSURlYWxBZGRfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRDbG9zZUJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TZWxlY3RUeXBlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0SWNvblR5cGU6IGNjLlNwcml0ZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0SWNvbk5hbWU6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRMYWJCdG46IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ1eUFkZDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5FZGl0Qm94KVxuXHRFZGl0ZVByaWNlOiBjYy5FZGl0Qm94ID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkVkaXRCb3gpXG5cdEVkaXRlTnVtOiBjYy5FZGl0Qm94ID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRMYWJSZW1haW46IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRMYWJQYXk6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdE5vZGVEZWFsSXRlbXM6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuQ2xvc2VOb2RlSXRlbXM6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuQmk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuTW86IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuWmhpOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bllhbjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TR1lZOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkhMTTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5YWUo6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU0haOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkw6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuTTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5aOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bko6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuWlE6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuWFc6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuQkg6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuUUw6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuWkc6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuTFpZOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bk9ZWDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5XQVM6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU1g6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU1M6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU1o6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuSFk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuSkM6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuUFg6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuSkw6IEJ1dHRvblBsdXMgPSBudWxsO1xuIFxufSJdfQ==