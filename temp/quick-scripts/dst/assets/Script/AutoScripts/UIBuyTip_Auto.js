
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIBuyTip_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5a9d6iPD+BFKJM7cZ885TPW', 'UIBuyTip_Auto');
// Script/AutoScripts/UIBuyTip_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIBuyTip_Auto = /** @class */ (function (_super) {
    __extends(UIBuyTip_Auto, _super);
    function UIBuyTip_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BuyTipLab = null;
        _this.BuyMoneyTypeSp = null;
        _this.BuyName = null;
        _this.BtnSure = null;
        _this.BuyIconTypeSp = null;
        _this.BtnClose = null;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], UIBuyTip_Auto.prototype, "BuyTipLab", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIBuyTip_Auto.prototype, "BuyMoneyTypeSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIBuyTip_Auto.prototype, "BuyName", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIBuyTip_Auto.prototype, "BtnSure", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIBuyTip_Auto.prototype, "BuyIconTypeSp", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIBuyTip_Auto.prototype, "BtnClose", void 0);
    UIBuyTip_Auto = __decorate([
        ccclass
    ], UIBuyTip_Auto);
    return UIBuyTip_Auto;
}(cc.Component));
exports.default = UIBuyTip_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlCdXlUaXBfQXV0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxnRUFBMEQ7QUFFcEQsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFFMUM7SUFBMkMsaUNBQVk7SUFBdkQ7UUFBQSxxRUFjQztRQVpBLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFFM0Isb0JBQWMsR0FBYyxJQUFJLENBQUM7UUFFakMsYUFBTyxHQUFhLElBQUksQ0FBQztRQUV6QixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLG1CQUFhLEdBQWMsSUFBSSxDQUFDO1FBRWhDLGNBQVEsR0FBZSxJQUFJLENBQUM7O0lBRTdCLENBQUM7SUFaQTtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO29EQUNRO0lBRTNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7eURBQ2E7SUFFakM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztrREFDTTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNNO0lBRTNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7d0RBQ1k7SUFFaEM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDTztJQVpSLGFBQWE7UUFEakMsT0FBTztPQUNhLGFBQWEsQ0FjakM7SUFBRCxvQkFBQztDQWRELEFBY0MsQ0FkMEMsRUFBRSxDQUFDLFNBQVMsR0FjdEQ7a0JBZG9CLGFBQWEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSUJ1eVRpcF9BdXRvIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRCdXlUaXBMYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0QnV5TW9uZXlUeXBlU3A6IGNjLlNwcml0ZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0QnV5TmFtZTogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU3VyZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5TcHJpdGUpXG5cdEJ1eUljb25UeXBlU3A6IGNjLlNwcml0ZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5DbG9zZTogQnV0dG9uUGx1cyA9IG51bGw7XG4gXG59Il19