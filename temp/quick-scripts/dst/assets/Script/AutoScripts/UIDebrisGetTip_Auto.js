
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIDebrisGetTip_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ffa8fnRY3BCJ6vJIic+/8Mf', 'UIDebrisGetTip_Auto');
// Script/AutoScripts/UIDebrisGetTip_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDebrisGetTip_Auto = /** @class */ (function (_super) {
    __extends(UIDebrisGetTip_Auto, _super);
    function UIDebrisGetTip_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.DebrisLab = null;
        _this.DebrisIconSp = null;
        _this.BtnClose = null;
        _this.BtnGoToBag = null;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], UIDebrisGetTip_Auto.prototype, "DebrisLab", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIDebrisGetTip_Auto.prototype, "DebrisIconSp", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDebrisGetTip_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDebrisGetTip_Auto.prototype, "BtnGoToBag", void 0);
    UIDebrisGetTip_Auto = __decorate([
        ccclass
    ], UIDebrisGetTip_Auto);
    return UIDebrisGetTip_Auto;
}(cc.Component));
exports.default = UIDebrisGetTip_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlEZWJyaXNHZXRUaXBfQXV0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxnRUFBMEQ7QUFFcEQsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFFMUM7SUFBaUQsdUNBQVk7SUFBN0Q7UUFBQSxxRUFVQztRQVJBLGVBQVMsR0FBYSxJQUFJLENBQUM7UUFFM0Isa0JBQVksR0FBYyxJQUFJLENBQUM7UUFFL0IsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixnQkFBVSxHQUFlLElBQUksQ0FBQzs7SUFFL0IsQ0FBQztJQVJBO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7MERBQ1E7SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs2REFDVztJQUUvQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3lEQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7MkRBQ1M7SUFSVixtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQVV2QztJQUFELDBCQUFDO0NBVkQsQUFVQyxDQVZnRCxFQUFFLENBQUMsU0FBUyxHQVU1RDtrQkFWb0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCJcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlEZWJyaXNHZXRUaXBfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0RGVicmlzTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5TcHJpdGUpXG5cdERlYnJpc0ljb25TcDogY2MuU3ByaXRlID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkNsb3NlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkdvVG9CYWc6IEJ1dHRvblBsdXMgPSBudWxsO1xuIFxufSJdfQ==