
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIDealSell_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ccdbaN5iPhHEqZO7gCAG3mq', 'UIDealSell_Auto');
// Script/AutoScripts/UIDealSell_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealSell_Auto = /** @class */ (function (_super) {
    __extends(UIDealSell_Auto, _super);
    function UIDealSell_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.CloseBtn = null;
        _this.BtnSelectType = null;
        _this.IconType = null;
        _this.IconName = null;
        _this.LabBtn = null;
        _this.BuySell = null;
        _this.EditePrice = null;
        _this.EditeNum = null;
        _this.LabRemain = null;
        _this.NodeDealItems = null;
        _this.BtnCloseNodeItems = null;
        _this.BtnBi = null;
        _this.BtnMo = null;
        _this.BtnZhi = null;
        _this.BtnYan = null;
        _this.BtnSGYY = null;
        _this.BtnHLM = null;
        _this.BtnXYJ = null;
        _this.BtnSHZ = null;
        _this.BtnL = null;
        _this.BtnM = null;
        _this.BtnZ = null;
        _this.BtnJ = null;
        _this.BtnZQ = null;
        _this.BtnXW = null;
        _this.BtnBH = null;
        _this.BtnQL = null;
        _this.BtnZG = null;
        _this.BtnLZY = null;
        _this.BtnOYX = null;
        _this.BtnWAS = null;
        _this.BtnSX = null;
        _this.BtnSS = null;
        _this.BtnSZ = null;
        _this.BtnHY = null;
        _this.BtnJC = null;
        _this.BtnPX = null;
        _this.BtnJL = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "CloseBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnSelectType", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIDealSell_Auto.prototype, "IconType", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealSell_Auto.prototype, "IconName", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealSell_Auto.prototype, "LabBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BuySell", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIDealSell_Auto.prototype, "EditePrice", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIDealSell_Auto.prototype, "EditeNum", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealSell_Auto.prototype, "LabRemain", void 0);
    __decorate([
        property(cc.Node)
    ], UIDealSell_Auto.prototype, "NodeDealItems", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnCloseNodeItems", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnBi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnMo", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnZhi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnYan", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnSGYY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnHLM", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnXYJ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnSHZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnL", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnM", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnJ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnZQ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnXW", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnBH", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnQL", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnZG", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnLZY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnOYX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnWAS", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnSX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnSS", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnSZ", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnHY", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnJC", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnPX", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealSell_Auto.prototype, "BtnJL", void 0);
    UIDealSell_Auto = __decorate([
        ccclass
    ], UIDealSell_Auto);
    return UIDealSell_Auto;
}(cc.Component));
exports.default = UIDealSell_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlEZWFsU2VsbF9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUE2QyxtQ0FBWTtJQUF6RDtRQUFBLHFFQThFQztRQTVFQSxjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLG1CQUFhLEdBQWUsSUFBSSxDQUFDO1FBRWpDLGNBQVEsR0FBYyxJQUFJLENBQUM7UUFFM0IsY0FBUSxHQUFhLElBQUksQ0FBQztRQUUxQixZQUFNLEdBQWEsSUFBSSxDQUFDO1FBRXhCLGFBQU8sR0FBZSxJQUFJLENBQUM7UUFFM0IsZ0JBQVUsR0FBZSxJQUFJLENBQUM7UUFFOUIsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixlQUFTLEdBQWEsSUFBSSxDQUFDO1FBRTNCLG1CQUFhLEdBQVksSUFBSSxDQUFDO1FBRTlCLHVCQUFpQixHQUFlLElBQUksQ0FBQztRQUVyQyxXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLGFBQU8sR0FBZSxJQUFJLENBQUM7UUFFM0IsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsVUFBSSxHQUFlLElBQUksQ0FBQztRQUV4QixVQUFJLEdBQWUsSUFBSSxDQUFDO1FBRXhCLFVBQUksR0FBZSxJQUFJLENBQUM7UUFFeEIsVUFBSSxHQUFlLElBQUksQ0FBQztRQUV4QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsWUFBTSxHQUFlLElBQUksQ0FBQztRQUUxQixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixXQUFLLEdBQWUsSUFBSSxDQUFDO1FBRXpCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFlLElBQUksQ0FBQzs7SUFFMUIsQ0FBQztJQTVFQTtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3FEQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7MERBQ1k7SUFFakM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztxREFDTztJQUUzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3FEQUNPO0lBRTFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7bURBQ0s7SUFFeEI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztvREFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3VEQUNTO0lBRTlCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7cURBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztzREFDUTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzBEQUNZO0lBRTlCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7OERBQ2dCO0lBRXJDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO21EQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7bURBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztvREFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO21EQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7bURBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDSztJQUUxQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2lEQUNHO0lBRXhCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7aURBQ0c7SUFFeEI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztpREFDRztJQUV4QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2lEQUNHO0lBRXhCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO21EQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7bURBQ0s7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDSztJQUUxQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNJO0lBNUVMLGVBQWU7UUFEbkMsT0FBTztPQUNhLGVBQWUsQ0E4RW5DO0lBQUQsc0JBQUM7Q0E5RUQsQUE4RUMsQ0E5RTRDLEVBQUUsQ0FBQyxTQUFTLEdBOEV4RDtrQkE5RW9CLGVBQWUiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSURlYWxTZWxsX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0Q2xvc2VCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU2VsZWN0VHlwZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5TcHJpdGUpXG5cdEljb25UeXBlOiBjYy5TcHJpdGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdEljb25OYW1lOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TGFiQnRuOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdXlTZWxsOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkVkaXRCb3gpXG5cdEVkaXRlUHJpY2U6IGNjLkVkaXRCb3ggPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuRWRpdEJveClcblx0RWRpdGVOdW06IGNjLkVkaXRCb3ggPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdExhYlJlbWFpbjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTm9kZSlcblx0Tm9kZURlYWxJdGVtczogY2MuTm9kZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5DbG9zZU5vZGVJdGVtczogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5CaTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5NbzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5aaGk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuWWFuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blNHWVk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuSExNOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blhZSjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TSFo6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuTDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5NOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blo6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuSjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5aUTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5YVzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5CSDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5RTDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5aRzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5MWlk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuT1lYOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bldBUzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TWDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TUzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TWjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5IWTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5KQzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5QWDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5KTDogQnV0dG9uUGx1cyA9IG51bGw7XG4gXG59Il19