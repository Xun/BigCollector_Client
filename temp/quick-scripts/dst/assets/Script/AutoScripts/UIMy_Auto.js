
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIMy_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ac0411e/YpCgrETAlXWXJNy', 'UIMy_Auto');
// Script/AutoScripts/UIMy_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMy_Auto = /** @class */ (function (_super) {
    __extends(UIMy_Auto, _super);
    function UIMy_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.HeadSp = null;
        _this.NameLab = null;
        _this.IDLab = null;
        _this.BtnClose = null;
        _this.MoneyNode = null;
        _this.MyMoneyLab = null;
        _this.BtnGetMoney = null;
        _this.BtnFriend = null;
        _this.BtnVip = null;
        _this.BtnShare = null;
        _this.BtnPlay = null;
        _this.BtnSetting = null;
        return _this;
    }
    __decorate([
        property(cc.Sprite)
    ], UIMy_Auto.prototype, "HeadSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIMy_Auto.prototype, "NameLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMy_Auto.prototype, "IDLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Node)
    ], UIMy_Auto.prototype, "MoneyNode", void 0);
    __decorate([
        property(cc.Label)
    ], UIMy_Auto.prototype, "MyMoneyLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnGetMoney", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnFriend", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnVip", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnShare", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnPlay", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMy_Auto.prototype, "BtnSetting", void 0);
    UIMy_Auto = __decorate([
        ccclass
    ], UIMy_Auto);
    return UIMy_Auto;
}(cc.Component));
exports.default = UIMy_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlNeV9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUF1Qyw2QkFBWTtJQUFuRDtRQUFBLHFFQTBCQztRQXhCQSxZQUFNLEdBQWMsSUFBSSxDQUFDO1FBRXpCLGFBQU8sR0FBYSxJQUFJLENBQUM7UUFFekIsV0FBSyxHQUFjLElBQUksQ0FBQztRQUV4QixjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFFMUIsZ0JBQVUsR0FBYSxJQUFJLENBQUM7UUFFNUIsaUJBQVcsR0FBZSxJQUFJLENBQUM7UUFFL0IsZUFBUyxHQUFlLElBQUksQ0FBQztRQUU3QixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsYUFBTyxHQUFlLElBQUksQ0FBQztRQUUzQixnQkFBVSxHQUFlLElBQUksQ0FBQzs7SUFFL0IsQ0FBQztJQXhCQTtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzZDQUNLO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7OENBQ007SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs0Q0FDSztJQUV4QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytDQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ1E7SUFFMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztpREFDUztJQUU1QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNVO0lBRS9CO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7Z0RBQ1E7SUFFN0I7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs2Q0FDSztJQUUxQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytDQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7OENBQ007SUFFM0I7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztpREFDUztJQXhCVixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBMEI3QjtJQUFELGdCQUFDO0NBMUJELEFBMEJDLENBMUJzQyxFQUFFLENBQUMsU0FBUyxHQTBCbEQ7a0JBMUJvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCJcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlNeV9BdXRvIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0SGVhZFNwOiBjYy5TcHJpdGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE5hbWVMYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRJRExhYiA6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkNsb3NlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdE1vbmV5Tm9kZTogY2MuTm9kZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TXlNb25leUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuR2V0TW9uZXk6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuRnJpZW5kOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blZpcDogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TaGFyZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5QbGF5OiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blNldHRpbmc6IEJ1dHRvblBsdXMgPSBudWxsO1xuIFxufSJdfQ==