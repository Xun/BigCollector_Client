
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIFriendGive_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '77825T9BPFOe7LJYPu+lNrY', 'UIFriendGive_Auto');
// Script/AutoScripts/UIFriendGive_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendGive_Auto = /** @class */ (function (_super) {
    __extends(UIFriendGive_Auto, _super);
    function UIFriendGive_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.BtnSerch = null;
        _this.BtnGive = null;
        _this.IDBox = null;
        _this.GiveBox = null;
        _this.HeadSP = null;
        _this.NameLab = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendGive_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendGive_Auto.prototype, "BtnSerch", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendGive_Auto.prototype, "BtnGive", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIFriendGive_Auto.prototype, "IDBox", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIFriendGive_Auto.prototype, "GiveBox", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIFriendGive_Auto.prototype, "HeadSP", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriendGive_Auto.prototype, "NameLab", void 0);
    UIFriendGive_Auto = __decorate([
        ccclass
    ], UIFriendGive_Auto);
    return UIFriendGive_Auto;
}(cc.Component));
exports.default = UIFriendGive_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlGcmllbmRHaXZlX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQStDLHFDQUFZO0lBQTNEO1FBQUEscUVBZ0JDO1FBZEEsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLGFBQU8sR0FBZSxJQUFJLENBQUM7UUFFM0IsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLFlBQU0sR0FBYyxJQUFJLENBQUM7UUFFekIsYUFBTyxHQUFhLElBQUksQ0FBQzs7SUFFMUIsQ0FBQztJQWRBO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7dURBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzt1REFDTztJQUU1QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3NEQUNNO0lBRTNCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0RBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztzREFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3FEQUNLO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7c0RBQ007SUFkTCxpQkFBaUI7UUFEckMsT0FBTztPQUNhLGlCQUFpQixDQWdCckM7SUFBRCx3QkFBQztDQWhCRCxBQWdCQyxDQWhCOEMsRUFBRSxDQUFDLFNBQVMsR0FnQjFEO2tCQWhCb0IsaUJBQWlCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCJcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlGcmllbmRHaXZlX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuQ2xvc2U6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU2VyY2g6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuR2l2ZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5FZGl0Qm94KVxuXHRJREJveDogY2MuRWRpdEJveCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5FZGl0Qm94KVxuXHRHaXZlQm94OiBjYy5FZGl0Qm94ID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0SGVhZFNQOiBjYy5TcHJpdGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE5hbWVMYWI6IGNjLkxhYmVsID0gbnVsbDtcbiBcbn0iXX0=