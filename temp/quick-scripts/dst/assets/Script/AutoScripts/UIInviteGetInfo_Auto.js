
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIInviteGetInfo_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ce2a9E9K3BMpICnFv+csbqK', 'UIInviteGetInfo_Auto');
// Script/AutoScripts/UIInviteGetInfo_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIInviteGetInfo_Auto = /** @class */ (function (_super) {
    __extends(UIInviteGetInfo_Auto, _super);
    function UIInviteGetInfo_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.SetInfoBtn = null;
        _this.CopyWechatBtn = null;
        _this.CopyQQBtn = null;
        _this.CloseBtn = null;
        _this.HeadSP = null;
        _this.NameLab = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIInviteGetInfo_Auto.prototype, "SetInfoBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIInviteGetInfo_Auto.prototype, "CopyWechatBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIInviteGetInfo_Auto.prototype, "CopyQQBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIInviteGetInfo_Auto.prototype, "CloseBtn", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIInviteGetInfo_Auto.prototype, "HeadSP", void 0);
    __decorate([
        property(cc.Label)
    ], UIInviteGetInfo_Auto.prototype, "NameLab", void 0);
    UIInviteGetInfo_Auto = __decorate([
        ccclass
    ], UIInviteGetInfo_Auto);
    return UIInviteGetInfo_Auto;
}(cc.Component));
exports.default = UIInviteGetInfo_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlJbnZpdGVHZXRJbmZvX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQWtELHdDQUFZO0lBQTlEO1FBQUEscUVBY0M7UUFaQSxnQkFBVSxHQUFlLElBQUksQ0FBQztRQUU5QixtQkFBYSxHQUFlLElBQUksQ0FBQztRQUVqQyxlQUFTLEdBQWUsSUFBSSxDQUFDO1FBRTdCLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsWUFBTSxHQUFjLElBQUksQ0FBQztRQUV6QixhQUFPLEdBQWEsSUFBSSxDQUFDOztJQUUxQixDQUFDO0lBWkE7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs0REFDUztJQUU5QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytEQUNZO0lBRWpDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7MkRBQ1E7SUFFN0I7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzswREFDTztJQUU1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3dEQUNLO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7eURBQ007SUFaTCxvQkFBb0I7UUFEeEMsT0FBTztPQUNhLG9CQUFvQixDQWN4QztJQUFELDJCQUFDO0NBZEQsQUFjQyxDQWRpRCxFQUFFLENBQUMsU0FBUyxHQWM3RDtrQkFkb0Isb0JBQW9CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCJcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlJbnZpdGVHZXRJbmZvX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0U2V0SW5mb0J0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRDb3B5V2VjaGF0QnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdENvcHlRUUJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRDbG9zZUJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5TcHJpdGUpXG5cdEhlYWRTUDogY2MuU3ByaXRlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHROYW1lTGFiOiBjYy5MYWJlbCA9IG51bGw7XG4gXG59Il19