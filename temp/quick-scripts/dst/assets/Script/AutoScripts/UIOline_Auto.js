
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIOline_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b7b0fXYG9BBtq/Avd32Qps3', 'UIOline_Auto');
// Script/AutoScripts/UIOline_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIOline_Auto = /** @class */ (function (_super) {
    __extends(UIOline_Auto, _super);
    function UIOline_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.OnlineTimeLab = null;
        _this.GetNumLab = null;
        _this.GetBtn = null;
        _this.GetDoubelBtn = null;
        _this.TipBtn = null;
        _this.CloseBtn = null;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], UIOline_Auto.prototype, "OnlineTimeLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIOline_Auto.prototype, "GetNumLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIOline_Auto.prototype, "GetBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIOline_Auto.prototype, "GetDoubelBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIOline_Auto.prototype, "TipBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIOline_Auto.prototype, "CloseBtn", void 0);
    UIOline_Auto = __decorate([
        ccclass
    ], UIOline_Auto);
    return UIOline_Auto;
}(cc.Component));
exports.default = UIOline_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlPbGluZV9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUEwQyxnQ0FBWTtJQUF0RDtRQUFBLHFFQWNDO1FBWkEsbUJBQWEsR0FBYSxJQUFJLENBQUM7UUFFL0IsZUFBUyxHQUFhLElBQUksQ0FBQztRQUUzQixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLGtCQUFZLEdBQWUsSUFBSSxDQUFDO1FBRWhDLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsY0FBUSxHQUFlLElBQUksQ0FBQzs7SUFFN0IsQ0FBQztJQVpBO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7dURBQ1k7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzttREFDUTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2dEQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7c0RBQ1c7SUFFaEM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztnREFDSztJQUUxQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2tEQUNPO0lBWlIsWUFBWTtRQURoQyxPQUFPO09BQ2EsWUFBWSxDQWNoQztJQUFELG1CQUFDO0NBZEQsQUFjQyxDQWR5QyxFQUFFLENBQUMsU0FBUyxHQWNyRDtrQkFkb0IsWUFBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSBcIi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJT2xpbmVfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0T25saW5lVGltZUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdEdldE51bUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0R2V0QnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEdldERvdWJlbEJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRUaXBCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0Q2xvc2VCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuIFxufSJdfQ==