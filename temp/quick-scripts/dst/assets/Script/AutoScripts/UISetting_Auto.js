
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UISetting_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b62fduA9cFGRoBIGa3DKEPd', 'UISetting_Auto');
// Script/AutoScripts/UISetting_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UISetting_Auto = /** @class */ (function (_super) {
    __extends(UISetting_Auto, _super);
    function UISetting_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.BtnOutLogin = null;
        _this.BtnMusic = null;
        _this.offNode = null;
        _this.onNode = null;
        _this.BtnShiMing = null;
        _this.BtnYinSi = null;
        _this.BtnXieYi = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UISetting_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UISetting_Auto.prototype, "BtnOutLogin", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UISetting_Auto.prototype, "BtnMusic", void 0);
    __decorate([
        property(cc.Node)
    ], UISetting_Auto.prototype, "offNode", void 0);
    __decorate([
        property(cc.Node)
    ], UISetting_Auto.prototype, "onNode", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UISetting_Auto.prototype, "BtnShiMing", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UISetting_Auto.prototype, "BtnYinSi", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UISetting_Auto.prototype, "BtnXieYi", void 0);
    UISetting_Auto = __decorate([
        ccclass
    ], UISetting_Auto);
    return UISetting_Auto;
}(cc.Component));
exports.default = UISetting_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlTZXR0aW5nX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQTRDLGtDQUFZO0lBQXhEO1FBQUEscUVBa0JDO1FBaEJBLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsaUJBQVcsR0FBZSxJQUFJLENBQUM7UUFFL0IsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixhQUFPLEdBQVksSUFBSSxDQUFDO1FBRXhCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFFdkIsZ0JBQVUsR0FBZSxJQUFJLENBQUM7UUFFOUIsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixjQUFRLEdBQWUsSUFBSSxDQUFDOztJQUU3QixDQUFDO0lBaEJBO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7b0RBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzt1REFDVTtJQUUvQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO29EQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7bURBQ007SUFFeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDSztJQUV2QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3NEQUNTO0lBRTlCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7b0RBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztvREFDTztJQWhCUixjQUFjO1FBRGxDLE9BQU87T0FDYSxjQUFjLENBa0JsQztJQUFELHFCQUFDO0NBbEJELEFBa0JDLENBbEIyQyxFQUFFLENBQUMsU0FBUyxHQWtCdkQ7a0JBbEJvQixjQUFjIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCJcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlTZXR0aW5nX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuQ2xvc2U6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuT3V0TG9naW46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuTXVzaWM6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTm9kZSlcblx0b2ZmTm9kZTogY2MuTm9kZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Ob2RlKVxuXHRvbk5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU2hpTWluZzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5ZaW5TaTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5YaWVZaTogQnV0dG9uUGx1cyA9IG51bGw7XG4gXG59Il19