
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIFriendSerch_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '84f1aytnrREUIxrLr2UYwFA', 'UIFriendSerch_Auto');
// Script/AutoScripts/UIFriendSerch_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendSerch_Auto = /** @class */ (function (_super) {
    __extends(UIFriendSerch_Auto, _super);
    function UIFriendSerch_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnSure = null;
        _this.BtnClose = null;
        _this.BtnSerch = null;
        _this.IDBox = null;
        _this.NameLab = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendSerch_Auto.prototype, "BtnSure", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendSerch_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendSerch_Auto.prototype, "BtnSerch", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIFriendSerch_Auto.prototype, "IDBox", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriendSerch_Auto.prototype, "NameLab", void 0);
    UIFriendSerch_Auto = __decorate([
        ccclass
    ], UIFriendSerch_Auto);
    return UIFriendSerch_Auto;
}(cc.Component));
exports.default = UIFriendSerch_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlGcmllbmRTZXJjaF9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUFnRCxzQ0FBWTtJQUE1RDtRQUFBLHFFQVlDO1FBVkEsYUFBTyxHQUFlLElBQUksQ0FBQztRQUUzQixjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixhQUFPLEdBQWEsSUFBSSxDQUFDOztJQUUxQixDQUFDO0lBVkE7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzt1REFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3dEQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7d0RBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDSTtJQUV6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3VEQUNNO0lBVkwsa0JBQWtCO1FBRHRDLE9BQU87T0FDYSxrQkFBa0IsQ0FZdEM7SUFBRCx5QkFBQztDQVpELEFBWUMsQ0FaK0MsRUFBRSxDQUFDLFNBQVMsR0FZM0Q7a0JBWm9CLGtCQUFrQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSBcIi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJRnJpZW5kU2VyY2hfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TdXJlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkNsb3NlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blNlcmNoOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkVkaXRCb3gpXG5cdElEQm94OiBjYy5FZGl0Qm94ID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHROYW1lTGFiOiBjYy5MYWJlbCA9IG51bGw7XG4gXG59Il19