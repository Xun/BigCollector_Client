
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIShiMing_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c89c36JSJdGd6aBCzqhTRC4', 'UIShiMing_Auto');
// Script/AutoScripts/UIShiMing_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIShiMing_Auto = /** @class */ (function (_super) {
    __extends(UIShiMing_Auto, _super);
    function UIShiMing_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Name = null;
        _this.IDCard = null;
        _this.BtnClose = null;
        _this.BtnSubMit = null;
        return _this;
    }
    __decorate([
        property(cc.EditBox)
    ], UIShiMing_Auto.prototype, "Name", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIShiMing_Auto.prototype, "IDCard", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShiMing_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShiMing_Auto.prototype, "BtnSubMit", void 0);
    UIShiMing_Auto = __decorate([
        ccclass
    ], UIShiMing_Auto);
    return UIShiMing_Auto;
}(cc.Component));
exports.default = UIShiMing_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlTaGlNaW5nX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQTRDLGtDQUFZO0lBQXhEO1FBQUEscUVBVUM7UUFSQSxVQUFJLEdBQWUsSUFBSSxDQUFDO1FBRXhCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixlQUFTLEdBQWUsSUFBSSxDQUFDOztJQUU5QixDQUFDO0lBUkE7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztnREFDRztJQUV4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO2tEQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7b0RBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztxREFDUTtJQVJULGNBQWM7UUFEbEMsT0FBTztPQUNhLGNBQWMsQ0FVbEM7SUFBRCxxQkFBQztDQVZELEFBVUMsQ0FWMkMsRUFBRSxDQUFDLFNBQVMsR0FVdkQ7a0JBVm9CLGNBQWMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSVNoaU1pbmdfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShjYy5FZGl0Qm94KVxuXHROYW1lOiBjYy5FZGl0Qm94ID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkVkaXRCb3gpXG5cdElEQ2FyZDogY2MuRWRpdEJveCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5DbG9zZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TdWJNaXQ6IEJ1dHRvblBsdXMgPSBudWxsO1xuIFxufSJdfQ==