
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIRank_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7d2b3b0k/1H76o2r8ZjjMXG', 'UIRank_Auto');
// Script/AutoScripts/UIRank_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIRank_Auto = /** @class */ (function (_super) {
    __extends(UIRank_Auto, _super);
    function UIRank_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.RankToggleNode = null;
        _this.CoinContentNode = null;
        _this.CoinContentViewNode = null;
        _this.CloseBtn = null;
        return _this;
    }
    __decorate([
        property(cc.Node)
    ], UIRank_Auto.prototype, "RankToggleNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIRank_Auto.prototype, "CoinContentNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIRank_Auto.prototype, "CoinContentViewNode", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIRank_Auto.prototype, "CloseBtn", void 0);
    UIRank_Auto = __decorate([
        ccclass
    ], UIRank_Auto);
    return UIRank_Auto;
}(cc.Component));
exports.default = UIRank_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlSYW5rX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQXlDLCtCQUFZO0lBQXJEO1FBQUEscUVBVUM7UUFSQSxvQkFBYyxHQUFZLElBQUksQ0FBQztRQUUvQixxQkFBZSxHQUFZLElBQUksQ0FBQztRQUVoQyx5QkFBbUIsR0FBWSxJQUFJLENBQUM7UUFFcEMsY0FBUSxHQUFlLElBQUksQ0FBQzs7SUFFN0IsQ0FBQztJQVJBO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7dURBQ2E7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt3REFDYztJQUVoQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzREQUNrQjtJQUVwQztRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO2lEQUNPO0lBUlIsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQVUvQjtJQUFELGtCQUFDO0NBVkQsQUFVQyxDQVZ3QyxFQUFFLENBQUMsU0FBUyxHQVVwRDtrQkFWb0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSBcIi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJUmFua19BdXRvIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdFJhbmtUb2dnbGVOb2RlOiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdENvaW5Db250ZW50Tm9kZTogY2MuTm9kZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Ob2RlKVxuXHRDb2luQ29udGVudFZpZXdOb2RlOiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdENsb3NlQnRuOiBCdXR0b25QbHVzID0gbnVsbDtcbiBcbn0iXX0=