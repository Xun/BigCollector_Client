
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIShare_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ee1aaKtjQFA64ct+eNWOX7L', 'UIShare_Auto');
// Script/AutoScripts/UIShare_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIShare_Auto = /** @class */ (function (_super) {
    __extends(UIShare_Auto, _super);
    function UIShare_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.CloseBtn = null;
        _this.WechatBtn = null;
        _this.PengYouQuanBtn = null;
        _this.PhotoBtn = null;
        _this.contentNode = null;
        _this.HeadSp1 = null;
        _this.NameLab1 = null;
        _this.HeadSp2 = null;
        _this.NameLab2 = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShare_Auto.prototype, "CloseBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShare_Auto.prototype, "WechatBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShare_Auto.prototype, "PengYouQuanBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIShare_Auto.prototype, "PhotoBtn", void 0);
    __decorate([
        property(cc.Node)
    ], UIShare_Auto.prototype, "contentNode", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIShare_Auto.prototype, "HeadSp1", void 0);
    __decorate([
        property(cc.Label)
    ], UIShare_Auto.prototype, "NameLab1", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIShare_Auto.prototype, "HeadSp2", void 0);
    __decorate([
        property(cc.Label)
    ], UIShare_Auto.prototype, "NameLab2", void 0);
    UIShare_Auto = __decorate([
        ccclass
    ], UIShare_Auto);
    return UIShare_Auto;
}(cc.Component));
exports.default = UIShare_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlTaGFyZV9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUEwQyxnQ0FBWTtJQUF0RDtRQUFBLHFFQW9CQztRQWxCQSxjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLGVBQVMsR0FBZSxJQUFJLENBQUM7UUFFN0Isb0JBQWMsR0FBZSxJQUFJLENBQUM7UUFFbEMsY0FBUSxHQUFlLElBQUksQ0FBQztRQUU1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUU1QixhQUFPLEdBQWMsSUFBSSxDQUFDO1FBRTFCLGNBQVEsR0FBYSxJQUFJLENBQUM7UUFFMUIsYUFBTyxHQUFjLElBQUksQ0FBQztRQUUxQixjQUFRLEdBQWEsSUFBSSxDQUFDOztJQUUzQixDQUFDO0lBbEJBO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDUTtJQUU3QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3dEQUNhO0lBRWxDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ087SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxREFDVTtJQUU1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO2lEQUNNO0lBRTFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7a0RBQ087SUFFMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztpREFDTTtJQUUxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2tEQUNPO0lBbEJOLFlBQVk7UUFEaEMsT0FBTztPQUNhLFlBQVksQ0FvQmhDO0lBQUQsbUJBQUM7Q0FwQkQsQUFvQkMsQ0FwQnlDLEVBQUUsQ0FBQyxTQUFTLEdBb0JyRDtrQkFwQm9CLFlBQVkiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSVNoYXJlX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0Q2xvc2VCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0V2VjaGF0QnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdFBlbmdZb3VRdWFuQnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdFBob3RvQnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdGNvbnRlbnROb2RlOiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0SGVhZFNwMTogY2MuU3ByaXRlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHROYW1lTGFiMTogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuU3ByaXRlKVxuXHRIZWFkU3AyOiBjYy5TcHJpdGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE5hbWVMYWIyOiBjYy5MYWJlbCA9IG51bGw7XG4gXG59Il19