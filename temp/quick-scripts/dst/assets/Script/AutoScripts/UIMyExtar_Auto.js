
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIMyExtar_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ecf0c8eTQxFtLgIHpoFKg8C', 'UIMyExtar_Auto');
// Script/AutoScripts/UIMyExtar_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMyExtar_Auto = /** @class */ (function (_super) {
    __extends(UIMyExtar_Auto, _super);
    function UIMyExtar_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.TodayExtarMoneyNumLab = null;
        _this.TodayExtarAddUserNumLab = null;
        _this.MonthExtarMoneyNumLab = null;
        _this.MonthExtarAddUserNumLab = null;
        _this.TotalExtarMoneyNumLab = null;
        _this.TotalExtarAddUserNumLab = null;
        _this.JieDuanNumLab = null;
        _this.JDCurExtarMoneyNumLab = null;
        _this.JDTotalExtarMoneyNumLab = null;
        _this.CompleteMoneyLab = null;
        _this.JDPS = null;
        _this.MultLab = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyExtar_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "TodayExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "TodayExtarAddUserNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "MonthExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "MonthExtarAddUserNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "TotalExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "TotalExtarAddUserNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "JieDuanNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "JDCurExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "JDTotalExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "CompleteMoneyLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UIMyExtar_Auto.prototype, "JDPS", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "MultLab", void 0);
    UIMyExtar_Auto = __decorate([
        ccclass
    ], UIMyExtar_Auto);
    return UIMyExtar_Auto;
}(cc.Component));
exports.default = UIMyExtar_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlNeUV4dGFyX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQTRDLGtDQUFZO0lBQXhEO1FBQUEscUVBNEJDO1FBMUJBLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsMkJBQXFCLEdBQWEsSUFBSSxDQUFDO1FBRXZDLDZCQUF1QixHQUFhLElBQUksQ0FBQztRQUV6QywyQkFBcUIsR0FBYSxJQUFJLENBQUM7UUFFdkMsNkJBQXVCLEdBQWEsSUFBSSxDQUFDO1FBRXpDLDJCQUFxQixHQUFhLElBQUksQ0FBQztRQUV2Qyw2QkFBdUIsR0FBYSxJQUFJLENBQUM7UUFFekMsbUJBQWEsR0FBYSxJQUFJLENBQUM7UUFFL0IsMkJBQXFCLEdBQWEsSUFBSSxDQUFDO1FBRXZDLDZCQUF1QixHQUFhLElBQUksQ0FBQztRQUV6QyxzQkFBZ0IsR0FBYSxJQUFJLENBQUM7UUFFbEMsVUFBSSxHQUFtQixJQUFJLENBQUM7UUFFNUIsYUFBTyxHQUFhLElBQUksQ0FBQzs7SUFFMUIsQ0FBQztJQTFCQTtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO29EQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aUVBQ29CO0lBRXZDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7bUVBQ3NCO0lBRXpDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aUVBQ29CO0lBRXZDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7bUVBQ3NCO0lBRXpDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aUVBQ29CO0lBRXZDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7bUVBQ3NCO0lBRXpDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7eURBQ1k7SUFFL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztpRUFDb0I7SUFFdkM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzttRUFDc0I7SUFFekM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs0REFDZTtJQUVsQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO2dEQUNHO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7bURBQ007SUExQkwsY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQTRCbEM7SUFBRCxxQkFBQztDQTVCRCxBQTRCQyxDQTVCMkMsRUFBRSxDQUFDLFNBQVMsR0E0QnZEO2tCQTVCb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSBcIi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJTXlFeHRhcl9BdXRvIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkNsb3NlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRUb2RheUV4dGFyTW9uZXlOdW1MYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRUb2RheUV4dGFyQWRkVXNlck51bUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE1vbnRoRXh0YXJNb25leU51bUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE1vbnRoRXh0YXJBZGRVc2VyTnVtTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0VG90YWxFeHRhck1vbmV5TnVtTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0VG90YWxFeHRhckFkZFVzZXJOdW1MYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRKaWVEdWFuTnVtTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0SkRDdXJFeHRhck1vbmV5TnVtTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0SkRUb3RhbEV4dGFyTW9uZXlOdW1MYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRDb21wbGV0ZU1vbmV5TGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Qcm9ncmVzc0Jhcilcblx0SkRQUzogY2MuUHJvZ3Jlc3NCYXIgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE11bHRMYWI6IGNjLkxhYmVsID0gbnVsbDtcbiBcbn0iXX0=