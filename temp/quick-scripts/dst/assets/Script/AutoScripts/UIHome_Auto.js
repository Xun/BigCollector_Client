
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIHome_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'eab67NVkkJG2pd3DYRReg9U', 'UIHome_Auto');
// Script/AutoScripts/UIHome_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIHome_Auto = /** @class */ (function (_super) {
    __extends(UIHome_Auto, _super);
    function UIHome_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.accEffect = null;
        _this.Accelerate = null;
        _this.RecoveryBtn = null;
        _this.topNode = null;
        _this.MakeMoneyTxt = null;
        _this.CoinTxt = null;
        _this.DiamondTxt = null;
        _this.MoneyTxt = null;
        _this.HeadSp = null;
        _this.RankBtn = null;
        _this.CollectBtn = null;
        _this.BagBtn = null;
        _this.ShopBtn = null;
        _this.DrawBtn = null;
        _this.WorkContent = null;
        _this.FriendBtn = null;
        _this.SignInBtn = null;
        _this.BuyBtn = null;
        _this.BtnCakeSp = null;
        _this.BuyCoinNumTxt = null;
        _this.MyBtn = null;
        _this.TaskBtn = null;
        _this.CombineAutoBtn = null;
        _this.CombineAutoTxt = null;
        return _this;
    }
    __decorate([
        property(cc.Node)
    ], UIHome_Auto.prototype, "accEffect", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "Accelerate", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "RecoveryBtn", void 0);
    __decorate([
        property(cc.Node)
    ], UIHome_Auto.prototype, "topNode", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "MakeMoneyTxt", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "CoinTxt", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "DiamondTxt", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "MoneyTxt", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIHome_Auto.prototype, "HeadSp", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "RankBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "CollectBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "BagBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "ShopBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "DrawBtn", void 0);
    __decorate([
        property(cc.Node)
    ], UIHome_Auto.prototype, "WorkContent", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "FriendBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "SignInBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "BuyBtn", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIHome_Auto.prototype, "BtnCakeSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "BuyCoinNumTxt", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "MyBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "TaskBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIHome_Auto.prototype, "CombineAutoBtn", void 0);
    __decorate([
        property(cc.Label)
    ], UIHome_Auto.prototype, "CombineAutoTxt", void 0);
    UIHome_Auto = __decorate([
        ccclass
    ], UIHome_Auto);
    return UIHome_Auto;
}(cc.Component));
exports.default = UIHome_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlIb21lX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQXlDLCtCQUFZO0lBQXJEO1FBQUEscUVBa0RDO1FBaERBLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFFMUIsZ0JBQVUsR0FBZSxJQUFJLENBQUM7UUFFOUIsaUJBQVcsR0FBZSxJQUFJLENBQUM7UUFFL0IsYUFBTyxHQUFZLElBQUksQ0FBQztRQUV4QixrQkFBWSxHQUFhLElBQUksQ0FBQztRQUU5QixhQUFPLEdBQWEsSUFBSSxDQUFDO1FBRXpCLGdCQUFVLEdBQWEsSUFBSSxDQUFDO1FBRTVCLGNBQVEsR0FBYSxJQUFJLENBQUM7UUFFMUIsWUFBTSxHQUFjLElBQUksQ0FBQztRQUV6QixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLGdCQUFVLEdBQWUsSUFBSSxDQUFDO1FBRTlCLFlBQU0sR0FBZSxJQUFJLENBQUM7UUFFMUIsYUFBTyxHQUFlLElBQUksQ0FBQztRQUUzQixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRTVCLGVBQVMsR0FBZSxJQUFJLENBQUM7UUFFN0IsZUFBUyxHQUFlLElBQUksQ0FBQztRQUU3QixZQUFNLEdBQWUsSUFBSSxDQUFDO1FBRTFCLGVBQVMsR0FBYyxJQUFJLENBQUM7UUFFNUIsbUJBQWEsR0FBYSxJQUFJLENBQUM7UUFFL0IsV0FBSyxHQUFlLElBQUksQ0FBQztRQUV6QixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLG9CQUFjLEdBQWUsSUFBSSxDQUFDO1FBRWxDLG9CQUFjLEdBQWEsSUFBSSxDQUFDOztJQUVqQyxDQUFDO0lBaERBO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7a0RBQ1E7SUFFMUI7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDUztJQUU5QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO29EQUNVO0lBRS9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ007SUFFeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztxREFDVztJQUU5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dEQUNNO0lBRXpCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7bURBQ1M7SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztpREFDTztJQUUxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOytDQUNLO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7Z0RBQ007SUFFM0I7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzttREFDUztJQUU5QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytDQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7Z0RBQ007SUFFM0I7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztnREFDTTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO29EQUNVO0lBRTVCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7a0RBQ1E7SUFFN0I7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQztrREFDUTtJQUU3QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOytDQUNLO0lBRTFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7a0RBQ1E7SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztzREFDWTtJQUUvQjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzhDQUNJO0lBRXpCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7Z0RBQ007SUFFM0I7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzt1REFDYTtJQUVsQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3VEQUNhO0lBaERaLFdBQVc7UUFEL0IsT0FBTztPQUNhLFdBQVcsQ0FrRC9CO0lBQUQsa0JBQUM7Q0FsREQsQUFrREMsQ0FsRHdDLEVBQUUsQ0FBQyxTQUFTLEdBa0RwRDtrQkFsRG9CLFdBQVciLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSUhvbWVfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShjYy5Ob2RlKVxuXHRhY2NFZmZlY3Q6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QWNjZWxlcmF0ZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRSZWNvdmVyeUJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Ob2RlKVxuXHR0b3BOb2RlOiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRNYWtlTW9uZXlUeHQ6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRDb2luVHh0OiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0RGlhbW9uZFR4dDogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE1vbmV5VHh0OiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5TcHJpdGUpXG5cdEhlYWRTcDogY2MuU3ByaXRlID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdFJhbmtCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0Q29sbGVjdEJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCYWdCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0U2hvcEJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHREcmF3QnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdFdvcmtDb250ZW50OiBjYy5Ob2RlID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEZyaWVuZEJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRTaWduSW5CdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnV5QnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0QnRuQ2FrZVNwOiBjYy5TcHJpdGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdEJ1eUNvaW5OdW1UeHQ6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdE15QnRuOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdFRhc2tCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0Q29tYmluZUF1dG9CdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdENvbWJpbmVBdXRvVHh0OiBjYy5MYWJlbCA9IG51bGw7XG4gXG59Il19