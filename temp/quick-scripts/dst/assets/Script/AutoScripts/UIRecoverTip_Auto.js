
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIRecoverTip_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b1aecLFsalKWrCZShRN4eGA', 'UIRecoverTip_Auto');
// Script/AutoScripts/UIRecoverTip_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIRecoverTip_Auto = /** @class */ (function (_super) {
    __extends(UIRecoverTip_Auto, _super);
    function UIRecoverTip_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnSure = null;
        _this.GetIconTypeSp = null;
        _this.BtnClose = null;
        _this.BuyMoneyTypeSp = null;
        _this.GetNumLab = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIRecoverTip_Auto.prototype, "BtnSure", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIRecoverTip_Auto.prototype, "GetIconTypeSp", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIRecoverTip_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIRecoverTip_Auto.prototype, "BuyMoneyTypeSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIRecoverTip_Auto.prototype, "GetNumLab", void 0);
    UIRecoverTip_Auto = __decorate([
        ccclass
    ], UIRecoverTip_Auto);
    return UIRecoverTip_Auto;
}(cc.Component));
exports.default = UIRecoverTip_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlSZWNvdmVyVGlwX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQStDLHFDQUFZO0lBQTNEO1FBQUEscUVBWUM7UUFWQSxhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLG1CQUFhLEdBQWMsSUFBSSxDQUFDO1FBRWhDLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsb0JBQWMsR0FBYyxJQUFJLENBQUM7UUFFakMsZUFBUyxHQUFhLElBQUksQ0FBQzs7SUFFNUIsQ0FBQztJQVZBO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7c0RBQ007SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs0REFDWTtJQUVoQztRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3VEQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7NkRBQ2E7SUFFakM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzt3REFDUTtJQVZQLGlCQUFpQjtRQURyQyxPQUFPO09BQ2EsaUJBQWlCLENBWXJDO0lBQUQsd0JBQUM7Q0FaRCxBQVlDLENBWjhDLEVBQUUsQ0FBQyxTQUFTLEdBWTFEO2tCQVpvQixpQkFBaUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSVJlY292ZXJUaXBfQXV0byBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TdXJlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0R2V0SWNvblR5cGVTcDogY2MuU3ByaXRlID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bkNsb3NlOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0QnV5TW9uZXlUeXBlU3A6IGNjLlNwcml0ZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0R2V0TnVtTGFiOiBjYy5MYWJlbCA9IG51bGw7XG4gXG59Il19