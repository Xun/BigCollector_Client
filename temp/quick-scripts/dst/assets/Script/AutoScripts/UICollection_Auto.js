
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UICollection_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4c62a0jAhdDIp65t6olWl8V', 'UICollection_Auto');
// Script/AutoScripts/UICollection_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UICollection_Auto = /** @class */ (function (_super) {
    __extends(UICollection_Auto, _super);
    function UICollection_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ContentNode = null;
        _this.WenFangSiBaoProgress = null;
        _this.WenFangSiBaoProgressLab = null;
        _this.BtnWenFangSiBao = null;
        _this.WenFangSiBaoGetNumTipLab = null;
        _this.SiJunZiProgress = null;
        _this.SiJunZiProgressLab = null;
        _this.BtnSiJunZi = null;
        _this.SiJunZiGetNumTipLab = null;
        _this.MingZhuProgress = null;
        _this.MingZhuProgressLab = null;
        _this.BtnMingZhu = null;
        _this.MingZhuGetNumTipLab = null;
        _this.ShenShouProgress = null;
        _this.ShenShouProgressLab = null;
        _this.BtnShenShou = null;
        _this.ShenShouGetNumTipLab = null;
        _this.BaDaJiaProgress = null;
        _this.BaDaJiaProgressLab = null;
        _this.BtnBaDaJia = null;
        _this.BaDaJiaGetNumTipLab = null;
        _this.JinChanProgress = null;
        _this.JinChanProgressLab = null;
        _this.JinChanGetNumTipLab = null;
        _this.PiXiuProgress = null;
        _this.PiXiuProgressLab = null;
        _this.BtnPiXiu = null;
        _this.PiXiuGetNumTipLab = null;
        _this.JinLongProgress = null;
        _this.JinLongProgressLab = null;
        _this.BtnJinLong = null;
        _this.JinLongGetNumTipLab = null;
        _this.CloseBtn = null;
        return _this;
    }
    __decorate([
        property(cc.Node)
    ], UICollection_Auto.prototype, "ContentNode", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "WenFangSiBaoProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "WenFangSiBaoProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnWenFangSiBao", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "WenFangSiBaoGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "SiJunZiProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "SiJunZiProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnSiJunZi", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "SiJunZiGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "MingZhuProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "MingZhuProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnMingZhu", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "MingZhuGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "ShenShouProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "ShenShouProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnShenShou", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "ShenShouGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "BaDaJiaProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "BaDaJiaProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnBaDaJia", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "BaDaJiaGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "JinChanProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "JinChanProgressLab", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "JinChanGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "PiXiuProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "PiXiuProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnPiXiu", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "PiXiuGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "JinLongProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "JinLongProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnJinLong", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "JinLongGetNumTipLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "CloseBtn", void 0);
    UICollection_Auto = __decorate([
        ccclass
    ], UICollection_Auto);
    return UICollection_Auto;
}(cc.Component));
exports.default = UICollection_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlDb2xsZWN0aW9uX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQStDLHFDQUFZO0lBQTNEO1FBQUEscUVBb0VDO1FBbEVBLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRTVCLDBCQUFvQixHQUFtQixJQUFJLENBQUM7UUFFNUMsNkJBQXVCLEdBQWEsSUFBSSxDQUFDO1FBRXpDLHFCQUFlLEdBQWUsSUFBSSxDQUFDO1FBRW5DLDhCQUF3QixHQUFhLElBQUksQ0FBQztRQUUxQyxxQkFBZSxHQUFtQixJQUFJLENBQUM7UUFFdkMsd0JBQWtCLEdBQWEsSUFBSSxDQUFDO1FBRXBDLGdCQUFVLEdBQWUsSUFBSSxDQUFDO1FBRTlCLHlCQUFtQixHQUFhLElBQUksQ0FBQztRQUVyQyxxQkFBZSxHQUFtQixJQUFJLENBQUM7UUFFdkMsd0JBQWtCLEdBQWEsSUFBSSxDQUFDO1FBRXBDLGdCQUFVLEdBQWUsSUFBSSxDQUFDO1FBRTlCLHlCQUFtQixHQUFhLElBQUksQ0FBQztRQUVyQyxzQkFBZ0IsR0FBbUIsSUFBSSxDQUFDO1FBRXhDLHlCQUFtQixHQUFhLElBQUksQ0FBQztRQUVyQyxpQkFBVyxHQUFlLElBQUksQ0FBQztRQUUvQiwwQkFBb0IsR0FBYSxJQUFJLENBQUM7UUFFdEMscUJBQWUsR0FBbUIsSUFBSSxDQUFDO1FBRXZDLHdCQUFrQixHQUFhLElBQUksQ0FBQztRQUVwQyxnQkFBVSxHQUFlLElBQUksQ0FBQztRQUU5Qix5QkFBbUIsR0FBYSxJQUFJLENBQUM7UUFFckMscUJBQWUsR0FBbUIsSUFBSSxDQUFDO1FBRXZDLHdCQUFrQixHQUFhLElBQUksQ0FBQztRQUVwQyx5QkFBbUIsR0FBYSxJQUFJLENBQUM7UUFFckMsbUJBQWEsR0FBbUIsSUFBSSxDQUFDO1FBRXJDLHNCQUFnQixHQUFhLElBQUksQ0FBQztRQUVsQyxjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLHVCQUFpQixHQUFhLElBQUksQ0FBQztRQUVuQyxxQkFBZSxHQUFtQixJQUFJLENBQUM7UUFFdkMsd0JBQWtCLEdBQWEsSUFBSSxDQUFDO1FBRXBDLGdCQUFVLEdBQWUsSUFBSSxDQUFDO1FBRTlCLHlCQUFtQixHQUFhLElBQUksQ0FBQztRQUVyQyxjQUFRLEdBQWUsSUFBSSxDQUFDOztJQUU3QixDQUFDO0lBbEVBO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MERBQ1U7SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzttRUFDbUI7SUFFNUM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztzRUFDc0I7SUFFekM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzs4REFDYztJQUVuQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3VFQUN1QjtJQUUxQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDOzhEQUNjO0lBRXZDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aUVBQ2lCO0lBRXBDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7eURBQ1M7SUFFOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztrRUFDa0I7SUFFckM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzs4REFDYztJQUV2QztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2lFQUNpQjtJQUVwQztRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO3lEQUNTO0lBRTlCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7a0VBQ2tCO0lBRXJDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7K0RBQ2U7SUFFeEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztrRUFDa0I7SUFFckM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzswREFDVTtJQUUvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO21FQUNtQjtJQUV0QztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDOzhEQUNjO0lBRXZDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aUVBQ2lCO0lBRXBDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7eURBQ1M7SUFFOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztrRUFDa0I7SUFFckM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzs4REFDYztJQUV2QztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2lFQUNpQjtJQUVwQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2tFQUNrQjtJQUVyQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDOzREQUNZO0lBRXJDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7K0RBQ2U7SUFFbEM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzt1REFDTztJQUU1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dFQUNnQjtJQUVuQztRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDOzhEQUNjO0lBRXZDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aUVBQ2lCO0lBRXBDO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7eURBQ1M7SUFFOUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztrRUFDa0I7SUFFckM7UUFEQyxRQUFRLENBQUMsb0JBQVUsQ0FBQzt1REFDTztJQWxFUixpQkFBaUI7UUFEckMsT0FBTztPQUNhLGlCQUFpQixDQW9FckM7SUFBRCx3QkFBQztDQXBFRCxBQW9FQyxDQXBFOEMsRUFBRSxDQUFDLFNBQVMsR0FvRTFEO2tCQXBFb0IsaUJBQWlCIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgQnV0dG9uUGx1cyBmcm9tIFwiLi8uLi9Db21tb24vQ29tcG9uZW50cy9CdXR0b25QbHVzXCJcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVUlDb2xsZWN0aW9uX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoY2MuTm9kZSlcblx0Q29udGVudE5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuUHJvZ3Jlc3NCYXIpXG5cdFdlbkZhbmdTaUJhb1Byb2dyZXNzOiBjYy5Qcm9ncmVzc0JhciA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0V2VuRmFuZ1NpQmFvUHJvZ3Jlc3NMYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0bldlbkZhbmdTaUJhbzogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0V2VuRmFuZ1NpQmFvR2V0TnVtVGlwTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Qcm9ncmVzc0Jhcilcblx0U2lKdW5aaVByb2dyZXNzOiBjYy5Qcm9ncmVzc0JhciA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0U2lKdW5aaVByb2dyZXNzTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5TaUp1blppOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRTaUp1blppR2V0TnVtVGlwTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Qcm9ncmVzc0Jhcilcblx0TWluZ1podVByb2dyZXNzOiBjYy5Qcm9ncmVzc0JhciA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TWluZ1podVByb2dyZXNzTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5NaW5nWmh1OiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRNaW5nWmh1R2V0TnVtVGlwTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Qcm9ncmVzc0Jhcilcblx0U2hlblNob3VQcm9ncmVzczogY2MuUHJvZ3Jlc3NCYXIgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdFNoZW5TaG91UHJvZ3Jlc3NMYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KEJ1dHRvblBsdXMpXG5cdEJ0blNoZW5TaG91OiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRTaGVuU2hvdUdldE51bVRpcExhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuUHJvZ3Jlc3NCYXIpXG5cdEJhRGFKaWFQcm9ncmVzczogY2MuUHJvZ3Jlc3NCYXIgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdEJhRGFKaWFQcm9ncmVzc0xhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuQmFEYUppYTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0QmFEYUppYUdldE51bVRpcExhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuUHJvZ3Jlc3NCYXIpXG5cdEppbkNoYW5Qcm9ncmVzczogY2MuUHJvZ3Jlc3NCYXIgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdEppbkNoYW5Qcm9ncmVzc0xhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdEppbkNoYW5HZXROdW1UaXBMYWI6IGNjLkxhYmVsID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLlByb2dyZXNzQmFyKVxuXHRQaVhpdVByb2dyZXNzOiBjYy5Qcm9ncmVzc0JhciA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0UGlYaXVQcm9ncmVzc0xhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuUGlYaXU6IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdFBpWGl1R2V0TnVtVGlwTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Qcm9ncmVzc0Jhcilcblx0SmluTG9uZ1Byb2dyZXNzOiBjYy5Qcm9ncmVzc0JhciA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0SmluTG9uZ1Byb2dyZXNzTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5KaW5Mb25nOiBCdXR0b25QbHVzID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRKaW5Mb25nR2V0TnVtVGlwTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRDbG9zZUJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG4gXG59Il19