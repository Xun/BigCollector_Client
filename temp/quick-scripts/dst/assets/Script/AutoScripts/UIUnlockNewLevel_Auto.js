
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIUnlockNewLevel_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5d508kJilBGOpda13fF4eYa', 'UIUnlockNewLevel_Auto');
// Script/AutoScripts/UIUnlockNewLevel_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIUnlockNewLevel_Auto = /** @class */ (function (_super) {
    __extends(UIUnlockNewLevel_Auto, _super);
    function UIUnlockNewLevel_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.NewLevelLab = null;
        _this.NewNameLab = null;
        _this.CurLevelProductionNumLab = null;
        _this.CurLevelProductionIconSp = null;
        _this.YinPiaoNode = null;
        _this.YinPiaoNumLab = null;
        _this.SuiPianNode = null;
        _this.SuiPianNameLab = null;
        _this.SuiPianIconSp = null;
        _this.CloseBtn = null;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "NewLevelLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "NewNameLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "CurLevelProductionNumLab", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIUnlockNewLevel_Auto.prototype, "CurLevelProductionIconSp", void 0);
    __decorate([
        property(cc.Node)
    ], UIUnlockNewLevel_Auto.prototype, "YinPiaoNode", void 0);
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "YinPiaoNumLab", void 0);
    __decorate([
        property(cc.Node)
    ], UIUnlockNewLevel_Auto.prototype, "SuiPianNode", void 0);
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "SuiPianNameLab", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIUnlockNewLevel_Auto.prototype, "SuiPianIconSp", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIUnlockNewLevel_Auto.prototype, "CloseBtn", void 0);
    UIUnlockNewLevel_Auto = __decorate([
        ccclass
    ], UIUnlockNewLevel_Auto);
    return UIUnlockNewLevel_Auto;
}(cc.Component));
exports.default = UIUnlockNewLevel_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlVbmxvY2tOZXdMZXZlbF9BdXRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdFQUEwRDtBQUVwRCxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUUxQztJQUFtRCx5Q0FBWTtJQUEvRDtRQUFBLHFFQXNCQztRQXBCQSxpQkFBVyxHQUFhLElBQUksQ0FBQztRQUU3QixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUU1Qiw4QkFBd0IsR0FBYSxJQUFJLENBQUM7UUFFMUMsOEJBQXdCLEdBQWMsSUFBSSxDQUFDO1FBRTNDLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRTVCLG1CQUFhLEdBQWEsSUFBSSxDQUFDO1FBRS9CLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRTVCLG9CQUFjLEdBQWEsSUFBSSxDQUFDO1FBRWhDLG1CQUFhLEdBQWMsSUFBSSxDQUFDO1FBRWhDLGNBQVEsR0FBZSxJQUFJLENBQUM7O0lBRTdCLENBQUM7SUFwQkE7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzs4REFDVTtJQUU3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDOzZEQUNTO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7MkVBQ3VCO0lBRTFDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7MkVBQ3VCO0lBRTNDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OERBQ1U7SUFFNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztnRUFDWTtJQUUvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzhEQUNVO0lBRTVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aUVBQ2E7SUFFaEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQztnRUFDWTtJQUVoQztRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDOzJEQUNPO0lBcEJSLHFCQUFxQjtRQUR6QyxPQUFPO09BQ2EscUJBQXFCLENBc0J6QztJQUFELDRCQUFDO0NBdEJELEFBc0JDLENBdEJrRCxFQUFFLENBQUMsU0FBUyxHQXNCOUQ7a0JBdEJvQixxQkFBcUIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gXCIuLy4uL0NvbW1vbi9Db21wb25lbnRzL0J1dHRvblBsdXNcIlxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVSVVubG9ja05ld0xldmVsX0F1dG8gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE5ld0xldmVsTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TmV3TmFtZUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdEN1ckxldmVsUHJvZHVjdGlvbk51bUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuU3ByaXRlKVxuXHRDdXJMZXZlbFByb2R1Y3Rpb25JY29uU3A6IGNjLlNwcml0ZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5Ob2RlKVxuXHRZaW5QaWFvTm9kZTogY2MuTm9kZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0WWluUGlhb051bUxhYjogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTm9kZSlcblx0U3VpUGlhbk5vZGU6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdFN1aVBpYW5OYW1lTGFiOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5TcHJpdGUpXG5cdFN1aVBpYW5JY29uU3A6IGNjLlNwcml0ZSA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRDbG9zZUJ0bjogQnV0dG9uUGx1cyA9IG51bGw7XG4gXG59Il19