
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UILottery_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9fcdfEoocNAqIUplBLQBh+c', 'UILottery_Auto');
// Script/AutoScripts/UILottery_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UILottery_Auto = /** @class */ (function (_super) {
    __extends(UILottery_Auto, _super);
    function UILottery_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.LotteryReward = null;
        _this.CloseBtn = null;
        _this.GoBtn = null;
        _this.NumTxt = null;
        _this.CDTxt = null;
        return _this;
    }
    __decorate([
        property(cc.Node)
    ], UILottery_Auto.prototype, "LotteryReward", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UILottery_Auto.prototype, "CloseBtn", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UILottery_Auto.prototype, "GoBtn", void 0);
    __decorate([
        property(cc.Label)
    ], UILottery_Auto.prototype, "NumTxt", void 0);
    __decorate([
        property(cc.Label)
    ], UILottery_Auto.prototype, "CDTxt", void 0);
    UILottery_Auto = __decorate([
        ccclass
    ], UILottery_Auto);
    return UILottery_Auto;
}(cc.Component));
exports.default = UILottery_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlMb3R0ZXJ5X0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQTRDLGtDQUFZO0lBQXhEO1FBQUEscUVBWUM7UUFWQSxtQkFBYSxHQUFZLElBQUksQ0FBQztRQUU5QixjQUFRLEdBQWUsSUFBSSxDQUFDO1FBRTVCLFdBQUssR0FBZSxJQUFJLENBQUM7UUFFekIsWUFBTSxHQUFhLElBQUksQ0FBQztRQUV4QixXQUFLLEdBQWEsSUFBSSxDQUFDOztJQUV4QixDQUFDO0lBVkE7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt5REFDWTtJQUU5QjtRQURDLFFBQVEsQ0FBQyxvQkFBVSxDQUFDO29EQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7aURBQ0k7SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztrREFDSztJQUV4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2lEQUNJO0lBVkgsY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQVlsQztJQUFELHFCQUFDO0NBWkQsQUFZQyxDQVoyQyxFQUFFLENBQUMsU0FBUyxHQVl2RDtrQkFab0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSBcIi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJTG90dGVyeV9BdXRvIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblx0QHByb3BlcnR5KGNjLk5vZGUpXG5cdExvdHRlcnlSZXdhcmQ6IGNjLk5vZGUgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0Q2xvc2VCdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0R29CdG46IEJ1dHRvblBsdXMgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdE51bVR4dDogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoY2MuTGFiZWwpXG5cdENEVHh0OiBjYy5MYWJlbCA9IG51bGw7XG4gXG59Il19