
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/AutoScripts/UIDealTip_Auto.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd69b6h3XUROEIs4BCC5A9jJ', 'UIDealTip_Auto');
// Script/AutoScripts/UIDealTip_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealTip_Auto = /** @class */ (function (_super) {
    __extends(UIDealTip_Auto, _super);
    function UIDealTip_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.SpChipIcon = null;
        _this.LabName = null;
        _this.LabCurNum = null;
        _this.LabTipType = null;
        _this.BtnSure = null;
        _this.LabBuyOrSell = null;
        _this.EditeNum = null;
        _this.BtnClose = null;
        return _this;
    }
    __decorate([
        property(cc.Sprite)
    ], UIDealTip_Auto.prototype, "SpChipIcon", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealTip_Auto.prototype, "LabName", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealTip_Auto.prototype, "LabCurNum", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealTip_Auto.prototype, "LabTipType", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealTip_Auto.prototype, "BtnSure", void 0);
    __decorate([
        property(cc.Label)
    ], UIDealTip_Auto.prototype, "LabBuyOrSell", void 0);
    __decorate([
        property(cc.EditBox)
    ], UIDealTip_Auto.prototype, "EditeNum", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIDealTip_Auto.prototype, "BtnClose", void 0);
    UIDealTip_Auto = __decorate([
        ccclass
    ], UIDealTip_Auto);
    return UIDealTip_Auto;
}(cc.Component));
exports.default = UIDealTip_Auto;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvQXV0b1NjcmlwdHMvVUlEZWFsVGlwX0F1dG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsZ0VBQTBEO0FBRXBELElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRTFDO0lBQTRDLGtDQUFZO0lBQXhEO1FBQUEscUVBa0JDO1FBaEJBLGdCQUFVLEdBQWMsSUFBSSxDQUFDO1FBRTdCLGFBQU8sR0FBYSxJQUFJLENBQUM7UUFFekIsZUFBUyxHQUFhLElBQUksQ0FBQztRQUUzQixnQkFBVSxHQUFhLElBQUksQ0FBQztRQUU1QixhQUFPLEdBQWUsSUFBSSxDQUFDO1FBRTNCLGtCQUFZLEdBQWEsSUFBSSxDQUFDO1FBRTlCLGNBQVEsR0FBZSxJQUFJLENBQUM7UUFFNUIsY0FBUSxHQUFlLElBQUksQ0FBQzs7SUFFN0IsQ0FBQztJQWhCQTtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3NEQUNTO0lBRTdCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7bURBQ007SUFFekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztxREFDUTtJQUUzQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3NEQUNTO0lBRTVCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7bURBQ007SUFFM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzt3REFDVztJQUU5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO29EQUNPO0lBRTVCO1FBREMsUUFBUSxDQUFDLG9CQUFVLENBQUM7b0RBQ087SUFoQlIsY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQWtCbEM7SUFBRCxxQkFBQztDQWxCRCxBQWtCQyxDQWxCMkMsRUFBRSxDQUFDLFNBQVMsR0FrQnZEO2tCQWxCb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IEJ1dHRvblBsdXMgZnJvbSBcIi4vLi4vQ29tbW9uL0NvbXBvbmVudHMvQnV0dG9uUGx1c1wiXG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVJRGVhbFRpcF9BdXRvIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblx0QHByb3BlcnR5KGNjLlNwcml0ZSlcblx0U3BDaGlwSWNvbjogY2MuU3ByaXRlID0gbnVsbDtcblx0QHByb3BlcnR5KGNjLkxhYmVsKVxuXHRMYWJOYW1lOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TGFiQ3VyTnVtOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TGFiVGlwVHlwZTogY2MuTGFiZWwgPSBudWxsO1xuXHRAcHJvcGVydHkoQnV0dG9uUGx1cylcblx0QnRuU3VyZTogQnV0dG9uUGx1cyA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5MYWJlbClcblx0TGFiQnV5T3JTZWxsOiBjYy5MYWJlbCA9IG51bGw7XG5cdEBwcm9wZXJ0eShjYy5FZGl0Qm94KVxuXHRFZGl0ZU51bTogY2MuRWRpdEJveCA9IG51bGw7XG5cdEBwcm9wZXJ0eShCdXR0b25QbHVzKVxuXHRCdG5DbG9zZTogQnV0dG9uUGx1cyA9IG51bGw7XG4gXG59Il19