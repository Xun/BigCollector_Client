
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataDeal.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '86cccZOwR5BRaVFT2dQpxD9', 'DataDeal');
// Script/DataModal/DataDeal.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataDealDetailItem = exports.DataDealItem = exports.DataDeal = void 0;
var DataDeal = /** @class */ (function () {
    function DataDeal() {
        this.dealBuyList = Array();
        this.dealSellList = Array();
        this.dealMyBuyList = Array();
        this.dealMySellList = Array();
        this.dealDetailList = Array();
        // public dealList: [];
    }
    return DataDeal;
}());
exports.DataDeal = DataDeal;
var DataDealItem = /** @class */ (function () {
    function DataDealItem() {
    }
    return DataDealItem;
}());
exports.DataDealItem = DataDealItem;
var DataDealDetailItem = /** @class */ (function () {
    function DataDealDetailItem() {
        this.chipID = 0;
        this.time = 0;
        this.type = 0;
        this.count = 0;
        this.price = 0;
    }
    return DataDealDetailItem;
}());
exports.DataDealDetailItem = DataDealDetailItem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFEZWFsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7UUFDVyxnQkFBVyxHQUFtQixLQUFLLEVBQWdCLENBQUM7UUFDcEQsaUJBQVksR0FBbUIsS0FBSyxFQUFnQixDQUFDO1FBQ3JELGtCQUFhLEdBQW1CLEtBQUssRUFBZ0IsQ0FBQztRQUN0RCxtQkFBYyxHQUFtQixLQUFLLEVBQWdCLENBQUM7UUFDdkQsbUJBQWMsR0FBeUIsS0FBSyxFQUFzQixDQUFDO1FBQzFFLHVCQUF1QjtJQUMzQixDQUFDO0lBQUQsZUFBQztBQUFELENBUEEsQUFPQyxJQUFBO0FBUFksNEJBQVE7QUFTckI7SUFBQTtJQVFBLENBQUM7SUFBRCxtQkFBQztBQUFELENBUkEsQUFRQyxJQUFBO0FBUlksb0NBQVk7QUFVekI7SUFBQTtRQUNJLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFDbkIsU0FBSSxHQUFXLENBQUMsQ0FBQztRQUNqQixTQUFJLEdBQVcsQ0FBQyxDQUFDO1FBQ2pCLFVBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsVUFBSyxHQUFXLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBQUQseUJBQUM7QUFBRCxDQU5BLEFBTUMsSUFBQTtBQU5ZLGdEQUFrQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBEYXRhRGVhbCB7XG4gICAgcHVibGljIGRlYWxCdXlMaXN0OiBEYXRhRGVhbEl0ZW1bXSA9IEFycmF5PERhdGFEZWFsSXRlbT4oKTtcbiAgICBwdWJsaWMgZGVhbFNlbGxMaXN0OiBEYXRhRGVhbEl0ZW1bXSA9IEFycmF5PERhdGFEZWFsSXRlbT4oKTtcbiAgICBwdWJsaWMgZGVhbE15QnV5TGlzdDogRGF0YURlYWxJdGVtW10gPSBBcnJheTxEYXRhRGVhbEl0ZW0+KCk7XG4gICAgcHVibGljIGRlYWxNeVNlbGxMaXN0OiBEYXRhRGVhbEl0ZW1bXSA9IEFycmF5PERhdGFEZWFsSXRlbT4oKTtcbiAgICBwdWJsaWMgZGVhbERldGFpbExpc3Q6IERhdGFEZWFsRGV0YWlsSXRlbVtdID0gQXJyYXk8RGF0YURlYWxEZXRhaWxJdGVtPigpO1xuICAgIC8vIHB1YmxpYyBkZWFsTGlzdDogW107XG59XG5cbmV4cG9ydCBjbGFzcyBEYXRhRGVhbEl0ZW0ge1xuICAgIHB1YmxpYyBndWlkPzogc3RyaW5nO1xuICAgIHB1YmxpYyBjaGlwSUQ6IG51bWJlcjtcbiAgICBwdWJsaWMgbmVlZENvdW50PzogbnVtYmVyO1xuICAgIHB1YmxpYyBjdXJyZW50Q291bnQ/OiBudW1iZXI7XG4gICAgcHVibGljIHByaWNlPzogbnVtYmVyOy8v5Y2V5Lu3XG4gICAgcHVibGljIHVzZXJBY2NvdW50Pzogc3RyaW5nO1xuICAgIHB1YmxpYyBzdGF0dXM/OiBudW1iZXI7IC8vIC0xIOWPlua2iCAwIOi/m+ihjOS4rSAxIOW3suWujOaIkFxufVxuXG5leHBvcnQgY2xhc3MgRGF0YURlYWxEZXRhaWxJdGVtIHtcbiAgICBjaGlwSUQ6IG51bWJlciA9IDA7XG4gICAgdGltZTogbnVtYmVyID0gMDtcbiAgICB0eXBlOiBudW1iZXIgPSAwO1xuICAgIGNvdW50OiBudW1iZXIgPSAwO1xuICAgIHByaWNlOiBudW1iZXIgPSAwO1xufSJdfQ==