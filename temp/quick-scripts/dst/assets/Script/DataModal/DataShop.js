
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataShop.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bd5f86TYsJLkanbpbzBMa08', 'DataShop');
// Script/DataModal/DataShop.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopJson = exports.ChipJson = exports.DataShopItem = exports.DataShop = void 0;
var DataShop = /** @class */ (function () {
    function DataShop() {
        this.list_item = Array();
    }
    return DataShop;
}());
exports.DataShop = DataShop;
var DataShopItem = /** @class */ (function () {
    function DataShopItem() {
        this.id = "";
        this.name = "";
        this.need_num = "";
        this.need_type = "";
    }
    return DataShopItem;
}());
exports.DataShopItem = DataShopItem;
var ChipJson = /** @class */ (function () {
    function ChipJson() {
        this.chipInfo = [];
    }
    return ChipJson;
}());
exports.ChipJson = ChipJson;
var ShopJson = /** @class */ (function () {
    function ShopJson() {
        this.shopInfo = [];
    }
    return ShopJson;
}());
exports.ShopJson = ShopJson;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFTaG9wLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7UUFDVyxjQUFTLEdBQW1CLEtBQUssRUFBZ0IsQ0FBQztJQUM3RCxDQUFDO0lBQUQsZUFBQztBQUFELENBRkEsQUFFQyxJQUFBO0FBRlksNEJBQVE7QUFJckI7SUFBQTtRQUNXLE9BQUUsR0FBVyxFQUFFLENBQUM7UUFDaEIsU0FBSSxHQUFXLEVBQUUsQ0FBQztRQUNsQixhQUFRLEdBQVcsRUFBRSxDQUFDO1FBQ3RCLGNBQVMsR0FBVyxFQUFFLENBQUM7SUFDbEMsQ0FBQztJQUFELG1CQUFDO0FBQUQsQ0FMQSxBQUtDLElBQUE7QUFMWSxvQ0FBWTtBQU96QjtJQUFBO1FBQ0ksYUFBUSxHQUFVLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBQUQsZUFBQztBQUFELENBRkEsQUFFQyxJQUFBO0FBRlksNEJBQVE7QUFJckI7SUFBQTtRQUNJLGFBQVEsR0FBVSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUFELGVBQUM7QUFBRCxDQUZBLEFBRUMsSUFBQTtBQUZZLDRCQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIERhdGFTaG9wIHtcbiAgICBwdWJsaWMgbGlzdF9pdGVtOiBEYXRhU2hvcEl0ZW1bXSA9IEFycmF5PERhdGFTaG9wSXRlbT4oKTtcbn1cblxuZXhwb3J0IGNsYXNzIERhdGFTaG9wSXRlbSB7XG4gICAgcHVibGljIGlkOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBuYW1lOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBuZWVkX251bTogc3RyaW5nID0gXCJcIjtcbiAgICBwdWJsaWMgbmVlZF90eXBlOiBzdHJpbmcgPSBcIlwiO1xufVxuXG5leHBvcnQgY2xhc3MgQ2hpcEpzb24ge1xuICAgIGNoaXBJbmZvOiBhbnlbXSA9IFtdO1xufVxuXG5leHBvcnQgY2xhc3MgU2hvcEpzb24ge1xuICAgIHNob3BJbmZvOiBhbnlbXSA9IFtdO1xufSJdfQ==