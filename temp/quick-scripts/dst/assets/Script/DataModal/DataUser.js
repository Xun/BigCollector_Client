
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataUser.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bffc2P1v01J06LhjMuNsq97', 'DataUser');
// Script/DataModal/DataUser.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserInfo = exports.OperationType = void 0;
var CocosHelper_1 = require("../Utils/CocosHelper");
var OperationType;
(function (OperationType) {
    OperationType[OperationType["buy"] = 1] = "buy";
    OperationType[OperationType["sell"] = 2] = "sell";
    OperationType[OperationType["concat"] = 3] = "concat";
    OperationType[OperationType["autoBuy"] = 4] = "autoBuy";
    OperationType[OperationType["autoSell"] = 5] = "autoSell";
})(OperationType = exports.OperationType || (exports.OperationType = {}));
var UserInfo = /** @class */ (function () {
    function UserInfo() {
        //基础信息
        this._userName = "";
        this._userAvatarUrl = "";
        this._userAccount = "";
        //金币
        this._userCoin = 0;
        //钻石
        this._userDiamond = 0;
        //金额
        this._userMoney = 0;
        //当前购买所需金币
        this._userCurNeedBuyCoinNum = 0;
        //当前每秒生产金币
        this._userMakeCoin = 0;
        //工作台数据
        this._userWorkbench = new Array();
        //当前解锁等级
        this._userUnlockLevel = 1;
        //自动合成时间
        this._combineAutoTime = 15;
        this._UserInfos = null;
    }
    Object.defineProperty(UserInfo.prototype, "userName", {
        get: function () {
            if (this._userName === undefined) {
                this._userName = "";
            }
            return this._userName;
        },
        set: function (value) {
            this._userName = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userAvatarUrl", {
        get: function () {
            if (this._userAvatarUrl === undefined) {
                this._userAvatarUrl = "";
            }
            return this._userAvatarUrl;
        },
        set: function (value) {
            this._userAvatarUrl = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userAccount", {
        get: function () {
            if (this._userAccount === undefined) {
                this._userAccount = "";
            }
            return this._userAccount;
        },
        set: function (value) {
            this._userAccount = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userCoin", {
        get: function () {
            if (this._userCoin === undefined) {
                this._userCoin = 0;
            }
            return this._userCoin;
        },
        set: function (value) {
            this._userCoin = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userDiamond", {
        get: function () {
            if (this._userDiamond === undefined) {
                this._userDiamond = 0;
            }
            return this._userDiamond;
        },
        set: function (value) {
            this._userDiamond = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userMoney", {
        get: function () {
            if (this._userMoney === undefined) {
                this._userMoney = 0;
            }
            return this._userMoney;
        },
        set: function (value) {
            this._userMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userCurNeedBuyCoinNum", {
        get: function () {
            if (this._userCurNeedBuyCoinNum === undefined) {
                this._userCurNeedBuyCoinNum = 0;
            }
            return this._userCurNeedBuyCoinNum;
        },
        set: function (value) {
            this._userCurNeedBuyCoinNum = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userMakeCoin", {
        get: function () {
            if (this._userMakeCoin === undefined) {
                this._userMakeCoin = 0;
            }
            return this._userMakeCoin;
        },
        set: function (value) {
            this._userMakeCoin = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userWorkbench", {
        get: function () {
            if (this._userWorkbench === undefined) {
                this._userWorkbench = [];
            }
            return this._userWorkbench;
        },
        set: function (value) {
            this._userWorkbench = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userUnlockLevel", {
        get: function () {
            if (this._userUnlockLevel === undefined) {
                this._userUnlockLevel = 1;
            }
            return this._userUnlockLevel;
        },
        set: function (value) {
            this._userUnlockLevel = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "combineAutoTime", {
        get: function () {
            if (this._combineAutoTime === undefined) {
                this._combineAutoTime = 15;
            }
            return this._combineAutoTime;
        },
        set: function (value) {
            this._combineAutoTime = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UserInfo.prototype, "userInfos", {
        get: function () {
            return this._UserInfos;
        },
        set: function (v) {
            this._UserInfos = v;
        },
        enumerable: false,
        configurable: true
    });
    //是否还有空余位置
    UserInfo.prototype.hasPosAtWorkbench = function () {
        for (var idx = 0; idx < this._userWorkbench.length; idx++) {
            if (this._userWorkbench[idx] === 0) {
                return true;
            }
        }
        return false;
    };
    ;
    /**
     * 增加物品到工作台
     * @param {string} itemId
     * @return {number} 添加到哪个位置
     */
    UserInfo.prototype.checkNullPos = function () {
        //检查有没有空位置
        if (!this.hasPosAtWorkbench()) {
            return -1;
        }
        //找到空位置
        for (var idx = 0; idx < this._userWorkbench.length; idx++) {
            if (this._userWorkbench[idx] === 0) {
                // this._userWorkbench[idx] = itemId;
                return idx;
            }
        }
    };
    /**
     * 物品结合
     */
    UserInfo.prototype.combineCake = function (originIndex, targetIndex, callback) {
        if (!this._userWorkbench) {
            callback(false);
            return false;
        }
        if (this._userWorkbench.length <= originIndex) {
            callback(false);
            return false;
        }
        if (this._userWorkbench.length <= targetIndex) {
            callback(false);
            return false;
        }
        var targetItemId = this._userWorkbench[targetIndex];
        if (this._userWorkbench[originIndex] !== targetItemId) {
            callback(false);
            return false;
        }
        var itemInfo = CocosHelper_1.default.getItemProduceInfoById(targetItemId);
        if (!itemInfo) {
            //未找到信息，不能判断下一步进化到那一步骤
            callback(false);
            return false;
        }
        if (!itemInfo.nextLevel) {
            callback(false, 'maxLevel');
            return false;
        }
        this._userWorkbench[targetIndex] = itemInfo.nextLevel;
        this._userWorkbench[originIndex] = 0; //将原有的置空
        var nextLevel = itemInfo.nextLevel;
        var isUnlock = this.unlockProduceLevel(nextLevel);
        callback(true, isUnlock);
        return true;
    };
    /**
     * 解锁新等级
     * @param nextLevel --新等级
     */
    UserInfo.prototype.unlockProduceLevel = function (nextLevel) {
        if (nextLevel > this._userUnlockLevel) {
            var isOnlyMaxLevel = true;
            for (var idx = 0; idx < this._userWorkbench.length; idx++) {
                if (nextLevel < Number(this._userWorkbench[idx])) {
                    isOnlyMaxLevel = false;
                    break;
                }
            }
            this._userUnlockLevel = nextLevel;
            return isOnlyMaxLevel;
        }
        return false;
    };
    return UserInfo;
}());
exports.UserInfo = UserInfo;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFVc2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG9EQUErQztBQUMvQyxJQUFZLGFBTVg7QUFORCxXQUFZLGFBQWE7SUFDckIsK0NBQU8sQ0FBQTtJQUNQLGlEQUFRLENBQUE7SUFDUixxREFBVSxDQUFBO0lBQ1YsdURBQVcsQ0FBQTtJQUNYLHlEQUFZLENBQUE7QUFDaEIsQ0FBQyxFQU5XLGFBQWEsR0FBYixxQkFBYSxLQUFiLHFCQUFhLFFBTXhCO0FBQ0Q7SUFBQTtRQUVJLE1BQU07UUFDRSxjQUFTLEdBQVcsRUFBRSxDQUFDO1FBV3ZCLG1CQUFjLEdBQVcsRUFBRSxDQUFDO1FBVzVCLGlCQUFZLEdBQVcsRUFBRSxDQUFDO1FBV2xDLElBQUk7UUFDSSxjQUFTLEdBQVcsQ0FBQyxDQUFDO1FBVzlCLElBQUk7UUFDSSxpQkFBWSxHQUFXLENBQUMsQ0FBQztRQVdqQyxJQUFJO1FBQ0ksZUFBVSxHQUFXLENBQUMsQ0FBQztRQVcvQixVQUFVO1FBQ0YsMkJBQXNCLEdBQVcsQ0FBQyxDQUFDO1FBVzNDLFVBQVU7UUFDRixrQkFBYSxHQUFXLENBQUMsQ0FBQztRQVdsQyxPQUFPO1FBQ0MsbUJBQWMsR0FBa0IsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQVdwRCxRQUFRO1FBQ0EscUJBQWdCLEdBQVcsQ0FBQyxDQUFDO1FBV3JDLFFBQVE7UUFDQSxxQkFBZ0IsR0FBVyxFQUFFLENBQUM7UUFXOUIsZUFBVSxHQUFjLElBQUksQ0FBQztJQTZHekMsQ0FBQztJQTdPRyxzQkFBVyw4QkFBUTthQUFuQjtZQUNJLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2FBQ3ZCO1lBQ0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFCLENBQUM7YUFDRCxVQUFvQixLQUFhO1lBQzdCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQzNCLENBQUM7OztPQUhBO0lBTUQsc0JBQVcsbUNBQWE7YUFBeEI7WUFDSSxJQUFJLElBQUksQ0FBQyxjQUFjLEtBQUssU0FBUyxFQUFFO2dCQUNuQyxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQzthQUM1QjtZQUNELE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUMvQixDQUFDO2FBQ0QsVUFBeUIsS0FBYTtZQUNsQyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUNoQyxDQUFDOzs7T0FIQTtJQU1ELHNCQUFXLGlDQUFXO2FBQXRCO1lBQ0ksSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLFNBQVMsRUFBRTtnQkFDakMsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7YUFDMUI7WUFDRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDN0IsQ0FBQzthQUNELFVBQXVCLEtBQWE7WUFDaEMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDOUIsQ0FBQzs7O09BSEE7SUFPRCxzQkFBVyw4QkFBUTthQUFuQjtZQUNJLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO2FBQ3RCO1lBQ0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFCLENBQUM7YUFDRCxVQUFvQixLQUFhO1lBQzdCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQzNCLENBQUM7OztPQUhBO0lBT0Qsc0JBQVcsaUNBQVc7YUFBdEI7WUFDSSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssU0FBUyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQzthQUN6QjtZQUNELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztRQUM3QixDQUFDO2FBQ0QsVUFBdUIsS0FBYTtZQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDOzs7T0FIQTtJQU9ELHNCQUFXLCtCQUFTO2FBQXBCO1lBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7YUFDdkI7WUFDRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDM0IsQ0FBQzthQUNELFVBQXFCLEtBQWE7WUFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDNUIsQ0FBQzs7O09BSEE7SUFPRCxzQkFBVywyQ0FBcUI7YUFBaEM7WUFDSSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsS0FBSyxTQUFTLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxDQUFDLENBQUM7YUFDbkM7WUFDRCxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQztRQUN2QyxDQUFDO2FBQ0QsVUFBaUMsS0FBYTtZQUMxQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1FBQ3hDLENBQUM7OztPQUhBO0lBT0Qsc0JBQVcsa0NBQVk7YUFBdkI7WUFDSSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssU0FBUyxFQUFFO2dCQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQzthQUMxQjtZQUNELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM5QixDQUFDO2FBQ0QsVUFBd0IsS0FBYTtZQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMvQixDQUFDOzs7T0FIQTtJQU9ELHNCQUFXLG1DQUFhO2FBQXhCO1lBQ0ksSUFBSSxJQUFJLENBQUMsY0FBYyxLQUFLLFNBQVMsRUFBRTtnQkFDbkMsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7YUFDNUI7WUFDRCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDL0IsQ0FBQzthQUNELFVBQXlCLEtBQWU7WUFDcEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDaEMsQ0FBQzs7O09BSEE7SUFPRCxzQkFBVyxxQ0FBZTthQUExQjtZQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLFNBQVMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQzthQUM3QjtZQUNELE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ2pDLENBQUM7YUFDRCxVQUEyQixLQUFhO1lBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDbEMsQ0FBQzs7O09BSEE7SUFPRCxzQkFBVyxxQ0FBZTthQUExQjtZQUNJLElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLFNBQVMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQzthQUM5QjtZQUNELE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ2pDLENBQUM7YUFDRCxVQUEyQixLQUFhO1lBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDbEMsQ0FBQzs7O09BSEE7SUFPRCxzQkFBVywrQkFBUzthQUlwQjtZQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQTtRQUMxQixDQUFDO2FBTkQsVUFBcUIsQ0FBWTtZQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQU1ELFVBQVU7SUFDVixvQ0FBaUIsR0FBakI7UUFDSSxLQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7WUFDdkQsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDaEMsT0FBTyxJQUFJLENBQUE7YUFDZDtTQUNKO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUFBLENBQUM7SUFFRjs7OztPQUlHO0lBQ0gsK0JBQVksR0FBWjtRQUNJLFVBQVU7UUFDVixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEVBQUU7WUFDM0IsT0FBTyxDQUFDLENBQUMsQ0FBQztTQUNiO1FBRUQsT0FBTztRQUNQLEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUN2RCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNoQyxxQ0FBcUM7Z0JBQ3JDLE9BQU8sR0FBRyxDQUFDO2FBQ2Q7U0FDSjtJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNILDhCQUFXLEdBQVgsVUFBWSxXQUFXLEVBQUUsV0FBVyxFQUFFLFFBQVE7UUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sSUFBSSxXQUFXLEVBQUU7WUFDM0MsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sSUFBSSxXQUFXLEVBQUU7WUFDM0MsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUVwRCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEtBQUssWUFBWSxFQUFFO1lBQ25ELFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQixPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUVELElBQUksUUFBUSxHQUFHLHFCQUFXLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFaEUsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNYLHNCQUFzQjtZQUN0QixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRTtZQUNyQixRQUFRLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQzVCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO1FBQ3RELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUTtRQUU5QyxJQUFJLFNBQVMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO1FBQ25DLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsRCxRQUFRLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3pCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxxQ0FBa0IsR0FBbEIsVUFBbUIsU0FBaUI7UUFDaEMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ25DLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQztZQUMxQixLQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7Z0JBQ3ZELElBQUksU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQzlDLGNBQWMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLE1BQU07aUJBQ1Q7YUFDSjtZQUVELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7WUFDbEMsT0FBTyxjQUFjLENBQUM7U0FDekI7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUwsZUFBQztBQUFELENBalBBLEFBaVBDLElBQUE7QUFqUFksNEJBQVEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVc2VySW5mb3MgfSBmcm9tIFwiLi4vTWFuYWdlci9JbnRlcmZhY2VNZ3JcIjtcbmltcG9ydCBDb2Nvc0hlbHBlciBmcm9tIFwiLi4vVXRpbHMvQ29jb3NIZWxwZXJcIjtcbmV4cG9ydCBlbnVtIE9wZXJhdGlvblR5cGUge1xuICAgIGJ1eSA9IDEsLy/otK3kubBcbiAgICBzZWxsID0gMiwvL+WHuuWUrlxuICAgIGNvbmNhdCA9IDMsLy/lkIjmiJBcbiAgICBhdXRvQnV5ID0gNCxcbiAgICBhdXRvU2VsbCA9IDUsXG59XG5leHBvcnQgY2xhc3MgVXNlckluZm8ge1xuXG4gICAgLy/ln7rnoYDkv6Hmga9cbiAgICBwcml2YXRlIF91c2VyTmFtZTogc3RyaW5nID0gXCJcIjtcbiAgICBwdWJsaWMgZ2V0IHVzZXJOYW1lKCk6IHN0cmluZyB7XG4gICAgICAgIGlmICh0aGlzLl91c2VyTmFtZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl91c2VyTmFtZSA9IFwiXCI7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJOYW1lO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IHVzZXJOYW1lKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fdXNlck5hbWUgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF91c2VyQXZhdGFyVXJsOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBnZXQgdXNlckF2YXRhclVybCgpOiBzdHJpbmcge1xuICAgICAgICBpZiAodGhpcy5fdXNlckF2YXRhclVybCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl91c2VyQXZhdGFyVXJsID0gXCJcIjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fdXNlckF2YXRhclVybDtcbiAgICB9XG4gICAgcHVibGljIHNldCB1c2VyQXZhdGFyVXJsKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fdXNlckF2YXRhclVybCA9IHZhbHVlO1xuICAgIH1cblxuICAgIHByaXZhdGUgX3VzZXJBY2NvdW50OiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBnZXQgdXNlckFjY291bnQoKTogc3RyaW5nIHtcbiAgICAgICAgaWYgKHRoaXMuX3VzZXJBY2NvdW50ID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX3VzZXJBY2NvdW50ID0gXCJcIjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fdXNlckFjY291bnQ7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgdXNlckFjY291bnQodmFsdWU6IHN0cmluZykge1xuICAgICAgICB0aGlzLl91c2VyQWNjb3VudCA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8v6YeR5biBXG4gICAgcHJpdmF0ZSBfdXNlckNvaW46IG51bWJlciA9IDA7XG4gICAgcHVibGljIGdldCB1c2VyQ29pbigpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5fdXNlckNvaW4gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fdXNlckNvaW4gPSAwO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl91c2VyQ29pbjtcbiAgICB9XG4gICAgcHVibGljIHNldCB1c2VyQ29pbih2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX3VzZXJDb2luID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLy/pkrvnn7NcbiAgICBwcml2YXRlIF91c2VyRGlhbW9uZDogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IHVzZXJEaWFtb25kKCk6IG51bWJlciB7XG4gICAgICAgIGlmICh0aGlzLl91c2VyRGlhbW9uZCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl91c2VyRGlhbW9uZCA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJEaWFtb25kO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IHVzZXJEaWFtb25kKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fdXNlckRpYW1vbmQgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvL+mHkeminVxuICAgIHByaXZhdGUgX3VzZXJNb25leTogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IHVzZXJNb25leSgpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5fdXNlck1vbmV5ID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX3VzZXJNb25leSA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJNb25leTtcbiAgICB9XG4gICAgcHVibGljIHNldCB1c2VyTW9uZXkodmFsdWU6IG51bWJlcikge1xuICAgICAgICB0aGlzLl91c2VyTW9uZXkgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvL+W9k+WJjei0reS5sOaJgOmcgOmHkeW4gVxuICAgIHByaXZhdGUgX3VzZXJDdXJOZWVkQnV5Q29pbk51bTogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IHVzZXJDdXJOZWVkQnV5Q29pbk51bSgpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5fdXNlckN1ck5lZWRCdXlDb2luTnVtID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX3VzZXJDdXJOZWVkQnV5Q29pbk51bSA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJDdXJOZWVkQnV5Q29pbk51bTtcbiAgICB9XG4gICAgcHVibGljIHNldCB1c2VyQ3VyTmVlZEJ1eUNvaW5OdW0odmFsdWU6IG51bWJlcikge1xuICAgICAgICB0aGlzLl91c2VyQ3VyTmVlZEJ1eUNvaW5OdW0gPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvL+W9k+WJjeavj+enkueUn+S6p+mHkeW4gVxuICAgIHByaXZhdGUgX3VzZXJNYWtlQ29pbjogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IHVzZXJNYWtlQ29pbigpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5fdXNlck1ha2VDb2luID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX3VzZXJNYWtlQ29pbiA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJNYWtlQ29pbjtcbiAgICB9XG4gICAgcHVibGljIHNldCB1c2VyTWFrZUNvaW4odmFsdWU6IG51bWJlcikge1xuICAgICAgICB0aGlzLl91c2VyTWFrZUNvaW4gPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvL+W3peS9nOWPsOaVsOaNrlxuICAgIHByaXZhdGUgX3VzZXJXb3JrYmVuY2g6IEFycmF5PG51bWJlcj4gPSBuZXcgQXJyYXkoKTtcbiAgICBwdWJsaWMgZ2V0IHVzZXJXb3JrYmVuY2goKTogbnVtYmVyW10ge1xuICAgICAgICBpZiAodGhpcy5fdXNlcldvcmtiZW5jaCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl91c2VyV29ya2JlbmNoID0gW107XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX3VzZXJXb3JrYmVuY2g7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgdXNlcldvcmtiZW5jaCh2YWx1ZTogbnVtYmVyW10pIHtcbiAgICAgICAgdGhpcy5fdXNlcldvcmtiZW5jaCA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8v5b2T5YmN6Kej6ZSB562J57qnXG4gICAgcHJpdmF0ZSBfdXNlclVubG9ja0xldmVsOiBudW1iZXIgPSAxO1xuICAgIHB1YmxpYyBnZXQgdXNlclVubG9ja0xldmVsKCk6IG51bWJlciB7XG4gICAgICAgIGlmICh0aGlzLl91c2VyVW5sb2NrTGV2ZWwgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fdXNlclVubG9ja0xldmVsID0gMTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fdXNlclVubG9ja0xldmVsO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IHVzZXJVbmxvY2tMZXZlbCh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX3VzZXJVbmxvY2tMZXZlbCA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8v6Ieq5Yqo5ZCI5oiQ5pe26Ze0XG4gICAgcHJpdmF0ZSBfY29tYmluZUF1dG9UaW1lOiBudW1iZXIgPSAxNTtcbiAgICBwdWJsaWMgZ2V0IGNvbWJpbmVBdXRvVGltZSgpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5fY29tYmluZUF1dG9UaW1lID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX2NvbWJpbmVBdXRvVGltZSA9IDE1O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9jb21iaW5lQXV0b1RpbWU7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgY29tYmluZUF1dG9UaW1lKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fY29tYmluZUF1dG9UaW1lID0gdmFsdWU7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfVXNlckluZm9zOiBVc2VySW5mb3MgPSBudWxsO1xuXG4gICAgcHVibGljIHNldCB1c2VySW5mb3ModjogVXNlckluZm9zKSB7XG4gICAgICAgIHRoaXMuX1VzZXJJbmZvcyA9IHY7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCB1c2VySW5mb3MoKTogVXNlckluZm9zIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX1VzZXJJbmZvc1xuICAgIH1cblxuICAgIC8v5piv5ZCm6L+Y5pyJ56m65L2Z5L2N572uXG4gICAgaGFzUG9zQXRXb3JrYmVuY2goKSB7XG4gICAgICAgIGZvciAobGV0IGlkeCA9IDA7IGlkeCA8IHRoaXMuX3VzZXJXb3JrYmVuY2gubGVuZ3RoOyBpZHgrKykge1xuICAgICAgICAgICAgaWYgKHRoaXMuX3VzZXJXb3JrYmVuY2hbaWR4XSA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiDlop7liqDnianlk4HliLDlt6XkvZzlj7BcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gaXRlbUlkIFxuICAgICAqIEByZXR1cm4ge251bWJlcn0g5re75Yqg5Yiw5ZOq5Liq5L2N572uXG4gICAgICovXG4gICAgY2hlY2tOdWxsUG9zKCkge1xuICAgICAgICAvL+ajgOafpeacieayoeacieepuuS9jee9rlxuICAgICAgICBpZiAoIXRoaXMuaGFzUG9zQXRXb3JrYmVuY2goKSkge1xuICAgICAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgICB9XG5cbiAgICAgICAgLy/mib7liLDnqbrkvY3nva5cbiAgICAgICAgZm9yIChsZXQgaWR4ID0gMDsgaWR4IDwgdGhpcy5fdXNlcldvcmtiZW5jaC5sZW5ndGg7IGlkeCsrKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5fdXNlcldvcmtiZW5jaFtpZHhdID09PSAwKSB7XG4gICAgICAgICAgICAgICAgLy8gdGhpcy5fdXNlcldvcmtiZW5jaFtpZHhdID0gaXRlbUlkO1xuICAgICAgICAgICAgICAgIHJldHVybiBpZHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiDnianlk4Hnu5PlkIhcbiAgICAgKi9cbiAgICBjb21iaW5lQ2FrZShvcmlnaW5JbmRleCwgdGFyZ2V0SW5kZXgsIGNhbGxiYWNrKSB7XG4gICAgICAgIGlmICghdGhpcy5fdXNlcldvcmtiZW5jaCkge1xuICAgICAgICAgICAgY2FsbGJhY2soZmFsc2UpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuX3VzZXJXb3JrYmVuY2gubGVuZ3RoIDw9IG9yaWdpbkluZGV4KSB7XG4gICAgICAgICAgICBjYWxsYmFjayhmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5fdXNlcldvcmtiZW5jaC5sZW5ndGggPD0gdGFyZ2V0SW5kZXgpIHtcbiAgICAgICAgICAgIGNhbGxiYWNrKGZhbHNlKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCB0YXJnZXRJdGVtSWQgPSB0aGlzLl91c2VyV29ya2JlbmNoW3RhcmdldEluZGV4XTtcblxuICAgICAgICBpZiAodGhpcy5fdXNlcldvcmtiZW5jaFtvcmlnaW5JbmRleF0gIT09IHRhcmdldEl0ZW1JZCkge1xuICAgICAgICAgICAgY2FsbGJhY2soZmFsc2UpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IGl0ZW1JbmZvID0gQ29jb3NIZWxwZXIuZ2V0SXRlbVByb2R1Y2VJbmZvQnlJZCh0YXJnZXRJdGVtSWQpO1xuXG4gICAgICAgIGlmICghaXRlbUluZm8pIHtcbiAgICAgICAgICAgIC8v5pyq5om+5Yiw5L+h5oGv77yM5LiN6IO95Yik5pat5LiL5LiA5q2l6L+b5YyW5Yiw6YKj5LiA5q2l6aqkXG4gICAgICAgICAgICBjYWxsYmFjayhmYWxzZSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIWl0ZW1JbmZvLm5leHRMZXZlbCkge1xuICAgICAgICAgICAgY2FsbGJhY2soZmFsc2UsICdtYXhMZXZlbCcpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5fdXNlcldvcmtiZW5jaFt0YXJnZXRJbmRleF0gPSBpdGVtSW5mby5uZXh0TGV2ZWw7XG4gICAgICAgIHRoaXMuX3VzZXJXb3JrYmVuY2hbb3JpZ2luSW5kZXhdID0gMDsgLy/lsIbljp/mnInnmoTnva7nqbpcblxuICAgICAgICBsZXQgbmV4dExldmVsID0gaXRlbUluZm8ubmV4dExldmVsO1xuICAgICAgICBsZXQgaXNVbmxvY2sgPSB0aGlzLnVubG9ja1Byb2R1Y2VMZXZlbChuZXh0TGV2ZWwpO1xuICAgICAgICBjYWxsYmFjayh0cnVlLCBpc1VubG9jayk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIOino+mUgeaWsOetiee6p1xuICAgICAqIEBwYXJhbSBuZXh0TGV2ZWwgLS3mlrDnrYnnuqdcbiAgICAgKi9cbiAgICB1bmxvY2tQcm9kdWNlTGV2ZWwobmV4dExldmVsOiBudW1iZXIpIHtcbiAgICAgICAgaWYgKG5leHRMZXZlbCA+IHRoaXMuX3VzZXJVbmxvY2tMZXZlbCkge1xuICAgICAgICAgICAgbGV0IGlzT25seU1heExldmVsID0gdHJ1ZTtcbiAgICAgICAgICAgIGZvciAodmFyIGlkeCA9IDA7IGlkeCA8IHRoaXMuX3VzZXJXb3JrYmVuY2gubGVuZ3RoOyBpZHgrKykge1xuICAgICAgICAgICAgICAgIGlmIChuZXh0TGV2ZWwgPCBOdW1iZXIodGhpcy5fdXNlcldvcmtiZW5jaFtpZHhdKSkge1xuICAgICAgICAgICAgICAgICAgICBpc09ubHlNYXhMZXZlbCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuX3VzZXJVbmxvY2tMZXZlbCA9IG5leHRMZXZlbDtcbiAgICAgICAgICAgIHJldHVybiBpc09ubHlNYXhMZXZlbDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbn1cblxuXG4iXX0=