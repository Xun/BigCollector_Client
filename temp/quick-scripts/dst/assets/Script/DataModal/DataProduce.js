
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataProduce.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cb104U/zSdKjZtkItAsSNb3', 'DataProduce');
// Script/DataModal/DataProduce.ts

"use strict";
// import Long = require("Long");
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProducesJson = exports.DataCurBuyProduce = void 0;
var DataCurBuyProduce = /** @class */ (function () {
    function DataCurBuyProduce() {
        //ID
        this._id = '0';
        //名字
        this._name = '0';
        //图片
        this._img = '0';
        //基础购买价格
        this._buyPrice = 0;
    }
    Object.defineProperty(DataCurBuyProduce.prototype, "id", {
        get: function () {
            if (this._id === undefined) {
                this._id = "0";
            }
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCurBuyProduce.prototype, "name", {
        get: function () {
            if (this._name === undefined) {
                this._name = "0";
            }
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCurBuyProduce.prototype, "img", {
        get: function () {
            if (this._img === undefined) {
                this._img = "0";
            }
            return this._img;
        },
        set: function (value) {
            this._img = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCurBuyProduce.prototype, "buyPrice", {
        get: function () {
            if (this._buyPrice === undefined) {
                this._buyPrice = 0;
            }
            return this._buyPrice;
        },
        set: function (value) {
            this._buyPrice = value;
        },
        enumerable: false,
        configurable: true
    });
    return DataCurBuyProduce;
}());
exports.DataCurBuyProduce = DataCurBuyProduce;
var ProducesJson = /** @class */ (function () {
    function ProducesJson() {
    }
    return ProducesJson;
}());
exports.ProducesJson = ProducesJson;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFQcm9kdWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxpQ0FBaUM7OztBQUVqQztJQUFBO1FBRUksSUFBSTtRQUNJLFFBQUcsR0FBVyxHQUFHLENBQUM7UUFXMUIsSUFBSTtRQUNJLFVBQUssR0FBVyxHQUFHLENBQUM7UUFXNUIsSUFBSTtRQUNJLFNBQUksR0FBVyxHQUFHLENBQUM7UUFXM0IsUUFBUTtRQUNBLGNBQVMsR0FBVyxDQUFDLENBQUM7SUFVbEMsQ0FBQztJQTdDRyxzQkFBVyxpQ0FBRTthQUFiO1lBQ0ksSUFBSSxJQUFJLENBQUMsR0FBRyxLQUFLLFNBQVMsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7YUFDbEI7WUFDRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDcEIsQ0FBQzthQUNELFVBQWMsS0FBYTtZQUN2QixJQUFJLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQztRQUNyQixDQUFDOzs7T0FIQTtJQU9ELHNCQUFXLG1DQUFJO2FBQWY7WUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssU0FBUyxFQUFFO2dCQUMxQixJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQzthQUNwQjtZQUNELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDO2FBQ0QsVUFBZ0IsS0FBYTtZQUN6QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUN2QixDQUFDOzs7T0FIQTtJQU9ELHNCQUFXLGtDQUFHO2FBQWQ7WUFDSSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUN6QixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQzthQUNuQjtZQUNELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztRQUNyQixDQUFDO2FBQ0QsVUFBZSxLQUFhO1lBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLENBQUM7OztPQUhBO0lBT0Qsc0JBQVcsdUNBQVE7YUFBbkI7WUFDSSxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO2dCQUM5QixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQzthQUN0QjtZQUNELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDO2FBQ0QsVUFBb0IsS0FBYTtZQUM3QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUMzQixDQUFDOzs7T0FIQTtJQUlMLHdCQUFDO0FBQUQsQ0FqREEsQUFpREMsSUFBQTtBQWpEWSw4Q0FBaUI7QUFtRDlCO0lBQUE7SUFFQSxDQUFDO0lBQUQsbUJBQUM7QUFBRCxDQUZBLEFBRUMsSUFBQTtBQUZZLG9DQUFZIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW1wb3J0IExvbmcgPSByZXF1aXJlKFwiTG9uZ1wiKTtcblxuZXhwb3J0IGNsYXNzIERhdGFDdXJCdXlQcm9kdWNlIHtcblxuICAgIC8vSURcbiAgICBwcml2YXRlIF9pZDogc3RyaW5nID0gJzAnO1xuICAgIHB1YmxpYyBnZXQgaWQoKTogc3RyaW5nIHtcbiAgICAgICAgaWYgKHRoaXMuX2lkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX2lkID0gXCIwXCI7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX2lkO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IGlkKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5faWQgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvL+WQjeWtl1xuICAgIHByaXZhdGUgX25hbWU6IHN0cmluZyA9ICcwJztcbiAgICBwdWJsaWMgZ2V0IG5hbWUoKTogc3RyaW5nIHtcbiAgICAgICAgaWYgKHRoaXMuX25hbWUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fbmFtZSA9IFwiMFwiO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9uYW1lO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IG5hbWUodmFsdWU6IHN0cmluZykge1xuICAgICAgICB0aGlzLl9uYW1lID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLy/lm77niYdcbiAgICBwcml2YXRlIF9pbWc6IHN0cmluZyA9ICcwJztcbiAgICBwdWJsaWMgZ2V0IGltZygpOiBzdHJpbmcge1xuICAgICAgICBpZiAodGhpcy5faW1nID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX2ltZyA9IFwiMFwiO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9pbWc7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgaW1nKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5faW1nID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLy/ln7rnoYDotK3kubDku7fmoLxcbiAgICBwcml2YXRlIF9idXlQcmljZTogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IGJ1eVByaWNlKCk6IG51bWJlciB7XG4gICAgICAgIGlmICh0aGlzLl9idXlQcmljZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl9idXlQcmljZSA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX2J1eVByaWNlO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IGJ1eVByaWNlKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fYnV5UHJpY2UgPSB2YWx1ZTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBQcm9kdWNlc0pzb24ge1xuICAgIGl0ZW1zSW5mbzogY2MuSnNvbkFzc2V0O1xufVxuIl19