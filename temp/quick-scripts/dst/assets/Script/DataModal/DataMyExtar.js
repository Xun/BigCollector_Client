
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataMyExtar.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0f0afHSrQJNkrg6A0LbouVY', 'DataMyExtar');
// Script/DataModal/DataMyExtar.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataMyExtar = void 0;
var DataMyExtar = /** @class */ (function () {
    function DataMyExtar() {
        this.today_extar_get_money = 0;
        this.today_extar_add_user = 0;
        this.month_extar_get_money = 0;
        this.month_extar_add_user = 0;
        this.total_extar_get_money = 0;
        this.total_extar_add_user = 0;
        this.cur_stage = "";
        this.cur_stage_money = 0;
        this.total_stage_moner = 0;
        this.add_mult = 0;
        this.complete_money = 0;
    }
    return DataMyExtar;
}());
exports.DataMyExtar = DataMyExtar;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFNeUV4dGFyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7UUFDVywwQkFBcUIsR0FBVyxDQUFDLENBQUM7UUFDbEMseUJBQW9CLEdBQVcsQ0FBQyxDQUFDO1FBQ2pDLDBCQUFxQixHQUFXLENBQUMsQ0FBQztRQUNsQyx5QkFBb0IsR0FBVyxDQUFDLENBQUM7UUFDakMsMEJBQXFCLEdBQVcsQ0FBQyxDQUFDO1FBQ2xDLHlCQUFvQixHQUFXLENBQUMsQ0FBQztRQUNqQyxjQUFTLEdBQVcsRUFBRSxDQUFDO1FBQ3ZCLG9CQUFlLEdBQVcsQ0FBQyxDQUFDO1FBQzVCLHNCQUFpQixHQUFXLENBQUMsQ0FBQztRQUM5QixhQUFRLEdBQVcsQ0FBQyxDQUFDO1FBQ3JCLG1CQUFjLEdBQVcsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFBRCxrQkFBQztBQUFELENBWkEsQUFZQyxJQUFBO0FBWlksa0NBQVciLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgRGF0YU15RXh0YXIge1xuICAgIHB1YmxpYyB0b2RheV9leHRhcl9nZXRfbW9uZXk6IG51bWJlciA9IDA7XG4gICAgcHVibGljIHRvZGF5X2V4dGFyX2FkZF91c2VyOiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBtb250aF9leHRhcl9nZXRfbW9uZXk6IG51bWJlciA9IDA7XG4gICAgcHVibGljIG1vbnRoX2V4dGFyX2FkZF91c2VyOiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyB0b3RhbF9leHRhcl9nZXRfbW9uZXk6IG51bWJlciA9IDA7XG4gICAgcHVibGljIHRvdGFsX2V4dGFyX2FkZF91c2VyOiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBjdXJfc3RhZ2U6IHN0cmluZyA9IFwiXCI7XG4gICAgcHVibGljIGN1cl9zdGFnZV9tb25leTogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgdG90YWxfc3RhZ2VfbW9uZXI6IG51bWJlciA9IDA7XG4gICAgcHVibGljIGFkZF9tdWx0OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBjb21wbGV0ZV9tb25leTogbnVtYmVyID0gMDtcbn0iXX0=