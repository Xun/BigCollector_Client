
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataRank.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f9635cnR6JOc6PhThV/LkS/', 'DataRank');
// Script/DataModal/DataRank.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataRankItem = exports.DataRank = void 0;
var DataRank = /** @class */ (function () {
    function DataRank() {
        this.rank_list = Array();
    }
    return DataRank;
}());
exports.DataRank = DataRank;
var DataRankItem = /** @class */ (function () {
    function DataRankItem() {
        this.uid = "";
        this.name = "";
        this.money = 0;
        this.picUrl = "";
    }
    return DataRankItem;
}());
exports.DataRankItem = DataRankItem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFSYW5rLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7UUFDVyxjQUFTLEdBQW1CLEtBQUssRUFBZ0IsQ0FBQztJQUM3RCxDQUFDO0lBQUQsZUFBQztBQUFELENBRkEsQUFFQyxJQUFBO0FBRlksNEJBQVE7QUFJckI7SUFBQTtRQUNXLFFBQUcsR0FBVyxFQUFFLENBQUM7UUFDakIsU0FBSSxHQUFXLEVBQUUsQ0FBQztRQUNsQixVQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQU0sR0FBVyxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUFELG1CQUFDO0FBQUQsQ0FMQSxBQUtDLElBQUE7QUFMWSxvQ0FBWSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBEYXRhUmFuayB7XG4gICAgcHVibGljIHJhbmtfbGlzdDogRGF0YVJhbmtJdGVtW10gPSBBcnJheTxEYXRhUmFua0l0ZW0+KCk7XG59XG5cbmV4cG9ydCBjbGFzcyBEYXRhUmFua0l0ZW0ge1xuICAgIHB1YmxpYyB1aWQ6IHN0cmluZyA9IFwiXCI7XG4gICAgcHVibGljIG5hbWU6IHN0cmluZyA9IFwiXCI7XG4gICAgcHVibGljIG1vbmV5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBwaWNVcmw6IHN0cmluZyA9IFwiXCI7XG59Il19