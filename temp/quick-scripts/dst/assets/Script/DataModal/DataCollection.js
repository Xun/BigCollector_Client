
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataCollection.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5e849aoGq9Gf5KX+/JssUhF', 'DataCollection');
// Script/DataModal/DataCollection.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataCollection = void 0;
var DataCollection = /** @class */ (function () {
    function DataCollection() {
        //文房四宝
        this._curWFSBProgress = 0;
        this._WFSBGetMoney = 0;
        //四君子
        this._curSJZProgress = 0;
        this._SJZGetMoney = 0;
        //四大名著
        this._curSDMZProgress = 0;
        this._SDMZGetMoney = 0;
        //四大神兽
        this._curSDSSProgress = 0;
        this._SDSSGetMoney = 0;
        //唐宋八大家
        this._curBDJProgress = 0;
        this._BDJGetMoney = 0;
        //金蟾
        this._curJCProgress = 0;
        this._JCDescribe = "";
        //貔貅
        this._curPXProgress = 0;
        this._PXDescribe = "";
        //金龙
        this._curJLProgress = 0;
        this._JLDescribe = "";
    }
    Object.defineProperty(DataCollection.prototype, "curWFSBProgress", {
        get: function () {
            if (this._curWFSBProgress === undefined) {
                this._curWFSBProgress = 0;
            }
            return this._curWFSBProgress;
        },
        set: function (value) {
            this._curWFSBProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "WFSBGetMoney", {
        get: function () {
            if (this._WFSBGetMoney === undefined) {
                this._WFSBGetMoney = 0;
            }
            return this._WFSBGetMoney;
        },
        set: function (value) {
            this._WFSBGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curSJZProgress", {
        get: function () {
            if (this._curSJZProgress === undefined) {
                this._curSJZProgress = 0;
            }
            return this._curSJZProgress;
        },
        set: function (value) {
            this._curSJZProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "SJZGetMoney", {
        get: function () {
            if (this._SJZGetMoney === undefined) {
                this._SJZGetMoney = 0;
            }
            return this._SJZGetMoney;
        },
        set: function (value) {
            this._SJZGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curSDMZProgress", {
        get: function () {
            if (this._curSDMZProgress === undefined) {
                this._curSDMZProgress = 0;
            }
            return this._curSDMZProgress;
        },
        set: function (value) {
            this._curSDMZProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "SDMZGetMoney", {
        get: function () {
            if (this._SDMZGetMoney === undefined) {
                this._SDMZGetMoney = 0;
            }
            return this._SDMZGetMoney;
        },
        set: function (value) {
            this._SDMZGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curSDSSProgress", {
        get: function () {
            if (this._curSDSSProgress === undefined) {
                this._curSDSSProgress = 0;
            }
            return this._curSDSSProgress;
        },
        set: function (value) {
            this._curSDSSProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "SDSSGetMoney", {
        get: function () {
            if (this._SDSSGetMoney === undefined) {
                this._SDSSGetMoney = 0;
            }
            return this._SDSSGetMoney;
        },
        set: function (value) {
            this._SDSSGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curBDJProgress", {
        get: function () {
            if (this._curBDJProgress === undefined) {
                this._curBDJProgress = 0;
            }
            return this._curBDJProgress;
        },
        set: function (value) {
            this._curBDJProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "BDJGetMoney", {
        get: function () {
            if (this._BDJGetMoney === undefined) {
                this._BDJGetMoney = 0;
            }
            return this._BDJGetMoney;
        },
        set: function (value) {
            this._BDJGetMoney = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curJCProgress", {
        get: function () {
            if (this._curJCProgress === undefined) {
                this._curJCProgress = 0;
            }
            return this._curJCProgress;
        },
        set: function (value) {
            this._curJCProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "JCDescribe", {
        get: function () {
            if (this._JCDescribe === undefined) {
                this._JCDescribe = "";
            }
            return this._JCDescribe;
        },
        set: function (value) {
            this._JCDescribe = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curPXProgress", {
        get: function () {
            if (this._curPXProgress === undefined) {
                this._curPXProgress = 0;
            }
            return this._curPXProgress;
        },
        set: function (value) {
            this._curPXProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "PXDescribe", {
        get: function () {
            if (this._PXDescribe === undefined) {
                this._PXDescribe = "";
            }
            return this._PXDescribe;
        },
        set: function (value) {
            this._PXDescribe = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "curJLProgress", {
        get: function () {
            if (this._curJLProgress === undefined) {
                this._curJLProgress = 0;
            }
            return this._curJLProgress;
        },
        set: function (value) {
            this._curJLProgress = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCollection.prototype, "JLDescribe", {
        get: function () {
            if (this._JLDescribe === undefined) {
                this._JLDescribe = "";
            }
            return this._JLDescribe;
        },
        set: function (value) {
            this._JLDescribe = value;
        },
        enumerable: false,
        configurable: true
    });
    return DataCollection;
}());
exports.DataCollection = DataCollection;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFDb2xsZWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0lBQUE7UUFDSSxNQUFNO1FBQ0UscUJBQWdCLEdBQVcsQ0FBQyxDQUFDO1FBVzdCLGtCQUFhLEdBQVcsQ0FBQyxDQUFDO1FBV2xDLEtBQUs7UUFDRyxvQkFBZSxHQUFXLENBQUMsQ0FBQztRQVc1QixpQkFBWSxHQUFXLENBQUMsQ0FBQztRQVdqQyxNQUFNO1FBQ0UscUJBQWdCLEdBQVcsQ0FBQyxDQUFDO1FBVzdCLGtCQUFhLEdBQVcsQ0FBQyxDQUFDO1FBV2xDLE1BQU07UUFDRSxxQkFBZ0IsR0FBVyxDQUFDLENBQUM7UUFXN0Isa0JBQWEsR0FBVyxDQUFDLENBQUM7UUFXbEMsT0FBTztRQUNDLG9CQUFlLEdBQVcsQ0FBQyxDQUFDO1FBVzVCLGlCQUFZLEdBQVcsQ0FBQyxDQUFDO1FBV2pDLElBQUk7UUFDSSxtQkFBYyxHQUFXLENBQUMsQ0FBQztRQVczQixnQkFBVyxHQUFXLEVBQUUsQ0FBQztRQVdqQyxJQUFJO1FBQ0ksbUJBQWMsR0FBVyxDQUFDLENBQUM7UUFXM0IsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFXakMsSUFBSTtRQUNJLG1CQUFjLEdBQVcsQ0FBQyxDQUFDO1FBVzNCLGdCQUFXLEdBQVcsRUFBRSxDQUFDO0lBVXJDLENBQUM7SUFyTEcsc0JBQVcsMkNBQWU7YUFBMUI7WUFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxTQUFTLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7YUFDN0I7WUFDRCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztRQUNqQyxDQUFDO2FBQ0QsVUFBMkIsS0FBYTtZQUNwQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLENBQUM7OztPQUhBO0lBTUQsc0JBQVcsd0NBQVk7YUFBdkI7WUFDSSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssU0FBUyxFQUFFO2dCQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQzthQUMxQjtZQUNELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM5QixDQUFDO2FBQ0QsVUFBd0IsS0FBYTtZQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMvQixDQUFDOzs7T0FIQTtJQU9ELHNCQUFXLDBDQUFjO2FBQXpCO1lBQ0ksSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLFNBQVMsRUFBRTtnQkFDcEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUM7YUFDNUI7WUFDRCxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7UUFDaEMsQ0FBQzthQUNELFVBQTBCLEtBQWE7WUFDbkMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDakMsQ0FBQzs7O09BSEE7SUFNRCxzQkFBVyx1Q0FBVzthQUF0QjtZQUNJLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxTQUFTLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO2FBQ3pCO1lBQ0QsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzdCLENBQUM7YUFDRCxVQUF1QixLQUFhO1lBQ2hDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzlCLENBQUM7OztPQUhBO0lBT0Qsc0JBQVcsMkNBQWU7YUFBMUI7WUFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxTQUFTLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7YUFDN0I7WUFDRCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztRQUNqQyxDQUFDO2FBQ0QsVUFBMkIsS0FBYTtZQUNwQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ2xDLENBQUM7OztPQUhBO0lBTUQsc0JBQVcsd0NBQVk7YUFBdkI7WUFDSSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssU0FBUyxFQUFFO2dCQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQzthQUMxQjtZQUNELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM5QixDQUFDO2FBQ0QsVUFBd0IsS0FBYTtZQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMvQixDQUFDOzs7T0FIQTtJQU9ELHNCQUFXLDJDQUFlO2FBQTFCO1lBQ0ksSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssU0FBUyxFQUFFO2dCQUNyQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO2FBQzdCO1lBQ0QsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDakMsQ0FBQzthQUNELFVBQTJCLEtBQWE7WUFDcEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUNsQyxDQUFDOzs7T0FIQTtJQU1ELHNCQUFXLHdDQUFZO2FBQXZCO1lBQ0ksSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLFNBQVMsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7YUFDMUI7WUFDRCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDOUIsQ0FBQzthQUNELFVBQXdCLEtBQWE7WUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDL0IsQ0FBQzs7O09BSEE7SUFPRCxzQkFBVywwQ0FBYzthQUF6QjtZQUNJLElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxTQUFTLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDO2FBQzVCO1lBQ0QsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQ2hDLENBQUM7YUFDRCxVQUEwQixLQUFhO1lBQ25DLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQ2pDLENBQUM7OztPQUhBO0lBTUQsc0JBQVcsdUNBQVc7YUFBdEI7WUFDSSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssU0FBUyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQzthQUN6QjtZQUNELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztRQUM3QixDQUFDO2FBQ0QsVUFBdUIsS0FBYTtZQUNoQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDOzs7T0FIQTtJQU9ELHNCQUFXLHlDQUFhO2FBQXhCO1lBQ0ksSUFBSSxJQUFJLENBQUMsY0FBYyxLQUFLLFNBQVMsRUFBRTtnQkFDbkMsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7YUFDM0I7WUFDRCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDL0IsQ0FBQzthQUNELFVBQXlCLEtBQWE7WUFDbEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDaEMsQ0FBQzs7O09BSEE7SUFNRCxzQkFBVyxzQ0FBVTthQUFyQjtZQUNJLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxTQUFTLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2FBQ3pCO1lBQ0QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVCLENBQUM7YUFDRCxVQUFzQixLQUFhO1lBQy9CLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQzdCLENBQUM7OztPQUhBO0lBT0Qsc0JBQVcseUNBQWE7YUFBeEI7WUFDSSxJQUFJLElBQUksQ0FBQyxjQUFjLEtBQUssU0FBUyxFQUFFO2dCQUNuQyxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQzthQUMzQjtZQUNELE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUMvQixDQUFDO2FBQ0QsVUFBeUIsS0FBYTtZQUNsQyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUNoQyxDQUFDOzs7T0FIQTtJQU1ELHNCQUFXLHNDQUFVO2FBQXJCO1lBQ0ksSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLFNBQVMsRUFBRTtnQkFDaEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7YUFDekI7WUFDRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDNUIsQ0FBQzthQUNELFVBQXNCLEtBQWE7WUFDL0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDN0IsQ0FBQzs7O09BSEE7SUFPRCxzQkFBVyx5Q0FBYTthQUF4QjtZQUNJLElBQUksSUFBSSxDQUFDLGNBQWMsS0FBSyxTQUFTLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDO2FBQzNCO1lBQ0QsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQy9CLENBQUM7YUFDRCxVQUF5QixLQUFhO1lBQ2xDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLENBQUM7OztPQUhBO0lBTUQsc0JBQVcsc0NBQVU7YUFBckI7WUFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssU0FBUyxFQUFFO2dCQUNoQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQzthQUN6QjtZQUNELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM1QixDQUFDO2FBQ0QsVUFBc0IsS0FBYTtZQUMvQixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUM3QixDQUFDOzs7T0FIQTtJQUlMLHFCQUFDO0FBQUQsQ0F4TEEsQUF3TEMsSUFBQTtBQXhMWSx3Q0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBEYXRhQ29sbGVjdGlvbiB7XG4gICAgLy/mlofmiL/lm5vlrp1cbiAgICBwcml2YXRlIF9jdXJXRlNCUHJvZ3Jlc3M6IG51bWJlciA9IDA7XG4gICAgcHVibGljIGdldCBjdXJXRlNCUHJvZ3Jlc3MoKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuX2N1cldGU0JQcm9ncmVzcyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl9jdXJXRlNCUHJvZ3Jlc3MgPSAwO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9jdXJXRlNCUHJvZ3Jlc3M7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgY3VyV0ZTQlByb2dyZXNzKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fY3VyV0ZTQlByb2dyZXNzID0gdmFsdWU7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfV0ZTQkdldE1vbmV5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBnZXQgV0ZTQkdldE1vbmV5KCk6IG51bWJlciB7XG4gICAgICAgIGlmICh0aGlzLl9XRlNCR2V0TW9uZXkgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fV0ZTQkdldE1vbmV5ID0gMDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fV0ZTQkdldE1vbmV5O1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IFdGU0JHZXRNb25leSh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX1dGU0JHZXRNb25leSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8v5Zub5ZCb5a2QXG4gICAgcHJpdmF0ZSBfY3VyU0paUHJvZ3Jlc3M6IG51bWJlciA9IDA7XG4gICAgcHVibGljIGdldCBjdXJTSlpQcm9ncmVzcygpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5fY3VyU0paUHJvZ3Jlc3MgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fY3VyU0paUHJvZ3Jlc3MgPSAwO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9jdXJTSlpQcm9ncmVzcztcbiAgICB9XG4gICAgcHVibGljIHNldCBjdXJTSlpQcm9ncmVzcyh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX2N1clNKWlByb2dyZXNzID0gdmFsdWU7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfU0paR2V0TW9uZXk6IG51bWJlciA9IDA7XG4gICAgcHVibGljIGdldCBTSlpHZXRNb25leSgpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5fU0paR2V0TW9uZXkgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fU0paR2V0TW9uZXkgPSAwO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9TSlpHZXRNb25leTtcbiAgICB9XG4gICAgcHVibGljIHNldCBTSlpHZXRNb25leSh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX1NKWkdldE1vbmV5ID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLy/lm5vlpKflkI3okZdcbiAgICBwcml2YXRlIF9jdXJTRE1aUHJvZ3Jlc3M6IG51bWJlciA9IDA7XG4gICAgcHVibGljIGdldCBjdXJTRE1aUHJvZ3Jlc3MoKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuX2N1clNETVpQcm9ncmVzcyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl9jdXJTRE1aUHJvZ3Jlc3MgPSAwO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9jdXJTRE1aUHJvZ3Jlc3M7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgY3VyU0RNWlByb2dyZXNzKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fY3VyU0RNWlByb2dyZXNzID0gdmFsdWU7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfU0RNWkdldE1vbmV5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBnZXQgU0RNWkdldE1vbmV5KCk6IG51bWJlciB7XG4gICAgICAgIGlmICh0aGlzLl9TRE1aR2V0TW9uZXkgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fU0RNWkdldE1vbmV5ID0gMDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fU0RNWkdldE1vbmV5O1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IFNETVpHZXRNb25leSh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX1NETVpHZXRNb25leSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8v5Zub5aSn56We5YW9XG4gICAgcHJpdmF0ZSBfY3VyU0RTU1Byb2dyZXNzOiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBnZXQgY3VyU0RTU1Byb2dyZXNzKCk6IG51bWJlciB7XG4gICAgICAgIGlmICh0aGlzLl9jdXJTRFNTUHJvZ3Jlc3MgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fY3VyU0RTU1Byb2dyZXNzID0gMDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fY3VyU0RTU1Byb2dyZXNzO1xuICAgIH1cbiAgICBwdWJsaWMgc2V0IGN1clNEU1NQcm9ncmVzcyh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX2N1clNEU1NQcm9ncmVzcyA9IHZhbHVlO1xuICAgIH1cblxuICAgIHByaXZhdGUgX1NEU1NHZXRNb25leTogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IFNEU1NHZXRNb25leSgpOiBudW1iZXIge1xuICAgICAgICBpZiAodGhpcy5fU0RTU0dldE1vbmV5ID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX1NEU1NHZXRNb25leSA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX1NEU1NHZXRNb25leTtcbiAgICB9XG4gICAgcHVibGljIHNldCBTRFNTR2V0TW9uZXkodmFsdWU6IG51bWJlcikge1xuICAgICAgICB0aGlzLl9TRFNTR2V0TW9uZXkgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvL+WUkOWui+WFq+Wkp+WutlxuICAgIHByaXZhdGUgX2N1ckJESlByb2dyZXNzOiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBnZXQgY3VyQkRKUHJvZ3Jlc3MoKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuX2N1ckJESlByb2dyZXNzID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX2N1ckJESlByb2dyZXNzID0gMDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fY3VyQkRKUHJvZ3Jlc3M7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgY3VyQkRKUHJvZ3Jlc3ModmFsdWU6IG51bWJlcikge1xuICAgICAgICB0aGlzLl9jdXJCREpQcm9ncmVzcyA9IHZhbHVlO1xuICAgIH1cblxuICAgIHByaXZhdGUgX0JESkdldE1vbmV5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBnZXQgQkRKR2V0TW9uZXkoKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuX0JESkdldE1vbmV5ID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX0JESkdldE1vbmV5ID0gMDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fQkRKR2V0TW9uZXk7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgQkRKR2V0TW9uZXkodmFsdWU6IG51bWJlcikge1xuICAgICAgICB0aGlzLl9CREpHZXRNb25leSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8v6YeR6J++XG4gICAgcHJpdmF0ZSBfY3VySkNQcm9ncmVzczogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IGN1ckpDUHJvZ3Jlc3MoKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuX2N1ckpDUHJvZ3Jlc3MgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fY3VySkNQcm9ncmVzcyA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX2N1ckpDUHJvZ3Jlc3M7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgY3VySkNQcm9ncmVzcyh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX2N1ckpDUHJvZ3Jlc3MgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9KQ0Rlc2NyaWJlOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBnZXQgSkNEZXNjcmliZSgpOiBzdHJpbmcge1xuICAgICAgICBpZiAodGhpcy5fSkNEZXNjcmliZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl9KQ0Rlc2NyaWJlID0gXCJcIjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fSkNEZXNjcmliZTtcbiAgICB9XG4gICAgcHVibGljIHNldCBKQ0Rlc2NyaWJlKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fSkNEZXNjcmliZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8v6LKU6LKFXG4gICAgcHJpdmF0ZSBfY3VyUFhQcm9ncmVzczogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IGN1clBYUHJvZ3Jlc3MoKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuX2N1clBYUHJvZ3Jlc3MgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fY3VyUFhQcm9ncmVzcyA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX2N1clBYUHJvZ3Jlc3M7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgY3VyUFhQcm9ncmVzcyh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX2N1clBYUHJvZ3Jlc3MgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9QWERlc2NyaWJlOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBnZXQgUFhEZXNjcmliZSgpOiBzdHJpbmcge1xuICAgICAgICBpZiAodGhpcy5fUFhEZXNjcmliZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl9QWERlc2NyaWJlID0gXCJcIjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fUFhEZXNjcmliZTtcbiAgICB9XG4gICAgcHVibGljIHNldCBQWERlc2NyaWJlKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fUFhEZXNjcmliZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8v6YeR6b6ZXG4gICAgcHJpdmF0ZSBfY3VySkxQcm9ncmVzczogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgZ2V0IGN1ckpMUHJvZ3Jlc3MoKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuX2N1ckpMUHJvZ3Jlc3MgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdGhpcy5fY3VySkxQcm9ncmVzcyA9IDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuX2N1ckpMUHJvZ3Jlc3M7XG4gICAgfVxuICAgIHB1YmxpYyBzZXQgY3VySkxQcm9ncmVzcyh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX2N1ckpMUHJvZ3Jlc3MgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9KTERlc2NyaWJlOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBnZXQgSkxEZXNjcmliZSgpOiBzdHJpbmcge1xuICAgICAgICBpZiAodGhpcy5fSkxEZXNjcmliZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLl9KTERlc2NyaWJlID0gXCJcIjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fSkxEZXNjcmliZTtcbiAgICB9XG4gICAgcHVibGljIHNldCBKTERlc2NyaWJlKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fSkxEZXNjcmliZSA9IHZhbHVlO1xuICAgIH1cbn0iXX0=