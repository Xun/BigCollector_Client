
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataBag.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '52db2OIKg1MD6nep22RXot2', 'DataBag');
// Script/DataModal/DataBag.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataBagItem = exports.DataBagInfo = void 0;
var DataBagInfo = /** @class */ (function () {
    function DataBagInfo() {
        this.list_item = Array();
        this.cur_bag_capacity = 0;
        this.total_bag_capacity = 6;
    }
    return DataBagInfo;
}());
exports.DataBagInfo = DataBagInfo;
var DataBagItem = /** @class */ (function () {
    function DataBagItem() {
        this.itemID = 0;
        this.itemCount = 0;
    }
    return DataBagItem;
}());
exports.DataBagItem = DataBagItem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFCYWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7SUFBQTtRQUNXLGNBQVMsR0FBa0IsS0FBSyxFQUFlLENBQUM7UUFDaEQscUJBQWdCLEdBQVcsQ0FBQyxDQUFDO1FBQzdCLHVCQUFrQixHQUFXLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBQUQsa0JBQUM7QUFBRCxDQUpBLEFBSUMsSUFBQTtBQUpZLGtDQUFXO0FBTXhCO0lBQUE7UUFDVyxXQUFNLEdBQVcsQ0FBQyxDQUFDO1FBQ25CLGNBQVMsR0FBVyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUFELGtCQUFDO0FBQUQsQ0FIQSxBQUdDLElBQUE7QUFIWSxrQ0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBEYXRhQmFnSW5mbyB7XG4gICAgcHVibGljIGxpc3RfaXRlbTogRGF0YUJhZ0l0ZW1bXSA9IEFycmF5PERhdGFCYWdJdGVtPigpO1xuICAgIHB1YmxpYyBjdXJfYmFnX2NhcGFjaXR5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyB0b3RhbF9iYWdfY2FwYWNpdHk6IG51bWJlciA9IDY7XG59XG5cbmV4cG9ydCBjbGFzcyBEYXRhQmFnSXRlbSB7XG4gICAgcHVibGljIGl0ZW1JRDogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgaXRlbUNvdW50OiBudW1iZXIgPSAwO1xufSJdfQ==