
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataGetMoneyLog.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8fdd6kyxBJPu53TUeDnNh5t', 'DataGetMoneyLog');
// Script/DataModal/DataGetMoneyLog.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataItemGetMoneyLog = exports.DataGetMoneyLog = void 0;
var DataGetMoneyLog = /** @class */ (function () {
    function DataGetMoneyLog() {
        this.list_get_money_log = Array();
    }
    return DataGetMoneyLog;
}());
exports.DataGetMoneyLog = DataGetMoneyLog;
var DataItemGetMoneyLog = /** @class */ (function () {
    function DataItemGetMoneyLog() {
        this.get_data = "";
        this.get_money_num = 0;
    }
    return DataItemGetMoneyLog;
}());
exports.DataItemGetMoneyLog = DataItemGetMoneyLog;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFHZXRNb25leUxvZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtJQUFBO1FBQ1csdUJBQWtCLEdBQTBCLEtBQUssRUFBdUIsQ0FBQztJQUNwRixDQUFDO0lBQUQsc0JBQUM7QUFBRCxDQUZBLEFBRUMsSUFBQTtBQUZZLDBDQUFlO0FBSTVCO0lBQUE7UUFDVyxhQUFRLEdBQVcsRUFBRSxDQUFDO1FBQ3RCLGtCQUFhLEdBQVcsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFBRCwwQkFBQztBQUFELENBSEEsQUFHQyxJQUFBO0FBSFksa0RBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIERhdGFHZXRNb25leUxvZyB7XG4gICAgcHVibGljIGxpc3RfZ2V0X21vbmV5X2xvZzogRGF0YUl0ZW1HZXRNb25leUxvZ1tdID0gQXJyYXk8RGF0YUl0ZW1HZXRNb25leUxvZz4oKTtcbn1cblxuZXhwb3J0IGNsYXNzIERhdGFJdGVtR2V0TW9uZXlMb2cge1xuICAgIHB1YmxpYyBnZXRfZGF0YTogc3RyaW5nID0gXCJcIjtcbiAgICBwdWJsaWMgZ2V0X21vbmV5X251bTogbnVtYmVyID0gMDtcbn0iXX0=