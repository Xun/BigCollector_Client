
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Script/DataModal/DataFriend.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a9d27ygiTFIVKNNK8n/l1hH', 'DataFriend');
// Script/DataModal/DataFriend.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataItemFriendInfo = exports.DataItemSeeDetail = exports.DataSeeDetail = exports.DataItemFriendGiveDetail = exports.DataFriendGiveDetail = exports.DataItemFriendMoneyDetail = exports.DataFriendMoneyDetail = exports.DataFriend = void 0;
var DataFriend = /** @class */ (function () {
    function DataFriend() {
        this.my_friend_num = 0;
        this.probabyly_money = 0;
        this.today_get_money = 0;
        this.total_get_money = 0;
        this.today_extar_get_money = 0;
        this.cur_get_money = 0;
        this.my_inviter_name = "";
        this.my_invite_head = "";
    }
    return DataFriend;
}());
exports.DataFriend = DataFriend;
var DataFriendMoneyDetail = /** @class */ (function () {
    function DataFriendMoneyDetail() {
        this.today_get_money = 0;
        this.total_get_money = 0;
        this.list_item_friend_money_detail = Array();
    }
    return DataFriendMoneyDetail;
}());
exports.DataFriendMoneyDetail = DataFriendMoneyDetail;
var DataItemFriendMoneyDetail = /** @class */ (function () {
    function DataItemFriendMoneyDetail() {
        this.time = 0;
        this.oneFriendEranings = 0;
        this.twoFriendEranings = 0;
        this.todayTotalEranings = 0;
    }
    return DataItemFriendMoneyDetail;
}());
exports.DataItemFriendMoneyDetail = DataItemFriendMoneyDetail;
var DataFriendGiveDetail = /** @class */ (function () {
    function DataFriendGiveDetail() {
        this.list_item_friend_give_detail = Array();
    }
    return DataFriendGiveDetail;
}());
exports.DataFriendGiveDetail = DataFriendGiveDetail;
var DataItemFriendGiveDetail = /** @class */ (function () {
    function DataItemFriendGiveDetail() {
        this.user_name = "";
        this.user_id = 0;
        this.user_head = "";
        this.give_time = "";
        this.give_num = 0;
    }
    return DataItemFriendGiveDetail;
}());
exports.DataItemFriendGiveDetail = DataItemFriendGiveDetail;
var DataSeeDetail = /** @class */ (function () {
    function DataSeeDetail() {
        this.list_item_friend_give_detail = Array();
    }
    return DataSeeDetail;
}());
exports.DataSeeDetail = DataSeeDetail;
var DataItemSeeDetail = /** @class */ (function () {
    function DataItemSeeDetail() {
        this.user_name = "";
        this.user_id = 0;
        this.user_head = "";
        this.see_time = "";
    }
    return DataItemSeeDetail;
}());
exports.DataItemSeeDetail = DataItemSeeDetail;
var DataItemFriendInfo = /** @class */ (function () {
    function DataItemFriendInfo() {
        this.user_name = "";
        this.user_level = "";
        this.user_head = "";
        this.add_time = "";
    }
    return DataItemFriendInfo;
}());
exports.DataItemFriendInfo = DataItemFriendInfo;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9TY3JpcHQvRGF0YU1vZGFsL0RhdGFGcmllbmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7SUFBQTtRQUNXLGtCQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG9CQUFlLEdBQVcsQ0FBQyxDQUFDO1FBQzVCLG9CQUFlLEdBQVcsQ0FBQyxDQUFDO1FBQzVCLG9CQUFlLEdBQVcsQ0FBQyxDQUFDO1FBQzVCLDBCQUFxQixHQUFXLENBQUMsQ0FBQztRQUNsQyxrQkFBYSxHQUFXLENBQUMsQ0FBQztRQUMxQixvQkFBZSxHQUFXLEVBQUUsQ0FBQztRQUM3QixtQkFBYyxHQUFXLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBQUQsaUJBQUM7QUFBRCxDQVRBLEFBU0MsSUFBQTtBQVRZLGdDQUFVO0FBV3ZCO0lBQUE7UUFDVyxvQkFBZSxHQUFXLENBQUMsQ0FBQztRQUM1QixvQkFBZSxHQUFXLENBQUMsQ0FBQztRQUM1QixrQ0FBNkIsR0FBZ0MsS0FBSyxFQUE2QixDQUFDO0lBQzNHLENBQUM7SUFBRCw0QkFBQztBQUFELENBSkEsQUFJQyxJQUFBO0FBSlksc0RBQXFCO0FBTWxDO0lBQUE7UUFDVyxTQUFJLEdBQVcsQ0FBQyxDQUFDO1FBQ2pCLHNCQUFpQixHQUFXLENBQUMsQ0FBQztRQUM5QixzQkFBaUIsR0FBVyxDQUFDLENBQUM7UUFDOUIsdUJBQWtCLEdBQVcsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFBRCxnQ0FBQztBQUFELENBTEEsQUFLQyxJQUFBO0FBTFksOERBQXlCO0FBT3RDO0lBQUE7UUFDVyxpQ0FBNEIsR0FBK0IsS0FBSyxFQUE0QixDQUFDO0lBQ3hHLENBQUM7SUFBRCwyQkFBQztBQUFELENBRkEsQUFFQyxJQUFBO0FBRlksb0RBQW9CO0FBS2pDO0lBQUE7UUFDVyxjQUFTLEdBQVcsRUFBRSxDQUFDO1FBQ3ZCLFlBQU8sR0FBVyxDQUFDLENBQUM7UUFDcEIsY0FBUyxHQUFXLEVBQUUsQ0FBQztRQUN2QixjQUFTLEdBQVcsRUFBRSxDQUFDO1FBQ3ZCLGFBQVEsR0FBVyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUFELCtCQUFDO0FBQUQsQ0FOQSxBQU1DLElBQUE7QUFOWSw0REFBd0I7QUFTckM7SUFBQTtRQUNXLGlDQUE0QixHQUF3QixLQUFLLEVBQXFCLENBQUM7SUFDMUYsQ0FBQztJQUFELG9CQUFDO0FBQUQsQ0FGQSxBQUVDLElBQUE7QUFGWSxzQ0FBYTtBQUkxQjtJQUFBO1FBQ1csY0FBUyxHQUFXLEVBQUUsQ0FBQztRQUN2QixZQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3BCLGNBQVMsR0FBVyxFQUFFLENBQUM7UUFDdkIsYUFBUSxHQUFXLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBQUQsd0JBQUM7QUFBRCxDQUxBLEFBS0MsSUFBQTtBQUxZLDhDQUFpQjtBQU85QjtJQUFBO1FBQ1csY0FBUyxHQUFXLEVBQUUsQ0FBQztRQUN2QixlQUFVLEdBQVcsRUFBRSxDQUFDO1FBQ3hCLGNBQVMsR0FBVyxFQUFFLENBQUM7UUFDdkIsYUFBUSxHQUFXLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBQUQseUJBQUM7QUFBRCxDQUxBLEFBS0MsSUFBQTtBQUxZLGdEQUFrQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBEYXRhRnJpZW5kIHtcbiAgICBwdWJsaWMgbXlfZnJpZW5kX251bTogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgcHJvYmFieWx5X21vbmV5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyB0b2RheV9nZXRfbW9uZXk6IG51bWJlciA9IDA7XG4gICAgcHVibGljIHRvdGFsX2dldF9tb25leTogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgdG9kYXlfZXh0YXJfZ2V0X21vbmV5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBjdXJfZ2V0X21vbmV5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyBteV9pbnZpdGVyX25hbWU6IHN0cmluZyA9IFwiXCI7XG4gICAgcHVibGljIG15X2ludml0ZV9oZWFkOiBzdHJpbmcgPSBcIlwiO1xufVxuXG5leHBvcnQgY2xhc3MgRGF0YUZyaWVuZE1vbmV5RGV0YWlsIHtcbiAgICBwdWJsaWMgdG9kYXlfZ2V0X21vbmV5OiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyB0b3RhbF9nZXRfbW9uZXk6IG51bWJlciA9IDA7XG4gICAgcHVibGljIGxpc3RfaXRlbV9mcmllbmRfbW9uZXlfZGV0YWlsOiBEYXRhSXRlbUZyaWVuZE1vbmV5RGV0YWlsW10gPSBBcnJheTxEYXRhSXRlbUZyaWVuZE1vbmV5RGV0YWlsPigpO1xufVxuXG5leHBvcnQgY2xhc3MgRGF0YUl0ZW1GcmllbmRNb25leURldGFpbCB7XG4gICAgcHVibGljIHRpbWU6IG51bWJlciA9IDA7XG4gICAgcHVibGljIG9uZUZyaWVuZEVyYW5pbmdzOiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyB0d29GcmllbmRFcmFuaW5nczogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgdG9kYXlUb3RhbEVyYW5pbmdzOiBudW1iZXIgPSAwO1xufVxuXG5leHBvcnQgY2xhc3MgRGF0YUZyaWVuZEdpdmVEZXRhaWwge1xuICAgIHB1YmxpYyBsaXN0X2l0ZW1fZnJpZW5kX2dpdmVfZGV0YWlsOiBEYXRhSXRlbUZyaWVuZEdpdmVEZXRhaWxbXSA9IEFycmF5PERhdGFJdGVtRnJpZW5kR2l2ZURldGFpbD4oKTtcbn1cblxuXG5leHBvcnQgY2xhc3MgRGF0YUl0ZW1GcmllbmRHaXZlRGV0YWlsIHtcbiAgICBwdWJsaWMgdXNlcl9uYW1lOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyB1c2VyX2lkOiBudW1iZXIgPSAwO1xuICAgIHB1YmxpYyB1c2VyX2hlYWQ6IHN0cmluZyA9IFwiXCI7XG4gICAgcHVibGljIGdpdmVfdGltZTogc3RyaW5nID0gXCJcIjtcbiAgICBwdWJsaWMgZ2l2ZV9udW06IG51bWJlciA9IDA7XG59XG5cblxuZXhwb3J0IGNsYXNzIERhdGFTZWVEZXRhaWwge1xuICAgIHB1YmxpYyBsaXN0X2l0ZW1fZnJpZW5kX2dpdmVfZGV0YWlsOiBEYXRhSXRlbVNlZURldGFpbFtdID0gQXJyYXk8RGF0YUl0ZW1TZWVEZXRhaWw+KCk7XG59XG5cbmV4cG9ydCBjbGFzcyBEYXRhSXRlbVNlZURldGFpbCB7XG4gICAgcHVibGljIHVzZXJfbmFtZTogc3RyaW5nID0gXCJcIjtcbiAgICBwdWJsaWMgdXNlcl9pZDogbnVtYmVyID0gMDtcbiAgICBwdWJsaWMgdXNlcl9oZWFkOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBzZWVfdGltZTogc3RyaW5nID0gXCJcIjtcbn1cblxuZXhwb3J0IGNsYXNzIERhdGFJdGVtRnJpZW5kSW5mbyB7XG4gICAgcHVibGljIHVzZXJfbmFtZTogc3RyaW5nID0gXCJcIjtcbiAgICBwdWJsaWMgdXNlcl9sZXZlbDogc3RyaW5nID0gXCJcIjtcbiAgICBwdWJsaWMgdXNlcl9oZWFkOiBzdHJpbmcgPSBcIlwiO1xuICAgIHB1YmxpYyBhZGRfdGltZTogc3RyaW5nID0gXCJcIjtcbn0iXX0=