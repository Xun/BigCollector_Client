"use strict";
cc._RF.push(module, 'fa57e3VykxJ7pwIODLII/zq', 'bundle');
// Protocol/bundle.js

/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal"); // Common aliases


var $Reader = $protobuf.Reader,
    $Writer = $protobuf.Writer,
    $util = $protobuf.util; // Exported root namespace

var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.InvokeBegin = function () {
  /**
   * Properties of an InvokeBegin.
   * @exports IInvokeBegin
   * @interface IInvokeBegin
   * @property {number|null} [invokeId] InvokeBegin invokeId
   * @property {string|null} [method] InvokeBegin method
   */

  /**
   * Constructs a new InvokeBegin.
   * @exports InvokeBegin
   * @classdesc Represents an InvokeBegin.
   * @implements IInvokeBegin
   * @constructor
   * @param {IInvokeBegin=} [properties] Properties to set
   */
  function InvokeBegin(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * InvokeBegin invokeId.
   * @member {number} invokeId
   * @memberof InvokeBegin
   * @instance
   */


  InvokeBegin.prototype.invokeId = 0;
  /**
   * InvokeBegin method.
   * @member {string} method
   * @memberof InvokeBegin
   * @instance
   */

  InvokeBegin.prototype.method = "";
  /**
   * Creates a new InvokeBegin instance using the specified properties.
   * @function create
   * @memberof InvokeBegin
   * @static
   * @param {IInvokeBegin=} [properties] Properties to set
   * @returns {InvokeBegin} InvokeBegin instance
   */

  InvokeBegin.create = function create(properties) {
    return new InvokeBegin(properties);
  };
  /**
   * Encodes the specified InvokeBegin message. Does not implicitly {@link InvokeBegin.verify|verify} messages.
   * @function encode
   * @memberof InvokeBegin
   * @static
   * @param {IInvokeBegin} message InvokeBegin message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  InvokeBegin.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.invokeId != null && Object.hasOwnProperty.call(message, "invokeId")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.invokeId);
    if (message.method != null && Object.hasOwnProperty.call(message, "method")) writer.uint32(
    /* id 2, wireType 2 =*/
    18).string(message.method);
    return writer;
  };
  /**
   * Encodes the specified InvokeBegin message, length delimited. Does not implicitly {@link InvokeBegin.verify|verify} messages.
   * @function encodeDelimited
   * @memberof InvokeBegin
   * @static
   * @param {IInvokeBegin} message InvokeBegin message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  InvokeBegin.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes an InvokeBegin message from the specified reader or buffer.
   * @function decode
   * @memberof InvokeBegin
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {InvokeBegin} InvokeBegin
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  InvokeBegin.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.InvokeBegin();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.invokeId = reader.int32();
          break;

        case 2:
          message.method = reader.string();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes an InvokeBegin message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof InvokeBegin
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {InvokeBegin} InvokeBegin
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  InvokeBegin.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies an InvokeBegin message.
   * @function verify
   * @memberof InvokeBegin
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  InvokeBegin.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.invokeId != null && message.hasOwnProperty("invokeId")) if (!$util.isInteger(message.invokeId)) return "invokeId: integer expected";
    if (message.method != null && message.hasOwnProperty("method")) if (!$util.isString(message.method)) return "method: string expected";
    return null;
  };
  /**
   * Creates an InvokeBegin message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof InvokeBegin
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {InvokeBegin} InvokeBegin
   */


  InvokeBegin.fromObject = function fromObject(object) {
    if (object instanceof $root.InvokeBegin) return object;
    var message = new $root.InvokeBegin();
    if (object.invokeId != null) message.invokeId = object.invokeId | 0;
    if (object.method != null) message.method = String(object.method);
    return message;
  };
  /**
   * Creates a plain object from an InvokeBegin message. Also converts values to other types if specified.
   * @function toObject
   * @memberof InvokeBegin
   * @static
   * @param {InvokeBegin} message InvokeBegin
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  InvokeBegin.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.invokeId = 0;
      object.method = "";
    }

    if (message.invokeId != null && message.hasOwnProperty("invokeId")) object.invokeId = message.invokeId;
    if (message.method != null && message.hasOwnProperty("method")) object.method = message.method;
    return object;
  };
  /**
   * Converts this InvokeBegin to JSON.
   * @function toJSON
   * @memberof InvokeBegin
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  InvokeBegin.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return InvokeBegin;
}();

$root.InvokeEnd = function () {
  /**
   * Properties of an InvokeEnd.
   * @exports IInvokeEnd
   * @interface IInvokeEnd
   * @property {number|null} [invokeId] InvokeEnd invokeId
   * @property {number|null} [status] InvokeEnd status
   * @property {string|null} [error] InvokeEnd error
   */

  /**
   * Constructs a new InvokeEnd.
   * @exports InvokeEnd
   * @classdesc Represents an InvokeEnd.
   * @implements IInvokeEnd
   * @constructor
   * @param {IInvokeEnd=} [properties] Properties to set
   */
  function InvokeEnd(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * InvokeEnd invokeId.
   * @member {number} invokeId
   * @memberof InvokeEnd
   * @instance
   */


  InvokeEnd.prototype.invokeId = 0;
  /**
   * InvokeEnd status.
   * @member {number} status
   * @memberof InvokeEnd
   * @instance
   */

  InvokeEnd.prototype.status = 0;
  /**
   * InvokeEnd error.
   * @member {string} error
   * @memberof InvokeEnd
   * @instance
   */

  InvokeEnd.prototype.error = "";
  /**
   * Creates a new InvokeEnd instance using the specified properties.
   * @function create
   * @memberof InvokeEnd
   * @static
   * @param {IInvokeEnd=} [properties] Properties to set
   * @returns {InvokeEnd} InvokeEnd instance
   */

  InvokeEnd.create = function create(properties) {
    return new InvokeEnd(properties);
  };
  /**
   * Encodes the specified InvokeEnd message. Does not implicitly {@link InvokeEnd.verify|verify} messages.
   * @function encode
   * @memberof InvokeEnd
   * @static
   * @param {IInvokeEnd} message InvokeEnd message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  InvokeEnd.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.invokeId != null && Object.hasOwnProperty.call(message, "invokeId")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.invokeId);
    if (message.status != null && Object.hasOwnProperty.call(message, "status")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int32(message.status);
    if (message.error != null && Object.hasOwnProperty.call(message, "error")) writer.uint32(
    /* id 3, wireType 2 =*/
    26).string(message.error);
    return writer;
  };
  /**
   * Encodes the specified InvokeEnd message, length delimited. Does not implicitly {@link InvokeEnd.verify|verify} messages.
   * @function encodeDelimited
   * @memberof InvokeEnd
   * @static
   * @param {IInvokeEnd} message InvokeEnd message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  InvokeEnd.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes an InvokeEnd message from the specified reader or buffer.
   * @function decode
   * @memberof InvokeEnd
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {InvokeEnd} InvokeEnd
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  InvokeEnd.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.InvokeEnd();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.invokeId = reader.int32();
          break;

        case 2:
          message.status = reader.int32();
          break;

        case 3:
          message.error = reader.string();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes an InvokeEnd message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof InvokeEnd
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {InvokeEnd} InvokeEnd
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  InvokeEnd.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies an InvokeEnd message.
   * @function verify
   * @memberof InvokeEnd
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  InvokeEnd.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.invokeId != null && message.hasOwnProperty("invokeId")) if (!$util.isInteger(message.invokeId)) return "invokeId: integer expected";
    if (message.status != null && message.hasOwnProperty("status")) if (!$util.isInteger(message.status)) return "status: integer expected";
    if (message.error != null && message.hasOwnProperty("error")) if (!$util.isString(message.error)) return "error: string expected";
    return null;
  };
  /**
   * Creates an InvokeEnd message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof InvokeEnd
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {InvokeEnd} InvokeEnd
   */


  InvokeEnd.fromObject = function fromObject(object) {
    if (object instanceof $root.InvokeEnd) return object;
    var message = new $root.InvokeEnd();
    if (object.invokeId != null) message.invokeId = object.invokeId | 0;
    if (object.status != null) message.status = object.status | 0;
    if (object.error != null) message.error = String(object.error);
    return message;
  };
  /**
   * Creates a plain object from an InvokeEnd message. Also converts values to other types if specified.
   * @function toObject
   * @memberof InvokeEnd
   * @static
   * @param {InvokeEnd} message InvokeEnd
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  InvokeEnd.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.invokeId = 0;
      object.status = 0;
      object.error = "";
    }

    if (message.invokeId != null && message.hasOwnProperty("invokeId")) object.invokeId = message.invokeId;
    if (message.status != null && message.hasOwnProperty("status")) object.status = message.status;
    if (message.error != null && message.hasOwnProperty("error")) object.error = message.error;
    return object;
  };
  /**
   * Converts this InvokeEnd to JSON.
   * @function toJSON
   * @memberof InvokeEnd
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  InvokeEnd.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return InvokeEnd;
}();

$root.CloseConnection = function () {
  /**
   * Properties of a CloseConnection.
   * @exports ICloseConnection
   * @interface ICloseConnection
   * @property {number|null} [code] CloseConnection code
   * @property {string|null} [reason] CloseConnection reason
   * @property {boolean|null} [allowReconnect] CloseConnection allowReconnect
   */

  /**
   * Constructs a new CloseConnection.
   * @exports CloseConnection
   * @classdesc Represents a CloseConnection.
   * @implements ICloseConnection
   * @constructor
   * @param {ICloseConnection=} [properties] Properties to set
   */
  function CloseConnection(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * CloseConnection code.
   * @member {number} code
   * @memberof CloseConnection
   * @instance
   */


  CloseConnection.prototype.code = 0;
  /**
   * CloseConnection reason.
   * @member {string} reason
   * @memberof CloseConnection
   * @instance
   */

  CloseConnection.prototype.reason = "";
  /**
   * CloseConnection allowReconnect.
   * @member {boolean} allowReconnect
   * @memberof CloseConnection
   * @instance
   */

  CloseConnection.prototype.allowReconnect = false;
  /**
   * Creates a new CloseConnection instance using the specified properties.
   * @function create
   * @memberof CloseConnection
   * @static
   * @param {ICloseConnection=} [properties] Properties to set
   * @returns {CloseConnection} CloseConnection instance
   */

  CloseConnection.create = function create(properties) {
    return new CloseConnection(properties);
  };
  /**
   * Encodes the specified CloseConnection message. Does not implicitly {@link CloseConnection.verify|verify} messages.
   * @function encode
   * @memberof CloseConnection
   * @static
   * @param {ICloseConnection} message CloseConnection message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CloseConnection.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.code != null && Object.hasOwnProperty.call(message, "code")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.code);
    if (message.reason != null && Object.hasOwnProperty.call(message, "reason")) writer.uint32(
    /* id 2, wireType 2 =*/
    18).string(message.reason);
    if (message.allowReconnect != null && Object.hasOwnProperty.call(message, "allowReconnect")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).bool(message.allowReconnect);
    return writer;
  };
  /**
   * Encodes the specified CloseConnection message, length delimited. Does not implicitly {@link CloseConnection.verify|verify} messages.
   * @function encodeDelimited
   * @memberof CloseConnection
   * @static
   * @param {ICloseConnection} message CloseConnection message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CloseConnection.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a CloseConnection message from the specified reader or buffer.
   * @function decode
   * @memberof CloseConnection
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {CloseConnection} CloseConnection
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CloseConnection.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.CloseConnection();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.code = reader.int32();
          break;

        case 2:
          message.reason = reader.string();
          break;

        case 3:
          message.allowReconnect = reader.bool();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a CloseConnection message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof CloseConnection
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {CloseConnection} CloseConnection
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CloseConnection.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a CloseConnection message.
   * @function verify
   * @memberof CloseConnection
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  CloseConnection.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.code != null && message.hasOwnProperty("code")) if (!$util.isInteger(message.code)) return "code: integer expected";
    if (message.reason != null && message.hasOwnProperty("reason")) if (!$util.isString(message.reason)) return "reason: string expected";
    if (message.allowReconnect != null && message.hasOwnProperty("allowReconnect")) if (typeof message.allowReconnect !== "boolean") return "allowReconnect: boolean expected";
    return null;
  };
  /**
   * Creates a CloseConnection message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof CloseConnection
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {CloseConnection} CloseConnection
   */


  CloseConnection.fromObject = function fromObject(object) {
    if (object instanceof $root.CloseConnection) return object;
    var message = new $root.CloseConnection();
    if (object.code != null) message.code = object.code | 0;
    if (object.reason != null) message.reason = String(object.reason);
    if (object.allowReconnect != null) message.allowReconnect = Boolean(object.allowReconnect);
    return message;
  };
  /**
   * Creates a plain object from a CloseConnection message. Also converts values to other types if specified.
   * @function toObject
   * @memberof CloseConnection
   * @static
   * @param {CloseConnection} message CloseConnection
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  CloseConnection.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.code = 0;
      object.reason = "";
      object.allowReconnect = false;
    }

    if (message.code != null && message.hasOwnProperty("code")) object.code = message.code;
    if (message.reason != null && message.hasOwnProperty("reason")) object.reason = message.reason;
    if (message.allowReconnect != null && message.hasOwnProperty("allowReconnect")) object.allowReconnect = message.allowReconnect;
    return object;
  };
  /**
   * Converts this CloseConnection to JSON.
   * @function toJSON
   * @memberof CloseConnection
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  CloseConnection.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return CloseConnection;
}();

$root.Envelope = function () {
  /**
   * Properties of an Envelope.
   * @exports IEnvelope
   * @interface IEnvelope
   * @property {Envelope.IHeader|null} [header] Envelope header
   * @property {Uint8Array|null} [payload] Envelope payload
   */

  /**
   * Constructs a new Envelope.
   * @exports Envelope
   * @classdesc Represents an Envelope.
   * @implements IEnvelope
   * @constructor
   * @param {IEnvelope=} [properties] Properties to set
   */
  function Envelope(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * Envelope header.
   * @member {Envelope.IHeader|null|undefined} header
   * @memberof Envelope
   * @instance
   */


  Envelope.prototype.header = null;
  /**
   * Envelope payload.
   * @member {Uint8Array} payload
   * @memberof Envelope
   * @instance
   */

  Envelope.prototype.payload = $util.newBuffer([]);
  /**
   * Creates a new Envelope instance using the specified properties.
   * @function create
   * @memberof Envelope
   * @static
   * @param {IEnvelope=} [properties] Properties to set
   * @returns {Envelope} Envelope instance
   */

  Envelope.create = function create(properties) {
    return new Envelope(properties);
  };
  /**
   * Encodes the specified Envelope message. Does not implicitly {@link Envelope.verify|verify} messages.
   * @function encode
   * @memberof Envelope
   * @static
   * @param {IEnvelope} message Envelope message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  Envelope.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.header != null && Object.hasOwnProperty.call(message, "header")) $root.Envelope.Header.encode(message.header, writer.uint32(
    /* id 1, wireType 2 =*/
    10).fork()).ldelim();
    if (message.payload != null && Object.hasOwnProperty.call(message, "payload")) writer.uint32(
    /* id 2, wireType 2 =*/
    18).bytes(message.payload);
    return writer;
  };
  /**
   * Encodes the specified Envelope message, length delimited. Does not implicitly {@link Envelope.verify|verify} messages.
   * @function encodeDelimited
   * @memberof Envelope
   * @static
   * @param {IEnvelope} message Envelope message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  Envelope.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes an Envelope message from the specified reader or buffer.
   * @function decode
   * @memberof Envelope
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {Envelope} Envelope
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  Envelope.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Envelope();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.header = $root.Envelope.Header.decode(reader, reader.uint32());
          break;

        case 2:
          message.payload = reader.bytes();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes an Envelope message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof Envelope
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {Envelope} Envelope
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  Envelope.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies an Envelope message.
   * @function verify
   * @memberof Envelope
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  Envelope.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";

    if (message.header != null && message.hasOwnProperty("header")) {
      var error = $root.Envelope.Header.verify(message.header);
      if (error) return "header." + error;
    }

    if (message.payload != null && message.hasOwnProperty("payload")) if (!(message.payload && typeof message.payload.length === "number" || $util.isString(message.payload))) return "payload: buffer expected";
    return null;
  };
  /**
   * Creates an Envelope message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof Envelope
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {Envelope} Envelope
   */


  Envelope.fromObject = function fromObject(object) {
    if (object instanceof $root.Envelope) return object;
    var message = new $root.Envelope();

    if (object.header != null) {
      if (typeof object.header !== "object") throw TypeError(".Envelope.header: object expected");
      message.header = $root.Envelope.Header.fromObject(object.header);
    }

    if (object.payload != null) if (typeof object.payload === "string") $util.base64.decode(object.payload, message.payload = $util.newBuffer($util.base64.length(object.payload)), 0);else if (object.payload.length) message.payload = object.payload;
    return message;
  };
  /**
   * Creates a plain object from an Envelope message. Also converts values to other types if specified.
   * @function toObject
   * @memberof Envelope
   * @static
   * @param {Envelope} message Envelope
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  Envelope.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.header = null;
      if (options.bytes === String) object.payload = "";else {
        object.payload = [];
        if (options.bytes !== Array) object.payload = $util.newBuffer(object.payload);
      }
    }

    if (message.header != null && message.hasOwnProperty("header")) object.header = $root.Envelope.Header.toObject(message.header, options);
    if (message.payload != null && message.hasOwnProperty("payload")) object.payload = options.bytes === String ? $util.base64.encode(message.payload, 0, message.payload.length) : options.bytes === Array ? Array.prototype.slice.call(message.payload) : message.payload;
    return object;
  };
  /**
   * Converts this Envelope to JSON.
   * @function toJSON
   * @memberof Envelope
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  Envelope.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  Envelope.Header = function () {
    /**
     * Properties of a Header.
     * @memberof Envelope
     * @interface IHeader
     * @property {IInvokeBegin|null} [invokeBegin] Header invokeBegin
     * @property {IInvokeEnd|null} [invokeEnd] Header invokeEnd
     * @property {ICloseConnection|null} [closeConnection] Header closeConnection
     * @property {string|null} [event] Header event
     */

    /**
     * Constructs a new Header.
     * @memberof Envelope
     * @classdesc Represents a Header.
     * @implements IHeader
     * @constructor
     * @param {Envelope.IHeader=} [properties] Properties to set
     */
    function Header(properties) {
      if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
        if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
      }
    }
    /**
     * Header invokeBegin.
     * @member {IInvokeBegin|null|undefined} invokeBegin
     * @memberof Envelope.Header
     * @instance
     */


    Header.prototype.invokeBegin = null;
    /**
     * Header invokeEnd.
     * @member {IInvokeEnd|null|undefined} invokeEnd
     * @memberof Envelope.Header
     * @instance
     */

    Header.prototype.invokeEnd = null;
    /**
     * Header closeConnection.
     * @member {ICloseConnection|null|undefined} closeConnection
     * @memberof Envelope.Header
     * @instance
     */

    Header.prototype.closeConnection = null;
    /**
     * Header event.
     * @member {string|null|undefined} event
     * @memberof Envelope.Header
     * @instance
     */

    Header.prototype.event = null; // OneOf field names bound to virtual getters and setters

    var $oneOfFields;
    /**
     * Header kind.
     * @member {"invokeBegin"|"invokeEnd"|"closeConnection"|"event"|undefined} kind
     * @memberof Envelope.Header
     * @instance
     */

    Object.defineProperty(Header.prototype, "kind", {
      get: $util.oneOfGetter($oneOfFields = ["invokeBegin", "invokeEnd", "closeConnection", "event"]),
      set: $util.oneOfSetter($oneOfFields)
    });
    /**
     * Creates a new Header instance using the specified properties.
     * @function create
     * @memberof Envelope.Header
     * @static
     * @param {Envelope.IHeader=} [properties] Properties to set
     * @returns {Envelope.Header} Header instance
     */

    Header.create = function create(properties) {
      return new Header(properties);
    };
    /**
     * Encodes the specified Header message. Does not implicitly {@link Envelope.Header.verify|verify} messages.
     * @function encode
     * @memberof Envelope.Header
     * @static
     * @param {Envelope.IHeader} message Header message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Header.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.invokeBegin != null && Object.hasOwnProperty.call(message, "invokeBegin")) $root.InvokeBegin.encode(message.invokeBegin, writer.uint32(
      /* id 1, wireType 2 =*/
      10).fork()).ldelim();
      if (message.invokeEnd != null && Object.hasOwnProperty.call(message, "invokeEnd")) $root.InvokeEnd.encode(message.invokeEnd, writer.uint32(
      /* id 2, wireType 2 =*/
      18).fork()).ldelim();
      if (message.closeConnection != null && Object.hasOwnProperty.call(message, "closeConnection")) $root.CloseConnection.encode(message.closeConnection, writer.uint32(
      /* id 3, wireType 2 =*/
      26).fork()).ldelim();
      if (message.event != null && Object.hasOwnProperty.call(message, "event")) writer.uint32(
      /* id 4, wireType 2 =*/
      34).string(message.event);
      return writer;
    };
    /**
     * Encodes the specified Header message, length delimited. Does not implicitly {@link Envelope.Header.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Envelope.Header
     * @static
     * @param {Envelope.IHeader} message Header message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */


    Header.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };
    /**
     * Decodes a Header message from the specified reader or buffer.
     * @function decode
     * @memberof Envelope.Header
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Envelope.Header} Header
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Header.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Envelope.Header();

      while (reader.pos < end) {
        var tag = reader.uint32();

        switch (tag >>> 3) {
          case 1:
            message.invokeBegin = $root.InvokeBegin.decode(reader, reader.uint32());
            break;

          case 2:
            message.invokeEnd = $root.InvokeEnd.decode(reader, reader.uint32());
            break;

          case 3:
            message.closeConnection = $root.CloseConnection.decode(reader, reader.uint32());
            break;

          case 4:
            message.event = reader.string();
            break;

          default:
            reader.skipType(tag & 7);
            break;
        }
      }

      return message;
    };
    /**
     * Decodes a Header message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Envelope.Header
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Envelope.Header} Header
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */


    Header.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };
    /**
     * Verifies a Header message.
     * @function verify
     * @memberof Envelope.Header
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */


    Header.verify = function verify(message) {
      if (typeof message !== "object" || message === null) return "object expected";
      var properties = {};

      if (message.invokeBegin != null && message.hasOwnProperty("invokeBegin")) {
        properties.kind = 1;
        {
          var error = $root.InvokeBegin.verify(message.invokeBegin);
          if (error) return "invokeBegin." + error;
        }
      }

      if (message.invokeEnd != null && message.hasOwnProperty("invokeEnd")) {
        if (properties.kind === 1) return "kind: multiple values";
        properties.kind = 1;
        {
          var error = $root.InvokeEnd.verify(message.invokeEnd);
          if (error) return "invokeEnd." + error;
        }
      }

      if (message.closeConnection != null && message.hasOwnProperty("closeConnection")) {
        if (properties.kind === 1) return "kind: multiple values";
        properties.kind = 1;
        {
          var error = $root.CloseConnection.verify(message.closeConnection);
          if (error) return "closeConnection." + error;
        }
      }

      if (message.event != null && message.hasOwnProperty("event")) {
        if (properties.kind === 1) return "kind: multiple values";
        properties.kind = 1;
        if (!$util.isString(message.event)) return "event: string expected";
      }

      return null;
    };
    /**
     * Creates a Header message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Envelope.Header
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Envelope.Header} Header
     */


    Header.fromObject = function fromObject(object) {
      if (object instanceof $root.Envelope.Header) return object;
      var message = new $root.Envelope.Header();

      if (object.invokeBegin != null) {
        if (typeof object.invokeBegin !== "object") throw TypeError(".Envelope.Header.invokeBegin: object expected");
        message.invokeBegin = $root.InvokeBegin.fromObject(object.invokeBegin);
      }

      if (object.invokeEnd != null) {
        if (typeof object.invokeEnd !== "object") throw TypeError(".Envelope.Header.invokeEnd: object expected");
        message.invokeEnd = $root.InvokeEnd.fromObject(object.invokeEnd);
      }

      if (object.closeConnection != null) {
        if (typeof object.closeConnection !== "object") throw TypeError(".Envelope.Header.closeConnection: object expected");
        message.closeConnection = $root.CloseConnection.fromObject(object.closeConnection);
      }

      if (object.event != null) message.event = String(object.event);
      return message;
    };
    /**
     * Creates a plain object from a Header message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Envelope.Header
     * @static
     * @param {Envelope.Header} message Header
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */


    Header.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};

      if (message.invokeBegin != null && message.hasOwnProperty("invokeBegin")) {
        object.invokeBegin = $root.InvokeBegin.toObject(message.invokeBegin, options);
        if (options.oneofs) object.kind = "invokeBegin";
      }

      if (message.invokeEnd != null && message.hasOwnProperty("invokeEnd")) {
        object.invokeEnd = $root.InvokeEnd.toObject(message.invokeEnd, options);
        if (options.oneofs) object.kind = "invokeEnd";
      }

      if (message.closeConnection != null && message.hasOwnProperty("closeConnection")) {
        object.closeConnection = $root.CloseConnection.toObject(message.closeConnection, options);
        if (options.oneofs) object.kind = "closeConnection";
      }

      if (message.event != null && message.hasOwnProperty("event")) {
        object.event = message.event;
        if (options.oneofs) object.kind = "event";
      }

      return object;
    };
    /**
     * Converts this Header to JSON.
     * @function toJSON
     * @memberof Envelope.Header
     * @instance
     * @returns {Object.<string,*>} JSON object
     */


    Header.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Header;
  }();

  return Envelope;
}();

$root.GameServer = function () {
  /**
   * Constructs a new GameServer service.
   * @exports GameServer
   * @classdesc Represents a GameServer
   * @extends $protobuf.rpc.Service
   * @constructor
   * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
   * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
   * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
   */
  function GameServer(rpcImpl, requestDelimited, responseDelimited) {
    $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
  }

  (GameServer.prototype = Object.create($protobuf.rpc.Service.prototype)).constructor = GameServer;
  /**
   * Creates new GameServer service using the specified rpc implementation.
   * @function create
   * @memberof GameServer
   * @static
   * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
   * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
   * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
   * @returns {GameServer} RPC service. Useful where requests and/or responses are streamed.
   */

  GameServer.create = function create(rpcImpl, requestDelimited, responseDelimited) {
    return new this(rpcImpl, requestDelimited, responseDelimited);
  };
  /**
   * Callback as used by {@link GameServer#login}.
   * @memberof GameServer
   * @typedef LoginCallback
   * @type {function}
   * @param {Error|null} error Error, if any
   * @param {LoginReply} [response] LoginReply
   */

  /**
   * Calls Login.
   * @function login
   * @memberof GameServer
   * @instance
   * @param {ILoginRequest} request LoginRequest message or plain object
   * @param {GameServer.LoginCallback} callback Node-style callback called with the error, if any, and LoginReply
   * @returns {undefined}
   * @variation 1
   */


  Object.defineProperty(GameServer.prototype.login = function login(request, callback) {
    return this.rpcCall(login, $root.LoginRequest, $root.LoginReply, request, callback);
  }, "name", {
    value: "Login"
  });
  /**
   * Calls Login.
   * @function login
   * @memberof GameServer
   * @instance
   * @param {ILoginRequest} request LoginRequest message or plain object
   * @returns {Promise<LoginReply>} Promise
   * @variation 2
   */

  /**
   * Callback as used by {@link GameServer#buyProduce}.
   * @memberof GameServer
   * @typedef BuyProduceCallback
   * @type {function}
   * @param {Error|null} error Error, if any
   * @param {BuyProduceReply} [response] BuyProduceReply
   */

  /**
   * Calls BuyProduce.
   * @function buyProduce
   * @memberof GameServer
   * @instance
   * @param {IBuyProduceRequest} request BuyProduceRequest message or plain object
   * @param {GameServer.BuyProduceCallback} callback Node-style callback called with the error, if any, and BuyProduceReply
   * @returns {undefined}
   * @variation 1
   */

  Object.defineProperty(GameServer.prototype.buyProduce = function buyProduce(request, callback) {
    return this.rpcCall(buyProduce, $root.BuyProduceRequest, $root.BuyProduceReply, request, callback);
  }, "name", {
    value: "BuyProduce"
  });
  /**
   * Calls BuyProduce.
   * @function buyProduce
   * @memberof GameServer
   * @instance
   * @param {IBuyProduceRequest} request BuyProduceRequest message or plain object
   * @returns {Promise<BuyProduceReply>} Promise
   * @variation 2
   */

  /**
   * Callback as used by {@link GameServer#combineProduce}.
   * @memberof GameServer
   * @typedef CombineProduceCallback
   * @type {function}
   * @param {Error|null} error Error, if any
   * @param {CombineProduceReply} [response] CombineProduceReply
   */

  /**
   * Calls CombineProduce.
   * @function combineProduce
   * @memberof GameServer
   * @instance
   * @param {ICombineProduceRequest} request CombineProduceRequest message or plain object
   * @param {GameServer.CombineProduceCallback} callback Node-style callback called with the error, if any, and CombineProduceReply
   * @returns {undefined}
   * @variation 1
   */

  Object.defineProperty(GameServer.prototype.combineProduce = function combineProduce(request, callback) {
    return this.rpcCall(combineProduce, $root.CombineProduceRequest, $root.CombineProduceReply, request, callback);
  }, "name", {
    value: "CombineProduce"
  });
  /**
   * Calls CombineProduce.
   * @function combineProduce
   * @memberof GameServer
   * @instance
   * @param {ICombineProduceRequest} request CombineProduceRequest message or plain object
   * @returns {Promise<CombineProduceReply>} Promise
   * @variation 2
   */

  /**
   * Callback as used by {@link GameServer#refreshWorkBench}.
   * @memberof GameServer
   * @typedef RefreshWorkBenchCallback
   * @type {function}
   * @param {Error|null} error Error, if any
   * @param {RefreshWorkBenchReply} [response] RefreshWorkBenchReply
   */

  /**
   * Calls RefreshWorkBench.
   * @function refreshWorkBench
   * @memberof GameServer
   * @instance
   * @param {IRefreshWorkBenchRequest} request RefreshWorkBenchRequest message or plain object
   * @param {GameServer.RefreshWorkBenchCallback} callback Node-style callback called with the error, if any, and RefreshWorkBenchReply
   * @returns {undefined}
   * @variation 1
   */

  Object.defineProperty(GameServer.prototype.refreshWorkBench = function refreshWorkBench(request, callback) {
    return this.rpcCall(refreshWorkBench, $root.RefreshWorkBenchRequest, $root.RefreshWorkBenchReply, request, callback);
  }, "name", {
    value: "RefreshWorkBench"
  });
  /**
   * Calls RefreshWorkBench.
   * @function refreshWorkBench
   * @memberof GameServer
   * @instance
   * @param {IRefreshWorkBenchRequest} request RefreshWorkBenchRequest message or plain object
   * @returns {Promise<RefreshWorkBenchReply>} Promise
   * @variation 2
   */

  return GameServer;
}();

$root.LoginRequest = function () {
  /**
   * Properties of a LoginRequest.
   * @exports ILoginRequest
   * @interface ILoginRequest
   * @property {string|null} [accessToken] LoginRequest accessToken
   */

  /**
   * Constructs a new LoginRequest.
   * @exports LoginRequest
   * @classdesc Represents a LoginRequest.
   * @implements ILoginRequest
   * @constructor
   * @param {ILoginRequest=} [properties] Properties to set
   */
  function LoginRequest(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * LoginRequest accessToken.
   * @member {string} accessToken
   * @memberof LoginRequest
   * @instance
   */


  LoginRequest.prototype.accessToken = "";
  /**
   * Creates a new LoginRequest instance using the specified properties.
   * @function create
   * @memberof LoginRequest
   * @static
   * @param {ILoginRequest=} [properties] Properties to set
   * @returns {LoginRequest} LoginRequest instance
   */

  LoginRequest.create = function create(properties) {
    return new LoginRequest(properties);
  };
  /**
   * Encodes the specified LoginRequest message. Does not implicitly {@link LoginRequest.verify|verify} messages.
   * @function encode
   * @memberof LoginRequest
   * @static
   * @param {ILoginRequest} message LoginRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  LoginRequest.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.accessToken != null && Object.hasOwnProperty.call(message, "accessToken")) writer.uint32(
    /* id 1, wireType 2 =*/
    10).string(message.accessToken);
    return writer;
  };
  /**
   * Encodes the specified LoginRequest message, length delimited. Does not implicitly {@link LoginRequest.verify|verify} messages.
   * @function encodeDelimited
   * @memberof LoginRequest
   * @static
   * @param {ILoginRequest} message LoginRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  LoginRequest.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a LoginRequest message from the specified reader or buffer.
   * @function decode
   * @memberof LoginRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {LoginRequest} LoginRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  LoginRequest.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.LoginRequest();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.accessToken = reader.string();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a LoginRequest message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof LoginRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {LoginRequest} LoginRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  LoginRequest.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a LoginRequest message.
   * @function verify
   * @memberof LoginRequest
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  LoginRequest.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.accessToken != null && message.hasOwnProperty("accessToken")) if (!$util.isString(message.accessToken)) return "accessToken: string expected";
    return null;
  };
  /**
   * Creates a LoginRequest message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof LoginRequest
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {LoginRequest} LoginRequest
   */


  LoginRequest.fromObject = function fromObject(object) {
    if (object instanceof $root.LoginRequest) return object;
    var message = new $root.LoginRequest();
    if (object.accessToken != null) message.accessToken = String(object.accessToken);
    return message;
  };
  /**
   * Creates a plain object from a LoginRequest message. Also converts values to other types if specified.
   * @function toObject
   * @memberof LoginRequest
   * @static
   * @param {LoginRequest} message LoginRequest
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  LoginRequest.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};
    if (options.defaults) object.accessToken = "";
    if (message.accessToken != null && message.hasOwnProperty("accessToken")) object.accessToken = message.accessToken;
    return object;
  };
  /**
   * Converts this LoginRequest to JSON.
   * @function toJSON
   * @memberof LoginRequest
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  LoginRequest.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return LoginRequest;
}();

$root.LoginReply = function () {
  /**
   * Properties of a LoginReply.
   * @exports ILoginReply
   * @interface ILoginReply
   * @property {IUserInfo|null} [userInfo] LoginReply userInfo
   * @property {number|null} [serverTime] LoginReply serverTime
   * @property {number|Long|null} [coin] LoginReply coin
   * @property {number|Long|null} [diamond] LoginReply diamond
   * @property {Array.<IWorkBenchItemInfo>|null} [workbenchItems] LoginReply workbenchItems
   */

  /**
   * Constructs a new LoginReply.
   * @exports LoginReply
   * @classdesc Represents a LoginReply.
   * @implements ILoginReply
   * @constructor
   * @param {ILoginReply=} [properties] Properties to set
   */
  function LoginReply(properties) {
    this.workbenchItems = [];
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * LoginReply userInfo.
   * @member {IUserInfo|null|undefined} userInfo
   * @memberof LoginReply
   * @instance
   */


  LoginReply.prototype.userInfo = null;
  /**
   * LoginReply serverTime.
   * @member {number} serverTime
   * @memberof LoginReply
   * @instance
   */

  LoginReply.prototype.serverTime = 0;
  /**
   * LoginReply coin.
   * @member {number|Long} coin
   * @memberof LoginReply
   * @instance
   */

  LoginReply.prototype.coin = $util.Long ? $util.Long.fromBits(0, 0, true) : 0;
  /**
   * LoginReply diamond.
   * @member {number|Long} diamond
   * @memberof LoginReply
   * @instance
   */

  LoginReply.prototype.diamond = $util.Long ? $util.Long.fromBits(0, 0, true) : 0;
  /**
   * LoginReply workbenchItems.
   * @member {Array.<IWorkBenchItemInfo>} workbenchItems
   * @memberof LoginReply
   * @instance
   */

  LoginReply.prototype.workbenchItems = $util.emptyArray;
  /**
   * Creates a new LoginReply instance using the specified properties.
   * @function create
   * @memberof LoginReply
   * @static
   * @param {ILoginReply=} [properties] Properties to set
   * @returns {LoginReply} LoginReply instance
   */

  LoginReply.create = function create(properties) {
    return new LoginReply(properties);
  };
  /**
   * Encodes the specified LoginReply message. Does not implicitly {@link LoginReply.verify|verify} messages.
   * @function encode
   * @memberof LoginReply
   * @static
   * @param {ILoginReply} message LoginReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  LoginReply.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.userInfo != null && Object.hasOwnProperty.call(message, "userInfo")) $root.UserInfo.encode(message.userInfo, writer.uint32(
    /* id 1, wireType 2 =*/
    10).fork()).ldelim();
    if (message.serverTime != null && Object.hasOwnProperty.call(message, "serverTime")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).uint32(message.serverTime);
    if (message.coin != null && Object.hasOwnProperty.call(message, "coin")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).uint64(message.coin);
    if (message.diamond != null && Object.hasOwnProperty.call(message, "diamond")) writer.uint32(
    /* id 4, wireType 0 =*/
    32).uint64(message.diamond);
    if (message.workbenchItems != null && message.workbenchItems.length) for (var i = 0; i < message.workbenchItems.length; ++i) {
      $root.WorkBenchItemInfo.encode(message.workbenchItems[i], writer.uint32(
      /* id 5, wireType 2 =*/
      42).fork()).ldelim();
    }
    return writer;
  };
  /**
   * Encodes the specified LoginReply message, length delimited. Does not implicitly {@link LoginReply.verify|verify} messages.
   * @function encodeDelimited
   * @memberof LoginReply
   * @static
   * @param {ILoginReply} message LoginReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  LoginReply.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a LoginReply message from the specified reader or buffer.
   * @function decode
   * @memberof LoginReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {LoginReply} LoginReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  LoginReply.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.LoginReply();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.userInfo = $root.UserInfo.decode(reader, reader.uint32());
          break;

        case 2:
          message.serverTime = reader.uint32();
          break;

        case 3:
          message.coin = reader.uint64();
          break;

        case 4:
          message.diamond = reader.uint64();
          break;

        case 5:
          if (!(message.workbenchItems && message.workbenchItems.length)) message.workbenchItems = [];
          message.workbenchItems.push($root.WorkBenchItemInfo.decode(reader, reader.uint32()));
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a LoginReply message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof LoginReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {LoginReply} LoginReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  LoginReply.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a LoginReply message.
   * @function verify
   * @memberof LoginReply
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  LoginReply.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";

    if (message.userInfo != null && message.hasOwnProperty("userInfo")) {
      var error = $root.UserInfo.verify(message.userInfo);
      if (error) return "userInfo." + error;
    }

    if (message.serverTime != null && message.hasOwnProperty("serverTime")) if (!$util.isInteger(message.serverTime)) return "serverTime: integer expected";
    if (message.coin != null && message.hasOwnProperty("coin")) if (!$util.isInteger(message.coin) && !(message.coin && $util.isInteger(message.coin.low) && $util.isInteger(message.coin.high))) return "coin: integer|Long expected";
    if (message.diamond != null && message.hasOwnProperty("diamond")) if (!$util.isInteger(message.diamond) && !(message.diamond && $util.isInteger(message.diamond.low) && $util.isInteger(message.diamond.high))) return "diamond: integer|Long expected";

    if (message.workbenchItems != null && message.hasOwnProperty("workbenchItems")) {
      if (!Array.isArray(message.workbenchItems)) return "workbenchItems: array expected";

      for (var i = 0; i < message.workbenchItems.length; ++i) {
        var error = $root.WorkBenchItemInfo.verify(message.workbenchItems[i]);
        if (error) return "workbenchItems." + error;
      }
    }

    return null;
  };
  /**
   * Creates a LoginReply message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof LoginReply
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {LoginReply} LoginReply
   */


  LoginReply.fromObject = function fromObject(object) {
    if (object instanceof $root.LoginReply) return object;
    var message = new $root.LoginReply();

    if (object.userInfo != null) {
      if (typeof object.userInfo !== "object") throw TypeError(".LoginReply.userInfo: object expected");
      message.userInfo = $root.UserInfo.fromObject(object.userInfo);
    }

    if (object.serverTime != null) message.serverTime = object.serverTime >>> 0;
    if (object.coin != null) if ($util.Long) (message.coin = $util.Long.fromValue(object.coin)).unsigned = true;else if (typeof object.coin === "string") message.coin = parseInt(object.coin, 10);else if (typeof object.coin === "number") message.coin = object.coin;else if (typeof object.coin === "object") message.coin = new $util.LongBits(object.coin.low >>> 0, object.coin.high >>> 0).toNumber(true);
    if (object.diamond != null) if ($util.Long) (message.diamond = $util.Long.fromValue(object.diamond)).unsigned = true;else if (typeof object.diamond === "string") message.diamond = parseInt(object.diamond, 10);else if (typeof object.diamond === "number") message.diamond = object.diamond;else if (typeof object.diamond === "object") message.diamond = new $util.LongBits(object.diamond.low >>> 0, object.diamond.high >>> 0).toNumber(true);

    if (object.workbenchItems) {
      if (!Array.isArray(object.workbenchItems)) throw TypeError(".LoginReply.workbenchItems: array expected");
      message.workbenchItems = [];

      for (var i = 0; i < object.workbenchItems.length; ++i) {
        if (typeof object.workbenchItems[i] !== "object") throw TypeError(".LoginReply.workbenchItems: object expected");
        message.workbenchItems[i] = $root.WorkBenchItemInfo.fromObject(object.workbenchItems[i]);
      }
    }

    return message;
  };
  /**
   * Creates a plain object from a LoginReply message. Also converts values to other types if specified.
   * @function toObject
   * @memberof LoginReply
   * @static
   * @param {LoginReply} message LoginReply
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  LoginReply.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};
    if (options.arrays || options.defaults) object.workbenchItems = [];

    if (options.defaults) {
      object.userInfo = null;
      object.serverTime = 0;

      if ($util.Long) {
        var _long = new $util.Long(0, 0, true);

        object.coin = options.longs === String ? _long.toString() : options.longs === Number ? _long.toNumber() : _long;
      } else object.coin = options.longs === String ? "0" : 0;

      if ($util.Long) {
        var _long = new $util.Long(0, 0, true);

        object.diamond = options.longs === String ? _long.toString() : options.longs === Number ? _long.toNumber() : _long;
      } else object.diamond = options.longs === String ? "0" : 0;
    }

    if (message.userInfo != null && message.hasOwnProperty("userInfo")) object.userInfo = $root.UserInfo.toObject(message.userInfo, options);
    if (message.serverTime != null && message.hasOwnProperty("serverTime")) object.serverTime = message.serverTime;
    if (message.coin != null && message.hasOwnProperty("coin")) if (typeof message.coin === "number") object.coin = options.longs === String ? String(message.coin) : message.coin;else object.coin = options.longs === String ? $util.Long.prototype.toString.call(message.coin) : options.longs === Number ? new $util.LongBits(message.coin.low >>> 0, message.coin.high >>> 0).toNumber(true) : message.coin;
    if (message.diamond != null && message.hasOwnProperty("diamond")) if (typeof message.diamond === "number") object.diamond = options.longs === String ? String(message.diamond) : message.diamond;else object.diamond = options.longs === String ? $util.Long.prototype.toString.call(message.diamond) : options.longs === Number ? new $util.LongBits(message.diamond.low >>> 0, message.diamond.high >>> 0).toNumber(true) : message.diamond;

    if (message.workbenchItems && message.workbenchItems.length) {
      object.workbenchItems = [];

      for (var j = 0; j < message.workbenchItems.length; ++j) {
        object.workbenchItems[j] = $root.WorkBenchItemInfo.toObject(message.workbenchItems[j], options);
      }
    }

    return object;
  };
  /**
   * Converts this LoginReply to JSON.
   * @function toJSON
   * @memberof LoginReply
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  LoginReply.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return LoginReply;
}();

$root.BuyProduceRequest = function () {
  /**
   * Properties of a BuyProduceRequest.
   * @exports IBuyProduceRequest
   * @interface IBuyProduceRequest
   * @property {number|null} [produceLevel] BuyProduceRequest produceLevel
   */

  /**
   * Constructs a new BuyProduceRequest.
   * @exports BuyProduceRequest
   * @classdesc Represents a BuyProduceRequest.
   * @implements IBuyProduceRequest
   * @constructor
   * @param {IBuyProduceRequest=} [properties] Properties to set
   */
  function BuyProduceRequest(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * BuyProduceRequest produceLevel.
   * @member {number} produceLevel
   * @memberof BuyProduceRequest
   * @instance
   */


  BuyProduceRequest.prototype.produceLevel = 0;
  /**
   * Creates a new BuyProduceRequest instance using the specified properties.
   * @function create
   * @memberof BuyProduceRequest
   * @static
   * @param {IBuyProduceRequest=} [properties] Properties to set
   * @returns {BuyProduceRequest} BuyProduceRequest instance
   */

  BuyProduceRequest.create = function create(properties) {
    return new BuyProduceRequest(properties);
  };
  /**
   * Encodes the specified BuyProduceRequest message. Does not implicitly {@link BuyProduceRequest.verify|verify} messages.
   * @function encode
   * @memberof BuyProduceRequest
   * @static
   * @param {IBuyProduceRequest} message BuyProduceRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  BuyProduceRequest.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.produceLevel != null && Object.hasOwnProperty.call(message, "produceLevel")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.produceLevel);
    return writer;
  };
  /**
   * Encodes the specified BuyProduceRequest message, length delimited. Does not implicitly {@link BuyProduceRequest.verify|verify} messages.
   * @function encodeDelimited
   * @memberof BuyProduceRequest
   * @static
   * @param {IBuyProduceRequest} message BuyProduceRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  BuyProduceRequest.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a BuyProduceRequest message from the specified reader or buffer.
   * @function decode
   * @memberof BuyProduceRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {BuyProduceRequest} BuyProduceRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  BuyProduceRequest.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.BuyProduceRequest();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.produceLevel = reader.int32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a BuyProduceRequest message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof BuyProduceRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {BuyProduceRequest} BuyProduceRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  BuyProduceRequest.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a BuyProduceRequest message.
   * @function verify
   * @memberof BuyProduceRequest
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  BuyProduceRequest.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) if (!$util.isInteger(message.produceLevel)) return "produceLevel: integer expected";
    return null;
  };
  /**
   * Creates a BuyProduceRequest message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof BuyProduceRequest
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {BuyProduceRequest} BuyProduceRequest
   */


  BuyProduceRequest.fromObject = function fromObject(object) {
    if (object instanceof $root.BuyProduceRequest) return object;
    var message = new $root.BuyProduceRequest();
    if (object.produceLevel != null) message.produceLevel = object.produceLevel | 0;
    return message;
  };
  /**
   * Creates a plain object from a BuyProduceRequest message. Also converts values to other types if specified.
   * @function toObject
   * @memberof BuyProduceRequest
   * @static
   * @param {BuyProduceRequest} message BuyProduceRequest
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  BuyProduceRequest.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};
    if (options.defaults) object.produceLevel = 0;
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) object.produceLevel = message.produceLevel;
    return object;
  };
  /**
   * Converts this BuyProduceRequest to JSON.
   * @function toJSON
   * @memberof BuyProduceRequest
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  BuyProduceRequest.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return BuyProduceRequest;
}();

$root.BuyProduceReply = function () {
  /**
   * Properties of a BuyProduceReply.
   * @exports IBuyProduceReply
   * @interface IBuyProduceReply
   * @property {number|null} [errcode] BuyProduceReply errcode
   * @property {number|null} [produceLevel] BuyProduceReply produceLevel
   * @property {number|null} [position] BuyProduceReply position
   */

  /**
   * Constructs a new BuyProduceReply.
   * @exports BuyProduceReply
   * @classdesc Represents a BuyProduceReply.
   * @implements IBuyProduceReply
   * @constructor
   * @param {IBuyProduceReply=} [properties] Properties to set
   */
  function BuyProduceReply(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * BuyProduceReply errcode.
   * @member {number} errcode
   * @memberof BuyProduceReply
   * @instance
   */


  BuyProduceReply.prototype.errcode = 0;
  /**
   * BuyProduceReply produceLevel.
   * @member {number} produceLevel
   * @memberof BuyProduceReply
   * @instance
   */

  BuyProduceReply.prototype.produceLevel = 0;
  /**
   * BuyProduceReply position.
   * @member {number} position
   * @memberof BuyProduceReply
   * @instance
   */

  BuyProduceReply.prototype.position = 0;
  /**
   * Creates a new BuyProduceReply instance using the specified properties.
   * @function create
   * @memberof BuyProduceReply
   * @static
   * @param {IBuyProduceReply=} [properties] Properties to set
   * @returns {BuyProduceReply} BuyProduceReply instance
   */

  BuyProduceReply.create = function create(properties) {
    return new BuyProduceReply(properties);
  };
  /**
   * Encodes the specified BuyProduceReply message. Does not implicitly {@link BuyProduceReply.verify|verify} messages.
   * @function encode
   * @memberof BuyProduceReply
   * @static
   * @param {IBuyProduceReply} message BuyProduceReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  BuyProduceReply.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.errcode != null && Object.hasOwnProperty.call(message, "errcode")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.errcode);
    if (message.produceLevel != null && Object.hasOwnProperty.call(message, "produceLevel")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int32(message.produceLevel);
    if (message.position != null && Object.hasOwnProperty.call(message, "position")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).int32(message.position);
    return writer;
  };
  /**
   * Encodes the specified BuyProduceReply message, length delimited. Does not implicitly {@link BuyProduceReply.verify|verify} messages.
   * @function encodeDelimited
   * @memberof BuyProduceReply
   * @static
   * @param {IBuyProduceReply} message BuyProduceReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  BuyProduceReply.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a BuyProduceReply message from the specified reader or buffer.
   * @function decode
   * @memberof BuyProduceReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {BuyProduceReply} BuyProduceReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  BuyProduceReply.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.BuyProduceReply();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.errcode = reader.int32();
          break;

        case 2:
          message.produceLevel = reader.int32();
          break;

        case 3:
          message.position = reader.int32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a BuyProduceReply message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof BuyProduceReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {BuyProduceReply} BuyProduceReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  BuyProduceReply.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a BuyProduceReply message.
   * @function verify
   * @memberof BuyProduceReply
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  BuyProduceReply.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.errcode != null && message.hasOwnProperty("errcode")) if (!$util.isInteger(message.errcode)) return "errcode: integer expected";
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) if (!$util.isInteger(message.produceLevel)) return "produceLevel: integer expected";
    if (message.position != null && message.hasOwnProperty("position")) if (!$util.isInteger(message.position)) return "position: integer expected";
    return null;
  };
  /**
   * Creates a BuyProduceReply message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof BuyProduceReply
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {BuyProduceReply} BuyProduceReply
   */


  BuyProduceReply.fromObject = function fromObject(object) {
    if (object instanceof $root.BuyProduceReply) return object;
    var message = new $root.BuyProduceReply();
    if (object.errcode != null) message.errcode = object.errcode | 0;
    if (object.produceLevel != null) message.produceLevel = object.produceLevel | 0;
    if (object.position != null) message.position = object.position | 0;
    return message;
  };
  /**
   * Creates a plain object from a BuyProduceReply message. Also converts values to other types if specified.
   * @function toObject
   * @memberof BuyProduceReply
   * @static
   * @param {BuyProduceReply} message BuyProduceReply
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  BuyProduceReply.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.errcode = 0;
      object.produceLevel = 0;
      object.position = 0;
    }

    if (message.errcode != null && message.hasOwnProperty("errcode")) object.errcode = message.errcode;
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) object.produceLevel = message.produceLevel;
    if (message.position != null && message.hasOwnProperty("position")) object.position = message.position;
    return object;
  };
  /**
   * Converts this BuyProduceReply to JSON.
   * @function toJSON
   * @memberof BuyProduceReply
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  BuyProduceReply.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return BuyProduceReply;
}();

$root.CombineProduceRequest = function () {
  /**
   * Properties of a CombineProduceRequest.
   * @exports ICombineProduceRequest
   * @interface ICombineProduceRequest
   * @property {number|null} [positionBase] CombineProduceRequest positionBase
   * @property {number|null} [positionTarget] CombineProduceRequest positionTarget
   */

  /**
   * Constructs a new CombineProduceRequest.
   * @exports CombineProduceRequest
   * @classdesc Represents a CombineProduceRequest.
   * @implements ICombineProduceRequest
   * @constructor
   * @param {ICombineProduceRequest=} [properties] Properties to set
   */
  function CombineProduceRequest(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * CombineProduceRequest positionBase.
   * @member {number} positionBase
   * @memberof CombineProduceRequest
   * @instance
   */


  CombineProduceRequest.prototype.positionBase = 0;
  /**
   * CombineProduceRequest positionTarget.
   * @member {number} positionTarget
   * @memberof CombineProduceRequest
   * @instance
   */

  CombineProduceRequest.prototype.positionTarget = 0;
  /**
   * Creates a new CombineProduceRequest instance using the specified properties.
   * @function create
   * @memberof CombineProduceRequest
   * @static
   * @param {ICombineProduceRequest=} [properties] Properties to set
   * @returns {CombineProduceRequest} CombineProduceRequest instance
   */

  CombineProduceRequest.create = function create(properties) {
    return new CombineProduceRequest(properties);
  };
  /**
   * Encodes the specified CombineProduceRequest message. Does not implicitly {@link CombineProduceRequest.verify|verify} messages.
   * @function encode
   * @memberof CombineProduceRequest
   * @static
   * @param {ICombineProduceRequest} message CombineProduceRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CombineProduceRequest.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.positionBase != null && Object.hasOwnProperty.call(message, "positionBase")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.positionBase);
    if (message.positionTarget != null && Object.hasOwnProperty.call(message, "positionTarget")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int32(message.positionTarget);
    return writer;
  };
  /**
   * Encodes the specified CombineProduceRequest message, length delimited. Does not implicitly {@link CombineProduceRequest.verify|verify} messages.
   * @function encodeDelimited
   * @memberof CombineProduceRequest
   * @static
   * @param {ICombineProduceRequest} message CombineProduceRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CombineProduceRequest.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a CombineProduceRequest message from the specified reader or buffer.
   * @function decode
   * @memberof CombineProduceRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {CombineProduceRequest} CombineProduceRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CombineProduceRequest.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.CombineProduceRequest();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.positionBase = reader.int32();
          break;

        case 2:
          message.positionTarget = reader.int32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a CombineProduceRequest message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof CombineProduceRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {CombineProduceRequest} CombineProduceRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CombineProduceRequest.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a CombineProduceRequest message.
   * @function verify
   * @memberof CombineProduceRequest
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  CombineProduceRequest.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.positionBase != null && message.hasOwnProperty("positionBase")) if (!$util.isInteger(message.positionBase)) return "positionBase: integer expected";
    if (message.positionTarget != null && message.hasOwnProperty("positionTarget")) if (!$util.isInteger(message.positionTarget)) return "positionTarget: integer expected";
    return null;
  };
  /**
   * Creates a CombineProduceRequest message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof CombineProduceRequest
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {CombineProduceRequest} CombineProduceRequest
   */


  CombineProduceRequest.fromObject = function fromObject(object) {
    if (object instanceof $root.CombineProduceRequest) return object;
    var message = new $root.CombineProduceRequest();
    if (object.positionBase != null) message.positionBase = object.positionBase | 0;
    if (object.positionTarget != null) message.positionTarget = object.positionTarget | 0;
    return message;
  };
  /**
   * Creates a plain object from a CombineProduceRequest message. Also converts values to other types if specified.
   * @function toObject
   * @memberof CombineProduceRequest
   * @static
   * @param {CombineProduceRequest} message CombineProduceRequest
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  CombineProduceRequest.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.positionBase = 0;
      object.positionTarget = 0;
    }

    if (message.positionBase != null && message.hasOwnProperty("positionBase")) object.positionBase = message.positionBase;
    if (message.positionTarget != null && message.hasOwnProperty("positionTarget")) object.positionTarget = message.positionTarget;
    return object;
  };
  /**
   * Converts this CombineProduceRequest to JSON.
   * @function toJSON
   * @memberof CombineProduceRequest
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  CombineProduceRequest.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return CombineProduceRequest;
}();

$root.CombineProduceReply = function () {
  /**
   * Properties of a CombineProduceReply.
   * @exports ICombineProduceReply
   * @interface ICombineProduceReply
   * @property {number|null} [errcode] CombineProduceReply errcode
   * @property {number|null} [produceLevel] CombineProduceReply produceLevel
   * @property {number|null} [positionNew] CombineProduceReply positionNew
   */

  /**
   * Constructs a new CombineProduceReply.
   * @exports CombineProduceReply
   * @classdesc Represents a CombineProduceReply.
   * @implements ICombineProduceReply
   * @constructor
   * @param {ICombineProduceReply=} [properties] Properties to set
   */
  function CombineProduceReply(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * CombineProduceReply errcode.
   * @member {number} errcode
   * @memberof CombineProduceReply
   * @instance
   */


  CombineProduceReply.prototype.errcode = 0;
  /**
   * CombineProduceReply produceLevel.
   * @member {number} produceLevel
   * @memberof CombineProduceReply
   * @instance
   */

  CombineProduceReply.prototype.produceLevel = 0;
  /**
   * CombineProduceReply positionNew.
   * @member {number} positionNew
   * @memberof CombineProduceReply
   * @instance
   */

  CombineProduceReply.prototype.positionNew = 0;
  /**
   * Creates a new CombineProduceReply instance using the specified properties.
   * @function create
   * @memberof CombineProduceReply
   * @static
   * @param {ICombineProduceReply=} [properties] Properties to set
   * @returns {CombineProduceReply} CombineProduceReply instance
   */

  CombineProduceReply.create = function create(properties) {
    return new CombineProduceReply(properties);
  };
  /**
   * Encodes the specified CombineProduceReply message. Does not implicitly {@link CombineProduceReply.verify|verify} messages.
   * @function encode
   * @memberof CombineProduceReply
   * @static
   * @param {ICombineProduceReply} message CombineProduceReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CombineProduceReply.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.errcode != null && Object.hasOwnProperty.call(message, "errcode")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.errcode);
    if (message.produceLevel != null && Object.hasOwnProperty.call(message, "produceLevel")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int32(message.produceLevel);
    if (message.positionNew != null && Object.hasOwnProperty.call(message, "positionNew")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).int32(message.positionNew);
    return writer;
  };
  /**
   * Encodes the specified CombineProduceReply message, length delimited. Does not implicitly {@link CombineProduceReply.verify|verify} messages.
   * @function encodeDelimited
   * @memberof CombineProduceReply
   * @static
   * @param {ICombineProduceReply} message CombineProduceReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  CombineProduceReply.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a CombineProduceReply message from the specified reader or buffer.
   * @function decode
   * @memberof CombineProduceReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {CombineProduceReply} CombineProduceReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CombineProduceReply.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.CombineProduceReply();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.errcode = reader.int32();
          break;

        case 2:
          message.produceLevel = reader.int32();
          break;

        case 3:
          message.positionNew = reader.int32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a CombineProduceReply message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof CombineProduceReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {CombineProduceReply} CombineProduceReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  CombineProduceReply.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a CombineProduceReply message.
   * @function verify
   * @memberof CombineProduceReply
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  CombineProduceReply.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.errcode != null && message.hasOwnProperty("errcode")) if (!$util.isInteger(message.errcode)) return "errcode: integer expected";
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) if (!$util.isInteger(message.produceLevel)) return "produceLevel: integer expected";
    if (message.positionNew != null && message.hasOwnProperty("positionNew")) if (!$util.isInteger(message.positionNew)) return "positionNew: integer expected";
    return null;
  };
  /**
   * Creates a CombineProduceReply message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof CombineProduceReply
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {CombineProduceReply} CombineProduceReply
   */


  CombineProduceReply.fromObject = function fromObject(object) {
    if (object instanceof $root.CombineProduceReply) return object;
    var message = new $root.CombineProduceReply();
    if (object.errcode != null) message.errcode = object.errcode | 0;
    if (object.produceLevel != null) message.produceLevel = object.produceLevel | 0;
    if (object.positionNew != null) message.positionNew = object.positionNew | 0;
    return message;
  };
  /**
   * Creates a plain object from a CombineProduceReply message. Also converts values to other types if specified.
   * @function toObject
   * @memberof CombineProduceReply
   * @static
   * @param {CombineProduceReply} message CombineProduceReply
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  CombineProduceReply.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.errcode = 0;
      object.produceLevel = 0;
      object.positionNew = 0;
    }

    if (message.errcode != null && message.hasOwnProperty("errcode")) object.errcode = message.errcode;
    if (message.produceLevel != null && message.hasOwnProperty("produceLevel")) object.produceLevel = message.produceLevel;
    if (message.positionNew != null && message.hasOwnProperty("positionNew")) object.positionNew = message.positionNew;
    return object;
  };
  /**
   * Converts this CombineProduceReply to JSON.
   * @function toJSON
   * @memberof CombineProduceReply
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  CombineProduceReply.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return CombineProduceReply;
}();

$root.RefreshWorkBenchRequest = function () {
  /**
   * Properties of a RefreshWorkBenchRequest.
   * @exports IRefreshWorkBenchRequest
   * @interface IRefreshWorkBenchRequest
   */

  /**
   * Constructs a new RefreshWorkBenchRequest.
   * @exports RefreshWorkBenchRequest
   * @classdesc Represents a RefreshWorkBenchRequest.
   * @implements IRefreshWorkBenchRequest
   * @constructor
   * @param {IRefreshWorkBenchRequest=} [properties] Properties to set
   */
  function RefreshWorkBenchRequest(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * Creates a new RefreshWorkBenchRequest instance using the specified properties.
   * @function create
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {IRefreshWorkBenchRequest=} [properties] Properties to set
   * @returns {RefreshWorkBenchRequest} RefreshWorkBenchRequest instance
   */


  RefreshWorkBenchRequest.create = function create(properties) {
    return new RefreshWorkBenchRequest(properties);
  };
  /**
   * Encodes the specified RefreshWorkBenchRequest message. Does not implicitly {@link RefreshWorkBenchRequest.verify|verify} messages.
   * @function encode
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {IRefreshWorkBenchRequest} message RefreshWorkBenchRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  RefreshWorkBenchRequest.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    return writer;
  };
  /**
   * Encodes the specified RefreshWorkBenchRequest message, length delimited. Does not implicitly {@link RefreshWorkBenchRequest.verify|verify} messages.
   * @function encodeDelimited
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {IRefreshWorkBenchRequest} message RefreshWorkBenchRequest message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  RefreshWorkBenchRequest.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a RefreshWorkBenchRequest message from the specified reader or buffer.
   * @function decode
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {RefreshWorkBenchRequest} RefreshWorkBenchRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  RefreshWorkBenchRequest.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.RefreshWorkBenchRequest();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a RefreshWorkBenchRequest message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {RefreshWorkBenchRequest} RefreshWorkBenchRequest
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  RefreshWorkBenchRequest.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a RefreshWorkBenchRequest message.
   * @function verify
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  RefreshWorkBenchRequest.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    return null;
  };
  /**
   * Creates a RefreshWorkBenchRequest message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {RefreshWorkBenchRequest} RefreshWorkBenchRequest
   */


  RefreshWorkBenchRequest.fromObject = function fromObject(object) {
    if (object instanceof $root.RefreshWorkBenchRequest) return object;
    return new $root.RefreshWorkBenchRequest();
  };
  /**
   * Creates a plain object from a RefreshWorkBenchRequest message. Also converts values to other types if specified.
   * @function toObject
   * @memberof RefreshWorkBenchRequest
   * @static
   * @param {RefreshWorkBenchRequest} message RefreshWorkBenchRequest
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  RefreshWorkBenchRequest.toObject = function toObject() {
    return {};
  };
  /**
   * Converts this RefreshWorkBenchRequest to JSON.
   * @function toJSON
   * @memberof RefreshWorkBenchRequest
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  RefreshWorkBenchRequest.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return RefreshWorkBenchRequest;
}();

$root.RefreshWorkBenchReply = function () {
  /**
   * Properties of a RefreshWorkBenchReply.
   * @exports IRefreshWorkBenchReply
   * @interface IRefreshWorkBenchReply
   * @property {Array.<number>|null} [workbenchInfo] RefreshWorkBenchReply workbenchInfo
   */

  /**
   * Constructs a new RefreshWorkBenchReply.
   * @exports RefreshWorkBenchReply
   * @classdesc Represents a RefreshWorkBenchReply.
   * @implements IRefreshWorkBenchReply
   * @constructor
   * @param {IRefreshWorkBenchReply=} [properties] Properties to set
   */
  function RefreshWorkBenchReply(properties) {
    this.workbenchInfo = [];
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * RefreshWorkBenchReply workbenchInfo.
   * @member {Array.<number>} workbenchInfo
   * @memberof RefreshWorkBenchReply
   * @instance
   */


  RefreshWorkBenchReply.prototype.workbenchInfo = $util.emptyArray;
  /**
   * Creates a new RefreshWorkBenchReply instance using the specified properties.
   * @function create
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {IRefreshWorkBenchReply=} [properties] Properties to set
   * @returns {RefreshWorkBenchReply} RefreshWorkBenchReply instance
   */

  RefreshWorkBenchReply.create = function create(properties) {
    return new RefreshWorkBenchReply(properties);
  };
  /**
   * Encodes the specified RefreshWorkBenchReply message. Does not implicitly {@link RefreshWorkBenchReply.verify|verify} messages.
   * @function encode
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {IRefreshWorkBenchReply} message RefreshWorkBenchReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  RefreshWorkBenchReply.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();

    if (message.workbenchInfo != null && message.workbenchInfo.length) {
      writer.uint32(
      /* id 1, wireType 2 =*/
      10).fork();

      for (var i = 0; i < message.workbenchInfo.length; ++i) {
        writer.int32(message.workbenchInfo[i]);
      }

      writer.ldelim();
    }

    return writer;
  };
  /**
   * Encodes the specified RefreshWorkBenchReply message, length delimited. Does not implicitly {@link RefreshWorkBenchReply.verify|verify} messages.
   * @function encodeDelimited
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {IRefreshWorkBenchReply} message RefreshWorkBenchReply message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  RefreshWorkBenchReply.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a RefreshWorkBenchReply message from the specified reader or buffer.
   * @function decode
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {RefreshWorkBenchReply} RefreshWorkBenchReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  RefreshWorkBenchReply.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.RefreshWorkBenchReply();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          if (!(message.workbenchInfo && message.workbenchInfo.length)) message.workbenchInfo = [];

          if ((tag & 7) === 2) {
            var end2 = reader.uint32() + reader.pos;

            while (reader.pos < end2) {
              message.workbenchInfo.push(reader.int32());
            }
          } else message.workbenchInfo.push(reader.int32());

          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a RefreshWorkBenchReply message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {RefreshWorkBenchReply} RefreshWorkBenchReply
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  RefreshWorkBenchReply.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a RefreshWorkBenchReply message.
   * @function verify
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  RefreshWorkBenchReply.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";

    if (message.workbenchInfo != null && message.hasOwnProperty("workbenchInfo")) {
      if (!Array.isArray(message.workbenchInfo)) return "workbenchInfo: array expected";

      for (var i = 0; i < message.workbenchInfo.length; ++i) {
        if (!$util.isInteger(message.workbenchInfo[i])) return "workbenchInfo: integer[] expected";
      }
    }

    return null;
  };
  /**
   * Creates a RefreshWorkBenchReply message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {RefreshWorkBenchReply} RefreshWorkBenchReply
   */


  RefreshWorkBenchReply.fromObject = function fromObject(object) {
    if (object instanceof $root.RefreshWorkBenchReply) return object;
    var message = new $root.RefreshWorkBenchReply();

    if (object.workbenchInfo) {
      if (!Array.isArray(object.workbenchInfo)) throw TypeError(".RefreshWorkBenchReply.workbenchInfo: array expected");
      message.workbenchInfo = [];

      for (var i = 0; i < object.workbenchInfo.length; ++i) {
        message.workbenchInfo[i] = object.workbenchInfo[i] | 0;
      }
    }

    return message;
  };
  /**
   * Creates a plain object from a RefreshWorkBenchReply message. Also converts values to other types if specified.
   * @function toObject
   * @memberof RefreshWorkBenchReply
   * @static
   * @param {RefreshWorkBenchReply} message RefreshWorkBenchReply
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  RefreshWorkBenchReply.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};
    if (options.arrays || options.defaults) object.workbenchInfo = [];

    if (message.workbenchInfo && message.workbenchInfo.length) {
      object.workbenchInfo = [];

      for (var j = 0; j < message.workbenchInfo.length; ++j) {
        object.workbenchInfo[j] = message.workbenchInfo[j];
      }
    }

    return object;
  };
  /**
   * Converts this RefreshWorkBenchReply to JSON.
   * @function toJSON
   * @memberof RefreshWorkBenchReply
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  RefreshWorkBenchReply.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return RefreshWorkBenchReply;
}();

$root.DecimalValue = function () {
  /**
   * Properties of a DecimalValue.
   * @exports IDecimalValue
   * @interface IDecimalValue
   * @property {number|Long|null} [uints] DecimalValue uints
   * @property {number|null} [nanos] DecimalValue nanos
   */

  /**
   * Constructs a new DecimalValue.
   * @exports DecimalValue
   * @classdesc Represents a DecimalValue.
   * @implements IDecimalValue
   * @constructor
   * @param {IDecimalValue=} [properties] Properties to set
   */
  function DecimalValue(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * DecimalValue uints.
   * @member {number|Long} uints
   * @memberof DecimalValue
   * @instance
   */


  DecimalValue.prototype.uints = $util.Long ? $util.Long.fromBits(0, 0, false) : 0;
  /**
   * DecimalValue nanos.
   * @member {number} nanos
   * @memberof DecimalValue
   * @instance
   */

  DecimalValue.prototype.nanos = 0;
  /**
   * Creates a new DecimalValue instance using the specified properties.
   * @function create
   * @memberof DecimalValue
   * @static
   * @param {IDecimalValue=} [properties] Properties to set
   * @returns {DecimalValue} DecimalValue instance
   */

  DecimalValue.create = function create(properties) {
    return new DecimalValue(properties);
  };
  /**
   * Encodes the specified DecimalValue message. Does not implicitly {@link DecimalValue.verify|verify} messages.
   * @function encode
   * @memberof DecimalValue
   * @static
   * @param {IDecimalValue} message DecimalValue message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  DecimalValue.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.uints != null && Object.hasOwnProperty.call(message, "uints")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int64(message.uints);
    if (message.nanos != null && Object.hasOwnProperty.call(message, "nanos")) writer.uint32(
    /* id 2, wireType 5 =*/
    21).sfixed32(message.nanos);
    return writer;
  };
  /**
   * Encodes the specified DecimalValue message, length delimited. Does not implicitly {@link DecimalValue.verify|verify} messages.
   * @function encodeDelimited
   * @memberof DecimalValue
   * @static
   * @param {IDecimalValue} message DecimalValue message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  DecimalValue.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a DecimalValue message from the specified reader or buffer.
   * @function decode
   * @memberof DecimalValue
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {DecimalValue} DecimalValue
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  DecimalValue.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.DecimalValue();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.uints = reader.int64();
          break;

        case 2:
          message.nanos = reader.sfixed32();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a DecimalValue message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof DecimalValue
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {DecimalValue} DecimalValue
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  DecimalValue.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a DecimalValue message.
   * @function verify
   * @memberof DecimalValue
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  DecimalValue.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.uints != null && message.hasOwnProperty("uints")) if (!$util.isInteger(message.uints) && !(message.uints && $util.isInteger(message.uints.low) && $util.isInteger(message.uints.high))) return "uints: integer|Long expected";
    if (message.nanos != null && message.hasOwnProperty("nanos")) if (!$util.isInteger(message.nanos)) return "nanos: integer expected";
    return null;
  };
  /**
   * Creates a DecimalValue message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof DecimalValue
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {DecimalValue} DecimalValue
   */


  DecimalValue.fromObject = function fromObject(object) {
    if (object instanceof $root.DecimalValue) return object;
    var message = new $root.DecimalValue();
    if (object.uints != null) if ($util.Long) (message.uints = $util.Long.fromValue(object.uints)).unsigned = false;else if (typeof object.uints === "string") message.uints = parseInt(object.uints, 10);else if (typeof object.uints === "number") message.uints = object.uints;else if (typeof object.uints === "object") message.uints = new $util.LongBits(object.uints.low >>> 0, object.uints.high >>> 0).toNumber();
    if (object.nanos != null) message.nanos = object.nanos | 0;
    return message;
  };
  /**
   * Creates a plain object from a DecimalValue message. Also converts values to other types if specified.
   * @function toObject
   * @memberof DecimalValue
   * @static
   * @param {DecimalValue} message DecimalValue
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  DecimalValue.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      if ($util.Long) {
        var _long2 = new $util.Long(0, 0, false);

        object.uints = options.longs === String ? _long2.toString() : options.longs === Number ? _long2.toNumber() : _long2;
      } else object.uints = options.longs === String ? "0" : 0;

      object.nanos = 0;
    }

    if (message.uints != null && message.hasOwnProperty("uints")) if (typeof message.uints === "number") object.uints = options.longs === String ? String(message.uints) : message.uints;else object.uints = options.longs === String ? $util.Long.prototype.toString.call(message.uints) : options.longs === Number ? new $util.LongBits(message.uints.low >>> 0, message.uints.high >>> 0).toNumber() : message.uints;
    if (message.nanos != null && message.hasOwnProperty("nanos")) object.nanos = message.nanos;
    return object;
  };
  /**
   * Converts this DecimalValue to JSON.
   * @function toJSON
   * @memberof DecimalValue
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  DecimalValue.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return DecimalValue;
}();

$root.UserInfo = function () {
  /**
   * Properties of a UserInfo.
   * @exports IUserInfo
   * @interface IUserInfo
   * @property {string|null} [account] UserInfo account
   * @property {string|null} [nickname] UserInfo nickname
   * @property {string|null} [avatarUrl] UserInfo avatarUrl
   * @property {IDecimalValue|null} [money] UserInfo money
   */

  /**
   * Constructs a new UserInfo.
   * @exports UserInfo
   * @classdesc Represents a UserInfo.
   * @implements IUserInfo
   * @constructor
   * @param {IUserInfo=} [properties] Properties to set
   */
  function UserInfo(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * UserInfo account.
   * @member {string} account
   * @memberof UserInfo
   * @instance
   */


  UserInfo.prototype.account = "";
  /**
   * UserInfo nickname.
   * @member {string} nickname
   * @memberof UserInfo
   * @instance
   */

  UserInfo.prototype.nickname = "";
  /**
   * UserInfo avatarUrl.
   * @member {string} avatarUrl
   * @memberof UserInfo
   * @instance
   */

  UserInfo.prototype.avatarUrl = "";
  /**
   * UserInfo money.
   * @member {IDecimalValue|null|undefined} money
   * @memberof UserInfo
   * @instance
   */

  UserInfo.prototype.money = null;
  /**
   * Creates a new UserInfo instance using the specified properties.
   * @function create
   * @memberof UserInfo
   * @static
   * @param {IUserInfo=} [properties] Properties to set
   * @returns {UserInfo} UserInfo instance
   */

  UserInfo.create = function create(properties) {
    return new UserInfo(properties);
  };
  /**
   * Encodes the specified UserInfo message. Does not implicitly {@link UserInfo.verify|verify} messages.
   * @function encode
   * @memberof UserInfo
   * @static
   * @param {IUserInfo} message UserInfo message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  UserInfo.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.account != null && Object.hasOwnProperty.call(message, "account")) writer.uint32(
    /* id 1, wireType 2 =*/
    10).string(message.account);
    if (message.nickname != null && Object.hasOwnProperty.call(message, "nickname")) writer.uint32(
    /* id 2, wireType 2 =*/
    18).string(message.nickname);
    if (message.avatarUrl != null && Object.hasOwnProperty.call(message, "avatarUrl")) writer.uint32(
    /* id 3, wireType 2 =*/
    26).string(message.avatarUrl);
    if (message.money != null && Object.hasOwnProperty.call(message, "money")) $root.DecimalValue.encode(message.money, writer.uint32(
    /* id 4, wireType 2 =*/
    34).fork()).ldelim();
    return writer;
  };
  /**
   * Encodes the specified UserInfo message, length delimited. Does not implicitly {@link UserInfo.verify|verify} messages.
   * @function encodeDelimited
   * @memberof UserInfo
   * @static
   * @param {IUserInfo} message UserInfo message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  UserInfo.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a UserInfo message from the specified reader or buffer.
   * @function decode
   * @memberof UserInfo
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {UserInfo} UserInfo
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  UserInfo.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.UserInfo();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.account = reader.string();
          break;

        case 2:
          message.nickname = reader.string();
          break;

        case 3:
          message.avatarUrl = reader.string();
          break;

        case 4:
          message.money = $root.DecimalValue.decode(reader, reader.uint32());
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a UserInfo message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof UserInfo
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {UserInfo} UserInfo
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  UserInfo.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a UserInfo message.
   * @function verify
   * @memberof UserInfo
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  UserInfo.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.account != null && message.hasOwnProperty("account")) if (!$util.isString(message.account)) return "account: string expected";
    if (message.nickname != null && message.hasOwnProperty("nickname")) if (!$util.isString(message.nickname)) return "nickname: string expected";
    if (message.avatarUrl != null && message.hasOwnProperty("avatarUrl")) if (!$util.isString(message.avatarUrl)) return "avatarUrl: string expected";

    if (message.money != null && message.hasOwnProperty("money")) {
      var error = $root.DecimalValue.verify(message.money);
      if (error) return "money." + error;
    }

    return null;
  };
  /**
   * Creates a UserInfo message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof UserInfo
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {UserInfo} UserInfo
   */


  UserInfo.fromObject = function fromObject(object) {
    if (object instanceof $root.UserInfo) return object;
    var message = new $root.UserInfo();
    if (object.account != null) message.account = String(object.account);
    if (object.nickname != null) message.nickname = String(object.nickname);
    if (object.avatarUrl != null) message.avatarUrl = String(object.avatarUrl);

    if (object.money != null) {
      if (typeof object.money !== "object") throw TypeError(".UserInfo.money: object expected");
      message.money = $root.DecimalValue.fromObject(object.money);
    }

    return message;
  };
  /**
   * Creates a plain object from a UserInfo message. Also converts values to other types if specified.
   * @function toObject
   * @memberof UserInfo
   * @static
   * @param {UserInfo} message UserInfo
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  UserInfo.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.account = "";
      object.nickname = "";
      object.avatarUrl = "";
      object.money = null;
    }

    if (message.account != null && message.hasOwnProperty("account")) object.account = message.account;
    if (message.nickname != null && message.hasOwnProperty("nickname")) object.nickname = message.nickname;
    if (message.avatarUrl != null && message.hasOwnProperty("avatarUrl")) object.avatarUrl = message.avatarUrl;
    if (message.money != null && message.hasOwnProperty("money")) object.money = $root.DecimalValue.toObject(message.money, options);
    return object;
  };
  /**
   * Converts this UserInfo to JSON.
   * @function toJSON
   * @memberof UserInfo
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  UserInfo.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return UserInfo;
}();

$root.UserMoney = function () {
  /**
   * Properties of a UserMoney.
   * @exports IUserMoney
   * @interface IUserMoney
   * @property {number|null} [coin] UserMoney coin
   * @property {number|null} [diamond] UserMoney diamond
   * @property {number|null} [money] UserMoney money
   */

  /**
   * Constructs a new UserMoney.
   * @exports UserMoney
   * @classdesc Represents a UserMoney.
   * @implements IUserMoney
   * @constructor
   * @param {IUserMoney=} [properties] Properties to set
   */
  function UserMoney(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * UserMoney coin.
   * @member {number} coin
   * @memberof UserMoney
   * @instance
   */


  UserMoney.prototype.coin = 0;
  /**
   * UserMoney diamond.
   * @member {number} diamond
   * @memberof UserMoney
   * @instance
   */

  UserMoney.prototype.diamond = 0;
  /**
   * UserMoney money.
   * @member {number} money
   * @memberof UserMoney
   * @instance
   */

  UserMoney.prototype.money = 0;
  /**
   * Creates a new UserMoney instance using the specified properties.
   * @function create
   * @memberof UserMoney
   * @static
   * @param {IUserMoney=} [properties] Properties to set
   * @returns {UserMoney} UserMoney instance
   */

  UserMoney.create = function create(properties) {
    return new UserMoney(properties);
  };
  /**
   * Encodes the specified UserMoney message. Does not implicitly {@link UserMoney.verify|verify} messages.
   * @function encode
   * @memberof UserMoney
   * @static
   * @param {IUserMoney} message UserMoney message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  UserMoney.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.coin != null && Object.hasOwnProperty.call(message, "coin")) writer.uint32(
    /* id 1, wireType 1 =*/
    9)["double"](message.coin);
    if (message.diamond != null && Object.hasOwnProperty.call(message, "diamond")) writer.uint32(
    /* id 2, wireType 1 =*/
    17)["double"](message.diamond);
    if (message.money != null && Object.hasOwnProperty.call(message, "money")) writer.uint32(
    /* id 3, wireType 5 =*/
    29)["float"](message.money);
    return writer;
  };
  /**
   * Encodes the specified UserMoney message, length delimited. Does not implicitly {@link UserMoney.verify|verify} messages.
   * @function encodeDelimited
   * @memberof UserMoney
   * @static
   * @param {IUserMoney} message UserMoney message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  UserMoney.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a UserMoney message from the specified reader or buffer.
   * @function decode
   * @memberof UserMoney
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {UserMoney} UserMoney
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  UserMoney.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.UserMoney();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.coin = reader["double"]();
          break;

        case 2:
          message.diamond = reader["double"]();
          break;

        case 3:
          message.money = reader["float"]();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a UserMoney message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof UserMoney
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {UserMoney} UserMoney
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  UserMoney.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a UserMoney message.
   * @function verify
   * @memberof UserMoney
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  UserMoney.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.coin != null && message.hasOwnProperty("coin")) if (typeof message.coin !== "number") return "coin: number expected";
    if (message.diamond != null && message.hasOwnProperty("diamond")) if (typeof message.diamond !== "number") return "diamond: number expected";
    if (message.money != null && message.hasOwnProperty("money")) if (typeof message.money !== "number") return "money: number expected";
    return null;
  };
  /**
   * Creates a UserMoney message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof UserMoney
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {UserMoney} UserMoney
   */


  UserMoney.fromObject = function fromObject(object) {
    if (object instanceof $root.UserMoney) return object;
    var message = new $root.UserMoney();
    if (object.coin != null) message.coin = Number(object.coin);
    if (object.diamond != null) message.diamond = Number(object.diamond);
    if (object.money != null) message.money = Number(object.money);
    return message;
  };
  /**
   * Creates a plain object from a UserMoney message. Also converts values to other types if specified.
   * @function toObject
   * @memberof UserMoney
   * @static
   * @param {UserMoney} message UserMoney
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  UserMoney.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.coin = 0;
      object.diamond = 0;
      object.money = 0;
    }

    if (message.coin != null && message.hasOwnProperty("coin")) object.coin = options.json && !isFinite(message.coin) ? String(message.coin) : message.coin;
    if (message.diamond != null && message.hasOwnProperty("diamond")) object.diamond = options.json && !isFinite(message.diamond) ? String(message.diamond) : message.diamond;
    if (message.money != null && message.hasOwnProperty("money")) object.money = options.json && !isFinite(message.money) ? String(message.money) : message.money;
    return object;
  };
  /**
   * Converts this UserMoney to JSON.
   * @function toJSON
   * @memberof UserMoney
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  UserMoney.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return UserMoney;
}();

$root.WorkBenchItemInfo = function () {
  /**
   * Properties of a WorkBenchItemInfo.
   * @exports IWorkBenchItemInfo
   * @interface IWorkBenchItemInfo
   * @property {number|null} [level] WorkBenchItemInfo level
   * @property {number|Long|null} [coinCost] WorkBenchItemInfo coinCost
   * @property {number|Long|null} [coinIncome] WorkBenchItemInfo coinIncome
   */

  /**
   * Constructs a new WorkBenchItemInfo.
   * @exports WorkBenchItemInfo
   * @classdesc Represents a WorkBenchItemInfo.
   * @implements IWorkBenchItemInfo
   * @constructor
   * @param {IWorkBenchItemInfo=} [properties] Properties to set
   */
  function WorkBenchItemInfo(properties) {
    if (properties) for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i) {
      if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }
  }
  /**
   * WorkBenchItemInfo level.
   * @member {number} level
   * @memberof WorkBenchItemInfo
   * @instance
   */


  WorkBenchItemInfo.prototype.level = 0;
  /**
   * WorkBenchItemInfo coinCost.
   * @member {number|Long} coinCost
   * @memberof WorkBenchItemInfo
   * @instance
   */

  WorkBenchItemInfo.prototype.coinCost = $util.Long ? $util.Long.fromBits(0, 0, false) : 0;
  /**
   * WorkBenchItemInfo coinIncome.
   * @member {number|Long} coinIncome
   * @memberof WorkBenchItemInfo
   * @instance
   */

  WorkBenchItemInfo.prototype.coinIncome = $util.Long ? $util.Long.fromBits(0, 0, false) : 0;
  /**
   * Creates a new WorkBenchItemInfo instance using the specified properties.
   * @function create
   * @memberof WorkBenchItemInfo
   * @static
   * @param {IWorkBenchItemInfo=} [properties] Properties to set
   * @returns {WorkBenchItemInfo} WorkBenchItemInfo instance
   */

  WorkBenchItemInfo.create = function create(properties) {
    return new WorkBenchItemInfo(properties);
  };
  /**
   * Encodes the specified WorkBenchItemInfo message. Does not implicitly {@link WorkBenchItemInfo.verify|verify} messages.
   * @function encode
   * @memberof WorkBenchItemInfo
   * @static
   * @param {IWorkBenchItemInfo} message WorkBenchItemInfo message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  WorkBenchItemInfo.encode = function encode(message, writer) {
    if (!writer) writer = $Writer.create();
    if (message.level != null && Object.hasOwnProperty.call(message, "level")) writer.uint32(
    /* id 1, wireType 0 =*/
    8).int32(message.level);
    if (message.coinCost != null && Object.hasOwnProperty.call(message, "coinCost")) writer.uint32(
    /* id 2, wireType 0 =*/
    16).int64(message.coinCost);
    if (message.coinIncome != null && Object.hasOwnProperty.call(message, "coinIncome")) writer.uint32(
    /* id 3, wireType 0 =*/
    24).int64(message.coinIncome);
    return writer;
  };
  /**
   * Encodes the specified WorkBenchItemInfo message, length delimited. Does not implicitly {@link WorkBenchItemInfo.verify|verify} messages.
   * @function encodeDelimited
   * @memberof WorkBenchItemInfo
   * @static
   * @param {IWorkBenchItemInfo} message WorkBenchItemInfo message or plain object to encode
   * @param {$protobuf.Writer} [writer] Writer to encode to
   * @returns {$protobuf.Writer} Writer
   */


  WorkBenchItemInfo.encodeDelimited = function encodeDelimited(message, writer) {
    return this.encode(message, writer).ldelim();
  };
  /**
   * Decodes a WorkBenchItemInfo message from the specified reader or buffer.
   * @function decode
   * @memberof WorkBenchItemInfo
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @param {number} [length] Message length if known beforehand
   * @returns {WorkBenchItemInfo} WorkBenchItemInfo
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  WorkBenchItemInfo.decode = function decode(reader, length) {
    if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
    var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.WorkBenchItemInfo();

    while (reader.pos < end) {
      var tag = reader.uint32();

      switch (tag >>> 3) {
        case 1:
          message.level = reader.int32();
          break;

        case 2:
          message.coinCost = reader.int64();
          break;

        case 3:
          message.coinIncome = reader.int64();
          break;

        default:
          reader.skipType(tag & 7);
          break;
      }
    }

    return message;
  };
  /**
   * Decodes a WorkBenchItemInfo message from the specified reader or buffer, length delimited.
   * @function decodeDelimited
   * @memberof WorkBenchItemInfo
   * @static
   * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
   * @returns {WorkBenchItemInfo} WorkBenchItemInfo
   * @throws {Error} If the payload is not a reader or valid buffer
   * @throws {$protobuf.util.ProtocolError} If required fields are missing
   */


  WorkBenchItemInfo.decodeDelimited = function decodeDelimited(reader) {
    if (!(reader instanceof $Reader)) reader = new $Reader(reader);
    return this.decode(reader, reader.uint32());
  };
  /**
   * Verifies a WorkBenchItemInfo message.
   * @function verify
   * @memberof WorkBenchItemInfo
   * @static
   * @param {Object.<string,*>} message Plain object to verify
   * @returns {string|null} `null` if valid, otherwise the reason why it is not
   */


  WorkBenchItemInfo.verify = function verify(message) {
    if (typeof message !== "object" || message === null) return "object expected";
    if (message.level != null && message.hasOwnProperty("level")) if (!$util.isInteger(message.level)) return "level: integer expected";
    if (message.coinCost != null && message.hasOwnProperty("coinCost")) if (!$util.isInteger(message.coinCost) && !(message.coinCost && $util.isInteger(message.coinCost.low) && $util.isInteger(message.coinCost.high))) return "coinCost: integer|Long expected";
    if (message.coinIncome != null && message.hasOwnProperty("coinIncome")) if (!$util.isInteger(message.coinIncome) && !(message.coinIncome && $util.isInteger(message.coinIncome.low) && $util.isInteger(message.coinIncome.high))) return "coinIncome: integer|Long expected";
    return null;
  };
  /**
   * Creates a WorkBenchItemInfo message from a plain object. Also converts values to their respective internal types.
   * @function fromObject
   * @memberof WorkBenchItemInfo
   * @static
   * @param {Object.<string,*>} object Plain object
   * @returns {WorkBenchItemInfo} WorkBenchItemInfo
   */


  WorkBenchItemInfo.fromObject = function fromObject(object) {
    if (object instanceof $root.WorkBenchItemInfo) return object;
    var message = new $root.WorkBenchItemInfo();
    if (object.level != null) message.level = object.level | 0;
    if (object.coinCost != null) if ($util.Long) (message.coinCost = $util.Long.fromValue(object.coinCost)).unsigned = false;else if (typeof object.coinCost === "string") message.coinCost = parseInt(object.coinCost, 10);else if (typeof object.coinCost === "number") message.coinCost = object.coinCost;else if (typeof object.coinCost === "object") message.coinCost = new $util.LongBits(object.coinCost.low >>> 0, object.coinCost.high >>> 0).toNumber();
    if (object.coinIncome != null) if ($util.Long) (message.coinIncome = $util.Long.fromValue(object.coinIncome)).unsigned = false;else if (typeof object.coinIncome === "string") message.coinIncome = parseInt(object.coinIncome, 10);else if (typeof object.coinIncome === "number") message.coinIncome = object.coinIncome;else if (typeof object.coinIncome === "object") message.coinIncome = new $util.LongBits(object.coinIncome.low >>> 0, object.coinIncome.high >>> 0).toNumber();
    return message;
  };
  /**
   * Creates a plain object from a WorkBenchItemInfo message. Also converts values to other types if specified.
   * @function toObject
   * @memberof WorkBenchItemInfo
   * @static
   * @param {WorkBenchItemInfo} message WorkBenchItemInfo
   * @param {$protobuf.IConversionOptions} [options] Conversion options
   * @returns {Object.<string,*>} Plain object
   */


  WorkBenchItemInfo.toObject = function toObject(message, options) {
    if (!options) options = {};
    var object = {};

    if (options.defaults) {
      object.level = 0;

      if ($util.Long) {
        var _long3 = new $util.Long(0, 0, false);

        object.coinCost = options.longs === String ? _long3.toString() : options.longs === Number ? _long3.toNumber() : _long3;
      } else object.coinCost = options.longs === String ? "0" : 0;

      if ($util.Long) {
        var _long3 = new $util.Long(0, 0, false);

        object.coinIncome = options.longs === String ? _long3.toString() : options.longs === Number ? _long3.toNumber() : _long3;
      } else object.coinIncome = options.longs === String ? "0" : 0;
    }

    if (message.level != null && message.hasOwnProperty("level")) object.level = message.level;
    if (message.coinCost != null && message.hasOwnProperty("coinCost")) if (typeof message.coinCost === "number") object.coinCost = options.longs === String ? String(message.coinCost) : message.coinCost;else object.coinCost = options.longs === String ? $util.Long.prototype.toString.call(message.coinCost) : options.longs === Number ? new $util.LongBits(message.coinCost.low >>> 0, message.coinCost.high >>> 0).toNumber() : message.coinCost;
    if (message.coinIncome != null && message.hasOwnProperty("coinIncome")) if (typeof message.coinIncome === "number") object.coinIncome = options.longs === String ? String(message.coinIncome) : message.coinIncome;else object.coinIncome = options.longs === String ? $util.Long.prototype.toString.call(message.coinIncome) : options.longs === Number ? new $util.LongBits(message.coinIncome.low >>> 0, message.coinIncome.high >>> 0).toNumber() : message.coinIncome;
    return object;
  };
  /**
   * Converts this WorkBenchItemInfo to JSON.
   * @function toJSON
   * @memberof WorkBenchItemInfo
   * @instance
   * @returns {Object.<string,*>} JSON object
   */


  WorkBenchItemInfo.prototype.toJSON = function toJSON() {
    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
  };

  return WorkBenchItemInfo;
}();

module.exports = $root;

cc._RF.pop();