"use strict";
cc._RF.push(module, '4c62a0jAhdDIp65t6olWl8V', 'UICollection_Auto');
// Script/AutoScripts/UICollection_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UICollection_Auto = /** @class */ (function (_super) {
    __extends(UICollection_Auto, _super);
    function UICollection_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ContentNode = null;
        _this.WenFangSiBaoProgress = null;
        _this.WenFangSiBaoProgressLab = null;
        _this.BtnWenFangSiBao = null;
        _this.WenFangSiBaoGetNumTipLab = null;
        _this.SiJunZiProgress = null;
        _this.SiJunZiProgressLab = null;
        _this.BtnSiJunZi = null;
        _this.SiJunZiGetNumTipLab = null;
        _this.MingZhuProgress = null;
        _this.MingZhuProgressLab = null;
        _this.BtnMingZhu = null;
        _this.MingZhuGetNumTipLab = null;
        _this.ShenShouProgress = null;
        _this.ShenShouProgressLab = null;
        _this.BtnShenShou = null;
        _this.ShenShouGetNumTipLab = null;
        _this.BaDaJiaProgress = null;
        _this.BaDaJiaProgressLab = null;
        _this.BtnBaDaJia = null;
        _this.BaDaJiaGetNumTipLab = null;
        _this.JinChanProgress = null;
        _this.JinChanProgressLab = null;
        _this.JinChanGetNumTipLab = null;
        _this.PiXiuProgress = null;
        _this.PiXiuProgressLab = null;
        _this.BtnPiXiu = null;
        _this.PiXiuGetNumTipLab = null;
        _this.JinLongProgress = null;
        _this.JinLongProgressLab = null;
        _this.BtnJinLong = null;
        _this.JinLongGetNumTipLab = null;
        _this.CloseBtn = null;
        return _this;
    }
    __decorate([
        property(cc.Node)
    ], UICollection_Auto.prototype, "ContentNode", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "WenFangSiBaoProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "WenFangSiBaoProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnWenFangSiBao", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "WenFangSiBaoGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "SiJunZiProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "SiJunZiProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnSiJunZi", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "SiJunZiGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "MingZhuProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "MingZhuProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnMingZhu", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "MingZhuGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "ShenShouProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "ShenShouProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnShenShou", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "ShenShouGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "BaDaJiaProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "BaDaJiaProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnBaDaJia", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "BaDaJiaGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "JinChanProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "JinChanProgressLab", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "JinChanGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "PiXiuProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "PiXiuProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnPiXiu", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "PiXiuGetNumTipLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UICollection_Auto.prototype, "JinLongProgress", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "JinLongProgressLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "BtnJinLong", void 0);
    __decorate([
        property(cc.Label)
    ], UICollection_Auto.prototype, "JinLongGetNumTipLab", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UICollection_Auto.prototype, "CloseBtn", void 0);
    UICollection_Auto = __decorate([
        ccclass
    ], UICollection_Auto);
    return UICollection_Auto;
}(cc.Component));
exports.default = UICollection_Auto;

cc._RF.pop();