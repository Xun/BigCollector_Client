"use strict";
cc._RF.push(module, 'dbcbbCGpTpNgrFvVqCfFAK2', 'UIFriendMonerDetail_Auto');
// Script/AutoScripts/UIFriendMonerDetail_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendMonerDetail_Auto = /** @class */ (function (_super) {
    __extends(UIFriendMonerDetail_Auto, _super);
    function UIFriendMonerDetail_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.TodayGetMoneyLab = null;
        _this.TotalGetMoneyLab = null;
        _this.ContentNode = null;
        _this.ContentViewNode = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIFriendMonerDetail_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriendMonerDetail_Auto.prototype, "TodayGetMoneyLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIFriendMonerDetail_Auto.prototype, "TotalGetMoneyLab", void 0);
    __decorate([
        property(cc.Node)
    ], UIFriendMonerDetail_Auto.prototype, "ContentNode", void 0);
    __decorate([
        property(cc.Node)
    ], UIFriendMonerDetail_Auto.prototype, "ContentViewNode", void 0);
    UIFriendMonerDetail_Auto = __decorate([
        ccclass
    ], UIFriendMonerDetail_Auto);
    return UIFriendMonerDetail_Auto;
}(cc.Component));
exports.default = UIFriendMonerDetail_Auto;

cc._RF.pop();