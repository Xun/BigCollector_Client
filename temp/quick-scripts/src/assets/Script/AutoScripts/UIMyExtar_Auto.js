"use strict";
cc._RF.push(module, 'ecf0c8eTQxFtLgIHpoFKg8C', 'UIMyExtar_Auto');
// Script/AutoScripts/UIMyExtar_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMyExtar_Auto = /** @class */ (function (_super) {
    __extends(UIMyExtar_Auto, _super);
    function UIMyExtar_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BtnClose = null;
        _this.TodayExtarMoneyNumLab = null;
        _this.TodayExtarAddUserNumLab = null;
        _this.MonthExtarMoneyNumLab = null;
        _this.MonthExtarAddUserNumLab = null;
        _this.TotalExtarMoneyNumLab = null;
        _this.TotalExtarAddUserNumLab = null;
        _this.JieDuanNumLab = null;
        _this.JDCurExtarMoneyNumLab = null;
        _this.JDTotalExtarMoneyNumLab = null;
        _this.CompleteMoneyLab = null;
        _this.JDPS = null;
        _this.MultLab = null;
        return _this;
    }
    __decorate([
        property(ButtonPlus_1.default)
    ], UIMyExtar_Auto.prototype, "BtnClose", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "TodayExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "TodayExtarAddUserNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "MonthExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "MonthExtarAddUserNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "TotalExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "TotalExtarAddUserNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "JieDuanNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "JDCurExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "JDTotalExtarMoneyNumLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "CompleteMoneyLab", void 0);
    __decorate([
        property(cc.ProgressBar)
    ], UIMyExtar_Auto.prototype, "JDPS", void 0);
    __decorate([
        property(cc.Label)
    ], UIMyExtar_Auto.prototype, "MultLab", void 0);
    UIMyExtar_Auto = __decorate([
        ccclass
    ], UIMyExtar_Auto);
    return UIMyExtar_Auto;
}(cc.Component));
exports.default = UIMyExtar_Auto;

cc._RF.pop();