"use strict";
cc._RF.push(module, '5a9d6iPD+BFKJM7cZ885TPW', 'UIBuyTip_Auto');
// Script/AutoScripts/UIBuyTip_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIBuyTip_Auto = /** @class */ (function (_super) {
    __extends(UIBuyTip_Auto, _super);
    function UIBuyTip_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.BuyTipLab = null;
        _this.BuyMoneyTypeSp = null;
        _this.BuyName = null;
        _this.BtnSure = null;
        _this.BuyIconTypeSp = null;
        _this.BtnClose = null;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], UIBuyTip_Auto.prototype, "BuyTipLab", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIBuyTip_Auto.prototype, "BuyMoneyTypeSp", void 0);
    __decorate([
        property(cc.Label)
    ], UIBuyTip_Auto.prototype, "BuyName", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIBuyTip_Auto.prototype, "BtnSure", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIBuyTip_Auto.prototype, "BuyIconTypeSp", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIBuyTip_Auto.prototype, "BtnClose", void 0);
    UIBuyTip_Auto = __decorate([
        ccclass
    ], UIBuyTip_Auto);
    return UIBuyTip_Auto;
}(cc.Component));
exports.default = UIBuyTip_Auto;

cc._RF.pop();