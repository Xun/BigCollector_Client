"use strict";
cc._RF.push(module, '5d508kJilBGOpda13fF4eYa', 'UIUnlockNewLevel_Auto');
// Script/AutoScripts/UIUnlockNewLevel_Auto.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonPlus_1 = require("./../Common/Components/ButtonPlus");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIUnlockNewLevel_Auto = /** @class */ (function (_super) {
    __extends(UIUnlockNewLevel_Auto, _super);
    function UIUnlockNewLevel_Auto() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.NewLevelLab = null;
        _this.NewNameLab = null;
        _this.CurLevelProductionNumLab = null;
        _this.CurLevelProductionIconSp = null;
        _this.YinPiaoNode = null;
        _this.YinPiaoNumLab = null;
        _this.SuiPianNode = null;
        _this.SuiPianNameLab = null;
        _this.SuiPianIconSp = null;
        _this.CloseBtn = null;
        return _this;
    }
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "NewLevelLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "NewNameLab", void 0);
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "CurLevelProductionNumLab", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIUnlockNewLevel_Auto.prototype, "CurLevelProductionIconSp", void 0);
    __decorate([
        property(cc.Node)
    ], UIUnlockNewLevel_Auto.prototype, "YinPiaoNode", void 0);
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "YinPiaoNumLab", void 0);
    __decorate([
        property(cc.Node)
    ], UIUnlockNewLevel_Auto.prototype, "SuiPianNode", void 0);
    __decorate([
        property(cc.Label)
    ], UIUnlockNewLevel_Auto.prototype, "SuiPianNameLab", void 0);
    __decorate([
        property(cc.Sprite)
    ], UIUnlockNewLevel_Auto.prototype, "SuiPianIconSp", void 0);
    __decorate([
        property(ButtonPlus_1.default)
    ], UIUnlockNewLevel_Auto.prototype, "CloseBtn", void 0);
    UIUnlockNewLevel_Auto = __decorate([
        ccclass
    ], UIUnlockNewLevel_Auto);
    return UIUnlockNewLevel_Auto;
}(cc.Component));
exports.default = UIUnlockNewLevel_Auto;

cc._RF.pop();