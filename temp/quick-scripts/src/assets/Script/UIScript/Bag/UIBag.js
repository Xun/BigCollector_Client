"use strict";
cc._RF.push(module, '74ea71pHThDZ6ERDnoqDKMw', 'UIBag');
// Script/UIScript/Bag/UIBag.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var UIToast_1 = require("../UIToast");
var ItemBag_1 = require("./ItemBag");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIBag = /** @class */ (function (_super) {
    __extends(UIBag, _super);
    function UIBag() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listBag = null;
        return _this;
    }
    UIBag.prototype.onLoad = function () {
        // GameMgr.dataModalMgr.BagInfo.list_item = [{ item_sp: "imgs/itemProduce/item1", item_name: "毛笔碎片", item_num: 1 }];
        // this.listBag.numItems = GameMgr.dataModalMgr.BagInfo.list_item.length;
        GameMgr_1.default.dataModalMgr.BagInfo.cur_bag_capacity = GameMgr_1.default.dataModalMgr.BagInfo.list_item.length;
    };
    UIBag.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.AddBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIBagAdd);
        }, this);
        this.view.AddBtn.addClick(function () {
            _this.onAddClick();
        }, this);
        this.listBag.numItems = GameMgr_1.default.dataModalMgr.BagInfo.list_item.length;
        GameMgr_1.default.dataModalMgr.BagInfo.cur_bag_capacity = GameMgr_1.default.dataModalMgr.BagInfo.list_item.length;
        this.view.BagNumLab.string = GameMgr_1.default.dataModalMgr.BagInfo.cur_bag_capacity + "/" + GameMgr_1.default.dataModalMgr.BagInfo.total_bag_capacity;
    };
    UIBag.prototype.onAddClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("BagInfo", { type: 2 })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.BagInfo.list_item = data.res.bagList;
                        GameMgr_1.default.dataModalMgr.BagInfo.total_bag_capacity = data.res.bagVol;
                        return [2 /*return*/];
                }
            });
        });
    };
    //垂直列表渲染器
    UIBag.prototype.onListBagRender = function (item, idx) {
        item.getComponent(ItemBag_1.default).setData(GameMgr_1.default.dataModalMgr.BagInfo.list_item[idx]);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIBag.prototype, "listBag", void 0);
    UIBag = __decorate([
        ccclass
    ], UIBag);
    return UIBag;
}(UIForm_1.UIWindow));
exports.default = UIBag;

cc._RF.pop();