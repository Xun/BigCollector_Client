"use strict";
cc._RF.push(module, '2bb095/a31FHo6dokcSVtRb', 'GenerateQR');
// Script/UIScript/Share/GenerateQR.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var GenerateQR = /** @class */ (function (_super) {
    __extends(GenerateQR, _super);
    function GenerateQR() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GenerateQR.prototype.onLoad = function () {
        this.init('http://baidu.com');
    };
    GenerateQR.prototype.init = function (url) {
        var ctx = this.node.addComponent(cc.Graphics);
        if (typeof (url) !== 'string') {
            console.log('url is not string', url);
            return;
        }
        this.createQR(ctx, url);
    };
    GenerateQR.prototype.createQR = function (ctx, url) {
        var qrcode = new QRCode(-1, QRErrorCorrectLevel.H);
        qrcode.addData(url);
        qrcode.make();
        ctx.fillColor = cc.Color.BLACK;
        //块宽高
        var tileW = this.node.width / qrcode.getModuleCount();
        var tileH = this.node.height / qrcode.getModuleCount();
        // 用Graphics画二维码
        for (var row = 0; row < qrcode.getModuleCount(); row++) {
            for (var col = 0; col < qrcode.getModuleCount(); col++) {
                if (qrcode.isDark(row, col)) {
                    // ctx.fillColor = cc.Color.BLACK;
                    var w = (Math.ceil((col + 1) * tileW) - Math.floor(col * tileW));
                    var h = (Math.ceil((row + 1) * tileW) - Math.floor(row * tileW));
                    ctx.rect(Math.round(col * tileW) - this.node.width / 2, Math.round(row * tileH) - this.node.height / 2, w, h);
                    ctx.fill();
                }
            }
        }
    };
    GenerateQR = __decorate([
        ccclass
    ], GenerateQR);
    return GenerateQR;
}(cc.Component));
exports.default = GenerateQR;

cc._RF.pop();