"use strict";
cc._RF.push(module, 'fcc2aY0G6VMe58TO/xv8+KX', 'UIShare');
// Script/UIScript/Share/UIShare.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIShare = /** @class */ (function (_super) {
    __extends(UIShare, _super);
    function UIShare() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.camereCapture = null;
        _this.texture2 = null;
        _this._width = 0;
        _this._height = 0;
        return _this;
    }
    UIShare.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.PhotoBtn.addClick(this.onCapture, this);
    };
    UIShare.prototype.onCapture = function () {
        var node = this.screenShot(this.view.contentNode);
        node.x = 0;
        node.y = 0;
        node.parent = cc.director.getScene();
        this.captureAction(node, node.width, node.height);
    };
    /**
     * 截图
     * @param targetNode  截图目标节点，如果为null则表示截全屏
     * @returns 返回截屏图片的node
     */
    UIShare.prototype.screenShot = function (targetNode) {
        if (targetNode === void 0) { targetNode = null; }
        //创建新的texture
        var texture = new cc.RenderTexture();
        texture.initWithSize(cc.winSize.width, cc.winSize.height, cc.game._renderContext.STENCIL_INDEX8);
        //创建新的spriteFrame
        var spriteFrame = new cc.SpriteFrame();
        var nodeX = cc.winSize.width / 2 + targetNode.x - targetNode.width / 2;
        var nodeY = cc.winSize.height / 2 + targetNode.y - targetNode.height / 2;
        var nodeWidth = targetNode.width;
        var nodeHeight = targetNode.height;
        if (targetNode == null) {
            spriteFrame.setTexture(texture);
        }
        else {
            //只显示node部分的图片
            spriteFrame.setTexture(texture, new cc.Rect(nodeX, nodeY, nodeWidth, nodeHeight));
        }
        //创建新的node
        var node = new cc.Node();
        var sprite = node.addComponent(cc.Sprite);
        sprite.spriteFrame = spriteFrame;
        //截图是反的，这里将截图scaleY取反，这样就是正的了
        // sprite.node.scaleY = - Math.abs(sprite.node.scaleY);
        sprite.spriteFrame.setFlipY(true);
        // let buffer = new ArrayBuffer(targetNode.width * targetNode.height * 4);
        // let data = new Uint8Array(buffer);
        // texture.readPixels(data, nodeX, nodeY, nodeWidth, nodeHeight);
        //手动渲染camera
        // camera.cullingMask = 0xffffffff;
        this.camereCapture.targetTexture = texture;
        this.camereCapture.render();
        this.camereCapture.targetTexture = null;
        return node;
    };
    UIShare.prototype.captureAction = function (capture, width, height) {
        var scaleAction = cc.scaleTo(1, 0.5);
        var targetPos = cc.v2(this.node.width / 2 + width / 2 - 50, height + height / 2);
        var moveAction = cc.moveTo(1, targetPos);
        var spawn = cc.spawn(scaleAction, moveAction);
        capture.runAction(spawn);
        var blinkAction = cc.blink(0.1, 1);
        // scene action
        this.node.runAction(blinkAction);
    };
    UIShare.prototype.saveFile = function (picData) {
        if (CC_JSB) {
            // let filePath = jsb.fileUtils.getWritablePath() + 'render_to_sprite_image.png';
            // let success = jsb.saveImageData(picData, this._width, this._height, filePath)
            // if (success) {
            //     cc.log("save image data success, file: " + filePath);
            // }
            // else {
            //     cc.error("save image data failed!");
            // }
        }
    };
    __decorate([
        property(cc.Camera)
    ], UIShare.prototype, "camereCapture", void 0);
    UIShare = __decorate([
        ccclass
    ], UIShare);
    return UIShare;
}(UIForm_1.UIWindow));
exports.default = UIShare;

cc._RF.pop();