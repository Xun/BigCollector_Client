"use strict";
cc._RF.push(module, 'ce700EVbt5MTrg0Llyxj9zb', 'UIHome');
// Script/UIScript/Home/UIHome.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var DataUser_1 = require("../../DataModal/DataUser");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var SoundMgr_1 = require("../../Manager/SoundMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var TAG_VIRTUAL_MOVE = 10001; //虚拟道具
var SPEED_VIRUTAL_MOVE = 2000;
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIHome = /** @class */ (function (_super) {
    __extends(UIHome, _super);
    function UIHome() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.itemProducePrefab = null;
        //是否自动合成
        _this._isCombining = false;
        //是否可以拖拽，当前是否在使用
        _this._isCanDrag = false;
        //当前拖拽节点
        _this._currentNode = null;
        //拖拽时的虚拟节点
        _this._virtualNode = null;
        return _this;
    }
    UIHome.prototype.load = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                SoundMgr_1.default.inst.playMusic("Sounds/bg");
                return [2 /*return*/, null];
            });
        });
    };
    UIHome.prototype.start = function () {
        var _this = this;
        // if (AdapterMgr.inst.checkIsIphoneX()) {
        //     AdapterMgr.inst.adapteByType(AdapterType.Top, this.view.topNode, 20);
        // }
        RpcConent_1.apiClient.listenMsg("LvUp", function (data) {
            _this.onLvUp(data);
        });
        RpcConent_1.apiClient.listenMsg("UpdateUserInfo", function (data) {
            _this.upDataUserInfo(data);
        });
        this.item_pos_arr = [new cc.Vec2(0, 360), new cc.Vec2(-175, 270), new cc.Vec2(175, 270), new cc.Vec2(0, 180), new cc.Vec2(-175, 90), new cc.Vec2(175, 90), new cc.Vec2(0, 0), new cc.Vec2(-175, -90), new cc.Vec2(175, -90), new cc.Vec2(0, -180), new cc.Vec2(-175, -270), new cc.Vec2(175, -270)];
        this.initEvent();
        this.initData();
        this.initClickEvent();
        this.initWorkbench();
        // GameMgr.client.on("user_info_modified", async (payload) => {
        //     GameMgr.dataModalMgr.UserInfo.userInfos = UserInfo.decode(payload);
        // })
        // let s: string = NativeMgr.getStr("OneClass", "FucTwo");
        // console.log("sssss:" + s);
        // this.adaptiveNoteLayout();
        // this.scheduleOnce(() => {
        //     EventCenter.emit("shouguide", CocosHelper.findChildInNode("_ButtonPlus$SettingBtn", this.node));
        // }, 1)
    };
    UIHome.prototype.upDataUserInfo = function (data) {
        this.view.CoinTxt.string = data.coin.toString();
        this.view.DiamondTxt.string = data.diamond.toString();
    };
    UIHome.prototype.onLvUp = function (datas) {
        if (datas.type === 1) {
            FormMgr_1.default.open(UIConfig_1.default.UIUnlockNewLevel, datas);
        }
    };
    UIHome.prototype.adaptiveNoteLayout = function () {
        var winSize = cc.winSize; //获取当前游戏窗口大小
        cc.log("--当前游戏窗口大小  w:" + winSize.width + "   h:" + winSize.height);
        var viewSize = cc.view.getFrameSize();
        cc.log("--视图边框尺寸：w:" + viewSize.width + "  h:" + viewSize.height);
        var canvasSize = cc.view.getCanvasSize(); //视图中canvas尺寸
        cc.log("--视图中canvas尺寸  w:" + canvasSize.width + "  H:" + canvasSize.height);
        var visibleSize = cc.view.getVisibleSize();
        cc.log("--视图中窗口可见区域的尺寸 w:" + visibleSize.width + "   h:" + visibleSize.height);
        var designSize = cc.view.getDesignResolutionSize();
        cc.log("--设计分辨率：" + designSize.width + "    h: " + designSize.height);
        cc.log("--当前节点的尺寸 w:" + this.node.width + "   h:" + this.node.height);
    };
    //初始化点击事件
    UIHome.prototype.initEvent = function () {
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.view.WorkContent.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    };
    //初始化点击事件
    UIHome.prototype.initClickEvent = function () {
        this.view.BuyBtn.addClick(this.onBuyProduceClick, this);
        this.view.CombineAutoBtn.addClick(this.onBtnCombineAutoClick, this);
        this.view.BagBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIBag);
            // NativeMgr.callNativeClass("OneClass", "FucOne", "test01", 11111, true, (str, num, b) => { console.log(str, num, b); })
        }, this);
        // this.view.SettingBtn.addClick(() => {
        // EventCenter.emit("shouguide", CocosHelper.findChildInNode("_ButtonPlus$BagBtn", this.node));
        // }, this);
        this.view.SignInBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UISignIn);
        }, this);
        this.view.DrawBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UILottery);
        }, this);
        this.view.RankBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIRank);
        }, this);
        this.view.CollectBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UICollection);
        }, this);
        this.view.ShopBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIShop);
            // FormMgr.open(UIConfig.UIDeal)
        }, this);
        this.view.MyBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIMy);
        }, this);
        this.view.TaskBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UITask);
        }, this);
        this.view.FriendBtn.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIFriend);
        }, this);
    };
    //初始化显示数据
    UIHome.prototype.initData = function () {
        // CocosHelper.loadHead("https://pic.sogou.com/d?query=%E5%A4%B4%E5%83%8F&forbidqc=&entityid=&preQuery=&rawQuery=&queryList=&st=191&did=5", this.view.HeadSp);
        // GameMgr.dataModalMgr.UserInfo.userWorkbench = [1, 1, 2, 3, 4, 4, 5, 5, 6, 7];
        this.view.BuyCoinNumTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userCurNeedBuyCoinNum.toString();
        this.view.MakeMoneyTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userMakeCoin.toString();
        this.view.CoinTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userCoin.toString();
        this.view.DiamondTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userDiamond.toString();
        this.view.MoneyTxt.string = GameMgr_1.default.dataModalMgr.UserInfo.userMoney.toString();
    };
    //初始化工作台信息
    UIHome.prototype.initWorkbench = function () {
        var arrChildren = this.view.WorkContent.children;
        if (arrChildren.length > 0) {
            //已经创建过，不需要再创建
            return;
        }
        for (var idx = 0; idx < GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length; idx++) {
            var item_node = cc.instantiate(this.itemProducePrefab);
            this.view.WorkContent.addChild(item_node);
            item_node.setPosition(this.item_pos_arr[idx]);
        }
        this.view.WorkContent.children.forEach(function (itemNode, index) {
            var itemInfo = itemNode.getComponent('ItemProduce');
            if (index < GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length) {
                itemInfo.setWorkbenchItemInfo(index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[index]);
            }
        }, this);
    };
    UIHome.prototype.onTouchStart = function (touchEvent) {
        this._currentNode = this.getCurrentNodeByTouchPos(touchEvent.getLocation());
        if (this._currentNode) {
            var ItemProduce_1 = this._currentNode.getComponent('ItemProduce');
            this._isCanDrag = ItemProduce_1.dragStart();
            if (this._isCanDrag) {
                var currentWorldPos = this._currentNode.convertToWorldSpaceAR(cc.v2(0, 0));
                this._offsetPos = currentWorldPos.sub(touchEvent.getLocation());
                this.showVirtualItem(ItemProduce_1.getInfo(), touchEvent.getLocation());
                this.showProduce(ItemProduce_1);
            }
        }
    };
    UIHome.prototype.onTouchMove = function (touchEvent) {
        if (!this._isCanDrag) {
            return;
        }
        this.updateVirutalItemPos(touchEvent.getLocation());
    };
    UIHome.prototype.onTouchEnd = function (touchEvent) {
        if (!this._currentNode) {
            return;
        }
        this.hideProduce();
        var cakeItem = this._currentNode.getComponent('ItemProduce');
        cakeItem.dragOver();
        var touchEndNode = this.getCurrentNodeByTouchPos(touchEvent.getLocation());
        if (this._currentNode === touchEndNode) {
            //属于点击自己
            this.hideVirtualItem(true);
            this._currentNode = null;
            return;
        }
        if (!this._isCanDrag) {
            return;
        }
        if (!touchEndNode) {
            //TODO 判断是否拖拽到生产台上，没有则是拖拽到无效位置
            var isDragToRecovery = this.checkIsDragToRecovery(cakeItem._index, touchEvent.getLocation());
            if (!isDragToRecovery) {
                this.hideVirtualItem(true);
            }
            else {
                this.hideVirtualItem(false);
            }
        }
        else {
            //判断是交换位置还是与之合成
            this.combinOrSwap(this._currentNode, touchEndNode);
        }
        this._currentNode = null;
        this._isCanDrag = false;
    };
    UIHome.prototype.onTouchCancel = function (touchEvent) {
        if (!this._currentNode) {
            return;
        }
        var ItemProduce = this._currentNode.getComponent('ItemProduce');
        if (!ItemProduce._itemId) {
            return;
        }
        this.hideProduce();
        ItemProduce.dragOver();
        if (ItemProduce.isUsed) {
            this._currentNode = null;
            return;
        }
        //检查下是否拖拽到回收站了
        var isDragToRecovery = this.checkIsDragToRecovery(ItemProduce._index, touchEvent.getLocation());
        if (!isDragToRecovery) {
            //拖到了无效位置
            this.hideVirtualItem(true);
        }
        else {
            //发起回收的请求,此处已由recovery接收走
            this.hideVirtualItem(false);
        }
        this._currentNode = null;
    };
    UIHome.prototype.getCurrentNodeByTouchPos = function (pos) {
        var arrNode = this.view.WorkContent.children;
        return arrNode.find(function (itemNode, index) {
            var box = itemNode.getBoundingBoxToWorld();
            if (box.contains(pos)) {
                return true;
            }
        }, this);
    };
    /**
     * 拖拽时原物体半透明，然后拖拽的其实是一个虚拟的半透明物体
     */
    UIHome.prototype.showVirtualItem = function (info, pos) {
        if (!this._virtualNode) {
            this._virtualNode = cc.instantiate(this.itemProducePrefab);
            this._virtualNode.parent = this.node.parent;
        }
        else {
            this._virtualNode.active = true;
        }
        this._virtualNode.stopActionByTag(TAG_VIRTUAL_MOVE);
        var cakeItem = this._virtualNode.getComponent('ItemProduce');
        cakeItem.setWorkbenchItemInfo(-1, info);
        cakeItem.showVirtual();
        this.updateVirutalItemPos(pos);
    };
    /**
     * 隐藏拖拽时的半透明图标
     */
    UIHome.prototype.hideVirtualItem = function (isGoBack) {
        var _this = this;
        if (this._virtualNode && this._virtualNode.active) {
            if (isGoBack) {
                // this.currentNode.converttow
                var posWorld = this._currentNode.convertToWorldSpaceAR(cc.v2);
                var posCurrentNode = this.node.parent.convertToNodeSpaceAR(posWorld);
                var duration = posCurrentNode.sub(this._virtualNode.position).mag() / SPEED_VIRUTAL_MOVE;
                CocosHelper_1.default.runTweenSync(this._virtualNode, cc.tween().to(0, duration).call(function () {
                    _this._virtualNode.active = false;
                }));
            }
            else {
                this._virtualNode.active = false;
            }
        }
    };
    /**
     * 更新拖拽时的半透明图标随手指移动位置
     */
    UIHome.prototype.updateVirutalItemPos = function (pos) {
        if (this._virtualNode && this._virtualNode.active) {
            //需要坐标转换
            var posNodeSpace = this.node.parent.convertToNodeSpaceAR(pos);
            this._virtualNode.position = posNodeSpace.add(this._offsetPos);
            this.updateDragPos(pos);
        }
    };
    UIHome.prototype.updateDragPos = function (pos) {
        var box = this.view.RecoveryBtn.node.getBoundingBoxToWorld();
        if (!box.contains(pos)) {
            if (this.view.RecoveryBtn.node.scale === 1.1) {
                this.view.RecoveryBtn.node.scale = 1;
            }
            return false;
        }
        if (this.view.RecoveryBtn.node.scale === 1) {
            this.view.RecoveryBtn.node.scale = 1.1;
        }
    };
    //检查是否拖到回收站了
    UIHome.prototype.checkIsDragToRecovery = function (workbenchIdx, pos) {
        return __awaiter(this, void 0, void 0, function () {
            var box, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        box = this.view.RecoveryBtn.node.getBoundingBoxToWorld();
                        if (!box.contains(pos)) {
                            return [2 /*return*/, false];
                        }
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount, ctrlType: DataUser_1.OperationType.sell, curPos: workbenchIdx })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        else {
                            this.updateWorkbench(workbenchIdx, -1, "Recovery");
                            return [2 /*return*/, true];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    //点击拖动时播放相同等级物品的动画
    UIHome.prototype.showProduce = function (itemProduce) {
        var index = itemProduce._index;
        var itemId = itemProduce._itemId;
        var arrNode = this.view.WorkContent.children;
        for (var idx = 0; idx < arrNode.length; idx++) {
            if (idx === index) {
                continue;
            }
            var produce = arrNode[idx].getComponent('ItemProduce');
            if (!produce.isUsed && produce.itemId === itemId) {
                // produce.playProduceAni(true);
            }
        }
    };
    //隐藏拖拽时的图标
    UIHome.prototype.hideProduce = function () {
        var arrNode = this.view.WorkContent.children;
        for (var idx = 0; idx < arrNode.length; idx++) {
            var produce = arrNode[idx].getComponent('ItemProduce');
            if (!produce.isUsed) {
                // produce.playProduceAni(false);
            }
        }
    };
    /**
     * 判断是交换还是结合
     */
    UIHome.prototype.combinOrSwap = function (originNode, targetNode) {
        return __awaiter(this, void 0, void 0, function () {
            var originScript, targetScript, originItem, targetItem, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        originScript = originNode.getComponent('ItemProduce');
                        targetScript = targetNode.getComponent('ItemProduce');
                        if (originScript.isUsed || targetScript.isUsed) {
                            this.hideVirtualItem(true);
                            return [2 /*return*/];
                        }
                        originItem = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originScript._index];
                        targetItem = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetScript._index];
                        if (originItem == null || targetItem == null) {
                            return [2 /*return*/, false];
                        }
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount, ctrlType: DataUser_1.OperationType.concat, curPos: originScript._index, target: targetScript._index })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp("数据错误");
                            this.updateWorkbench(originScript._index, targetScript._index, "Exchange");
                            return [2 /*return*/];
                        }
                        if (originScript._itemId === targetScript._itemId) {
                            this.combinProduce(originScript._index, targetScript._index, datas.res.userWorkBench[targetScript._index]);
                        }
                        else {
                            //位置交换
                            console.log("jiaohuan");
                            if (this.swapProduce(originScript._index, targetScript._index)) {
                                this.updateWorkbench(originScript._index, targetScript._index, "Exchange");
                            }
                        }
                        this.hideVirtualItem(false);
                        return [2 /*return*/];
                }
            });
        });
    };
    //更新工作台
    UIHome.prototype.updateWorkbench = function (originIndex, targetIndex, type) {
        if (!type) {
            this.initWorkbench();
            return;
        }
        var originInfo;
        var targetInfo;
        if (originIndex >= 0) {
            var originNode = this.view.WorkContent.children[originIndex];
            originInfo = originNode.getComponent('ItemProduce');
        }
        if (targetIndex >= 0) {
            var targetNode = this.view.WorkContent.children[targetIndex];
            targetInfo = targetNode.getComponent('ItemProduce');
        }
        switch (type) {
            case "Exchange":
                console.log("交换");
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                originInfo.setWorkbenchItemInfo(originInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originInfo._index]);
                break;
            case "Combine":
                console.log("组合");
                originInfo.setWorkbenchItemInfo(originInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originInfo._index]);
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                break;
            case "Produce":
                console.log("生产");
                targetInfo.setWorkbenchItemInfo(targetInfo._index, GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetInfo._index]);
                break;
            case "Recovery":
                console.log("回收");
                originInfo.setWorkbenchItemInfo(originInfo._index, 0);
                GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originInfo._index] = 0;
                break;
        }
    };
    /**
     * 交换生产物位置
     * @param {number} originIndex
     * @param {number} targetIndex
     */
    UIHome.prototype.swapProduce = function (originIndex, targetIndex) {
        if (!GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench) {
            return false;
        }
        if (originIndex >= GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length || targetIndex >= GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length) {
            return false;
        }
        var originItemId = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originIndex];
        var targetItemId = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetIndex];
        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetIndex] = originItemId;
        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originIndex] = targetItemId;
        return true;
    };
    /**
     * 生产物合并
     * @param {Number} originIndex 原位置的index
     * @param {Number} targetIndex 目标位置的index
     * @param {String} itemId 物品id
     */
    UIHome.prototype.combinProduce = function (originIndex, targetIndex, itemId) {
        var originNode = this.view.WorkContent.children[originIndex];
        var targetNode = this.view.WorkContent.children[targetIndex];
        var originScript = originNode.getComponent('ItemProduce');
        var targetScript = targetNode.getComponent('ItemProduce');
        originScript.isUsed = true; //要结合，先将两个生产物置为不可拖拽
        targetScript.isUsed = true;
        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[originIndex] = 0;
        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[targetIndex] = itemId;
        this.updateWorkbench(originScript._index, targetScript._index, "Combine");
    };
    //购买事件
    UIHome.prototype.onBuyProduceClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isEnough, index, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        isEnough = GameMgr_1.default.dataModalMgr.UserInfo.userCoin >= GameMgr_1.default.dataModalMgr.CurBuyProduceInfo.buyPrice;
                        if (!isEnough) {
                            UIToast_1.default.popUp("金币不足！");
                            return [2 /*return*/];
                        }
                        if (!GameMgr_1.default.dataModalMgr.UserInfo.hasPosAtWorkbench()) {
                            UIToast_1.default.popUp("请先出售多余物品！");
                            return [2 /*return*/];
                        }
                        index = GameMgr_1.default.dataModalMgr.UserInfo.checkNullPos();
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("WorkBenchCtrl", { userAccount: GameMgr_1.default.dataModalMgr.UserInfo.userAccount, ctrlType: DataUser_1.OperationType.buy, curPos: index })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[index] = datas.res.userWorkBench[index];
                        this.updateWorkbench(-1, index, "Produce");
                        return [2 /*return*/];
                }
            });
        });
    };
    //自动合成
    UIHome.prototype.onBtnCombineAutoClick = function () {
        if (this._isCombining) {
            UIToast_1.default.popUp("正在自动合成中");
            return;
        }
        var at = GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime;
        if (!at || at <= 0) {
            //停止自动合成
            this.scheduleCombineOver();
            return;
        }
        if (!this._isCombining) {
            this.checkIsCombineAuto();
        }
    };
    //检查是否正在自动合成
    UIHome.prototype.checkIsCombineAuto = function () {
        var spareTime = GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime;
        if (!spareTime || spareTime <= 0) {
            this.scheduleCombineOver();
            return;
        }
        this._isCombining = true;
        this.schedule(this.combineAuto, 1);
        this.view.CombineAutoTxt.string = CocosHelper_1.default.formatTimeForSecond(15);
        this.view.CombineAutoTxt.node.active = true;
    };
    /**
     *
     * 自动合成
     */
    UIHome.prototype.combineAuto = function () {
        GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime -= 1;
        if (GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime <= 0) {
            this.scheduleCombineOver();
        }
        this.view.CombineAutoTxt.string = CocosHelper_1.default.formatTimeForSecond(GameMgr_1.default.dataModalMgr.UserInfo.combineAutoTime);
        var arrSort = [];
        for (var idx = 0; idx < GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench.length; idx++) {
            var cakeNode = this.view.WorkContent.children[idx];
            var cakeItem = cakeNode.getComponent('ItemProduce');
            var isUsed = cakeItem.isUsed;
            if (isUsed) {
                continue;
            }
            var itemId = GameMgr_1.default.dataModalMgr.UserInfo.userWorkbench[idx];
            if (itemId === 0) {
                continue; //礼盒或者是空位置要屏蔽掉
            }
            arrSort.push({ itemId: itemId, index: idx });
        }
        //按照蛋糕id进行排序
        arrSort.sort(function (a, b) {
            return Number(a.itemId) - Number(b.itemId);
        });
        var originIndex = -1;
        var targetIndex = -1;
        for (var idx = 0; idx < arrSort.length - 1; idx++) {
            if (arrSort[idx].itemId === arrSort[idx + 1].itemId && arrSort[idx].itemId.toString() !== "38") { //排除最高等级，避免最高等级影响后续合成
                //找到等级最低的蛋糕了
                targetIndex = arrSort[idx].index;
                originIndex = arrSort[idx + 1].index;
                break;
            }
        }
        if (originIndex !== -1 && targetIndex !== -1) {
            if (this._currentNode) {
                var cakeItem = this._currentNode.getComponent('ItemProduce');
                if (cakeItem.index === originIndex || cakeItem.index === targetIndex) {
                    cakeItem.dragOver();
                    this.hideProduce();
                    this._currentNode = null;
                    this.hideVirtualItem(false);
                }
            }
            this.combinOrSwap(this.view.WorkContent.children[originIndex], this.view.WorkContent.children[targetIndex]);
        }
    };
    /**
     * 关闭合成倒计时
     */
    UIHome.prototype.scheduleCombineOver = function () {
        this._isCombining = false;
        this.view.CombineAutoTxt.node.active = false;
        this.unschedule(this.combineAuto);
    };
    __decorate([
        property(cc.Prefab)
    ], UIHome.prototype, "itemProducePrefab", void 0);
    UIHome = __decorate([
        ccclass
    ], UIHome);
    return UIHome;
}(UIForm_1.UIScreen));
exports.default = UIHome;

cc._RF.pop();