"use strict";
cc._RF.push(module, 'e361cy6Ih1C5JAmRoLs82SR', 'UILotteryGetTip');
// Script/UIScript/Lottery/UILotteryGetTip.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UILotteryGetTip = /** @class */ (function (_super) {
    __extends(UILotteryGetTip, _super);
    function UILotteryGetTip() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // onLoad () {}
    UILotteryGetTip.prototype.start = function () {
        var _this = this;
        this.view.BtnSure.addClick(function () {
            _this.closeSelf();
        }, this);
    };
    UILotteryGetTip.prototype.onShow = function (params) {
        console.log("获得类型：" + params);
    };
    UILotteryGetTip = __decorate([
        ccclass
    ], UILotteryGetTip);
    return UILotteryGetTip;
}(UIForm_1.UIWindow));
exports.default = UILotteryGetTip;

cc._RF.pop();