"use strict";
cc._RF.push(module, 'dda071AclpG/LHrHJaGISlC', 'UICollection');
// Script/UIScript/Collection/UICollection.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UICollection = /** @class */ (function (_super) {
    __extends(UICollection, _super);
    function UICollection() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UICollection.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () { _this.closeSelf(); }, this);
        this.view.WenFangSiBaoProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curWFSBProgress / 4;
        this.view.WenFangSiBaoProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curWFSBProgress + "/4";
        this.view.WenFangSiBaoGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.WFSBGetMoney.toString();
        this.view.SiJunZiProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curSJZProgress / 4;
        this.view.SiJunZiProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curSJZProgress + "/4";
        this.view.SiJunZiGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.SJZGetMoney.toString();
        this.view.MingZhuProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curSDMZProgress / 4;
        this.view.MingZhuProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curSDMZProgress + "/4";
        this.view.MingZhuGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.SDMZGetMoney.toString();
        this.view.ShenShouProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curSDSSProgress / 4;
        this.view.ShenShouProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curSDSSProgress + "/4";
        this.view.ShenShouGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.SDSSGetMoney.toString();
        this.view.BaDaJiaProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curBDJProgress / 8;
        this.view.BaDaJiaProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curBDJProgress + "/8";
        this.view.BaDaJiaGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.BDJGetMoney.toString();
        this.view.JinChanProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curJCProgress / 1;
        this.view.JinChanProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curJCProgress + "/1";
        this.view.JinChanGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.JCDescribe;
        this.view.PiXiuProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curPXProgress / 1;
        this.view.PiXiuProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curPXProgress + "/1";
        this.view.PiXiuGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.JCDescribe;
        this.view.JinLongProgress.getComponent(cc.ProgressBar).progress = GameMgr_1.default.dataModalMgr.CollectionInfo.curJLProgress / 1;
        this.view.JinLongProgressLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.curJLProgress + "/1";
        this.view.JinLongGetNumTipLab.string = GameMgr_1.default.dataModalMgr.CollectionInfo.JLDescribe;
    };
    UICollection = __decorate([
        ccclass
    ], UICollection);
    return UICollection;
}(UIForm_1.UIWindow));
exports.default = UICollection;

cc._RF.pop();