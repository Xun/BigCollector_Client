"use strict";
cc._RF.push(module, '5758a1DBa5DaaI4PN+iQ8vo', 'ItemDealMySell');
// Script/UIScript/Shop/ItemDealMySell.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var GameMgr_1 = require("../../Manager/GameMgr");
var UIManager_1 = require("../../Manager/UIManager");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var UIDealMySell_1 = require("./UIDealMySell");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemDealMySell = /** @class */ (function (_super) {
    __extends(ItemDealMySell, _super);
    function ItemDealMySell() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item_name = null;
        _this.item_num = null;
        _this.item_type = null;
        _this.item_price = null;
        _this.item_icon = null;
        _this.item_cancel = null;
        _this.curType = "进行中";
        _this.deal_id = "";
        return _this;
    }
    ItemDealMySell.prototype.setData = function (data) {
        this.item_name.string = CocosHelper_1.default.getChipNameByID(data.chipID.toString());
        var chip_name = this.item_name.string.replace("碎片", "");
        CocosHelper_1.default.setDealIcon(chip_name, this.item_icon);
        this.item_num.string = "已卖出" + (data.needCount - data.currentCount) + "/" + data.needCount;
        this.item_price.string = data.price.toString();
        if (data.status === -1) {
            this.curType = "订单下架";
        }
        if (data.status === 1) {
            this.curType = "已完成";
        }
        this.item_type.string = this.curType;
        this.deal_id = data.guid;
        this.item_cancel.active = data.status === 0;
    };
    ItemDealMySell.prototype.onCancel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var datas, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealCancel", { type: 1, dealID: this.deal_id })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        for (i = 0; i < GameMgr_1.default.dataModalMgr.DataDealInfo.dealMySellList.length; i++) {
                            if (GameMgr_1.default.dataModalMgr.DataDealInfo.dealMySellList[i].guid == datas.res.dealList[0].guid) {
                                GameMgr_1.default.dataModalMgr.DataDealInfo.dealMySellList[i] = datas.res.dealList[0];
                                UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDealMySell.prefabUrl).getComponent(UIDealMySell_1.default).reflashList();
                                break;
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        property(cc.Label)
    ], ItemDealMySell.prototype, "item_name", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMySell.prototype, "item_num", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMySell.prototype, "item_type", void 0);
    __decorate([
        property(cc.Label)
    ], ItemDealMySell.prototype, "item_price", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemDealMySell.prototype, "item_icon", void 0);
    __decorate([
        property(cc.Node)
    ], ItemDealMySell.prototype, "item_cancel", void 0);
    ItemDealMySell = __decorate([
        ccclass
    ], ItemDealMySell);
    return ItemDealMySell;
}(cc.Component));
exports.default = ItemDealMySell;

cc._RF.pop();