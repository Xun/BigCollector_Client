"use strict";
cc._RF.push(module, '8796eXQYoBPbo5MRJ7Mh+EB', 'UIShop');
// Script/UIScript/Shop/UIShop.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var ItemShop_1 = require("./ItemShop");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIShop = /** @class */ (function (_super) {
    __extends(UIShop, _super);
    function UIShop() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listShop = null;
        return _this;
    }
    UIShop.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        GameMgr_1.default.dataModalMgr.ShopInfo.list_item = GameMgr_1.default.dataModalMgr.ShopInfoJson.shopInfo;
        this.listShop.numItems = GameMgr_1.default.dataModalMgr.ShopInfo.list_item.length;
    };
    //垂直列表渲染器
    UIShop.prototype.onListShopRender = function (item, idx) {
        item.getComponent(ItemShop_1.default).setData(GameMgr_1.default.dataModalMgr.ShopInfo.list_item[idx]);
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIShop.prototype, "listShop", void 0);
    UIShop = __decorate([
        ccclass
    ], UIShop);
    return UIShop;
}(UIForm_1.UIWindow));
exports.default = UIShop;

cc._RF.pop();