"use strict";
cc._RF.push(module, '817fe3W/UdKyqpf3zAn6UlN', 'UIDealTip');
// Script/UIScript/Shop/UIDealTip.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var GameMgr_1 = require("../../Manager/GameMgr");
var UIManager_1 = require("../../Manager/UIManager");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var UIDeal_1 = require("./UIDeal");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDealTip = /** @class */ (function (_super) {
    __extends(UIDealTip, _super);
    function UIDealTip() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._dealType = 0;
        _this._chipNum = 0;
        _this._curNum = 0;
        _this._chipName = "";
        _this._dealId = "";
        _this._price = 0;
        return _this;
        // update (dt) {}
    }
    // onLoad () {}
    UIDealTip.prototype.onShow = function (params) {
        if (params.type == 2) {
            this.view.LabBuyOrSell.string = "购买";
            this.view.LabCurNum.string = "购买1个" + params.chipName + "碎片需要" + params.price + "玉石";
            this.view.LabTipType.string = "";
        }
        else {
            this.view.LabBuyOrSell.string = "出售";
            this.view.LabCurNum.string = "当前可出售：999个";
            this.view.LabTipType.string = "出售所得玉石将扣5%手续费";
        }
        this._price = params.price;
        this._dealId = params.orderId;
        this._dealType = params.type;
        this._chipNum = params.chipNum;
        this._chipName = params.chipName;
        this.view.LabName.string = params.chipName + "碎片";
        CocosHelper_1.default.setDealIcon(params.chipName, this.view.SpChipIcon);
    };
    UIDealTip.prototype.start = function () {
        var _this = this;
        this.view.BtnClose.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.EditeNum.node.on('text-changed', this.onInputNum, this);
        this.view.BtnSure.addClick(this.onSureClick, this);
    };
    UIDealTip.prototype.onInputNum = function (event) {
        this._curNum = Number(event.string);
        var curPrice = this._curNum * this._price;
        this.view.LabCurNum.string = "购买" + this._curNum + "个" + this._chipName + "需要" + curPrice + "玉石";
    };
    UIDealTip.prototype.onSureClick = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this._chipNum <= 0) {
                            UIToast_1.default.popUp("请输入正确的数量");
                            return [2 /*return*/];
                        }
                        if (this._chipNum < this._curNum) {
                            UIToast_1.default.popUp("超过当前订单数量");
                            return [2 /*return*/];
                        }
                        if (!(this._dealType == 2)) return [3 /*break*/, 2];
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealType", { type: this._dealType, dealID: this._dealId, count: this._curNum })];
                    case 1:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList = data.res.dealList;
                        UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDeal.prefabUrl).getComponent(UIDeal_1.default).reflushBuyList();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, RpcConent_1.apiClient.callApi("DealType", { type: this._dealType, dealID: this._dealId, count: this._curNum })];
                    case 3:
                        data = _a.sent();
                        if (!data.isSucc) {
                            UIToast_1.default.popUp(data.err.message);
                            return [2 /*return*/];
                        }
                        GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList = data.res.dealList;
                        UIManager_1.default.getInstance().getForm(UIConfig_1.default.UIDeal.prefabUrl).getComponent(UIDeal_1.default).reflushSellList();
                        _a.label = 4;
                    case 4:
                        this.closeSelf();
                        return [2 /*return*/];
                }
            });
        });
    };
    UIDealTip = __decorate([
        ccclass
    ], UIDealTip);
    return UIDealTip;
}(UIForm_1.UIWindow));
exports.default = UIDealTip;

cc._RF.pop();