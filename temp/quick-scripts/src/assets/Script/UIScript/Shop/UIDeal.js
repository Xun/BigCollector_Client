"use strict";
cc._RF.push(module, '15b2aw7TjxJopD6dXRZYwwH', 'UIDeal');
// Script/UIScript/Shop/UIDeal.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var GameMgr_1 = require("../../Manager/GameMgr");
var RpcConent_1 = require("../../Net/RpcConent");
var UIConfig_1 = require("../../UIConfig");
var CocosHelper_1 = require("../../Utils/CocosHelper");
var UIToast_1 = require("../UIToast");
var ItemDeal_1 = require("./ItemDeal");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIDeal = /** @class */ (function (_super) {
    __extends(UIDeal, _super);
    function UIDeal() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.listDeal = null;
        _this.chipid = "";
        _this.itemname = "笔";
        _this.deal_type = 0;
        return _this;
    }
    UIDeal.prototype.onLoad = function () {
        this.view.NodeDealItems.active = false;
    };
    UIDeal.prototype.start = function () {
        var _this = this;
        this.view.DealToggleNode.children.forEach(function (item) {
            item.on(cc.Node.EventType.TOUCH_END, function (e) {
                _this.onSelectType(_this.itemname);
            }, _this);
        });
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.BtnCloseNodeItems.addClick(function () {
            _this.view.NodeDealItems.active = false;
        }, this);
        this.view.BtnSelectType.addClick(function () {
            _this.view.NodeDealItems.active = !_this.view.NodeDealItems.active;
        }, this);
        this.view.BtnDealDetail.addClick(function () {
            _this.view.NodeDealItems.active = false;
            FormMgr_1.default.open(UIConfig_1.default.UIDealDetail);
        }, this);
        this.view.BtnMyBuy.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIDealMyGet);
        }, this);
        this.view.BtnMySell.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIDealMySell);
        }, this);
        this.view.BtnBi.addClick(function () {
            _this.onSelectType("笔");
        }, this);
        this.view.BtnZhi.addClick(function () {
            _this.onSelectType("纸");
        }, this);
        this.view.BtnMo.addClick(function () {
            _this.onSelectType("墨");
        }, this);
        this.view.BtnYan.addClick(function () {
            _this.onSelectType("砚");
        }, this);
        this.view.BtnM.addClick(function () {
            _this.onSelectType("梅");
        }, this);
        this.view.BtnL.addClick(function () {
            _this.onSelectType("兰");
        }, this);
        this.view.BtnZ.addClick(function () {
            _this.onSelectType("竹");
        }, this);
        this.view.BtnJ.addClick(function () {
            _this.onSelectType("菊");
        }, this);
        this.view.BtnXYJ.addClick(function () {
            _this.onSelectType("西游记");
        }, this);
        this.view.BtnHLM.addClick(function () {
            _this.onSelectType("红楼梦");
        }, this);
        this.view.BtnSHZ.addClick(function () {
            _this.onSelectType("水浒传");
        }, this);
        this.view.BtnSGYY.addClick(function () {
            _this.onSelectType("三国演义");
        }, this);
        this.view.BtnQL.addClick(function () {
            _this.onSelectType("青龙");
        }, this);
        this.view.BtnBH.addClick(function () {
            _this.onSelectType("白虎");
        }, this);
        this.view.BtnXW.addClick(function () {
            _this.onSelectType("玄武");
        }, this);
        this.view.BtnZQ.addClick(function () {
            _this.onSelectType("朱雀");
        }, this);
        this.view.BtnZG.addClick(function () {
            _this.onSelectType("曾巩");
        }, this);
        this.view.BtnLZY.addClick(function () {
            _this.onSelectType("柳宗元");
        }, this);
        this.view.BtnWAS.addClick(function () {
            _this.onSelectType("王安石");
        }, this);
        this.view.BtnHY.addClick(function () {
            _this.onSelectType("韩愈");
        }, this);
        this.view.BtnOYX.addClick(function () {
            _this.onSelectType("欧阳修");
        }, this);
        this.view.BtnSX.addClick(function () {
            _this.onSelectType("苏洵");
        }, this);
        this.view.BtnSZ.addClick(function () {
            _this.onSelectType("苏辙");
        }, this);
        this.view.BtnSS.addClick(function () {
            _this.onSelectType("苏轼");
        }, this);
        this.view.BtnJC.addClick(function () {
            _this.onSelectType("金蟾");
        }, this);
        this.view.BtnPX.addClick(function () {
            _this.onSelectType("貔貅");
        }, this);
        this.view.BtnJL.addClick(function () {
            _this.onSelectType("金龙");
        }, this);
        this.onSelectType("笔");
        // this.changeType();
    };
    UIDeal.prototype.onSelectType = function (itme_name) {
        return __awaiter(this, void 0, void 0, function () {
            var index, datas;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.itemname = itme_name;
                        CocosHelper_1.default.setDealIcon(itme_name, this.view.DealIcon);
                        this.view.NodeDealItems.active = false;
                        this.chipid = CocosHelper_1.default.getChipInfoByName(itme_name + "碎片");
                        console.log("------------" + this.chipid);
                        index = CocosHelper_1.default.getContenerindex(this.view.DealToggleNode);
                        if (index == 0) {
                            this.deal_type = 2;
                        }
                        if (index == 1) {
                            this.deal_type = 1;
                        }
                        return [4 /*yield*/, RpcConent_1.apiClient.callApi("Deal", { type: this.deal_type, chipID: Number(this.chipid) })];
                    case 1:
                        datas = _a.sent();
                        if (!datas.isSucc) {
                            UIToast_1.default.popUp(datas.err.message);
                            return [2 /*return*/];
                        }
                        if (index == 0) {
                            console.log("aaaaaa");
                            GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList = datas.res.dealList;
                            this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList.length;
                        }
                        if (index == 1) {
                            console.log("bbbbbb");
                            GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList = datas.res.dealList;
                            this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList.length;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    UIDeal.prototype.reflushBuyList = function () {
        this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList.length;
    };
    UIDeal.prototype.reflushSellList = function () {
        this.listDeal.numItems = GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList.length;
    };
    //垂直列表渲染器
    UIDeal.prototype.onListDealRender = function (item, idx) {
        var index = CocosHelper_1.default.getContenerindex(this.view.DealToggleNode);
        console.log("-------------index:" + index);
        if (index == 0) {
            if (GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList.length) {
                item.getComponent(ItemDeal_1.default).setData(GameMgr_1.default.dataModalMgr.DataDealInfo.dealSellList[idx], 1);
            }
        }
        else {
            if (GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList.length > 0) {
                item.getComponent(ItemDeal_1.default).setData(GameMgr_1.default.dataModalMgr.DataDealInfo.dealBuyList[idx], 2);
            }
        }
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIDeal.prototype, "listDeal", void 0);
    UIDeal = __decorate([
        ccclass
    ], UIDeal);
    return UIDeal;
}(UIForm_1.UIWindow));
exports.default = UIDeal;

cc._RF.pop();