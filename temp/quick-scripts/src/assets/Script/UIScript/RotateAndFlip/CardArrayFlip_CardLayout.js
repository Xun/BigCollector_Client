"use strict";
cc._RF.push(module, '34a14VJ+FVL85T5e314dkhm', 'CardArrayFlip_CardLayout');
// Script/UIScript/RotateAndFlip/CardArrayFlip_CardLayout.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CardArrayFlip_Card_1 = require("./CardArrayFlip_Card");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode;
var CardArrayFlip_Layout = /** @class */ (function (_super) {
    __extends(CardArrayFlip_Layout, _super);
    function CardArrayFlip_Layout() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._radius = 350;
        _this._offset = 90;
        _this._k = 0;
        /** 卡片组件 */
        _this.cards = null;
        return _this;
    }
    Object.defineProperty(CardArrayFlip_Layout.prototype, "radius", {
        get: function () {
            return this._radius;
        },
        set: function (value) {
            this._radius = value;
            this.updateLayout();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CardArrayFlip_Layout.prototype, "offset", {
        get: function () {
            return this._offset;
        },
        set: function (value) {
            this._offset = value;
            this.updateLayout();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CardArrayFlip_Layout.prototype, "k", {
        get: function () {
            return this._k;
        },
        set: function (value) {
            this._k = value;
            this.updateKValue();
        },
        enumerable: false,
        configurable: true
    });
    CardArrayFlip_Layout.prototype.onLoad = function () {
        this.init();
        this.registerEvent();
    };
    CardArrayFlip_Layout.prototype.onDisable = function () {
        this.unregisterEvent();
    };
    /**
     * 初始化
     */
    CardArrayFlip_Layout.prototype.init = function () {
        this.onChildChange();
    };
    /**
     * 注册事件
     */
    CardArrayFlip_Layout.prototype.registerEvent = function () {
        // 节点增删
        this.node.on(cc.Node.EventType.CHILD_ADDED, this.onChildChange, this);
        this.node.on(cc.Node.EventType.CHILD_REMOVED, this.onChildChange, this);
        // 旋转改变
        this.node.on(cc.Node.EventType.ROTATION_CHANGED, this.onRotationChange, this);
    };
    /**
     * 反注册事件
     */
    CardArrayFlip_Layout.prototype.unregisterEvent = function () {
        this.node.off(cc.Node.EventType.CHILD_ADDED, this.onChildChange, this);
        this.node.off(cc.Node.EventType.CHILD_REMOVED, this.onChildChange, this);
        this.node.off(cc.Node.EventType.ROTATION_CHANGED, this.onRotationChange, this);
    };
    /**
     * 子节点变化回调
     */
    CardArrayFlip_Layout.prototype.onChildChange = function () {
        // 重新获取组件
        this.cards = this.getComponentsInChildren(CardArrayFlip_Card_1.default);
        // 更新 k 值
        this.updateKValue();
        // 更新布局
        this.updateLayout();
    };
    /**
     * 旋转变化回调
     */
    CardArrayFlip_Layout.prototype.onRotationChange = function () {
        // 更新层级
        this.updateHierarchy();
    };
    /**
     * 更新布局
     */
    CardArrayFlip_Layout.prototype.updateLayout = function () {
        var nodes = this.node.children, count = nodes.length, radius = this._radius, offset = this._offset, delta = 360 / count;
        for (var i = 0; i < count; i++) {
            var node = nodes[i], angleY = -(delta * i), radian = (Math.PI / 180) * (angleY - offset);
            // 位置
            node.x = radius * Math.cos(radian);
            node.z = -(radius * Math.sin(radian));
            // 角度
            var _a = node.eulerAngles, x = _a.x, z = _a.z;
            node.eulerAngles = cc.v3(x, angleY, z);
            // node.rotationY = angleY;      // keep warning
            // node.eulerAngles.y = angleY;  // not working
        }
        // 更新层级
        this.updateHierarchy();
    };
    /**
     * 更新层级
     */
    CardArrayFlip_Layout.prototype.updateHierarchy = function () {
        var cards = this.cards, length = cards.length;
        // 更新卡片节点在世界坐标系中的 z 值
        for (var i = 0; i < length; i++) {
            cards[i].updateWorldZ();
        }
        // 排序从大到小，z 值越小的显示在越后面，层级 index 也越小
        cards.sort(function (a, b) { return a.z - b.z; });
        // 调整节点层级
        for (var i = 0; i < length; i++) {
            cards[i].setSiblingIndex(i);
        }
    };
    /**
     * 更新卡片的 k 值
     */
    CardArrayFlip_Layout.prototype.updateKValue = function () {
        var cards = this.cards;
        for (var i = 0, l = cards.length; i < l; i++) {
            cards[i].k = this._k;
        }
    };
    __decorate([
        property
    ], CardArrayFlip_Layout.prototype, "_radius", void 0);
    __decorate([
        property({ displayName: CC_DEV && '阵列半径' })
    ], CardArrayFlip_Layout.prototype, "radius", null);
    __decorate([
        property
    ], CardArrayFlip_Layout.prototype, "_offset", void 0);
    __decorate([
        property({ displayName: CC_DEV && '卡片角度偏移' })
    ], CardArrayFlip_Layout.prototype, "offset", null);
    __decorate([
        property
    ], CardArrayFlip_Layout.prototype, "_k", void 0);
    __decorate([
        property({ displayName: CC_DEV && '正反面阈值' })
    ], CardArrayFlip_Layout.prototype, "k", null);
    CardArrayFlip_Layout = __decorate([
        ccclass,
        executeInEditMode
    ], CardArrayFlip_Layout);
    return CardArrayFlip_Layout;
}(cc.Component));
exports.default = CardArrayFlip_Layout;

cc._RF.pop();