"use strict";
cc._RF.push(module, '659b8MDsJFPz5aiPsq4V0Q2', 'CardArrayFlip_Card');
// Script/UIScript/RotateAndFlip/CardArrayFlip_Card.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode;
var CardArrayFlip_Card = /** @class */ (function (_super) {
    __extends(CardArrayFlip_Card, _super);
    function CardArrayFlip_Card() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.back = null;
        _this.front = null;
        _this.k = 0;
        _this._z = 0;
        return _this;
    }
    Object.defineProperty(CardArrayFlip_Card.prototype, "z", {
        /** 节点在世界坐标中的 z 值 */
        get: function () {
            return this._z;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(CardArrayFlip_Card.prototype, "facingScreen", {
        /** 是否面向屏幕 */
        get: function () {
            return this.node.forward.z >= this.k;
        },
        enumerable: false,
        configurable: true
    });
    CardArrayFlip_Card.prototype.onEnable = function () {
        this.updateWorldZ();
    };
    CardArrayFlip_Card.prototype.update = function (dt) {
        this.updateDisplay();
    };
    /**
     * 更新样式
     */
    CardArrayFlip_Card.prototype.updateDisplay = function () {
        var front = this.facingScreen;
        this.front.active = front;
        this.back.active = !front;
    };
    /**
     * 更新节点在世界坐标中的 z 值
     */
    CardArrayFlip_Card.prototype.updateWorldZ = function () {
        var worldPos = this.node.parent.convertToWorldSpaceAR(this.node.position);
        this._z = worldPos.z;
    };
    /**
     * 设置层级
     * @param index 下标
     */
    CardArrayFlip_Card.prototype.setSiblingIndex = function (index) {
        this.node.setSiblingIndex(index);
    };
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_Card.prototype, "back", void 0);
    __decorate([
        property(cc.Node)
    ], CardArrayFlip_Card.prototype, "front", void 0);
    __decorate([
        property
    ], CardArrayFlip_Card.prototype, "k", void 0);
    CardArrayFlip_Card = __decorate([
        ccclass,
        executeInEditMode
    ], CardArrayFlip_Card);
    return CardArrayFlip_Card;
}(cc.Component));
exports.default = CardArrayFlip_Card;

cc._RF.pop();