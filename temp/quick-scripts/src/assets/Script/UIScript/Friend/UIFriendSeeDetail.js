"use strict";
cc._RF.push(module, '9f8fcAhCUtAq5OTeNZjyvJQ', 'UIFriendSeeDetail');
// Script/UIScript/Friend/UIFriendSeeDetail.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ListUtil_1 = require("../../Common/Components/ListUtil");
var UIForm_1 = require("../../Common/UIForm");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIFriendSeeDetail = /** @class */ (function (_super) {
    __extends(UIFriendSeeDetail, _super);
    function UIFriendSeeDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.list_see_detail = null;
        return _this;
    }
    UIFriendSeeDetail.prototype.onLoad = function () {
        // this.list_see_detail.numItems
    };
    UIFriendSeeDetail.prototype.start = function () {
        var _this = this;
        this.view.CloseBtn.addClick(function () {
            _this.closeSelf();
        }, this);
    };
    //垂直列表渲染器
    UIFriendSeeDetail.prototype.onListRankRender = function (item) {
        // item.getComponent(ItemFriendSeeDetail).setData();
    };
    __decorate([
        property(ListUtil_1.default)
    ], UIFriendSeeDetail.prototype, "list_see_detail", void 0);
    UIFriendSeeDetail = __decorate([
        ccclass
    ], UIFriendSeeDetail);
    return UIFriendSeeDetail;
}(UIForm_1.UIWindow));
exports.default = UIFriendSeeDetail;

cc._RF.pop();