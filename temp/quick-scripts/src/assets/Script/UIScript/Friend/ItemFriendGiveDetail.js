"use strict";
cc._RF.push(module, '7a59c7Lx65KT4x5ZdxMYmuJ', 'ItemFriendGiveDetail');
// Script/UIScript/Friend/ItemFriendGiveDetail.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemFriendGiveDetail = /** @class */ (function (_super) {
    __extends(ItemFriendGiveDetail, _super);
    function ItemFriendGiveDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.uname = null;
        _this.uid = null;
        _this.utime = null;
        _this.uhead = null;
        _this.givenum = null;
        return _this;
    }
    ItemFriendGiveDetail.prototype.start = function () {
    };
    ItemFriendGiveDetail.prototype.setData = function () {
    };
    __decorate([
        property(cc.Label)
    ], ItemFriendGiveDetail.prototype, "uname", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendGiveDetail.prototype, "uid", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendGiveDetail.prototype, "utime", void 0);
    __decorate([
        property(cc.Sprite)
    ], ItemFriendGiveDetail.prototype, "uhead", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendGiveDetail.prototype, "givenum", void 0);
    ItemFriendGiveDetail = __decorate([
        ccclass
    ], ItemFriendGiveDetail);
    return ItemFriendGiveDetail;
}(cc.Component));
exports.default = ItemFriendGiveDetail;

cc._RF.pop();