"use strict";
cc._RF.push(module, 'fb9c3YtKd9Lio7M6uVogfCp', 'ItemFriendExtraDetail');
// Script/UIScript/Friend/ItemFriendExtraDetail.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CocosHelper_1 = require("../../Utils/CocosHelper");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var ItemFriendExtraDetail = /** @class */ (function (_super) {
    __extends(ItemFriendExtraDetail, _super);
    function ItemFriendExtraDetail() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.data_lab = null;
        _this.friend_one_lab = null;
        _this.friend_two_lab = null;
        _this.total_lab = null;
        return _this;
    }
    ItemFriendExtraDetail.prototype.setData = function (data) {
        this.data_lab.string = CocosHelper_1.default.changeTime(data.time);
        this.friend_one_lab.string = data.oneFriendEranings + "元";
        this.friend_two_lab.string = data.twoFriendEranings + "元";
        this.total_lab.string = data.todayTotalEranings + "元";
    };
    __decorate([
        property(cc.Label)
    ], ItemFriendExtraDetail.prototype, "data_lab", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendExtraDetail.prototype, "friend_one_lab", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendExtraDetail.prototype, "friend_two_lab", void 0);
    __decorate([
        property(cc.Label)
    ], ItemFriendExtraDetail.prototype, "total_lab", void 0);
    ItemFriendExtraDetail = __decorate([
        ccclass
    ], ItemFriendExtraDetail);
    return ItemFriendExtraDetail;
}(cc.Component));
exports.default = ItemFriendExtraDetail;

cc._RF.pop();