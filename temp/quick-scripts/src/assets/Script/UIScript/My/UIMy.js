"use strict";
cc._RF.push(module, '830665HxcBGgL4WbMMsN6ET', 'UIMy');
// Script/UIScript/My/UIMy.ts

"use strict";
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var UIForm_1 = require("../../Common/UIForm");
var FormMgr_1 = require("../../Manager/FormMgr");
var UIConfig_1 = require("../../UIConfig");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var UIMy = /** @class */ (function (_super) {
    __extends(UIMy, _super);
    function UIMy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // onLoad () {}
    UIMy.prototype.start = function () {
        var _this = this;
        this.view.BtnClose.addClick(function () {
            _this.closeSelf();
        }, this);
        this.view.BtnGetMoney.addClick(function () {
            // FormMgr.open(UIConfig.UIGetMoneyRule);
        }, this);
        this.view.BtnFriend.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UIMyFriend);
        }, this);
        this.view.BtnVip.addClick(function () {
            // FormMgr.open(UIConfig.UIVip);
        }, this);
        this.view.BtnShare.addClick(function () {
            // FormMgr.open(UIConfig.UIShare);
        }, this);
        this.view.BtnSetting.addClick(function () {
            FormMgr_1.default.open(UIConfig_1.default.UISetting);
        }, this);
        this.view.BtnPlay.addClick(function () {
            // FormMgr.open(UIConfig.UIGame);
        }, this);
    };
    UIMy = __decorate([
        ccclass
    ], UIMy);
    return UIMy;
}(UIForm_1.UIWindow));
exports.default = UIMy;

cc._RF.pop();