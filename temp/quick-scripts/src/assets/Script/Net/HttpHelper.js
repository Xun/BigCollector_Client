"use strict";
cc._RF.push(module, '88ad0BlA9lMAY1NP0RXaOJQ', 'HttpHelper');
// Script/Net/HttpHelper.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var HttpHelper = /** @class */ (function (_super) {
    __extends(HttpHelper, _super);
    function HttpHelper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * get请求
     * @param {string} url
     * @param {function} callback
     */
    HttpHelper.prototype.httpGet = function (url, callback) {
        var xhr = cc.loader.getXMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status == 200) {
                var respone = xhr.responseText;
                cc.log("httpGet response:" + respone);
                var rsp = JSON.parse(respone);
                callback(rsp);
            }
            else if (xhr.readyState === 4 && xhr.status == 401) {
                cc.log("httpGet response:", xhr.readyState);
            }
            else {
                cc.log("httpGet response:" + xhr.readyState);
            }
        };
        cc.log("get url ", url);
        xhr.open('GET', url, true);
        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET, POST');
        xhr.setRequestHeader('Access-Control-Allow-Headers', 'x-requested-with,content-type,authorization');
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.timeout = 8000; // 8 seconds for timeout
        xhr.send();
    };
    /**
     * post请求
     * @param {string} url
     * @param {object} params
     * @param {function} callback
     */
    HttpHelper.prototype.httpPost = function (url, params, callback) {
        var xhr = cc.loader.getXMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status == 200) {
                var respone = xhr.responseText;
                cc.log("response:", respone);
                var rsp = JSON.parse(respone);
                callback(rsp);
            }
            else {
                cc.log("httpPost response error ", xhr.readyState);
            }
        };
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET, POST');
        xhr.setRequestHeader('Access-Control-Allow-Headers', 'x-requested-with,content-type');
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.timeout = 8000; // 8 seconds for timeout
        cc.log("send:", JSON.stringify(params));
        xhr.send(JSON.stringify(params));
    };
    //post 表格数据
    HttpHelper.POST = function (url, param, callback) {
        if (param === void 0) { param = {}; }
        var xhr = cc.loader.getXMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                var response = xhr.responseText;
                if (xhr.status >= 200 && xhr.status < 300) {
                    callback(true, response);
                }
                else {
                    callback(false, response);
                }
            }
        };
        cc.log("send param :" + param);
        xhr.send(JSON.stringify(param));
    };
    HttpHelper = __decorate([
        ccclass
    ], HttpHelper);
    return HttpHelper;
}(cc.Component));
exports.default = HttpHelper;

cc._RF.pop();