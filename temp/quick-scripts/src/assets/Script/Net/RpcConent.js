"use strict";
cc._RF.push(module, '24e90+N+BJCD69wJUWlBqdD', 'RpcConent');
// Script/Net/RpcConent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiClient = void 0;
var tsrpc_browser_1 = require("tsrpc-browser");
var serviceProto_1 = require("./shared/protocols/serviceProto");
exports.apiClient = new tsrpc_browser_1.WsClient(serviceProto_1.serviceProto, {
    server: 'ws://192.168.0.104:3004',
    json: false,
    // jsonPrune: false,
    logger: console,
    heartbeat: {
        interval: 3000,
        timeout: 5000
    }
});

cc._RF.pop();