"use strict";
cc._RF.push(module, 'd47afYEAFhDBI9nPCEezcSK', 'DataModalMgr');
// Script/Manager/DataModalMgr.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataModalMgr = void 0;
var DataBag_1 = require("../DataModal/DataBag");
var DataCollection_1 = require("../DataModal/DataCollection");
var DataDeal_1 = require("../DataModal/DataDeal");
var DataFriend_1 = require("../DataModal/DataFriend");
var DataGetMoneyLog_1 = require("../DataModal/DataGetMoneyLog");
var DataMyExtar_1 = require("../DataModal/DataMyExtar");
var DataProduce_1 = require("../DataModal/DataProduce");
var DataRank_1 = require("../DataModal/DataRank");
var DataShop_1 = require("../DataModal/DataShop");
var DataUser_1 = require("../DataModal/DataUser");
var DataModalMgr = /** @class */ (function () {
    function DataModalMgr() {
        //用户信息
        this.UserInfo = new DataUser_1.UserInfo();
        this.CurBuyProduceInfo = new DataProduce_1.DataCurBuyProduce();
        this.ItemProfucesJson = new DataProduce_1.ProducesJson();
        this.CollectionInfo = new DataCollection_1.DataCollection();
        this.BagInfo = new DataBag_1.DataBagInfo();
        this.ShopInfo = new DataShop_1.DataShop();
        this.FriendInfo = new DataFriend_1.DataFriend();
        this.FriendMonerDetailInfo = new DataFriend_1.DataFriendMoneyDetail();
        this.DataGetMoneyLogInfo = new DataGetMoneyLog_1.DataGetMoneyLog();
        this.DataMyExtarInfo = new DataMyExtar_1.DataMyExtar();
        this.DataFriendGiveDetailInfo = new DataFriend_1.DataFriendGiveDetail();
        this.DataSeeDetailInfo = new DataFriend_1.DataSeeDetail();
        this.DataDealInfo = new DataDeal_1.DataDeal();
        this.ChipsInfoJson = new DataShop_1.ChipJson();
        this.ShopInfoJson = new DataShop_1.ShopJson();
        this.DataRankInfo = new DataRank_1.DataRank();
    }
    return DataModalMgr;
}());
exports.DataModalMgr = DataModalMgr;

cc._RF.pop();