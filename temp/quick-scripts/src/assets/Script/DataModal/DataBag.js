"use strict";
cc._RF.push(module, '52db2OIKg1MD6nep22RXot2', 'DataBag');
// Script/DataModal/DataBag.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataBagItem = exports.DataBagInfo = void 0;
var DataBagInfo = /** @class */ (function () {
    function DataBagInfo() {
        this.list_item = Array();
        this.cur_bag_capacity = 0;
        this.total_bag_capacity = 6;
    }
    return DataBagInfo;
}());
exports.DataBagInfo = DataBagInfo;
var DataBagItem = /** @class */ (function () {
    function DataBagItem() {
        this.itemID = 0;
        this.itemCount = 0;
    }
    return DataBagItem;
}());
exports.DataBagItem = DataBagItem;

cc._RF.pop();