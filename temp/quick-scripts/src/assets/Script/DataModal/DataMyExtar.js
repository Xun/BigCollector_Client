"use strict";
cc._RF.push(module, '0f0afHSrQJNkrg6A0LbouVY', 'DataMyExtar');
// Script/DataModal/DataMyExtar.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataMyExtar = void 0;
var DataMyExtar = /** @class */ (function () {
    function DataMyExtar() {
        this.today_extar_get_money = 0;
        this.today_extar_add_user = 0;
        this.month_extar_get_money = 0;
        this.month_extar_add_user = 0;
        this.total_extar_get_money = 0;
        this.total_extar_add_user = 0;
        this.cur_stage = "";
        this.cur_stage_money = 0;
        this.total_stage_moner = 0;
        this.add_mult = 0;
        this.complete_money = 0;
    }
    return DataMyExtar;
}());
exports.DataMyExtar = DataMyExtar;

cc._RF.pop();