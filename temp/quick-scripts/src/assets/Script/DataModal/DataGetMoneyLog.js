"use strict";
cc._RF.push(module, '8fdd6kyxBJPu53TUeDnNh5t', 'DataGetMoneyLog');
// Script/DataModal/DataGetMoneyLog.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataItemGetMoneyLog = exports.DataGetMoneyLog = void 0;
var DataGetMoneyLog = /** @class */ (function () {
    function DataGetMoneyLog() {
        this.list_get_money_log = Array();
    }
    return DataGetMoneyLog;
}());
exports.DataGetMoneyLog = DataGetMoneyLog;
var DataItemGetMoneyLog = /** @class */ (function () {
    function DataItemGetMoneyLog() {
        this.get_data = "";
        this.get_money_num = 0;
    }
    return DataItemGetMoneyLog;
}());
exports.DataItemGetMoneyLog = DataItemGetMoneyLog;

cc._RF.pop();