"use strict";
cc._RF.push(module, 'cb104U/zSdKjZtkItAsSNb3', 'DataProduce');
// Script/DataModal/DataProduce.ts

"use strict";
// import Long = require("Long");
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProducesJson = exports.DataCurBuyProduce = void 0;
var DataCurBuyProduce = /** @class */ (function () {
    function DataCurBuyProduce() {
        //ID
        this._id = '0';
        //名字
        this._name = '0';
        //图片
        this._img = '0';
        //基础购买价格
        this._buyPrice = 0;
    }
    Object.defineProperty(DataCurBuyProduce.prototype, "id", {
        get: function () {
            if (this._id === undefined) {
                this._id = "0";
            }
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCurBuyProduce.prototype, "name", {
        get: function () {
            if (this._name === undefined) {
                this._name = "0";
            }
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCurBuyProduce.prototype, "img", {
        get: function () {
            if (this._img === undefined) {
                this._img = "0";
            }
            return this._img;
        },
        set: function (value) {
            this._img = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DataCurBuyProduce.prototype, "buyPrice", {
        get: function () {
            if (this._buyPrice === undefined) {
                this._buyPrice = 0;
            }
            return this._buyPrice;
        },
        set: function (value) {
            this._buyPrice = value;
        },
        enumerable: false,
        configurable: true
    });
    return DataCurBuyProduce;
}());
exports.DataCurBuyProduce = DataCurBuyProduce;
var ProducesJson = /** @class */ (function () {
    function ProducesJson() {
    }
    return ProducesJson;
}());
exports.ProducesJson = ProducesJson;

cc._RF.pop();