"use strict";
cc._RF.push(module, 'bd5f86TYsJLkanbpbzBMa08', 'DataShop');
// Script/DataModal/DataShop.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopJson = exports.ChipJson = exports.DataShopItem = exports.DataShop = void 0;
var DataShop = /** @class */ (function () {
    function DataShop() {
        this.list_item = Array();
    }
    return DataShop;
}());
exports.DataShop = DataShop;
var DataShopItem = /** @class */ (function () {
    function DataShopItem() {
        this.id = "";
        this.name = "";
        this.need_num = "";
        this.need_type = "";
    }
    return DataShopItem;
}());
exports.DataShopItem = DataShopItem;
var ChipJson = /** @class */ (function () {
    function ChipJson() {
        this.chipInfo = [];
    }
    return ChipJson;
}());
exports.ChipJson = ChipJson;
var ShopJson = /** @class */ (function () {
    function ShopJson() {
        this.shopInfo = [];
    }
    return ShopJson;
}());
exports.ShopJson = ShopJson;

cc._RF.pop();