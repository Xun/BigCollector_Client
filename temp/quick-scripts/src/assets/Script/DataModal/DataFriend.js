"use strict";
cc._RF.push(module, 'a9d27ygiTFIVKNNK8n/l1hH', 'DataFriend');
// Script/DataModal/DataFriend.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataItemFriendInfo = exports.DataItemSeeDetail = exports.DataSeeDetail = exports.DataItemFriendGiveDetail = exports.DataFriendGiveDetail = exports.DataItemFriendMoneyDetail = exports.DataFriendMoneyDetail = exports.DataFriend = void 0;
var DataFriend = /** @class */ (function () {
    function DataFriend() {
        this.my_friend_num = 0;
        this.probabyly_money = 0;
        this.today_get_money = 0;
        this.total_get_money = 0;
        this.today_extar_get_money = 0;
        this.cur_get_money = 0;
        this.my_inviter_name = "";
        this.my_invite_head = "";
    }
    return DataFriend;
}());
exports.DataFriend = DataFriend;
var DataFriendMoneyDetail = /** @class */ (function () {
    function DataFriendMoneyDetail() {
        this.today_get_money = 0;
        this.total_get_money = 0;
        this.list_item_friend_money_detail = Array();
    }
    return DataFriendMoneyDetail;
}());
exports.DataFriendMoneyDetail = DataFriendMoneyDetail;
var DataItemFriendMoneyDetail = /** @class */ (function () {
    function DataItemFriendMoneyDetail() {
        this.time = 0;
        this.oneFriendEranings = 0;
        this.twoFriendEranings = 0;
        this.todayTotalEranings = 0;
    }
    return DataItemFriendMoneyDetail;
}());
exports.DataItemFriendMoneyDetail = DataItemFriendMoneyDetail;
var DataFriendGiveDetail = /** @class */ (function () {
    function DataFriendGiveDetail() {
        this.list_item_friend_give_detail = Array();
    }
    return DataFriendGiveDetail;
}());
exports.DataFriendGiveDetail = DataFriendGiveDetail;
var DataItemFriendGiveDetail = /** @class */ (function () {
    function DataItemFriendGiveDetail() {
        this.user_name = "";
        this.user_id = 0;
        this.user_head = "";
        this.give_time = "";
        this.give_num = 0;
    }
    return DataItemFriendGiveDetail;
}());
exports.DataItemFriendGiveDetail = DataItemFriendGiveDetail;
var DataSeeDetail = /** @class */ (function () {
    function DataSeeDetail() {
        this.list_item_friend_give_detail = Array();
    }
    return DataSeeDetail;
}());
exports.DataSeeDetail = DataSeeDetail;
var DataItemSeeDetail = /** @class */ (function () {
    function DataItemSeeDetail() {
        this.user_name = "";
        this.user_id = 0;
        this.user_head = "";
        this.see_time = "";
    }
    return DataItemSeeDetail;
}());
exports.DataItemSeeDetail = DataItemSeeDetail;
var DataItemFriendInfo = /** @class */ (function () {
    function DataItemFriendInfo() {
        this.user_name = "";
        this.user_level = "";
        this.user_head = "";
        this.add_time = "";
    }
    return DataItemFriendInfo;
}());
exports.DataItemFriendInfo = DataItemFriendInfo;

cc._RF.pop();